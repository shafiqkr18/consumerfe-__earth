<?php

return [
    "footer"    =>  [
        "links"     =>  [
            "facebook"      =>  "https://www.facebook.com/ZoomPropertyUAE/",
            "google"        =>  "https://plus.google.com/u/0/103251563061642583375",
            "twitter"       =>  "https://twitter.com/zoompropertyuae",
            "instagram"     =>  "https://www.instagram.com/zoompropertyuae/",
            "youtube"       =>  "https://www.youtube.com/channel/UCR5oOwcFEES5IoiSxD-vk6g",
            "linkedin"       =>  "https://www.linkedin.com/company/zoom-property/",
            "play_store"    =>  "https://play.google.com/store/apps/details?id=com.zoomproperty.consumerapp_new",
            "app_store"     =>  "https://itunes.apple.com/sa/app/zoom-property-search/id1329272689?mt=8&amp;ign-mpt=uo%3D2"
        ]
    ],
    "seo"  =>  [
        "pages" =>  [
            [
                "name"  =>  "home",
                "id"    =>  "home",
                "value" =>  "home"
            ],
            [
                "name"  =>  "Property Listings",
                "id"    =>  "property_listings",
                "value" =>  "property_listings"
            ],
            [
                "name"  =>  "Property Details",
                "id"    =>  "property_details",
                "value" =>  "property_details"
            ],
            [
                "name"  =>  "Project Listings",
                "id"    =>  "project_listings",
                "value" =>  "project_listings"
            ],
            [
                "name"  =>  "Project Details",
                "id"    =>  "project_details",
                "value" =>  "project_details"
            ],
            [
                "name"  =>  "Budget Search Landing",
                "id"    =>  "budget_search_landing",
                "value" =>  "budget_search_landing"
            ],
            [
                "name"  =>  "Budget Search Listings",
                "id"    =>  "budget_search_listings",
                "value" =>  "budget_search_listings"
            ],
            [
                "name"  =>  "Agent Landing",
                "id"    =>  "agent_landing",
                "value" =>  "agent_landing"
            ],
            [
                "name"  =>  "Agent Listings",
                "id"    =>  "agent_listings",
                "value" =>  "agent_listings"
            ],
            [
                "name"  =>  "Agent Details",
                "id"    =>  "agent_details",
                "value" =>  "agent_details"
            ],
            [
                "name"  =>  "Home Services Landing",
                "id"    =>  "services_landing",
                "value" =>  "services_landing"
            ],
            [
                "name"  =>  "Home Services Listings",
                "id"    =>  "services_listings",
                "value" =>  "services_listings"
            ],
            [
                "name"  =>  "Real Choice",
                "id"    =>  "real_choice",
                "value" =>  "real_choice"
            ],
            [
                "name"  =>  "Privacy Policy",
                "id"    =>  "privacy_policy",
                "value" =>  "privacy_policy"
            ],
            [
                "name"  =>  "Terms and Conditions",
                "id"    =>  "terms_and_conditions",
                "value" =>  "terms_and_conditions"
            ]
        ],
        "parameters"    =>  [
            "home"                  =>  [],
            "terms_and_conditions"  =>  [],
            "privacy_policy"        =>  [],
            "property_listings"     =>  [
                [
                    "name"  => "Rent/Buy",
                    "value" => "rent_buy"
                ],
                [
                    "name"  =>  "Residential Commercial",
                    "value" =>  "residential_commercial"
                ],
                [
                    "name"  =>  "City",
                    "value" =>  "city"
                ],
                [
                    "name"  =>  "Area",
                    "value" =>  "area"
                ],
                [
                    "name"  =>  "Building",
                    "value" =>  "building"
                ],
                [
                    "name"  =>  "Property Type",
                    "value" =>  "type"
                ],
                [
                    "name"  =>  "Bedroom (min)",
                    "value" =>  "bedroom.min"
                ],
                [
                    "name"  =>  "Bedroom (max)",
                    "value" =>  "bedroom.max"
                ],
                [
                    "name"  =>  "Bathroom (min)",
                    "value" =>  "bathroom.min"
                ],
                [
                    "name"  =>  "Bathroom (max)",
                    "value" =>  "bathroom.max"
                ],
                [
                    "name"  =>  "Price (min)",
                    "value" =>  "price.min"
                ],
                [
                    "name"  =>  "Price (max)",
                    "value" =>  "price.max"
                ],
                [
                    "name"  =>  "Featured",
                    "value" =>  "featured"
                ]
            ],
            "property_details"      =>  [
                [
                    "name"  =>  "Rent/Buy",
                    "value" =>  "rent_buy"
                ],
                [
                    "name"  =>  "Residential Commercial",
                    "value" =>  "residential_commercial"
                ],
                [
                    "name"  =>  "City",
                    "value" =>  "city"
                ],
                [
                    "name"  =>  "Area",
                    "value" =>  "area"
                ],
                [
                    "name"  =>  "Building",
                    "value" =>  "building"
                ],
                [
                    "name"  =>  "Property Type",
                    "value" =>  "type"
                ],
                [
                    "name"  =>  "Bedroom",
                    "value" =>  "bedroom"
                ],
                [
                    "name"  =>  "Bathroom",
                    "value" =>  "bathroom"
                ],
                [
                    "name"  =>  "Title",
                    "value" =>  "writeup.title"
                ],
                [
                    "name"  =>  "Price",
                    "value" =>  "price"
                ],
                [
                    "name"  =>  "Reference Number",
                    "value" =>  "ref_no"
                ],
                [
                    "name"  =>  "Built-up Area",
                    "value" =>  "dimension.builtup_area"
                ]
            ],
            "project_listings"      =>  [
                [
                    "name"  =>  "Developer Name",
                    "value" =>  "developer_name"
                ],
                [
                    "name"  =>  "Total Projects",
                    "value" =>  "total"
                ]
            ],
            "project_details"       =>  [
                [
                    "name"  =>  "Developer Name",
                    "value" =>  "developer.contact.name"
                ],
                [
                    "name"  =>  "Project Name",
                    "value" =>  "writeup.title"
                ],
                [
                    "name"  =>  "Project Description",
                    "value" =>  "writeup.description"
                ],
                [
                    "name"  =>  "Area",
                    "value" =>  "area"
                ],
                [
                    "name"  =>  "City",
                    "value" =>  "city"
                ],
                [
                    "name"  =>  "Property Type",
                    "value" =>  "unit.property_type"
                ]
            ],
            "agent_landing"         =>  [],
            "agent_listings"        =>  [
                [
                    "name"  =>  "Agency Name",
                    "value" =>  "agency_name"
                ],
                [
                    "name"  =>  "Area",
                    "value" =>  "area"
                ],
                [
                    "name"  =>  "Total Agents",
                    "value" =>  "total"
                ]
            ],
            "agent_details"         =>  [
                [
                    "name"  =>  "Agent Name",
                    "value" =>  "contact.name"
                ],
                [
                    "name"  =>  "Agency Name",
                    "value" =>  "agency.contact.name"
                ],
                [
                    "name"  =>  "Agent Bio",
                    "value" =>  "contact.about"
                ]
            ],
            "services_landing"      =>  [],
            "services_listings"     =>  [
                [
                    "name"  =>  "Service",
                    "value" =>  "service.name"
                ],
                [
                    "name"  =>  "Total Services",
                    "value" =>  "total"
                ]
            ]
        ],
        "property_type" =>  [
            "alias" =>  [
                'Apartment' =>  "Flat",
                "Villa"     =>  "House",
                "Townhouse" =>  "House"
            ]
        ],
        'internal_links'    =>  [
            'home'  =>  [
        		[
        			'property_type'             =>  'apartment',
        			'rent_sale'                 =>  'rent',
        			'label'                     =>  'Apartments for rent'
        		],
        		[
        			'property_type'             =>  'apartment',
        			'rent_sale'                 =>  'sale',
        			'label'                     =>  'Apartments for sale'
        		],
        		[
        			'property_type'             =>  'villa',
        			'rent_sale'                 =>  'sale',
        			'label'                     =>  'Villas for sale'
        		],
        		[
        			'property_type'             =>  'villa',
        			'rent_sale'                 =>  'rent',
        			'label'                     =>  'Villas for rent'
                ],
        		[
        			'property_type'             =>  'townhouse',
        			'rent_sale'                 =>  'sale',
        			'label'                     =>  'Townhouse for sale'
        		],
        		[
        			'property_type'             =>  'townhouse',
        			'rent_sale'                 =>  'rent',
        			'label'                     =>  'Townhouse for rent'
        		]
            ]
        ]
    ],
    "property"  =>  [
        "query_params"  =>  [
            'agent_id'          =>  'agent._id',
            'agent_email'       =>  'agent.contact.email',
            'agency_id'         =>  'agent.agency._id',
            'agency_email'      =>  'agent.agency.contact.email',
            'furnishing'        =>  'furnished',
            'status'            =>  'completion_status',
            'keyword'           =>  'keyword',
            'page'              =>  'page',
            'sort'              =>  'sort'
        ]
    ],
    "datalayer"  =>  [
        "fields"  =>  [
            "property"  =>  [
                'residential_commercial',
                'rent_buy',
                'city',
                'area',
                'building',
                '_id',
                'ref_no',
                'type',
                'furnished',
                'bedroom',
                'bathroom',
                'price',
                'dimension.builtup_area',
                'agent._id',
                'agent.contact.name',
                'agent.contact.phone',
                'agent.contact.picture',
                'agent.agency._id',
                'agent.agency.contact.name',
                'number'
            ],
            "agent"  =>  [
                '_id',
                'contact.name',
                'contact.phone',
                'contact.picture',
                'agency._id',
                'agency.contact.name',
                'number'
            ],
            "project"   =>  [
                '_id',
                'name',
                'city',
                'area',
                'pricing.starting',
                'unit.property_type',
                'developer._id',
                'developer.contact.name',
                'developer.contact.phone',
                'settings.custom_form',
                'number'
            ]
        ]
    ],
    'country_codes'    =>  [
        'AE'    =>  'UAE',
        'SA'    =>  'KSA',
        'GB'    =>  'UK'
    ]
];
