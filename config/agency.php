<?php

    return  [

        "email_ids" =>  [
                [
                    "name"  =>  "For Est Real Estate",
                    "id"    =>  "zb-forest-real-estate-12082",
                    "email" =>  "info@forest.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Exclusive Links",
                    "id"    =>  "zb-exclusive-links-real-estate-brokers-708",
                    "email" =>  "zarah@exclusive-links.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Union Square House Real Estate",
                    "id"    =>  "zb-union-square-house-real-estate-2443",
                    "email" =>  "waleed@ushre.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "MD Properties",
                    "id"    =>  "zb-md-properties-17252",
                    "email" =>  "leads@mdproperties.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "HMS Homes",
                    "id"    =>  "zb-hms-homes-15570",
                    "email" =>  "info@hmshomes.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Candour Real Estate LLC",
                    "id"    =>  "zb-candour-real-estate-llc-CN-1010367",
                    "email" =>  "info@candourproperty.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Eagle Hills",
                    "id"    =>  "zb-eagle-hills-859366",
                    "email" =>  "info@eaglehills.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Eagle Hills",
                    "id"    =>  "zb-eagle-hills-859366",
                    "email" =>  "info@eaglehills.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "DAMAC",
                    "id"    =>  "zb-damac-17493",
                    "email" =>  "marissa.gonsalves@damacgroup.com",
                    "dev"   =>  "zd-damac-17493"
                ],
                [
                    "name"  =>  "Emaar Properties",
                    "id"    =>  "zb-emaar-properties-634201",
                    "email" =>  "YBarghouti@emaar.ae",
                    "dev"   =>  "zd-emaar-89364"
                ],
                [
                    "name"  =>  "Hamptons International",
                    "id"    =>  "zb-hamptons-international-358",
                    "email" =>  "enquiries@hamptons.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Lahej and Sultan Real Estate",
                    "id"    =>  "zb-lahejand-sultan-real-estate-16692",
                    "email" =>  "info@lahejandsultanrealestate.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Prestige Real Estate",
                    "id"    =>  "zb-prestige-real-estate-1981",
                    "email" =>  "otabek@prestigedubai.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "OBG Real Estate Broker",
                    "id"    =>  "zb-obg-real-estate-broker-17196",
                    "email" =>  "joyce@obg.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Meraas",
                    "id"    =>  "zb-meraas-56893",
                    "email" =>  "sales.enquiry@meraas.ae",
                    "dev"   =>  "zd-meraas-56893"
                ],
                [
                    "name"  =>  "Crompton Partners Estate Agents AUH",
                    "id"    =>  "zb-crompton-partners-estate-agents-auh-CN-1487192",
                    "email" =>  "dennis@cpestateagents.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Driven Properties",
                    "id"    =>  "zb-driven-properties-11917",
                    "email" =>  "fatma@drivenproperties.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Harbor Real Estate",
                    "id"    =>  "zb-harbor-real-estate-2004",
                    "email" =>  "maybelle@harbordubai.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Aqua Properties",
                    "id"    =>  "zb-aqua-properties-303",
                    "email" =>  "martha@aquaproperties.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "JK Properties",
                    "id"    =>  "zb-jk-properties-2563",
                    "email" =>  "jesusa@jk-properties.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Real Choice Real Estate Brokers LLC",
                    "id"    =>  "zb-real-choice-real-estate-brokers-llc-1068",
                    "email" =>  "info@dubaifirsthome.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "A1 Properties LLC",
                    "id"    =>  "zb-a1-properties-llc-12095",
                    "email" =>  "info@a1properties.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Binayah Real Estate Brokers LLC",
                    "id"    =>  "zb-binayah-real-estate-brokers-llc-1162",
                    "email" =>  "crm3@binayah.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "MJB Properties",
                    "id"    =>  "zb-mjb-properties-12388",
                    "email" =>  "info@mjb.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Nationwide Middle East Properties",
                    "id"    =>  "zb-nationwide-middle-east-properties-CN-1197386",
                    "email" =>  "Marketing@nwmea.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "La Capitale Real Estate",
                    "id"    =>  "zb-la-capitale-real-estate-2610",
                    "email" =>  "info@lcdubai.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Clarke & Scott Real Estate",
                    "id"    =>  "zb-clarke-scott-real-estate-15981",
                    "email" =>  "info@clarkeandscott.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Rocky Real Estate Brokerage LLC",
                    "id"    =>  "zb-rocky-real-estate-brokerage-llc-16963",
                    "email" =>  "aubrey.i@rockyrealestate.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "PH Real Estate",
                    "id"    =>  "zb-ph-real-estate-15997",
                    "email" =>  "admin@phrealestate.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "D&B Properties",
                    "id"    =>  "zb-db-properties-16576",
                    "email" =>  "fm@dandbdubai.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Arms & McGregor International Realty",
                    "id"    =>  "zb-arms-mc-gregor-international-realty-12283",
                    "email" =>  "marketing@armsmcgregor.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Dacha Real Estate",
                    "id"    =>  "zb-dacha-real-estate-393",
                    "email" =>  "info@dacha-re.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Aim Properties",
                    "id"    =>  "zb-aim-properties-12454",
                    "email" =>  "eddyn@aimproperties.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Mylo Real Estate",
                    "id"    =>  "zb-mylo-real-estate-12800",
                    "email" =>  "mo@mylorealestate.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Select Group",
                    "id"    =>  "zb-select-group-12562",
                    "email" =>  "afaf.burki@select-group.ae",
                    "dev"   =>  "zd-select-group-66743"
                ],
                [
                    "name"  =>  "Select Property",
                    "id"    =>  "zb-select-property-694",
                    "email" =>  "listings@selectproperty.ae",
                    "dev"   =>  "zd-select-property-56719"
                ],
                [
                    "name"  =>  "ENGEL & VÖLKERS Dubai",
                    "id"    =>  "zb-engel-volkers-dubai-16081",
                    "email" =>  "edna.alvarado@engelvoelkers.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Hunt & Harris Real Estate Dubai",
                    "id"    =>  "zb-hunt-harris-real-estate-dubai-27779",
                    "email" =>  "info@huntandharris.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Haus & Haus",
                    "id"    =>  "zb-haus-haus-12357",
                    "email" =>  "kristoff@hausandhaus.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "St. Clair Real Estate",
                    "id"    =>  "zb-st-clair-real-estate-1413",
                    "email" =>  "inquiry@stclair.ae",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "My Island Real Estate Brokers LLC",
                    "id"    =>  "zb-my-island-real-estate-brokers-llc-12739",
                    "email" =>  "info@myislandrealestate.com",
                    "dev"   =>  ""
                ],
                [
                    "name"  =>  "Prescott",
                    "id"    =>  "zb-prescott-448627",
                    "email" =>  "zeeshan@prescottuae.ae",
                    "dev"   =>  "zd-prescott-78392"
                ]
            ]
        ];
