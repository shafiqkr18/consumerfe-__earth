<?php

return [
    'area'  => [
      'difc'        =>  'DIFC',
      'damac hills' =>  'DAMAC Hills',
      'emaar beachfront' =>  'EMAAR Beachfront',
      'rak industrial and technology park' =>  'RAK Industrial And Technology Park',
      'dubiotech' =>  'DuBiotech'
    ],
    'building'  =>  [
      'ad one tower' =>  'AD One Tower',
      'rak tower' =>  'RAK Tower',
      'skb plaza' =>  'SKB Plaza',
      'dubiotech' =>  'DuBiotech',
      'azizi aura' =>  'AZIZI Aura',
      'the loft office 2' =>  'The LOFT Office 2',
      'the loft office 3' =>  'The LOFT Office 3',
      'vida residences' =>  'VIDA Residences',
      'acico business park' =>  'ACICO Business Park',
      'blvd crescent' =>  'BLVD Crescent',
      'damac maison the distinction' =>  'DAMAC Maison The Distinction',
      'hds sunstar ii' =>  'HDS Sunstar II',
      'cbd' =>  'CBD (Central Business District)',
      'jafza' =>  'JAFZA',
      'jafza warehouse' =>  'JAFZA Warehouse',
      'art xii' =>  'Art XII',
      'api residency' =>  'API Residency',
      'api trio towers' =>  'API Trio Towers',
      'dxb tower' =>  'DXB Tower',
      'dip 2' =>  'DIP 2',
      'emaar beachfront' =>  'EMAAR Beachfront',
      'sama townhouses' =>  'SAMA Townhouses',
      'una apartments' =>  'UNA Apartments',
      'mag 5 boulevard' =>  'MAG 5 Boulevard',
      'mag 5' =>  'MAG 5',
      'emaar south' =>  'EMAAR South',
      'damac maison de ville tenora' =>  'DAMAC Maison De Ville Tenora',
      'azizi riviera' =>  'AZIZI Riviera',
      'azizi riviera 26' =>  'AZIZI Riviera 26',
      'azizi riviera 35' =>  'AZIZI Riviera 35',
      'azizi riviera 10' =>  'AZIZI Riviera 10',
      'azizi riviera 23' =>  'AZIZI Riviera 23',
      'azizi riviera 1' =>  'AZIZI Riviera 1',
      'azizi riviera 11' =>  'AZIZI Riviera 11',
      'azizi riviera 16' =>  'AZIZI Riviera 16',
      'azizi riviera 21' =>  'AZIZI Riviera 21',
      'azizi riviera 32' =>  'AZIZI Riviera 32',
      'azizi riviera 37' =>  'AZIZI Riviera 37',
      'azizi riviera 4' =>  'AZIZI Riviera 4',
      'azizi riviera 40' =>  'AZIZI Riviera 40',
      'azizi riviera 5' =>  'AZIZI Riviera 5',
      'azizi gardens' =>  'AZIZI Gardens',
      'azizi residence' =>  'AZIZI Residence',
      'azizi roy mediterranean' =>  'AZIZI Roy Mediterranean',
      'azizi star serviced apartments' =>  'AZIZI Star Serviced Apartments',
      'azizi shaista' =>  'AZIZI Shaista',
      'azizi plaza' =>  'AZIZI Plaza',
      'azizi freesia' =>  'AZIZI Freesia',
      'azizi liatris' =>  'AZIZI Liatris',
      'azizi berton' =>  'AZIZI Berton',
      'azizi feirouz' =>  'AZIZI Feirouz',
      'azizi pearl' =>  'AZIZI Pearl',
      'azizi yasamine' =>  'AZIZI Yasamine',
      'azizi samia serviced apartments' =>  'AZIZI Samia Serviced Apartments',
      'the polo residence c iv' =>  'The Polo Residence C IV',
      'the polo residence c ii' =>  'The Polo Residence C II',
      'the polo residence b iii' =>  'The Polo Residence B III',
      'damac villas by paramount hotels and resorts' =>  'DAMAC Villas By Paramount Hotels And Resorts',
      'naia golf terrace at akoya' =>  'NAIA Golf Terrace At Akoya',
      'difc' =>  'DIFC',
      'jbr' =>  'JBR',
      'mag eye' =>  'MAG Eye',
      'koa canvas' =>  'KOA Canvas',
      'sit tower' =>  'SIT Tower',
      'milano by giovanni botique suites' =>  'MILANO By Giovanni Botique Suites',
      'aces chateau' =>  'ACES Chateau',
      'spica residential' =>  'SPICA Residential',
      'five at jumeirah village circle' =>  'FIVE At Jumeirah Village Circle',
      'sidra villas iii' =>  'Sidra Villas III',
      'sidra villas ii' =>  'Sidra Villas II',
      'se7en city jlt' =>  'Se7en City JLT',
      'mazaya business avenue bb2' =>  'Mazaya Business Avenue BB2',
      'mbl residences' =>  'MBL Residences',
      'hds business centre' =>  'HDS Business Centre',
      'one jlt' =>  'One JLT',
      'damac maison privé' =>  'DAMAC Maison Privé',
      'xl tower' =>  'XL Tower',
      'b2b tower' =>  'B2B Tower',
      'mag 318' =>  'MAG 318',
      'damac maison canal views' =>  'DAMAC Maison Canal Views',
      'damac maison cour jardin' =>  'DAMAC Maison Cour Jardin',
      'rbc tower' =>  'RBC Tower',
      'damac maison the vogue' =>  'DAMAC Maison The Vogue',
      'mbk tower' =>  'MBK Tower',
      'vida residences dubai mall' =>  'VIDA Residences Dubai Mall',
      'damac maison the distinction' =>  'DAMAC Maison The Distinction',
      'blvd heights tower 1' =>  'BLVD Heights Tower 1',
      'dt1' =>  'DT1',
      'blvd crescent 1' =>  'BLVD Crescent 1',
      'blvd crescent 2' =>  'BLVD Crescent 2',
      'blvd heights tower 2' =>  'BLVD Heights Tower 2',
      'il primo' =>  'IL Primo',
      'blvd heights podium' =>  'BLVD Heights Podium',
      'the address blvd sky collection' =>  'The Address BLVD Sky Collection',
      'five palm jumeirah' =>  'FIVE Palm Jumeirah',
      'one palm' =>  'ONE PALM',
      'liv residence' =>  'LIV Residence',
      'mag 218' =>  'MAG 218',
      'vida residences dubai marina' =>  'VIDA Residences Dubai Marina',
      'dec tower 1' =>  'DEC Tower 1',
      'dec tower 2' =>  'DEC Tower 2',
      'dec towers' =>  'DEC Towers',
      'jam marina residence' =>  'JAM Marina Residence',
      'kg tower' =>  'KG Tower',
      'damac heights' =>  'DAMAC Heights',
      'damac residenze' =>  'DAMAC Residenze',
      'tfg marina hotel' =>  'TFG Marina Hotel',
    ]
]

?>
