<?php

return [
    [
        'ref_no'  =>  'prestigedubai-794940',
        'writeup'   =>  [
            'title'     =>  'Sea view | Type A | High Floor | Hatimi'
        ],
        'image'     =>  "https://s3-ap-southeast-1.amazonaws.com/mycrm-pro-accounts-v2/property/full/819/bCgv1AIsQlIlNAFZ.jpeg",
        '_id'       =>  'zp-prestigedubai-794940',
        'price'     =>  3500000,
        'dimension'     =>  [
            'builtup_area'  =>  2158
        ],
        'bedroom'   =>  3,
        'bathroom'  =>  3,
        'type'      =>  'Apartment',
        'rent_buy'  =>  'Sale',
        'residential_commercial'    =>  'Residential',
        'agent'     =>  [
            'contact'   =>  [
                'phone'     =>  '971508523945',
                'name'      =>  'Jade Maras',
                'email'     =>  'jade@prestigedubai.com'
            ],
            'agency'    =>  [
                'contact'   =>  [
                    'name'      =>  'Prestige Real Estate',
                    'phone'     =>  '044468300',
                    'email'     =>  'otabek@prestigedubai.com'
                ]
            ]
        ]
    ],
    [
        'ref_no'  =>  'PRS-S-3584',
        'writeup'   =>  [
            'title'     =>  'Marina I 2 Bed I High Floor'
        ],
        'image'     =>  "https://s3.ap-south-1.amazonaws.com/zp-property-images/zp-PRS-S-3584_1_desktop",
        '_id'       =>  'zp-PRS-S-3584',
        'price'     =>  2750000,
        'dimension'     =>  [
            'builtup_area'  =>  1601
        ],
        'bedroom'   =>  2,
        'bathroom'  =>  3,
        'type'      =>  'Apartment',
        'rent_buy'  =>  'Sale',
        'residential_commercial'    =>  'Residential',
        'agent'     =>  [
            'contact'   =>  [
                'phone'     =>  '971508523945',
                'name'      =>  'Jessica Horie',
                'email'     =>  'jessica@prestigedubai.com'
            ],
            'agency'    =>  [
                'contact'   =>  [
                    'name'      =>  'Prestige Real Estate',
                    'phone'     =>  '044468300',
                    'email'     =>  'otabek@prestigedubai.com'
                ]
            ]
        ]
    ]
]

?>
