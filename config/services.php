<?php

if( str_contains( env( 'APP_URL' ), [ 'localhost', 'zp', 'consumerfe' ] ) ){

    $facebook_client_id         =   '1943393372647418';
    $facebook_client_secret     =   '26bc0673429ba67c4d3f1dc1139ca794';

    $google_client_id           =   '204730724171-ei377q7e2dpdrhk7dm7mdbc6ieih747e.apps.googleusercontent.com';
    $google_client_secret       =   'vYgFFz7MxedPT3LOQTCutPK8';

    $google_maps_api_key        =   'AIzaSyD4sP2TVMnjLZS5WNjml-GgFjJhPvvtu7U';

}
else {

    $facebook_client_id         =   '394613127644138';
    $facebook_client_secret     =   '169f58c4f2e523709db48daced2f6a8c';

    $google_client_id           =   '653339222179-8jsq04b6nhguc44kkrh79f9gcpenni46.apps.googleusercontent.com';
    $google_client_secret       =   'DpJLctMFLpTwGocQbrZZEK5F';

    $google_maps_api_key        =   'AIzaSyC2U4EUuRuD0K5SY4vi2wtscuIW3AOfQc4';

}

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => $facebook_client_id,
        'client_secret' => $facebook_client_secret,
        'redirect' => env( 'APP_URL' ).'login/facebook/callback',
    ],

    'google' => [
        'client_id' => $google_client_id,
        'client_secret' => $google_client_secret,
        'redirect' => env( 'APP_URL' ).'login/google/callback',
        'map_api_key'   =>  $google_maps_api_key
    ],

];
