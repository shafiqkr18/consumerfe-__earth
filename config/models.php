<?php

return [
    'user'      =>  [
        'leaf'      =>  ($leaf='user'),
        'api'       =>  [
            'endpoint'  =>  [
                'lo'        =>  $leaf.'/auth/login',
                're'        =>  $leaf.'/auth/register',
                'so'        =>  $leaf.'/auth/social',
                'vf'        =>  $leaf.'/auth/verify',
                'fo'        =>  $leaf.'/auth/forgot',
                'rs'        =>  $leaf.'/auth/reset',
                'sh'        =>  $leaf.'/#',
                'up'        =>  $leaf.'/#',
                'ch'        =>  $leaf.'/#/auth/change',
                'favourite'     =>  [
                    'model'         =>  $leaf.'/#/favourite/#/#',
                    'search'        =>  $leaf.'/#/favourite/#',
                    'delete'        =>  $leaf.'/#/favourite/#/#'
                ],
                'alert'     =>  [
                    'all'           =>  $leaf.'/#/alert',
                    'list'          =>  $leaf.'/#/alert/#',
                    'model'         =>  $leaf.'/#/alert/#/#',
                    'search'        =>  $leaf.'/#/alert/#',
                    'delete'        =>  $leaf.'/#/alert/#/#'
                ]
            ]
        ]
    ],
    'property'  =>  [
        'leaf'      =>  ($leaf='property'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'si'    =>  $leaf.'/#/similar'
            ]
        ]
    ],
    'agency'  =>  [
        'leaf'      =>  ($leaf='agency'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'si'    =>  $leaf.'/#/similar'
            ]
        ]
    ],
    'agent'  =>  [
        'leaf'      =>  ($leaf='agent'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'si'    =>  $leaf.'/#/similar'
            ]
        ]
    ],
    'service'  =>  [
        'leaf'      =>  ($leaf='service'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'si'    =>  $leaf.'/#'
            ]
        ],
        'type'  =>  [
            'leaf'      =>  ($leaf='service/type'),
            'api'       =>  [
                'endpoint'  =>  [
                    'r'     =>  $leaf,
                    's'     =>  $leaf.'/#'
                ]
            ]
        ]
    ],
    'developer'  =>  [
        'leaf'      =>  ($leaf='developer'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'si'    =>  $leaf.'/#/similar'
            ]
        ]
    ],
    'project'  =>  [
        'leaf'      =>  ($leaf='project'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'si'    =>  $leaf.'/#/similar'
            ]
        ]
    ],
    'analytic'  =>  [
        'leaf'      =>  ($leaf='analytic'),
        'api'       =>  [
            'endpoint'  =>  [
                'g'     =>  [
                    'listings'         =>  $leaf.'/#/guideme/listings',
                    'stats'        =>  $leaf.'/#/guideme/stats'
                ]
            ]
        ]
    ],
    'seo'   =>  [
        'leaf'      =>  ($leaf='seo/contents'),
        'api'       =>  [
            'endpoint'  =>  [
                'd'         =>  $leaf.'/details'
            ]
        ]
    ],
    'takeover'  =>  [
        'leaf'      =>  ($leaf='takeover'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf
            ]
        ]
    ],
    'microsite'  =>  [
        'leaf'      =>  ($leaf='microsite'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf,
                's'     =>  $leaf.'/#',
                'd'     =>  $leaf.'/details'
            ]
        ]
    ],
    'publicity-video'  =>  [
        'leaf'      =>  ($leaf='publicity-video'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  $leaf
            ]
        ]
    ],
    'ratings'  =>  [
        'leaf'      =>  ($leaf='ratings'),
        'api'       =>  [
            'endpoint'  =>  [
                'r'     =>  'agent/#/'.$leaf
            ]
        ]
    ]
]

?>
