<?php

return [
    'info@lcdubai.com'  =>  '97142451703',
    'info@clarkeandscott.com'  =>  '97142463979',
    'leads@mdproperties.ae'  =>  '97142451704',
    'marketing@nwmea.com'  =>  '97145128287',
    'otabek@prestigedubai.com'  =>  '97142463969',
    'info@forest.ae'  =>  '97142451706',
    'inquiry@stclair.ae'  =>  '97145128260',
    'mo@mylorealestate.com'  =>  '97142451707',
    'edna.alvarado@engelvoelkers.com'  =>  '97145128243',
    'YBarghouti@emaar.ae'  =>  '97142482503',
    'enquiries@hamptons.ae'  =>  '97142463198',
    'maybelle@harbordubai.com'  =>  '97145128292',
    'listings@selectproperty.ae'  =>  '97145128391',
    'martha@aquaproperties.com'  =>  '97142463201',
    'afaf.burki@select-group.ae'  =>  '97142482504'
]

?>
