var helpers  =   {
    phonenumber: {
        init: function(id, separateDialCode=true){
            helpers.phonenumber.input = document.querySelector("#" + id);

            if(helpers.phonenumber.iti){
                //Destroy if already init
                helpers.phonenumber.iti.destroy();
            }

            helpers.phonenumber.iti = intlTelInput(helpers.phonenumber.input, {
                initialCountry: "ae",
                separateDialCode: separateDialCode
            });
        }
    },
    redirect: function(type){
        search.property.regular.store_variables     =   new search.property.regular.variables();
        search.property.regular.store_variables.rent_buy = [];
        search.property.regular.store_variables.rent_buy.push(type);

        //Check for Property Type from Dropdown
        if($('#property_type').dropdown('get value') && $('#property_type').dropdown('get value').length){
            search.property.regular.store_variables.type = [];
            search.property.regular.store_variables.type.push($('#property_type').dropdown('get value'));
        }

        search.property.regular.__init();
    },
    unshift: function(data){
        var temp_value = '';
        data.bathroom_min = data.bathroom.slice(0);
        data.bathroom_max = data.bathroom.slice(0);
        data.bedroom_min = data.bedroom.slice(0);
        data.bedroom_max = data.bedroom.slice(0);
        data.area_min = data.area.slice(0);
        data.area_max = data.area.slice(0);
        data.price_sale_min = data.price.sale.slice(0);
        data.price_sale_max = data.price.sale.slice(0);
        data.price_rent_min = data.price.rent.slice(0);
        data.price_rent_max = data.price.rent.slice(0);
        temp_value = _.has(lang_static, 'form.placeholders.min.bed') ? _.get(lang_static, 'form.placeholders.min.bed') : 'Min Bed',
        data.bedroom_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.bed') ? _.get(lang_static, 'form.placeholders.max.bed') : 'Max Bed',
        data.bedroom_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.min.bath') ? _.get(lang_static, 'form.placeholders.min.bath') : 'Min Bath',
        data.bathroom_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.bath') ? _.get(lang_static, 'form.placeholders.max.bath') : 'Max Bath',
        data.bathroom_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.min.area') ? _.get(lang_static, 'form.placeholders.min.area') : 'Min Area',
        data.area_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.area') ? _.get(lang_static, 'form.placeholders.max.area') : 'Max Area',
        data.area_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
        data.price_sale_min.unshift({name: temp_value, value: ''});
        data.price_rent_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
        data.price_sale_max.unshift({name: temp_value, value: ''});
        data.price_rent_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.type') ? _.get(lang_static, 'form.placeholders.type') : 'Property Type',
        data.property_type.commercial.rent.unshift({name: temp_value, value: ''});
        data.property_type.commercial.sale.unshift({name: temp_value, value: ''});
        data.property_type.residential.rent.unshift({name: temp_value, value: ''});
        data.property_type.residential.sale.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.furnishing') ? _.get(lang_static, 'form.placeholders.furnishing') : 'Furnishing',
        data.furnishing.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
        data.completion_status.unshift({name: temp_value, value: ''});
        return data;

    },
    filter: {
        property: function(){
            $('#property_type').dropdown('clear');
            $('#property_bedroom_min').dropdown('clear');
            $('#property_bedroom_max').dropdown('clear');
            $('#property_bathroom_min').dropdown('clear');
            $('#property_bathroom_max').dropdown('clear');
            $('#property_area_min').dropdown('clear');
            $('#property_area_max').dropdown('clear');
            $('#property_furnishing').dropdown('clear');
            $('#property_price_min').dropdown('clear');
            $('#property_price_max').dropdown('clear');
            $('#property_suburb').val('');
            $('#property_suburb_hidden').val('');
            $('#completion_status').dropdown('clear');
            $('#keyword').val('');
            $('#property_rent_buy').dropdown('set selected', 'Buy');

            last_segment        =   window.location.href.split( '/' ).pop();
            new_last_segment    =   last_segment.replace( 'featured-', '' );
            newUrl              =   window.location.href.replace( last_segment, new_last_segment);
            newUrl              =   new URI( newUrl );
            newUrl              =   newUrl.query('').toString();

            history.pushState({}, null, newUrl);
            helpers.store.remove('property_search');
            search.property.regular.store_variables     =   new search.property.regular.variables();
        },
        project: function(){
            $('#projects_project').dropdown('clear');
            $('#completion_status').dropdown('clear');
            $('#project_location').val('');
            $('#keyword').val('');

            newUrl              =   window.location.href;
            newUrl              =   new URI( newUrl );
            newUrl              =   newUrl.query('').toString();

            history.pushState({}, null, newUrl);
            helpers.store.remove('project_search');
            search.projects.store_variables     =   new search.projects.variables();
        },
        agent: function(){
            $('#agent_brokerage').dropdown('clear');
            $('#agent_areas').dropdown('clear');
            $('#keyword').val('');

            newUrl              =   window.location.href;
            newUrl              =   new URI( newUrl );
            newUrl              =   newUrl.query('').toString();

            history.pushState({}, null, newUrl);
            helpers.store.remove('agent_search');
            search.agent.store_variables     =   new search.agent.variables();
        }
    },
    home:{
        property: '/' + selectedLanguage,
        project: {
            uae: '/' + selectedLanguage + '/projects/uae',
            international: '/' + selectedLanguage + '/projects/international'
        },
        agent: '/' + selectedLanguage + '/agents-search/uae/all-agency/all-suburbs',
        service: '/' + selectedLanguage + '/services'
    },
    parseNumberCustom : function(number_string) {
        var new_number = parseInt(number_string.toString().replace(/[^0-9\.]/g, ''));
        return new_number;
    },
    lazy_load   :   function(){
        $('.f_p').each(function() {
            if($(this).parents('#property_card_template').length != 1){

                var cdn_img    =   $(this).attr('src');
                var scd_img    =   $(this).attr('data-second');
                //Gallery or single
                var type     =   $(this).attr('data-type');
                helpers.fallback_img(cdn_img, scd_img, type, this);

            }
        });
    },
    fallback_img: function(first_img, second_img, type, trigger){

        helpers.imageExists(first_img, function(exists) {
            if(exists === false) {
                helpers.imageExists(second_img, function(sec_exists) {
                    if(sec_exists === true) {
                        $(trigger).attr("src", second_img);
                    } else {
                        if(type == 'gallery'){
                            $(trigger).parent().remove();
                        }else{
                            $(trigger).attr("src", cdnUrl + 'assets/img/default/no-img-available.jpg');
                        }
                    }
                }, trigger);
            }
        }, trigger);

    },
    imageExists: function(url, callback) {
        var img = new Image();
        img.onload = function() { callback(true); };
        img.onerror = function() { callback(false); };
        img.src = url;
    },
    modal_image_load: function(first_img, second_img, trigger){
        helpers.imageExists(first_img, function(exists) {
            if(exists === false) {
                helpers.imageExists(second_img, function(sec_exists) {
                    if(sec_exists === true) {
                        $(trigger).attr("src", second_img);
                    } else {
                        $(trigger).attr("src", cdnUrl + 'assets/img/default/no-agent-img.png');
                    }
                }, trigger);
            } else {
                $(trigger).attr("src", first_img);
            }
        }, trigger);
    },
    clear: {
        errors: function(){
            $('#register_error, #password_error, #signin_error').html('');
        }
    },
    reset_emi_form      :   function(){

        var form_wrapper    =   $( '#mortgage_calculator_wrapper' );

        $( '#emi_years' ).val( form_wrapper.data( 'init-years' ));
        $( '#emi_down_payment' ).val( form_wrapper.data( 'init-down-payment' ));
        $( '#emi_interest' ).val( form_wrapper.data( 'init-interest' ));

    },
    toast: {
        __init(msg){
            $('#toast').html(msg);
            $('#toast').fadeIn();
            $("#toast").css('visibility', 'visible');
            setTimeout(function(){
                $('#toast').fadeOut();
            }, 3000);
        }
    },
    toast_dashboard: {
        __init(msg){
            $('#toast_dashboard').html(msg);
            $('#toast_dashboard').fadeIn();
            $("#toast_dashboard").css('visibility', 'visible');
            setTimeout(function(){
                $('#toast_dashboard').fadeOut();
            }, 3000);
        }
    },
    share:{
        __init: function(){
            if( !$(".sharethis-inline-share-buttons, .st-btn").is(":visible")) {
                $('.sharethis-inline-share-buttons, .st-btn').show();

            } else {
                $('.sharethis-inline-share-buttons, .st-btn').hide();
            }

        }
    },
    sort: {
        __init: function(){

            $('#listings_sort_btn').dropdown({
                placeholder: 'Sort Results',
                action: function(text, value, element){

                    var sort_type = text.match("--(.*)--"),
                    url = new URI( window.location.href );

                    window.location.href = url.setSearch( 'sort', sort_type[1] ).toString();

                }
            });

        }
    },
    pagination: {
        __init: function(mobile){

            var listings_length  =   Math.ceil( $( '.listings_total' ).attr( 'data-listings_total' ) / 20 );
            listings_length      =  listings_length > 500 ? 500 : listings_length;

            if( listings_length > 1 ){

                var url 		    =     new URI( window.location.href ),
                current_page 	=     url.search(true).page ? url.search(true).page : 1;

                // Pagination trigger
                $('#pagination').twbsPagination({
                    totalPages: listings_length,
                    visiblePages: mobile ? 3 : 8,
                    initiateStartPageClick: false,
                    paginationClass: 'pages',
                    prev: _.has(lang_static, 'pagination.prev') ? _.get(lang_static, 'pagination.prev') : 'Prev',
                    next: _.has(lang_static, 'pagination.next') ? _.get(lang_static, 'pagination.next') : 'Next',
                    startPage: parseInt( current_page ),
                    onPageClick: function ( event, page ) {
                        if( page == 1 ){
                            window.location.href 	=	url.removeSearch( 'page' ).toString();
                        } else {
                            window.location.href    =   url.setSearch( 'page', page ).toString();
                        }
                    }
                });

            } else {
                $('#pagination').hide();
            }

        }
    },
    validate    :   {
        __init      :   function( ){

        },
        email: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email.toLowerCase());
        }
    },
    sanitize    :   {
    },
    format    :   {
    },
    store : {
        read: function(data){
            return localStorage.getItem(data);
        },
        write: function(name, data){
            localStorage.setItem(name, data);
        },
        parse: function(data){
            return JSON.parse(data);
        },
        stringify: function(data){
            return JSON.stringify(data);
        },
        clear: function(){
            localStorage.clear();
        },
        remove: function(item){
            localStorage.removeItem(item);
        },
        temp_data : {
            read: function(){
                return helpers.store.temp_data.write;
            },
            write : ''
        }
    },
    crypto : {
        encrypt: function(data){
            return CryptoJS.AES.encrypt(data, "My Secret Passphrase");
        },
        decrypt: function(data){
            return CryptoJS.AES.decrypt(data, "My Secret Passphrase");
        },
        string: function(data){
            return data.toString(CryptoJS.enc.Utf8);
        }
    },
    populate: {
        modals: function(data){
            var fullname = _.get(data,'name','').split(' ');
            // 1.   Call Back
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings')
            .find('input[name=name]').val(_.get(data,'name',''));
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings')
            .find('input[name=phone]').val(_.get(data,'number',''));
            $('#modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_email, #project_lead, #modal_ideal_property, #property_request_listings')
            .find('input[name=email]').val(_.get(data,'email',''));

            $('#modal_property_mortgage').find('input[name=first_name]').val(fullname[0]);
            $('#modal_property_mortgage').find('input[name=last_name]').val(fullname[1]);

            // 2.   Dashboard
            $('#user_dashboard').find('p.name').html(_.get(data,'name',''));
            $('#user_dashboard').find('input[name=name]').val(_.get(data,'name',''));
            $('#user_dashboard').find('input[name=email]').val(_.get(data,'email',''));
            $('#user_dashboard').find('input[name=number]').val(_.get(data,'number',''));
            $('#user_dashboard').find('input[name=nationality]').val(_.get(data,'nationality',''));

            //3. Property alert
            $('#modal_property_alert').find('input[name=name]').val(_.get(data,'name',''));
            $('#modal_property_alert').find('input[name=email]').val(_.get(data,'email',''));

            //4. Projects callback
            $('#modal_developer_callback_name').find('input[name=name]').val(_.get(data,'name',''));
            $('#modal_developer_callback_phone').find('input[name=phone]').val(_.get(data,'number',''));

        },
        header: function(data){
            $('.dashboard').html(_.get(data.name.split(' '),'0',''));
        }
    },
    depopulate: {
        modals: function(){
            // 1. Lead
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings').find('input[name=name]').val('');
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings').find('input[name=phone]').val('');
            $('#modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_email, #project_lead, #modal_ideal_property, #property_request_listings').find('input[name=email]').val('');

            // 2.   Dashboard
            $('#user_dashboard').find('p.name').html('');
            $('#user_dashboard').find('input[name=name]').val('');
            $('#user_dashboard').find('input[name=email]').val('');
            $('#user_dashboard').find('input[name=number]').val('');
            $('#user_dashboard').find('input[name=nationality]').val('');

            //3. Property alert
            $('#modal_property_alert').find('input[name=name]').val('');
            $('#modal_property_alert').find('input[name=email]').val('');

            //4. Projects callback
            $('#modal_developer_callback_name').find('input[name=name]').val('');
            $('#modal_developer_callback_phone').find('input[name=email]').val('');

            //5. Clear Auth Forms
            $("#modal_signin").trigger('reset');
            $("#modal_register").trigger('reset');
            $("#modal_forgot_password").trigger('reset');
        },
        header: function(){

        }
    },
    user: {
        state: {
            is_logged_in: function(){
                if(helpers.store.read('user')){
                    try{
                        return [true, helpers.store.parse(helpers.crypto.string(helpers.crypto.decrypt(helpers.store.read('user'))))];
                    }
                    catch(err){
                        return [false, []];
                    }
                }
                return [false, []];
            }
        },
        loggedin : {
            __init: function(name){

                /**
                * #1. Read localstorage
                * #2. decrypto data
                * #3. toString data
                * #4. parse data
                * #5. check login status
                */

                if(helpers.store.read(name)){

                    //#1
                    var read = helpers.store.read(name);

                    //#2
                    var decrypt = helpers.crypto.decrypt(read);

                    //#3
                    var string = helpers.crypto.string(decrypt);

                    //#4
                    var user = helpers.store.parse(string);

                    //#5
                    return user.login;

                } else {

                    return false;

                }

            }
        }
    },
    trigger: {
        modal: {
            __init: function(type, transition){
                $('.ui.modal.' + type).modal('setting', 'transition', transition ? transition : 'fade up').modal('show');

                if(type === 'signin_register'){
                    var input = document.getElementById("signin_input_password");

                    input.addEventListener("keyup", function(event) {
                        event.preventDefault();
                        if (event.keyCode === 13) {
                            document.getElementById("signin_submit_btn").click();
                        }
                    });
                }
            }
        }
    },
    events: {
        bind : {
            mouseenter : function(class_name){
                $(class_name).mouseenter(function(){
                });
            }
        },
        unbind : {
            mouseenter : function(class_name){
                $(class_name).unbind('mouseenter mouseleave');
            }
        }
    },
    clean   :   {
        junk    :   function(el){
            return _.isObject(el) ? helpers.clean.internal(el) : el;
        },
        internal    :   function(el){
            return _.transform(el, function(result, value, key) {
                var isCollection = _.isObject(value);
                var cleaned = isCollection ? helpers.clean.internal(value) : value;
                if ( !_.isBoolean(cleaned) && !_.isNumber(cleaned) && (_.isUndefined(cleaned) || _.isEmpty(cleaned)) ) {
                    return;
                }
                _.isArray(result) ? result.push(cleaned) : (result[key] = cleaned);
            });
        }
    },
    indexof: function(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] == value) {
                return i;
            }
        }
        return null;
    },
    bind    :   function(data,binds,template_o,sink){

        var limiter = '%';
        try{
            // Clean the sink
            $('#'+sink).html('');
            // Fill the sink
            for(var j=0; j<data.length; j++){

                if(_.get(data,j+'.property.agent.agency.settings.lead.call_tracking.track'))
                _.set(data, j+'.property.agent.contact.phone', _.get(data,j+'.property.agent.agency.settings.lead.call_tracking.phone'));
                else if (_.get(data,j+'.property.agent.contact.phone', '') == '') {
                    _.set(data,j+'.property.agent.contact.phone', _.get(data,j+'.property.agent.agency.contact.phone'));
                }
                var template = template_o,
                bind_data = [];
                bind_data[j] = _.get(data,j+'.property');

                for(var b=0; b<binds.length; b++){

                    // Hook
                    var hook = limiter+_.get(binds,b,'')+limiter;

                    if(/\d/.test(_.get(binds,b))){
                        var bbind = eval(_.get(binds,b));
                    }
                    else{
                        var bbind = _.get(binds,b);
                    }

                    // Bind
                    var bind   =   _.get(_.get(data,j),bbind);

                    var residential_commercial = _.get(_.get(data,j),'property.residential_commercial');
                    var bedroom = _.get(_.get(data,j),'property.bedroom');
                    var bathroom = _.get(_.get(data,j),'property.bathroom');

                    if(_.get(binds,b,'') == 'property.web_link'){
                        var bind = baseUrl  +_.get(_.get(data,j),'property.rent_buy').replace(/[\W_]/g, '-').toLowerCase()+'/'
                        +_.get(_.get(data,j),'property.residential_commercial').toLowerCase()+'/'
                        +_.get(_.get(data,j),'property.writeup.title').replace(/[\W_]/g, "-").replace(/-{2,}/g, '-').toLowerCase()+'/'
                        +_.get(_.get(data,j),'property._id');
                    }
                    if(_.get(binds,b,'') == 'agency.logo'){
                        var agency_logo     =   _.get(_.get(data,j),'property.agent.agency.contact.email').split('@')[1];
                        var bind            =   cdnUrl + 'agency/logo/' + agency_logo;
                    }
                    if(_.get(binds,b,'') == 'property.price'){
                        var bind  =   (_.get(_.get(data,j),'property.price')).toLocaleString('en');
                    }
                    if(_.get(binds,b,'') == 'property.building'){
                        var building = _.get(_.get(data,j),'property.building');
                        if(!_.isEmpty(building)){
                            var bind  =  _.has(lang, 'building.'+_.snakeCase(building)) ? _.get(lang, 'building.'+_.snakeCase(building)) + ',' : building + ',';
                        }
                    }
                    if(_.get(binds,b,'') == 'property.area'){
                        var area = _.get(_.get(data,j),'property.area');
                        var bind  =  _.has(lang, 'area.'+_.snakeCase(area)) ? _.get(lang, 'area.'+_.snakeCase(area)) : area;
                    }
                    if(_.get(binds,b,'') == 'property.city'){
                        var city = _.get(_.get(data,j),'property.city');
                        var bind  =  _.has(lang, 'city.'+_.snakeCase(city)) ? _.get(lang, 'city.'+_.snakeCase(city)) : city;
                    }
                    if(_.get(binds,b,'') == 'property.type'){
                        var type = _.get(_.get(data,j),'property.type');
                        var bind  =  _.has(lang, 'type.'+_.snakeCase(type)) ? _.get(lang, 'type.'+_.snakeCase(type)) : type;
                    }
                    if(_.get(binds,b,'') == 'property.rent_buy'){
                        var rent_buy = _.get(_.get(data,j),'property.rent_buy');
                        var bind  =  _.has(lang, 'rent_buy.'+ _.snakeCase('for ' + rent_buy)) ? _.get(lang, 'rent_buy.'+ _.snakeCase('for_' + rent_buy)) : rent_buy;
                    }
                    if(_.get(binds,b,'') == 'property.bedroom'){
                        var bed = {
                            'bed': _.get(_.get(data,j),'property.bedroom')
                        }
                        var trans = helpers.trans_choice( _.get(lang_static, 'global.bed'),_.get(_.get(data,j),'property.bedroom') , bed );
                        var bind  =  '<i class="bed icon"></i> ' + (_.has(lang_static, 'global.bed') ? _.toLower(trans) : _.get(_.get(data,j),'property.bedroom'));
                        bind = (residential_commercial !== "Commercial" || (bedroom !== 0 && bedroom !== -1)) ? bind : '';
                    }
                    if(_.get(binds,b,'') == 'property.bathroom'){
                        var bath = {
                            'bath': _.get(_.get(data,j),'property.bathroom')
                        }
                        var trans = helpers.trans_choice( _.get(lang_static, 'global.bath'),_.get(_.get(data,j),'property.bathroom') , bath );
                        var bind  =  '<i class="bath icon"></i> ' + (_.has(lang_static, 'global.bath') ? _.toLower(trans) : _.get(_.get(data,j),'property.bathroom'));
                        bind = bathroom !== 0 ? bind : '';
                    }
                    if(_.get(binds,b,'') == 'property.dimension.builtup_area'){
                        var sqft  = _.has(lang_static, 'global.sqft') ? _.get(lang_static, 'global.sqft') : 'sqft';
                        var bind  =  (_.get(_.get(data,j),'property.dimension.builtup_area') == 0) ? '' : _.get(_.get(data,j),'property.dimension.builtup_area') +' '+ sqft;
                        if(_.get(_.get(data,j),'property.dimension.builtup_area') == 0){
                            $("#sq_similar_").hide();
                        }
                        else{
                            $("#sq_similar_"+_.get(_.get(data,j),'property.dimension.builtup_area')).show();
                        }
                    }

                    var regex = new RegExp( limiter+_.get(binds,b)+':(.*?)'+limiter );

                    if(template.match(regex)){
                        var inner_regex = new RegExp( limiter+_.get(binds,b)+':0:1'+limiter );
                        if(template.match(inner_regex)){
                            var hook    =   limiter+_.get(binds,b,'')+':0:1'+limiter;
                            var bind    =  _.get(_.get(_.get(data,j),_.get(binds,b)), 0);
                        }
                    }

                    template = template.split( hook ).join( bind );
                }
                $('#'+sink).append(template);
                //Binding all click actions
                let current = _.get(bind_data,j);
                $("#t_call_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.call.__init(current);
                });
                $("#t_callback_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.callback.__init(current);
                });
                $("#t_email_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.email.__init(current);
                });
                $("#t_chat_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.whatsapp.__init(current);
                });


            }
        }
        catch(err){
        }

    },
    read_more_less : function(className, height, idname){

        var elem = $("." + className).text();
        if (elem == "more..." || elem == _.get(lang_static, 'global.more')) {
            var text  =  _.has(lang_static, 'global.less') ? _.get(lang_static, 'global.less') : "less...";
            $("." + className).text(text);
            $('#' + idname).css('height', 'auto');
            $('#' + idname + ' ul li').css('display','inline-block');
        } else {
            var text  =  _.has(lang_static, 'global.more') ? _.get(lang_static, 'global.more') : "more...";
            $("." + className).text(text);
            $('#' + idname).css('height', height );
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
        }

    },
    read_more_less_new : function(className, height, idname){
        var elem = $("." + className).text();
        if (elem == _.get(lang_static, 'global.more') || elem == _.get(lang_static, 'project.details.read_more') ) {
            var text  =  _.has(lang_static, 'project.details.read_less') ? _.get(lang_static, 'project.details.read_less') : "Read Less";
            $("." + className).text(text);
            $('#' + idname).css('height', 'auto');
            $('#' + idname).addClass('descriptxt-collapsed');
            $('#' + idname + ' ul li').css('display','inline-block');
            $("." + className).parent().css('padding-top', '1rem');
        } else {
            var text  =  _.has(lang_static, 'project.details.read_more') ? _.get(lang_static, 'project.details.read_more') : "Read More";
            $("." + className).text(text);
            $('#' + idname).css('height', height );
            $('#' + idname).removeClass('descriptxt-collapsed');
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
            $("." + className).parent().css('padding-top', '1rem');
        }

    },
    view_more_less : function(className, height, idname, idIcon='', iconCurrentClass='', iconExpectedClass='', extraBehavior=false, label_more = 'property.details.view_more', label_less = 'property.details.view_less'){
        var elem = $.trim($("." + className).text());
        if (elem == $.trim(_.get(lang_static, label_more))) {
            var text  =  _.has(lang_static, label_less) ? _.get(lang_static, label_less) : "View less";
            $("." + className).text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconCurrentClass).addClass(iconExpectedClass);
            }
            $('#' + idname).css('height', 'auto');
            $('#' + idname).addClass(idname+'-collapsed');
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', 'auto' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
        } else {
            var text  =  _.has(lang_static, label_more) ? _.get(lang_static, label_more) : "View more";
            $("." + className).text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconExpectedClass).addClass(iconCurrentClass);
            }
            $('#' + idname).css('height', height );
            $('#' + idname).removeClass(idname+'-collapsed');
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', '9rem' );
                $("." + className).parent().parent().css('top', '-76px' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
        }

    },
    view_more_amenities : function(className, height, idname, idIcon='', iconCurrentClass='', iconExpectedClass='', extraBehavior=false, label_more = 'property.details.view_more', label_less = 'property.details.view_less'){
        var elem = $.trim($("." + className + " span").text());

        if (elem == $.trim(_.get(lang_static, label_more))) {
            var text  =  _.has(lang_static, label_less) ? _.get(lang_static, label_less) : "View less";
            $("." + className + " span").text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconCurrentClass).addClass(iconExpectedClass);
            }
            $('#' + idname).css('height', 'auto');
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', 'auto' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
        } else {
            var text  =  _.has(lang_static, label_more) ? _.get(lang_static, label_more) : "View more";
            $("." + className + " span").text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconExpectedClass).addClass(iconCurrentClass);
            }
            $('#' + idname).css('height', height );
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', '9rem' );
                $("." + className).parent().css('top', '-76px' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
        }

    },
    trans_choice: function(key, count = 1, replace = {}){

        let translations = key.split('|');
        let translation  = '';
        var BreakException = {};
        try {
            translations.forEach((trans) => {
                if(_.startsWith(trans, '{'+count+'}')){
                    translation = _.trim(_.replace(_.replace(trans, /\[(.+?)\]/g, ''),/\{(.+?)\}/g,'' ));
                    throw BreakException;
                }
                else if((_.includes(trans,'[') && _.includes(trans, ']'))){

                    var range = trans.substring(trans.lastIndexOf("[") + 1,trans.lastIndexOf("]"));
                    var nums = range.split(',');
                    if(((count >= _.get(nums,0)) && (_.get(nums,1) == '*')) || (count <= _.get(nums,1))){
                        translation = _.trim(_.replace(_.replace(trans, /\[(.+?)\]/g, ''),/\{(.+?)\}/g,'' ));
                    }
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e !== BreakException) throw e;
        }
        for (var placeholder in replace) {
            translation = translation.replace(`:${placeholder}`, replace[placeholder]);
        }
        return translation;
    },
    trans_value: function(data, replace = {}){
        let translation  = '';
        var BreakException = {};
        try {
            for (var var_trans in replace) {
                translation =  _.replace(data, ':'+var_trans, replace[var_trans]);
            }
            return translation;
        } catch (e) {
            if (e !== BreakException) throw e;
        }
    },
    navToggle: function(e){
        let res_comm    =   $(e).attr("data-value");
        $(e).parent().children().removeClass('purple');
        $(e).addClass('purple');
        let rent_buy_menu = $(e).parent().parent().parent();
        rent_buy_menu.find('.row').removeClass('show');
        rent_buy_menu.find('.row').addClass('hide');
        rent_buy_menu.find('.row.'+res_comm).removeClass('hide');
        rent_buy_menu.find('.row.'+res_comm).addClass('show');
    }
}


//ON CHANGE EVENTS

$("#emi_years").change(function(){

    if($(this).val() > 25){
        $(this).val(25);
    } else if($(this).val() < 1){
        $(this).val(1);
    }

});


$("#emi_interest").change(function(){

    if($(this).val() > 5){
        $(this).val(5);
    } else if($(this).val() < 1){
        $(this).val(1);
    }

});

var lead = {
    property: {
        store: {
            save : '',
            retrive: function(){
                return lead.property.store.save;
            }
        },
        details: {
            email: {
                __init: function(trigger){

                    //Add loading to the Button
                    $(trigger).addClass('loading');

                    var number = helpers.phonenumber.iti.getNumber();

                    var payload = {
                        action: 'email',
                        source: 'consumer',
                        subsource: 'desktop',
                        message: $('#property_details_email_textarea').val(),
                        property:{
                            _id: $(trigger).attr("data-_id")
                        },
                        user: {
                            contact: {
                                name: $('#property_details_email_name').val(),
                                email: $('#property_details_email_email').val(),
                                phone: number
                            }
                        }
                    }

                    var url = baseApiUrl + 'lead/property';

                    axios.post(url, payload).then(function (response) {
                        //Add loading to the Button
                        $(trigger).removeClass('loading');
                        $('.ui.modal.property_email').modal('hide');
                        helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                        other.datalayer('property','email_submit', payload);
                    }).catch(function (error) {
                        //Add loading to the Button
                        $(trigger).removeClass('loading');
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    });

                }
            }
        },
        call : {
            __init: function(trigger, mobile){

                payload = lead.property.store.retrive();

                var url = baseApiUrl + 'lead/property';

                axios.post(url, payload).then(function (response) {

                    if(mobile){
                        //gtm push on submit
                        var property_data = helpers.store.temp_data.read();
                        other.datalayer('porperty','call_submit', property_data);
                    }

                }).catch(function (error) {});

                if(mobile){
                    window.location.href = 'tel:'+$(trigger).html();
                }

            }
        },
        whatsapp : {
            __init: function(payload){

                var url = baseApiUrl + 'lead/property';

                axios.post(url, payload).then(function (response) {
                    var property_data = helpers.store.temp_data.read();
                    other.datalayer('property','whatsapp_submit', property_data);
                }).catch(function (error) {});

            }
        },
        email: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: $('#modal_property_email_textarea').val(),
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_property_email_name').val(),
                            email: $('#modal_property_email_email').val(),
                            phone: number
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/property';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_email').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();

                    other.datalayer('property','email_submit', property_data);

                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        callback: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber(),
                    user = JSON.parse(helpers.store.read('user_o'));

                var payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.property.callback.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.callback.payload_msg'), {reference: $(trigger).attr("data-ref_no")}) : 'Hi, I found your property with ref: ' + $(trigger).attr("data-ref_no") + ' on Zoom Property. Please contact me. Thank you.',
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_property_callback_name').val(),
                            email: _.has(user, 'email') ? user.email : 'anonymous@domain.com',
                            phone: number
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/property';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_callback').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();

                    other.datalayer('property','callback_submit', property_data);

                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        call_request_back: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber(),
                    user_o = JSON.parse(helpers.store.read('user_o'));

                let payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.property.callback.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.callback.payload_msg'), {reference: $(trigger).attr("data-ref_no")}) : 'Hi, I found your property with ref: ' + $(trigger).attr("data-ref_no") + ' on Zoom Property. Please contact me. Thank you.',
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_property_call_name').val(),
                            email: _.has(user_o, 'email') ? user_o.email : $('#modal_property_call_email').val(),
                            phone: number
                        }
                    }
                }

                //Create alert if send me latest property information and news is active
                if($('#modal_property_call_latest_info').is(':checked')){
                  user.newsletter.subscribe(payload.user.contact.email);
                }

                //1.
                var url = baseApiUrl + 'lead/property';
                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_callback').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();

                    other.datalayer('property','callback_submit', property_data);

                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        mortgage: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var work_number = parseInt($('#mortgage_user_work_phone').val());

                var number = helpers.phonenumber.iti.getNumber();

                var work_phone = work_number;


                var payload = {
                    emi:{
                        loan_amount: helpers.parseNumberCustom($( '#project_loan_amount' ).text()),
                        monthly: helpers.parseNumberCustom($( '#project_emi_monthly' ).text()),
                        down_payment: helpers.parseNumberCustom($( '#project_emi_down_payment' ).text()),
                        total_amount: helpers.parseNumberCustom($( '#project_emi_total_amount' ).text()),
                        total_interest: helpers.parseNumberCustom($( '#project_emi_total_interest' ).text()),
                        interest_rate: parseFloat($( '#emi_interest' ).val()),
                        term: helpers.parseNumberCustom($( '#project_emi_term' ).text())
                    },
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            first_name: $('#mortgage_user_name').val(),
                            last_name: $('#mortgage_user_name').val(),
                            email: $('#mortgage_user_email').val(),
                            mobile_number: number,
                            work_number: work_phone
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/property/mortgage';

                //2.
                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();
                    other.datalayer('mortgage_submit', property_data);
                    $('.ui.modal.property_mortgage').modal('hide');
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_mortgage').modal('hide');
                });

            }
        },
        request: {
            __init: function(trigger){

                var payload = {
                    user: {
                        contact: {
                            name: $('#property_request_name').val(),
                            email: $('#property_request_email').val(),
                            phone: $('#property_request_phone').val(),
                        }
                    },
                    message: $('#property_request_message').val(),
                    url: $(trigger).attr('data-url'),
                    title: $(trigger).attr('data-title')

                }
                var url = baseApiUrl + 'lead/property/listings';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $("#property_request_listings").trigger('reset');

                    //gtm push on submit
                    // var property_data = helpers.store.temp_data.read();
                    // dataLayer.push({ event: 'track_property_mortgage_submit', data: property_data });
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                });

            }
        },
        alert: {
            __init: function(trigger){

                $(trigger).addClass('loading');

                var payload = {
                    data: {
                        area: lead.property.alert.store_variables.area.toString(),
                        rent_buy: lead.property.alert.store_variables.rent_buy.toString(),
                        price: {
                            min: lead.property.alert.store_variables.price.min,
                            max: lead.property.alert.store_variables.price.max
                        }
                    },
                    consumer: {
                        name : $('#property_alert_name').val(),
                        email: $('#property_alert_email').val()
                    },
                    frequency: lead.property.alert.store_variables.frequency.toString()
                }
                var user = JSON.parse(helpers.store.read('user_o'));
                var url = baseApiUrl + 'user/' + user.id + '/alert/property';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_alert_created') );
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_alert').modal('hide');
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_alert').modal('hide');
                });


            },
            variables   :   function(){
                return {
                    area		       	:   [],
                    rent_buy	        :	[],
                    frequency           :   [],
                    price				:		{
                        min			    :		undefined,
                        max			    :		undefined,
                    }
                }
            }
        }
    },
    agent: {
        store: {
            save : '',
            retrive: function(){
                return lead.agent.store.save;
            }
        },
        call : {
            __init: function(trigger){

                payload = lead.agent.store.retrive();

                /**
                * 1.   build URL
                * 2.   Api call with payload and URL
                * 3.   Show toast on success or Error
                */

                var agent_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/agent';

                //2.
                axios.post(url, payload).then(function (response) {}).catch(function (error) {});

                if(trigger){
                    other.datalayer('agent','call_submit',agent_data);
                    window.location.href = 'tel:'+$(trigger).html();
                }

            }
        },
        callback: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.agent.call_back.payload_msg') ? _.get(lang_static, 'modals.agent.call_back.payload_msg') : 'Hi, I found your profile on Zoom Property. Please contact me. Thank you.',
                    agent:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            name: $('#modal_agent_callback_name').val(),
                            phone: number
                        }
                    }
                }

                var agent_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/agent';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.agent_callback').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    other.datalayer('agent','callback_submit', agent_data);
                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        email: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: $('#modal_agent_email_textarea').val(),
                    agent:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_agent_email_name').val(),
                            email: $('#modal_agent_email_email').val(),
                            phone: number
                        }
                    }
                }

                var agent_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/agent';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.agent_email').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    other.datalayer('agent', 'email_submit', agent_data);
                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        whatsapp : {
            __init: function(payload){

                var url = baseApiUrl + 'lead/agent';

                axios.post(url, payload).then(function (response) {
                    other.datalayer('agent', 'whatsapp_click', payload);
                }).catch(function (error) {});

            }
        }
    },
    project: {
        call : {
            __init: function(trigger){

                var user    =   JSON.parse(helpers.store.read('user_o'));

                $data_temp = helpers.store.temp_data.read();

                if(!_.isEmpty($data_temp) && !_.isUndefined($data_temp.developer))
                    data_trigger = $data_temp;

                var url     =   baseApiUrl + 'lead/project';

                var name = !_.isEmpty($(trigger).attr("data-project_name")) ? $(trigger).attr("data-project_name") : (!_.isEmpty(data_trigger.name) ? data_trigger.name : '');

                var payload = {
                    action: 'call',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : (!_.isEmpty(data_trigger.subsource) ? data_trigger.subsource : 'desktop'),
                    message: _.has(lang_static, 'modals.project.call.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.project.call.payload_msg'), {name: name}) : 'Hi, I found your Project ' + name + ' on zoom property. Please contact me. Thank you.',
                    project: {
                        _id: !_.isEmpty($(trigger).attr("data-project_id")) ? $(trigger).attr('data-project_id') : data_trigger._id
                    },
                    user: {
                        contact: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        }
                    }
                }

                if (payload.user.contact.phone.length === 0) {
                    delete payload.user.contact.phone
                }

                helpers.store.temp_data.write = payload;

                axios.post(url, payload).then(function (response) {}).catch(function (error) {});

                //gtm push on submit
                var project_data = helpers.store.temp_data.read();

                if(trigger){
                    other.datalayer('project','call_submit', project_data);
                    window.location.href = 'tel:'+$(trigger).html();
                }
            }
        },
        email: {
            __init: function(data_trigger){
                //Add loading to the Button
                $('#project_lead').addClass('loading');
                $data_temp = helpers.store.temp_data.read();
                if(_.isUndefined(data_trigger.developer) && !_.isEmpty($data_temp) && !_.isUndefined($data_temp.developer))
                    data_trigger = $data_temp;
                var number = helpers.phonenumber.iti.getNumber();

                var name = !_.isEmpty($('project_lead_name').attr("data-project_name")) ? $('project_lead_name').attr("data-project_name") : (!_.isEmpty(data_trigger.name) ? data_trigger.name : '');

                var msg = _.has(lang_static, 'modals.project.email.payload_msg') ? ' ' + helpers.trans_value(_.get(lang_static, 'modals.project.email.payload_msg'), {name: name}) : '';
                msg = !_.isUndefined($('#project_lead_textarea')) && !_.isEmpty($('#project_lead_textarea').val()) ? $('#project_lead_textarea').val() : (!_.isUndefined($('#modal_developer_email_textarea')) ? $('#modal_developer_email_textarea').val() : msg);

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty(data_trigger.subsource) ? data_trigger.subsource : 'desktop',
                    message: msg,
                    project:{
                        _id: data_trigger._id
                    },
                    user: {
                        contact: {
                            name: !_.isUndefined($('#project_lead_name').val()) && !_.isEmpty($('#project_lead_name').val()) ? $('#project_lead_name').val() : $('#modal_developer_email_name').val(),
                            email: !_.isUndefined($('#project_lead_email').val())&& !_.isEmpty($('#project_lead_email').val()) ? $('#project_lead_email').val() : $('#modal_developer_email_email').val(),
                            phone: number
                        }
                    }
                }
                if(data_trigger.settings.custom_form){
                    $('.ui.modal.custom_enquiry').modal({
                        onHide: function(){
                            $('#project_lead').removeClass('loading');
                        }
                    }).modal('show');
                    data_trigger.payload = payload;
                    helpers.store.temp_data.write = data_trigger;
                }
                else{
                    helpers.store.temp_data.write = data_trigger;

                    //1.
                    var url = baseApiUrl + 'lead/project';
                    //2.
                    axios.post(url, payload).then(function (response) {
                        helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                        //Remove loading to the Button
                        $('#project_lead').removeClass('loading');
                        var project_data = helpers.store.temp_data.read();
                        other.datalayer('project','email_submit', data_trigger);
                    }).catch(function (error) {
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                        //Remove loading to the Button
                        $(trigger).removeClass('loading');
                    });
                }

            }
        },
        callback: {
            __init: function(trigger){
                //Add loading to the Button
                $('#project_lead').addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.project.callback.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.project.callback.payload_msg'), {name: $('#pages_projects_details').attr('data-project_name')}) : 'Hi, I found your Project ' + $('#pages_projects_details').attr('data-project_name') + ' on zoom property. Please contact me. Thank you.',
                    project:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            name: $('#modal_developer_callback_name').val(),
                            phone: number
                        }
                    }
                }
                helpers.store.temp_data.write = payload;

                var url = baseApiUrl + 'lead/project';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    $('#project_lead').removeClass('loading');
                    $('.ui.modal.developer_callback').modal('setting', 'transition', 'fade up').modal('hide');

                    //gtm push on submit
                    var project_data = helpers.store.temp_data.read();

                    other.datalayer('project','callback_submit', project_data);

                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');
                    $('.ui.modal.developer_callback').modal('setting', 'transition', 'fade up').modal('hide');
                });

            }
        },
        whatsapp : {
            __init: function(payload){

                var url = baseApiUrl + 'lead/project';

                axios.post(url, payload).then(function (response) {
                    other.datalayer('project', 'whatsapp_click', payload);
                }).catch(function (error) {});

            }
        },
        mortgage: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var work_number = parseInt($('#mortgage_user_work_phone').val());

                var country = $('#modal_mortgage_email_country').attr('data-country-code');

                var work_phone = country + work_number;


                var payload = {
                    emi:{
                        loan_amount: helpers.parseNumberCustom($( '#project_loan_amount' ).text()),
                        monthly: helpers.parseNumberCustom($( '#project_emi_monthly' ).text()),
                        down_payment: helpers.parseNumberCustom($( '#project_emi_down_payment' ).text()),
                        total_amount: helpers.parseNumberCustom($( '#project_emi_total_amount' ).text()),
                        total_interest: helpers.parseNumberCustom($( '#project_emi_total_interest' ).text()),
                        interest_rate: parseFloat($( '#emi_interest' ).val()),
                        term: helpers.parseNumberCustom($( '#project_emi_term' ).text())
                    },
                    project:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            first_name: $('#mortgage_user_name').val(),
                            last_name: $('#mortgage_user_name').val(),
                            email: $('#mortgage_user_email').val(),
                            mobile_number: number,
                            work_number: work_phone
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/project/mortgage';

                //2.
                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');

                    //gtm push on submit
                    var project_data = helpers.store.temp_data.read();
                    other.datalayer('mortgage_submit', project_data);
                    $('.ui.modal.property_mortgage').modal('hide');
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_mortgage').modal('hide');
                });

            }
        },
        request: {
            __init: function(trigger){
                var payload = {
                    user: {
                        contact: {
                            name: $('#modal_developer_enquiry_name').val(),
                            email: $('#modal_developer_enquiry_email').val(),
                            phone: $('#modal_developer_enquiry_phone').val(),
                        }
                    },
                    message: $('#modal_developer_enquiry_message').val(),
                    url: $(trigger).attr('data-url'),
                    title: $(trigger).attr('data-title')

                }
                var url = baseApiUrl + 'lead/project/listings';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $("#modal_developer_enquiry").trigger('reset');
                    dataLayer.push({ event: 'track_email_submit'});
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');
                });

            }
        }
    },
    services: {
        email: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: $('#modal_services_contact_user_message').val(),
                    service: {
                        _id: $(trigger).attr("data-service_id")
                    },
                    user: {
                        contact: {
                            name: $('#modal_services_contact_user_name').val(),
                            email: $('#modal_services_contact_user_email').val(),
                            phone: number
                        }
                    }
                }

                var service_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/service';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.services_contact').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    other.datalayer('service','contact_submit', service_data);
                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        }
    },
    mortgage: {
        __init: function(trigger){
            if($(trigger).attr('data-model') == 'property'){
                return lead.property.mortgage.__init(trigger);
            } else {
                return lead.project.mortgage.__init(trigger);
            }
        }
    },
    preferences: {
        __init: function(trigger){

            //#1.- Add loading to the Button
            $(trigger).addClass('loading');
            //#2.- Getting phone number
            var number = helpers.phonenumber.iti.getNumber();
            //#3.- Building payload
            var locations   = $('#ge_location').val();
            var rent_buy_val = _.isUndefined($('#ge_rent_buy')) ? '' : $('#ge_rent_buy').val();
            //#3.1.- Building location payload
            locations  =   _.isUndefined(locations) ? [] : locations.split('-');
            var building = null, area = null, city = null, residential_commercial = null, rent_buy = null;
            if(locations.length === 1){
                city = _.get(locations,0);
            }
            else if(locations.length === 2){
                city = _.get(locations,1);
                area = _.get(locations,0);
            }
            else if(locations.length === 3){
                city     = _.get(locations,2);
                area     = _.get(locations,1);
                building = _.get(locations,0);
            }
            //#3.2.- Building Residential/Commercial rent_buy type payload
            if(!_.includes( rent_buy_val, '-' ) || (_.startsWith( _.lowerCase(rent_buy_val), 'short') ) ){
                residential_commercial = 'Residential';
                rent_buy = _.replace(rent_buy_val, /-/g, ' ');
            }
            else{
                residential_commercial = 'Commercial';
                rent_buy = rent_buy_val.split(/-(.+)/);
                rent_buy = _.replace(rent_buy[1], /-/g,' ');
            }
            var payload = {
                action: 'email',
                source: 'consumer',
                subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                message: _.has(lang_static, 'modals.preference.contact.message') ? helpers.trans_value(_.get(lang_static, 'modals.preference.contact.message'), {location: _.join(locations, ',')}) : 'Hi, I am looking for a Property in ' + _.join(locations, ',') + '. Please contact me. Thank you.',
                user: {
                    contact : {
                        name: $('#ge_name').val(),
                        email: $('#ge_email').val(),
                        phone: number
                    }
                },
                preferences: {
                    bedroom: {
                        min: 0,
                        max: 0
                    },
                    bathroom: {
                        min: 0,
                        max: 0
                    },
                    price: {
                        min: 0,
                        max: 0
                    },
                    city: city,
                    type: $('#ge_type').val(),
                    rent_buy: rent_buy,
                    residential_commercial: residential_commercial,
                    area: _.isNull(area) ? area : [area],
                    building: _.isNull(building) ? building : [building]
                },
                property: {
                    city: city,
                    type: $('#ge_type').val(),
                    rent_buy: rent_buy,
                    residential_commercial: residential_commercial,
                    area: area,
                    building: building
                }
            }

            //4.- API call
            var url = baseApiUrl + 'lead/property';
            axios.post(url, payload).then(function (response) {

                //Remove loading to the Button
                $(trigger).removeClass('loading');

                helpers.toast.__init('Your Request has been sent! someone will get in touch soon.');
                // other.general_enquiry.slide_context();

                //gtm push on submit
                var property_data = payload;

                other.datalayer('property','email_submit', property_data);

            }).catch(function (error) {
                //Remove loading to the Button
                $(trigger).removeClass('loading');
                helpers.toast.__init('Something went wrong, Please try later');
            });
        }
    }
}

lead.property.alert.store_variables     =   new lead.property.alert.variables();

/**
 *
 *      Google Maps related f(x)s
 *
 */

var g_map   =   {
    center              :   {
		lat				   : 	25.2048,
		lng				   : 	55.2708
	},
    map_container       :   '',
    results_container   :   '',
    allMarkers		    :	[],
    allResults		    :	[],
    mapTypesList	    :	[ 'mosque', 'hospital', 'school', 'gym', 'bank', 'shopping_mall', 'parking', 'park', 'gas_station' ],
    selectedIcon        :   envUrl + 'assets/img/map/property-marker.png',
    propertyIcon        :   envUrl + 'assets/img/map/property-marker.png',
    map 		        : 	{
        map                 :   null,
        invoke              :   function( container = g_map.map_container ){
            g_map.map.map   =   new google.maps.Map( document.getElementById( container ), {
                center				  : 		g_map.center,
                scrollwheel			  : 		false,
                zoom				  : 		(container.indexOf( 'results-map' ) > -1 )? 14 : 15
            });
            return g_map.map.map;
        },
        change_center       :   function( lat, lng ){
            g_map.map.map.setCenter( new google.maps.LatLng( lat, lng ) )
        }
    },
    //Property List Card
    property_map_list		:	{
		invoke					:		function(){
            let mapListCard			=		document.getElementsByClassName( 'property_card_list_map' );
			for (let i = 0; i < mapListCard.length; i++) {
                mapListCard[i].addEventListener( 'mouseover', function() {
                    var lat = $(this).data( 'lat' ),
                        lng = $(this).data( 'lng' );
                    if( lat === 0 && lng === 0 ){
                        $( '#results-no-map' ).css( 'visibility', 'visible' );
                    }
                    else {
                        // Populate content of infowindow wrapper with div data
                        let propertyCard    	=  		this.querySelector( '.property_list' ),
                            propertyMarker;
                        if ( typeof propertyCard.id !== 'undefined' ){
                            $( '#infowindow-property-name' ).html( propertyCard.getAttribute( 'data-name' ) );
                            $( '#infowindow-property-location' ).html( propertyCard.getAttribute( 'data-location' ) );
                            $( '#infowindow-property-price' ).html( propertyCard.getAttribute( 'data-price' ) );
                            $( '#infowindow-property-bed' ).html( propertyCard.getAttribute( 'data-bedroom' ) );
                            $( '#infowindow-property-bath' ).html( propertyCard.getAttribute( 'data-bathroom' ) );
                            $( '#infowindow-property-price' ).html( propertyCard.getAttribute( 'data-price' ) );
                            $( '#infowindow-property-area' ).html( propertyCard.getAttribute( 'data-area' ) );
                            $( '#infowindow-property-agent-name' ).html( propertyCard.getAttribute( 'data-agent' ) );
                            $( '#infowindow-property-agency-name' ).html( propertyCard.getAttribute( 'data-agency' ) );
                            g_map.infoWindow.infoWindow.setContent( $( '#infowindow-wrapper' ).html() );
                            for (let i = 0; i < g_map.allMarkers.length; i++) {
                                if( g_map.allMarkers[i].pid === propertyCard.getAttribute( 'id' ).split( '-' )[1] ){
                                    propertyMarker 	= g_map.allMarkers[i];
                                    g_map.allMarkers[i].setIcon(g_map.selectedIcon);
                                    g_map.infoWindow.infoWindow.open(g_g_map.map.map, propertyMarker);
                                    g_map.landmarks.highlight_in_list_of_results(propertyCard.id.split('-')[1], "");
                                }
                            }
                        }
                    }
                });

                mapListCard[i].addEventListener( 'mouseout', function() {
                    $( '#results-no-map' ).css( 'visibility', 'hidden' );
                    let propertyCard = this.querySelector( '.property_list' ),
                            propertyMarker;
                    g_map.infoWindow.infoWindow.setContent( "" );
                    for (let i = 0; i < g_map.allMarkers.length; i++) {
                        if (g_map.allMarkers[i].pid === propertyCard.getAttribute('id').split('-')[0]) {
                            g_map.allMarkers[i].setIcon( g_map.propertyIcon );
                            google.maps.event.addListener(g_map.infoWindow.infoWindow, 'closeclick', function () {
                                g_map.allMarkers[i].setIcon(g_map.propertyIcon);
                            });
                        }
                    }
                    g_map.infoWindow.infoWindow.close();
                });
            }
		}
	},

    marker 				:	function( location = g_map.center, map = g_map.map.map ){

        // init marker
        var marker  =   new google.maps.Marker({

            map					: 		map,
            animation			: 		google.maps.Animation.DROP,
            position			: 		{ lat: location.lat, lng: location.lng },
            icon: (location.hasOwnProperty('pid') || window.location.href.indexOf('search') > -1 )  ? g_map.propertyIcon : g_map.selectedIcon,
            pid                 :       location.pid

        });

        // event handling
        //
        // mouse over - show infowindow with DOM data

        if( marker.pid ){

            marker.addListener('click', function () {

                let self = this;

                for (let i = 0; i < g_map.allMarkers.length; i++) {

                    g_map.allMarkers[i].setIcon(g_map.propertyIcon);

                }

                google.maps.event.addListener(g_map.infoWindow.infoWindow, 'closeclick', function () {

                    self.setIcon(g_map.propertyIcon);

                });

                g_map.landmarks.highlight_in_list_of_results(self.pid);

                self.setIcon(g_map.selectedIcon);

                if (typeof self.pid !== 'undefined') {

                    // Populate content of infowindow wrapper with div data
                    var propertyCard = g_map.results_container.find('#list-' + self.pid + '-property-card'),
                        residential_commercial = _.startsWith( 'commercial', _.toLower(propertyCard.attr( 'data-rent-buy' ))) ? 'commercial' : 'residential';
                        rent_buy = _.toLower(propertyCard.attr( 'data-rent-buy' )) === 'buy' ? 'sale' : 'rent';
                        property_name = _.toLower(propertyCard.attr('data-name').split( ' ' ).join( '-' ).replace( /[$?|{}()]/g, '-' ).replace( /-+/g, '-' ));

                    $('#infowindow-property-name').html(propertyCard.attr('data-name'));
                    $('#infowindow-property-location').html(propertyCard.attr('data-location'));
                    $('#infowindow-property-bed').html(propertyCard.attr('data-bedroom'));
                    $('#infowindow-property-bath').html(propertyCard.attr('data-bathroom'));
                    $('#infowindow-property-price').html(propertyCard.attr('data-price'));
                    $('#infowindow-property-agent-name').html(propertyCard.attr('data-agent'));
                    $('#infowindow-property-agency-name').html(propertyCard.attr('data-agency'));

                    document.getElementById( 'infowindow-redirect' ).href = baseUrl + rent_buy +'/' + residential_commercial + '/' + property_name + '/' + self.pid;

                    g_map.infoWindow.infoWindow.setContent($('#infowindow-wrapper').html());

                    g_map.infoWindow.infoWindow.open(g_map.map.map, self);

                    g_map.landmarks.highlight_in_list_of_results(self.pid);

                }

            });

        }

		//Pushing marker to array
        g_map.allMarkers.push( marker );

        return marker;

    },

    places_service 	    : 	function( map = g_map.map.map ){

        return new google.maps.places.PlacesService( map );

    },

    infoWindow 			: 	{

        infoWindow          :   '',

        invoke              :   function(){

            g_map.infoWindow.infoWindow     =   new google.maps.InfoWindow();

            return g_map.infoWindow.infoWindow;

        }
    },

    landmarks           :   {

        unique_landmark_type    :   function( locationTypes, queryTypes ){
            for (var i = 0; i < locationTypes.length; i++) {
                for (var j = 0; j < queryTypes.length; j++) {
                    if ( queryTypes[j] === locationTypes[i] ){
                        return queryTypes[j];
                    }
                }
            }
        },

        create_landmark_markers     :   function ( places, display = false, type, map = g_map.map.map ) {
            for (var i = 0; i < places.length; i++) {
                var place 		= 	places[i],
                    marker 		= 	new google.maps.Marker({
                        id 					:		place.id,
                        name				:		place.name,
                        map					: 		map,
                        types				:		place.types,
                        filter 				:		g_map.landmarks.unique_landmark_type( place.types, g_map.mapTypesList ),
                        icon: cdnUrl + 'assets/img/map/map_markers/' + 'location-' + type +'.png',
                        position			: 		place.geometry.location,
                        visible             :       display,
                    });
                g_map.allMarkers.push( marker );
            }
        },

        locate_landmarks    :   function( center = g_map.center, landmarkTypes = g_map.mapTypesList, radius = 1500 ){
            for (let k = 0; k < landmarkTypes.length; k++ ){
                let eachLandmarkType = landmarkTypes[k];
                // Locate nearby landmarks in radius of 2KM
                g_map.places_service().nearbySearch({
                    location: center,
                    radius: radius,
                    types: [eachLandmarkType]
                }, function (results, status, pagination) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        g_map.landmarks.create_landmark_markers(results);
                        if (pagination.hasNextPage) {
                            pagination.nextPage();
                        }
                    }
                });

            }
        },

        remove_all_landmark_markers     :   function( markers ){

            for ( var i = 0; i < markers.length; i++ ) {

                markers[i].setMap( null );

            }

        },

        show_hide_markers       :   function( type, value ){
            if( value === true ) {
                g_map.places_service().nearbySearch({
                    location: g_map.center,
                    radius: 1500,
                    types: [type]
                }, function (results, status, pagination) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        g_map.landmarks.create_landmark_markers(results, value, type);
                        // if (pagination.hasNextPage) {
                        //     pagination.nextPage();
                        // }
                    }
                });
            } else {
                for (var i = 0; i < g_map.allMarkers.length; i++) {
                    if( g_map.allMarkers[i].filter === type ) {
                        g_map.allMarkers[i].setVisible(false);
                    }
                }
            }
        },

        highlight_in_list_of_results    :   function( pid, clickTrigger="marker" ){

            // remove previous highlights
            $( '#map-container .property-card-list' ).removeClass('animate');

			if( clickTrigger === "marker" ){

	            $('html, body').scrollTop($("#map-view").find('#' + pid + '-property-tile-list').offset().top - 240);

			}

            // Scale item
            g_map.results_container.find( '#' + pid + '-property-tile-list .property-card-list' ).addClass('animate');

        },

    },

    /**
     * Initialize the map
     * @param  {JSON} center    Center of the map [property id]
     * @param  {array} locations lat,lng
     * @param  {string} mapContainer Id of div in which to invoke the map
     * @param  {string} resultsContainer Id of div in which to hold the results
     * @return {[type]}           [description]
     */
    __init        :   function( center, locations, mapContainer, resultsContainer ){
        g_map.map_container         =   mapContainer;
        g_map.results_container     =   $( '#' + resultsContainer );
        // Identify center
        g_map.center                =   center;
        // Invoke map
        g_map.map.invoke();
        // Identify map center
        g_map.marker();
        g_map.landmarks.locate_landmarks(center);
        // Invoke locations
        if( locations.length > 0 ){
            for( var i = 0; i < locations.length; i++ ){
                g_map.marker( locations[ i ] );
            }
        }
        // Invoke Info Window
        g_map.infoWindow.invoke();
		//
		g_map.property_map_list.invoke();
		$( '.btn-directions' ).on( 'click', function() {
            let d   =   document.getElementById( 'property-details' ).getAttribute( 'data-dest' );
			window.open( 'https://www.google.com/maps/?q=' + d, '_blank' );
		})
    }
};

var other = {
    __init  :   function(){
    },
    blog: {
        listings: function(){
            /**
             * 1.   call blogs api
             */
            axios.get( baseUrl+'blog/recent' ).then( function( response ) {
                $( '#blog_content' ).html( response.data );
                $('#blog_content').find('.blog_card').slice(2, 4).remove();
                $('.blog_card').addClass('column');

                var url = $('.blog_card>a').attr('href');
                $('.blog_card').append('<div class="read"><a href="'+ url + '" target="_blank">Read more <i class="long arrow alternate right icon"></i></a></div>');
                $('.blog_card>a').removeAttr('href').removeAttr('target');

                // $('.blog_card>a').removeAttr('href').removeAttr('target');
                // $('.blog_card').append("<div class='read'><a href=" + baseUrl + 'projects/uae' + " target='_blank'>Read more <i class='long arrow alternate right icon'></i></a></div>");
                $( "#blog_content:nth-child(1)" ).append( "<div class='col l4 m4 s4 column'><a><div style='background-image: url(" + cdnUrl + 'assets/img/banner/landing_new_project.png' + ");'></div><div>Checkout our new projects!</div></a><div class='read'><a href=" + baseUrl + 'projects/uae' + " target='_blank'>Read more <i class='long arrow alternate right icon'></i></a></div></div>" );
            }).catch( function( error ) {
            });
        }
    },
    ideal_property: {
        email: function(trigger, e){
            var url = baseApiUrl+'enquiry/ideal_property';
            var payload = {
                name: $('#ideal_property_name').val(),
                email: $('#ideal_property_email').val(),
                phone: $('#ideal_property_phone').val(),
                budget: $('#ideal_property_budget').val(),
                type: $('#ideal_property_type').val(),
                location: $('#ideal_property_location').val()
            }
            if( helpers.phonenumber.iti.isValidNumber() ) {
                $(trigger).addClass('loading');
                axios.post( url, payload ).then( function( response ) {
                    $(trigger).removeClass('loading');
                    if( response.data.error ) {
                        helpers.toast.__init( response.data.error );
                    } else {
                        dataLayer.push({ event: 'track_email_submit', data: payload });
                        $('.ui.modal.ideal_property').modal('hide');
                        helpers.toast.__init( response.data.success );
                    }
                }).catch( function( error ) {
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });
            } else {
                helpers.toast.__init( _.get(lang_static, 'toast.error_phone') );
            }
        }
    },
    custom_enquiry: {
        __init: function(trigger){

            $(trigger).addClass('loading');
            var temp = helpers.store.temp_data.read(),
            payload = temp.payload;

            payload.custom_form = temp.settings.custom_form;
            payload.custom_data = {
                    when: $('#modal_custom_enquiry_when').find('[name="when"]:checked').val(),
                    times: $('#modal_custom_enquiry_times').find('[name="times"]:checked').val(),
                    rooms: $('#modal_custom_enquiry_rooms').find('[name="rooms"]:checked').val(),
                    when_now: $('#modal_custom_enquiry_when_now').find('[name="when_now"]:checked').val(),
                    investor_job: $('#modal_custom_enquiry_investor_job').find('[name="investor_job"]:checked').val(),
                    where: $('#modal_custom_enquiry_where').find('[name="where"]:checked').val()
                };

            _.pull(temp, 'payload');
            var data_trigger = temp;

            helpers.store.temp_data.write = data_trigger;

            //1.
            var url = baseApiUrl + 'lead/project';
            //2.
            axios.post(url, payload).then(function (response) {
                helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                //Remove loading to the Button
                $('.ui.modal.custom_enquiry').modal('setting', 'transition', 'fade up').modal('hide');
                $(trigger).removeClass('loading');
                $('#project_lead').removeClass('loading');
                var project_data = helpers.store.temp_data.read();
                other.datalayer('project','email_submit', data_trigger);
            }).catch(function (error) {
                helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                //Remove loading to the Button
                $(trigger).removeClass('loading');
                $('.ui.modal.custom_enquiry').modal('setting', 'transition', 'fade up').modal('hide');
                $('#project_lead').removeClass('loading');
            });

        }
    },
    general_enquiry: {
      slide_context: function(){
          if($('.general_enquiry_container').hasClass("active")){
            $('.general_enquiry_container').removeClass('active');
            $('.general_enquiry_container').removeClass('popOut');
            $('.general_enquiry_container').addClass('slideIn');
            $('.general_enquiry_container').addClass('moveIn');
          }
          else{
            $('.general_enquiry_container').removeClass('moveIn');
            $('.general_enquiry_container').addClass('slideOut');
            $('.general_enquiry_container').addClass('popOut');
            $('.general_enquiry_container').addClass('active');
          }
      }
    },
    change_currency: function( trigger ){
        var value = $(trigger).children('option:selected').data('value'),
            currency = $(trigger).children('option:selected').data('currency');
        helpers.store.write( 'selectedCurrency', JSON.stringify( {currency: currency, value: value} ) );
        $( '.get_price' ).each( function( index ){
            var current_price = $(this).data('price'),
                new_price = new Intl.NumberFormat('en-AE', { maximumFractionDigits: 0 }).format( _.multiply(value,helpers.parseNumberCustom(current_price)) );
            $(this).find('.value').html( new_price );
            $(this).find('.currency').html( currency );
        } );
    },
    change_currency_mobile: function( currency, value ){
        var frmcurrency = currency.substr(currency.length - 3);
        helpers.store.write( 'selectedCurrency', JSON.stringify( {currency: frmcurrency, value: value} ) );
        $( '.get_price' ).each( function( index ){
            var current_price = $(this).data('price'),
                new_price = new Intl.NumberFormat('en-AE', { maximumFractionDigits: 0 }).format( _.multiply(value,helpers.parseNumberCustom(current_price)) );
            $(this).find('.value').html( new_price );
            $(this).find('.currency').html( frmcurrency );
            // only for tester push
        } );
    },
    update_currency: function(){
        var dataJSON = helpers.store.read( 'selectedCurrency' ),
            data = JSON.parse( dataJSON ),
            value = _.isNull(data) || _.isUndefined(data) ? '1' : data.value,
            currency = _.isNull(data) || _.isUndefined(data) ? 'AED' : data.currency;
        $('#currency_dropdown').children('option[data-currency='+currency+']').attr('selected','selected');
        $( '.get_price' ).each( function( index ){
            var current_price = $(this).data('price'),
                new_price = new Intl.NumberFormat('en-AE', { maximumFractionDigits: 0 }).format( _.multiply(value,helpers.parseNumberCustom(current_price)) );
            $(this).find('.value').html( new_price );
            $(this).find('.currency').html( currency );
        } );
    },
    datalayer: function(model,eventName, data){
        /** Default data */
        info = dataLayer[0];
        /** Data per model */
        switch(model) {
          case 'property':
            final_data = {
                //Website
                website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : _.get(data, 'residential_commercial') + '-' +_.get(data, 'rent_buy'),
                pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Product',
                device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,
                //Location
                loc_city_name          : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.get(data, 'city') : _.get(info, 'loc_city_name'),
                loc_neighbourhood_name : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.get(data, 'area') : _.get(info, 'loc_neighbourhood_name'),
                loc_name               : _.has(data, 'building') && !_.isEmpty(_.get(data, 'building')) ? _.get(data, 'building') : _.get(info, 'loc_name'),
                loc_breadcrumb         : ((_.has(data, 'city') && !_.isEmpty(_.get(data, 'city'))) || (_.has(data, 'area') && !_.isEmpty(_.get(data, 'area'))) || (_.has(data, 'building') && !_.isEmpty(_.get(data, 'building')))) ? _.snakeCase(_.toLower(_.get(data, 'city','')))+';'+_.snakeCase(_.toLower(_.get(data, 'area','')))+';'+_.snakeCase(_.toLower(_.get(data, 'building',''))) : _.get(info, 'loc_breadcrumb'),
                loc_city_id            : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.snakeCase(_.toLower(_.get(data, 'city'))) :_.get(info, 'loc_city_id'),
                loc_neighbourhood_id   : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.snakeCase(_.toLower(_.get(data, 'area'))) : _.get(info, 'loc_neighbourhood_id'),
                loc_id                 : _.has(data, 'building') && !_.isEmpty(_.get(data, 'building')) ? _.snakeCase(_.toLower(_.get(data, 'building'))) : _.get(info, 'loc_id'),
                //Property
                property_id            : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'property_id'),
                property_furnishing    : _.has(data, 'furnished') ? (_.get(data, 'furnished') == 0 ? 'Unfurnished' : (_.get(data, 'furnished') == 1 ? 'Furnished' : 'Partly-furnished')) : _.get(info, 'property_furnishing'),
                property_type          : _.has(data, 'type') && !_.isEmpty(_.get(data, 'type')) ? _.get(data, 'type') : _.get(info, 'property_type'),
                property_bed           : _.has(data, 'bedroom') ? _.get(data, 'bedroom') : _.get(info, 'property_bed'),
                property_area          : _.has(data, 'dimension.builtup_area') ? _.get(data, 'dimension.builtup_area') : _.get(info, 'property_area'),
                property_price         : _.has(data, 'price') ? _.get(data, 'price') : _.get(info, 'property_price'),
                property_bath          : _.has(data, 'bathroom') ? _.get(data, 'bathroom') : _.get(info, 'property_bath'),
                property_currency      : _.has(info, 'property_currency') ? _.get(info, 'property_currency') : 'AED',
                //Agent
                agent_id               : _.has(data, 'agent._id') ? _.get(data, 'agent._id') : _.get(info, 'agent_id'),
                agent_name             : _.has(data, 'agent.contact.name') ? _.get(data, 'agent.contact.name') : _.get(info, 'agent_name'),
                agency_id              : _.has(data, 'agent.agency._id') ? _.get(data, 'agent.agency._id') : _.get(info, 'agency_id'),
                agency_name            : _.has(data, 'agent.agency.contact.name') ? _.get(data, 'agent.agency.contact.name') : _.get(info, 'agency_name')
            }
            break;
          case 'project':
              final_data = {
                  //Website
                  website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : 'Projects',
                  pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Product',
                  device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                  language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,
                  //Location
                  loc_city_name          : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.get(data, 'city') : _.get(info, 'loc_city_name'),
                  loc_neighbourhood_name : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.get(data, 'area') : _.get(info, 'loc_neighbourhood_name'),
                  loc_breadcrumb         : ((_.has(data, 'city') && !_.isEmpty(_.get(data, 'city'))) || (_.has(data, 'area') && !_.isEmpty(_.get(data, 'area')))) ? _.snakeCase(_.toLower(_.get(data, 'city','')))+';'+_.snakeCase(_.toLower(_.get(data, 'area',''))) : _.get(info, 'loc_breadcrumb'),
                  loc_city_id            : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.snakeCase(_.toLower(_.get(data, 'city'))) :_.get(info, 'loc_city_id'),
                  loc_neighbourhood_id   : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.snakeCase(_.toLower(_.get(data, 'area'))) : _.get(info, 'loc_neighbourhood_id'),
                  //Project
                  project_id             : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'projecy_id'),
                  project_name           : _.has(data, 'name') && !_.isEmpty(_.get(data, 'name')) ? _.get(data, 'name') : _.get(info, 'project_name'),
                  project_price          : _.has(data, 'pricing.starting') ? _.get(data, 'pricing.starting') : _.get(info, 'project_price'),
                  project_type           : _.has(data, 'unit.property_type') ? _.get(data, 'unit.property_type') : _.get(info, 'project_type'),
                  project_currency       : _.has(info, 'project_currency') ? _.get(info, 'project_currency') : 'AED',
                  //Developer
                  developer_id               : _.has(data, 'developer._id') ? _.get(data, 'developer._id') : _.get(info, 'developer_id'),
                  developer_name             : _.has(data, 'developer.contact.name') ? _.get(data, 'developer.contact.name') : _.get(info, 'developer_name'),
              }
            break;
          case 'agent':
              final_data = {
                  //Website
                  website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : 'Agent Finder',
                  pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Community',
                  device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                  language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,

                  //Agent
                  agent_id               : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'agent_id'),
                  agent_name             : _.has(data, 'contact.name') && !_.isEmpty(_.get(data, 'contact.name')) ? _.get(data, 'contact.name') : _.get(info, 'agent_name'),
                  agency_id              : _.has(data, 'agency._id') ? _.get(data, 'agency._id') : _.get(info, 'agency_id'),
                  agency_name            : _.has(data, 'agency.contact.name') ? _.get(data, 'agency.contact.name') : _.get(info, 'agency_name'),
              }
            break;
          case 'service':
              final_data = {
                  //Website
                  website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : 'Services',
                  pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Services',
                  device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                  language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,

                  //Service
                  service_id             : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'service_id'),
                  service_name           : _.has(data, 'contact.name') && !_.isEmpty(_.get(data, 'contact.name')) ? _.get(data, 'contact.name') : _.get(info, 'service_name'),
              }
            break;
        }
        //User
        let user = JSON.parse(helpers.store.read('user_o'));
        user === null ? final_data.user_status = 'Not Logged In' : final_data.user_status = 'Logged In';
        //Event
        final_data.event = eventName;
        dataLayer.push(final_data);
    },
    change_language: function( trigger ){
        var language = $(trigger).children("option:selected").val();
        $('#language').val(language);
        window.location.href = language;
    },
    update_language: function() {
        let url =   new URI( window.location.href );
        var lang = url.segment(2);
        if (lang == '' || lang == 'en') {
            $('#language').val('en');
        } else if (lang == 'ar') {
            $('#language').val('ar');    
        }
    }
}

var search  =   {
    property:   {
        bindings    :   [ 'property.web_link','property.title','property.writeup.title','property.type','property.rent_buy','property.bedroom','property.bathroom','property.price','property.building','property.city','property.area','property.ref_no','property.dimension.builtup_area','property.images.original','property.residential_commercial','property._id','property.agent.contact.name','property.agent.agency.contact.name','property.agent.agency.contact.email','property.agent.contact.phone','agency.logo','_id'],
        template    :   $("#property_card_template").html(),
        regular     :   {
            __init      :   function(el){

                //Add loading to the Search Button
                $(el).addClass('loading');

                // Check for city, area, building
                var values = $('#property_suburb_hidden').val();
                
                search.property.regular.store_variables.suburb.value = values;
                search.property.regular.store_variables.suburb.label = $('#property_suburb_hidden_b').val();

                if( !_.isUndefined(values) && !_.isEmpty(values) ){

                    areas = values.split(', ');
                    areas.length = (areas.length)-1;
                    $.each(areas, function( index, value ){

                        locations  =   value.split('-');
                        if(locations.length === 1){
                            if(!_.includes(search.property.regular.store_variables.city, locations[0])){
                                search.property.regular.store_variables.city.push(locations[0]);
                            }
                        } else if(locations.length === 2){
                            if(!_.includes(search.property.regular.store_variables.city, locations[1])){
                                search.property.regular.store_variables.city.push(locations[1]);
                            }
                            if(!_.includes(search.property.regular.store_variables.area, locations[0])){
                                search.property.regular.store_variables.area.push(locations[0]);
                            }
                        } else if(locations.length === 3){
                            if(!_.includes(search.property.regular.store_variables.city, locations[2])){
                                search.property.regular.store_variables.city.push(locations[2]);
                            }
                            if(!_.includes(search.property.regular.store_variables.area, locations[1])){
                                search.property.regular.store_variables.area.push(locations[1]);
                            }
                            if(!_.includes(search.property.regular.store_variables.building, locations[0])){
                                search.property.regular.store_variables.building.push(locations[0]);
                            }
                        }

                    });

                    if(areas.length > 1){
                        all_locations   =   values.replace(/,\s*$/, "");
                        all_locations   =   encodeURIComponent(window.btoa(all_locations));
                        search.property.regular.store_variables.query_string.location = all_locations;
                    }

                }

                if(!_.isEmpty($('#keyword').val())){
                    search.property.regular.store_variables.query_string.keyword = $('#keyword').val();
                }

                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

                var qrsObj_prev     =   {};
                if(_.has(qrsObj_url_prev,'agency_id')){
                    qrsObj_prev.agency_id   =   qrsObj_url_prev.agency_id;
                }
                if(_.has(qrsObj_url_prev,'sort')){
                    qrsObj_prev.sort        =   qrsObj_url_prev.sort;
                }

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                    url         =   url.setSearch( qrsObj );
                    url         =   url.setSearch( qrsObj_prev );
                
                window.location.href =  url.toString();

            },
            prepopulate     :   function(){

                // Get payload
                var payload_data = JSON.parse( helpers.store.read('property_search') );

                // Translate Location while switching between languages
                if(!_.isEmpty(payload_data) && _.includes(payload_data,'suburb.value')){

                    areas = _.get(payload_data,'suburb.value').split(', ');
                    areas.length = (areas.length)-1;
                    payload_data.suburb.label = '';
                    $.each(areas, function( index, value ){
                        selectedLoc = _.filter(searchJson.property.location, function(loc) {
                            return loc.value === value;
                        });
                        if(!_.includes(selectedLoc,'0')){
                            payload_data.suburb.label += _.get(selectedLoc,'0.label') + ', ';
                        }
                    });

                }

                if(!payload_data){
                    payload_data = search.property.regular.deconstruct.url(window.location.href.replace(baseUrl,''));
                }

                // Populate form
                search.property.regular.populate_form(payload_data);
            },
            populate_form   :   function(payload){

                pages.landing.dropdown.callback.rent_buy(payload.rent_buy[0]);

                setTimeout(function(){
                    $('#property_rent_buy').dropdown('set selected', _.get(payload,'rent_buy[0]'));
                    $('#property_suburb').val(_.get(payload,'suburb.label'));
                    $('#property_suburb_hidden').val(_.get(payload,'suburb.value'));
                    $('#property_suburb_hidden_b').val(_.get(payload,'suburb.label'));
                    $('#property_type').dropdown('set selected', _.get(payload,'type[0]'));
                    $('#property_bedroom_min').dropdown('set selected', _.get(payload,'bedroom.min'));
                    $('#property_bedroom_max').dropdown('set selected', _.get(payload,'bedroom.max'));
                    $('#property_bathroom_min').dropdown('set selected', _.get(payload,'bathroom.min'));
                    $('#property_bathroom_max').dropdown('set selected', _.get(payload,'bathroom.max'));
                    $('#property_area_min').dropdown('set selected', _.get(payload,'dimension.builtup_area.min'));
                    $('#property_area_max').dropdown('set selected', _.get(payload,'dimension.builtup_area.max'));
                    $('#property_price_min').dropdown('set selected', _.get(payload,'price.min'));
                    $('#property_price_max').dropdown('set selected', _.get(payload,'price.max'));
                    $('#property_furnishing').dropdown('set selected', _.get(payload,'query_string.furnishing'));
                    $('#completion_status').dropdown('set selected', _.get(payload,'query_string.status'));
                    $('#keyword').val(_.get(payload,'query_string.keyword'));
                }, 100);

            },
            variables   :   function(){
                return {
                    ref_no      :   [],
                    permit_no   :   [],
                    building    :   [],
                    city		:	[],
                    area		:	[],
                    coordinates :   {
                        lat         :   undefined,
                        lng         :   undefined
                    },
                    dimension   :   {
                        builtup_area    :   {
                            min             :   undefined,
                            max             :   undefined
                        },
                        plot_size       :   {
                            min             :   undefined,
                            max             :   undefined
                        }
                    },
                    agent           :   {
                        contact         :   {
                            email           :   undefined
                        },
                        _id             :   undefined
                    },
                    amenities           :   [],
                    rent_buy	        :	[],
                    rental_frequency    :   [],
                    residential_commercial : [],
                    type       :       [],
                    settings    :   {
                        verified    :   undefined,
                        chat        :   undefined,
                        status      :   undefined
                    },
                    featured : {
                        status  : undefined
                    },
                    primary_view    :   [],
                    bedroom		        :		{
                        min             :       undefined,
                        max             :       undefined,
                    },
                    bathroom			:		{
                        min			    :		undefined,
                        max			    :		undefined,
                    },
                    price				:		{
                        min			    :		undefined,
                        max			    :		undefined,
                    },
                    // furnishing	:	[],
                    size    :   20,
                    page    :   1,
                    consumer_email  :   undefined,
                    agent   :   {
                        contact     :   {
                            email       :   undefined,
                        },
                        agency  :   {
                            contact     :   {
                                email       :   undefined
                            }
                        },
                        _id         :   undefined
                    },
                    sort            :   undefined,
                    featured        :   undefined,
                    suburb          :   {
                        label: undefined,
                        value: undefined
                    },
                    query_string    :   {
                        furnishing      :	undefined,
                        status          :   undefined,
                        keyword         :   undefined,
                        location        :   undefined
                    }
                }
            },
            construct   :   {
                url     :   function(search_inputs){

                    if( ( search_inputs.city.length > 1 ) && ( search_inputs.city[0] === " " ) ) {
                        var cityArray = _.slice( search_inputs.city, 1 );
                    } else {
                        var cityArray = search_inputs.city;
                    }

                    var urlObj = {
                        suburb: search_inputs.area.join('-or-') || null,
                        building: search_inputs.building.join('-or-') || null,
                        city: cityArray.join('-or-') || null,
                        rent_buy: _.isEmpty(search.property.regular.store_variables.rent_buy) ? 'Sale' : search_inputs.rent_buy.pop() || null,
                        // residential_commercial:
                        property_type: search_inputs.type.join('-or-') || null,
                        price_min: search_inputs.price.min || null,
                        price_max: search_inputs.price.max || null,
                        area_min: search_inputs.dimension.builtup_area.min || null,
                        area_max: search_inputs.dimension.builtup_area.max || null,
                        bedroom_min: (search_inputs.bedroom.min == 0) ? 'studio' : search_inputs.bedroom.min || null,
                        bedroom_max: (search_inputs.bedroom.max == 0) ? 'studio' : search_inputs.bedroom.max || null,
                        bathroom_min: search_inputs.bathroom.min || null,
                        bathroom_max: search_inputs.bathroom.max || null
                        // furnishing: search_inputs.furnishing || null
                    };

                    search.property.regular.store_variables.suburb = search_inputs.suburb;

                    search.property.regular.store_variables.rent_buy = [];
                    search.property.regular.store_variables.rent_buy.push(urlObj.rent_buy);

                    // 1-to-4-bedroom
                    // -apartment
                    // -for-rent-or-sale
                    // -in-Dubai-Marina-or-JBR-area
                    // -in-dubai-city
                    // -less-than-500000-aed
                    // -greater-than-100000-aed
                    // -with-1-to-10-bathroom
                    // Saving Previous Search
                    var previous_search 	=	{};
                    // Constructing query
                    var query = [];
                    // Setting Bedrooms
                    var bedroom_str = [];

                    if (!_.isNull(urlObj.bedroom_min)){
                        bedroom_str.push(urlObj.bedroom_min);
                    }

                    if (!_.isNull(urlObj.bedroom_max)) {
                        if( urlObj.bedroom_max != urlObj.bedroom_min ){
                            if ( !_.isNull(urlObj.bedroom_min) && !_.isEmpty(bedroom_str) ){
                                bedroom_str.push('to');
                            }
                            bedroom_str.push(urlObj.bedroom_max);
                        }
                    }

                    if (!_.isEmpty(bedroom_str)) {
                        bedroom_str.push('bedroom');
                        query['bedroom'] = bedroom_str.join("-");
                    }


                    // Setting Property_Type
                    if (!_.isNull(urlObj.property_type)) {
                        var all_selected_property_types		=	urlObj.property_type.toLowerCase().trim().split(" ");
                        query['type'] 				        = 	all_selected_property_types.join("-");
                    }


                    // Rent or Sale
                    if (!_.isNull(urlObj.rent_buy)) {
                        if (!_.isUndefined(urlObj.rent_buy)){
                            query['rent_buy'] = urlObj.rent_buy.toLowerCase();
                        }
                    }

                    // Suburbs
                    if (!_.isNull(urlObj.suburb)) {
                        var all_selected_suburbs 	=	urlObj.suburb.toLowerCase().replace(/ *\([^)]*\) */g, "").trim().split(' ').join('-');
                        query['area'] 		= 	all_selected_suburbs + "-area";
                    }

                    // Buildings
                    if (!_.isNull(urlObj.building)) {
                        var all_selected_buildings = urlObj.building.toLowerCase().replace(/ *\([^)]*\) */g, "").trim().split(' ').join('-');
                        query['building'] 				 = 	all_selected_buildings + "-building";
                    }


                    // Cities
                    if (!_.isNull(urlObj.city)) {
                        var all_selected_cities 	=	urlObj.city.toLowerCase().trim().split(' ');
                        query['city'] 				= 	all_selected_cities.join('-') + "-city";
                    }


                    // Price min
                    if (!_.isNull(urlObj.price_min)){
                        query['price_min'] = "price-greater-than-" + urlObj.price_min + "-aed";
                    }

                    // Price max
                    if (!_.isNull(urlObj.price_max)){
                        query['price_max'] = "price-less-than-" + urlObj.price_max + "-aed";
                    }

                    // Area min
                    if (!_.isNull(urlObj.area_min)){
                        query['area_min'] = "builtup-area-greater-than-" + urlObj.area_min + "-sqft";
                    }

                    // Area max
                    if (!_.isNull(urlObj.area_max)){
                        query['area_max'] = "builtup-area-less-than-" + urlObj.area_max + "-sqft";
                    }


                    // Bathrooms
                    var bathroom_str = [];
                    if (!_.isNull(urlObj.bathroom_min)){
                        bathroom_str.push(urlObj.bathroom_min);
                    }

                    if (!_.isNull(urlObj.bathroom_max)) {
                        if (_.isNull(urlObj.bathroom_min)){
                            bathroom_str.push(0);
                        }
                        bathroom_str.push('to')
                        bathroom_str.push(urlObj.bathroom_max);
                    }
                    if (!_.isEmpty(bathroom_str)) {
                        bathroom_str.push('bathroom');
                        query['bathroom'] = bathroom_str.join("-");
                    }
                    // Generating URL
                    var url 		= [];
                    var buckets 	= [];
                    var seo_buckets = [ 'rent_buy', 'city', 'area', 'building' ];
                    var delete_from_query 	=	[];

                    if( _.intersection(seo_buckets, _.keys(query)).length - 1 == 0 ){
                        var length = _.intersection(seo_buckets, _.keys(query)).length;
                    }
                    else{
                        var length = _.intersection(seo_buckets, _.keys(query)).length - 1
                    }
                    for( var u=0; u<(length); u++ ){

                        var v = seo_buckets[u];

                        if( v === 'rent_buy' && !_.isUndefined( query[v] ) ){
                            if( !_.includes( query[v], '-' ) || (_.startsWith( _.lowerCase(query[v]), 'short') ) ){
                                query[v]	=	"residential-" + query[v];
                            }
                            var p 	=	query[v].split(/-(.+)/);
                            url.push( ( p[1] == 'sale' ) ?  'buy' : p[1] );
                            url.push( p[0] );
                            query[v] 	=	p[1];
                        }
                        else{
                            if( v === 'city' && !_.isUndefined( query[v] ) ){
                                url.push( query[v].replace("-city","") );
                            }
                            if( v === 'area' && !_.isUndefined( query[v] ) ){
                                url.push( query[v].replace("-area","") );
                            }
                            if( v === 'building' && !_.isUndefined( query[v] ) ){
                                url.push( query[v].replace("-building","") );
                            }
                            delete_from_query.push( v );
                        }

                    }

                    url = url.join("/");

                    for( u=0; u<delete_from_query.length; u++ ){
                        delete query[delete_from_query[u]];
                    }
                    // Prefixes
                    var prefixes = [];

                    prefixes[0] = [];
                    prefixes[1] = [];
                    prefixes[2] = [];
                    prefixes[3] = [];
                    prefixes[4] = [];
                    prefixes[5] = [];
                    prefixes[6] = [];
                    prefixes[7] = [];
                    prefixes[8] = [];
                    prefixes[9] = [];
                    prefixes[10] = [];
                    prefixes[0]['field'] = "bedroom";
                    prefixes[0]['prefix'] = "";
                    prefixes[1]['field'] = "type";
                    prefixes[1]['prefix'] = "-";
                    prefixes[2]['field'] = "rent_buy";
                    prefixes[2]['prefix'] = "-for-";
                    prefixes[3]['field'] = "area";
                    prefixes[3]['prefix'] = "-in-";
                    prefixes[4]['field'] = "building";
                    prefixes[4]['prefix'] = "-in-";
                    prefixes[5]['field'] = "city";
                    prefixes[5]['prefix'] = "-in-";
                    prefixes[6]['field'] = "price_min";
                    prefixes[6]['prefix'] = "-";
                    prefixes[7]['field'] = "price_max";
                    prefixes[7]['prefix'] = "-";
                    prefixes[8]['field'] = "area_min";
                    prefixes[8]['prefix'] = "-";
                    prefixes[9]['field'] = "area_max";
                    prefixes[9]['prefix'] = "-";
                    prefixes[10]['field'] = "bathroom";
                    prefixes[10]['prefix'] = "-with-";


                    // Storing in Local Storage
                    if( !_.isEmpty( previous_search ) ){
                        localStorage.setItem( 'last_search', JSON.stringify(previous_search) );
                    }

                    // Construct String
                    var url_string = [];
                    first_one = true;
                    for (var i = 0; i < prefixes.length; i++) {
                        var field = prefixes[i]['field'];
                        var prefix = prefixes[i]['prefix'];

                        if (field in query) {
                            if (!first_one) {
                                url_string.push(prefix + query[field]);
                            }
                            else {
                                url_string.push(query[field]);
                                first_one = false;
                            }

                        }
                    }

                    last_segment = window.location.href.split( '/' ).pop();
                    search.property.regular.store_variables.featured = _.startsWith(last_segment,'featured-') ? 'featured' : undefined;
                    var last_slug   =   ( !_.isEmpty(search.property.regular.store_variables.featured) ? 'featured-' : '' ) + 'properties-for-';

                    var slug = ( !_.isNull(urlObj.property_type) || !_.isNull(urlObj.bedroom_min) || !_.isNull(urlObj.bedroom_max) ) ? '' : last_slug;

                    url	 =	_.endsWith( url, "-area" ) ? url.replace("-area","") : url;

                    url += "/"+slug+url_string.join("-").replace(" ", "-").replace(/-+/g, "-");

                    url = url.replace(/-+/g, "-");

                    var urlPath = baseUrl + url;
                    // hotfix for residential//commercial/properties
                    // if( !_.includes(urlPath, 'city') ){
                    //     urlPath 	=	urlPath.replace( "residential/properties", "residential-properties" ).replace( "commercial/properties", "commercial-properties" );
                    // }

                    return urlPath;

                }
            },
            deconstruct :   {
                buckets         :   function(url){

                    // Query to return
                    var query = new search.property.regular.variables;
                    // Url Parts
                    url_parts = url.split('/');
                    // Checking if buckets exist
                    if( url_parts.length === 1 ){
                        // No Buckets
                        var fragments_url   =   '';
                        var buckets_url     =   url_parts;
                    }
                    else{
                        // Yes Buckets
                        var fragments_url       =   url_parts.pop();
                        var buckets_url         =   url_parts;
                    }
                    // Buckets
                    var buckets     =   [ 'rent_buy', 'residential_commercial', 'city', 'area', 'building' ];
                    // Assign buckets
                    for( var i=0; i<buckets_url.length; i++ ){
                        var query_field     =   buckets[i];
                        var query_value     =   buckets_url[i];

                        if(query_field == 'rent_buy'){
                            // Typecasting buy, sale
                            if(query_value == 'buy'){
                                query_value = 'sale';
                            }
                            query.rent_buy.push(_.startCase(query_value));
                        }
                        if(query_field == 'residential_commercial'){
                            query.residential_commercial.push(_.startCase(query_value));
                        }
                        if(query_field == 'city'){
                            query.city = _.startCase(query_value.replace(/-/g,' ')).split(' Or ');
                        }
                        if(query_field == 'area'){
                            query.area = _.startCase(query_value.replace(/-/g,' ')).split(' Or ');
                        }
                        if(query_field == 'building'){
                            query.building = _.startCase(query_value.replace(/-/g,' ')).split(' Or ');
                        }
                    }
                    // Return bucketed query
                    return [query,fragments_url];
                },
                query_string    :   function(query,string){
                    // Check if query string exists
                    if(_.includes(string,'?')){
                        var query_string    =   string.split('?').pop().split('&');
                        for(var i=0; i<query_string.length; i++){
                            parts = query_string[i].split('=');
                            var key = parts[0];
                            var value = parts[1];
                            if(key == 'page'){
                                query.page = parseInt(value);
                            }
                            else if(key == 'sort'){
                                query.sort = value;
                            }
                            else if(key == 'consumer_email'){
                                query.consumer_email = decodeURIComponent(value);
                            }
                            else if(key == 'agent_id'){
                                query.agent._id = decodeURIComponent(value);
                            }
                            else if(key == 'agent_email'){
                                query.agent.contact.email = [ decodeURIComponent(value) ];
                            }
                            else if(key == 'agency_id'){
                                query.agent.agency._id = decodeURIComponent(value);
                            }
                            else if(key == 'agency_email'){
                                query.agent.agency.contact.email = [ decodeURIComponent(value) ];
                            }
                            else if(key == 'furnishing'){
                                query.furnished = parseInt(value);
                            }
                            else if(key == 'status'){
                                query.completion_status = value;
                            }
                            else if(key == 'keyword'){
                                query.keyword = decodeURIComponent(value);
                            }

                        }
                        // Remove query from fragments_url
                        string = string.split('?')[0];
                    }
                    return [query,string];
                },
                fragments       :   function(query,url){
                    // Hooking the url to identify start
                    url     =   ":"+url;
                    //  Getting fixed hooks first
                    //  PRICE
                    if(_.includes(url,'aed')){
                        // Check for min, max
                        var match   =   url.match("price-less-than-(.*)-aed");
                        if(match){
                            // Max present
                            query.price.max = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                        var match   =   url.match("price-greater-than-(.*)-aed");
                        if(match){
                            // Min present
                            query.price.min = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                    }
                    // Builtup Area
                    if(_.includes(url,'builtup-area')){
                        // Check for min, max
                        var match   =   url.match("builtup-area-less-than-(.*)-sqft");
                        if(match){
                            // Max present
                            query.dimension.builtup_area.max = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                        var match   =   url.match("builtup-area-greater-than-(.*)-sqft");
                        if(match){
                            // Min present
                            query.dimension.builtup_area.min = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                    }
                    // Dynamic Hooks
                    // BEDROOM
                    if(_.includes(url,'bedroom')){
                        if(_.includes(url,'studio')){
                            url = url.replace('studio',0);
                        }
                        // Check for start
                        var match   =   url.match("with-(.*)-bedroom");
                        if(!match){
                            // Check for mid
                            match   =   url.match(":(.*)-bedroom");
                        }
                        if(match){
                            var beds = match[1].match(/\d/g);
                            if(beds.length==1){
                                query.bedroom.min = parseInt(beds[0]);
                                query.bedroom.max = parseInt(beds[0]);
                            }
                            else if(beds.length==2){
                                query.bedroom.min = parseInt(beds[0]);
                                query.bedroom.max = parseInt(beds[1]);
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // BATHROOM
                    if(_.includes(url,'bathroom')){
                        // Check for mid
                        var match   =   url.match("with-(.*)-bathroom");
                        if(!match){
                            // Check for start
                            match   =   url.match(":(.*)-bathroom");
                        }
                        if(match){
                            var baths = match[1].match(/\d/g);
                            if(baths.length==1){
                                query.bathroom.min = parseInt(baths[0]);
                                query.bathroom.max = parseInt(baths[0]);
                            }
                            else if(baths.length==2){
                                query.bathroom.min = parseInt(baths[0]);
                                query.bathroom.max = parseInt(baths[1]);
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // Area
                    if(_.includes(url,'area')){
                        // Check for mid
                        var match   =   url.match("in-(.*)-area");
                        if(!match){
                            match   =   url.match(":(.*)-area");
                        }
                        if(match){
                            var areas = match[1].replace(/-/g,' ').split(' or ');
                            for(var i=0; i<areas.length; i++){
                                query.area.push(decodeURIComponent(_.trim(areas[i])));
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // Building
                    if(_.includes(url,'building')){
                        // Check for mid
                        var match   =   url.match("in-(.*)-building");
                        if(!match){
                            match   =   url.match(":(.*)-building");
                        }
                        if(match){
                            var buildings = match[1].replace(/-/g,' ').split(' or ');
                            for(var i=0; i<buildings.length; i++){
                                query.building.push(decodeURIComponent(_.trim(buildings[i])));
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // City
                    if(_.includes(url,'city')){
                        // Check for mid
                        var match   =   url.match("in-(.*)-city");
                        if(!match){
                            match   =   url.match(":(.*)-city");
                        }
                        if(match){
                            var buildings = match[1].replace(/-/g,' ').split(' or ');
                            for(var i=0; i<buildings.length; i++){
                                query.city.push(decodeURIComponent(_.startCase(_.trim(buildings[i]))));
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    //RENT_BUY
                    if(_.includes(url, 'rent') || _.includes(url, 'sale')){
                        // Check for mid
                        var match   =   url.match("for-(.*)");
                        if(!match){
                            match   =   url.match(":(.*)");
                        }
                        if(match){
                            query.rent_buy.push(_.startCase(match[1]));
                            query.rent_buy  =   _.uniq(query.rent_buy);
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }

                    // TYPE
                    if(!_.includes(url, 'properties')){
                        // Check for mid
                        match   =   url.match(":(.*)");
                        all_prop_types = searchJson.property.types;
                        matched_type = '';

                        for( var i = 0; i < all_prop_types.length; i++ ){
                            let matc   =  url.match(all_prop_types[i]);
                            if(!_.isEmpty(matc)){
                                matched_type   =   matc[0];
                            }
                        }
                        if(!_.isEmpty(matched_type)){
                            query.type.push(_.startCase(matched_type.replace('-',' ')));
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(matched_type,'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    else{
                        if(_.includes(url,'residential')){
                            query.residential_commercial.push('Residential');
                        }
                        else if(_.includes(url,'commercial')){
                            query.residential_commercial.push('Commercial');
                        }
                        else if(_.includes(url,'short')){
                            query.residential_commercial.push('Short Term Rent');
                        }
                        query.residential_commercial    =   _.uniq(query.residential_commercial);
                    }

                    // Check for mid
                    match   =   url.match(":(.*)");
                    all_keywords = searchJson.keywords;
                    matched_keyword = '';

                    for( var i = 0; i < all_keywords.length; i++ ){
                        let matc   =  url.match(all_keywords[i]);
                        if(!_.isEmpty(matc)){
                            matched_keyword   =   matc[0];
                        }
                    }

                    if(!_.isEmpty(matched_keyword)){
                        query.keyword = _.startCase(matched_keyword.replace('-',' '));
                        query.keyword_in_path = 1;
                        // Trim URL
                        // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                        url = _.trimEnd(url.replace(':','::').replace(matched_keyword,'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                    }

                    if(_.includes(url, 'featured')){
                        query.featured = {};
                        query.featured.status = true;
                    }
                    return query;
                },
                url             :   function(url){
                    var query,fragments;
                    // Extract buckets
                    parts       =   search.property.regular.deconstruct.buckets(url);
                    query       =   parts[0];
                    fragments   =   parts[1];
                    // Extract Query String Params
                    parts       =   search.property.regular.deconstruct.query_string(query,fragments);
                    query       =   parts[0];
                    fragments   =   parts[1];
                    // Extract other fragments
                    query       =   search.property.regular.deconstruct.fragments(query,fragments);

                    return query;
                }
            },
            find      :   function(el){

                // Check for city, area, building
                var values = el['value'];
                search.property.regular.store_variables.suburb.value = values;
                // search.property.regular.store_variables.suburb.label = el['label'];

                if( !_.isUndefined(values) && !_.isEmpty(values) ){

                    areas = values;

                    locations  =   areas.split('-');
                    if(locations.length === 1){
                        if(!_.includes(search.property.regular.store_variables.city, locations[0])){
                            search.property.regular.store_variables.city.push(locations[0]);
                        }
                    } else if(locations.length === 2){
                        if(!_.includes(search.property.regular.store_variables.city, locations[1])){
                            search.property.regular.store_variables.city.push(locations[1]);
                        }
                        if(!_.includes(search.property.regular.store_variables.area, locations[0])){
                            search.property.regular.store_variables.area.push(locations[0]);
                        }
                    } else if(locations.length === 3){
                        if(!_.includes(search.property.regular.store_variables.city, locations[2])){
                            search.property.regular.store_variables.city.push(locations[2]);
                        }
                        if(!_.includes(search.property.regular.store_variables.area, locations[1])){
                            search.property.regular.store_variables.area.push(locations[1]);
                        }
                        if(!_.includes(search.property.regular.store_variables.building, locations[0])){
                            search.property.regular.store_variables.building.push(locations[0]);
                        }
                    }

                    // if(areas.length > 1){
                    //     all_locations   =   areas.replace(/,\s*$/, "");
                    //     all_locations   =   encodeURIComponent(window.btoa(all_locations));
                    //     search.property.regular.store_variables.query_string.location = all_locations;
                    // }

                }
                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
                
                var qrsObj_prev     =   {};

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                    url         =   url.setSearch( qrsObj );
                    url         =   url.setSearch( qrsObj_prev );

                window.location.href =  url.toString();

            }
        },
        budget      :   {
            __init      :   function(el){

                //Add loading to the Search Button
                $(el).addClass('loading');

                // 1.   Construct URL
                var url     =   search.property.budget.construct.url(search.property.budget.store_variables);

                //Writing the search to localstorage
                helpers.store.write('property_budget_search', helpers.store.stringify(search.property.budget.store_variables));

                // 2.   Redirect
                window.location.href =  url;

            },
            prepopulate     :   function(){
                // Get payload
                var payload = search.property.budget.deconstruct.url(window.location.href.replace(baseUrl,''));

                // Populate form
                search.property.budget.populate_form(payload);
            },
            populate_form   :   function(payload){

                pages.landing.dropdown.callback.rent_buy(payload.property.rent_buy);

                $('#property_rent_buy').dropdown('set selected', payload.property.rent_buy);
                $('#budget_city').dropdown('set selected', payload.property.city);

                setTimeout(function() {
                    $('#property_price_min').dropdown('set selected', payload.property.price.min);
                    $('#property_price_max').dropdown('set selected', payload.property.price.max);
                }, 100);

            },
            variables   :   function(){
                return {
                    city                :       [],
                    rent_buy	        :		[],
                    price				:		{
                        min				:		undefined,
                        max				:		undefined
                    }
                }
            },
            construct: {
                url: function(search_inputs){
                    var residential_commercial  =  _.startsWith( _.lowerCase(search_inputs.rent_buy), 'commercial') ? 'Commercial' : 'Residential';
                    var is_short_term           =  _.includes( _.lowerCase(search_inputs.rent_buy), 'short') ? true : false;
                    var data = {
                        city: search_inputs.city.length ? search_inputs.city : 'Dubai',
                        rent_buy: _.endsWith( _.lowerCase(search_inputs.rent_buy), 'sale') ? 'Sale' : ( is_short_term ? search_inputs.rent_buy.toString().replace('Commercial-','') : 'Rent' ),
                        res_comm: residential_commercial,
                        price: {
                            min: search_inputs.price.min ? search_inputs.price.min : _.endsWith( _.lowerCase(search_inputs.rent_buy), 'rent') ? 10000 : 100000,
                            max: search_inputs.price.max ? search_inputs.price.max : _.endsWith( _.lowerCase(search_inputs.rent_buy), 'rent') ? 1000000 : 50000000
                        }
                    }
                    var url = baseUrl + 'budget-search/results?residential_commercial=' + data.res_comm + '&rent_buy=' + data.rent_buy + '&price_min=' + data.price.min + '&price_max=' + data.price.max + '&emirate=' + data.city;

                    return url;
                }
            },
            deconstruct: {
                url: function(){
                    var url = window.location.href;

                    //construct payload for validation
                    var payload = {
                        property : {}
                    };

                    //RENT_BUY
                    if(_.includes(url, '=Commercial')){
                        payload.property.rent_buy = 'Commercial-' + url.split('rent_buy=').pop().split('&price')[0];
                    } else if(_.includes(url, '=Residential')){
                        payload.property.rent_buy = url.split('rent_buy=').pop().split('&price')[0];
                    }

                    //City
                    if(url.match("emirate=(.*)")){
                        var city = url.match("emirate=(.*)")[1];
                        payload.property.city = decodeURI(city);
                    }

                    //PRICE
                    if(url.match("price_min=(.*)")){
                        var prices = url.match("price_min=(.*)")[1].match(/^\d+|\d+\b|\d+(?=\w)/g);
                        payload.property.price = {};
                        payload.property.price.min = parseInt(prices[0]);
                        payload.property.price.max = parseInt(prices[1]);
                    }

                    return payload;

                }
            },
            listings: {
                __init    :   function(){

                    var url = window.location.href.replace('results','data');

                    axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {
                        // Populate
                        $('#listings_wrapper').html(response.data);
                        // Activate Accordian
                        $('.ui.accordion').accordion();
                        // Lazy Load
                        helpers.lazy_load();
                    }).catch( function( error ) {
                    });

                }
            }
        },
        listings    :   function(payload,sink){

            // Check for rent_buy
            if(_.includes(_.get(payload,'rent_buy.0',''), 'Commercial')){
                var parts = _.get(payload,'rent_buy.0','').split(/-(.+)/);
                payload.rent_buy                    =   [];
                payload.residential_commercial      =   [];
                payload.rent_buy.push(_.get(parts,'1','Sale').replace(/-/g, ' '));
                payload.residential_commercial.push(_.get(parts,'0','Commercial'));
            }
            else if(!_.isEmpty(_.get(payload,'rent_buy.0',undefined))){
                var rent_buy                        =   _.get(payload,'rent_buy.0','').replace(/-/g, ' ');
                payload.rent_buy                    =   [];
                payload.residential_commercial      =   [];
                payload.rent_buy.push(rent_buy);
                payload.residential_commercial.push('Residential');
            }

            // Check for city, area, building
            var value = $('#property_suburb_hidden').val();
            // var a = search.property.regular.store_variables.suburb[0];
            if( !_.isUndefined(value) && !_.isEmpty(value) ){
                areas      =   value.split(',');
                locations  =   areas[areas.length -1 ];
                locations  =   locations.split('-');
                if(locations.length === 1){
                    if(!_.includes(search.property.regular.store_variables.city, locations[0])){
                        search.property.regular.store_variables.city.push(locations[0]);
                    }
                } else if(locations.length === 2){
                    if(!_.includes(search.property.regular.store_variables.city, locations[1])){
                        search.property.regular.store_variables.city.push(locations[1]);
                    }
                    if(!_.includes(search.property.regular.store_variables.area, locations[0])){
                        search.property.regular.store_variables.area.push(locations[0]);
                    }
                } else if(locations.length === 3){
                    if(!_.includes(search.property.regular.store_variables.city, locations[2])){
                        search.property.regular.store_variables.city.push(locations[2]);
                    }
                    if(!_.includes(search.property.regular.store_variables.area, locations[1])){
                        search.property.regular.store_variables.area.push(locations[1]);
                    }
                    if(!_.includes(search.property.regular.store_variables.building, locations[0])){
                        search.property.regular.store_variables.building.push(locations[0]);
                    }
                }
            }

            var url = baseApiUrl+"property?query=" + JSON.stringify(helpers.clean.junk(payload));
            // #3
            axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {

                // 1.   Identify Data
                var data    =   response.data.data;
                // 2.   TODO Validation for if data exists
                var property = _.map(data,function(item){
                    var t = {};
                    t.property  = item;
                    t._id       = _.get(item,'_id','');
                    return t;
                });

                // 3.   Initiate Bind
                if(sink == 'featured_properties'){
                    if(property.length == 0){
                        payload.featured.status = false;
                        search.property.listings(payload, 'featured_properties');
                    }else{
                        property = _.shuffle(property);
                        property = _.take(property, 2);
                        helpers.bind(property, search.property.bindings, search.property.template, sink);
                    }
                }else{
                    if(property.length > 0){ $('#' + sink).parent().siblings().removeClass('hide'); }
                    helpers.bind(property, search.property.bindings, search.property.template, sink);
                }
                // 4.   Lazy Load
                helpers.lazy_load();

            }).catch( function( error ) {
            });
        },
        similar     :   function(id,sink){
            var url = baseApiUrl+"property/"+id+"/similar";
            // #3
            axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {
                // 1.   Identify Data
                // TODO Validation for if data exists
                var data    =   response.data.data;
                var property = _.map(data,function(item){
                    var t = {};
                    t.property  = item;
                    t._id       = _.get(item,'_id','');
                    return t;
                });
                $( "#property_card_list" ).prev().removeClass( "hide" );
                // 3.   Initiate Bind
                helpers.bind(property,search.property.bindings,search.property.template,sink);
                // 4.   Lazy Load
                helpers.lazy_load();

            }).catch( function( error ) {
            });
        },
        
    },
    agent:  {
        __init: function(el) {

            //Add loading to the Search Button
            $(el).addClass('loading');

            if(!_.isEmpty($('#keyword').val())){
                search.agent.store_variables.query_string.keyword = $('#keyword').val();
            }

            // 1.   Construct URL
            var url     =   search.agent.construct.url(search.agent.store_variables);

            //Writing the search to localstorage
            helpers.store.write('agent_search', helpers.store.stringify(search.agent.store_variables));

            //  2.1.    Get new query string object
            var qrsObj  =   search.agent.store_variables.query_string;
            for (var propName in qrsObj) {
                if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                    delete qrsObj[propName];
                }
            }

            //  2.2.  Retain search with previous query string
            var url_prev        =   new URI( window.location.href );
            var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
            var qrsObj_prev     =   {};
            if(_.has(qrsObj_url_prev,'sort')){
                qrsObj_prev.sort        =   qrsObj_url_prev.sort;
            }

            //  2.3.  Construct URL with new and previous query strings
            var url         =   new URI( url );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );
            // 3.   Redirect
            window.location.href =   url.toString();

        },
        prepopulate     :   function(){

            // var url = window.location.href.replace(baseUrl,'');
            // var payload = {};
            // var parts = url.split('/');
            // var area = parts[parts.length - 1];
            // payload.agent_brokerage = url.match("uae/(.*)/")[1];
            // payload.agent_areas = area.split('-area')[0].split('-').join(' ');

            var url         =   new URI( window.location.href );
            var payload     =   {};

            // Path segments
            // Brokerage
            if(!_.isEmpty(url.segment(3))){
                payload.agent_brokerage = url.segment(3);
            }
            // Area
            if(!_.isEmpty(url.segment(4))){
                var area    =   url.segment(4);
                area        =   _.endsWith( area, "-area" ) ? area.replace("-area","") : area;
                payload.agent_areas = _.startCase(area);
            }

            // Query string
            query_string    =   url.query(true);

            if(url.hasQuery("keyword")){
                payload.keyword = query_string.keyword;
            }

            // Populate form
            search.agent.populate_form(payload);
        },
        populate_form   :   function(payload){
            $('#agent_brokerage').dropdown('set selected', _.get(payload,'agent_brokerage'));
            $('#agent_areas').dropdown('set selected', _.get(payload,'agent_areas'));
            $('#keyword').val(_.get(payload,'keyword'));
        },
        variables   :   function(){
            return {
                agency      :   [],
                area        :   [],
                city        :   [],
                building    :   [],
                query_string: {
                    keyword     :   undefined
                }
            }
        },
        construct   :   {
            url     :   function(urlObj){

                var url = '';

                //All inputs empty
                if(_.isEmpty(urlObj.agency) && _.isEmpty(urlObj.area)) {
                    var all_empty = 'agents-search/uae/all-agency/all-suburbs';
                    url += all_empty;
                }

                if(!_.isEmpty(urlObj.agency) && _.isEmpty(urlObj.area)) {
                    url += 'agents-search/uae/' + urlObj.agency + '/all-suburbs';

                } else if(_.isEmpty(urlObj.agency) && !_.isEmpty(urlObj.area)) {

                        var area = 'agents-search/uae/all-agency' + '/' + urlObj.area + '-area';

                        url += area.replace(/\s/g, '-');

                } else if(!_.isEmpty(urlObj.agency) && !_.isEmpty(urlObj.area)) {

                    var agency = urlObj.agency;

                    var area = 'agents-search/uae/' + agency + '/' + urlObj.area + '-area';

                    url += area.replace(/\s/g, '-');

                }

                var urlPath = baseUrl + url;

                return urlPath;

            }
        }
    },
    agency:  {
        __init      :       function(){
            search.agency.listings();
        },
        variables   :   {
            agency	 :		{
                name :   null
            }
        }
    },
    projects: {
        __init: function(el){
            //Add loading to the Search Button
            $(el).addClass('loading');

            let country =   $(el).attr('data-country');

            // Check for city and area
            var value   =   $('#project_location').val();
            if( !_.isUndefined(value) && !_.isEmpty(value) ){

                search.projects.store_variables.location = value;

                areas      =   value.split(',');
                locations  =   areas[areas.length -1 ];
                locations  =   locations.split('-');

                if(locations.length === 1){
                    if(!_.includes(search.projects.store_variables.query_string.country, locations[0])){
                        search.projects.store_variables.query_string.country.push(locations[0].replace(/ *\([^)]*\) */g, "").trim());
                    }
                }
                else if(locations.length === 2){
                    if(!_.includes(search.projects.store_variables.query_string.city, locations[0])){
                        search.projects.store_variables.query_string.city.push(locations[0].replace(/ *\([^)]*\) */g, "").trim());
                    }
                    if(!_.includes(search.projects.store_variables.query_string.country, locations[1])){
                        search.projects.store_variables.query_string.country.push(locations[1].replace(/ *\([^)]*\) */g, "").trim());
                    }
                }
                else if(locations.length === 3){
                    if(!_.includes(search.projects.store_variables.query_string.area, locations[0])){
                        search.projects.store_variables.query_string.area.push(locations[0].replace(/ *\([^)]*\) */g, "").trim());
                    }
                    if(!_.includes(search.projects.store_variables.query_string.city, locations[1])){
                        search.projects.store_variables.query_string.city.push(locations[1].replace(/ *\([^)]*\) */g, "").trim());
                    }
                    if(!_.includes(search.projects.store_variables.query_string.country, locations[2])){
                        search.projects.store_variables.query_string.country.push(locations[2].replace(/ *\([^)]*\) */g, "").trim());
                    }
                }
            }

            search.projects.store_variables.country_segment = country;

            if(!_.isEmpty($('#keyword').val())){
                search.projects.store_variables.query_string.keyword = $('#keyword').val();
            }

            //  1.  Construct URL
            var url     =   search.projects.construct.url(search.projects.store_variables);

            //  Writing search to the localstorage
            helpers.store.write('project_search_'+country, helpers.store.stringify(search.projects.store_variables));

            //  2.1.    Get new query string object
            var qrsObj  =   search.projects.store_variables.query_string;
            for (var propName in qrsObj) {
                if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                    delete qrsObj[propName];
                }
            }

            //  2.2.  Retain search with previous query string
            var url_prev        =   new URI( window.location.href );
            var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

            var qrsObj_prev     =   {};
            if(_.has(qrsObj_url_prev,'sort')){
                qrsObj_prev.sort        =   qrsObj_url_prev.sort;
            }

            //  2.3.  Construct URL with new and previous query strings
            var url         =   new URI( url );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );

            // 3.   Redirect
            window.location.href =  baseUrl + url.toString();
        },
        construct: {
            url     :   function(urlObj){
                var url = urlObj.developer ? 'projects/' + urlObj.country_segment + '/' + urlObj.developer : 'projects/' + urlObj.country_segment;
                return url;
            }
        },
        prepopulate     :   function(country){
            // Get payload
            var payload = JSON.parse(localStorage.getItem("project_search_"+country));

            // Populate form
            if(payload){
                search.projects.populate_form(payload);
            }
        },
        populate_form   :   function(payload){
            $('#projects_project').dropdown('set selected', payload.developer);
            $('#completion_status').dropdown('set selected', payload.query_string.status);
            $('#keyword').val(_.get(payload,'query_string.keyword'));
            $('#project_location').val(_.get(payload,'location'));
        },
        variables   :   function(){
            return {
                developer       :   undefined,
                location        :   undefined,
                query_string    :   {
                    status          :   undefined,
                    keyword         :   undefined,
                    area            :   [],
                    city            :   [],
                    country         :   [],
                    country_segment :   undefined
                }
            }
        }
    },
    services: {
        __init: function(el){
            $(el).addClass('loading');
            search.services.listings($(el).attr('data-_id'));
        },
        listings: function(type) {
            var url = type ? 'services/' + type + '/uae' : 'services';
            window.location.href = baseUrl + url;
        }
    },
    redirect_landing: function() {
        helpers.store.remove('property_search');
        window.location.href = baseUrl
    },
    new_search: function(el) {
        let page = $(el).attr('data-page');
        let subpage = $(el).attr('data-subpage');
        helpers.filter[page]();
        // window.location.href = helpers.home[page];
        window.location.href = !_.isEmpty(subpage) ? helpers.home[page][subpage] : helpers.home[page];
    }
}

search.property.regular.store_variables     =   new search.property.regular.variables();
search.property.budget.store_variables      =   new search.property.budget.variables();
search.agent.store_variables                =   new search.agent.variables();
search.projects.store_variables             =   new search.projects.variables();

var user = {
    __init:   function(){
        $(".user_dashboard").sidebar('setting', 'transition', 'overlay');
        setTimeout(function(){ $(".pusher").css({height:'auto'}); }, 2000);
    },
    newsletter: {
        __init: function(trigger){
            if ( !_.isEmpty($( "#newsletter_email" ).val()) && /\S+@\S+\.\S+/.test($( "#newsletter_email" ).val()) ) {
                // Subscribe to newsletter
                email      =   $( "#newsletter_email" ).val();
                user.newsletter.subscribe(email);
                $(trigger).html('Subscribed!');
            }
            else{
                helpers.toast.__init( _.get(lang_static, 'toast.error_email_b') );
            }
        },
        subscribe: function(email){
            axios.post(baseApiUrl + 'user/newsletter/subscribe/'+email,[]).then(function (response) {
                $("#newsletter_email").val('');
                helpers.toast.__init( _.get(lang_static, 'toast.success_newsletter_subscribed') );
            }).catch(function (error) {
            });
        }
    },
    property: {
        unfavourite: {
            __init: function(trigger){
                var state   =   helpers.user.state.is_logged_in();

                // #1
                if(state[0]){
                    $(trigger).addClass('loading');
                    // #2
                    var modal_id    =   $(trigger).attr("data-_uid"),
                    user_id         =   _.get(state[1],'id','');
                    url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;
                    // #3
                    axios.delete(url).then(function (response) {

                        $(trigger).removeClass('favourite');
                        $(trigger).removeClass('loading');
                        $(trigger).addClass('outline');
                        // $(trigger).closest(".dashboard_card").remove();
                        $(trigger).removeAttr("data-_uid");

                    }).catch(function (error) {});
                } else {

                    if(mobile === true){
                        pages.signin.modal.__init();
                    } else {
                        helpers.trigger.modal.__init('signin_register', 'fade up');
                    }

                }
            },
            search: function(trigger) {
                var state   =   helpers.user.state.is_logged_in();

                $(trigger).addClass('loading');

                var modal_id    =   $(trigger).attr("data-_id"),
                user_id         =   _.get(state[1],'id','');
                url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;

                axios.delete(url).then(function (response) {
                    user.property.favourite.property.searches();
                }).catch(function (error) {});

            },
            property: function(trigger) {

                var state   =   helpers.user.state.is_logged_in();

                $(trigger).addClass('loading');

                var modal_id    =   $(trigger).attr("data-_id"),
                user_id         =   _.get(state[1],'id','');
                url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;

                axios.delete(url).then(function (response) {
                    user.property.favourite.property.properties();
                }).catch(function (error) {});

            }
        },
        favourite: {
            __init: function(trigger, mobile){

                var state   =   helpers.user.state.is_logged_in();

                // #1
                if(state[0]){

                    $(trigger).addClass('loading');

                    // Check if favourite or unfavourite
                    if( $(trigger).attr("data-_uid") ){
                        // Unfavourite
                        user.property.unfavourite.__init(trigger);

                    } else {

                        // #2
                        var modal_id    =   $(trigger).attr("data-_id"),
                        user_id         =   _.get(state[1],'id','');
                        url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;
                        // #3
                        axios.post(url).then(function (response) {
                            $(trigger).removeClass("outline").addClass("favourite");
                            $(trigger).removeClass('loading');
                            $(trigger).attr("data-_uid", response.data._id);
                        }).catch(function (error) {
                            $(trigger).removeClass('loading');
                        });

                    }

                } else {

                    if(mobile === true){

                        pages.signin.modal.__init();

                    } else {

                        helpers.trigger.modal.__init('signin_register', 'fade up');

                    }

                }

            },
            search: {
                __init: function(trigger, is_mobile){
                    /**
                    * trigger = this
                    * 1.   Check if user_logged else show login modal
                    * 2.   Build payload URL
                    * 3.   Call Api
                    */

                    // #1
                    if(helpers.user.loggedin.__init('user')){
                        //#3
                        var user_ = JSON.parse(helpers.store.read('user_o'));
                        var user_id = _.get(user_,'id');
                        var payload = {};

                        var previous_search = search.property.regular.deconstruct.url(window.location.href.replace(baseUrl,''));

                        if(previous_search){
                            payload = previous_search
                        } else {
                            payload = helpers.store.stringify({
                                rent_buy: 'Sale',
                                residential_commercial: ['Residential']
                            });
                        }

                        axios.post( baseApiUrl + "user/"+ user_id +"/favourite/property", helpers.clean.junk(payload)).then(function (response) {
                            helpers.toast.__init( _.get(lang_static, 'toast.success_search_saved') );
                        }).catch(function (error) {
                        });

                    } else {

                        if(is_mobile === 'mobile'){

                            pages.signin.modal.__init();

                        } else {

                            helpers.trigger.modal.__init('signin_register', 'fade up');
                        }
                        //#2

                    }
                }
            },
            property: {
                properties: function() {

                    var state   =   helpers.user.state.is_logged_in();

                    if(state[0]){

                        var payload = {};
                        payload.subtype = ['model'];

                        var url = baseUrl+"users/"+_.get(state[1],'id','-')+"/favourite/property?query=" + JSON.stringify(helpers.clean.junk(payload));

                        axios.get(url).then( function( response ) {

                            $('#favourite_properties').html(response.data);

                            helpers.lazy_load();

                        }).catch(function (error) {});

                    }

                },
                searches: function(){
                    var state   =   helpers.user.state.is_logged_in();

                    var payload = {
                        subtype : ['search']

                    }
                    var url = baseUrl+"users/"+_.get(state[1],'id','-')+"/favourite/property?query=" + JSON.stringify(payload);

                    axios.get(url).then( function( response ) {

                        $('#favourite_searches').html(response.data);

                    }).catch(function (error) {
                    });

                }
            }
        },
        alert: {
            __init: function(trigger, is_mobile) {

                if(helpers.user.loggedin.__init('user')){

                    $('#property_alert_rent_buy').dropdown({
                        values: pages.landing.dropdown.data.rent_buy,
                        placeholder: 'Rent or Buy',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.rent_buy = [];
                                lead.property.alert.store_variables.rent_buy.push(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            user.property.alert.rent_buy(value);
                        }
                    });

                    $('#property_alert_price_min').dropdown({
                        values: pages.landing.dropdown.data.price.sale,
                        placeholder: 'Min Price',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && (lead.property.alert.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.price.min = parseInt(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if ($('#property_alert_rent_buy').dropdown('get text') === 'Buy' || $('#property_alert_rent_buy').dropdown('get text') === 'Commercial Buy') {
                                var rent_type = pages.landing.dropdown.data.price.sale
                            } else {
                                var rent_type = pages.landing.dropdown.data.price.rent
                            }
                            pages.landing.dropdown.callback.from(value, '#property_alert_price_max', rent_type, 'price');
                        }
                    });
                    $('#property_alert_price_min').dropdown('set selected', '100000');

                    $('#property_alert_price_max').dropdown({
                        values: pages.landing.dropdown.data.price.sale,
                        placeholder: 'Max Price',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && (lead.property.alert.store_variables.price.max != parseInt(value)) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.price.max = parseInt(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if ($('#property_alert_rent_buy').dropdown('get text') === 'Buy' || $('#property_alert_rent_buy').dropdown('get text') === 'Commercial Buy') {
                                var rent_type = pages.landing.dropdown.data.price.sale
                            } else {
                                var rent_type = pages.landing.dropdown.data.price.rent
                            }
                            pages.landing.dropdown.callback.to(value, '#property_alert_price_min', rent_type, 'price');
                        }
                    });
                    $('#property_alert_price_max').dropdown('set selected', '1000000');

                    setTimeout(function() {
                        $('#property_alert_suburb').dropdown({
                            values: pages.agent.dropdown.data.areas,
                            placeholder: 'Areas',
                            maxSelections: 1,
                            action: function(text, value, element) {
                                $(this).dropdown('set selected', value);
                                areas = value.split(',');
                                locations = areas[areas.length - 1];
                                lead.property.alert.store_variables.area.push(locations);
                                lead.property.alert.store_variables.area = [_.uniq(lead.property.alert.store_variables.area).pop()];
                            }
                        });
                    }, 500);

                    $('#property_alert_frequency').dropdown({
                        values: pages.landing.dropdown.data.alert.frequency,
                        placeholder: 'Frequency',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.frequency = [];
                                lead.property.alert.store_variables.frequency.push(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                        }
                    });
                    $('#property_alert_frequency').dropdown('set selected', 'Weekly');

                    $('.ui.modal.property_alert').modal('setting', 'transition', 'horizontal flip').modal('show');

                } else {

                    if(is_mobile === 'mobile'){

                        pages.signin.modal.__init();

                    } else {

                        helpers.trigger.modal.__init('signin_register', 'fade up');
                    }

                }

            },
            rent_buy: function(value) {

                if (value === 'Sale' || value === 'Commercial-Sale') {

                    $('#property_alert_price_min, #property_alert_price_max').empty();

                    $.each(pages.landing.dropdown.data.price.sale, function(i, item) {
                        $('#property_alert_price_min, #property_alert_price_max').append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    $('#property_alert_price_min, #property_alert_price_max').dropdown('clear');

                } else if (value === 'Rent' || value === 'Commercial-Rent' || value === 'Short-term-Rent') {

                    $('#property_alert_price_min, #property_alert_price_max').empty();

                    $.each(pages.landing.dropdown.data.price.rent, function(i, item) {
                        $('#property_alert_price_min, #property_alert_price_max').append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    $('#property_alert_price_min, #property_alert_price_max').dropdown('clear');

                }
            },
            property: {
                properties: function() {

                    var state   =   helpers.user.state.is_logged_in();

                    if(state[0]){

                        var payload = {};
                        payload.frequency = ['daily', 'monthly', 'weekly', 'yearly'];

                        var url = baseUrl+"users/"+_.get(state[1],'id','-')+"/alert/property?query=" + JSON.stringify(helpers.clean.junk(payload));

                        axios.get(url).then( function( response ) {

                            $('#user_alerts').html( response.data);
                        }).catch(function (error) {});

                    }

                }
            },
            unsubscribe: {
                alert: function(trigger){
                    var state   =   helpers.user.state.is_logged_in();

                    $(trigger).addClass('loading');

                    var frequency       =   $(trigger).attr("data-_frequency");

                    var payload = {};
                    payload.frequency = [frequency];

                    var modal_id    =   $(trigger).attr("data-_id"),
                    user_id         =   _.get(state[1],'id','');
                    url             =   baseApiUrl + 'user/' + user_id + '/alert/property/' + modal_id + '?query=' + JSON.stringify(helpers.clean.junk(payload));

                    axios.delete(url).then(function (response) {
                        user.property.alert.property.properties();
                    }).catch(function (error) {});

                }
            }
        }
    },
    auth: {
        login: {
            __init: function(trigger){

                $(trigger).addClass('loading');

                //#1    User login API call
                let data = {
                    email: $('#modal_signin').find('input[name=email]').val(),
                    password: $('#modal_signin').find('input[name=password]').val()
                }

                axios.post(baseApiUrl + 'user/auth/login', data).then(function (response) {
                    $('#signin_error').html('');
                    //1# init user dashboard
                    $('.item').tab();
                    $('.ui.form .error.message').hide();

                    //2# prepare object for user data in localstorage
                    var user_data   = user.session.data(response);
                    var data        = helpers.store.stringify(user_data);
                    var encrypt     = helpers.crypto.encrypt(data);

                    //3# write user data to localstorage
                    helpers.store.write('user', encrypt);
                    helpers.store.write('user_o', JSON.stringify(user_data));

                    // Initiate Login State
                    eval('pages.'+page+'.state()');
                    //4# close modal
                    $('.ui.modal.signin_register').modal('hide');
                    $('.ui.modal.mobile_signin_register').modal('hide');

                    helpers.toast.__init( _.get(lang_static, 'toast.success_signed_in') );
                    $(trigger).removeClass('loading');

                }).catch(function (error) {
                    $("#modal_signin").trigger('reset');
                    $('.ui.form .error.message').show();
                    var errs    =   error.response.data.errors;
                    var errors  =   '';
                    for(var i in errs){
                        errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                    }
                    $('#signin_error').html('<div class="ui error message" style="display:block">'+errors+'</div>');
                    $('#signin_error').html(error.response.data.message);
                    $(trigger).removeClass('loading');

                });
            }
        },
        logout: {
            __init: function(type){

                if(type === 'mobile'){
                    $('.user_dashboard').sidebar('toggle');
                } else {
                    $('.right.vertical.sidebar').sidebar('toggle');
                }
                axios.get(baseApiUrl + 'user/auth/logout').then(function (response) {
                    helpers.depopulate.modals();
                }).catch(function (error) {});
                helpers.store.clear();
                eval('pages.'+page+'.state()');

            }
        },
        register: {
            __init: function(trigger){

                $(trigger).addClass('loading');

                let data = {
                    name: $('#modal_register').find('input[name=full_name]').val(),
                    email: $('#modal_register').find('input[name=email]').val(),
                    password: $('#modal_register').find('input[name=password]').val(),
                    password_confirmation: $('#modal_register').find('input[name=confirm_password]').val()
                }

                axios.post(baseApiUrl + 'user/auth/register', data).then(function (response) {

                    $('#register_error').html('');

                    let payload = {
                        email: data.email,
                        password: data.password
                    }

                    axios.post(baseApiUrl + 'user/auth/login', payload).then(function (response) {
                        //1# init user dashboard
                        $('.item').tab();

                        //2# prepare object for user data in localstorage
                        var user_data   = user.session.data(response);
                        var data        = helpers.store.stringify(user_data);
                        var encrypt     = helpers.crypto.encrypt(data);

                        //3# write user data to localstorage
                        helpers.store.write('user', encrypt);
                        helpers.store.write('user_o', JSON.stringify(user_data));

                        // Initiate Login State
                        eval('pages.'+page+'.state()');
                        //4# close modal
                        $('.ui.modal.signin_register').modal('hide');
                        $('.ui.modal.mobile_signin_register').modal('hide');

                        $(trigger).removeClass('loading');
                        helpers.toast.__init( _.get(lang_static, 'toast.success_registered') );


                    }).catch(function (error) {
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                        $(trigger).removeClass('loading');
                    });

                }).catch(function (error) {
                    $("#modal_register").trigger('reset');
                    if(error.response.data.errors){
                        // error.response.data.errors.email
                        var errs    =   error.response.data.errors;
                        var errors  =   '';
                        for(var i in errs){
                            errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                        }
                        $('#register_error').html('<div class="ui error message">'+errors+'</div>');
                        // $('#register_error').html('Please enter all required fields.');
                    }else{
                        $('#register_error').html(error.response.data.message);
                    }
                    $(trigger).removeClass('loading');
                });

            }
        },
        password: {
            forgot: function(trigger){

                $(trigger).addClass('loading');

                var url = baseApiUrl + 'user/auth/forgot';
                let data = {
                    email: $('#modal_forgot_password').find('input[name=email]').val()
                }

                axios.post(url, data).then(function (response) {

                    $('#password_error').html('');

                    //4# close modal
                    $('.ui.modal.signin_register').modal('hide');
                    $('.ui.modal.mobile_signin_register').modal('hide');

                    helpers.toast.__init( _.get(lang_static, 'toast.success_email_sent') );
                    $(trigger).removeClass('loading');

                }).catch(function (error) {
                    $("#modal_forgot_password").trigger('reset');
                    if(error.response.data.errors){
                        $('.ui.form .error.message').show();
                        var errs    =   error.response.data.errors;
                        var errors  =   '';
                        for(var i in errs){
                            errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                        }
                        $('#password_error').html(errors);
                    }else{
                        $('#password_error').html(error.response.data.message);
                    }
                    $(trigger).removeClass('loading');
                });

            }
        },
        social: {
            google: {
                __init: function(){

                    var signinWin;

                    signinWin = window.open(envUrl + "login/google", "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);

                    signinWin.focus();

                    $('.ui.modal.signin_register').modal('setting', 'transition', 'fade up').modal('hide');

                    return false;

                }
            },
            facebook: {
                __init: function(){

                    var signinWin;

                    signinWin = window.open(envUrl + "login/facebook", "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);

                    signinWin.focus();

                    $('.ui.modal.signin_register').modal('setting', 'transition', 'fade up').modal('hide');

                    return false;

                }
            },
            callback: {
                __init	: 		function( data ){

                    axios.post( baseApiUrl + "user/auth/social", {
                        provider       :   data.provider,
                        provider_id    :   data.provider_id,
                        name           :   data.name,
                        email          :   data.email,
                        avatar         :   data.avatar
                    }).then(function ( response ) {

                        //1 init user dashboard
                        $('.item').tab();

                        //2 prepare object for user data in localstorage
                        var user_data   = {
                            email: response.data.contact.email,
                            name: response.data.contact.name,
                            id: response.data._id,
                            login: true
                        }

                        //3 close window
                        window.close();

                        //4 write data
                        window.opener.user.auth.social.opener.__init(user_data);

                    }).catch( function( error ) {
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    });

                }
            },
            opener:{
                __init: function(user_data){

                    var data        = helpers.store.stringify(user_data);
                    var encrypt     = helpers.crypto.encrypt(data);

                    helpers.store.write('user', encrypt);
                    helpers.store.write('user_o', JSON.stringify(user_data));

                    // Initiate Login State
                    eval('pages.'+page+'.state()');

                    helpers.toast.__init( _.get(lang_static, 'toast.success_signed_in') );

                }
            }
        }
    },
    dashboard : {
        trigger: {
            __init      :   function(el){
                $('.user_dashboard').sidebar('toggle');
                $('.user_dashboard').sidebar('setting', 'onShow', function(){
                    $(".pusher").css({height:'100%',overflow:'hidden'});
                });
                $('.user_dashboard').sidebar('setting', 'onHidden', function(){
                    $(".pusher").css({height:'auto',overflow:'visible'});
                });
            }
        },
        password: {
            update: function(trigger){

                $(trigger).addClass('loading');

                var password = $('#change_password_password > input').val(),
                confirmPassword = $("#change_password_password_confirmation > input").val();

                $('#change_password_error').html('');

                var state   =   helpers.user.state.is_logged_in();

                var url = baseApiUrl + 'user/' + _.get(state[1],'id','') + '/auth/change';

                var payload = {
                    old_password: $('#change_password_old_password > input').val(),
                    password: password,
                    password_confirmation: confirmPassword
                }

                axios.put(url, payload).then(function(response){

                    $(trigger).removeClass('loading');
                    $('#change_password_error').html('');
                    $('#change_password_cancel_btn').addClass('hide');
                    $('#change_password_edit_btn').removeClass('hide');
                    $('#change_password_update_btn').addClass('disabled');

                    helpers.toast_dashboard.__init( _.get(lang_static, 'toast.success_password_changed') );

                    $('#change_password_old_password > input').val('');
                    $('#change_password_password > input').val('');
                    $("#change_password_password_confirmation > input").val('');

                }).catch(function (error) {
                    $(trigger).removeClass('loading');

                    if(error.response.data.errors){
                        // error.response.data.errors.email
                        var errs    =   error.response.data.errors;
                        var errors  =   '';
                        for(var i in errs){
                            errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                        }
                        $('#change_password_error').html('<div class="ui error message" style="display:block">'+errors+'</div>');
                    }else{
                        $('#change_password_error').html(error.response.data.message);
                    }
                });

            },
            cancel: function(trigger){
                $('#change_password_error').html('');

                $('#change_password_edit_btn').removeClass('hide');
                $('#change_password_cancel_btn').addClass('hide');
                $('#change_password_cancel_btn').addClass('disabled');
                $('#change_password_update_btn').addClass('disabled');

                $('#change_password_old_password').addClass('disabled');
                $('#change_password_password').addClass('disabled');
                $('#change_password_password_confirmation').addClass('disabled');

                $('#change_password_old_password > input').val('');
                $('#change_password_password > input').val('');
                $("#change_password_password_confirmation > input").val('');
            },
            edit: function(trigger){
                $('#change_password_edit_btn').addClass('hide');
                $('#change_password_cancel_btn').removeClass('hide');
                $('#change_password_cancel_btn').removeClass('disabled');
                $('#change_password_update_btn').removeClass('disabled');

                $('#change_password_old_password').removeClass('disabled');
                $('#change_password_password').removeClass('disabled');
                $('#change_password_password_confirmation').removeClass('disabled');

                //on enter script
                var input = document.getElementById('change_password_password_confirmation');

                input.addEventListener("keyup", function(event) {
                    event.preventDefault();
                    if (event.keyCode === 13) {
                        document.getElementById('change_password_update_btn').click();
                    }
                });

            }
        },
        profile: {
            update: function(trigger){

                $(trigger).addClass('loading');

                var state   =   helpers.user.state.is_logged_in();

                var url = baseApiUrl + 'user/' + _.get(state[1],'id','');

                var payload = {
                    contact:{
                        name: $('#profile_update_name > input').val(),
                        phone: $('#profile_update_phone > input').val(),
                        nationality: $('#profile_update_nationality > input').val()
                    }
                }

                axios.put(url, payload).then(function(response){

                    $(trigger).removeClass('loading');
                    $('#profile_cancel_btn').addClass('hide');
                    $('#profile_edit_btn').removeClass('hide');
                    $('#profile_update_btn').addClass('disabled');

                    //1# prepare object for user data in localstorage
                    var user_data   =   user.session.data(response);
                    var data        =   helpers.store.stringify(user_data);
                    var encrypt     =   helpers.crypto.encrypt(data);

                    //2# write user data to localstorage
                    helpers.store.write('user', encrypt);
                    helpers.store.write('user_o', JSON.stringify(user_data));

                    user.dashboard.profile.cancel(trigger);

                }).catch(function (error) {
                    helpers.toast_dashboard.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');

                    $('#profile_cancel_btn').addClass('hide');
                    $('#profile_edit_btn').removeClass('hide');
                    $('#profile_update_name').addClass('disabled');
                    $('#profile_update_phone').addClass('disabled');
                    $('#profile_update_nationality').addClass('disabled');
                    $('#profile_update_btn').addClass('disabled');

                });

                helpers.toast_dashboard.__init('Profile updated successfully');

            },
            cancel: function(trigger){
                $('#profile_edit_btn').removeClass('hide');
                $('#profile_cancel_btn').addClass('hide');
                $('#profile_cancel_btn').addClass('disabled');
                $('#profile_update_btn').addClass('disabled');

                $('#profile_update_name').addClass('disabled');
                $('#profile_update_phone').addClass('disabled');
                $('#profile_update_nationality').addClass('disabled');
            },
            edit: function(trigger){
                $('#profile_edit_btn').addClass('hide');
                $('#profile_cancel_btn').removeClass('hide');
                $('#profile_cancel_btn').removeClass('disabled');
                $('#profile_update_btn').removeClass('disabled');

                $('#profile_update_name').removeClass('disabled');
                $('#profile_update_phone').removeClass('disabled');
                $('#profile_update_nationality').removeClass('disabled');
            }
        }
    },
    session:{
        data: function(response){
            var user_data   = {
                email: response.data.contact.email,
                name: response.data.contact.name,
                number: response.data.contact.phone,
                nationality: response.data.contact.nationality,
                id: response.data._id,
                login: true
            }
            return user_data;
        }
    }
}

$('.item').tab();

var validate = {
    rules: {
        lead: {
            email: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_email').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            callback: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_callback').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            call: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_call').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            details: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#property_request_details').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            listings: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                        return helpers.phonenumber.iti.isValidNumber();
                    };

                    $('#property_request_listings').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    }
                                    ,
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            project: {
                email:{
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#project_lead').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                        $('#modal_developer_email').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                },
                enquiry:{
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_developer_enquiry').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                },
                                message: {
                                    identifier: 'message',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your message'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                },
                callback:{
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_developer_callback').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                }

            },
            mortgage: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_mortgage').form({
                        fields: {
                            mortgage_user_name: {
                                identifier: 'mortgage_user_name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your first name'
                                    }
                                ]
                            },
                            mortgage_user_name_last: {
                                identifier: 'mortgage_user_name_last',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your last name'
                                    }
                                ]
                            },
                            mortgage_user_email: {
                                identifier: 'mortgage_user_email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            mortgage_user_phone: {
                                identifier: 'mortgage_user_phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            agent: {
                callback: {
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_agent_callback').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                },
                email: {
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_agent_email').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                }
            },
            service:{
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_services_contact').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            general_enquiry:{
              __init: function(){
                  $.fn.form.settings.rules.phone_format_valid = function() {
                    return helpers.phonenumber.iti.isValidNumber();
                  };
                  $('#modal_general_enquiry').form({
                      fields: {
                          name: {
                              identifier: 'name',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please enter your name'
                                  }
                              ]
                          },
                          email: {
                              identifier: 'email',
                              rules: [
                                  {
                                      type   : 'email',
                                      prompt : 'Please enter a valid email'
                                  }
                              ]
                          },
                          type: {
                              identifier: 'type',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please select a property type'
                                  }
                              ]
                          },
                          rent_buy: {
                              identifier: 'rent_buy',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please select Rent or Buy'
                                  }
                              ]
                          },
                          location: {
                              identifier: 'location',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please select a location'
                                  }
                              ]
                          },
                          phone: {
                              identifier: 'phone',
                              rules: [
                                  {
                                      type   : 'phone_format_valid',
                                      prompt : 'Please enter a valid number'
                                  }
                              ]
                          }
                      }
                  });
              }
            }
        },
        auth: {
            login: {
                __init: function(){
                    $('#modal_signin').form({
                        fields: {
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            password: {
                                identifier: 'password',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your password'
                                    },
                                    {
                                        type   : 'minLength[6]',
                                        prompt : 'Your password must be at least {ruleValue} characters'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            register: {
                __init: function(){
                    $('#modal_register').form({
                        fields: {
                            full_name: {
                                identifier: 'full_name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your full name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            password: {
                                identifier: 'password',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your password'
                                    },
                                    {
                                        type   : 'minLength[6]',
                                        prompt : 'Your password must be at least {ruleValue} characters'
                                    }
                                ]
                            },
                            confirm_password: {
                                identifier: 'confirm_password',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please confirm your password'
                                    },
                                    {
                                        type   : 'minLength[6]',
                                        prompt : 'Your password must be at least 6 characters'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            forgot_password: {
                __init: function(){
                    $('#modal_forgot_password').form({
                        fields: {
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            }
                        }
                    });
                }
            }
        }
    }
}

var google_maps = {
    map_id : {},
    map : {},
    center              :   {
		lat				   : 	25.2048,
		lng				   : 	55.2708
	},
    icons : {
        property : envUrl + 'assets/img/map/property-marker.png'
    },
    listings: {
        __init: function(id, coordinates, data){

            map_id = id;

            if(data){
                var property = {
                    title: data.title,
                    type: data.type,
                    rent_buy: data.rent_buy,
                    bed: data.bedroom,
                    bath: data.bathroom,
                    sqft: data.sqft,
                    price: data.price,
                    ref: data.ref,
                    building: data.building,
                    area: data.area,
                    city: data.city,
                    href: data.href
                }
            } else {
                var property = {
                    title: 'Property',
                    rent_buy: 'Property',
                }
            }

            var contentString = '<a href="'+ property.href +'">'+
                '<div id="property_listings_map_hover_card">'+
                '<div>' + property.title + '</div>'+
                '<div>' + property.type + ' for ' + property.rent_buy + '</div>'+
                '<div>' + property.bed + ' bed, ' + property.bath + ' bath, ' + property.sqft + ' sqft' + '</div>'+
                '<div><span>' + 'AED ' + '</span><span>' + property.price + '</span>' + ' | REF: ' + property.ref + '</div>'+
                '<div>' + property.building + ' ' + property.area + ', ' + property.city + '</div>'+
                '</div>'+
                '</a>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });

            var lat = coordinates ? coordinates.lat : 25.2048,
                lng = coordinates ? coordinates.lng : 55.2708;

            var mapOptions = {
                zoom: coordinates ? 15 : 12,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = google_maps.map = new google.maps.Map(document.getElementById(id), mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: coordinates ? google_maps.icons.property : '',
                map: google_maps.map
            });

            marker.addListener('click', function() {
             infowindow.open(map, marker);
           });

        }
    },
    events: {
        bind : {
            mouseenter : function(class_name){

                $(class_name).mouseenter(function(trigger){
                    $('.no_loc').hide();

                    var coordinates = {
                        lat: Number($(this).attr('data-coordinates_lat')),
                        lng: Number($(this).attr('data-coordinates_lng'))
                    }

                    var data = {
                        title: $(this).attr('data-title'),
                        type: $(this).attr('data-type'),
                        rent_buy: $(this).attr('data-rent_buy'),
                        bedroom: $(this).attr('data-bedroom'),
                        bathroom: $(this).attr('data-bathroom'),
                        sqft: $(this).attr('data-sqft'),
                        price: $(this).attr('data-price'),
                        ref: $(this).attr('data-ref'),
                        building: $(this).attr('data-building'),
                        area: $(this).attr('data-area'),
                        city: $(this).attr('data-city'),
                        href: $(this).attr('data-href')
                    }

                    var agency  =   $(this).attr('data-agency');

                    // No location overlay
                    if(coordinates.lat === 0 || coordinates.lng === 0){
                        $('.no_loc').show();
                        $('.no_loc.text').html(agency + ' has not provided a location for this property');
                    } else {
                        google_maps.listings.__init(map_id, coordinates, data);
                    }

                });

            }
        },
        unbind : {
            mouseenter : function(class_name){
                $(class_name).unbind('mouseenter mouseleave');
            }
        }
    }
}

var pages = {
    landing: {
        // Used to stop the rent buy trigger from launching on page load
        first: 2,
        property: {
            call: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Api call
                    */

                    var user = JSON.parse(helpers.store.read('user_o'));

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_call_phone');
                    }, 500);

                    var agent_name  = _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : $(data_trigger).parent().attr("data-agent_contact_name");
                        agent_name  = !_.isEmpty(agent_name) ? agent_name : 'Agent name not available';
                    var agent_phone = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'agent.contact.phone') ? _.get(data_trigger, 'agent.contact.phone') : ( $(data_trigger).parent().attr("data-agent_contact_phone") ? $(data_trigger).parent().attr("data-agent_contact_phone") : 'Phone not available') );

                    var agency_name = _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : $(data_trigger).parent().attr("data-agency_contact_name");
                        agency_name = !_.isEmpty(agency_name) ? agency_name : 'Agency name not available';

                    var property_ref_no = _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : $(data_trigger).parent().attr("data-ref_no");
                        property_ref_no = !_.isEmpty(property_ref_no) ? property_ref_no : 'Reference number not available';

                    var property_id = _.has(data_trigger, '_id') ? _.get(data_trigger, '_id') : $(data_trigger).parent().attr("data-_id");
                        property_id = !_.isEmpty(property_id) ? property_id : 'ID not available';

                    //1
                    var data = {
                        agent: {
                            name: agent_name,
                            phone: agent_phone
                        },
                        agency: {
                            name: agency_name
                        },
                        property: {
                            reference: property_ref_no,
                            _id: property_id
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        source: 'desktop'
                    }

                    if(!data.agent.phone.includes("+") && !_.startsWith(data.agent.phone,'0')){
                        $('#modal_property_call_agent_contact_phone').html('+' + data.agent.phone);
                    }
                    else if(_.startsWith(data.agent.phone,'0')){
                        $('#modal_property_call_agent_contact_phone').html(data.agent.phone.replace('0', '+971'));
                    }else {
                        $('#modal_property_call_agent_contact_phone').html(data.agent.phone);
                    }

                    //2
                    $('#modal_property_call_agent_name').html(data.agent.name);
                    $('#modal_property_call_agency_name').html(data.agency.name);
                    $('#modal_property_call_ref_no').html(data.property.reference);
                    $('#modal_property_call_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_call_data').attr('data-_id', data.property._id);
                    $('#modal_property_call_data').attr('data-subsource', data.subsource);

                    //3
                    $('.ui.modal.property_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    pages.signup_guide_slider.__init( 'call-slider' );

                    //4
                    var payload = {
                        action: 'call',
                        source: 'consumer',
                        subsource: 'desktop',
                        message: _.has(lang_static, 'modals.property.call.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.call.payload_msg'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.',
                        property: {
                            _id: data.property._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //storing the payload for call buton api call
                    lead.property.store.save = payload;

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','call_click', data_trigger);
                    other.datalayer('property','call_submit', data_trigger);

                    lead.property.call.__init();

                }
            },
            email: {
                __init: function(data_trigger) {
                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_email_phone');
                    }, 500);

                    var agent_name  = _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : $(data_trigger).parent().attr("data-agent_contact_name");
                        agent_name  = !_.isEmpty(agent_name) ? agent_name : 'Agent name not available';

                    var agent_picture  = _.has(data_trigger, 'agent.contact.picture') ? _.get(data_trigger, 'agent.contact.picture') : $(data_trigger).parent().attr("data-agent_contact_picture");
                        agent_picture  = !_.isEmpty(agent_picture) ? agent_picture : '';

                    var agency_name = _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : $(data_trigger).parent().attr("data-agency_contact_name");
                        agency_name = !_.isEmpty(agency_name) ? agency_name : 'Agency name not available';

                    var property_ref_no = _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : $(data_trigger).parent().attr("data-ref_no");
                        property_ref_no = !_.isEmpty(property_ref_no) ? property_ref_no : 'Reference number not available';

                    var property_id = _.has(data_trigger, '_id') ? _.get(data_trigger, '_id') : $(data_trigger).parent().attr("data-_id");
                        property_id = !_.isEmpty(property_id) ? property_id : 'ID not available';

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: agent_name,
                            picture: agent_picture
                        },
                        agency: {
                            name: agency_name
                        },
                        property: {
                            reference: property_ref_no,
                            _id: property_id
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_email_agent_img');


                    //2
                    $('#modal_property_email_agent_name').html(data.agent.name);
                    $('#modal_property_email_agency_name').html(data.agency.name);
                    $('#modal_property_email_textarea').html(_.has(lang_static, 'modals.property.email.message') ? helpers.trans_value(_.get(lang_static, 'modals.property.email.message'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.')
                    $('#modal_property_email_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_email_data').attr('data-_id', data.property._id);
                    $('#modal_property_email_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.property_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    pages.signup_guide_slider.__init( 'email-slider' );

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;

                    other.datalayer('property','email_click', data_trigger);

                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_callback_phone');
                    }, 500);

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : 'Agent name not available',
                            picture: _.has(data_trigger, 'agent.contact.picture') ? _.get(data_trigger, 'agent.contact.picture') : ''
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : 'Agency name not available'
                        },
                        property: {
                            reference: _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : 'reference number not available',
                            _id: _.get(data_trigger, '_id')
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_callback_agent_img');

                    //2
                    $('#modal_property_callback_agent_name').html(data.agent.name);
                    $('#modal_property_callback_agency_name').html(data.agency.name);
                    $('#modal_property_callback_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_callback_data').attr('data-_id', data.property._id);
                    $('#modal_property_callback_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.property_callback').modal('setting', 'transition', 'vertical flip').modal('show');

                    pages.signup_guide_slider.__init( 'callback-slider' );

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','callback_click', data_trigger);

                }
            },
            whatsapp: {
                __init: function(data_trigger) {

                    var wapp_phone_number = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'agent.contact.phone') ? _.get(data_trigger, 'agent.contact.phone') :'Phone not available');

                    if (wapp_phone_number == 'Phone not available') {
                        alert('Phone number not available');
                    } else {

                        var agency_email  = _.get(data_trigger, 'agent.agency.contact.email');
                        //NOTE TEMPORARY CHANGE TO TRACK WHATSAPP (REMOVE AFTER TRIAL COMPLETED)
                        wapp_phone_number = '+971529887539'

                        var user = JSON.parse(helpers.store.read('user_o'));

                        //1
                        var data = {
                            agent: {
                                name: _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : 'Agent name not available',
                                phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : 'Phone not available'
                            },
                            agency: {
                                name:  _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : 'Agency name not available'
                            },
                            property: {
                                reference: _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : 'reference number not available',
                                _id: _.get(data_trigger, '_id')
                            },
                            user: {
                                name: user ? user.name : 'anonymous',
                                email: user ? user.email : 'anonymous@domain.com',
                                phone: user && _.has(user, 'user.phone') ? user.phone : ''
                            }
                        }

                        var payload = {
                            action: 'whatsapp',
                            source: 'consumer',
                            subsource: 'desktop',
                            message: _.has(lang_static, 'modals.property.whatsapp.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.whatsapp.payload_msg'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.',
                            property: {
                                _id: data.property.id
                            },
                            user: {
                                contact: {
                                    name: data.user.name,
                                    email: data.user.email,
                                    phone: data.user.phone
                                }
                            }
                        }

                        if (payload.user.contact.phone.length === 0) {
                            delete payload.user.contact.phone
                        }

                        //whatsapp lead create api
                        lead.property.whatsapp.__init(payload);

                        //gtm push on submit
                        helpers.store.temp_data.write = data_trigger;
                        other.datalayer('property','whatsapp_submit', data_trigger);

                        window.open(
                            'whatsapp://send?text='+encodeURIComponent(payload.message)+'&phone=' + wapp_phone_number,
                            '_blank'
                        );
                    }
                }
            }
        },
        __init: function() {
            page = 'landing';

            /**
            * 2.   Initial Blog posts are loaded
            * 3.   Change Featured Properties based on rent/buy dropdown
            * 4.   Change Featured Properties based on area dropdown
            * 5.   Fire a search
            * 6.   Newsletter
            * 7.   Load images of featured properties
            * 8.   Ideal Property Popup
            */
            // 2.
            other.blog.listings();

            // 3.
            pages.landing.dropdown.__init();

            // 7.
            helpers.lazy_load();

            // 4.   State
            pages.landing.state();

            // 8.   State
            pages.landing.ideal_property.__init();

            $('#rentbuy_area item').tab({history:false});
            $('#popular-buy item').tab({history:false});
            $('#popular-rent item').tab({history:false});

            $('.popup-video-youtube').magnificPopup({
                type: 'iframe'
            });

        },
        state: function() {
            var state = helpers.user.state.is_logged_in();
            if (state[0]) {
                // Landing Login State
                /**
                * 1.   Header shows 'username'
                * 2.   Lead modals are populated with user details
                */
                // 1.
                $('.state_li').show();
                $('.state_lo').hide();
                // 2.
                helpers.populate.modals(state[1]);
                helpers.populate.header(state[1]);
                $('.item').tab();
            } else {
                $('.state_lo').show();
                $('.state_li').hide();
                helpers.depopulate.modals();
                helpers.depopulate.header();
            }
        },
        subscribe_for_newsletter: function(el) {
            user.newsletter.__init(el);
        },
        change_featured_properties: function() {

            /**
            * 1.   Onchange    =   rent/buy
            * 2.   Onchange    =   area
            */

            // Changing the title

            featured_stub = "Featured :residential_commercial: Properties for :rent_buy: :area:";
            featured_stub = featured_stub.replace( ':area:', '');

            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'residential_commercial', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'residential_commercial', []))) {

                residential_commercial  =   _.get(search.property.regular.store_variables, 'residential_commercial.0').replace('-', ' ');
                featured_stub = featured_stub.replace( ':residential_commercial:', residential_commercial );
            }

            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'city', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'city', []))) {
                featured_stub = featured_stub.replace( ':area:', ' in ' + _.get(search.property.regular.store_variables, 'city.0') );
            }

            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'area', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'area', []))) {
                featured_stub = featured_stub.replace( ':area:', ' in ' + _.get(search.property.regular.store_variables, 'area.0') );
            }
            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'building', undefined)) &&
                !_.isEmpty(_.get(search.property.regular.store_variables, 'building', []))) {
                featured_stub = featured_stub.replace( ':area:', ' in ' + _.get(search.property.regular.store_variables, 'building.0') );
            }
            // Rent Buy
            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'rent_buy', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'rent_buy', []))) {
                if (_.includes(_.get(search.property.regular.store_variables, 'rent_buy.0'), 'Commercial')) {
                    var parts = _.get(search.property.regular.store_variables, 'rent_buy.0').split(/-(.+)/);
                    rent_buy  = _.get(parts, '1', 'Sale').replace(/-/g, ' ');
                    residential_commercial  =   _.get(parts, '0', 'Commercial');
                    featured_stub = featured_stub.replace( ':rent_buy:', rent_buy );
                    featured_stub = featured_stub.replace( ':residential_commercial:', residential_commercial );
                } else if (!_.isEmpty(_.get(search.property.regular.store_variables, 'rent_buy.0'))) {
                    rent_buy  = _.get(search.property.regular.store_variables, 'rent_buy.0').replace(/-/g, ' ');
                    residential_commercial  =   'Residential';
                    featured_stub = featured_stub.replace( ':rent_buy:', rent_buy);
                    featured_stub = featured_stub.replace( ':residential_commercial:', residential_commercial);
                }
            }
            var url = search.property.regular.construct.url(search.property.regular.store_variables);
            last_segment = url.split( '/' ).pop();
            featured_url = ( !_.startsWith( rent_buy, 'Short' ) ) ? url.replace( last_segment, 'featured-'+last_segment ) : url;

            featured_stub  =  _.has(lang_static, 'featured.'+_.toLower(residential_commercial)+'.'+_.toLower(rent_buy)+'.title') ? _.get(lang_static, 'featured.'+_.toLower(residential_commercial)+'.'+_.toLower(rent_buy)+'.title') : featured_stub;
            featured_stub = '<a href=" ' + featured_url + '" style="color:black;">' + featured_stub + '</a>';
            $('.f_properties').html(featured_stub);

            var query = {};
            query.area = _.get(search.property.regular.store_variables, 'area', []);
            query.city = _.get(search.property.regular.store_variables, 'city', []);
            query.building = _.get(search.property.regular.store_variables, 'building', []);
            query.rent_buy = _.get(search.property.regular.store_variables, 'rent_buy', 'Sale');
            query.size = 5;
            query.featured = {};
            query.featured.status = true;
            search.property.listings(query, 'featured_properties');
        },
        search: {
            toggle: function(el) {
                // Fields drawer
                $('.advanced_search').toggleClass('search_active');
                // Text editing
                if ($('.advanced_search').hasClass('search_active')) {
                    $(el).find('a').html('- Advanced Search');
                } else {
                    $(el).find('a').html('+ Advanced Search');

                }
            }
        },
        dropdown: {
            __init: function() {

                $('#property_rent_buy').dropdown({
                    values: pages.landing.dropdown.data.rent_buy,
                    placeholder: _.has(lang_static, 'global.rent_buy') ? _.get(lang_static, 'global.rent_buy') : 'Rent or Buy',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.rent_buy = [];
                            search.property.regular.store_variables.rent_buy.push(value);
                        }

                        if ($('#landing_search').length) {
                            pages.landing.first--;

                            if (!_.isEmpty(_.get(search.property.regular.store_variables, 'rent_buy', [])) && pages.landing.first < 2) {
                                pages.landing.change_featured_properties();
                            }
                        }

                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                        if(!value){
                            $(this).dropdown('clear');
                        }
                    }
                });

                var auto = document.getElementById('property_suburb');
                var auto_hidden = document.getElementById('property_suburb_hidden');
                var auto_hidden_b = document.getElementById('property_suburb_hidden_b');

                new Awesomplete(auto, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    multiple: true,
                    autoFirst: true,
                    filter: function(text, input) {
                        return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                    },
                    item: function(text, input) {
                        return Awesomplete.ITEM(text, input.match(/[^,]*$/)[0]);
                    },
                    replace: function (text) {

                        var before = this.input.value.match(/^.+,\s*|/)[0];
                        var before_hidden = '';

                        selectedLoc = _.filter(searchJson.property.location, function(loc) {
                            return loc.value === text.value;
                        });

                        if(!_.includes(selectedLoc,'0.label')){
                            search.property.regular.store_variables.suburb.label = before + _.get(selectedLoc,'0.label') + ', ';
                        }

                        items = search.property.regular.store_variables.suburb.label.split(', ');
                        items.length = (items.length)-1;
                        $.each(items, function( index, value ){
                            selectedLoc = _.filter(searchJson.property.location, function(loc) {
                                return loc.label === value;
                            });
                            before_hidden += _.get(selectedLoc,'0.value') + ', ';
                        });

                        search.property.regular.store_variables.suburb.value = before_hidden;

                        auto_hidden.value = search.property.regular.store_variables.suburb.value;
                        auto_hidden_b.value = search.property.regular.store_variables.suburb.label;
                        this.input.value = search.property.regular.store_variables.suburb.label;

                    }
                });

                $('#property_type').dropdown({
                    values: pages.landing.dropdown.data.property_type.residential.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.type') ? _.get(lang_static, 'form.placeholders.type') : 'Property Type',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.type = [];
                            search.property.regular.store_variables.type.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.type = [];
                        }
                    }
                });

                $('#property_bedroom_min').dropdown({
                    values: pages.landing.dropdown.data.bedroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bed') ? _.get(lang_static, 'form.placeholders.min.bed') : 'Min Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bedroom.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bedroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bedroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_bedroom_max', pages.landing.dropdown.data.bedroom_max, 'bedroom');
                        }
                    }
                });

                $('#property_bedroom_max').dropdown({
                    values: pages.landing.dropdown.data.bedroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bed') ? _.get(lang_static, 'form.placeholders.max.bed') : 'Max Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                            (search.property.regular.store_variables.bedroom.max != parseInt(value)) &&
                            !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.bedroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bedroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_bedroom_min', pages.landing.dropdown.data.bedroom_min, 'bedroom');
                        }
                    }
                });

                $('#property_bathroom_min').dropdown({
                    values: pages.landing.dropdown.data.bathroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bath') ? _.get(lang_static, 'form.placeholders.min.bath') : 'Min Bath',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bathroom.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bathroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bathroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_bathroom_max', pages.landing.dropdown.data.bathroom_max, 'bathroom');
                        }
                    }
                });

                $('#property_bathroom_max').dropdown({
                    values: pages.landing.dropdown.data.bathroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bath') ? _.get(lang_static, 'form.placeholders.max.bath') : 'Max Bath',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bathroom.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bathroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bathroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_bathroom_min', pages.landing.dropdown.data.bathroom_min, 'bathroom');
                        }
                    }
                });

                $('#property_area_min').dropdown({
                    values: pages.landing.dropdown.data.area_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.area') ? _.get(lang_static, 'form.placeholders.min.area') : 'Min Area',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.dimension.builtup_area.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.dimension.builtup_area.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.dimension.builtup_area.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_area_max', pages.landing.dropdown.data.area_max, 'area');
                        }
                    }
                });

                $('#property_area_max').dropdown({
                    values: pages.landing.dropdown.data.area_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.area') ? _.get(lang_static, 'form.placeholders.max.area') : 'Max Area',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.dimension.builtup_area.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.dimension.builtup_area.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.dimension.builtup_area.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_area_min', pages.landing.dropdown.data.area_min, 'area');
                        }
                    }
                });

                $('#property_price_min').dropdown({
                    values: pages.landing.dropdown.data.price_sale_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.price.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_price_max', rent_type, 'price');
                        }

                    }
                });

                $('#property_price_max').dropdown({
                    values: pages.landing.dropdown.data.price_sale_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.price.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.price.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_price_min', rent_type, 'price');
                        }
                    }
                });

                $('#property_furnishing').dropdown({
                    values: pages.landing.dropdown.data.furnishing,
                    placeholder: _.has(lang_static, 'form.placeholders.furnishing') ? _.get(lang_static, 'form.placeholders.furnishing') : 'Furnishing',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.property.regular.store_variables.query_string.furnishing =   parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.query_string.furnishing =   undefined;
                        }
                    }
                });

                $('#completion_status').dropdown({
                    values: pages.landing.dropdown.data.completion_status,
                    placeholder: _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.property.regular.store_variables.query_string.status = [];
                            search.property.regular.store_variables.query_string.status.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.query_string.status = [];
                        }
                    }
                });

            },
            callback: {
                from: function(value, id, path, type) {

                    $(id).dropdown('clear');

                    var clone = path,
                    array = clone.slice(0);

                    array.splice(0, helpers.indexof(array, "value", parseInt(value)));

                    $(id).empty();

                    if(value != 0){
                        array.unshift({name: $(id).dropdown('get text'), value: ''});
                    }

                    $.each(array, function(i, item) {
                        $(id).append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    if (!(($(id).dropdown('get text') == 'Max Bed') || ($(id).dropdown('get text') == 'Max Bath') || ($(id).dropdown('get text') == 'Max Area') || ($(id).dropdown('get text') == 'Max Price'))) {

                        if (_.includes($(id).dropdown('get text'), 'Sq. ft')) {
                            $(id).dropdown('set selected', $(id).dropdown('get text').match(/\d+/)[0]);
                        } else if (_.includes($(id).dropdown('get text'), 'Beds') || _.includes($(id).dropdown('get text'), 'Bed')) {
                            if ($(id).dropdown('get text') === 'Studio') {
                                $(id).dropdown('set selected', 0);
                            } else {
                                $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                            }
                        } else if (_.includes($(id).dropdown('get text'), 'Baths')) {
                            $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                        } else if (_.includes($(id).dropdown('get text'), 'AED')) {
                            var price_with_comma = $(id).dropdown('get text').substr($(id).dropdown('get text').indexOf(" ") + 1);
                            var remove_comma = parseFloat(price_with_comma.replace(/,/g, ''))
                            $(id).dropdown('set selected', remove_comma);
                        }

                    } else {
                        $(id).dropdown('clear');
                    }

                },
                to: function(value, id, path, type) {

                    //
                    // var clone = path,
                    //     array = clone.slice(0);
                    //
                    // array.splice(helpers.indexof(array, "value", parseInt(value)) + 1, array.length);
                    //
                    // $(id).empty();
                    //
                    // $.each(array, function(i, item) {
                    //     $(id).append($('<option>', {
                    //         value: item.value,
                    //         text: item.name
                    //     }))
                    // })
                    //
                    // if (!(($(id).dropdown('get text') == 'Min Bed') || ($(id).dropdown('get text') == 'Min Bath') || ($(id).dropdown('get text') == 'Min Area') || ($(id).dropdown('get text') == 'Min Price'))) {
                    //
                    //     if (_.includes($(id).dropdown('get text'), 'Sq. ft')) {
                    //         $(id).dropdown('set selected', $(id).dropdown('get text').match(/\d+/)[0]);
                    //     } else if (_.includes($(id).dropdown('get text'), 'Beds') || _.includes($(id).dropdown('get text'), 'Bed')) {
                    //         if ($(id).dropdown('get text') === 'Studio') {
                    //             $(id).dropdown('set selected', 0);
                    //         } else {
                    //             $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                    //         }
                    //     } else if (_.includes($(id).dropdown('get text'), 'Baths')) {
                    //         $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                    //     } else if (_.includes($(id).dropdown('get text'), 'AED')) {
                    //         var price_with_comma = $(id).dropdown('get text').substr($(id).dropdown('get text').indexOf(" ") + 1);
                    //         var remove_comma = parseFloat(price_with_comma.replace(/,/g, ''))
                    //         $(id).dropdown('set selected', remove_comma);
                    //     }
                    //
                    // } else {
                    //     $(id).dropdown('clear');
                    // }
                },
                rent_buy: function(value) {

                    if (value === 'Commercial-Rent' || value === 'Commercial-Sale') {
                        $("#property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing").dropdown().addClass("disabled");

                        $('#property_type').empty();

                        if (value === 'Commercial-Rent') {
                            $.each(pages.landing.dropdown.data.property_type.commercial.rent, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        } else {
                            $.each(pages.landing.dropdown.data.property_type.commercial.sale, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        }

                        $('#property_type, #property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing').dropdown('clear');

                    } else {
                        $("#property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing").dropdown().removeClass("disabled");

                        $('#property_type').empty();

                        if (value === 'Rent') {
                            $.each(pages.landing.dropdown.data.property_type.residential.rent, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        } else {
                            $.each(pages.landing.dropdown.data.property_type.residential.sale, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        }

                        $('#property_type, #property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing').dropdown('clear');

                    }

                    if (value === 'Sale' || value === 'Commercial-Sale') {

                        $('#property_price_min, #property_price_max').empty();

                        $.each(pages.landing.dropdown.data.price_sale_min, function(i, item) {
                            $('#property_price_min').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $.each(pages.landing.dropdown.data.price_sale_max, function(i, item) {
                            $('#property_price_max').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $('#property_price_min, #property_price_max').dropdown('clear');

                    } else if (value === 'Rent' || value === 'Commercial-Rent' || value === 'Short-term-Rent') {

                        $('#property_price_min, #property_price_max').empty();

                        $.each(pages.landing.dropdown.data.price_rent_min, function(i, item) {
                            $('#property_price_min').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $.each(pages.landing.dropdown.data.price_rent_max, function(i, item) {
                            $('#property_price_max').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $('#property_price_min, #property_price_max').dropdown('clear');

                    }
                }
            },
            data: helpers.unshift(searchJson.property)
        },
        ideal_property: {
            __init: function() {

                helpers.phonenumber.init('ideal_property_phone');

                var auto = document.getElementById('ideal_property_location');

                new Awesomplete(auto, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    autoFirst: true
                });

                var mouseX = 0;
                var mouseY = 0;

                document.addEventListener("mousemove", function(e) {
                    mouseX = e.clientX;
                    mouseY = e.clientY;
                });

                //$('.ui.modal.ideal_property').modal('setting', 'transition', 'vertical flip').modal('show');

                $(document).mouseleave(function () {
                    if ( (mouseY < 100) && (localStorage.idealProperty === undefined) ) {
                        localStorage.idealProperty = 'yes';
                        $('.ui.modal.ideal_property').modal('setting', 'transition', 'vertical flip').modal('show');
                    }
                });

            }
        },
        general_enquiry: {
            __init: function(){

                var location_field = document.getElementById('ge_location'),
                    user = JSON.parse(helpers.store.read('user_o'));

                new Awesomplete(location_field, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    autoFirst: true
                });

                $('div.ge.dropdown').dropdown();

                helpers.phonenumber.init('ge_phone');

                var data = {
                    name: _.has(user, 'name') ? user.name : '',
                    email: _.has(user, 'email') ? user.email : '',
                }

                $('#ge_name').val(data.name);
                $('#ge_email').val(data.email);

                validate.rules.lead.general_enquiry.__init();
            }
          },
        quick_access:   {
            __init      :   function(el){

                search.property.regular.store_variables.rent_buy = [ $(el).data('rent_buy') ];
                search.property.regular.store_variables.type = [ $(el).data('type') ];
                var price_min = $(el).data('min'),
                    price_max = $(el).data('max');
                if( price_min !== '' ) {
                    search.property.regular.store_variables.price.min = price_min;
                }
                if( price_max !== '' ) {
                    search.property.regular.store_variables.price.max = price_max;
                }

                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

                var qrsObj_prev     =   {};
                if(_.has(qrsObj_url_prev,'agency_id')){
                    qrsObj_prev.agency_id   =   qrsObj_url_prev.agency_id;
                }
                if(_.has(qrsObj_url_prev,'sort')){
                    qrsObj_prev.sort        =   qrsObj_url_prev.sort;
                }

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );

                window.location.href =  url.toString();

            }
        }
    },
    property: {
        listings: {
            map: {
                __init: function() {

                    google_maps.listings.__init('property_listings_map');

                    $('.ui.sticky.map, #property_list_view, #property_grid_view').fadeIn();
                    $('.property_listings_ad, #property_map_view').hide();

                    $('#property_card_grid').attr('id', 'property_card_list');
                    $("#property_card_list").attr('class', 'eleven wide column');
                    $('#property_card_list > .ui.two.column.grid').attr('class', 'ui one column grid');
                    $('#property_card_list').siblings('.five.wide.column').attr('class', 'five wide column');

                    //Sticky map in listings page
                    setTimeout(function() {
                        $('.ui.sticky.map').sticky({
                            context: '#pages_property_listings'
                        });
                    }, 1000);

                    //Bind mouseenter/hover event for google maps trigger on card
                    google_maps.events.bind.mouseenter('.ui.segment');

                    helpers.store.write('card_view', helpers.store.stringify('map'));
                }
            },
            grid: {
                __init: function() {
                    $('.ui.sticky.map, #property_grid_view').hide();
                    $('#property_card_list').attr('id', 'property_card_grid');
                    $('#property_card_grid, .property_listings_ad, #property_list_view, #property_map_view').fadeIn();
                    $("#property_card_grid").attr('class', 'eleven wide column');
                    $('#property_card_grid > .ui.one.column.grid').attr('class', 'ui two column grid');
                    $('#property_card_grid').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter/hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.ui.segment');

                    helpers.store.write('card_view', helpers.store.stringify('grid'));

                    // if($('.p_c:nth-child(5) > div > div > div > div.jss263 > span').html() === 'featured'){
                    //     $(".p_c:nth-child(5)").hide();
                    // }


                }
            },
            list: {
                __init: function() {
                    $('.ui.sticky.map, #property_list_view').hide();
                    $('#property_card_grid').attr('id', 'property_card_list');
                    $('#property_card_list, .property_listings_ad, #property_grid_view, #property_map_view').fadeIn();
                    $("#property_card_list").attr('class', 'eleven wide column');
                    $('#property_card_list > .ui.two.column.grid').attr('class', 'ui one column grid');
                    $('#property_card_list').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter/hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.ui.segment');

                    helpers.store.write('card_view', helpers.store.stringify('list'));
                }
            },
            __init: function() {
                page = 'landing';
                pages.landing.state();

                var view = JSON.parse(localStorage.getItem("card_view"));

                if(view === 'map'){
                    pages.property.listings.map.__init();
                } else if(view === 'grid'){
                    pages.property.listings.grid.__init();
                }

                pages.landing.dropdown.__init();

                //pagination init
                helpers.pagination.__init();

                //sorting init
                helpers.sort.__init();

                // Lazy Load
                helpers.lazy_load();

                // $('.property_listings_ad').sticky();
                $('.gnral-stcky').sticky({
                    bottomOffset : 14,
                    context: '#property_card_list'
                }
                );

                $('.ui.grid aside').sticky();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 100);

                if(document.getElementById("property_request_phone")){
                    setTimeout(function() {
                        helpers.phonenumber.init('property_request_phone');
                    }, 500);
                }

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                other.update_currency();


            }
        },
        details: {
            slider: {
                __init: function() {

                    $('.slider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerPadding: 0,
                        dots: false,
                        infinite: true,
                        appendDots: '.slider-nav',
                        appendArrows: '.slider-arrows',
                        nextArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--left"><i class="chevron right icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--right"><i class="chevron right icon"></i></a>',
                        prevArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--right"><i class="chevron left icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--left"><i class="chevron left icon"></i></a>'
                    });

                    setTimeout(function() {
                        helpers.phonenumber.init('property_details_email_phone');
                    }, 500);

                }
            },
            map: {
                __init: function(lat, lng) {
                    var center = {};

                    center.lat = Number(lat);
                    center.lng = Number(lng);

                    // No location overlay
                    if (center.lat === 0 || center.lng === 0) {
                        $('.no_loc').show();
                    } else {
                        g_map.map_container = 'property_details_map';
                        g_map.center = center;
                        g_map.map.invoke();
                        g_map.marker();
                        $('.ui.checkbox').checkbox();
                    }
                    return;
                }
            },
            info: {
                __init: function() {

                    var suburb = $('#pages_property_details_data').attr('data-_area');

                    if(suburb === 'jumeirah-lake-towers(-j-l-t)'){
                        suburb = 'jumeirah-lake-towers';
                    }

                    var url = envUrl + 'en/blog/guide/' + suburb;

                    axios.get(url).then(function(response) {
                        var data;

                        if (response.data.length < 5) {
                            $('.local_info_show').addClass('hide');
                        } else {
                            $('.local_info_show').removeClass('hide');
                            data = response.data;
                            $('#property_details_local_info').html(data);
                        }

                    }).catch(function(error) {});

                }
            },
            mortgage: {
                __init: function(trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('mortgage_user_phone');
                    }, 500);

                    $('.ui.modal.property_mortgage').modal('setting', 'transition', 'fade up').modal('show');

                    var years = Number($('#emi_years').val().split(',').join('')),
                    down_payment = Number($('#emi_down_payment').val().split(',').join('')),
                    interest = Number($('#emi_interest').val().split(',').join('')),
                    principal = $('#purchase_price').html().replace(/,/g, ''),
                    finance_amount = principal - down_payment;

                    //Computing EMI
                    var i = interest / 100 / 12,
                    n = years * 12,
                    monthly = (principal - down_payment) * (1 - i) * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1),
                    total_amount = monthly * years * 12;

                    $('#project_loan_amount').html($('#purchase_price').html());
                    $('#project_emi_monthly').text(Math.ceil(monthly).toLocaleString('en'));
                    $('#project_emi_down_payment').text('AED ' + Math.ceil(down_payment).toLocaleString('en'));
                    $('#project_emi_total_amount').text('AED ' + Math.ceil(total_amount).toLocaleString('en'));
                    $('#project_emi_total_interest' ).text( 'AED ' + Math.ceil( total_amount - principal ).toLocaleString('en') );
                    $('#project_emi_term').text(years);

                    //finance amount
                    $('#project_emi_finance_amount').text('AED ' + finance_amount.toLocaleString('en'));

                    $('#mortgage_lead').attr('data-model', $(trigger).attr('data-model'));
                }
            },
            dewa: {
                tabs: {
                    energy: {
                        __init: function(){
                            var monthly = parseInt($('#property_monthly_value').html()),
                            elec_cost   =   parseInt($('#monthly_cost_energy_electricity').val()),
                            gas_cost    =   parseInt($('#monthly_cost_energy_gas').val());

                            $('#monthly_cost_energy_electricity, #monthly_cost_energy_gas').change(function(trigger) {

                                var energy = elec_cost + gas_cost,
                                minus = monthly - energy;

                                var new_monthly = minus + parseInt($('#monthly_cost_energy_electricity').val()) + parseInt($('#monthly_cost_energy_gas').val());

                                $('#property_monthly_value').html(Math.round(new_monthly));
                            });

                        }
                    },
                    water: {
                        __init: function(){
                            var monthly = parseInt($('#property_monthly_value').html()),
                            water_cost   =   parseInt($('#monthly_cost_water').val());

                            $('#monthly_cost_water').change(function() {

                                var minus = monthly - water_cost;

                                var new_monthly = minus + parseInt($('#monthly_cost_water').val());

                                $('#property_monthly_value').html(Math.round(new_monthly));

                            });

                        }
                    }
                },
                sale : {
                    __init: function(){

                        var years = Number($('#emi_years').val().split(',').join('')),
                        down_payment = Number($('#emi_deposit').val().split(',').join('')),
                        interest = Number($('#emi_interest').val().split(',').join('')),
                        principal = $('#purchase_price').html().replace(/,/g, ''),
                        energy_electricity = !_.isEmpty($('#monthly_cost_energy_electricity').val()) ? parseInt($('#monthly_cost_energy_electricity').val()) : 0,
                        energy_gas = !_.isEmpty($('#monthly_cost_energy_gas').val()) ? parseInt($('#monthly_cost_energy_gas').val()) : 0,
                        energy = energy_electricity + energy_gas,
                        water = parseInt($('#monthly_cost_water').val());

                        //Computing EMI
                        var i = interest / 100 / 12,
                        n = years * 12,
                        monthly = (principal - down_payment) * (1 - i) * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1) + energy + water,
                        total_amount = monthly * years * 12;

                        $('#project_loan_amount').html($('#purchase_price').html());
                        $('#project_emi_monthly').text(Math.ceil(monthly).toLocaleString('en'));
                        $('#project_emi_down_payment').text('AED ' + Math.ceil(down_payment).toLocaleString('en'));
                        $('#project_emi_total_amount').text('AED ' + Math.ceil(total_amount).toLocaleString('en'));
                        $('#project_emi_total_interest' ).text( 'AED ' + Math.ceil( total_amount - principal ).toLocaleString('en') );
                        $('#project_emi_term').text(years);

                        $('#property_monthly_value').html(Math.round(monthly).toLocaleString('en'));

                    }
                },
                rent: {
                    __init: function(){
                        //TODO add cheque calculator
                    }
                }
            },
            get_home_finance: function(e){

                $('html, body').animate({
                    scrollTop: $("#property_details_email_data").offset().top //scroll to this div so the mortgage calc looks in center of screen
                }, 1000);

                setTimeout(function(){
                    $('.wiggle').addClass('wiggle-animate');
                }, 1100);

            },
            __init: function() {

                page = 'landing';
                pages.landing.state();

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                pages.landing.dropdown.__init();
                $('.secondary.menu .item').tab();
                helpers.lazy_load();

                //Property local info
                // pages.property.details.info.__init();

                setTimeout(function(){
                    pages.property.details.slider.__init();
                    other.update_currency();
                }, 1200);

                pages.property.details.map.__init($('#property_details_map').attr('data-lat'), $('#property_details_map').attr('data-lng'));

                //dewa calculations init for sale and rent
                setTimeout(function(){

                    let url =   new URI( window.location.href );

                    if( url.segment(1) == 'sale' ){
                        pages.property.details.dewa.sale.__init();

                        $('#emi_deposit, #emi_years, #emi_interest').change(function() {
                            pages.property.details.dewa.sale.__init();
                        });

                    } else {
                        pages.property.details.dewa.rent.__init();
                    }

                }, 2000);

                //Property similar properties
                search.property.similar($('#similar_properties').attr('data-_id'), 'similar_properties');

            }
        }
    },
    budget: {
        landing: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                pages.budget.dropdown.__init();

            }
        },
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                pages.budget.dropdown.__init();

                search.property.budget.listings.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.budget.prepopulate();
                }, 1500);
            },
            sort: {
                __init: function(_class) {
                    $('.p_c').hide();
                    $('.' + _class).show();
                }
            }
        },
        dropdown: {
            __init: function() {

                $('#budget_city').dropdown({
                    values: pages.landing.dropdown.data.cities,
                    placeholder: _.has(lang_static, 'global.emirate') ? _.get(lang_static, 'global.emirate') : 'Emirate',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.city = [];
                            search.property.budget.store_variables.city.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                    }
                });

                $('#property_rent_buy').dropdown({
                    values: pages.landing.dropdown.data.rent_buy,
                    placeholder: _.has(lang_static, 'global.rent_buy') ? _.get(lang_static, 'global.rent_buy') : 'Rent or Buy',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.rent_buy = [];
                            search.property.budget.store_variables.rent_buy.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                    }
                });

                $('#property_price_min').dropdown({
                    values: pages.landing.dropdown.data.price.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.budget.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price.sale
                        } else {
                            var rent_type = pages.landing.dropdown.data.price.rent
                        }
                        pages.landing.dropdown.callback.from(value, '#property_price_max', rent_type, 'price');
                    }
                });

                $('#property_price_max').dropdown({
                    values: pages.landing.dropdown.data.price.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.budget.store_variables.price.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price.sale
                        } else {
                            var rent_type = pages.landing.dropdown.data.price.rent
                        }
                        pages.landing.dropdown.callback.to(value, '#property_price_min', rent_type, 'price');
                    }
                });
            }
        }
    },
    services: {
        __init: function() {
            page = 'landing';
            pages.landing.state();
            // $('.ui.sticky').sticky({context: '#listings_wrapper'});
            pages.services.dropdown.__init();
        },
        dropdown: {
            __init: function() {
                setTimeout(function() {
                    $('#services_service').dropdown({
                        values: pages.services.dropdown.data.list,
                        placeholder: _.has(lang_static, 'form.placeholders.search_service') ? _.get(lang_static, 'form.placeholders.search_service') : 'Search for a service',
                        maxSelections: 6,
                        action: function(text, value, element) {
                            $("#search").attr('data-_id', value);
                            $(this).dropdown('set selected', value).dropdown('hide');
                        }
                    });
                }, 1000);
            },
            data: searchJson.service
        },
        listings: {
            contact: {
                __init: function(trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_services_contact_user_phone');
                    }, 500);

                    //1
                    var data = {
                        contact: {
                            name: $(trigger).parent().attr("data-service_contact_name") ? $(trigger).parent().attr("data-service_contact_name") : 'Service name not available',
                            logo: $(trigger).parent().attr("data-service_logo") ? $(trigger).parent().attr("data-service_logo") : 'Logo not available',
                        },
                        writeup: {
                            description: $(trigger).parent().attr("data-service_description") ? $(trigger).parent().attr("data-service_description") : 'Description not available'
                        },
                        _id: $(trigger).parent().attr("data-service_id")
                    }

                    //2
                    $('#modal_services_contact_name').html(data.contact.name);
                    $('#modal_services_contact_logo').attr('src', data.contact.logo);
                    $('#modal_services_contact_description').html(data.writeup.description);
                    $('#modal_services_contact_data').attr('data-service_id', data._id);
                    $('#modal_services_contact_data').attr('data-subsource', 'desktop');

                    //3
                    $('.ui.modal.services_contact').modal('setting', 'transition', 'vertical flip').modal('show');

                    helpers.store.temp_data.write = data;

                    other.datalayer('service', 'contact_click', data);

                }
            }
        }
    },
    projects: {
        listings: {
            map: {
                __init: function() {

                    google_maps.listings.__init('projects_listings_map');

                    $('.ui.sticky.map, #projects_list_view, #projects_grid_view').fadeIn();
                    $('.projects_listings_ad, #projects_map_view, .enquiry_project').hide();

                    $('#projects_card_grid').attr('id', 'projects_card_list');
                    $("#projects_card_list").attr('class', 'ten wide column');

                    $('#projects_card_list > .ui.link.two.cards').attr('class', 'ui link one cards');
                    // $('#projects_card_list').siblings('.five.wide.column').attr('class', 'six wide column');

                    //Sticky map in listings page
                    setTimeout(function() {
                        $('.ui.sticky.map').sticky({
                            context: '#pages_projects_listings'
                        });
                    }, 1000);

                    //Bind mouseenter/hover event for google maps trigger on card
                    google_maps.events.bind.mouseenter('.card');

                }
            },
            grid: {
                __init: function() {
                    $('.ui.sticky.map, #projects_grid_view').hide();
                    $('#projects_card_list').attr('id', 'projects_card_grid');
                    $('#projects_card_grid, .projects_listings_ad, #projects_list_view, #projects_map_view, .enquiry_project').fadeIn();

                    // $("#projects_card_grid").attr('class', 'eleven wide column');
                    $('#projects_card_grid > .ui.link.one.cards').attr('class', 'ui link two cards');
                    // $('#projects_card_grid').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter/hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.card');

                }
            },
            list: {
                __init: function() {
                    $('.ui.sticky.map, #projects_list_view').hide();
                    $('#projects_card_grid').attr('id', 'projects_card_list');
                    $('#projects_card_list, .projects_listings_ad, #projects_grid_view, #projects_map_view, .enquiry_project').fadeIn();

                    // $("#projects_card_list").attr('class', 'eleven wide column');
                    $('#projects_card_list > .ui.link.two.cards').attr('class', 'ui link one cards');
                    // $('#projects_card_list').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter//hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.card');
                }
            },
            __init: function(country='uae') {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.projects.dropdown.__init(country);

                //pagination init
                helpers.pagination.__init();

                //sorting init
                helpers.sort.__init();

                // Initiate Lazy Load
                helpers.lazy_load();

                // Prepopulate search
                setTimeout(function() {
                    search.projects.prepopulate(country);
                }, 100);
                //
                helpers.phonenumber.init('modal_developer_enquiry_phone');

                setTimeout(function() {
                    $('ui.sticky.enquiry_project').sticky({
                        context: '#pages_projects_listings'
                    });
                }, 1000);

                $('.progress').progress();

                validate.rules.lead.project.enquiry.__init();
            }
        },
        details: {
            __init: function(country='uae') {
                page = 'landing';
                pages.landing.state();
                // pages.projects.dropdown.__init(country);
                pages.projects.details.slider.__init();
                pages.projects.details.map.__init($('#projects_details_map').attr('data-lat'), $('#projects_details_map').attr('data-lng'));
                //Tabs init
                $('#main-tab .item').tab();
                $('#multimedia .item').tab();

                //Accordin init for floor plans
                // $('.ui.accordion').accordion();

                // Initiate Lazy Load
                helpers.lazy_load();
                setTimeout(function() {
                    helpers.phonenumber.init('project_lead_phone', false);
                }, 500);

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                pages.property.listings.list.__init();

            },
            view_video_360: function(id){
                // $('html, body').animate({
                //     scrollTop: $("#" + id).offset().top
                // }, 1000);

                if(id === 'view_video'){
                    var url = $('#video_source').attr('src');
                    if(_.includes(url, '?autoplay')){
                        var url_upd = $('#video_source').attr('src').substring(0, $('#video_source').attr('src').indexOf('?'))
                        $('#video_source').attr('src', url_upd + '?autoplay=true');
                    }else{
                        $("#video_src")[0].play();
                    }
                }
            },
            slider: {
                __init: function() {

                    $('.slider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        lazyLoad: 'ondemand',
                        dots: false,
                        infinite: true,
                        appendDots: '.slider-nav',
                        appendArrows: '.slider-arrows',
                        nextArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--left"><i class="chevron right icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--right"><i class="chevron right icon"></i></a>',
                        prevArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--right"><i class="chevron left icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--left"><i class="chevron left icon"></i></a>'
                    });


                    // $('.slides').slick({
                    //     slidesToShow: 1,
                    //     slidesToScroll: 1,
                    //     dots: true,
                    //     arrows: false,
                    //     infinite: true,
                    //     autoplay: false
                    // });


                    // $('.slider').glide({
                    //     autoplay: false,
                    //     arrowsWrapperClass: 'slider-arrows',
                    //     arrowRightText: '',
                    //     arrowLeftText: ''
                    // });
                    // $('.slider-arrow--right').append('<i class="chevron right icon"></i>');
                    // $('.slider-arrow--left').append('<i class="chevron left icon"></i>');
                }
            },
            map: {
                __init: function(lat, lng) {
                    var center = {};

                    center.lat = Number(lat);
                    center.lng = Number(lng);

                    // No location overlay
                    if (center.lat === 0 || center.lng === 0) {
                        $('.no_loc').show();
                    } else {
                        g_map.__init(center, [], 'projects_details_map');
                        $('.ui.checkbox').checkbox();
                    }
                    return;
                }
            },
            call: {
                __init: function(data_trigger) {

                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') : 'Phone not available',
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        subsource: 'desktop'
                    }

                    $('#modal_developer_call_developer_contact_phone').html(data.developer.phone);

                    $('#modal_developer_call_project_name').html(data.project.name);
                    $('#modal_developer_call_contact_name').html(data.developer.name);
                    $('#modal_developer_call_data').attr('data-_id', data.project._id);
                    $('#modal_developer_call_data').attr('data-subsource', data.subsource);

                    $('.ui.modal.developer_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','call_click', data_trigger);
                    other.datalayer('project','call_submit', data_trigger);

                    lead.project.call.__init();
                }
            },
            email: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_developer_email_phone');
                    }, 500);

                    //1
                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') :'Phone not available')
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        subsource: 'desktop',
                        custom_form: 'setting.custom_form'
                    }
                    // helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_email_agent_img');

                    //2
                    $('#modal_developer_email_project_name').html(data.project.name);
                    $('#modal_developer_email_contact_name').html(data.developer.name);
                    $('#modal_developer_email_data').attr('data-custom_form', data.custom_form);
                    $('#modal_developer_email_data').attr('data-_id', data.project._id);
                    $('#modal_developer_email_data').attr('data-subsource', data.subsource);

                    //3
                    $('.ui.modal.developer_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','email_click', data_trigger);
                }
            }
        },
        dropdown: {
            __init: function(country) {
                //TODO enable country wise routing
                let uae = 'united arab emirates (uae)';
                country =   country == 'uae' ? uae : country;
                if(country=='international'){
                    projects_project_values     =   _.omit(pages.projects.dropdown.data.developers, uae);
                    completion_status_values    =   _.omit(pages.projects.dropdown.data.completion_status, uae);
                    project_locations_list      =   _.omit(pages.projects.dropdown.data.locations, uae);

                    projects_project_values     =   _.uniqBy(_.flatten(_.map(projects_project_values, 'developers')),'value');
                    completion_status_values    =   _.uniqBy(_.flatten(_.map(completion_status_values, 'completion_status')),'value');
                    project_locations_list      =   _.uniqBy(_.flatten(_.map(project_locations_list, 'locations')),'value');
                }
                else{
                    projects_project_values     =   _.orderBy(pages.projects.dropdown.data.developers[country].developers,'name','asc');
                    completion_status_values    =   pages.projects.dropdown.data.completion_status[country].completion_status;
                    project_locations_list      =   pages.projects.dropdown.data.locations[country].locations;
                }

                $('#projects_project').dropdown({
                    values: projects_project_values,
                    placeholder: _.has(lang_static, 'form.placeholders.all_developer') ? _.get(lang_static, 'form.placeholders.all_developer') : 'All Developer',
                    maxSelections: 1,
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.projects.store_variables.developer = value;
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.projects.store_variables.developer = undefined;
                        }
                    }
                });

                $('#completion_status').dropdown({
                    values: completion_status_values,
                    placeholder: _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.projects.store_variables.query_string.status = value;
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.projects.store_variables.query_string.status = undefined;
                        }
                    }
                });

                var auto = document.getElementById('project_location');

                new Awesomplete(auto, {
                    // list: searchJson.project.locations,
                    list: project_locations_list,
                    maxItems: 30,
                    autoFirst: true,
                    replace: function (text) {
                        if (!_.isUndefined(text) && !_.isEmpty(text)) {
                            search.projects.store_variables.location = text.label;
                        }
                        this.input.value = search.projects.store_variables.location;
                    }
                });

            },
            data: searchJson.project
        }
    },
    agent: {
        landing: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.agent.dropdown.__init();
            }
        },
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.agent.dropdown.__init();

                // Lazy Load
                helpers.lazy_load();

                //pagination init
                helpers.pagination.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.agent.prepopulate();
                }, 1500);

                $('.popup-video-youtube').magnificPopup({
                    type: 'iframe'
                });
            },
            call: {
                __init: function(data_trigger) {

                  /**
                  * 1.   Create Object with Data from trigger
                  * 2.   Bind Data to ID's
                  * 3.   Show Modal
                  */
                  var user = JSON.parse(helpers.store.read('user_o'));
                    //1
                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : 'Phone not available'
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        _id: _.get(data_trigger, '_id')
                    }

                    if(!data.agent.phone.includes("+") && !_.startsWith(data.agent.phone,'0')){
                        $('#modal_agent_call_agent_contact_phone').html('+' + data.agent.phone);
                    }
                    else if(_.startsWith(data.agent.phone,'0')){
                        $('#modal_agent_call_agent_contact_phone').html(data.agent.phone.replace('0', '+971'));
                    } else {
                        $('#modal_agent_call_agent_contact_phone').html(data.agent.phone);
                    }

                    //2
                    $('#modal_agent_call_agent_name').html(data.agent.name);
                    $('#modal_agent_call_agency_name').html(data.agency.name);

                    //3
                    $('.ui.modal.agent_call').modal('setting', 'transition', 'vertical flip').modal('show');
                    //4
                    var payload = {
                        action: 'call',
                        source: 'consumer',
                        subsource: 'desktop',
                        message: _.has(lang_static, 'modals.agent.call.payload_msg') ? _.get(lang_static, 'modals.agent.call.payload_msg') : 'Hi, I found your profile on Zoom Property. Please contact me. Thank you.',
                        agent: {
                            _id: data._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //storing the payload for call buton api call
                    lead.agent.store.save = payload;


                    payload.agent_name = data.agent.name;
                    payload.agency_name = data.agency.name;

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','call_submit', data_trigger);

                    lead.agent.call.__init();

                }
            },
            email: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_agent_email_phone');
                    }, 500);

                    pages.signup_guide_slider.__init( 'email-slider' );

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */

                    //1
                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            picture: _.has(data_trigger, 'contact.picture') ? _.get(data_trigger, 'contact.picture') : '',
                            _id: _.get(data_trigger, '_id')
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_agent_email_agent_img');

                    //2
                    // $('#modal_agent_email_agent_img').attr("src", data.agent.picture);
                    $('#modal_agent_email_agent_name').html(data.agent.name);
                    $('#modal_agent_email_agency_name').html(data.agency.name);
                    $('#modal_agent_email_data').attr('data-_id', data.agent._id);

                    //3
                    $('.ui.modal.agent_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','email_click', data_trigger);

                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_agent_callback_phone');
                    }, 500);

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        }
                    }

                    // helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_agent_callback_agent_img');

                    //2
                    // $('#modal_agent_callback_agent_name').html(data.agent.name);
                    // $('#modal_agent_callback_agency_name').html(data.agency.name);
                    // $('#modal_agent_callback_textarea').html('Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.')
                    $('#modal_agent_callback_data').attr('data-_id', data.agent._id);
                    $('#modal_agent_callback_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.agent_callback').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','callback_click', data_trigger);

                }
            },
            whatsapp: {
                __init: function(trigger) {

                    if ($(trigger).parent().attr("data-agent_contact_phone").length === 0) {
                        alert('Phone number not available');
                    } else {

                        //whatsapp lead create api
                        lead.agent.whatsapp.__init(payload);

                        window.open(
                            'https://wa.me/' + $(trigger).parent().attr("data-agent_contact_phone"),
                            '_blank'
                        );

                    }
                }
            }
        },
        details: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                /**
                * 1.   Load Properties for agent
                * 2.   Populate popup with Agent Data
                */
                // 1.
                var sink = 'agent_properties';
                var query = new search.property.regular.variables();
                query.agent._id = $('#' + sink).attr('data-_id');
                query.size = 3;
                search.property.listings(query, sink);

                pages.agent.dropdown.__init();

                $('.popup-video-youtube').magnificPopup({
                    type: 'iframe'
                });

                $('.rating').rating('disable');
            },
            review: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Show Modal
                    */

                    //1
                    var data = {
                        agent: {
                            _id: _.get(data_trigger, '_id')
                        },
                    }

                    $('#modal_agent_review_rate').rating();

                    //2
                    $('.ui.modal.agent_review').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','review_click', data_trigger);

                }
            },
        },
        dropdown: {
            __init: function() {

                setTimeout(function() {

                    $('#agent_brokerage').dropdown({
                        values: _.orderBy(pages.agent.dropdown.data.list,'name', 'asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.brokerage') ? _.get(lang_static, 'form.placeholders.brokerage') : 'Brokerage',
                        maxSelections: 1,
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                                search.agent.store_variables.agency.push(value);
                                search.agent.store_variables.agency = [_.uniq(search.agent.store_variables.agency).pop()];
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if(!value){
                                $(this).dropdown('clear');
                                search.agent.store_variables.agency = undefined;
                            }
                        }
                    });

                    $('#agent_areas').dropdown({
                        values: _.orderBy(pages.agent.dropdown.data.areas,'name','asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.areas') ? _.get(lang_static, 'form.placeholders.areas') : 'Areas',
                        maxSelections: 1,
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                                areas = value.split(',');
                                locations = areas[areas.length - 1];
                                search.agent.store_variables.area.push(locations);
                                search.agent.store_variables.area = [_.uniq(search.agent.store_variables.area).pop()];
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if(!value){
                                $(this).dropdown('clear');
                                search.agent.store_variables.area = undefined;
                            }
                        }
                    });

                }, 1000);

            },
            data: searchJson.brokerage
        }
    },
    components: {
        modal: {
            signin: {
                toggle: {
                    __init: function(id) {

                        helpers.clear.errors();

                        let array = [$('#modal_signin'), $('#modal_register'), $('#modal_forgot_password')];

                        for (var k = 0; k < array.length; k++) {
                            each = array[k];
                            if (id === $(each).attr('id')) {
                                $(each).show();
                            } else {
                                $(each).hide();
                            }
                        }

                        if (id === 'modal_register') {

                            var input = document.getElementById('register_input_confirm_password');

                            input.addEventListener("keyup", function(event) {
                                event.preventDefault();
                                if (event.keyCode === 13) {
                                    document.getElementById('register_submit_btn').click();
                                }
                            });

                        } else if (id === 'modal_forgot_password') {

                            var input = document.getElementById('forgot_password_input_confirm_password');

                            input.addEventListener("keyup", function(event) {
                                event.preventDefault();
                                if (event.keyCode === 13) {
                                    document.getElementById('forgot_password_submit_btn').click();
                                }
                            });

                        }

                    }
                }
            }
        }
    },
    signin: {
        modal: {
            __init: function() {

                helpers.clear.errors();

                $('.ui.modal.signin_register').modal('setting', 'transition', 'fade').modal('show');

                var input = document.getElementById("signin_input_password");

                input.addEventListener("keyup", function(event) {
                    event.preventDefault();
                    if (event.keyCode === 13) {
                        document.getElementById("signin_submit_btn").click();
                    }
                });

            }
        }
    },
    reset_password: {
        change_password: function(trigger){

            $(trigger).addClass('loading');

            var password = $('#reset_password_new_password > input').val(),
            confirmPassword = $("#reset_password_confirm_password > input").val();

            $('#change_password_error').html('');

            var url_token = window.location.href,
            parts = url_token.split('/'),
            token = parts.pop();

            var url = baseApiUrl + "user/auth/reset";

            var payload = {
                email: $('#reset_password_email > input').val(),
                password: password,
                password_confirmation: confirmPassword,
                token: token
            }

            axios.post(url, payload).then(function(response){

                $(trigger).removeClass('loading');

                helpers.toast.__init( _.get(lang_static, 'toast.success_password_changed') );

                $('#reset_password_new_password > input').val('');
                $('#reset_password_confirm_password > input').val('');
                $("#reset_password_email > input").val('');

            }).catch(function (error) {

                $(trigger).removeClass('loading');

                if(error.response.data.errors){
                    // error.response.data.errors.email
                    var errs    =   error.response.data.errors;
                    var errors  =   '';
                    for(var i in errs){
                        errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                    }
                    $('#change_password_error').html('<div class="ui error message">'+errors+'</div>');
                }else{
                    $('#change_password_error').html(error.response.data.message);
                }

                // helpers.toast.__init('Something went wrong, Please try later');

                // $('#reset_password_new_password > input').val('');
                // $('#reset_password_confirm_password > input').val('');
                // $("#reset_password_email > input").val('');

                // $('#change_password_error').html('Something went wrong, Please try later');
            });

        },
        __init: function() {

        }
    },
    signup_guide_slider: {
        __init: function( trigger ) {
            $('.'+trigger).slick({
                arrows: false,
                dots: true,
                autoplay: true,
                autoplaySpeed: 2000
            });
        }
    }
}

$('.counter').each(function() {
    $(this).prop('counter', 0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function(now) {
            $(this).text(Math.ceil(now));
        }
    });
});
