const { mix } = require('laravel-mix');
const { exec } = require('child_process');

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for the application as well as bundling up all the JS files.
|
*/
mix
// ZP - Begin
.sass('resources/assets/scss/main.scss', 'public/assets/css/main.css')
.sass('resources/assets/scss/mobile/_main.scss', 'public/assets/css/_main.css')
.minify(['public/assets/css/main.css', 'public/assets/css/main.rtl.css', 'public/assets/css/_main.css', 'public/assets/css/_main.rtl.css'])
.combine([
    'resources/assets/js/fx/*',
    'resources/assets/js/ux/desktop_and_tablet/*'
], 'public/js/app.js')
.combine([
    'resources/assets/js/dependencies/*',
    'public/js/app.js',
], 'public/js/skeleton.js')
.combine([
    'resources/assets/js/dependencies/*',
    'resources/assets/js/fx/*',
    'resources/assets/js/ux/mobile/*'
], 'public/js/_skeleton.js')
.minify(['public/js/skeleton.js', 'public/js/_skeleton.js'])
// ZP - End

// Microsites - Begin
.sass('resources/assets/microsites/styles.scss', 'public/assets/microsites/css/styles.css')
.minify(['public/assets/microsites/css/styles.css', 'public/assets/microsites/css/styles.rtl.css'])
.combine([
    'resources/assets/microsites/dependencies/*',
    'resources/assets/microsites/*.js'
], 'public/assets/microsites/js/script.js')
.minify(['public/assets/microsites/js/script.js'])
// Microsites - End

.then(() => {})
.options({
    processCssUrls: false,
    postCss: [ require('autoprefixer') ]
}).sourceMaps().version();
