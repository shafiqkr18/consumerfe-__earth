<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Agent extends TestCase
{
    /**
     * @dataProvider searchQueryProvider
     */
    public function testAgentSearch($request){

        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Agent Search","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Agent($request);
        $response       =   $controller->index()->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('agent',$response, 'Agent Read request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,array_get($response,'total',0), 'Agent Read returned results');

    }

    /**
     * @dataProvider agentIdProvider
     */
    public function testAgentDetails($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Agent Details","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Agent(new \Illuminate\Http\Request);
        $response       =   $controller->show($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('agent',$response, 'Agent Show request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'agent',[])), 'Agent Read returned results');
    }

    /**
     * @dataProvider agentIdProvider
     */
    public function testSimilarProperties($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Similar Agents","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Agent(new \Illuminate\Http\Request);
        $response       =   $controller->similar($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('agent', $response, 'Similar Agents request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'agent',[])), 'Similar Agents returned results');
    }

    // Stupid test to format the output.
    public function testComplete(){
        echo "\n";
        $this->assertTrue(true);
    }

    public function searchQueryProvider(){
        $request    =   new \Illuminate\Http\Request;
        return [
            [
                $request->replace([
                    'contact'   =>  [
                        'email'     =>  'tatiana@obg.ae'
                    ]
                ])
            ],
            [
                $request->replace([
                    'contact'   =>  [
                        'license_no'     =>  '131233'
                    ]
                ])
            ]
        ];
    }
    public function agentIdProvider(){
        return [
            [
                'tatiana@obg.ae'
            ]
        ];
    }
}
