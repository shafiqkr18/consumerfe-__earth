<?php

namespace Tests\Feature;

use Tests\TestCase;
use \App\Traits\CLI;

class Property extends TestCase
{

    /**
     * @dataProvider searchQueryProvider
     */
    public function testPropertySearch($request){

        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Property Search","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Property($request);
        $response       =   $controller->index()->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('property',$response, 'Property Read request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,array_get($response,'total',0), 'Property Read returned results');

    }

    /**
     * @dataProvider propertyIdProvider
     */
    public function testPropertyDetails($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Property Details","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Property(new \Illuminate\Http\Request);
        $response       =   $controller->show($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('property',$response, 'Property Show request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'property',[])), 'Property Read returned results');
    }

    /**
     * @dataProvider propertyIdProvider
     */
    public function testSimilarProperties($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Similar Properties","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Property(new \Illuminate\Http\Request);
        $response       =   $controller->similar($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('property',$response, 'Similar Property request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'property',[])), 'Similar Properties returned results');
    }

    // Stupid test to format the output.
    public function testComplete(){
        echo "\n";
        $this->assertTrue(true);
    }

    public function searchQueryProvider(){
        $request    =   new \Illuminate\Http\Request;
        return [
            [
                $request->replace([
                'area'                      =>  ['Dubai Marina'],
                'rent_buy'                  =>  ['Rent'],
                'residential_commercial'    =>  ['residential']
                ])
            ]
        ];
    }
    
    public function propertyIdProvider(){
        return [
            [
                'zp-obg-123'
            ]
        ];
    }

}
