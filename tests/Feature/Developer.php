<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Developer extends TestCase
{
    /**
     * @dataProvider searchQueryProvider
     */
    public function testDeveloperSearch($request){

        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Developer Search","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Developer($request);
        $response       =   $controller->index()->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('developer',$response, 'Developer Read request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,array_get($response,'total',0), 'Developer Read returned results');

    }

    /**
     * @dataProvider developerIdProvider
     */
    public function testDeveloperDetails($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Developer Details","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Developer(new \Illuminate\Http\Request);
        $response       =   $controller->show($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('developer',$response, 'Developer Show request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'developer',[])), 'Developer Read returned results');
    }

    /**
     * @dataProvider developerIdProvider
     */
    public function testSimilarProperties($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Similar Developers","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Developer(new \Illuminate\Http\Request);
        $response       =   $controller->similar($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('developer',$response, 'Similar Developer request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'developer',[])), 'Similar Developers returned results');
    }

    // Stupid test to format the output.
    public function testComplete(){
        echo "\n";
        $this->assertTrue(true);
    }



    public function searchQueryProvider(){
        $request    =   new \Illuminate\Http\Request;
        return [
            [
                $request->replace([
                    'name'  =>  ['Emaar']
                ])
            ]
        ];
    }
    public function developerIdProvider(){
        return [
            [
                'zd-abc-123'
            ]
        ];
    }
}
