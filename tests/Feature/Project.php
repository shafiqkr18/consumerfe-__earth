<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Project extends TestCase
{
    /**
     * @dataProvider searchQueryProvider
     */
    public function testProjectSearch($request){

        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Project Search","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Project($request);
        $response       =   $controller->index()->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('project',$response, 'Project Read request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,array_get($response,'total',0), 'Project Read returned results');

    }

    /**
     * @dataProvider projectIdProvider
     */
    public function testProjectDetails($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Project Details","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Project(new \Illuminate\Http\Request);
        $response       =   $controller->show($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('project',$response, 'Project Show request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'project',[])), 'Project Read returned results');
    }

    /**
     * @dataProvider projectIdProvider
     */
    public function testSimilarProperties($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Similar Properties","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Project(new \Illuminate\Http\Request);
        $response       =   $controller->similar($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('project',$response, 'Similar Project request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'project',[])), 'Similar Projects returned results');
    }

    // Stupid test to format the output.
    public function testComplete(){
        echo "\n";
        $this->assertTrue(true);
    }

    public function searchQueryProvider(){
        $request    =   new \Illuminate\Http\Request;
        return [
            [
                $request->replace([
                    'area'  =>  ['Dubai Marina']
                ])
            ]
        ];
    }

    public function projectIdProvider(){
        return [
            [
                'zj-abc-123'
            ]
        ];
    }

}
