<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Agency extends TestCase
{
    /**
     * @dataProvider searchQueryProvider
     */
    public function testAgencySearch($request){

        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Agency Search","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Agency($request);
        $response       =   $controller->index()->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('agency',$response, 'Agency Read request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,array_get($response,'total',0), 'Agency Read returned results');

    }

    /**
     * @dataProvider agencyIdProvider
     */
    public function testAgencyDetails($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Agency Details","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Agency(new \Illuminate\Http\Request);
        $response       =   $controller->show($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('agency',$response, 'Agency Show request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'agency',[])), 'Agency Read returned results');
    }

    /**
     * @dataProvider agencyIdProvider
     */
    public function testSimilarAgencies($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Similar Agencies","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Agency(new \Illuminate\Http\Request);
        $response       =   $controller->similar($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('agency',$response, 'Similar Agency request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'agency',[])), 'Similar Agencies returned results');
    }

    // Stupid test to format the output.
    public function testComplete(){
        echo "\n";
        $this->assertTrue(true);
    }

    public function searchQueryProvider(){
        $request    =   new \Illuminate\Http\Request;
        return [
            [
                $request->replace([
                    'contact'   =>  [
                        'email'     =>  'joyce@obg.ae'
                    ]
                ])
            ],
            [
                $request->replace([
                    'contact'   =>  [
                        'orn'     =>  '131233'
                    ]
                ])
            ]
        ];
    }

    public function agencyIdProvider(){
        return [
            [
                'joyce@obg.ae'
            ]
        ];
    }
    
}
