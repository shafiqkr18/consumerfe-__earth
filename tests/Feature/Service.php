<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class Service extends TestCase
{
    /**
     * @dataProvider searchQueryProvider
     */
    public function testServiceSearch($request){

        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Service Search","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Service($request);
        $response       =   $controller->index()->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('service',$response, 'Service Read request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,array_get($response,'total',0), 'Service Read returned results');

    }

    /**
     * @dataProvider serviceIdProvider
     */
    public function testServiceDetails($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Service Details","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Service(new \Illuminate\Http\Request);
        $response       =   $controller->show($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('service',$response, 'Service Show request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'service',[])), 'Service Read returned results');
    }

    /**
     * @dataProvider serviceIdProvider
     */
    public function testSimilarProperties($id){
        // Name the test
        $cli    =   new \App\Traits\CLI();
        echo $cli->getColoredString("Similar Properties","cyan")."\n";

        $controller     =   new \App\Http\Controllers\Service(new \Illuminate\Http\Request);
        $response       =   $controller->similar($id)->getData(1);

        // 1.   Check if successful response
        $this->assertArrayHasKey('service',$response, 'Similar Service request successful');
        // 2.   Search results returned
        $this->assertGreaterThan(0,count(array_get($response,'service',[])), 'Similar Services returned results');
    }

    // Stupid test to format the output.
    public function testComplete(){
        echo "\n";
        $this->assertTrue(true);
    }

    public function searchQueryProvider(){
        $request    =   new \Illuminate\Http\Request;
        return [
            [
                $request->replace([
                    'type'  =>  'mover'
                ])
            ]
        ];
    }

    public function serviceIdProvider(){
        return [
            [
                'zp-obg-123'
            ]
        ];
    }

}
