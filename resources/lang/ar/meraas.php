<?php

return [

    'handover' => 'التسليم',
    'i_am_intrested' => 'انا مهتم',
    'form_bedrooms' => 'كم عدد غرف النوم ؟',
    'projects' => [
        'zj-la-rive-port-de-la-mer' => [
            'label_a' => '<span>يعرض <strong>La Rive</strong></span>مجتمع التملك الحر الرئيسي',
            'label_b' => '<span>٥-١ غرف شقق المطلة على الشاطئ</span> في لاريف ابتداء من ١.٢ مليون د.إ .'
        ],
        'zj-bluewaters-residences' => [
            'label_a' => '<span> المعيشة على شاطئ البحر في<strong>بلو ووترز</strong></span>الأكثر رواجا بعد تراجع الجزيرة في دبي',
            'label_b' => '<span>1-4 Bedroom Beachfront Apartments</span> Starting From AED 2.1M'
        ],
        'zj-nikki-beach-residences' => [
            'label_a' => '<span>Beachfront Apartments and Townhouses</span>A new way of life',
            'label_b' => '<span>1-4 Bedroom Apartments</span> Starting From AED 2.4M'
        ],
        'zj-cherrywoods' => [
            'label_a' => '<span>Exclusive Collection of Freehold Townhouses</span>5 Year Payment Plan Available',
            'label_b' => '<span>3-4 Bedroom Spacious Townhouses</span> Starting from AED 1.2M'
        ],
        'zj-bvlgari' => [
            'label_a' => '<span>Resort and Residences</span>Experience a Lifestyle Reserved for the Privileged',
            'label_b' => '<span>Ready to move in units</span> Starting from AED 4.5M'
        ],
        'zj-port-de-la-mer' => [
            'label_a' => '<span>Resort and Residences</span>Experience a Lifestyle Reserved for the Privileged',
            'label_b' => '<span>Ready to move in units</span> Starting from AED 4.5M'
        ]
    ]

];
