<?php

return [

	'handover' => 'التسليم',
    'i_am_intrested' => 'انا مهتم',
    'form_bedrooms' => 'كم عدد غرف النوم ؟',
    'projects' => [
        'zj-la-quinta' => [
            'label' => '<span>سجل اهتمامك الآن</span>'
        ],
        'zj-mudon-views' => [
            'label' => '<span>Register your interest now</span>'
        ],
        'zj-1-jbr' => [
            'label' => '<span>Register your interest now</span>'
        ]
    ]

];
