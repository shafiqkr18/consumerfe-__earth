<?php

return [

    'global'    =>  [
        'hi'                      =>  'مرحبا',
        'seo_title'               =>  'الموقع العقاري الأفضل في الأمارات',
        'seo_description'         =>  'الموقع العقاري رقم ١:  زوم بروبرتي هو أفضل وأكبر موقع للعقارات في دولة الإمارات العربية المتحدة، حيث يوفر مجموعة كبيرة من العقارات السكنية والتجارية للإيجار والبيع في  دولة الإمارات . إذا كنت تبحث عن منزل جديد ، استثمار ، بيت عطلات في الإمارات ، ستجد كل ذلك و اكثر في  موقعنا.',
        'seo_h1'                  =>  '',
        'seo_h2'                  =>  '',
        'seo_h3'                  =>  '',
        'seo_image_alt'           =>  'زوم بروبرتي',
        'to'                      =>  'إلى',
        'for'                     =>  'لآجل',
        'in'                      =>  'في',
        'video'                   =>  'فيديو',
        'from'                    =>  'من',
        'sqft'                    =>  'قدم مربع',
        'sqft_long'               =>  'قدم مربع',
        'aed'                     =>  'درهم',
        'uae'                     =>  'الإمارات',
        'emirate'                 =>  'الإمارة',
        'search'                  =>  'بحث',
        'search_filter'           =>  'البحث/تصفية البحث',
        'advanced_search'         =>  'بحث متقدم',
        'find'                    =>  'اعثر',
        'rent_buy'                => 'للبيع/ للايجار',
        'rent_property'           =>  'إيجارعقارات من آجل',
        'buy_property'            =>  'شراءعقارات من آجل',
        'bedroom'                 =>  '{0} لا يوجد غرف نوم |{1} غرفة نوم |[*,2] غرف نوم',
        'bedrooms'                =>  'Bedrooms',
        'bathroom'                =>  '{0} لا يوجد حمام |{1} حمام |[*,2] حمامات',
        'price'                   =>  'السعر',
        'area'                    =>  'المساحة',
        'bed'                     =>  '{0} استوديو |{1} :bed غرفة نوم |[2,*] :bed غرف نوم',
        'bath'                    =>  '{0} لا يوجد حمام |{1} :bath حمام |[2,*] :bath حمامات',
        'default_title'           =>  'شقة رائعة',
        'default_agency_name'     =>  'وكالة مثيرة للاهتمام',
        'default_agency_title'    =>  'واسطة',
        'default_agent_name'      =>  'عميل ممتاز',
        'default_agent_title'     =>  'هذا العميل',
        'default_project_name'    =>  'مشروع رائع',
        'default_service_name'    =>  'خدمة رائعة',
        'about'                   =>  'حول',
        'properties_for'          =>  'عقارات من آجل',
        'properties'              =>  'عقارات',
        'featured'                =>  'متميزة',
        'sponsored'               =>  'Sponsored ad',
        'verified'                =>  'التحقق',
        'visit_wesite'            =>  'زيارة الموقع',
        'more'                    =>  'المزيد...',
        'less'                    =>  'أقل...',
        'agency'                  =>  '{0} وكالة |{1} وكالات',
        'agent'                   =>  '{0} عميل |{1} عملاء',
        'completion_status'       =>  'حالة الانتهاء',
        'time'                    =>  'الوقت',
        'international'           =>  'العالمي',
        'studio'                  =>  'استوديو',
        'international_projects'  =>  'مشاريع عالمية',
        'and'                     =>  'و',
        'most_popular'            =>  'الأكثر شهره',
        'popular_searches'        =>  'البحث الأكثر شعبية',
        'types'                   =>  'متنوعة',
        'available_unit'          =>  'وحرات سكنية جاهزة في',
        'cheque'                  =>  '{1} Cheque |[2,*] Cheques'
    ],
    'featured'              =>  [
        'residential'            =>  [
            'sale'                  =>  [
                'title'                 =>  'عقارات سكنية  متميزة للبيع'
            ],
            'rent'                  =>  [
                'title'                 =>  'عقارات سكنية  متميزة للايجار'
            ],
            'short term rent'           =>  [
                'title'                 =>  'عقارات سكنية  متميزة للايجار لفترة قصيرة'
            ]
        ],
        'commercial'            =>  [
            'sale'                  =>  [
                'title'                 =>  'عقارات تجارية متميزة للبيع'
            ],
            'rent'                  =>  [
                'title'                 =>  'عقارات تجارية متميزة للايجار'
            ],
            'short term rent'           =>  [
                'title'                 =>  'عقارات تجارية للإيجار لفترة قصيرة'
            ]
        ]

    ],
    'pagination' =>[
        'next'  =>  'التالى',
        'prev'  =>  'السابق'
    ],
    'menu_breadcrumbs' =>  [
        'home'            =>  'الصفحة الرئيسية',
        'agent_finder'    =>  'أيجنت فايندر',
        'new_projects'    =>  'مشاريع جديدة',
        'budget_search'   =>  'بحث بالميزانية',
        'privacy_policy'  =>  'سياسة الخصوصية',
        'services'        =>  'خدمات',
        'terms_conditions'=>  'الشروط والاحكام',
        'agents_login'    =>  'تسجيل الدخول للوسطاء',
        'blog'            =>  'مقالات',
        'more'            =>  'المزيد',
        'explore'         =>  'Explore',
        'for_brokers'     =>  'خاص بالوكالات',
        'create_alert'    =>  'إنشاء تنبيه',
        'home_services'   =>  'خدمات منزلية',
        'buy'             =>  'شراء',
        'rent'            =>  'إيجار',
        'residential'     =>  'سكني',
        'commercial'      =>  'تجاري',
        'commercial_buy'  =>  'شراء تجاري',
        'commercial_rent' =>  'إيجارتجاري',
        'search_property' =>  'البحث عن عقار',
        'list_property'   =>  'قوائم العقارات ',
        'my_account'      =>  'حسابي',
        'international'   =>  'العالمي'
    ],
    'footer'    =>  [
        'newsletter'  =>  [
            'title'       =>  'شارك أخبارنا الجديدة',
            'message'     =>  'اترك لنا عنوان بريدك الإلكتروني وسنحرص على ان نبقيك على إطلاع مستمر.'
        ],
        'keep_in_touch'         =>  'كن على اتصال دائم بنا',
        'trending_searches'     =>  'البحث الاكثر شيوعا',
        'popular_searches_item' =>  ':type :for_rent_buy في :loc',
        'popular_areas'         =>  'المناطق الأكثر شعبية',
        'trending_properties'   =>  'العقارات الأحدث',
        'social_media'      =>  [
            'facebook'          =>      'https://www.facebook.com/ZoomPropertyUAE/',
            'twitter'           =>      'https://twitter.com/zoompropertyuae',
            'instagram'         =>      'https://www.instagram.com/zoompropertyuae/',
            'googleplus'        =>      'https://plus.google.com/u/2/103251563061642583375'
        ]
    ],
    'card'      =>  [
        'actions'   =>  [
            'call'      =>  'اتصال',
            'email'     =>  'الايميل',
            'chat'      =>  'محادثة',
            'call_back' =>  'معاودة الاتصال',
            'whatsapp'  =>  'واتس اب'
        ]
    ],
    'form'      =>  [
        'contact'  => 'اتصل',
        'more_details_title'  => 'للحصول على مزيد من المعلومات قم بتعبئة بياناتك',
        'your_messagge'       =>  'رسالتك',
        'default_messagge_property' =>  'مرحبا، لقد وجدت هذا العقار مع :reference من خلال زوم بروبرتي ارجو التواصل معي . شكرا.',
        'default_messagge_project'  =>  'مرحبا, اريد معرفة المزيد عن هذا المشروع.',
        'send_email'          =>  'إرسال بريد إلكتروني',
        'view_phone_number'   =>  'إظهار رقم الهاتف',
        'all_fields_required' =>  'جميع الفراغات مطلوبة',
        'placeholders'        =>  [
            'full_name'           =>  'الإسم الكامل',
            'email'               =>  'البريد الإلكتروني',
            'email_address'       =>  'Email Address',
            'phone'               =>  'أدخل رقم الاتصال الخاص بك',
            'phone_number'        =>  '+971',
            'max_budget'          =>  'الحد القصى للميزانية',
            'preferred_locations' =>  'المواقع المفضلة',
            'password'            =>  'كلمة المرور',
            'password_confirm'    =>  'تأكييد كلمة السر الجديدة',
            'password_current'    =>  'كلمة السر القديمة',
            'password_new'        =>  'كلمة السر الجديدة',
            'nationality'         =>  'الجنسية',
            'mobile'              =>  'الهاتف المحمول',
            'mobile_number'       =>  'رقم الهاتف المحمول',
            'work_phone'          =>  'هاتف العمل',
            'location'            => 'ادخل للمناطق , المدن و البنايات',
            'cities_areas'        => 'المناطق / المدن',
            'clear'               => 'إعادة تعيين البحث',
            'clear_mobile'        => 'إعادة تعيين البحث',
            'filters'             =>  'Filters',
            'min'                 => [
                'price'                 => 'أقل سعر',
                'bed'                   => 'أقل عدد لعرف النوم',
                'bath'                  => 'أقل عدد لعرف الحمام',
                'area'                  => 'أقل مساحة',
            ],
            'max'                 => [
                'price'                 => 'أقصى سعر',
                'bed'                   => 'أقصى عدد لعرف النوم',
                'bath'                  => 'أقصى عدد لعرف الحمام',
                'area'                  => 'أقصى مساحة',
            ],
            'type'                =>  'نوع العقار',
            'furnishing'          =>  'تأثيث',
            'areas'               =>  'المناطق',
            'brokerage'           =>  'الوسيط العقاري',
            'country'             =>  'الدولة',
            'search_service'      =>  'البحث عن خدمة',
            'search_developer'    =>  'البحث عن مطور',
            'all_developer'       =>  'All Properties',
            'areas_buildings'     =>  'المناطق والمباني',
            'budget'              =>  'ميزانية',
            'completion_status'   =>  'حالة الانتهاء',
            'keyword'             =>  'الكلمات الرئيسيه'
        ],
        'button'              =>  [
            'reset'               =>  'إعادة',
            'submit'              =>  'تقديم الطلب',
            'call'                =>  'اتصال',
            'call_now'            =>  'اتصل الان',
            'call_agent'          =>  'Call Agent',
            'send'                =>  'إرسال',
            'contact'             =>  'اتصل',
            'request_details'     =>  'طلب تفاصيل',
            'request_call_back'   =>  'اعادة الاتصال',
            'request_call_free_back'   =>  'طلب اتصال مجاني',
            'lets_chat'           =>  'ابدا المحادثة!',
            'create_alert'        =>  'انشاء تنبيه',
            'register'            =>  'تسجيل',
            'reset_password'      =>  'اعادة تعيين كلمة السر',
            'change_password'     =>  'تغيير كلمة السر',
            'update'              =>  'تحديث',
            'edit'                =>  'تعديل',
            'cancel'              =>  'الغاء',
            'start_favourite'     =>  'ابدء البحث و فضله',
            'set_alert'           =>  'إنشاء تنبيه جديد',
            'subscribe'           =>  'اشتراك',
            'new_search'          =>  'بدء بحث جديد'
        ],
        'ideal_property' =>[
            'first_message'  =>  'دعنا نساعدك للحصول على عقارك المثالي',
            'second_message'  =>  'اطلعنا على ما تحتاجه و سوف نقوم بالبحث بالنيابة عنك'
        ],
        'general_enquiry' =>[
            'btn'       =>  'استفسر الان',
            'message'   =>  'أخبرنا على ماذا تبحث وأين، وسوف نتواصل مع ١٠ عملاء متخصصين نيابة عنك.'
        ]
    ],
    'grid'      =>  [
        'sort'      =>  [
            'title'       =>  'فرز النتائج',
            'low_to_high' =>  'أقل إلى أكثر',
            'high_to_low' =>  'أكثر إلى أقل',
            'newest_to_oldest' =>  'الجديد الى القديم',
            'oldest_to_newest' =>  'القديم الى الجديد',
            'title_mobile' => 'تصنيف',
            'save_mobile' =>  'حفظ'
        ],
        'view'      =>  [
            'map'          =>  'عرض على الخريطة',
            'list'         =>  'عرض القائمة',
            'grid'         =>  'عرض شبكي'
        ],
        'button'      =>  [
            'alert'        =>  'تنبيهي',
            'save'         =>  'حفظ البحث'
        ],
        'search' => 'تجد'
    ],
    'landing'   =>  [
        'message'          =>  'الاخبار و المقالات',
        'message_mobile'   =>  'بحث سريع عن عقار',
        'quick_access' => [
            'link_a' => 'شقق للإيجار تحت',
            'link_b' => 'شقق للبيع تحت'
        ],
        'popular_property' =>  'عقارات عامة',
        'other' =>  'أخرى',
        'hot_project' =>  'مشروع مهم',
        'latest_video' =>  'اخر الفيديوهات',
        'view_all' =>  'عرض الكل',
        'view_less' =>  'عرض اقل',
        'views_neighborhood' =>  'اطلالات الجيران',
        'searches' =>  'بحث سريع عن عقار',
    ],
    'mortgage'          =>  [
        'title'             =>  'آلة حساب الرهن العقاري',
        'description'       =>  'الحسابات تعتمد على تكلفة متوسطة لكل واحدة قدم مربع للشقق. هذا يتضمن الضريبة البلدية الشهرية. استخدم الفراغات القابلة للتعديل لتعديل الحسابات بشكل مناسب.',
        'other_description' =>  'الحسابات حسب متوسط التكلفة للقدم المربع للشقق . شاملا الضريبة الشهرية للبلدية . استخدم خيارات التعديل للقيام بتعديل و اعادة الحساب',
        'monthly_costs'     =>  'التكاليف الشهرية',
        'monthly_total'     =>  'إجمالي المبلغ الشهري',
        'emi_title'         =>  'الأقساط الشهرية المتساوية تقريبا هي',
        'monthly'           =>  'في الشهر',
        'per_monthly'       =>  'شهريا',
        'leave_details_message'  =>  'اترك لنا  بريدك الالكتروني و سوف نطلعك على المزيد',
        'categories'        =>  [
            'mortgage'          =>  'الرهن العقاري',
            'energy'            =>  'الطاقة',
            'water'             =>  'المياه'
        ],
        'calculations'      =>  [
            'message'              =>  'عدل القيمة اعلاه لتحديد القيمة الكلية الشهرية',
            'purchase_price'       =>  'سعر الشراء',
            'deposit'              =>  'دفعة مسبقة (%)',
            'repayment_term'       =>  'مده السداد (بالسنوات)',
            'interest_rate'        =>  'معدل الفائدة (%)',
            'municipal_fees'       =>  'بلدية المستحقات',
            'avg_electricity_cost' =>  'معدل قيمة استهلاك الكهرباء',
            'avg_gas_cost'         =>  'معدل قيمة اسهلاك الوقود',
            'avg_water_cost'       =>  'معدل قيمة استهلاك الماء',
            'down_payment'         =>  'دفعة اولى في الدرهم',
            'finance_amount'       =>  'مبلغ التمويل',
            'term'                 =>  'المدة بالسنوات',
            'interest'             =>  'الفائدة (٪)',
            'loan_amount'          =>  'كمية القرض',
            'purchase_price'       =>  'سعر الشراء',
            'total_amount'         =>  'كمية المبلغ المدفوع',
            'total_interest'       =>  'الفائدة الكلية',
        ],
        'buy_only'          =>  'إشتري هذا العقار فقط من',
        'ma_powered_by'     =>  'حاسبة الرهن العقاري بواسطة',
        'subject_approval'  =>  'هذه الاسعار تقريبة و قابلة للتغير عند الموافقة النهائية',
        'powered_by'        =>  'بواسطة',
        'get_finance'       =>  'إحصل على تمويل عقاري الان',
        'estimated_mortgage'=>  'الرهن العقاري المقدر',
    ],
    'landmarks'       =>  [
        'mosque'          =>  'المساجد ',
        'hospital'        =>  'المستشفيات ',
        'school'          =>  'المدارس ',
        'gym'             =>  'صالات الألعاب الرياضية ',
        'bank'            =>  'البنوك ',
        'shopping_mall'   =>  'المولات ',
        'parking'         =>  'مواقف السيارات',
        'park'            =>  'الحدائق ',
        'gas_station'     =>  'محطات الوقود '
    ],
    'modals'    =>  [
        'hints'     =>  [
            'name'      =>  'اسمك الاول',
            'email'     =>  'بريدك الالكتروني',
            'phone'     =>  'رقم هاتفك',
            'full_name' =>  'اسمك',
            'last_name' =>  'اسم العائلة',
            'country'   =>  'البلد',
            'request_call_back' => 'اعادة الاتصال في :',
            'latest_info' => 'من فضلك ارسل لي احدث العروض و الاخبار العقارية'
        ],
        'agent'     =>  [
            'call'      =>  [
                'message'   =>  'سوف تتكلم مع',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'call_back' =>  [
                'message'   =>  'مرحبًا ، لقد وجدت ملفك على زوم بروبرتي. ارجوا التواصل . شكرا.',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'email'     =>   [
                'message'   =>  'مرحبًا ، لقد وجدت ملفك على زوم بروبرتي. ارجوا التواصل . شكرا.',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp'     =>   [
                'message'   =>  'مرحبًا ، لقد وجدت ملفك على زوم بروبرتي. ارجوا التواصل . شكرا.',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'consumer'    =>  [
            'alert'       =>  [
                'title'       =>  'دعنا نساعدك للحصول على عقارك المثالي',
                'subtitle'    =>  'اخبرنا ماذا تريد و نحن سوف نقوم بهملية البحث.'
            ]
        ],
        'developer'   =>  [
            'call'        =>  [
                'message'   =>  'رقم المطور'
            ]
        ],
        'property'    =>  [
            'alert'       =>  [
                'title'       =>  'انشاء تنبيه خاص بالملكية',
                'subtitle'    =>  'قم بانشاء تنبيه و كم اول من يعلم باحدث العقارات!'
            ],
            'call'      =>  [
                'message'     =>  'يمكنك الاتصال',
                'ref_message' =>  'لا تنسى ادخال الرقم',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.',
                'catch_msg'   =>  'To get a <span>FREE</span> call back from the agent please provide your details below.'
            ],
            'call_back' =>  [
                'message'     =>  'نحن نتحدث الانكليزية ، العربية، الهندية، الاسبانية و ٧٢ لغة مختلفة',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ],
            'call_me' =>  [
                'message'     =>  'Hi, I found your property with ref: :reference on zoom property. Please call me back. Thank you.'
            ],
            'email' =>  [
                'message'     =>  'مرحبا، لقد وجدت هذا العقار مع :reference من خلال زوم بروبرتي ارجو التواصل معي . شكرا.',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp' =>  [
                'message'     =>  'مرحبًا ، لقد رأيت إعلانك على زوم بروبرتي،  ref: :reference، يرجى معاودة الاتصال.',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'project'    =>  [
            'call'      =>  [
                'message'     =>  'يمكنك الاتصال',
                'ref_message' =>  'رقم التواصل للمطور',
                'payload_msg' =>  'Hi, I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'call_back' =>  [
                'message'     =>  'نحن نتحدث الانكليزية ، العربية، الهندية، الاسبانية و ٧٢ لغة مختلفة',
                'payload_msg' =>  'Hi, I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'email' =>  [
                'message'     =>  'مرحبا, اريد معرفة المزيد عن هذا المشروع.',
                'payload_msg' =>  'I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp' =>  [
                'message'     =>  'Hi, I saw your advert on Zoom Property, project: :name, please get back in touch',
                'payload_msg' =>  'Hi, I found your project :name on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'service'     =>  [
            'contact'     =>  [
                'message'     =>  'مرحبا, لقد وجدت هذه الخدمة من خلال zoom property.  شكرا . رجاءا اتصل بي لاحقا.'
            ]
        ],
        'preference'     =>  [
            'contact'     =>  [
                'message'     =>  'مرحبا، انا ابحث عن عقار في :location. ارجو التواصل. شكرا.'
            ]
        ],
        'signin_register'  => [
            'title'       =>  'سجل معنا',
            'subtitle'    =>  'احصل على تجربة شخصية',
            'signin'      =>  'تسجيل الدخول',
            'forgot_password' =>  'نسيت كلمة المرور؟',
            'account'     =>  'ليس لديك حساب؟',
            'already_registered'  =>  'مسجل بالفعل?',
            'signin_here'  =>  'تسجيل الدخول هنا',
            'register_here'   =>  'سجل هنا',
            'register'    =>  'تسجيل',
            'signin_facebook'    =>  'تسجيل الدخول عبر فيسبوك',
            'signin_google'      =>  'تسجيل الدخول عبر الجيميل',
            'register_facebook' =>  'تسجيل عبر فيسبوك',
            'register_google'   =>  'تسجيل عبر جوجل',
            'agree_terms' =>  'من خلال التسجيل معنا سوف تكون قد وافقت على',
            'agree_terms_signin' =>  'عبر تسجيل الدخول معنا سوف تكون قد وافقت على',
            'privacy_policy'   =>  'سياسة الخصوصية',
            'terms_conditions' =>  'الشروط والاحكام',
            'go_back_signin'    =>  'العودة الى تسجيل الدخول',
            'promotions'  =>  [
                'update_profile'  =>  'تحديث معلومات الملف الخاص و التفضيلات',
                'get_alerts'      =>  'احصل على تنبيهات عبر البريد الإلكتروني عن العقارات الجديدة',
                'save_favourites' =>  'احفظ العقارات المفضلة لديك',
                'save_search'     =>  'حفظ البحث'
            ]
        ]
    ],
    'sections'  =>  [
        'banner'    =>  [
            'agent'   =>  [
                'title'   =>  'البحث عن الوكيل'
            ],
            'budget'   =>  [
                'title'      =>  'دعنا نعرف ميزانياتك',
                'subtitle'   =>  'و سنقوم باختيار أفضل عقار قد يناسبك وفي أفضل موقع'
            ],
            'service'   =>  [
                'title'      =>  'خدمات ساعد نفسك في الانتقال الي'
            ],
            'landing'   =>  [
                'title'      =>  'اكتشف المزيد',
                'subtitle'   =>  'اعثر على عقارك المتميز والحي المثالي'
            ]
        ],
        'chat'  =>  [
            'title' =>  'انت تتحدث مع وسيط'
        ],
        'dashboard'  =>  [
            'profile'               =>  'الملف الشخصي',
            'favourite_searches'    =>  'البحوث المفضلة',
            'favourite_properties'  =>  'العقارات المفضلة',
            'alerts'                =>  'تنبيهات العقارات',
            'email_chat'            =>  'البريد الإلكتروني والدردشة',
            'settings'              =>  'الاعدادات',
            'sign_out'              =>  'تسجيل الخروج',
            'not_favourite_search'  =>  'ليس لديك أي عمليات بحث مفضلة',
            'not_favourite_property'=>  'ليس لديك أي عقارات مفضلة',
            'not_alerts'            =>  'ليس لديك اي تنبيهات عقارية جديدة',
            'not_chats'             =>  'لا يوجد محادثات مسبقة',
            'search_again'          =>  'عاود البحث',
            'change_password'       =>  'تغيير كلمة المرور القديمة',
            'subscribe'             =>  'شارك اخبارنا الجديدة'
        ]
    ],
    'property'  =>  [
        'details'   =>  [
            'similar_properties' =>  'عقارات مماثلة',
            'ref_no_title'        =>  'الرقم المرجعي',
            'no_building'         =>  'بعض الابنية',
            'no_area'             =>  'بعض المناطق',
            'no_city'             =>  'بعض المدن',
            'play'                =>  'Play',
            'view'                =>  'View',
            'tab_titles'          =>  [
                'gallery'             =>  'العرض',
                'agent_details'       =>  'معلومات الوسيط',
                'property_details'    =>  'معلومات العقار',
                'map'                 =>  'الخريطة والمناطق المجاورة',
                'video'               =>  'فيديو',
                'video_degree'        =>  '٣٦٠',
                'local_info'          =>  'المعلومات المحلية',
            ],
            'not_location_provided' =>  ':agency_name لم يتوفر العنوان لهذا العقار ',
            'amenities_title'   =>  'المرافق',
            'description'       =>  'اوصاف الشقة',
            'agent_card'          =>  [
                'marketed_by'         =>  'تم التسويق بواسطة',
                'name_title'          =>  'اسم الوكيل',
                'license_title'       =>  'رقم مؤسسة التنظيم العقاري',
                'agency_name_title'   =>  'المؤسسة',
                'view_all'            =>  'عرض جميع العقارات من خلال',
                'view_profile'            =>  'عرض الملف الشخصي',
                'play_now'            =>  'العب الان',
            ],
            'view_more'          =>  'عرض المزيد',
            'view_less'          =>  'عرض اقل',
            'stats'         =>  [
                'area_stats'     =>  'احصائيات المنطقة',
                'avg_floor'      =>  'متوسط مساحة الطابق ',
                'decreased'      =>  'الانخفاض سابقا',
                'increased'      =>  'increased in last',
                'months'         =>  'اشهر',
                'price'         =>  'السعر',
                'trends'         =>  'الاتجاهات',
                'by_property'   =>  'حسب نوع العقار'
            ],
            'expert_review'   =>  'مراجعة الخبراء',
            'cheaper_area'   =>  'الارخص و المشابه في المنطقة',
            'cheaper_bed'   =>  'الارخص مع نفس عدد الأسرة',
        ],
        'listings'  =>  [
            'properties_found'   =>  'تم ايجاد العقارات',
            'properties_no_found_title'   =>  'لم نتمكن من العثور على أي عقارات مطابقة لبحثك.',
            'properties_no_found_subtitle'   =>  'ربما تكون هذه مناسبة لك ايضا  ؟'
        ],
        'lead'  => [
            'call'  => [
                'message' => 'Hi, I found your property with ref: :ref_no on Zoom Property. Please contact me. Thank you.'
            ],
            'callback'  => [
                'message' => 'Hi, I found your property with ref: :ref_no on Zoom Property. Please contact me. Thank you.'
            ],
            'email'  => [
                'message' => 'Hi, I found your property with ref: :ref_no on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp'  => [
                'message' => 'Hi, I saw your advert on Zoom Property, ref: :ref_no, please get back in touch'
            ]
        ]
    ],
    'agent'     =>  [
        'details'   =>  [
            'area_specialist'   =>  'خبير المنطقة',
            'top_agents'        =>  'Top Agents',
            'nationality'       =>  'الجنسية',
            'languages'         =>  'اللغات',
            'company'           =>  'الشركة',
            'license'           =>  'الرخصة',
            'properties_by'     =>  'مليكات خاصة ب',
            'contact_agent'     =>  'Contact this agent',
            'listed_property'   =>  'قوائم العقارات',
            'featured_agent'    =>  'Featured Agent',
            'about_me'          =>  'About me',
            'reviews'           =>  'Reviews',
            'reviews_me'        =>  'Reviews Me',
            'out_of'            =>  'out of',
            'featured_review'   =>  'Featured Review',
            'load_more'         =>  'Load More',
            'load_less'         =>  'Load Less',
            'agent_title'       =>  'Rate this agent',
            'my_properties'     =>  'My properties',
            'areas_covered'     =>  'Areas Covered',
            'specialty'         =>  'Specialty',
            'find_out_more'     =>  'Find out More',
            'agent_found'       =>  '{0} تم العثور على وكلاء|{1} تم العثور على وكيل|[2,*] لم يتم العثور على وكلاء',
            'rate'   =>  [
                'excelent'   =>  'Excellent',
                'good'       =>  'Good',
                'regular'    =>  'Regular',
                'bad'        =>  'Bad',
                'very_bad'   =>  'Very Bad'
            ]
        ]
    ],
    'budget'    =>  [
        'refine_search' =>  'تحسين البحث',
        'seo_title'     =>  'بحث عن العقارات حسب الميزانية لـ :rent_buy under AED :price_max'
    ],
    'project'   =>  [
        'starting_from'  => 'تبدأ من',
        'details'   =>  [
            'by'  => 'بواسطة',
            'developed_by'  => 'طورت بواسطة',
            'contact'       => 'اتصل',
            'location'      => 'Location',
            'view_all'      => 'عرض جميع المشاريع الحديثة',
            'about'         => 'عن المشروع',
            'development_progress' => 'التقدم في عملية التنمية',
            'expected_completion_date' => 'تاريخ التسليم ',
            'status' => 'الحالة',
            'mortgage_deposit' => 'Mortgage Deposit From',
            'progress' => [
                'foundations'   =>  'أسس',
                'parking'       =>  'مواقف السيارات',
                'building'      =>  'البناء',
                'completion'    =>  'الانتهاء'
            ],
            'features_title'  =>  'ملامح',
            'tab_titles'          =>  [
                'gallery'             =>  'العرض',
                'floor_plans'         =>  'خطط المبنى',
                'completion_status'   =>  'حالة الاكمال',
                '3d_tour'             =>  'جولة ثلاثية الابعاد',
                'details'             =>  'التفاصيل',
                'email'               =>  'الايميل',
                'map'                 =>  'خريطة',
                'about'               => 'عن المشروع'
            ],
            'payment_plan'    =>  'خطط الدفع',
            'view_brochure'    =>  'عرض الكتيب',
            'download'    =>  'تحميل',
            'brochure'    =>  'الكتيب',
            'read_more'    =>  'اقرأ المزيد',
            'read_less'    =>  'اقرأ الاقل'
        ],
        'listings'  =>  [
            'new_projects'    =>  '{0} لم يتم العثور على خدمات |{1} تم العثور علي مشروع جديد |[2,*]  تم العثور علي مشاريع جديده',
            'move_in'         =>  'جاهز للانتقال'
        ]
    ],
    'services'    =>  [
        'listings'      =>   [
            'results_found'       =>   '{0} لم يتم العثور على خدمات |{1}  تم العثور على الخدمة |[2,*]  تم العثور على خدمات',
            'default_description' =>   'سنجعلك تقفز من السعادة !'
        ]
    ],
    'toast'    =>  [
        'error_default' =>   'حدث خطأ ما ، يرجى المحاولة لاحقًا',
        'error_phone' =>   'يرجى إدخال رقم هاتف متاح.',
        'error_email' =>   'يرجى إدخال عنوان بريد إلكتروني صالح لإعادة تعيين كلمة المرور الخاصة بك.',
        'error_email_b' =>   'أدخل عنوان بريد إلكتروني صالح.',
        'success_request_sent'       =>   'تم ارسال طلبك! شخص ما سيقوم بلتواصل قريبا.',
        'success_alert_created' =>   'تم إنشاء تنبيه بنجاح!',
        'success_password_changed' =>   'لقد تم تغيير كلمة المرور الخاصة بك بنجاح!',
        'success_newsletter_subscribed' =>   'لقد اشتركت بنجاح في نشراتنا الإخبارية.',
        'success_search_saved' =>   'لقد قمت بحفظ هذا البحث بنجاح.',
        'success_signed_in' =>   'لقد قمت بتسجيل الدخول بنجاح.',
        'success_registered' =>   'لقد قمت بالتسجيل وتسجيل الدخول بنجاح.',
        'success_email_sent' =>   'تم إرسال بريد إلكتروني إلى بريدك الإلكتروني المسجل.',
        'showing_results' =>   ' عرض النتائج من'
    ],
    'results'   =>  [
        'results_found'             =>  'النتائج التي تم العثور عليها',
        'no_results_found'          =>  'لم يتم العثور علي نتائج',
        'no_results_found_title'    =>  'لم نستطع العثور علي اي نتائج لبحثك المحدد.',
        'no_results_found_subtitle' =>  'ربما تكون هذه مناسبة لك ايضا  ؟',
        'not_specified'             =>  'Not Specified'
    ],
    'currencies' => [
        [
            'label' => 'درهم اماراتي',
            'code' => 'AED',
            'value' => '1',
            'flag' => 'ae'
        ],
        [
            'label' => 'دولار امريكي',
            'code' => 'USD',
            'value' => '0.2723',
            'flag' => 'us'
        ],
        [
            'label' => 'دولار استرالي',
            'code' => 'AUD',
            'value' => '0.4036',
            'flag' => 'au'
        ],
        [
            'label' => 'دولار كندي',
            'code' => 'CAD',
            'value' => '0.3622',
            'flag' => 'ca'
        ],
        [
            'label' => 'جنيه استرليني',
            'code' => 'GBP',
            'value' => '0.2228',
            'flag' => 'vg'
        ],
        [
            'label' => 'يورو',
            'code' => 'EUR',
            'value' => '0.2456',
            'flag' => 'eu'
        ],
        [
            'label' => 'ين صيني',
            'code' => 'RMB',
            'value' => '1.9501',
            'flag' => 'cn'
        ],
        [
            'label' => 'ريال سعودي',
            'code' => 'SAR',
            'value' => '1.0212',
            'flag' => 'sa'
        ],
        [
            'label' => 'روبيةالهند',
            'code' => 'INR',
            'value' => '19.5433',
            'flag' => 'in'
        ],
        [
            'label' => 'روبية باكستان',
            'code' => 'PKR',
            'value' => '42.8917',
            'flag' => 'pk'
        ]
    ],
    'guideme'   => [
        'property_area' => 'العقارات الموجودة في منطقتك',
        'menu_label' => 'ارشدني',
        'finding_location' => 'العثور على موقعك ...',
        'enter_location' => 'أدخل الموقع',
        'view_map' => 'إعرض الخريطة',
        'enable_location' => 'يرجى تمكين موقعك',
        'you_are_in' => 'أنت في ',
        'no_results' => 'لا توجد نتائج',
        'no_results_area_increased' => 'عذرًا ، لم تكن هناك نتائج لاختيارك ، لذا قمنا بزيادة مساحة البحث',
        'showing_stats_for' => 'تظهر لك احصائيات المنطقة ل',
        'no_stats' => 'إحصائيات المنطقة غير متوفرة',
        'apartment' => 'شقة',
        'loft_apartment' => 'شقة ( لوفت )',
        'hotel_apartment' => 'شقة فندقية',
        'flat_apartment' => 'شقة',
        'whole_building' => 'مبنى كامل',
        'land' => 'أرض',
        'full_floor' => 'طابق كامل',
        'office_space' => 'مكتب',
        'office' => 'مكتب',
        'labor_camp' => 'مخيم عمال',
        'villa' => 'فيلا',
        'plot' => 'قطعة أرض',
        'bulk_units' => 'وحدات سكنية بالجملة',
        'bungalow' => 'جناح صغير',
        'compound' => 'مجمع',
        'duplex' => 'دوبلكس ذو طابقين',
        'half_floor' => 'نصف طابق',
        'townhouse' => 'تاون هاوس',
        'retail' => 'تجزئه',
        'shop' => 'متجر',
        'show_room' => 'صالة عرض',
        'staff_accommodation' => 'سكن موظفين',
        'warehouse' => 'مستودع',
        'factory' => 'مصنع',
        'business_center' => 'مركز أعمال',
        'penthouse' => 'بنتهاوس',
        'graph_label_a' => 'شهر',
        'graph_label_b' => 'السعر',
        'default_location' => 'مركز تجاري',
        'price' => 'السعر',
        'average' => 'معدل',
        'sqft' => 'قدم مربع',
        'area' => 'منطقة',
        'sale' => 'شراء',
        'rent' => 'تأجير'
    ]

];
