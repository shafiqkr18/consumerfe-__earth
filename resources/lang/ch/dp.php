<?php

return [

	'handover' => 'Handover',
    'i_am_intrested' => 'I\'m Interested',
    'form_bedrooms' => 'How many bedrooms?',
    'projects' => [
        'zj-la-quinta' => [
            'label' => '<span>Register your interest now</span>',
            'logo'    => 'zj-la-quinta.png',
            'thumb'   => 'zj-la-quinta.jpg',
            'video_thumb'   => 'zj-la-quinta.jpg'
        ],
        'zj-mudon-views' => [
            'label' => '<span>Register your interest now</span>',
            'logo'    => 'zj-mudon-views.png',
            'thumb'   => 'zj-mudon-views.jpg',
            'video_thumb'   => 'zj-mudon-views.jpg'
        ],
        'zj-1-jbr' => [
            'label' => '<span>Register your interest now</span>',
            'logo'    => 'zj-1-jbr.png',
            'thumb'   => 'zj-1-jbr.jpg',
            'video_thumb'   => 'zj-1-jbr.jpg'
        ]
    ]

];
