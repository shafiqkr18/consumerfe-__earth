<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码必须至少为六个字符并与密码确认相匹配。',
    'reset' => '您的密码已经更新成功！',
    'sent' => '密码重置链接已经发送到了您的电子邮箱。',
    'token' => '密码重置出错。',
    'user' => "使用此电子邮箱注册的用户不存在。",

];
