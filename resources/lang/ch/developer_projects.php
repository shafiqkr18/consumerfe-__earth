<?php
return [

    'data'       =>  [
        [
            '_id'		=>	'zd-meraas-56893',
            'stub'		=>	'meraas',
            'contact'   =>  [
                'name'          =>  'Meraas',
                'email'         =>  'sales.enquiry@meraas.ae',
                'phone'         =>  '97142463203',
                'whatsapp_link' =>  '',
                'website'       =>	'http://www.meraas.ae/',
                'orn'           =>  '',
                'address'       =>  [
                    'address_line_one'  =>  'Al Satwa, 32 C Street',
                    'state'             =>  'Dubai',
                    'city'              =>  'Dubai',
                    'country'           =>  'UAE',
                    'postal_code'       =>  '123311'
                ]
            ],
            'image' 	=>  [
                'logo'		=>	'https://drive.google.com/uc?export=view&id=1w6TSHr8lQZ3WENy6pi_7fB3d9goPe9H7',
                'banner'    =>  '',
                'gallery'   =>  []
            ],
            'write_up'  =>  [
                'description'   =>  'Meraas real estate comprises property development sales and asset management across some of Dubai’s most sought-after and iconic locations, such as City Walk, Bluewaters, Jumeira Bay, Pearl Jumeira and La Mer. Meraas real estate prides itself on working with the best in class partners with a commitment to building projects of the highest quality, delivered on time, using the most comprehensive and integrated master planning. Meraas also boasts a significant land bank in key areas in Dubai'
            ],
            'settings'  =>  [
                'status'    =>  true,
                'approved'  =>  true,
                'role_id'   =>  5,
                'chat'      =>  false,
                'lead'      =>  [
                    'admin'      =>  [
                        'email'          =>  'sales.enquiry@meraas.ae',
                        'receive_emails' =>  true,
                        'cc'             =>  [],
                    ],
                    'call_tracking'   =>  [
                        'track'       =>  true,
                        'phone'       =>  '97142463203'
                    ]
                ]
            ],
            'featured'  =>  [
                'status'    =>  false,
                'from'      =>  null,
                'to'        =>  null
            ],
            'projects'	=>   [
                [
                    '_id'						=>	'zj-bluewaters-residences-67846',
                    'ref_no'                    =>  'bluewaters-residences-67846',
                    'name' 						=>	'Bluewaters Residences - Ready to Move In',
                    'web_link'                  =>  '',
                    'area' 					    =>	'Bluewaters',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.07675,
                        'lng'                       =>  55.12375
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1gKBSGvi6Mqm5NMkhL2wBUD97TQu4Dpd7',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      '2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 2-3A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1DNkrjYtjHnW_a5kbPp1NSK3VPw5YQt8x'
                                    ],
                                    [
                                        'type'      =>      'Type 2-1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1CYqQ0h15-fgoQO3vsDyah-xKO61xVyjI'
                                    ],
                                    [
                                        'type'      =>      'Type 2-1A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1mxG6zIl3vJWtMa6cacKbHRUbIm0JT5LN'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '3 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1-1A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1ktTx8vzYkVfgkarslfkoH_xOkiYPfCBA'
                                    ],
                                    [
                                        'type'      =>      'Type 2-1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1Pn8KyzPtuhYrV65gLqit5pvACaAv3xIz'
                                    ],
                                    [
                                        'type'      =>      'Type 2-5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1UCmK19XdUIpzfciYTYpyDP5At-CncW4d'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '4 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1-1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1hE-RecvfoS0k_ia54nwsvrhR-BQEcHs1'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1FIK_wdYlV2ElUpaAYxKN7at3mTrHNepu',
                            'https://drive.google.com/uc?export=view&id=1angqoEkX5WZ8tFFDOx86DLXlRgEQpSKP',
                            'https://drive.google.com/uc?export=view&id=13VkkJO1nScyC7NY1WveYfHEoG34je01d',
                            'https://drive.google.com/uc?export=view&id=1REAqXhedRGJ4xrxkHOG02UqXb84Mm2jv',
                            'https://drive.google.com/uc?export=view&id=1-D4XiA_27-gzWPc43GUbVhTFhWvmaMMp',
                            'https://drive.google.com/uc?export=view&id=1iyH4WpIVw_Nj2w3G_G52JZucrbPQn_70',
                            'https://drive.google.com/uc?export=view&id=1-lfol631P3J2QZtJI3WNDqO1mMLaWCWt',
                            'https://drive.google.com/uc?export=view&id=1o607UAD9LmFHSmi6e1wbHLQBvD_P5Tox',
                            'https://drive.google.com/uc?export=view&id=1RPyllwW76aydB0RIZxYmX3IERwcQ1P19',
                            'https://drive.google.com/uc?export=view&id=17S9pgChJ5vQZA3FsYm6YVrLqL9zznDl6',
                            'https://drive.google.com/uc?export=view&id=10CbdNNpGl77jQ2J_DJNJxpIZXswsqDgR',
                            'https://drive.google.com/uc?export=view&id=1N0v_IC4UDQdCHg5CNyS3TDNtnq6R3Po7',
                            'https://drive.google.com/uc?export=view&id=17mzLeEZnchpwTd3OE2X1xmCxwYadpR8W',
                            'https://drive.google.com/uc?export=view&id=1sbOQ5aNciZeG6iZTIZJvRZgSM__NBo45',
                            'https://drive.google.com/uc?export=view&id=1-eEvui4b_RXa52WRYmUnpyILmptfZjsz',
                            'https://drive.google.com/uc?export=view&id=1G8ALtH64OEHGe44ge_ZieSEqo7bmHM8t',
                            'https://drive.google.com/uc?export=view&id=1gKBSGvi6Mqm5NMkhL2wBUD97TQu4Dpd7'
                        ],
                        "portal"                   =>   [
                            "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/banner",
                            "desktop" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/desktop/16"
                            ],
                            "mobile" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/bluewaters-residences/gallery/mobile/16"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'COMPLETED AND READY TO MOVE IN! Bluewaters is a destination with a pioneering spirit, blending waterfront living with the exhilaration of the city to create an unrivalled experience for residents and visitors. The island represents a colourful beacon adorning Dubai’s urban landscape, drawing in those seeking endless lifestyle opportunities.
                        Located off the coast of Jumeirah Beach Residence opposite The Beach, Bluewaters is home to Ain Dubai, the world’s largest observation wheel, and the first Caesars Palace hotels in the Middle East.  With a range of other exciting attractions, the lively, family-oriented neighbourhood is set to become one of the world’s largest tourism hotspots, with its own distinctive residential, retail, hospitality, and entertainment that will leave all who arrive spoilt for choice.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  519,
                        'type'                      =>  '2, 3 & 4 bed apartments',
                        'amenities'                 =>  [ 'Security','Built in Wardrobes','Central A/C','Covered Parking','Shared Gym','Shared Pool','Fully Fitted Kitchen','Nearby Shops','View of Sea','Restaurants' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  2100000,
                        'emi'                       =>  0,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  'Put just 20% down to move in',
                        'payment_plans'             =>  [
                            "| 10%    |    Purchase Date",
                            "| 10%    |    6 Months",
                            "| 10%    |    12 Months",
                            "| 10%    |    16 Months",
                            "| 10%    |    20 Months",
                            "| 10%    |    24 Months",
                            "| 10%    |    28 Months",
                            "| 15%    |    32 Months",
                            "| 15%    |    36 Months"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  'https://www.youtube.com/embed/gY-OVojCdJM'
                        ],
                        'degree'                    =>  [
                            'link'                      =>  'https://my.matterport.com/show/?m=fGgmsSd4Ti2&utm_source=4'
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  1,
                        'approved'                  =>  true,
                        'lead'                      =>  [
                            'email'                     =>  'sales.enquiry@meraas.ae'
                        ]
                    ],
                    'expected_completion_date'  =>  'Completed',
                    'status'					=>	'Completed',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ],
                [
                    '_id'						=>	'zj-port-de-la-mer-78495',
                    'ref_no'        =>  'port-de-la-mer-78495',
                    'name' 						=>	'Port De La Mer',
                    'web_link'                  =>  '',
                    'area' 					    =>	'La Mer, Jumeirah',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.240658,
                        'lng'                       =>  55.251326
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1jkBoLBcuuJ9FwoZnrtEoNN5Jmj8Fy2Jz',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      'Building 1-1 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1jqCz0M-ChSKzt_q2IQZxpKjBhTGtFnBg'
                                    ],
                                    [
                                        'type'      =>      'Type 1A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1Mp9DyPOwQKwtMGPJgpSKYqga1enOP9dL'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=11qhbxW6sesfb_OviJaF1p0NKK0E5tb1t'
                                    ],
                                    [
                                        'type'      =>      'Type 2A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1mKGc2zNIp_2hJcomr4Do7J2G8pNYEAky'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=16MR_oa_hHdQdjh199uEBnr5CvMZxA_KT'
                                    ],
                                    [
                                        'type'      =>      'Type 3A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=14CSYqG6I-4bW2soFV7CUJAtAqKKizIzQ'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=12P_lidUCWy-X4Ce2qTguN8ceubcfBuRz'
                                    ],
                                    [
                                        'type'      =>      'Type 4A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1fJsi6pRWKn4o0Eb_lDs2ksJQMSpuIUnN'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1qputrqHsiqt1Z-XbWc7aPzdFBq4aElxS'
                                    ],
                                    [
                                        'type'      =>      'Type 5A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1FVgSQg4ADGqJxgtvZQtsEOxunIqej-rl'
                                    ],
                                    [
                                        'type'      =>      'Type 6',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1AKuZpZdgzaxV3ITq3AITdbJhvZJ0ioXt'
                                    ],
                                    [
                                        'type'      =>      'Type 6A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1AyJdykFe_mS_mKhCIJLO4iUfEaea4gmY'
                                    ],
                                    [
                                        'type'      =>      'Type 7',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1PW1olrmG3LQknJ2JhEAeEvXDqRnfhVn_'
                                    ],
                                    [
                                        'type'      =>      'Type 7A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1dw31kcObApDAA-j7r1LYP5aYCUD2ueEU'
                                    ],
                                    [
                                        'type'      =>      'Type 8',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1TOgaJjMCnAc3aBVVvYs2Aa326YoFzwpp'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 1-2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1A4-CTQUY01SZqrE5jgs3hFzqek6X19EA'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1MQYV0Kseo21C1JQP8H7wnD8sy0A3Ln6E'
                                    ],
                                    [
                                        'type'      =>      'Type 2A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1tyDuwzGo5EXjGl0fYZjzmFDu7M4hCCSx'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1BnU9qSomf4k7FPm6r1tgov8g64B_yEEu'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1Sj9w4Y92YRE9e-VMoQMwR4S6qBDQyIR1'
                                    ],
                                    [
                                        'type'      =>      'Type 4A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=15XpBJmqyh_bC40-XccaUUwUzUWqoB-lG'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1ZNDwaO0ujl8TBzaFlXIr3FTJQVLcjB4r'
                                    ],
                                    [
                                        'type'      =>      'Type 5A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1MBL_7MINvgrkDc5hMYYiJy-m6J49JYxH'
                                    ],
                                    [
                                        'type'      =>      'Type 6',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1uk-dg4rbm9u837rUJuE2El1eldRfTEdj'
                                    ],
                                    [
                                        'type'      =>      'Type 7',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1QoFJD9kBgSrBuJg35KWWewRihhqAYzp7'
                                    ],
                                    [
                                        'type'      =>      'Type 8',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1OvOWPIywEBnr33XDMuGtwSmDkCJFF567'
                                    ],
                                    [
                                        'type'      =>      'Type 9',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1T9L3osPPaRO963tjNIzuTG8cNGTsJ2qm'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 1-3 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1P6r4772r-YJLw4pou7qmp7F8IFEfY_Fz'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 1-4 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=13NTEtPgeR-0gWClSSrwaDd5vbDDsmc34'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 2-1 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1IAzMmg_kRe4vTmIj7TdS5dl048E7vVFn'
                                    ],
                                    [
                                        'type'      =>      'Type 1A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1R8vYSTDS8x1V29YffivMmgtmmnrPP_uX'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1Z58ak4PSjE6rh7Vqvod4z0WX-BtSc_Gv'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=19nXeRpuNbtEHrAxr2DOz3LaBhTj4vAJw'
                                    ],
                                    [
                                        'type'      =>      'Type 3A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1u-EmO6VLKan_zK498-2EFYNbeYo3WoWo'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1lVHnOL0_Dbq3mHuVPV3Dyz4wA31pjgzB'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=17qOXK-gRdcY1-8UB7dZfxqLe8R7gfDaA'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 2-2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1eWhAZTFvMHDrvfg7CUVdpdS-N_5cF85u'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1CJyqxTzteMj5Oial7FpmwawwndA_CB5W'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1FgT1JDPi3ZJ74LoJ6RrU92ywQ6XPJgHt'
                                    ],
                                    [
                                        'type'      =>      'Type 3A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=18iSN2WJidQnI0UxsWl122Fg9saeejE79'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1JeC8HIJ0w9BzWo70vdDr1IKktYAR1AV-'
                                    ],
                                    [
                                        'type'      =>      'Type 4A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1WT_mC2EBLPrwVzIxjxK02ff3o9f8fSg4'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1lpG6SytsZaB3QOSkudc_TFJVcelUeoEd'
                                    ],
                                    [
                                        'type'      =>      'Type 5A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1cdipnEaSuy99eFbtBeGcXK0jgWMCHEUH'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 2-3 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1buNdAh2K8GyUBRZsBvhcHKUk1xpquqv-'
                                    ],
                                    [
                                        'type'      =>      'Type 1A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1WPF1-nwdOxKdzneJh_uAuZSHzPVYrPEs'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1BbuiVjzjMgcnd4p5qe0UO5U6dm0fYuGc'
                                    ],
                                    [
                                        'type'      =>      'Type 2A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1b90nxzPNoVxgFSa6ehe06cbuaPjwQqBx'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1MTTuCBq023oOTLvTYktdDHh6m2W-UbFA'
                                    ],
                                    [
                                        'type'      =>      'Type 3A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1NJ-wauAgQOXjWaZlWRtBbtLD9UDkyg2a'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1PEFbJDIsYCKblC-qGKiRIyFOymPXnyCL'
                                    ],
                                    [
                                        'type'      =>      'Type 4A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1XCTOSUA0y4gpj9lO3FsRWJOA3pDagfa-'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1WDvdoldLRhz-1tjHE9WICas6PUSNiZWX'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Building 1-4 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1C5K7aC3DfFRXjCK-_r_153o3TyznR0z8'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1surbYn0lcpQjB_RBtQpmvFTPhZ27X9iB'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1H7fdaCFNxqmpARtZKm8R9z5QUIt4na1w'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1jSb6LnLABeVeB7jnb_2bpPMRGpMVYHbt',
                            'https://drive.google.com/uc?export=view&id=1icMxQKhY-PAG1BfmgcnmeV0gufBeHCFE',
                            'https://drive.google.com/uc?export=view&id=1JlsXrAhmNE4SBjYlAipVccMRrM4FiWSe',
                            'https://drive.google.com/uc?export=view&id=1AvZkSPh31wiS-WO01B7PU6xz4Mglu5jG',
                            'https://drive.google.com/uc?export=view&id=1xU9Zcz5y7oj_buJqv5Bi4CyzzHGisGlN',
                            'https://drive.google.com/uc?export=view&id=1LhknICMjhIsUWx5p_ycCoPNfuJNEqkku',
                            'https://drive.google.com/uc?export=view&id=1WHg1CxWvXtceRL9dclTlavAFNmqATcIh',
                            'https://drive.google.com/uc?export=view&id=1Aw-vEQ13wVFR-w_TKm99c7bC9RANz0T_',
                            'https://drive.google.com/uc?export=view&id=1GcDNr1SZjqgUumM8lTnN8oDCXWbp4Fqp',
                            'https://drive.google.com/uc?export=view&id=1jkBoLBcuuJ9FwoZnrtEoNN5Jmj8Fy2Jz'
                        ],
                        "portal" => [
                            "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/banner",
                            "desktop" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/desktop/9"
                            ],
                            "mobile" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/meraas/projects/port-de-la-mer/gallery/mobile/9"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Inspired by the allure of the Mediterranean, Port de La Mer is a seaside community set around a world-class marina. La Côte, which will be the first neighbourhood in Port de La Mer, and will offer a range of 1, 2, 3 and 4-bedroom. With views of the ocean and Dubai skyline, convenient beach access will complete the ultimate seaside lifestyle.<br><br>Port de La Mer is the first freehold master community in Dubai and is situated in the hear of Jumeirah, just 10 minutes from La Mer and City Walk and 15 minutes from Dubai International Airport.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  519,
                        'type'                      =>  '1, 2, 3 & 4 bed apartments',
                        'amenities'                 =>  [ 'Large Swimming Pools','Gym','Beach Access','Marina Access','Hotels','Reatail Outlets','Restaurants','Waterfront' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  1300000,
                        'emi'                       =>  0,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  '',
                        'payment_plans'             =>  [
                            "1st Installment  | 5%    |    Purchase Date",
                            "2nd Installment  | 5%    |    5 November 2018",
                            "3rd Installment  | 10%   |    5 May 2019",
                            "4th Installment  | 10%   |    5 November 2019",
                            "5th Installment  | 10%   |    5 May 2020",
                            "6th Installment  | 60%   |    On Handover"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  'https://www.youtube.com/embed/bteYBihoWdA'
                        ],
                        'degree'                    =>  [
                            'link'                      =>  ''
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  1,
                        'approved'                  =>  true,
                        'lead'                      =>  [
                            'email'                     =>  'sales.enquiry@meraas.ae'
                        ]
                    ],
                    'expected_completion_date'  =>  'Q4 2020',
                    'status'					=>	'Under Construction',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ]
            ]
        ],
        [
            '_id'		=>	'zd-select-property-56719',
            'stub'		=>	'select-property',
            'contact'   =>  [
                'name'          =>  'Select Property',
                'email'         =>  'leads@selectproperty.com',
                'phone'         =>  '97145128391',
                'whatsapp_link' =>  '',
                'website'       =>	'',
                'orn'           =>  '',
                'address'       =>  [
                    'address_line_one'  =>  '',
                    'state'             =>  '',
                    'city'              =>  '',
                    'country'           =>  '',
                    'postal_code'       =>  ''
                ]
            ],
            'image' 	=>  [
                'logo'		=>	'https://drive.google.com/uc?export=view&id=1GlDXpBlLBVeubbTvGzgBeCSQyGd5sfn2',
                'banner'    =>  '',
                'gallery'   =>  []
            ],
            'write_up'  =>  [
                'description'   =>  'We’ve been creating history since 2004. Back then, we recognised the vast investment potential of Dubai’s property market. Acting before anyone else, we worked exclusively with a leading Marina developer, sold thousands of properties, and secured record growth for our investors. Dubai was the foundation of our success, but we wanted to build on this – quite literally. We knew that, in order to give our investors the high returns they wanted, we needed to do it all ourselves. Build, sell and manage. Combining this freedom with shrewd selection of new investment opportunities, we now create and evolve end user brands. With this revolutionary approach, the Group can control every aspect of the investment. None of the tenant focus is lost, all of the advantages are passed on to our investors.'
            ],
            'settings'  =>  [
                'status'    =>  true,
                'approved'  =>  true,
                'role_id'   =>  5,
                'chat'      =>  false,
                'lead'      =>  [
                    'admin'      =>  [
                        'email'          =>  'listings@selectproperty.ae',
                        'receive_emails' =>  false,
                        'cc'             =>  ['leads@selectproperty.com']
                    ],
                    'call_tracking'   =>  [
                        'track'       =>  true,
                        'phone'       =>  '97145128391'
                    ]
                ]
            ],
            'featured'  =>  [
                'status'    =>  false,
                'from'      =>  null,
                'to'        =>  null
            ],
            'projects'	=>   [
                [
                    '_id'						=>	'zj-pacific-al-marjan-66713',
                    'ref_no'                    =>  'pacific-al-marjan-66713',
                    'name' 						=>	'Pacific, Al Marjan',
                    'web_link'                  =>  '',
                    'area' 					    =>	'Al Marjan',
                    'city'						=>	'Ras Al Khaimah',
                    'coordinates'               =>  [
                        'lat'                       =>  25.685945,
                        'lng'                       =>  55.733123
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1KYx6BWGfl7RNJrKeubKz2WG1-6AkgjjL',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      '1 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1aOyDz4XpqBjROv2qTvBnK7ij5i6qxaBa'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1uXJUvZJLlXEz9-Bix5Jrs_JphZkp9c5o'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1qqCKsbxvKC2M10d9Q6iVHyVfMnuEcfDn'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Beach Duplex',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Beach Duplex',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=11L01exzSDcjW_f-6dTF-UnifySjF-guB'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Duplex',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Duplex',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1fevdgg1WXSoIY1UUpXyLJ3znCGFP96KK'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1DY8EA7REAdDYKK0H_IIPEzLe95unArJ6',
                            'https://drive.google.com/uc?export=view&id=1njlcCl_Pol_qgNAkifxYBcqvZX69F2wG',
                            'https://drive.google.com/uc?export=view&id=1BHt4_29GNMtgGCu6VsglXQQ7TqUX2Plz',
                            'https://drive.google.com/uc?export=view&id=1X5OtTdx_tsgToN_nRnbAiX_aOPgimY88',
                            'https://drive.google.com/uc?export=view&id=1AlARBubkwJ-a-HcJ6Cw9mgmR-UwIoRU4',
                            'https://drive.google.com/uc?export=view&id=10kWLnYwm7P-B9HauSagTgbkRYzlf4ivX',
                            'https://drive.google.com/uc?export=view&id=1OnPYu6WktdNmLeANVMJpBTEP7Amj1u-6',
                            'https://drive.google.com/uc?export=view&id=1QJ43-ciIOypp-c1kuvoBEjwoDCwocuGq',
                            'https://drive.google.com/uc?export=view&id=1SeRXprWviPPNDkC0rbTh1aOGasXhvcZb',
                            'https://drive.google.com/uc?export=view&id=1FXdR8vgsc7tCKQLmE5M-M1RZAVxfcGgW',
                            'https://drive.google.com/uc?export=view&id=1ml2KyJMIpoo6vWFkOBB-usau-qpD4h1B',
                            'https://drive.google.com/uc?export=view&id=1KHjOCPvMPrzNjrnwT33UvVMoKNpa7is1',
                            'https://drive.google.com/uc?export=view&id=1O0501Oiv9GKxdXPEG0VKjl7wTVrI-6Nb',
                            'https://drive.google.com/uc?export=view&id=1VyGobTecLfCkdeMWsz379aXaGeXrRXjD',
                            'https://drive.google.com/uc?export=view&id=1vODy7HdmwtDxFABJulxnCgtaUENqHX0z',
                            'https://drive.google.com/uc?export=view&id=1EM24tFCsWvLHBc8of52M8PgRJVX3ROFy',
                            'https://drive.google.com/uc?export=view&id=1xUxctRxRCXLYscXjAP-e4Da__Vxi41Zd',
                            'https://drive.google.com/uc?export=view&id=1KYx6BWGfl7RNJrKeubKz2WG1-6AkgjjL'
                        ],
                        "portal" => [
                            "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/banner",
                            "desktop" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/desktop/16"
                            ],
                            "mobile" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-property/projects/pacific-al-marjan/gallery/mobile/16"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Pacific is an award-winning beachfront development on Al Marjan Island, offering ocean views like no other property in Ras Al Khaimah (RAK). An exceptional lifestyle investment, Pacific features spacious apartments with modern finishings, rooftop swimming pools, tennis courts, sea-view gymnasium and direct beach access.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  519,
                        'type'                      =>  'Studio, 1 & 2 Bedroom apartments',
                        'amenities'                 =>  [ 'Security','Built in Wardrobes','Central A/C','Covered Parking','Shared Gym','Shared Pool','Shared Spa' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  327375,
                        'emi'                       =>  0,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  '',
                        'payment_plans'             =>  [
                            "| 25%    |    Reservation Deposit",
                            "| 25%    |    On Contract",
                            "| 50%    |    Within 1 year"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  'https://www.youtube.com/embed/uGSY716NqDI'
                        ],
                        'degree'                    =>  [
                            'link'                      =>  ''
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  1,
                        'approved'                  =>  true,
                        'lead'                      =>  [
                            'email'                     =>  'leads@selectproperty.com'
                        ]
                    ],
                    'expected_completion_date'  =>  'Completed',
                    'status'					=>	'Completed',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ]
            ]
        ],
        [
            '_id'		=>	'zd-select-group-66743',
            'stub'		=>	'select-group',
            'contact'   =>  [
                'name'          =>  'Select Group',
                'email'         =>  'sales@select-group.ae',
                'phone'         =>  '97142463979',
                'whatsapp_link' =>  '',
                'website'       =>	'',
                'orn'           =>  '',
                'address'       =>  [
                    'address_line_one'  =>  '',
                    'state'             =>  '',
                    'city'              =>  '',
                    'country'           =>  '',
                    'postal_code'       =>  ''
                ]
            ],
            'image' 	=>  [
                'logo'		=>	'https://drive.google.com/uc?export=view&id=126dvr7nwlyBZ8DgLTKVnHp-EfWER91-W',
                'banner'    =>  '',
                'gallery'   =>  []
            ],
            'write_up'  =>  [
                'description'   =>  'Since inception in 2002, Select Group has forged an outstanding reputation for credibility and quality. Our projects comprise award-winning real estate developments in the GCC and Europe. Emphasis on quality guarantees our customers an exceptional experience in any of our residential, commercial, hospitality, retail and mixed-use projects.<br><br>With the development division at the core of the business, Select Group’s portfolio includes 20 million square feet of property with a combined GDV in excess of 17 billion dirhams. 5,000 homes with a BUA of 10 million square feet, worth GDV of AED 7.5 billion, have been delivered to date. Another 5,300 homes with a BUA of 10 million square feet, worth GDV of AED 10 billion, are currently at various stages of development. Astute financial planning, technical expertise and strong asset management allows the group to identify, execute and deliver real estate projects that generate maximum return for all stakeholders to the highest possible standard.<br><br>Our investment division identifies real estate opportunities where we can create value with a view to maximising returns. Market research and the ability to predict emerging trends give us the edge and we utilise our development expertise to redevelop and refurbish properties to our exacting standards before implementing a financial strategy to uplift revenue.<br><br>Expansion within the hospitality industry is another key element of Select Group’s diversification and growth strategy. Partnering with world renowned brands like InterContinental, Radisson Blue, Jumeirah Group and most recently Novum Hospitality reflects our continuous quest for quality within the hospitality division of the group.<br><br>As one of the Middle East’s leading real estate development and investment company, we rely on long-standing relationships to build our projects. With expertise in development, redevelopment, regeneration and investment, we are focused on a value investing approach and we work with the best in the industry – whether that’s suppliers, partners or employees – for unparalleled results.'
            ],
            'settings'  =>  [
                'status'    =>  true,
                'approved'  =>  true,
                'role_id'   =>  5,
                'chat'      =>  false,
                'lead'      =>  [
                    'admin'      =>  [
                        'email'          =>  'afaf.burki@select-group.ae',
                        'receive_emails' =>  false,
                        'cc'             =>  ['sales@select-group.ae']
                    ],
                    'call_tracking'   =>  [
                        'track'       =>  true,
                        'phone'       =>  '97142463979'
                    ]
                ]
            ],
            'featured'  =>  [
                'status'    =>  false,
                'from'      =>  null,
                'to'        =>  null
            ],
            'projects'	=>   [
                [
                    '_id'						=>	'zj-marina-gate-66713',
                    'ref_no'                    =>  'marina-gate-66713',
                    'name' 						=>	'The Residences at Marina Gate',
                    'web_link'                  =>  '',
                    'area' 					    =>	'Dubai Marina',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.086507,
                        'lng'                       =>  55.146775
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1lpdvaZR7Et10IxGUrHfoTDPuWPSX7Gx0',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      '1 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1F',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1aj20YrRtbx7rF-mMusTGx_mUHMF41yZI'
                                    ],
                                    [
                                        'type'      =>      'Type 1H',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1gqmEaXO77ZN9Rtx_K7Gq1ESDdJywk505'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 2C',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1pIw4NEjL2Rf8LOQWk6zUx3m9Z-pgm4fc'
                                    ],
                                    [
                                        'type'      =>      'Type 2E',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=18WnBGXhW8phZpNSiDZ905obB8YRnBS21'
                                    ],
                                    [
                                        'type'      =>      'Type 2J',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1AXPNulcCA7ZazYGS-oQ_PhDiPAescth8'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      'Penthouse',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'The Founders Penthouse',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1EaLJLRzsJ9vxMpA8zN3zYXIycEQbuKy7'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1taoUevtcfR-8d3ZoByVcWyQE7NXemrDK',
                            'https://drive.google.com/uc?export=view&id=1RWTXg2369PFFKUs7QprW8XqjX2NyeJ1c',
                            'https://drive.google.com/uc?export=view&id=1u-bRw3NLMsc6fXJ1uxr88brOjj9jpi6i',
                            'https://drive.google.com/uc?export=view&id=1UjxT2tUReSQLfZPuHuY4DxOpU-punEx5',
                            'https://drive.google.com/uc?export=view&id=15NXFRh7XSx4O4MyndxKCQYulsUAXKiCR',
                            'https://drive.google.com/uc?export=view&id=1qxxLWFt1ryHcfk73K1eopQH2ynBqElkO',
                            'https://drive.google.com/uc?export=view&id=14T5cfhTGqXgSrT6IH4N5j1gvjxkSihff',
                            'https://drive.google.com/uc?export=view&id=17xfTy79wrlqVG9jgUOkKIU5ROiKfyUBu',
                            'https://drive.google.com/uc?export=view&id=14hVlF1xxG5J0Dbp5lSPyanJ4r-bbWiAv',
                            'https://drive.google.com/uc?export=view&id=1s_R7cQtXdjvUV5HhRdPVm-EUZNLhP3uL',
                            'https://drive.google.com/uc?export=view&id=1KP7ptYz8S62unnRWGGmBssp2eVPnwh3k',
                            'https://drive.google.com/uc?export=view&id=1Vgzl-hXEstemaSVjZQTlczGsaskhYbGe',
                            'https://drive.google.com/uc?export=view&id=1TfehlCCgggF3jLhevzBvWI6-tFKfwVZl',
                            'https://drive.google.com/uc?export=view&id=1UYpGX8QS-gZvYGC62OZLN49FgAjDSFJt',
                            'https://drive.google.com/uc?export=view&id=1lpdvaZR7Et10IxGUrHfoTDPuWPSX7Gx0'
                        ],
                        "portal" => [
                            "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/banner",
                            "desktop" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/desktop/14"
                            ],
                            "mobile" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/the-residences-at-marina-gate/gallery/mobile/14"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Marking the northern entrance to the towering Dubai Marina community, The Residences at Marina Gate is a triumvirate of residential towers at the original gateway to Dubai Marina. Marina Gate has been conceptualized with great attention to detail. In keeping with needs of the residents, the building integrates all its functions into a sequence of inviting spaces, which permit a proper progression from public to private areas, from active to quiet uses, and from utilitarian functions to presentation areas. The scenic podium levels will house a retail colonnade, villas and sports clubs coupled with tree-lined communal spaces and infinity pools to complete the contemporary facade.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  519,
                        'type'                      =>  '1,2,3,4 and 5 Bedroom Apartments',
                        'amenities'                 =>  [ 'Common terrace', 'Gymnasium', 'Infinity Pool', 'Restaurant', 'Retail area', 'Sauna', 'Steam room', 'Tennis court' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  1516666,
                        'emi'                       =>  0,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  '',
                        'payment_plans'             =>  [
                            "| 03%    |    Unit Reservation",
                            "| 07%    |    Issuance of Sale and Purchase Agreement",
                            "| 10%    |    6 Months Later",
                            "| 80%    |    Upon Completion"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  ''
                        ],
                        'degree'                    =>  [
                            'link'                      =>  ''
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  1,
                        'approved'                  =>  true,
                        'lead'                      =>  [
                            'email'                     =>  ''
                        ]
                    ],
                    'expected_completion_date'  =>  'Q2 2019',
                    'status'					=>	'Under Construction',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ],
                [
                    '_id'						=>	'zj-studio-one-98736',
                    'ref_no'                    =>  'studio-one-98736',
                    'name' 						=>	'Studio One',
                    'web_link'                  =>  '',
                    'area' 					    =>	'Dubai Marina',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.068758450282008,
                        'lng'                       =>  55.12897610664368
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1J3po6_8O2wQc2_f113SDxrgns5VAV1Ts',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      '1 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1A',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1bzNPyx1VIpEDL2PtyoeM583X2E0mKD4H'
                                    ],
                                    [
                                        'type'      =>      'Type 1B',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1Vn44YTWYt9gOqOD5aOPdfMvI1YbmZUGi'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 2B',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1rjF3p6S3xd2FWu4MT_ZI9VrjKk6_IXcp'
                                    ],
                                    [
                                        'type'      =>      'Type 2C',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=11wDlThKv7mEN5CrCbkwAusb2LAylZ_h6'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1YeS_Il14YCWTKIeiX-nbUuCTvKWgZpDd',
                            'https://drive.google.com/uc?export=view&id=1QSnHG0TUhVJGkm7fnodVpPWc7zyHmxQ7',
                            'https://drive.google.com/uc?export=view&id=1k4JDOr9c6gAT1PTkOtAhJ9JsNwKOXcPz',
                            'https://drive.google.com/uc?export=view&id=13MLH2LfiOq_fzspKTC8AZK-nYVU875ur',
                            'https://drive.google.com/uc?export=view&id=12euhWE5Vt9iXVbe57ZL0jioLhBbuDOMh',
                            'https://drive.google.com/uc?export=view&id=1LD83HrDkpiKbnYZbRAZqnlfuRTiPTd8p',
                            'https://drive.google.com/uc?export=view&id=189uLIZKI0FFPBF_LSWdoQtK66PzTccrU',
                            'https://drive.google.com/uc?export=view&id=118DLhwBpzoTV-vwHGN6UCNARK_qfkz-N',
                            'https://drive.google.com/uc?export=view&id=1vDPxTZm98xPhzS-DC1OXXsmV8udRNZ5M',
                            'https://drive.google.com/uc?export=view&id=1uzKlkzVawyJXYeEOV35kL8yyHwouxGFR',
                            'https://drive.google.com/uc?export=view&id=1rf4FjwaL1sXkRO77EikV2Punbs_3XzPt',
                            'https://drive.google.com/uc?export=view&id=1J3po6_8O2wQc2_f113SDxrgns5VAV1Ts'
                        ],
                        "portal" => [
                            "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/banner",
                            "desktop" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/desktop/15"
                            ],
                            "mobile" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/select-group/projects/studio-one/gallery/mobile/15"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Studio One is destined to be a stellar addition to the Dubai Marina skyline. Spread over 31 floors, Studio One has been designed for the millennials, the busy young professionals, the first time home owners and savvy investors looking for attractive returns. This modern development offers homes featuring studios, one bedroom apartments and a select number of two bedroom residences. The carefully planned homes feature full length windows and highly efficient layouts. Two levels of dedicated underground parking, a breathtaking swimming pool and conveniently located ground level retail are some of the amenities that the residents will enjoy. From its modern exterior to carefully crafted interior elements, from prime finishing to diverse amenities, Studio One is the place to call home.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  519,
                        'type'                      =>  'Studio 1,2 Bedroom Apartments',
                        'amenities'                 =>  [ 'Pool deck', 'Sea View','Gymnasium', 'Steam room', 'Sauna', 'Retail outlets', 'Underground parking' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  961000,
                        'emi'                       =>  0,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  '',
                        'payment_plans'             =>  [
                            "| 05%    |    On Reservation",
                            "| 05%    |    After 4 Months",
                            "| 15%    |    After 8 Months",
                            "| 75%    |    On Handover"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  'https://drive.google.com/uc?export=view&id=16hzHgsu3e6T8qFfv5WkPr7a8ud3G5Wux'
                        ],
                        'degree'                    =>  [
                            'link'                      =>  'https://my.matterport.com/show/?m=x8rr8nUSATq&play=1&brand=0&help=0&mls=0&qs=1&title=0'
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  1,
                        'approved'                  =>  true,
                        'lead'                      =>  [
                            'email'                     =>  ''
                        ]
                    ],
                    'expected_completion_date'  =>  'Q1 2019',
                    'status'					=>	'Under Construction',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ]
            ]
        ],
        [
            '_id'		=>	'zd-palma-holdings-43627',
            'stub'		=>	'palma-holdings',
            'contact'   =>  [
                'name' 		    =>  'Palma Holding',
                'email'         =>  'serenia@palmaholding.com',
                'phone'         =>  '97144522202',
                'whatsapp_link' =>  '',
                'website'	    =>	'',
                'orn'           =>  '',
                'address'       =>  [
                    'address_line_one'  =>  '',
                    'state'             =>  '',
                    'city'              =>  '',
                    'country'           =>  '',
                    'postal_code'       =>  ''
                ]
            ],
            'image' 	=>  [
                'logo'		=>	'https://drive.google.com/uc?export=view&id=1uuZgDuHB-PrxzxVP_e2aosMBAws7bp8B',
                'banner'    =>  'https://lh3.googleusercontent.com/TtQtLia8KZdTvpZzp0zi55MYXxsHaQk9dd9rooFJ7zZPIh1gyV_iWEVi5a1xevKzdsSkCX5MFxKDhkWD4OUqt7AmZjDsl2kUiTYs78dnHZ2Pxerk6NoKJ1ltQRVKSfjlouY23FVOk5LA6JtVeSoOqLChocVIqTbr7aIvrE-nUKr4ZgMU3wxRaY7-xq74TBi251iGZ4KzJZ8AoHYI4qE9ADFY-jgSHW_825_KbaGSHB230IaMnjtNLAi17iOp8YkwTUiJfRuCiSlSdIDAgdfuBq0D9z1OHs29BsnQrG5FFuVKahNM64qAYXURZ1txFdjyX0zDycKTIBJX9LRWLlvJg7ZfLWekaHNi8OaMItSPy7OTnYoZlEDSv_Kcg_pm4KobLH74LNNwWME7ASXdanqxyRAbAc3bQXCvwz17w_aTVAsPG-5XboBouKJimfqnY1LlKF2mjywwNFdpAV7lsJCMGxpw5iIt1pW9Jjpshxcrgrz5dD83LdJMIlBf_SgbOWtSp0kO0vv1JuiWlIc4iVlBK1jMdAXQlt2VOkujxaVtsXM56Gz9EUJfRDiQETKf55Rk=w1440-h701',
                'gallery'   =>  []
            ],
            'write_up'  =>  [
                'description'   =>  'Palma is a world-class, bespoke developer and one of the most renowned and prestigious names in Dubai’s real estate industry. Palma has built its impressive $ 1 billion portfolio project by project, taking on one at a time to ensure that each is given the absolute focus and attention of the company’s team of meticulous experts. With each new signature project Palma aims to eclipse its achievement to date.'
            ],
            'settings'  =>  [
                'status'    =>  true,
                'approved'  =>  true,
                'role_id'   =>  5,
                'chat'      =>  false,
                'lead'      =>  [
                    'admin'      =>  [
                        'email'          =>  'serenia@palmaholding.com',
                        'receive_emails' =>  true,
                        'cc'             =>  []
                    ],
                    'call_tracking'   =>  [
                        'track'       =>  false,
                        'phone'       =>  ''
                    ]
                ]
            ],
            'featured'  =>  [
                'status'    =>  false,
                'from'      =>  null,
                'to'        =>  null
            ],
            'projects'	=>   [
                [
                    '_id'						=>	'zj-serenia-residence-43257',
                    'ref_no'                    =>  'serenia-residence-43257',
                    'name'                      =>	'Serenia Residences',
                    'web_link'                  =>  '',
                    'area' 					    =>	'Palm Jumeirah',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.132271,
                        'lng'                       =>  55.152677
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://lh3.googleusercontent.com/ivR8mpk3F1NQL22YCLHXYPEyHg_i63--LCRo7JdTwumB_Q8BhgHt07ZH_AHgx9cO8wt8Qs76eH6BXu1DyAgPQ8Cw8e5w4BwGnu_HXArUDncuVrSxmW82CakB-rPWY0OuQE73qo8yK6fAF6JfphBpPa4trcvwk-SBu6Va9SINex-juLWdN0P3PxhwUlEnlFUgO-o6Y03XWq4uW4atZpmgsUVS_J_nCpa0XzmUc-ot35uXalrrktEwLIoy85ByeM-idYQAmKv9Z8qdpviXyk6YJKlUFw5ukGigANkSJeBnJ5L-gyf7kBYSaErtILf-clVpvfUcux5jofxrwiM7cJB5Ol0JOmosvXpQg4wSl16okatZ8CK3NjfdAiPwy8fMm8pD2vvo-A01JiZKijIqabaYZqW_uqYWFQ5mYhrjreeUF5O6WMLtrbH8qS4PgH9KU1X0T0avNY_t3bQAydEjYS3c4FaQw7mFC-ZohiaUk3MAFxnwsapzZO5pfQvu9fEqSH21xdK0BKteDvjT6tg4sBZc3myTwk0nSyssYCaYetyo-DFweMBUs6MoigHRxB_1QHTr=w1440-h701',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      '1 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1ta2CZtS2k8nsuIz37MhmGIZgWfBqTsEs'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1wYZ7cQ1cvUdTtZpY5xel_gzbaeh32QNk'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1w5brXll8iiaOzgC-7cWNjvoeInXlTS5M'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1-aqGZsKb8nYapuLiFudkMHNfo1G5_eAx'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=18CmoP0uivQQuxEbSTI3K1Ul2QSjb3j6-'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '2 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=13fFq7uXxLSa19kmBS7eBZiNjDcEsw7Md'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1jJW6_a_22axofzudTMU79OIdbRj7_VKb'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1enUIv3WtSd_KPHEwEi8XIerbsRwS8n8V'
                                    ],
                                    [
                                        'type'      =>      'Type 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1b_uWHW-GOda8w4N4kjgZAhIc65YgXTxL'
                                    ],
                                    [
                                        'type'      =>      'Type 5',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1adI_J2OwHjUPKdbeWPqI5J9xhmezLP_K'
                                    ],
                                    [
                                        'type'      =>      'Type 6',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=o17qsg67-5-NgEim8eWHislj4l-387UNB'
                                    ],
                                    [
                                        'type'      =>      'Type 7',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1jWVzflHy0SgsCmoaYvsjOSpL8L3TFzSBB'
                                    ],
                                    [
                                        'type'      =>      'Type 8',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1YdIR3EOofyhf83SZoYNtWLl6DoGBCdD3'
                                    ],
                                    [
                                        'type'      =>      'Type 9',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1VKagLmZbomMUiz_KrxnCk-iA7iuFxKGz'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '3 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1aiyqEzy9NyIIm2Dtk5BTMOKfRXbzK2ly'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1D_xgKTf_4aLFYcBBwt7EVoj52HLFtRzC'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1sAoSMBos4PXd4vDpG6s3F4OqvDRvKYjY'
                                    ]
                                ]
                            ],
                            [
                                'name'      =>      '4 BHK',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type 1',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1-yoLlSsSqVAKHt5As3kgS1R1Rzp-6ugO'
                                    ],
                                    [
                                        'type'      =>      'Type 2',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1bGiNYBqbCWjQ-cPdgAa93OafTe3KReNa'
                                    ],
                                    [
                                        'type'      =>      'Type 3',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1TPWIn3c1TsfZKvyIGUP76RiNNs83-hZC'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://lh3.googleusercontent.com/fIXGhMbnKgRsxi4anltU8nzND-ifnkf6sy-cQM0JtnoTJgp8J5foTmMAU7VgDdlpo_AfAI0YLbFKMrZCQJnXJk7C_6z3_f7dSHOYO-NbzfBwxxtmf90imgW4gH656j_jylULWvYoMZhQ1CT4NvJEmRrVOhN3c-CfZBG30OO1oz1kATRiESGYWDnLj46p0eX5UZBnGLGdfwG52sSxNR5uprvxxHtjKFPqlydsjz1PRrK29HGOJ0uowx9l2NnP4Ix1vzmpkHTCy1QWXfHTHzNaNHNnlwhmGcapVK8uR64TzPGZviwkvmsz23ZHkZ4fg62rPCMO-3pCiKrxMu5ZO2zlVEccVpXuoYRqBG18LX7VeZBWD8lPxG_ymAisX_6YuSFPH5fMPrnK6HaF_UPHBj-gV9DX426mT41isxn_kJKuy_nrK8ykks2h8_YTAioauEvMMLP7Ahd_Wz51-Ugg3eBr3MM9bvd7ThaNVibnsyA9ZdwhscTFdBOqFffjE9aYFOmgueJAXIzIw0hQ6mB-Q1HccnDI6Nf-d7OF15sCynSRHHT1hIytxd4vJQf2ihBxYzE0=w1440-h701',
                            'https://lh3.googleusercontent.com/qvGj9WXT5fFXIfrZVUlGOem3H40_1Ocwq-M73CrRUjinBZ6MVp6ieHue3sjXziMBFfRtSXDDGjPV0gEG7EXrX2sPDNpfvJe3-i0gEL2GgnqcGgcSDojHQem1qInATvY1gQPHpQSo4-mLYB3m0EJrvN0JjMFbDrq1xYRNFbphppP7k3AVOpYhUnlTBQK8bHCyPDtxWmuDSWnKV5XtxKUuTlXF6C3c59wDTKKWAADHIGJ30O3vXjH49_banGNa3PUoq0PGShD5VbCLQ-bzav8LvjqwxKpJNbsKTaBkRdcYuuFFBAjA05z1s2mt5rAjRZuWW7Kjd2E9FJeYUQTyyX64vW0si_7h5Zbx7S5t4bwOtOsUyBnq2MJpSRLulIsfj7gtPwG_VafpiW12vMDvi5gx0rstf7ib5XgFErt01kqKaqf25JADiYtV3JqTfWZQDDvROUbmW_R6KcfEka15I4bR95hPnoHAHM9e4JC9zgR4T-WP2YrYHdBScuf1qkUmG9BJOrfk_mzgtI41h8L2QJ1OxOm4el7FW58JQZRps0aURpVSsSZ84E-sQS9zqtTZJXek=w1440-h701',
                            'https://lh3.googleusercontent.com/YlO2ktnDukekvp0ySL3gFa0oyVXAlpuCD8e_AwNEyxQCz0BeHAFl9Su5UzVCkJqsFt1iIYdmFqNitcL9xhh6BCmMjTp1IKsS_lnC1AMkW2aMPaJ6p0-d8QdzsASZDPrcRfkMP4x8JVDxjl1E1Odyvu3ENMUjm4X8taIZ94HqT86G8b7D_94KJDZyoRo5li6YWAhmF19ih1fBcxb5aUAtdV-IcvZdHmy8qVC_MQ1OOQ0ILQMLe99lDLLJ1qPTPiX8lrk5HKY3tLwC4pnk66Mx7Py01p-OhoqWMFs5WvXNSq2963PEB05r7G0NrcVAgeFUwmyNLgihmnVttLGk-7nwAhkZsGiSfGf43vNVpMhTpmnJ5VAclr8VujHJVhAdaKG7a9MJdpjXF6kDuMBPihKLFES1LUWRHhggiHI86Zlt-AbUHYo1HrfCfPcYDQna82_UoHswGtJXVXxhIKlprW19bdaQTwJtWQ2HArtNKjOlZWPWBr73UOqjeCLlzWna3CZDNYq0_4XQzD6GAyI2bpvxO8voZP-wDaLOE0v5oLjZ9WSBxhWm4ykNJ6ZUtIuCSFpi=w1440-h701',
                            'https://lh3.googleusercontent.com/Qotj7LLqD9xx_WJCDybY1g0HjNe3foCBteH4vmo7MvjeexpVKN1BXEDKZBNdnpu_usgSYkFs9GwxGFo4cg3Zjkj5_eKSUak7bLh-MbE1Kaa0TRIz3AwOCQ9vH4gya77mSk0W4eYR8DO4Xw0mofmX_idZw6ZhJCrRaMDG0rkLafvATAnbbJhftp2nAE-DeZJta-VliGJI08PhJAkkGazGojtw9uSGhgXtvQ6RlaCQCTq6KHSdXdzbGRe5adPQbSZD8TSNtIRmaDZIeCMOdVfCVThON-qX8qvYvkITN2ae1wHeIp2CxSOfblJB3SDlpztqrWJBr1I96AqCK7LmaWl3rWvkxpHjAJVcVGBvmQ-HArjvacWmNQLF3EpREbBHZ2gBpZF9XV3yYgwqEglKeD22vo9vVW8klhorLzTgQM7Qk2Bxtp8gUsmE__UIJZ7qBezvI0FFNJeeJeZsVfPDK-AHe5xCx_sFGwdQD5lNpv0hMF3FI3Cx3v1LZyfSEV8nbM6JHGCIOFw4gBsrjG-6B3M35PfxpHCeU-D9AzeB9c9B3kagRCvvOauibWvkcBkX8MXg=w1440-h701',
                            'https://lh3.googleusercontent.com/kT-6jINPUTQndHsRZNhNJ4jLIlEVkWQTYx7pMqgG-NsIqF8ZgqKfeHQMxSEUQIy197c46iDkSgP3wamLLdkN7AMgWK8Wtd1ihAUEhC_AtApvw49dFAW_nYk9qD0B7Tu44PSvZ1N42zLPGkAebQv2sNsBOCKPUU2c8EXMqWkRaK9_Ny3E28hyzYsDaJrffNxtw-liQllyMkSNzLYtp82ude_ZJX25nFBwcQjSqnRxhEHsJ6baz6ruxcXKIqRAAv1HLV4vG3xX-09AuoH6yyWTAkoz1du3JtyeQoCmreE6WTMVz4buRFO55x-B9HaKHaW_ZaeWgPLC6Ql5lA87LmcnBsYfKo9Nri2qFVSiT4F-nZoI0GEmf55NsgFhQjus1CPge77Li597zMFCMCz_0YzfqX0tCSRKpha_oi1t8ec1Ten3qGISfWx-rTQEUVmLlF9NG8K19aQ25Vg8NLHif0jstLkURd4kkLhUWJmcoYiNczOpSTJhE3v-P46qwBWhfbIY9zZuH5Vi0g0iN2Ne8r8dAnypsFv7J2FuS4ioYUQhGZ2DQX995P95rdYtxUetxRKg=w1440-h701',
                            'https://lh3.googleusercontent.com/Tp6CkVm9u4Q4E3rQZnTK-CO14UhMoJYd01BEv9kess-MrdL0DM6qRjfSzjc5Zn8QgdifFEvzBFqNOwSxBbTcS984KQdsxH5-jTJ8loJnwL9EvKXuC_rE2N905qV9WqXe1F62XecQD5IhEYOp16F2TedeDXETfsHerAzzBiXTssfYc75wtduC6fdydAJ8Tc4s4tPkJBe9ThVUklpHBcrNlYXuT734yIQQ1BENla_6PF1YBC1wUHrjszU8v9l1NwVyibCbj1q2f94TeMe-FOQwh649zu6bUuusTviVFzJT1cEJyRw7n0FdQQ81IU6jukXp3YfTCrC35WKlQmJJ8XvVX0iZEi2tYkn7IwwPwbvELUoEVL1_Y2DFoFApVstrLHgVNJtXX59e-fWUDA6YTgqJs-04bvLUNIHbQkq0EXi6IsfF6eNIJA6BMh-M3sK-AGdHha1TpkUTtWPeJPaMoYT6rZSeXQeioRSx_7H50xCVzIPPbr5ayQeANxW3mu-IAvi_1cBamACvxVpZRWPL_mURXYNCJUeloSTaUtx33vaKLH6_c3H7U--rhNRsmXDDiwJ9=w1440-h701',
                            'https://lh3.googleusercontent.com/TGzGinLoYJ1C5UmwVDirlGi0k8h5C1NzbPNZiP0bK8kSWO_hxp--InbW7Kb0yPSdKNLWimWTZxGjVFe-CFMYyVXXKLfgIMLitqm8OYJMSksYSnioyv0V4yoJdCjLen4C6w1dU-dJ6783ExA24ss8vsJjY2brNFi9WPPGWRQTtPqLtsG12HlB3qCQPjPQ0-OogJAFsdjYnHbeGLP0Oc8y4WjDz4SHl6GoZvI_gClK12YAxnwWIr06YUOM2rQZTLsVeBme0QpFKn78iRw9QcGFOloGrigle-sIfkTDCy2clTtr3k367FIN5HaD6DPR5fk5mXgw2fcHG3QAPA8azNk830CRbC6sTRRhOgXiFWpykfj7tZSv8tiEEz9YcQK850DYICR-PPVoSG8fJEr9kJ4OQ4JpEBAX8a8eyckjiF7PDV6gKnxgAo7hiX9x3nw-9AqDOLQad7OaxvWF7WJGAaEFF949GLDdaTVCppn8fH4Y1b_uNl7C_f5uI5VrdORko3T65moFYw-BDeAJK8_esLJVKHIZ5nHnHmBW0ih7HZYr1wb1wRpnoHsjxvQBsIvVMFTz=w1440-h701',
                            'https://lh3.googleusercontent.com/IQU5mHecDTKRzrDppkUbZLoJYNMQyBqkyfZpxSOsyZg6rn3SsyTfxcxxu7iEQp1U72TV3kr5lyWhx6ugZDOZHFHQjeOfia0H54kzFPQD5_6zfQtbV3tLoHNvFYIOWQjdxk_fyjO-wliW1W8T51FnVUdmjltF0Hr271nNm9wJO2tkopvcLoxQqVDQrIBDbk6ONTWhzXCsnKzPO0JvBXaRuwioTv6P44UDJDQqDJXMWQz8TYA_hP1t9XJMD-uV-Qheeswx92IrvIoGAT0TaebRZBpnMXQuMWoxu-DgLBJZGw_iYQ1rMdMGSeDSfZNYRr3HDeKoXYeZWB_QTI2fJFPpSS207VCdFsIPwlg-Vn0lrW9zkdRTx_FxPLdgyIhd0VPiNQvnUPPr9bWjYu3GnXu5Tr_mbeLTGjvIr6t3tzjEbI_XJDAPw83Qqp0MbX5Zz_rE6ZMT6ebXx9Jgr2Skp4RboEAcP8zb09Gs1lON3oduc5BN-9_861lyZNLnkRpFFGGrhq7gQxu6KtCkljjUKxwnj-5lMzvSHD3I-ZIQnw4zSuU98BQHjvVXlfj52D1w2KKS=w1440-h701',
                            'https://lh3.googleusercontent.com/bOxs_MDAqsHESrOv1zHVeRdphnvWkoDWoFn5KKxt-aVkuIGembYQaAj9Pny8M_zAoAAd8PUZZ4vXNwIOoZVt-ILd3iFyatTVs8exCJJem1In7nvvAaobDCeb0XEwIBs33MF2Zal1xmWQEkvdFDwSTPcaeNCMtqu53appVCuyzVwNTJDt9lqgcgOkNJzfp1bUR8UovdWo3jrA4CWpAwxTxSlE_GSBbZCCG9yL44WVEMD2vcHmx-hjwK7ZITbfxJ56bBRhJhrXTIY0NBHJOw0GQJmw8R3ff4kOX0LYFtMSKTI_YDeyHQoSEkpsEdwFurjFC9ZDZMWwIlByeyk6kvpheOOPzAAueRn_sdZFjKgH8UfbbTqSksPJvJUKDNRnfRz4AS47Hykh9Yhqep8vVf2pbSEFqzovEZ5f_CsbF8yGaOc-NdsX788jtgfA6s0Sb62LdigpjSTcVJmN3Qmja_p1ub1TCkm1KRZhfwWK0ynBspnDb6LmxesSSyul2_jSx513m11SQWvPalKvbmHMzgtWSf06YZq-wt4wWmfzbWR6mO7TWnkljCR7YxlrmTrVbhUF=w1440-h701',
                            'https://lh3.googleusercontent.com/PBLNZel6OHLZsd-mb523N5RsQG0T66tePQnxACtOv1gamxddeqNl6wQvLzBw6rZiYa6WX5HiJCv_SSXY_RFIB_0va8GD_8EF3TYrlUvObDL74C-FH9Vyj2X8UunjYgicJd8LdPrgtC4A8nBitpxNOEvgDJH2lzqly-9YHTBoF6zIw5zEHLDzdkrUQbwH6t9XciC4swBUQwnLl_md7JVlpwvs0KU_bX06Fg5J3fZieMzucyvX_sslxKnWZzbWbcF4h79XOEThVuo-LqTiGVKq2-4hEIGZdc3NNGKFOLSW9ttX3XK3Tj4wUgnvANiUxe0_4i-EU7mvfk3_QozQ2q8LAVv3sxbXhMWMcHoisfi_tZ5pyTNO1QZtxc1QY9aeKonKnxO6vL1dNCthwMNVae9RyTwgNS90cXDdjRW3bgqv10OWWVouu3_4BoT1wKkv0gU54U1_CZuLZIgWUYcO5kgKyDDESrDBWtcI86OFo513jEDQj4T55HWdCNrXr6cNJfb_VCq3EFo4wE_eVetmE0opFu_gVegiB7omFiH3qEXOJ1syCpP7fSFBhDucZUthq2ZN=w1440-h701',
                            'https://lh3.googleusercontent.com/OTDLYd1b1a0YEqCY3rmOj41t8sPCDEV-Uoiu2keP2HpZQw9nOMT-X36-Qm2-tctA9aaEQhK1Cai_zLGE-gvWsnxHm6QXR-bRDvZGl1XxWzyKnwebybaRvMhNNyoD3g8ePnrbere497xVAkmGLaDHVWqPq1PRrMMRgKwnJnQbx-gBx3AEONVbIfrs-HUh3So9lCFy9wwuSRG-7XXgbKLx5e35YatPkUtHBkvNN2sFN-uFxJW5OVDlLw_ProkxgXruANYc_ZqFzRNm__KtH57aUCQBkDp69q-vg-xChYJ2wYa2ECu-10XyGVK52zncpIuPVCMHHxwR5YgueykxjdDq_aEdo35-TAEUarE9wOdrE8WOPqu_3eXKXO1RvyvLqvQ66u3RliZfM9VIphkEysxSuxohzXtytLX-khfyrI1AcbMeNtWxSSSpD8uBqgLwR9n0OsjlXAfWuvWPI1ZOk_PnMZEdf_s1OCptnXkPGfCRsnN3rUSrb44dAMqn2ziFUHjmQbqykQw_hKrsTLNPXkNdSvd4vd1i4ezVDOz1TJwkCI51mvlL4XifhJoHRruNpLBj=w1440-h701',
                            'https://lh3.googleusercontent.com/qMffvBFh9uBYfzRSswGiB5lIxIdFJqLJ6QUJuOV8nAouma4Qy6YSNpJvlQSqWdtLXzR7jYauYYFhjgGCiVIkVpVIQOJ_h_3LBFty3dtW4It1BMTUa3W5BeCfESE_85UwadWfEkMwQmRP_aVEPK02fyyTvg-doX0nMukML2iBPyL3LLSYna8olVLStN_NmS6Gjr17TzK2spsQlju3BgWbtiGgB6U1N3MSpzkXxk2fJJZqs8ClW3eCOnbclcmgadWY1iUjbuuw8YFWje-jMLTcNZuG2D9RIz1G5UbkZJQnlvnVMIZ0_LBJbtU2mtWS5A1irwokKzzkuH-thzRKmVHOTtN3mcmpv96TCeVeJvIXtNZxw-7gKDI_EZqyQtblogQr_zHJyQqfuHGaBcJyxXUTEdGupnPKk6rwojT3NbuRX_JxLA0gB8CRfmRoQb7-I9u51mX5xQnjFygvl-p19DowWruyN3BZ_96D1wdbycapNABl1NhQ88qqrYmQPkBWg4eldDerr2zCAAVXEmx8lL5qJ-b96UeFpeIk3qSX5J8QcdzuW9E-HUmM5FH0Av0kCDvK=w1440-h701',
                            'https://lh3.googleusercontent.com/ivR8mpk3F1NQL22YCLHXYPEyHg_i63--LCRo7JdTwumB_Q8BhgHt07ZH_AHgx9cO8wt8Qs76eH6BXu1DyAgPQ8Cw8e5w4BwGnu_HXArUDncuVrSxmW82CakB-rPWY0OuQE73qo8yK6fAF6JfphBpPa4trcvwk-SBu6Va9SINex-juLWdN0P3PxhwUlEnlFUgO-o6Y03XWq4uW4atZpmgsUVS_J_nCpa0XzmUc-ot35uXalrrktEwLIoy85ByeM-idYQAmKv9Z8qdpviXyk6YJKlUFw5ukGigANkSJeBnJ5L-gyf7kBYSaErtILf-clVpvfUcux5jofxrwiM7cJB5Ol0JOmosvXpQg4wSl16okatZ8CK3NjfdAiPwy8fMm8pD2vvo-A01JiZKijIqabaYZqW_uqYWFQ5mYhrjreeUF5O6WMLtrbH8qS4PgH9KU1X0T0avNY_t3bQAydEjYS3c4FaQw7mFC-ZohiaUk3MAFxnwsapzZO5pfQvu9fEqSH21xdK0BKteDvjT6tg4sBZc3myTwk0nSyssYCaYetyo-DFweMBUs6MoigHRxB_1QHTr=w1440-h701',
                            'https://lh3.googleusercontent.com/Y2Wh9TdLPU0sOHvcOAKoP1KdBwHH6N6wUtT2XbC0b_jW3FcNoBBkL7a1CCnizRpB5Q3ooNHPc-FDgV4mvU5wThpjvR1Fq7sGNXltqBvbSVdwGbGGLfl9YdSV9N5RumQkWbBAKzPzHHTMtu8YcO8vSwAHXeq4BNjwca3Tmjx6nQDKstvJnj47D76g807CjO5JNRDFH2DeT007F9ivK8OebDxeHPvA9Zs_u1NzbP_Il-uRpyk6vqCSNqfVrDbD1pi9akNqrQYEdYVSgGgZVeVL25ppCPOr0kqNJuW8E4WIanFcvnlGryHIhFS08HfpWZnmkNbdQndnz6s-IzTmqGVOALl-_KOmteH2MwplqU8I61nz3y6PCZJr4RXAy9Zv8ThnEtZTSZsLqWTgVRupakza2KyU25Pg38sFvJpEI7PpcOHMfOE0VQOf4Fi3oPq27MsqMol0nCrs7GkVuEw_MmoT5pMtnKV6KtUrjcYUAWfkZQwwRzjyyj4XUpFDVvFjZvOoG1bePtXpWVKyubtEgCgyp41aC9U9XS-y2vKN_48Va8M9G_TPtc1eOOCipXZI3_Kp=w1440-h701',
                            'https://lh3.googleusercontent.com/QdrsvMLVhhCEp69F7XMUvcEQp-RlEqYpCZMLdj5S-8C3PTatf0ilTVUvttB0StAiBma-iqqGRsp3CzdJV-It9AL6q_cFpSiX9UgJn0usqLGk54HQfNmZMEvMK0ZKWbx0EnYh_0r6mtDuruCtQz1ptEUhZfIyiVFJ9FSn3AsdcaHJBkr__ztnLj10cIpRyrqgHft8gbeIwSx-5wfaa0eyQt1qjQuBVFQo5mVsnfHVVGKbetyr79EfOepkB3SrQQ1XKS7CShMF13nCPT5C9GuMSx83B3551Z2og45uLIx99qHC1Og5TNyKMJqASu5tPcYC5B-UbhA8Wh3Nz6uKXd1wSXgk4TR7ch6qiGNqll-NiPApub-J8g6_FgdPDlmKqpi4e9Gxl-zVvqWN2uSD2D9pG4iR4Gb7igJLYhr3i5-DjjeEDKbbs3BmEkDQD4oDD_EDLwMkR8CaYZKaKUgNjzYoNfJtNw12b-msT93YhnzfpvvetvkD_Yosk02z_5HxfkN9TVHCA_b4G_0NL3Ojhr5lk175HfRGS73mL5QXqX-tmmR7-xg52OdkSCs5qLwrxrsq=w1440-h701'
                        ],
                        "portal" =>  [
                            "banner" =>  "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/banner",
                            "desktop" =>  [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/desktop/14"
                            ],
                            "mobile" =>  [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/palma-holdings/projects/serenia-residence/gallery/mobile/14"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Serenia Residences is an exclusive residential community development that features beachfront living at its finest and promises an unrivalled lifestyle, inviting residents into a world of architectural excellence, contemporary interiors, with an array of outdoor facilities and bespoke services. The development’s prime location and its wealth of high-level amenities has resulted in the most exclusive beachfront residence in the city. Designed by famous architect Hazel Wong, the Serenia Residences project is exclusively residential, and features contemporary architecture and layouts unique to the Palm Jumeirah. The AED 1.5 billion project comprises of 250 units, and encompasses apartments that range from one to three bedrooms and penthouse suites.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  0,
                        'type'                      =>  '1/2 and 3 Bedrooms, half floor penthouses',
                        'amenities'                 =>  [ 'BBQ area', 'Cleaning service', 'Doorman', 'Games room', 'Garage', 'Infinity Pool', 'Jacuzzi', 'Overflow pool', 'Prayer room', 'Reception service', 'Security gate', 'Valet parking', 'Video security', 'Lap pool', 'Putting green', 'Beach access', 'Tennis court' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  2500000,
                        'emi'                       =>  2500000,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  '',
                        'payment_plans'             =>  [
                            "|| Bank Finance Available"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  ''
                        ],
                        'degree'                    =>  [
                            'link'                      =>  'https://my.matterport.com/show/?m=XsP31VhLMu2'
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  1,
                        'approved'                  =>  true,
                        'lead'                      =>  [
                            'email'                     =>  ''
                        ]
                    ],
                    'expected_completion_date'  =>  'Completed',
                    'status'					=>	'Ready To Move In',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ]
            ]
        ],
        [
            '_id'		=>	'zd-damac-17493',
            'stub'		=>	'damac',
            'contact'   =>  [
                'name'          =>  'DAMAC',
                'email'         =>  'marissa.gonsalves@damacgroup.com',
                'phone'         =>  '97145205101',
                'whatsapp_link' =>  '',
                'website'       =>	'https://www.damacproperties.com/',
                'orn'           =>  '17493',
                'address'       =>  [
                    'address_line_one'  =>  '6F Damac Executive Heights Bldg., Al Barsha South',
                    'state'             =>  'Dubai',
                    'city'              =>  'Dubai',
                    'country'           =>  'UAE',
                    'postal_code'       =>  '2195'
                ]
            ],
            'image' 	=>  [
                'logo'		=>	'https://drive.google.com/uc?export=view&id=1LsDvp9p6mzIiofZpmmQANHxueiwIAFL2',
                'banner'    =>  '',
                'gallery'   =>  []
            ],
            'write_up'  =>  [
                'description'   =>  'DAMAC Properties has been at the forefront of the Middle East’s luxury real estate market since 2002, delivering luxury residential, commercial and leisure properties across the region, including the UAE, Saudi Arabia, Qatar, Jordan, Lebanon and the United Kingdom. Making its mark at the highest end of stylish living, DAMAC Properties has cemented its place as the leading luxury developer in the region, having delivered over 21,700 homes, with a development portfolio of more than 40,000 units at various stages of progress. This includes 10,000 hotel rooms, serviced hotel apartments and hotel villas that will be managed by its wholly - owned DAMAC Hotels & Resorts. *Figures as of 30th June 2018.'
            ],
            'settings'  =>  [
                'status'    =>  true,
                'approved'  =>  true,
                'role_id'   =>  5,
                'chat'      =>  false,
                'lead'      =>  [
                    'admin'      =>  [
                        'email'          =>  'marissa.gonsalves@damacgroup.com',
                        'receive_emails' =>  false,
                        'cc'             =>  ["portal.inquiries@damacgroup.com"]
                    ],
                    'call_tracking'   =>  [
                        'track'       =>  true,
                        'phone'       =>  '97142463205'
                    ]
                ]
            ],
            'featured'  =>  [
                'status'    =>  false,
                'from'      =>  null,
                'to'        =>  null
            ],
            'projects'	=>   [
                [
                    '_id'						=>	'zj-damac-hills-73927',
                    'ref_no'                    =>  'damac-hills-73927',
                    'name' 						=>	'DAMAC Hills',
                    'web_link'                  =>  '',
                    'area' 					    =>	'DAMAC Hills',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.027193,
                        'lng'                       =>  55.252485
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1Kz2j3eXF_dH3tgNei1-uCaECZhZpo2m-',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      'Villa',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Type V2-F',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1AAwIXRRdguQ6eMrlceMaYfIaAp8ZCjJo'
                                    ],
                                    [
                                        'type'      =>      'Type V3-FF',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1WA63uoz4xd2aRHTn-u6boneyjamfuNJj'
                                    ],
                                    [
                                        'type'      =>      'Type V3-GF',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1SP1b_s0W0igGwkofFxLe1_TrOHlyAIZn'
                                    ],
                                    [
                                        'type'      =>      'Type V5-F',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1B849l15xImBU3y7ORykT4JCkr_P5uFZf'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1i4QQ0Dvw3CETFdwf44aYOV-p2WqPubPW',
                            'https://drive.google.com/uc?export=view&id=1R467bqHpN5IRcA2AyPjdrAu0y7h3tV9V',
                            'https://drive.google.com/uc?export=view&id=1C2HYR9jyjDzosHxcqv2T9CA17JwQ3dhC',
                            'https://drive.google.com/uc?export=view&id=1CFRDvTNmBSMavPUjSrpdE4VWXKQQwxbn',
                            'https://drive.google.com/uc?export=view&id=1FH4l7cUT-CFugEPEJ3jd2FINNnnHyxKx',
                            'https://drive.google.com/uc?export=view&id=1VfLws7gzbBX6nb-PvDO8HHLLN5dpibIF',
                            'https://drive.google.com/uc?export=view&id=1gFO_irlg_WD6y6DDKyKTqiK0DsMKr_21',
                            'https://drive.google.com/uc?export=view&id=1B2oS6UriPVSMzFv0DUa7csXDe4zdif1R',
                            'https://drive.google.com/uc?export=view&id=1BQdg5mPkaUT9P3k7U1Gclz0UtZ-_w-jC',
                            'https://drive.google.com/uc?export=view&id=1R7GNQqsYxx0iEUvUo1n4x4kzHvdAnOJD',
                            'https://drive.google.com/uc?export=view&id=1F_AacoZ7DiCkKtvd-lgHo2OAYwFDRW2x',
                            'https://drive.google.com/uc?export=view&id=18moqott5QmLVUnZIL7LZafST0r_77YGB',
                            'https://drive.google.com/uc?export=view&id=1q0_heXb8IMn9VFdxnKzDtisxeALc-Jg6',
                            'https://drive.google.com/uc?export=view&id=1MRNJEs8gSfNeCLCN9SfjAC2zoiOrx9WM',
                            'https://drive.google.com/uc?export=view&id=1inmO5zLEvexcKiJHuRnUaKqWuuGHdeXA',
                            'https://drive.google.com/uc?export=view&id=1MioEQWpyRBTXD_s7Hf1qNif5sMiPmdXj',
                            'https://drive.google.com/uc?export=view&id=1P1ubUY1YFpmkI9KkMIBXldtYTVA0f2y2'
                        ],
                        "portal" =>  [
                            "banner" =>  "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/banner",
                            "desktop" =>  [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/desktop/16"
                            ],
                            "mobile" =>  [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/damac-hills/gallery/mobile/16"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Spanning 42 million square feet, DAMAC Hills is home to a variety of incredible properties, some the result of collaborations with the likes of world-renowned brands such as Fendi Casa and Paramount Hotels & Resorts. Mansions, villas and apartments all have access to world-class retail, dining, entertainment and leisure spaces, along with spectacular outdoor areas in the form of the Trump International Golf Club Dubai and The Park, over four million square feet of lush greenery. Just a 10 minute drive along the Umm Suqeim Road, this exclusive community boasts easy access to the city’s major road networks, yet it feels like a world apart.'
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Villas, Apartments',
                        'count'                     =>  519,
                        'type'                      =>  'Studio, 1 and 2 bedrooms apartments',
                        'amenities'                 =>  [ 'Training academy','Spa and wellness centre','Supermarkets','Banks','Trump International Golf Club Dubai','Little Village Children’s Play Area','Outdoor Skyview Cinema','The Arena – a seasonal multipurpose area', 'Schools', 'Nurseries', 'Pharmacies','Laundries' ]
                    ],
                    'pricing'                   =>  [
                        'starting'                  =>  544000,
                        'emi'                       =>  0,
                        'per_sqft'                  =>  0,
                        'payment_text'              =>  '',
                        'payment_plans'             =>  [
                            "1st Installment + RERA Registration fees | 20% + 4%   |    Purchase Date",
                            "2nd Installment    | 10%    |    Within 90 days of Sale Date",
                            "3rd Installment    | 10%    |    Within 180 days of Sale Date",
                            "4th Installment    | 10%    |    Within 270 days of Sale Date",
                            "5th Installment    | 10%    |    Within 360 days of Sale Date",
                            "6th Installment    | 20%    |    Within 540 days of Sale Date",
                            "7th Installment    | 20%    |    Within 720 days of Sale Date"
                        ]
                    ],
                    'video'                     =>  [
                        'normal'                    =>  [
                            'link'                      =>  'https://www.youtube.com/embed/aUsoTDchvAU'
                        ],
                        'degree'                    =>  [
                            'link'                      =>  ''
                        ]
                    ],
                    'settings'                  =>  [
                        'status'                    =>  0,
                        'approved'                  =>  false,
                        'lead'                      =>  [
                            'email'                     =>  'digital.leads@damacgroup.com'
                        ]
                    ],
                    'expected_completion_date'  =>  'Completed',
                    'status'					=>	'Completed',
                    'timestamp'                 =>  [
                        'created'                   =>  null,
                        'updated'                   =>  null
                    ]
                ],
                [
                    '_id'						=>	'zj-carson-damac-hills-74545',
                    'ref_no'                    =>  'carson-damac-hills-74545',
                    'name' 						=>	'Carson - DAMAC Hills',
                    'web_link'                  =>  '',
                    'area' 					    =>	'DAMAC Hills',
                    'city'						=>	'Dubai',
                    'coordinates'               =>  [
                        'lat'                       =>  25.024703,
                        'lng'                       =>  55.243274
                    ],
                    'image'                     =>  [
                        'banner'                    =>  'https://drive.google.com/uc?export=view&id=1nfnczxfCwyCOC0aWznuoAu82aur7bseY',
                        'floor_plans'               =>  [
                            [
                                'name'      =>      'Apartment',
                                'plans'     =>      [
                                    [
                                        'type'      =>      'Tower C 4',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1dTNQLP-UgVPrI8u2FT6deb51micjcEmr'
                                    ],
                                    [
                                        'type'      =>      'Tower C 5-15',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1Lw_eJ1TyBRkq_f2vvPGcLMXjJe0mwq7m'
                                    ],
                                    [
                                        'type'      =>      'Tower C 16-30',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1FAZIUFOWtsyMg_LWLCi7swJHjgAky5NC'
                                    ],
                                    [
                                        'type'      =>      'Tower C 31',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1MI-eShQyCtCJdjeZGaZfH8qrhA-a77aJ'
                                    ],
                                    [
                                        'type'      =>      'Tower C 32',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1kLMKEOm4itnrdZX_PBb4mzcH7AMQ4pKu'
                                    ],
                                    [
                                        'type'      =>      'Tower C 33',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=1i1zdHEMQ811c5hYdIHhgkvJaoTwIXxHb'
                                    ],
                                    [
                                        'type'      =>      'Tower C 34',
                                        'img'       =>      'https://drive.google.com/uc?export=view&id=16SWtn3yVJ8sMTMCLeU4iQjyGsFGoqGOW'
                                    ]
                                ]
                            ]
                        ],
                        'gallery'                   =>  [
                            'https://drive.google.com/uc?export=view&id=1i4QQ0Dvw3CETFdwf44aYOV-p2WqPubPW',
                            'https://drive.google.com/uc?export=view&id=1R467bqHpN5IRcA2AyPjdrAu0y7h3tV9V',
                            'https://drive.google.com/uc?export=view&id=1C2HYR9jyjDzosHxcqv2T9CA17JwQ3dhC',
                            'https://drive.google.com/uc?export=view&id=1CFRDvTNmBSMavPUjSrpdE4VWXKQQwxbn',
                            'https://drive.google.com/uc?export=view&id=1FH4l7cUT-CFugEPEJ3jd2FINNnnHyxKx',
                            'https://drive.google.com/uc?export=view&id=1VfLws7gzbBX6nb-PvDO8HHLLN5dpibIF',
                            'https://drive.google.com/uc?export=view&id=1gFO_irlg_WD6y6DDKyKTqiK0DsMKr_21',
                            'https://drive.google.com/uc?export=view&id=1B2oS6UriPVSMzFv0DUa7csXDe4zdif1R',
                            'https://drive.google.com/uc?export=view&id=1BQdg5mPkaUT9P3k7U1Gclz0UtZ-_w-jC',
                            'https://drive.google.com/uc?export=view&id=1R7GNQqsYxx0iEUvUo1n4x4kzHvdAnOJD',
                            'https://drive.google.com/uc?export=view&id=1F_AacoZ7DiCkKtvd-lgHo2OAYwFDRW2x',
                            'https://drive.google.com/uc?export=view&id=18moqott5QmLVUnZIL7LZafST0r_77YGB',
                            'https://drive.google.com/uc?export=view&id=1q0_heXb8IMn9VFdxnKzDtisxeALc-Jg6',
                            'https://drive.google.com/uc?export=view&id=1MRNJEs8gSfNeCLCN9SfjAC2zoiOrx9WM',
                            'https://drive.google.com/uc?export=view&id=1inmO5zLEvexcKiJHuRnUaKqWuuGHdeXA',
                            'https://drive.google.com/uc?export=view&id=1MioEQWpyRBTXD_s7Hf1qNif5sMiPmdXj',
                            'https://drive.google.com/uc?export=view&id=1nfnczxfCwyCOC0aWznuoAu82aur7bseY',
                            'https://drive.google.com/uc?export=view&id=1P1ubUY1YFpmkI9KkMIBXldtYTVA0f2y2'
                        ],
                        "portal" => [
                            "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/banner",
                            "desktop" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/16",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/desktop/17"
                            ],
                            "mobile" => [
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/0",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/1",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/2",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/3",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/4",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/5",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/6",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/7",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/8",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/9",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/10",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/11",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/12",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/13",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/14",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/15",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/16",
                                "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/carson-damac-hills/gallery/mobile/17"
                            ]
                        ]
                    ],
                    'writeup'                   =>  [
                        'description'               =>  'Carson comprises three residential towers commanding far-reaching views across the entire DAMAC Hills development. With its supreme location overlooking the Trump International Golf Club Dubai, there’s a staggering choice of outdoor facilities within easy reach. From premium retail therapy and fine dining, to The Arena and the Little Village Children’s play area, Carson gives you access to it all. <br /><br />From the moment you enter Carson, you’ll experience the refined ambiance of the elegantly styled lobby, which sets the scene for your arrival home. Apartments feature thoughtful spaces along with breathtaking views. <br /><br />The apartments have the following specifications: <br />
                        <ul>
                        <li>
                        Property type range: Studio – 2BR
                        </li>
                        <li>
                        Price range: AED 544,000 – AED 1,702,000
                        </li>
                        <li>
                        Area sq ft range: 408 sq ft – 1,361 sq ft
                        </li>
                        </ul>
                        <br /><br />
                        Spanning 42 million square feet, DAMAC Hills is home to a variety of incredible properties, some the result of collaborations with the likes of world-renowned brands such as Fendi Casa and Paramount Hotels & Resorts.
                        Mansions, villas and apartments all have access to world-class retail, dining, entertainment and leisure spaces, along with spectacular outdoor areas in the form of the Trump International Golf Club Dubai and The Park, over four million square feet of lush greenery.
                        Just a 10 minute drive along the Umm Suqeim Road, this exclusive community boasts easy access to the city’s major road networks, yet it feels like a world apart.
                        '
                    ],
                    'unit'                      =>  [
                        'property_type'             =>  'Apartments',
                        'count'                     =>  519,
                        'type'                      =>  'Studio, 1 and 2 bedrooms apartments',
                        'amenities'                 =>  [ 'Schools and Nursery','Parking','Nature-parks','Security','Swimming Pool','Children Playground','Dining','Disabled-accessibility','Entertainment','Fitness','Golf','Hotel-services','Laundry-services'
                    ]
                ],
                'pricing'                   =>  [
                    'starting'                  =>  544000,
                    'emi'                       =>  0,
                    'per_sqft'                  =>  0,
                    'payment_text'              =>  '',
                    'payment_plans'             =>  [
                        "Downpayment | 10% + 4%*   |    Purchase Date + RERA Registration fees",
                        "1st Installment    | 10%    |    Within 120 days of Sale Date",
                        "2nd Installment    | 10%    |    Within 360 days of Sale Date",
                        "3rd Installment    | 20%    |    Completion",
                        "Next Installment   | 2%     |    For 23 months",
                        "Final Installment  | 4%     |    Within 720 days of Sale Date"
                    ]
                ],
                'video'                     =>  [
                    'normal'                    =>  [
                        'link'                      =>  'https://www.youtube.com/embed/kQuSBLezUqg'
                    ],
                    'degree'                    =>  [
                        'link'                      =>  ''
                    ]
                ],
                'settings'                  =>  [
                    'status'                    =>  1,
                    'approved'                  =>  true,
                    'lead'                      =>  [
                        'email'                     =>  'digital.leads@damacgroup.com'
                    ]
                ],
                'expected_completion_date'  =>  'Q4 2019',
                'status'					=>	'Completed',
                'timestamp'                 =>  [
                    'created'                   =>  '2018-10-24T10:54:46Z',
                    'updated'                   =>  '2018-10-24T10:54:46Z'
                ]
            ],
            [
                '_id'						=>	'zj-aykon-city-74546',
                'ref_no'                    =>  'aykon-city-74546',
                'name' 						=>	'AYKON City',
                'web_link'                  =>  '',
                'area' 					    =>	'Sheikh Zayed Road',
                'city'						=>	'Dubai',
                'coordinates'               =>  [
                    'lat'                       =>  25.181304,
                    'lng'                       =>  55.252577
                ],
                'image'                     =>  [
                    'banner'                    =>  'https://drive.google.com/uc?export=view&id=1U15D7RzKSCClqkGTFYccyauzRSdCAhcc',
                    'floor_plans'               =>  [
                        [
                            'name'      =>      'Apartment',
                            'plans'     =>      [
                                [
                                    'type'      =>      'Level 37',
                                    'img'       =>      'https://drive.google.com/uc?export=view&id=12uiqFGoBpkAmIMrukbJOdmlVrU2_Z6wn'
                                ],
                                [
                                    'type'      =>      'Levels 12-21',
                                    'img'       =>      'https://drive.google.com/uc?export=view&id=1N6ltVz6kh0ijocwfzYPkjqrgGZkzOrmZ'
                                ],
                                [
                                    'type'      =>      'Levels 31-33 & 38-42',
                                    'img'       =>      'https://drive.google.com/uc?export=view&id=1IitD6HyiYLFMcOlF2bab29XORFXWbV8x'
                                ]
                            ]
                        ]
                    ],
                    'gallery'                   =>  [
                        'https://drive.google.com/uc?export=view&id=1U15D7RzKSCClqkGTFYccyauzRSdCAhcc',
                        'https://drive.google.com/uc?export=view&id=1FgRSE1PaOH45FvbSwpVQlqJy74z1Sm__',
                        'https://drive.google.com/uc?export=view&id=1Z5iWqUpFTmyluZppa1upjJE54PoIF5I9',
                        'https://drive.google.com/uc?export=view&id=1V4qYD5naRxz_kt5BG_0qtMvW1zSYHST8',
                        'https://drive.google.com/uc?export=view&id=19uOe36eNRcPhsWown8OPcLmUEMRhELP6',
                        'https://drive.google.com/uc?export=view&id=1ksC1mm2F9URoUfiSVgxYpaottjydkMDa',
                        'https://drive.google.com/uc?export=view&id=1QfRczr9k4DCqSxkRS5HOOIE4fW3aLCwh',
                        'https://drive.google.com/uc?export=view&id=1gwfmZcmwz344o2IQ6yJRvuH0QVKrg5NG',
                        'https://drive.google.com/uc?export=view&id=1aAGjLLlxXHVKFdMy-qdZAp9jjpkZ0_qi',
                        'https://drive.google.com/uc?export=view&id=1E_F7e31Ue7JoO9DvtHNXyOPHk3uJolMx'
                    ],
                    "portal" => [
                        "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/banner",
                        "desktop" => [
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/0",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/1",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/2",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/3",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/4",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/5",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/6",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/7",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/8",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/desktop/9"
                        ],
                        "mobile" => [
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/0",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/1",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/2",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/3",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/4",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/5",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/6",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/7",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/8",
                            "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/damac/projects/aykon-city/gallery/mobile/9"
                        ]
                    ]
                ],
                'writeup'                   =>  [
                    'description'               =>  "AYKON City is a unique concept – an entire city set within the magnificent metropolis of Dubai. Located on Sheikh Zayed Road, with views across the city's most prestigious neighbourhood, the development will be a new architectural icon on its majestic skyline.<br /><br />Overlooking Dubai Canal, AYKON City comprises multiple towers, stretching the boundaries of design and challenging the ordinary, with lush amenities and unprecedented lifestyles on offer.<br /><br />
                    AYKON City has won two awards since its launch:<br />
                    <ul>
                        <li>
                        2016 Arabian Property Awards: Best in Dubai
                        <ul>
                        <li>
                            Best Hotel Architecture Dubai
                        </li>
                        <li>
                            Mixed-Use Architecture Dubai – Highly Commended
                        </li>
                        </ul>
                        </li>
                    </ul>
                    "
                ],
                'unit'                      =>  [
                    'property_type'             =>  'Apartments',
                    'count'                     =>  519,
                    'type'                      =>  'Studio, 1 and 2 bedrooms',
                    'amenities'                 =>  [ 'Schools and Nursery','Parking','Nature-parks','Security','Swimming Pool','Children Playground','Dining','Disabled-accessibility','Entertainment','Fitness','Golf','Hotel-services','Laundry-services'
                ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  804000,
                'emi'                       =>  0,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Deposit | 10% + 4%*   |    Immediate + RERA Registration fees",
                    "1st Installment    | 10%    |    Within 90 days of Sale Date",
                    "2nd Installment    | 10%    |    Within 180 days of Sale Date",
                    "3rd Installment    | 10%    |    On Completion of basement structure",
                    "4th Installment    | 10%    |    On Completion of 5th floor Structure",
                    "5th Installment    | 10%    |    On completion of 25th floor structure",
                    "6th Installment    | 10%    |    On completion of 45th Floor Structure",
                    "7th Installment    | 10%    |    On Completion of 60th Floor Structure",
                    "8th Installment    | 20%    |    On Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  'https://www.youtube.com/embed/D5cjDDW1G24'
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  false,
                'lead'                      =>  [
                    'email'                     =>  'digital.leads@damacgroup.com'
                ]
            ],
            'expected_completion_date'  =>  'Q4 2021',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  '2018-10-31T10:31:46Z',
                'updated'                   =>  '2018-10-31T10:31:46Z'
            ]
        ]
    ]
],
[
    '_id'		=>	'zd-prescott-78392',
    'stub'		=>	'prescott',
    'contact'   =>  [
        'name'          =>  'Prescott',
        'email'         =>  'zeeshan@prescottuae.ae',
        'phone'         =>  '97143999936',
        'whatsapp_link' =>  '',
        'website'       =>	'',
        'orn'           =>  '',
        'address'       =>  [
            'address_line_one'  =>  '',
            'state'             =>  'Dubai',
            'city'              =>  'Dubai',
            'country'           =>  'UAE',
            'postal_code'       =>  ''
        ]
    ],
    'image' 	=>  [
        'logo'		=>	'https://drive.google.com/uc?export=view&id=1gQwpypNQJZwcNiCwMxm3NewZrapsDMiQ',
        'banner'    =>  '',
        'gallery'   =>  []
    ],
    'write_up'  =>  [
        'description'   =>  'We have been building homes and offices in the UAE for well over a decade now. Our name is synonymous with quality projects. At Prescott, we have a simple philosophy: we build you a home like we would for our own.'
    ],
    'settings'  =>  [
        'status'    =>  true,
        'approved'  =>  true,
        'role_id'   =>  5,
        'chat'      =>  false,
        'lead'      =>  [
            'admin'      =>  [
                'email'          =>  'zeeshan@prescottuae.ae',
                'receive_emails' =>  false,
                'cc'             =>  ["sales@prescottuae.ae"]
            ],
            'call_tracking'   =>  [
                'track'       =>  true,
                'phone'       =>  '97142463206'
            ]
        ]
    ],
    'featured'  =>  [
        'status'    =>  false,
        'from'      =>  null,
        'to'        =>  null
    ],
    'projects'	=>   [
        [
            '_id'						=>	'zj-prime-views-67735',
            'ref_no'         =>  'prime-views-67735',
            'name' 						=>	'Prime Views',
            'web_link'                  =>  '',
            'area' 					    =>	'Meydan',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.156924,
                'lng'                       =>  55.294074
            ],
            'image'                     =>  [
                'banner'                    =>  'https://drive.google.com/uc?export=view&id=1rczYR9o8gKWNPtsbCBcwr3t3AGUAeGQ8',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type A',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1DCF4bppeCzmzC15rKVPoLQMpU-8LZ9HE'
                            ],
                            [
                                'type'      =>      'Type B',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1BSMgC0n3oXV37o0I06Zk9-PpeGb4IeO_'
                            ],
                            [
                                'type'      =>      'Type C',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1H-YVOXUTdXMNhGZ4FTT80D7jiYBZ3C_e'
                            ],
                            [
                                'type'      =>      'Type D',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1LtsqIszbsrtNdeqM9MU256-xc0lgP5Er'
                            ],
                            [
                                'type'      =>      'Type E',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1eiTw2dIZPXWUYXEwuAgMs7DOGRhixnD1'
                            ],
                            [
                                'type'      =>      'Type F',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1UyPszIWml5nYpQq2mTQpsv7zf627TcGF'
                            ],
                            [
                                'type'      =>      'Type G',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Khmc3mGWuItnp9psSLAOk9lOApmMPmSr'
                            ],
                            [
                                'type'      =>      'Type H',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1kRoXq8X8GaIG0rWMcwFQ3ft94xXbfPne'
                            ],
                            [
                                'type'      =>      'Type J',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1fJjJcwtpvRQi_gIx8Ru4Jw0fz-molm2B'
                            ],
                            [
                                'type'      =>      'Type K',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1rfFyaryrpxl0ir5-iQ2I0k469Xue45jc'
                            ],
                            [
                                'type'      =>      'Type L',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1u6jOGEbotXowx0FxNRGCvxGXBRCD3HfQ'
                            ],
                            [
                                'type'      =>      'Type M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=19e_fGPnhYWjAA0i2jXpFSRi7w9UevjP6'
                            ],
                            [
                                'type'      =>      'Type N',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ywkSq_GnPDD0YLRnykBP4RioAvVOf86f'
                            ],
                            [
                                'type'      =>      'Type P',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1EuTXR0UnS0nt7SdfFz8md2XM6MLMJ4l6'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type A',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1C6yCNPuDRa0-Pu2cHGcBhMh6cK_3_4Qd'
                            ],
                            [
                                'type'      =>      'Type B',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1isHokawIpZhFcdYEL3qvhqwttnOOxPo6'
                            ],
                            [
                                'type'      =>      'Type C',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1yMMSZugbOSqA2Vhw8c6-_fFetT6eFD_V'
                            ],
                            [
                                'type'      =>      'Type D',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1DT5JUFvdUu2LDVeGFfvyZd0jmhVInG-T'
                            ],
                            [
                                'type'      =>      'Type E',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=16aqW2IT_CvHolylVNRUeurWbJs74E6QZ'
                            ],
                            [
                                'type'      =>      'Type F',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1h93WD1mYGnUx1iWsLGHoHIHXG9Rx5qh2'
                            ],
                            [
                                'type'      =>      'Type G',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1qs34GYsnPHW24LgBctpAAD_wxz5-7lJm'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://drive.google.com/uc?export=view&id=1PKHY8B-59fphzOWmznW5WIXeYRfjQBtb',
                    'https://drive.google.com/uc?export=view&id=1K6oTYXKMm8CJ0RppG5_A2voQ6rLDNkHO',
                    'https://drive.google.com/uc?export=view&id=19911LQ61uDnR3oLi18st8SElqv0Sdwnr',
                    'https://drive.google.com/uc?export=view&id=1pJhKlatRmLcwArH3HZAuqcp00NhWJQ_r',
                    'https://drive.google.com/uc?export=view&id=1oqfB782N82vtCfEEbrwSl6e0CkCufaMK',
                    'https://drive.google.com/uc?export=view&id=1Y33LeUZjUSA2nARJuWQCGNXWmP878JMX',
                    'https://drive.google.com/uc?export=view&id=1kBw-LKujasvSjcr4sGHQpy1ly17k6g6T',
                    'https://drive.google.com/uc?export=view&id=1L7Ynkrd_hKq_Z5M90vMdh8Uhg0p96aE7',
                    'https://drive.google.com/uc?export=view&id=1rczYR9o8gKWNPtsbCBcwr3t3AGUAeGQ8'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/desktop/9"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/prescott/projects/prime-views/gallery/mobile/9"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Prime Views by Prescott is a contemporary residential development situated in the heart of Meydan Avenue. Located opposite to The Meydan Hotel, it is surrounded by the world-famous Meydan Racecourse and is within close proximity to Meydan One and Downtown Dubai.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  519,
                'type'                      =>  '1 and 2 bedrooms',
                'amenities'                 =>  [ 'Common Area', 'Large Pool', 'Gymnasium', '24 Hour Security', 'Retail Space', 'Dedicated Car Parking', 'Branded White Goods', 'BBQ Area' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  929088,
                'emi'                       =>  0,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "1st Installment    | 10%    |    Deposit",
                    "2nd Installment    | 10%    |    after 4 months",
                    "3rd Installment    | 10%    |    after 7 months",
                    "4th Installment    | 10%    |    after 10 months",
                    "5th Installment    | 12%    |    upon handover",
                    "6th Installment    | 1%     |    per month for 48 months after handover"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  ''
                ],
                'degree'                    =>  [
                    'link'                      =>  'https://my.matterport.com/show/?m=sGjVUSs7JC6'
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  'sales@prescottuae.ae'
                ]
            ],
            'expected_completion_date'  =>  'Q1 2020',
            'status'					=>	'Parking',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ]
    ]
],
[
    '_id'		=>	'zd-emaar-89364',
    'stub'		=>	'emaar',
    'contact'   =>  [
        'name'          =>  'Emaar',
        'email'         =>  'Tele_Sales@emaar.ae',
        'phone'         =>  '97142482503',
        'whatsapp_link' =>  'https://api.whatsapp.com/send?phone=+971529887539&text=Hello%2C%20I%20am%20interested%20to%20know%20more%20about%20The%20Residences%20at%20Marina%20Gate.%20%20Could%20you%20please%20contact%20me%20with%20more%20information.%20Thanks!',
        'website'       =>	'https://www.emaar.com/en/',
        'orn'           =>  '',
        'address'       =>  [
            'address_line_one'  =>  '',
            'state'             =>  '',
            'city'              =>  '',
            'country'           =>  '',
            'postal_code'       =>  ''
        ]
    ],
    'image' 	=>  [
        'logo'		=>	'https://drive.google.com/uc?export=view&id=1hI7W0OPNwNLH1WYuDuFUa0s7yB1DPS4x',
        'banner'    =>  'https://drive.google.com/uc?export=view&id=0Bx71O3CpyGf3Sm5SVEozS3FPMUdua3BCeWpvVzVTYXk1Q2tF',
        'gallery'   =>  []
    ],
    'write_up'  =>  [
        'description'   =>  'Emaar Development us the UAE build-to-sell property development business of Emaar Properties, responsible for iconic master-planned freehold communities in Dubai including Emirates Living, Downtown Dubai, Dubai Marina and Arabian Ranches. Emaar development has a high quality land bank of 170 million square foot gross area for build to sell properties in the UAE, which at an average development rate is expected to service at least 12 years of sales.'
    ],
    'settings'  =>  [
        'status'    =>  true,
        'approved'  =>  true,
        'role_id'   =>  5,
        'chat'      =>  false,
        'lead'      =>  [
            'admin'      =>  [
                'email'          =>  'YBarghouti@emaar.ae',
                'receive_emails' =>  false,
                'cc'             =>  ['tele_sales@emaar.ae']
            ],
            'call_tracking'   =>  [
                'track'       =>  true,
                'phone'       =>  '97142482503'
            ]
        ]
    ],
    'featured'  =>  [
        'status'    =>  false,
        'from'      =>  null,
        'to'        =>  null
    ],
    'projects'	=>   [
        [
            '_id'						=>	'zj-sunrise-bay-89369',
            'ref_no'                    =>  'sunrise-bay-89369',
            'name' 						=>	'Sunrise Bay',
            'web_link'                  =>  '',
            'area' 					    =>	'Dubai Marina',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.098862,
                'lng'                       =>  55.141130
            ],
            'image'                     =>  [
                'banner'                    =>  'https://drive.google.com/uc?export=view&id=0Bx71O3CpyGf3Sm5SVEozS3FPMUdua3BCeWpvVzVTYXk1Q2tF',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1099aSSjznFNzKnk6NdhwdFQEVEor2GEv'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=14iVn2lttTqoM1dcjQViOBPTPdKBGsn0C'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1eO5td_QGO33Yf4WqXJ-QFET0NxwsjSJo'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1PcyEbtu5nO9lj-1RF-53gUtmaBNR3sJh'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=11z63Zbj4HzPowf-dYrvVRlkY7PtZBg7F'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1nhy_c4U-UkkD2KS5YbkrWb25ptDk4iey'
                            ],
                            [
                                'type'      =>      'Type 7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1KjU6SGuptSniM2pIRhvmnsj7z2W17li-'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1AT8sz25gtttV1JpNVFGWjuuJ-RlbMLvJ'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Eo8UdyZN2uQg1roO5hWWPCE2qiuCGAf9'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1-f3xzE8_KeZb06brj6MD79V4KX1hqGLQ'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1soHdyWk1-QfaLaJu2iHxFt9DMB7JCfya'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1AJZF6lNa2dgcRt0YzWp1I9efKjPb5AHl'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1h-TwpC7XVoGwmzRxjlKCAonwR_tJieV7'
                            ],
                            [
                                'type'      =>      'Type 7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1FCbTFKVIblWwXkB9a2rCYew4nIxmvZDu'
                            ],
                            [
                                'type'      =>      'Type 8',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1lIxfI8PDxNn5WItwoWr0rlPRr1XdP8Hg'
                            ],
                            [
                                'type'      =>      'Type 9',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1YZRDIL6XD_slR9-8bg3tWOUF1aXdm2I0'
                            ],
                            [
                                'type'      =>      'Type 10',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1-pEwu6vQ05cIQKLxIpHmyOx8EjYffxhS'
                            ],
                            [
                                'type'      =>      'Type 11',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1EqaDYCNCK3gHbRcEuyZni5YOQqIa0GEi'
                            ],
                            [
                                'type'      =>      'Type 12',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1HD3sIZk34rHy3OCwvzNbHkn6snSmnZCz'
                            ],
                            [
                                'type'      =>      'Type 13',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1u10nvpmfgsLpP2PHS4GT8_F5vXJnAHAO'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '3 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1AVdlHs3HnRJQDkGbBA4ZaSwgwWYns3i4'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1KIc06ExiiQI76f5TgCWrv7LWepDPHdnD'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Bxk0dT_VF0dbiARX3dImUPNeDDcUy1zi'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Sj1QGgS0KrrSaXJjtf_rr2DGmNUSYyxU'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1tKpFBVORmCBRNejlgfjub6CX8jh_euLR'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1R2tLZWg7wS-HLkS-FHSC_kvfZ-5R-y-z'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://drive.google.com/uc?export=view&id=1g24Z_iD1O4qpunT5XfK6TB3X08W5aXZE',
                    'https://drive.google.com/uc?export=view&id=1NppHFQffj2-j4xz3TKiKv-F-DN9lj-mT',
                    'https://drive.google.com/uc?export=view&id=1FX3lBTzQpo7ySQhKZb2rBtwgPs86b2ka',
                    'https://drive.google.com/uc?export=view&id=1dR9Ze2limkCGPKv5LPiGfUvlQq4diWQ-',
                    'https://drive.google.com/uc?export=view&id=14S4jZZe5IQPh2nqNbPKRiWxpoIFEvFYl',
                    'https://drive.google.com/uc?export=view&id=18h_1LxZzvbv-K0B74EceLhU7ZUGik3BV',
                    'https://drive.google.com/uc?export=view&id=1C14olJyuNFabfxOBjmD--KAafGc5cM0s',
                    'https://drive.google.com/uc?export=view&id=1ItcryOhcsnI79GdzpDhx6g6E9utXPe6b',
                    'https://drive.google.com/uc?export=view&id=1_fTrYPc08YqFOOaDO_dLBdos_pKn42_O',
                    'https://drive.google.com/uc?export=view&id=1bdyPNVIUumj3Nhe1yG3oOW1FTz-9C0BA',
                    'https://drive.google.com/uc?export=view&id=1sdzxPtggaIBvifCh0PwDl9BFBbhjUiQG',
                    'https://drive.google.com/uc?export=view&id=0Bx71O3CpyGf3Sm5SVEozS3FPMUdua3BCeWpvVzVTYXk1Q2tF'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/desktop/10"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/sunrise-bay/gallery/mobile/10"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'A premium twin tower of 26 storey, Sunrise Bay is set only few steps away from the ocean and vibrant marina. The beautiful crafted building blends artfully with its beachfront setting and offers gorgeous views of Dubai Marina and the open sea. With a choice of 1, 2, 3 and 4 bedrooms premium apartments, nothing says luxury beachfront living like home in Sunrise Bay.                                                              Cosmopolitan living by the sea. Emaar Beachfront is the exclusive residential community within the new maritime epicentre of the UAE, Dubai Harbour. The meticulously master-planned beachfront development represents a unique blend of cosmopolitan living in a prime location and a serene seaside lifestyle. Comprising 27 exceptional towers, Emaar Beachfront offers a broad range of 1, 2, 3 and 4-bedroom luxury apartments. Some of the apartments will overlook the crystal-blue waters of the Arabian Gulf, while others will sport views of Dubai Marina. Residents of Emaar Beachfront will have exclusive access to a 750-metre long strip of beach on either side of their home, as well as seamless access to Sheikh Zayed Road and Dubai Marina. A major Boulevard stretching across the community will offer a broad range of retail outlets, stylish restaurants and trendy cafes, while the beach itself will be dotted with beach sport and resort-style facilities for a wholesome lifestyle.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  519,
                'type'                      =>  '1, 2, 3 and 4 bedroom apartments',
                'amenities'                 =>  [ 'Infinity Pool', 'Gym,Retail Outlets', 'Restaurants', 'Beach Access', 'Sports Facilities', 'Podium Deck', 'Healthcare Facilities', 'Balcony' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  1296888,
                'emi'			            =>	1296888,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Down Payment       |    5%  |     Purchase Date",
                    "1st Installment    |    5%  |     After 3 months from Purchase Date",
                    "2nd Installment    |    5%  |     After 6 months from Purchase Date",
                    "3rd Installment    |    5%  |     After 9 months from Purchase Date",
                    "4th Installment    |    5%  |     After 12 months from Purchase Date",
                    "5th Installment    |    5%  |     After 15 months from Purchase Date",
                    "6th Installment    |    5%  |     After 18 months from Purchase Date",
                    "7th Installment    |    5%  |     After 21 months from Purchase Date",
                    "8th Installment    |   10%  |     60% Construction Completion",
                    "Final Installment  |   50%  |     100% Construction Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  'https://www.youtube.com/watch?v=l1vdiXCvL5k&feature=youtu.be'
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Q4 2021',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ],
        [
            '_id'						=>	'zj-saffron-83925',
            'ref_no'                    =>  'saffron-83925',
            'name'                      =>	'Saffron',
            'web_link'                  =>  '',
            'area' 					    =>	'Emaar South',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  24.8584094,
                'lng'                       =>  55.1543256
            ],
            'image'                     =>  [
                'banner'                    =>  'https://drive.google.com/uc?export=view&id=1LCL9-BIChpjNy8HzwPR6EHJQ1n2kPPDw',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '3 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Vn5H9PFun7ekkcWBYUMJYjCKKSg6L_89'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '4 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1QRTNvCeEiQJDEbTcc-97ZtL_RolnQ27P'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://drive.google.com/uc?export=view&id=1HsDtU0uCRbxOlKiIz91rH5GAQZZNh2GP',
                    'https://drive.google.com/uc?export=view&id=1P6o5AugxamwsJJdkoCPvBEQFak0Af_wn',
                    'https://drive.google.com/uc?export=view&id=1nfJEdzeFVju5gx1zhuIpvrNQVpQJyzIL',
                    'https://drive.google.com/uc?export=view&id=1lXse-VrGJomQwKYIgSLfQMP80i9LbLF5',
                    'https://drive.google.com/uc?export=view&id=1moXZwk7tmnVgoke-vX19_KbN12O_m8Ua',
                    'https://drive.google.com/uc?export=view&id=1rcA_kip0X_YUFCpFUXKaud4Wwm2876-m',
                    'https://drive.google.com/uc?export=view&id=13V5Hznkc1ThsLGpRIFZp2PChwNnw8i1I',
                    'https://drive.google.com/uc?export=view&id=1LCL9-BIChpjNy8HzwPR6EHJQ1n2kPPDw',
                    'https://drive.google.com/uc?export=view&id=10IOQ2zZc8dzMDvF-LMal2aTgMHxg4quF'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/desktop/8"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/saffron/gallery/mobile/8"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Elegant Townhouses that Prize Nature, Space and the Outdoors. Own a home in a family-friendly environment that celebrates relaxation, play and the outdoors. Set amongst the new parks and ample green space of Emaar South, Saffron is a stunning collection of townhouses with beautifully manicured private gardens. Choose between 3 and 4-bedroom townhouses designed with flair and within arm\'s reach of nature.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Townhouses',
                'count'                     =>  0,
                'type'                      =>  '3 & 4 bedroom townhouses',
                'amenities'                 =>  [ 'BBQ Area', 'On low floor', 'Shared Pool', 'Balcony', 'Shopping Mall', 'Gymnasium', 'Broadband Ready', 'Pets Allowed', 'Built in wardrobes', 'Maid\'s Room', 'Private Garden', 'Central AC', 'Tennis Courts', 'Professional Gardener', 'View of gardens', 'Communal gardens', 'Shops', 'Public Park', 'Community View', 'Mosque', 'Public Parking', 'Restaurants', 'Covered Parking', 'Public Transport' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  1431888,
                'emi'                       =>  1431888,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Down Payment      |     10%    |    Purchase Date",
                    "1st Installment   |     10%    |    25th August 2018",
                    "2nd Installment   |     10%    |    25th Feburary 2019",
                    "3rd Installment   |     10%    |    40% Construction Completion",
                    "4th Installment   |     10%    |    60% Construction Completion",
                    "5th Installment   |     10%    |    100% Construction Completion",
                    "6th Installment   |     10%    |    6 Months - Post Handover",
                    "7th Installment   |     10%    |    12 Months - Post Handover",
                    "8th Installment   |     10%    |    18 Months - Post Handover",
                    "Final Installment |     10%    |    24 Months - Post Handover",
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  'https://www.youtube.com/watch?v=ZufUgAwTF-w'
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Dec-20',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ],
        [
            '_id'						=>	'zj-collective-36425',
            'ref_no'                    =>  'collective-36425',
            'name'                      =>	'Collective',
            'web_link'                  =>  '',
            'area' 					    =>	'Dubai Hills Estate',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.1004918,
                'lng'                       =>  55.2446795
            ],
            'image'                     =>  [
                'banner'                    =>  'https://drive.google.com/uc?export=view&id=1fpe3mcsi30njgSd0xhU1p041iioCH09U',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1-LTiWEitnnOoAdHlK1SPw-MqUZAXLXda'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=13WjUBg_MvsUnpdAVbws2PY7UDDV2NXo8'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1P70Pe1PH-A5QdoBecaXKh2D3rh5vnM-r'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1VpCTW3MBGF599pJPwbW-zal7Tb2Zlcyz'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1HOlvwuU3d53xYRsprMyNoBetcOoA9NSI'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://drive.google.com/uc?export=view&id=1gogIxuhmKUcgLb0is5_tXMy-jGDNgtn7',
                    'https://drive.google.com/uc?export=view&id=1FFn-kAl1WRVb6mUdCUvtqBRPCCzmU5ar',
                    'https://drive.google.com/uc?export=view&id=1QEZiluQbWkQFsrjGDHEmohwZFkCGE_st',
                    'https://drive.google.com/uc?export=view&id=11RGVntldwm3STBEN1IdWcZbzenygodj5',
                    'https://drive.google.com/uc?export=view&id=1uCo69W5X-Af8GBLuTOvRPcnGR7TWFzcz',
                    'https://drive.google.com/uc?export=view&id=1Z-a1Upr0PvFQEWIxNp1wDHkSGhpSs1yM',
                    'https://drive.google.com/uc?export=view&id=10Mv0M0kF2Ivt3zhFKtZUlLGNg1ejbgZY',
                    'https://drive.google.com/uc?export=view&id=11QgDIgg3JbwpDrze-ikZ0AfAyt6c8lTa',
                    'https://drive.google.com/uc?export=view&id=1uH5nzcDqhPJFtIe0zwgIeV0ZUWCWsVGs',
                    'https://drive.google.com/uc?export=view&id=1CfoQTB5B4v8tWQXE-Xq3vFgnwzj2gTzQ',
                    'https://drive.google.com/uc?export=view&id=1Nwhn3r5fy_Y7LCwVTYk5rnLtM4m4tgjb',
                    'https://drive.google.com/uc?export=view&id=1PahJx3KLqrGAskIQJsnrjymfbnEWq09W',
                    'https://drive.google.com/uc?export=view&id=1fpe3mcsi30njgSd0xhU1p041iioCH09U',
                    'https://drive.google.com/uc?export=view&id=1Ryp9VvlJi0n74iTngf7n80LogONMpAHy'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/10",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/11",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/12",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/desktop/13"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/10",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/11",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/12",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/collective/gallery/mobile/13"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Contemporary co-living spaces that foster a sense of community. Collective is a contemporary co-living space in the budding and central location of Dubai Hills Estate. Occupants will have private residences opening onto a world of shared dining, leisure, sports, retail and cultural facilities on-site. Invest in 1 or 2 bedroom apartments and enjoy high rental returns for years to come.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  0,
                'type'                      =>  '',
                'amenities'                 =>  [ 'BBQ area', 'Driver\'s Room', 'On low floor', 'Shared Pool', 'Balcony', 'On mid floor', 'Shopping Mall', 'Basement Parking', 'Gymnasium', 'Broadband Ready', 'Pets Allowed', 'Central AC', 'Metro Station', 'Communal Gardens', 'Shops', 'Public Park', 'Community View', 'Mosque', 'Public Parking', 'Restaurants', 'Covered Parking', 'Public Transport' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  671888,
                'emi'                       =>  671888,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "1st Installment   |     10%    |    Purchase Date",
                    "2nd Installment   |     10%    |    22nd September 2018",
                    "3rd Installment   |     10%    |    22nd April 2019",
                    "4th Installment   |      5%    |    20% Construction Completion",
                    "5th Installment   |      5%    |    40% Construction Completion",
                    "6th Installment   |     10%    |    60% Construction Completion",
                    "7th Installment   |     10%    |    80% Construction Completion",
                    "Final Installment |     40%    |    100% Construction Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  'https://www.youtube.com/watch?v=6VvQxZVMp0U'
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Jun-21',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ],
        [
            '_id'						=>	'zj-the-grand-83925',
            'ref_no'                    =>  'the-grand-83925',
            'name'                      =>	'The Grand',
            'web_link'                  =>  '',
            'area' 					    =>	'Dubai Creek Harbour',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.2077399,
                'lng'                       =>  55.3403735
            ],
            'image'                     =>  [
                'banner'                    =>  'https://drive.google.com/uc?export=view&id=1WoT3n5cTUNOlnMsWj4QM-GzY93XL7gLX',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1X1FGv9o9Cn2DV2iTnEyeNZ0yAm05GIKj'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1kTOxB3wX8mtoFRF6iiGmjeYf95-10hxZ'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1kTOxB3wX8mtoFRF6iiGmjeYf95-10hxZ'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=19t_94AjJjmx0RqXwDwI66b13E2mggvHz'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1xJ6x3RLk70rCMikFuQaWnFfiNqr5D6Dy'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1wtp3BFyCFZJbZV44FtX-K96lOmGTsUjd'
                            ],
                            [
                                'type'      =>      'Type 7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1r4Woh-dMQmUQfsf6y0hYvKdjEE2ycgzk'
                            ],
                            [
                                'type'      =>      'Type 8',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=10heKEy2I-15PKVAskRJXRO2IhL2Djow6'
                            ],
                            [
                                'type'      =>      'Type 9',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1GLD_w8JXtSWM_RyPI5MDjjqFVgkEyJWg'
                            ],
                            [
                                'type'      =>      'Type 10',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1j3BZlUxGVsZ2OlcO9fwPC9mqmZ2bsOhd'
                            ],
                            [
                                'type'      =>      'Type 11',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=18oxJTN_dz7GeKlZNTUjsVMTFIueHWrit'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=15YMHt8-nY8-uHej4lDieYWZ0c92sJl_E'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1PO54HRbb7HNh37t0dRmAGHBzDXZ93yxp'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=17fOVOJCEVDAHW1xOGBWudXtU7N4GuTmb'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=19V4_m0sYyFsMhNZTCKJk6NitGKAmoCqq'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=12fbbQlbRrgg4xC4tPsjMz5eECcLZr7sL'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1OfVR2vgAglFC4ZTLvW7F_Pr7-jb1_D3V'
                            ],
                            [
                                'type'      =>      'Type 7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Zb6Dhp-n53gtiOW-9y15yNZlXUz1Llpy'
                            ],
                            [
                                'type'      =>      'Type 8',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1oaJqQQ_ZKmdEzDAj3MUdGuwzh4y4fDua'
                            ],
                            [
                                'type'      =>      'Type 9',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1pAwL6YuBvx_iFQB7lMvLecw36vAYe7Wb'
                            ],
                            [
                                'type'      =>      'Type 10',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1IEPXNEFF1h8VwdPbkrSosRPm6vIhkXxu'
                            ],
                            [
                                'type'      =>      'Type 11',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1XuI7pYOLQgnaSGq7sN0UHv4BZXatUDIh'
                            ],
                            [
                                'type'      =>      'Type 12',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1NPgAFcfmfbKwzN_N0A372U0qqgera9Ga'
                            ],
                            [
                                'type'      =>      'Type 13',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=19uIxy9G65fTUS5phZh0onGtlC3kW8E7C'
                            ],
                            [
                                'type'      =>      'Type 14',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1vkMmzkklc-rdPUm9xUeiaSm7Weg8b6Wa'
                            ],
                            [
                                'type'      =>      'Type 15',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1L2QSBjqz2XC3HhBUaNoFkc9m7-4Qa57N'
                            ],
                            [
                                'type'      =>      'Type 16',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1PIXKPCO0VEkGI96Jwf9fhhIn1tt1bcmZ'
                            ],
                            [
                                'type'      =>      'Type 17',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1T8sxUFmcIJosg8slTgjK9Nc3GekZeKUz'
                            ],
                            [
                                'type'      =>      'Type 18',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1NlDETxHgw9G-F7xDTcFc1CEz6SawIhG6'
                            ],
                            [
                                'type'      =>      'Type 19',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1EWaQFW2BAW1LbHl6l7tU4P3hbN8LH86-'
                            ],
                            [
                                'type'      =>      'Type 20',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=15ZMXtS85Twp96wOdQY81n3uxC-mg53Zl'
                            ],
                            [
                                'type'      =>      'Type 21',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Sa3Cw7HTFNNBJkOrQbQTbT2gTGImBstt'
                            ],
                            [
                                'type'      =>      'Type 22',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1FUOF5gujNQfWHbLo5vR_9yFkvkC9UCXG'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '3 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1IK_FOD_dNqGfXOzVW5qJo8ad23suNRI4'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=14LNdgWXDYUm7Lo8ybVpVrXQC2Eq05q5W'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1w3K6mww4cKkoum-k-rTr9oBFsfrKws7M'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1QF6KCiH7ivR-NHQJBjDedNTpOfnWbhlP'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=17NvDGrXqESouyI_QJHfc1-hBmMsn-l7c'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1vW4oZDFOM4Z5jlHGlwAejECQxKLgr_x2'
                            ],
                            [
                                'type'      =>      'Type 7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=119QD4B1W8qW9emSnFEwzfZTQRzdauaNz'
                            ],
                            [
                                'type'      =>      'Type 8',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1jcQ5_SsKco-IZgjCFaPXyPsXbCa_cDAC'
                            ],
                            [
                                'type'      =>      'Type 9',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1kW34pKQgYE3zDhdZmDsOL8C-49JA-enK'
                            ],
                            [
                                'type'      =>      'Type 10',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1V9y8XVhzo2B_7qIIdAYR3oSXIp0-LAt4'
                            ],
                            [
                                'type'      =>      'Type 11',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1wdwJcB-9Da1xnB62SSn_7mETGOIiseld'
                            ],
                            [
                                'type'      =>      'Type 12',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=14AqDSMt8Ogud2wej9n5Gr52BlkEW7IbG'
                            ],
                            [
                                'type'      =>      'Type 13',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=14De0TWY7U_SfsVh_t-eezGUhgjKENWCR'
                            ],
                            [
                                'type'      =>      'Type 14',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1FjdVcLS7gP4gGrR1-DZFoIpVGnXDm-_u'
                            ],
                            [
                                'type'      =>      'Type 15',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1jyKC216OuUBEU2CZDrLZNzDXBC3nZnj_'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '4 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=16nq1RAMFIONI5hQ09jkdWQ_SbgQv_jxe'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1kg80WkXmSXnoPSCy3u9Edqnt_XxJEHs8'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://drive.google.com/uc?export=view&id=1AtbixKaamZX2fXh7KTk0wy51sg410pbY',
                    'https://drive.google.com/uc?export=view&id=15kFxOgI4wuRHEJ9HZsG7JujUehvU_Dab',
                    'https://drive.google.com/uc?export=view&id=1rWH3hSS3ZP1rsUZ-LUkYX8F1OK3wDLh7',
                    'https://drive.google.com/uc?export=view&id=1Pk4Q8RqWvZENZzkXW8ZQYt6O41hEb0KK',
                    'https://drive.google.com/uc?export=view&id=1cRhGZODOpMlLnYycIVlm9H0MnVp-Ar3X',
                    'https://drive.google.com/uc?export=view&id=1yHg67Tz8Dq1z9AoLnj53YslC9e18IIdx',
                    'https://drive.google.com/uc?export=view&id=1yur5ujmEwDbQMuQxTL7mnNRdGVdFMCES',
                    'https://drive.google.com/uc?export=view&id=1HL1sAp0lCCT-FTJvjSB2CavDDtWSm3ea',
                    'https://drive.google.com/uc?export=view&id=16_MBCAvMtf41C6tcStxqBosT4r1BqWwZ',
                    'https://drive.google.com/uc?export=view&id=1WoT3n5cTUNOlnMsWj4QM-GzY93XL7gLX',
                    'https://drive.google.com/uc?export=view&id=12nw8j1aWEEWt_wy6NfvFwCdjW6fUubC0'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/desktop/10"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/emaar/projects/the-grand/gallery/mobile/10"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Ultra-modern Living in Dubai Creek Harbour. Rising majestically from Dubai Creek Harbour, The Grand will set the standard for ultra-modern living. Envisioned as an ‘Urban Luxe’ retreat, this iconic waterfront tower is set only steps away from the marina and promenade, giving residents boundless dining, retail and entertainment experiences to choose from.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  0,
                'type'                      =>  '1, 2, 3 & 4 bedroom apartments',
                'amenities'                 =>  [ 'On Low floor', 'Shared pool', 'Balcony', 'On mid floor', 'Shopping Mall', 'Basement parking', 'On mid floor', 'Shopping mall', 'Basement parking', 'Gymnasium', 'Broadband Ready', 'Pets Allowed', 'Built in wardrobes', 'Maid\'s Room', 'Study', 'Central AC', 'Marble Floor', 'Metro Station', 'Children\'s Playarea', 'Fully Fitted Kitchen', 'View of gardens', 'Communal Gardens', 'Shops', 'Public Park', 'View of Sea', 'Community View', 'Mosque', 'Public Parking', 'Restaurants', 'Covered Parking', 'On High floor', 'Public Transport' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  1176888,
                'emi'                       =>  1176888,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "1st Installment   |      5%    |    Purchase Date",
                    "2nd Installment   |      5%    |    25th July 2018",
                    "3rd Installment   |     10%    |    25th December 2018",
                    "4th Installment   |     10%    |    25th April 2019",
                    "5th Installment   |     10%    |    30% Construction Completion",
                    "6th Installment   |     10%    |    50% Construction Completion",
                    "7th Installment   |     10%    |    80% Construction Completion",
                    "8th Installment   |     40%    |    100% Construction Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  'https://www.youtube.com/watch?v=dJ3BQCnpfsU'
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Dec-21',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ]
    ]
],
[
    '_id'		=>	'zd-jumeirah-luxury-89298',
    'stub'		=>	'jumeirah-luxury',
    'contact'   =>  [
        'name'          =>  'Jumeirah Luxury',
        'email'         =>  'h.gaber@lli.ae',
        'phone'         =>  '97143753115',
        'whatsapp_link' =>  'https://api.whatsapp.com/send?phone=+971554327933&text=Hi%20,%20I%20want%20to%20buy%20this%20apartment%20.%20Please%20contact%20me%20back',
        'website'       =>	'',
        'orn'           =>  '',
        'address'       =>  [
            'address_line_one'  =>  '',
            'state'             =>  '',
            'city'              =>  '',
            'country'           =>  '',
            'postal_code'       =>  ''
        ]
    ],
    'image' 	=>  [
        'logo'		=>	'https://drive.google.com/uc?export=view&id=1TOqHSElzMedMyvMua8c6XGXEwEmkva7U',
        'banner'    =>  'https://lh3.googleusercontent.com/UZxG1nGxKTxRa_71y0Bz990g1S4ydz7U32v7WF-8KVdXgIpvcVitWKATVoalCaOe8_rqL9f4X5mhb70xJIGUV2tZ8P3q1NmCEV0prztxvxCUaHKKfOfDtDSQpwoT7Uw8CFWQDDBQf1hGhv77JBCmiVRbzIAIU5GyGsb48HaoMvo2wpHwB5uHNXOG5xp1zrqvNmZS7_Ysn9D4fvJFsRMCKCoPcVpILzoAxX6BybzmndpGMIvLK_Nj4dWF6Ar7tWJl-xv0rTlYAUoc3w348rVX_TNNkmEdN6E2oZc_v-Z2pA0nO9e6Jn5iTn55v9vKDywjBLssZNKPn4GWV66yoWFFRkn-a_hWUK_L9EX8XJDQ01SKvYq4wHCPHsk2Z8Tf7j61XmDrcJo856Gi4SeZzLztIrXNDBhUPNO6Mu9vpxh4fbahvlveySUxaBCU4uUj6cnrPFZythoG08TqcTVzbhYcrS2tE6tW6xi8tC2yJAn78x4BzjQHkUG9BZfZN9DueUe_e-QFU21k_Gt2eYaa5kVLV-SLespu-xQvcOgFFAYXGsaMV4Ssa5Whz_uv10SK8tav=w1440-h701',
        'gallery'   =>  []
    ],
    'write_up'  =>  [
        'description'   =>  ''
    ],
    'settings'  =>  [
        'status'    =>  true,
        'approved'  =>  true,
        'role_id'   =>  5,
        'chat'      =>  false,
        'lead'      =>  [
            'admin'      =>  [
                'email'          =>  'h.gaber@lli.ae',
                'receive_emails' =>  true,
                'cc'             =>  []
            ],
            'call_tracking'   =>  [
                'track'       =>  false,
                'phone'       =>  ''
            ]
        ]
    ],
    'featured'  =>  [
        'status'    =>  false,
        'from'      =>  null,
        'to'        =>  null
    ],
    'projects'	=>   [
        [
            '_id'						=>	'zj-jumeirah-luxury-89348',
            'ref_no'                    =>  'jumeirah-luxury-89348',
            'name'                      =>	'Jumeirah Luxury',
            'web_link'                  =>  '',
            'area' 					    =>	'Jumeirah Golf Estates',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.023466,
                'lng'                       =>  55.204467
            ],
            'image'                     =>  [
                'banner'                    =>  'https://lh3.googleusercontent.com/IN73lm3f_ugkL_m6iaFlbQUZWO32oitKLIM_V1c6F812Ne9oN520xYI-TWg52JrXlTEnumj7PKptt4_qUql_caKENzhMJ7_SNPdZkayexyubamPb36iiTkY8ToFGvTizZSmvh_2vKcwE8zwv6eE9GbNo7GP0M8V2Ez6G4Tz9EwOpLAn7B9aVPGFRXCdMGI5L93CFdSQRmX0L4D8hJVelPWp6F_KwQhPb_0pTfiWHAG3daaphH72R742ct9eDC_uxRZv-UR8xmucNJxeXIhsGnsWvP2wuUON8GhgVRy-D7D1UC8I-S4sVVWedN52zUHTgXs-U3F-soKTX__F48cAAl_GLdhXvo4VnBs3uUzopNZgmD38V5jsPn3oxROPJtxfWCYEr98ZQCkdYIsk4YmfQIPg1NxYmIO_0O7cSlYkoCePKJTk1Yqu96oL8Tbl6b9I0QgvnTd4eTtThLaroRl2vKV7Mp61BU8kZCt2mJx7kLtyIbOB9t1cT7aqi-J_mbKtmEIAblGaJ2ldp6G80pBhowUYBqQKycFYI-DYPnxtXEFXrwQNzd4YtpF7gCl6Mw2la=w1440-h701',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '3 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Flower T2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1NuaBNfOUQLqP4WvFfC_Jwx-5pL4AhmYl'
                            ],
                            [
                                'type'      =>      'Flower V2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1fJrfbc4UiJrw1pXpdJp02ZelCkIXMphS'
                            ],
                            [
                                'type'      =>      'Golf V2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1mcLWvIcRqOO3W0Mk9l4QENbZOhgY8ebo'
                            ],
                            [
                                'type'      =>      'Water T3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1WhijPx8JfahlWv9G-8wt85CVmPV3EWe0'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '4 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Flower T1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1WNKwFtOS3-5qssuUTAJ60iBOmjP0MI0E'
                            ],
                            [
                                'type'      =>      'Flower V1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1n8tEih_UP14b097OQgGOXBmspVn-fifb'
                            ],
                            [
                                'type'      =>      'Golf T1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1_tLxZEmrnsUlvWw3pC0TXKP0XdsmFtc6'
                            ],
                            [
                                'type'      =>      'Golf T2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1vNg-BBNr_ZskgqK-sNJOjU6wCYjK5FJC'
                            ],
                            [
                                'type'      =>      'Water T1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ZVkSUHlNK23b-DTADMnZfz0-CfTDPBsE'
                            ],
                            [
                                'type'      =>      'Water T2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1XLnov8fBKBHRyh1BJnL5P01wqySMFseg'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '5 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Golf V1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1SlOymuwnOfDYzRDhxTpj2XMrovOTZPla'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://lh3.googleusercontent.com/aohbrLMZt8diZJ5uZi3WkXw_NGMV-tB4lRAjYG4GmQWEyiaTfJ9gKQQyhrfDxhknw4wDojSx3wDM7IS1GqV4k7AKJe2hyXH2b8577Q1RkCIy5DH3vKbkgY2hksdcCWRu6W5wPyPGIX9OONCqYGiRWaXUeEPC0KzFJDKk2Wa0U9wsarVtaZlLcPqRim-d5e15BCctLxJ76x3z3P4Pub1y3WculGr2Z091gFNBsNGdtkQ_yevOLHP4hH3OqE4Vg_jv6kltKZWTmidH24BfuIyOMouBbxcT8qgbXKuteFcFmcLP4_bBmsbULmtz1P_1qo5Lg9lDyH2afeQ2Br9v8EDCgjm7ipxiHEV5N1O5C5zlMaRC-eAkpwv5erf78Z39L1uRr1_I20PM_PYNcr5n0IM0z3Vf4dSUnEs9spGcLOoOvDX9z9YEpbvY22vj6heEACQS810l_RHb_MC5u_fgD6nUxtKIXapuvEMcnOKtj17H6_xgk4KtnBKvBLBiWBQsBD1Mw7iwZDvBcz7p-0wMtyzwifr1ZCqxgMQv0pBPfgZnVs-dcrk1rgayQ2TyGmEq43Gt=w1440-h701',
                    'https://lh3.googleusercontent.com/qeGfOKHulWd8z1HqPZPDQZQrdtKIgt_EB2JW0ns0TxLKDCnV7edUnwUdE1rbiP23X7KUnnzybXw3U-i76jcUGCkOgjyobJshc3T2UoQeub5V4hD-j1FuCE9e2l6Xm5cdWDGZj2XSWbgF5OfKMW4Jk73NpIUL4__G2mNKtn_g6PRHPlz5TIH_nPGI5kG6q8TR9XgLwms_MIi3HZ1cDuxyxTMt3Y-MliD-IVOgETvszvwOEP0qMArgqHj_wNJzhtJ3p3PVAxOIHkZaVLP6AN5O2vwzWtPYJT9vyTdmHt3mqOYpkaanITraGeoVTSixurzPGzcZP65KRY1a7UIUgTiBYOz_LhbFrJvhlDVHA0w7Uakx89XcDfo7leXsmMVV3_6QqXE45nwuQ9T0nBWACxevoQu499y3Qca0rS_kUy-1ImfF60UmYPChNXcKiMPRA3Xj0JCqBTF7yFQ5O1Gs1X9bMLHqerBIUITkQe5Z9FLDpI7Ur__kfXwMrpSH8RK5EtXjruEkOVwyV-uSZiAikXrXBgb_ozDLuwiMRLgz2Edeqv7bbz1CFhFtvjajX4GGRxkK=w1440-h701',
                    'https://lh3.googleusercontent.com/nymrku8VQuKkQawLm7Nf_Gi3rNDxKs2UOhDsxZyY1M9UDYF2xDJBvOmZyqogniKz7rotgn09121FOcNQO_JqmGQjWTR9uCT6H79Lls33Mc5DNK7fn_z_peb8EmwvvGLY9rPkQzkvgKW_jJ-oCJOYVWGFCt1uJvoB5OyopPETmrWOcNNZl-kIXBnFVWLlUM8AlCgXNtEA9pTzPvaf7zN-2eTIz1aW_Y1sYtB4PhdO1lehRsJP9wWhN1Txu95bvCTlRFXo_PJC1xTldjo_jNttYBpSBoAaHORbZGyPCM0pE-S8NabRfVC8MR5lCBVzWel2man9-dsZXaQVlMbQUOYeJRGBVl3HbsFp2eppYV1GnHMJrnYdnQVFSZSDXdkDWwsft6ADcWZ5reLAAkowNKnzwhNp-tPdwV36WoPUBdJ72wFANQKkE9p1TzlSh1d5y_StUAtJjgpUUUbisBRnN3LslcoNuJv0Nk5RSf0uUqhlzrRikdJCpoJvkGzBkCCm9Cv86pW6Jt56TMS76BY73qT5xUHYL_6q-Q_LPVe-u9xv9YApoaAk3WyE2T6Ny5sJuE9r=w1440-h701',
                    'https://lh3.googleusercontent.com/4kCQZdTphZEwxFOtFldMrljqQio5sU0i58G-lW5MkYF8KMpfcY5WdyJEYtC4cyk8AXVpn4QTP1o1kGt4kODxCaHx2zAtWjM3GbPG9ND2w6EwZ1ob0n5ky2UQPi1IhYf1y-BQ5PvNX_NRtrgC54ZBeyrvbQ2JAkyiCXvWl_7ZeOPzEuIndHX5hYTYoakSi-0Jj9vogEhouXwTu8x1CzyEmT8-KyWAmdGBZiuwxzcpCJyO_DHmnzJ91_OEhPKjCv_rgVTjP94UcvtQfekqaQAkRJCTLgC649U1PVqn5fnsRqFhvyid7FLMuliDtw_Tg2PnMOiJIdZ9Ucn7M0Ro5-T_8hc-7ZyBEHEZJ4vYu9GOHSElpjOpuQXEGpicoY7RJ_5T-5__gmLu7vyqQBq44TrA9KaTk0lqECm5j-FHEu68nkpL_u1juY69twDIDKflmdYi-CURyn7fdvjxKULUUi8B2n93T60TS3gLfaY7rBKGIXYJZzka79HX9oer-vG9RyfRsmtYISZzdSi5L2lEOssW0xCAs9dbrzCMCax0axdRDrgzf8DFytbIuU_XDVX5zI-f=w1440-h701',
                    'https://lh3.googleusercontent.com/nom-MIfFPJyGyqCmjPihKLxA1OnOWM-UgeGgis-JWwbX1z0EZRnOBVN-vZoBf3LJ6aMFrtINbdjhEQPCPcgV1_zTY6gQYICGV_ch0kB728efMg9jK1h6GMSJZaknI_FHIHwqpi1VCCBd1xUpz1XilfKhdCegiNQh-XN00sgW9Jj5Bcv9D2fccV8T_Iy8lBVMsnWHEZ8OKeaO24zfcZNigkfdc_sh__atLpmFBDoIXH4BRFKQI9hN-kvJk-XdlvEWEGya6neuzsMZ_TfFcScpmkZXYwKAkjTLtIyde1G__jLseE3zGqfNgkeNjysxLv293MNW-fIKtLKFL9oS0kMEaZl3GAEBfMmY2XuPXMtbCSWhWwrOKTmI9ZoqfaxOv3TylIj-iGxQUJcUzcqyniTcxmAesbumPdHsYN_CQFQ4-Ip7E95NGol_1I73zvt9Nf4xpJCpNC8Qf4TJkzPM4zd_QTeeasCu9inzIttGbPoaw7o6C3npd-O6N3JaaLZtL2FytTa0oilEziIQxOUQowiHl9WNfm-QHnzDISNI_qvX5LcQcV2jCgWS3kJ4JjeoWUZA=w1440-h701',
                    'https://lh3.googleusercontent.com/JViQA1ywV255dPAR-aOm_iAOjvlL6bU3hlSOPIY8nRxepxDn_ZpJh-abhu1QxlfRCEkEzjHHqQ4CGivS-ZXKeWLoTB50Z_gwJ77ptrPeDJ6gUTCcvdFxioUU5nZAiR46wxQ5RhA33pBF-OKp2oPOsQnzi_K15oidR7f4gN74MPygTf0jK3TVHc5Vg5acWBLtxclVdKIQmUff6JgtmGp1Q9a9bt6eOuJqleClzU4a1HTBNepa5oPIVDf_WOXgS4Bp-z_eImgKqAt7OYg_rYMW9x-wGN1EwvAvMUI5R2-OFXwKZi8zQwNV_55-YLxW4HpvXCh-tDB5N3OIbqFmVGVe2IqvTu9XW5jnMRjnn5tPX5Qtqgjo1WD0L_ybCNTGxK0umvoaCjcnRRQwxdolj2uO8EFSgrVuCvMhc5z6uyQvjJnqeG1p7ka2oSzhoWpRTfXLdaQxvwekhJWPLTB8MCe680hGhAwoeZepzjeNHdIHvK8JpsSOwgaGYu9ZFpOE_BiydZGq41Wddospp5SQpxXHCpXK_tzIAzAy6dxOEtFiepMo2IbZW5pNQG-OJlkBvrRC=w1440-h701',
                    'https://lh3.googleusercontent.com/h3i5OofrYt4Lyi4bYCxH7pUBM3eABHDK9wU0QnbV_TnidunLXpAXD-xpo2iwbn5VOYQ4SLKZM7tU4vzgCMSB9SpoZgc9T819mMIh2-ij_Xp4_UvlKt06Nt_DKkwb0xqI2d0an4frrGSVhUqDP3Ylvj4cUyzCuk9b3BEg8DmiJ15gK71nj9Afw3wPeJqdNmbOf0oLXCF8yIp7CuWmBAZhRnKGpnWkNhBCGyk9rAPd-TD0b6p_H4bWB1kw1dt_hEEpsHd9GPypjXx4_cxq-uRegDwalAu8CYW2VyEXcBOrCx_xW6zNgZo3hzD6C9jvUZH75ywzUlkIkpq1zqDFW0ugB56oxM8cFucY4MGXYqJvNZieyOSlSH5f43WVIha9_iyPNbO6omP7Cz5IctT2CBfGa8BOYdh_e3KzAJRkT-MxPZgveJqulbqT0-TZYboIZvrC11bxLaxakNBj2o0ZGLVXMOJSpWj6lS2ipzSIz-k2CZfTp1edD1rhPGOODk7MR3dERF8RWcAVq9aqrwjsMlVt11jBbowoVQrQ62KaLzNpHmuaYbDQ9n_wbzc3FQciANvU=w1440-h701',
                    'https://lh3.googleusercontent.com/IfLpMXDYY5ngpOkGqqGHtz1RwjBfl4VXdeIEm6QYhlFUbxXW_3OujBMWPMdzrlQF7Kdz7qmY2lNXYUn_i7eqpIGIILlHGTiqodCqCe9gWSN5-dQx-Q3ed8HWhhxHQEPGrWA0vMLKO7RQNv3tLZyEbHaKQeOhrsid_4VD81qr3ZFFUEEn5o4q08b-Ih5uHB1zC-AER1GeyoiD0A85qEMiecTjrK6riU2N-AyJrZ8QdgqjBWIBKH00D7s72DoJ1VPCCWJrgGwBrrx8f-dDrXBrRr3MT1G5dJ55Ne2QuR50c8sLqDJnz4wy1zd9Sm7BgNEkrmQpNIN11g3SbBIBt3GuxjdlG3Lb0MuaDt9qHnXUSecR7KuAGAbrHNPHkp5KGK_R4TpQZ_U7b5K073yCtFciDjq5RgLzFAEgZ8_w5YRhf3c3mjFw65HBZHGos4BQIZqEmGiUkeYgNv7xbkGiMbsgbTY5PtT9RaZAEKLomkwLtcym1ouaY8LfxpYsYJ9vkuSUAaRsPrJ3TWvrV7FTwfMg3iBI4DHec3bsMHwvI25CVLAOi28dopHgFjZjERgFgA6f=w1440-h701',
                    'https://lh3.googleusercontent.com/KguIncCDN8woebaI-a6gRRbBD3uQFAP4_um3u7C4Q8RNUuLhyu-vpjs4nPkw7KotCy9PDT9Emz9RP5AAd80ENfVavGc8ZHBAKJgkFdAnUxi2xvHRIVmS_kZKIdNcV1uzr339KIs2lm1NN9GL2EA2mox1psH-uDCaTp00b-vd_jrxE8t_VteRIdiC4hEXKjfs2_E_naqSpJTXJXgYvY6OnCPQDxioaOaFHrk0jZX2YCNFn_hBlSSoBNKSyYykJ-yEu_jS5nuxvQRFGO7b6A_aEf_b4YL3nCPGJfnBRAIsr2VfMlEK7G-GCaIqAvvDaBEX5X6I-bAj3wf-TKefj-j92bMYLTqBSuG869UlK9D4V42Y1fwvRbyUD5TBukIycySyosanQu1KZAU3RCh3jOORg7JkxwznX0t51m_svMlZnW-wc633vEQSSJwRpO0tmXM5h4L4y-SBo-CRYJXYErhB80GFdmuxaQ0IA4_cFhjU3LcZkg9x0zjzhsCIebd4_k8JQtJif3tUVE9MlTlPeJyvWcaoetSK2H8jh6-6aTu9GD7k4wfCCK9GdkOvz4Xl5fd4=w1440-h701',
                    'https://lh3.googleusercontent.com/UXNtlXGCFHkNZA9iEsL-cFQldIg8kdQ84Nll7BnZgGJnMOeH3mF1Fjg4Hs6r6zvgSko1NDThVsXgZzWO5Vnssjyc_w5JkJFKg8ZHxhFmV6-TWXVL1qco-zoYaIz7--BC0paTGCngP5tfRXGdBcKThF-Jvc7dHxaUSiCT9p4rPYVWltxr_p9Z_WSTRdCb1eepiaGPNITuKqCPD2CUS5gfXMP3kUzZkYCZqHeJICeRjCFdmqdf57hRvi4hqikfntRnbmlhvKndYyC_rp9QX2Gf8gThh6Gz0Q7ta1Xvz0scpdkytlJjDhf_ozhfDnh8kmNvLs7anPQMRLEv0Sqsxa6-XpJY5naCI68K7jbqBL9Ktf96zvwLpXAb4Vn0PsHEnVnRk7vrbPP5zPG3vPKxsuH13m9XMShsXqBZ1x00tP0ghcu3jMnwGezxRdqcBp1e1j2kPwKVuVynMlnZfF_m5WDo5pnqjOdnAjDzRU-QxdXCnIZ6-ooAby_XU_KCbFUTXBg6rNFhfjFpa-YkifDQz9_yWQazWRVWrnNZSyuqJK3Esc28pYx2KbE6wx97KxuClo4D=w1440-h701',
                    'https://lh3.googleusercontent.com/wl8LA9_00TGrTk5n28tROi2dJaYF8cnVDheF2FGr4K2AWB0-RMJqzgJxYj5e71GVtTX-XJFIHjeQvRFezQfhteIQU-8NqzcWXkjlF0wsY7y9T000zr5Cn3ybAi0HDUCZIexUPa7xk8nAsw3iok4qRTfU0w8dAecYV6v90p9P43LBfbkHaZ6RnfY5uiGg8ilCZVZUm1eq1tGiIaCq1rGTXdPqOwcwXEHpW5AFTkZ3f40Rd014UFNV3szFBet60L0WBG_L0Cxv1GSb9e45hRtoc2ibc3Tq6DeqTjT21iKk151nVLTNgkulol6IIxNoumLTO7CXQJ-kyyHyM-bNGMQq0zJoj7U--KiZmqaXR2H9rGprafroKuu_UPlWcRLdYkTYkTpgsZzvo-wTDE5QFYWKcKxjWgL9x0308ceLRCO6UqSKqPqkDV1PiIKIF0as-9B8LbFRxuWe-NpwobqTviitcn2S3969-0PPo8ecpZRceaMN7Z2BaXHkkC7MvU24sazdN-Fwb8Q3LD1heaVZKqttEiVErgkjC65eUAdr-eLKqV-XwsAmfY0ozCmlOJq34COh=w1440-h701',
                    'https://lh3.googleusercontent.com/IN73lm3f_ugkL_m6iaFlbQUZWO32oitKLIM_V1c6F812Ne9oN520xYI-TWg52JrXlTEnumj7PKptt4_qUql_caKENzhMJ7_SNPdZkayexyubamPb36iiTkY8ToFGvTizZSmvh_2vKcwE8zwv6eE9GbNo7GP0M8V2Ez6G4Tz9EwOpLAn7B9aVPGFRXCdMGI5L93CFdSQRmX0L4D8hJVelPWp6F_KwQhPb_0pTfiWHAG3daaphH72R742ct9eDC_uxRZv-UR8xmucNJxeXIhsGnsWvP2wuUON8GhgVRy-D7D1UC8I-S4sVVWedN52zUHTgXs-U3F-soKTX__F48cAAl_GLdhXvo4VnBs3uUzopNZgmD38V5jsPn3oxROPJtxfWCYEr98ZQCkdYIsk4YmfQIPg1NxYmIO_0O7cSlYkoCePKJTk1Yqu96oL8Tbl6b9I0QgvnTd4eTtThLaroRl2vKV7Mp61BU8kZCt2mJx7kLtyIbOB9t1cT7aqi-J_mbKtmEIAblGaJ2ldp6G80pBhowUYBqQKycFYI-DYPnxtXEFXrwQNzd4YtpF7gCl6Mw2la=w1440-h701',
                    'https://lh3.googleusercontent.com/tz4YS5jtpVdnSRt06Ob3ozAxtTGiO9fcuHLMlKW-3ATyUzD_59EZq5NaZnweWTKhuXDyzLiqqHQJp37iS-AEdIQBzarUINc-IJpUTZnY2dJvUQgc3sO5AL1YUmE4tDzEzSm6U94-g2rFDAq7X9HJJv01dpU_yYxUZDkJswpsIMmidVohvmP--8UVlAC15XjsXIKZaP47mrJfSNqyIg_Ltqd2Gf6F_uTEa9gwf4EwsuGN5ug4DnTnfUCDqMm8OL6CzTTbrFChluFFFXTiD5iW_Z2lseLXnX5g6yuA9HK5Um1gKs9_VRTAQ3Cu1wci5QWJmu9ZALb-cwA_Yjm_LDcjlmsE8PxSu8KHednXAI3kkvDnMJISPw09y3gzHag3S0EV3ctgdZJBw3rsHJydfCReoY5BJ0p9cEiniXsxYFDqHCTNvDQcupaxum8mtmOeDd22qbsvc-WQKAEc8qhM-_VdjSHSXiWttVCW9qjs9kz_DU4uU7WF1KnzZ8_bmlSTe8xrqoW0RT8QWIgNk43I7g-803nylOqEbbNfou-ZhGmvsWmkQRCC3qSj0W6abp8PlZwF=w1440-h701'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/10",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/11",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/desktop/12"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/8",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/9",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/10",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/11",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/jumeirah-luxury/projects/jumeirah-luxury/gallery/mobile/12"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Jumeirah Luxury is located in Jumeirah Golf Estates in Dubai. This development is an amazing place to call home due to it being surrounded by state-of-the art golf estates. Jumeirah Golf Estates is located directly adjacent to Emirates Road behind the International Media Production Zone. This four-course golf estate provides its residents with fine Golf Estate living. The homes are styled with a Mediterranean theme. The location is also excellent as it provides easy access to the rest of Dubai and the other Emirates by being situated along the Emirates Road. The four courses have been designed based on the different elements of Life such as Fire, Water, Wind, and Earth. Each of these courses has been designed by a professional golfer, the Earth and Fire golf courses were designed by Greg Norman, the Wind course designed Sergio Garcia, Pete Dye and Greg Norman and the Water course designed by Vijay Singh.&nbsp; The Jumeirah Luxury development has many amenities and facilities such as smart home technology, security gate, high-speed internet service, video security, garage, residents lounge area, gymnasium, meeting room, bar, BBQ area and a standard pool. These villas have anywhere between 2 to 5 bedrooms. The built-up area is 2012sqft to 3460sqft and the plot sizes vary from 1911.13 to 2813.79. Residents at these villas can enjoy that they are pet-friendly.&nbsp; The best aspects of living in this development are that it is a golf course community and therefore the residents can benefit from having tranquil and green surroundings. Jumeirah Luxury is easily a high-quality development in a prime area which offers great value for money. The area has very modern and contemporary design which makes it an interesting place to live in the city. The interior décor is very modern and ensures residents enjoy living in a spacious and modern home in Jumeirah Golf Estates'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Townhouses',
                'count'                     =>  291,
                'type'                      =>  '3,4 & 5 bedroom townhouses',
                'amenities'                 =>  [ 'Bar', 'BBQ area', 'Common garden', 'Garage', 'Golf course', 'Gymnasium', 'High-speed internet', 'Meeting room', 'Private storage', 'Residents lounge area', 'Restaurants', 'Retail area', 'Security gate', 'Smart home technology', 'Standard pool', 'Video security' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  2300000,
                'emi'                       =>  2300000,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "|  50%  |  Construction",
                    "|  50%  |  Handover"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  ''
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Q4 2019',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ]
    ]
],
[
    '_id'		=>	'zd-eagle-hills-31290',
    'stub'		=>	'eagle-hills',
    'contact'   =>  [
        'name' 		     =>  'Eagle Hills',
        'email'          =>  'lmt.global@eaglehills.com',
        'phone'          =>  '97142482506',
        'whatsapp_link'  =>  'https://api.whatsapp.com/send?phone=+971554327933&text=Hi%20,%20I%20want%20to%20buy%20this%20apartment%20.%20Please%20contact%20me%20back',
        'website'        =>	 '',
        'orn'            =>  '',
        'address'        =>  [
            'address_line_one'  =>  '',
            'state'             =>  '',
            'city'              =>  '',
            'country'           =>  '',
            'postal_code'       =>  ''
        ]
    ],
    'image' 	 =>  [
        'logo'		=>	'https://drive.google.com/uc?export=view&id=1lWEDArSHEcODGLumaACJNq_aY5lDIbPM',
        'banner'    =>  '',
        'gallery'   =>  []
    ],
    'write_up'   =>  [
        'description'   =>  ''
    ],
    'settings'   =>  [
        'status'    =>  true,
        'approved'  =>  true,
        'role_id'   =>  5,
        'chat'      =>  false,
        'lead'      =>  [
            'admin'      =>  [
                'email'          =>  'info@eaglehills.ae',
                'receive_emails' =>  false,
                'cc'             =>  ['lmt.global@eaglehills.com']
            ],
            'call_tracking'   =>  [
                'track'       =>  true,
                'phone'       =>  '97145509199'
            ]
        ]
    ],
    'featured'  =>  [
        'status'    =>  false,
        'from'      =>  null,
        'to'        =>  null
    ],
    'projects'	=>   [
        [
            '_id'						=>	'zj-sapphire-residence-31290',
            'ref_no'                    =>  'sapphire-residence-31290',
            'name'                      =>	'Sapphire Beach Residence',
            'web_link'                  =>  '',
            'area' 					    =>	'Maryam Island',
            'city'						=>	'Sharjah',
            'coordinates'               =>  [
                'lat'                       =>  25.320929,
                'lng'                       =>  55.360272
            ],
            'image'                     =>  [
                'banner'                    =>  'https://drive.google.com/uc?export=view&id=1WsrDdNVxJU3r7FlRSAj61f-MT0OtgdH1',
                'floor_plans'               =>  [
                    [
                        'name'      =>      'Studio',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type ST-A3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1xcmCRnqvZ6PkqxPyRjr7UI0ew9V0Ea6F'
                            ],
                            [
                                'type'      =>      'Type ST-A1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1HxqmAqHhMiRKQgImm951UxqsTRU9o255'
                            ],
                            [
                                'type'      =>      'Type ST-A2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1QNIoOhfzYi6oUqXmupE0lOtUe6-BnwkH'
                            ],
                            [
                                'type'      =>      'Type ST-A6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1oursDZ34ml1nc0OUrJIEvcvPKD7lBCLz'
                            ],
                            [
                                'type'      =>      'Type ST-A2M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=191HKhlH5PXQVidBo_fWPNvVSxKMECP-6'
                            ],
                            [
                                'type'      =>      'Type ST-A1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1bvo0ugnUzRDI7YF_mG2REAE73s7gnqBO'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1A-7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=15kz3RMimLn0_cpi1UlMliDLPLotvVzuJ'
                            ],
                            [
                                'type'      =>      'Type 1B-2M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1jdCco8wTEaxJJ1J_3XiTI8vGoGdVJhnn'
                            ],
                            [
                                'type'      =>      'Type 1B-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1vMDnLL2STVK7XECsasGO0Xgt3W1-MHaJ'
                            ],
                            [
                                'type'      =>      'Type 1A-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1MzEYZ_bbwkNf1AZJM2i1neOnJdwJ-vSp'
                            ],
                            [
                                'type'      =>      'Type 1B-3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1BzO4tsQKO2ur7ucFg3lMrh5b5hHsmTqe'
                            ],
                            [
                                'type'      =>      'Type 1A-3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1PJTfxEkJ9usv59xWPaAIH6Q_4s4zYJ3E'
                            ],
                            [
                                'type'      =>      'Type 1A-5M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=16XomMQ1Qs1shSw4a-6oS2NO1mNF277VN'
                            ],
                            [
                                'type'      =>      'Type 1B-4M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1VKNGnSYqlmlZTfS2b7trGXUrrozovXUK'
                            ],
                            [
                                'type'      =>      'Type 1A-3M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1qT-Ylnap-zYGDBVuINrGF_WeIOy64GPG'
                            ],
                            [
                                'type'      =>      'Type 1A-6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=180GYNwK17S7ZjXUnxB2qQKyWZaXZDqpo'
                            ],
                            [
                                'type'      =>      'Type 1A-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1GkublsJkZslZgdr7cYHZGvkFV2NgT_iQ'
                            ],
                            [
                                'type'      =>      'Type 1B-5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1T3XhGZmJGPQE_gSnJyYjsoi3AbLMoyby'
                            ],
                            [
                                'type'      =>      'Type 1A-4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1DV4DSy_hWQsml0-fs-D6yr3o22ZdshWb'
                            ],
                            [
                                'type'      =>      'Type 1B-3M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=11t-4C_heFuFQyXA29pQheyLPO-jeWA8z'
                            ],
                            [
                                'type'      =>      'Type 1A-4M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1cbLA90Fl51nt3Yww26mcRqxuWFCg9qBl'
                            ],
                            [
                                'type'      =>      'Type 1B-4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1JbmBKQGt8QsLc4a0QesXWmgyHo-wzFtv'
                            ],
                            [
                                'type'      =>      'Type 1B-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ZhpuCIwjppRio968_XtuRsbmVlXMP3Gx'
                            ],
                            [
                                'type'      =>      'Type 1B-5M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1sHErF8PwItwab3iebMiJ6z7uAPieMV7z'
                            ],
                            [
                                'type'      =>      'Type 1A-5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=11vFd1MjSNCj3NbFAVXNZ4K5LT7RxKz5F'
                            ],
                            [
                                'type'      =>      'Type 1B-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1KcWmCxbNuFS8Sljb0kZ73z868jXL64Zt'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 2J-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1MDblHAm6TOWOhwJySaRGrhFkKL_yJO26'
                            ],
                            [
                                'type'      =>      'Type 2F-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1MvDrWv4a47769Rel5eCZgx943Qm16lZa'
                            ],
                            [
                                'type'      =>      'Type 2F-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1_ZVgviBPeXmZcEo_m572zpS19eE566Z4'
                            ],
                            [
                                'type'      =>      'Type 2J-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1u8QE8ERn6Xl_pydnOyArjPvsIqzny5nc'
                            ],
                            [
                                'type'      =>      'Type 2G-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1PmfbC_fF92AuOEwl8h87UUYIsQe1NG2s'
                            ],
                            [
                                'type'      =>      'Type 2E-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1hUC7q5BcOkVsG3klGA7_HlxIK_YMjQFM'
                            ],
                            [
                                'type'      =>      'Type 2D-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1x8fr_3UrqxEHkZbhroN5yOoOu-u7-8pA'
                            ],
                            [
                                'type'      =>      'Type 2A-4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1yv20tU81n1plwHc2dBqj5MKl7I9hpJZ1'
                            ],
                            [
                                'type'      =>      'Type 2B-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1TkKyIGAeKv8Wr0Hq1ALfp9dPP5EFgeJO'
                            ],
                            [
                                'type'      =>      'Type 2G-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1DX_3-n_9ckFtMhrwSmt5Snf-gQbCLPZx'
                            ],
                            [
                                'type'      =>      'Type 2B-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1G3ZgEudWLYgAJ4xaQfXDAw06wqmC2xlc'
                            ],
                            [
                                'type'      =>      'Type 2D-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1SpQLOPj7I9F5Gxij0cnQw2-hOHcJO-RS'
                            ],
                            [
                                'type'      =>      'Type 2E-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1m3cq9kuweBq6QvKotzZPwomQbj1vJXmF'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '3 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 3B-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1-3vPYQqd2vu3ecN-IQmhHqP1g1Vn6f43'
                            ],
                            [
                                'type'      =>      'Type 3D-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1_HGpiiOpgFWaNGDsB9BIb3U_hSnyMeFJ'
                            ],
                            [
                                'type'      =>      'Type 3C-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=15NWB7LhlXS8XaMNJ3RcseCUkFP2KsXD5'
                            ],
                            [
                                'type'      =>      'Type 3B-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=16Bvc5nx8hrM2clYGmKMWq9vbB0KaEcgD'
                            ],
                            [
                                'type'      =>      'Type 3C-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Gn7KUU864to3WHTX1MbR3gSv_xwMH7e0'
                            ],
                            [
                                'type'      =>      'Type 3A-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=11QKIQ9BrIK4hpIKVAdl9FvPSA6XcASK0'
                            ],
                            [
                                'type'      =>      'Type 3C-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1lB27qHPh5Mx-KfUFeUrA1NXIIS2hubMJ'
                            ],
                            [
                                'type'      =>      'Type 3B-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1CW6_UAvPmjpKYM2oTyu-7_QQbV7MMJoh'
                            ],
                            [
                                'type'      =>      'Type 3A-1M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1QJ1LRAhhUf-ApxONOeDB73YAZqyED4kH'
                            ],
                            [
                                'type'      =>      'Type 3D-3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1o57PWJEs-MrNFrccRko4B3WVU7dNv_QV'
                            ]
                            ,[
                                'type'      =>      'Type 3D-2M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=10zK81KG6zy3yajsId9fe_2h1I-XDYpXs'
                            ],
                            [
                                'type'      =>      'Type 3B-2M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1hDfK3sSvEfoaZEIZC-986O5UWf3BLu9F'
                            ],
                            [
                                'type'      =>      'Type 3C-2M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1hHVbkPeDV5JM4_iVDknHfhcaXf171CbV'
                            ],
                            [
                                'type'      =>      'Type 3D-3M',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ep0aPai2YCX76MNdKxIUuuyejrqLvZQS'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://drive.google.com/uc?export=view&id=1NAFZi-k-jwQNBNf9uQqvkNnaqLv9ypwh',
                    'https://drive.google.com/uc?export=view&id=1ns3dueTi81LSz_6D1RZwva1ENi9MNVkl',
                    'https://drive.google.com/uc?export=view&id=1vXNowdTeJvVdfHlc6Lk5mZPgQomPnIeN',
                    'https://drive.google.com/uc?export=view&id=1H4CyGNDvmOJFcpBdDbBS4uWPWDmE_We4',
                    'https://drive.google.com/uc?export=view&id=1WsrDdNVxJU3r7FlRSAj61f-MT0OtgdH1',
                    'https://drive.google.com/uc?export=view&id=1upG614QzM1u0S28sODg6dE8RoPbr7YI'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/desktop/4"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/eagle-hills/projects/sapphire-residence/gallery/mobile/4"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Sapphire Beach Residence is one of the first residential buildings released at the flagship Maryam Island, a premier mixed-use development that is home to exquisitely crafted homes, state-of-the-art facilities, premium community services and a wide range of retail and leisure experiences. Sapphire Beach Residence is situated at the heart of Sharjah’s most impressive waterfront destination - Maryam Island - which will offer world-class urban planning design and unparalleled views of both the Arabian Sea and the city’s downtown skyline. Since its founding in 2014, Eagle Hills has realised an impressive growth plan, which has seen it enter into agreements to support the redevelopment and reinvigoration of a number of cities in Africa, Eastern Europe and the Middle East.
                As an Abu Dhabi-based private real estate investment and development company, Eagle Hills has so far achieved a number of significant milestones, with more ambitious plans to be unveiled, including the debut of several ground-breaking new projects from its operations in Serbia, Bahrain, Jordan, Morocco, Oman, and the United Arab Emirates.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  0,
                'type'                      =>  'Studio, 1, 2 & 3 Bedrooms',
                'amenities'                 =>  [ 'Balcony', 'Built In Wardrobes', 'Central AC', 'Childrens Play Area', 'Communal Gardens', 'Covered Parking', 'Gym', 'Fully Fitted Kitchen', 'Shops', 'Pets Allowed', 'Public Parking', 'Public Transport', 'View Of Sea', 'Restaurants' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  328000,
                'emi'                       =>  328000,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Down Payment       | 10%   | On Booking",
                    "1st Instalment     | 10%   | 1st Nov 2018",
                    "2nd Instalment     | 10%   | 1st Apr 2019",
                    "Final Installment  | 70%   | Completion 30th Jun 2020"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  ''
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Q2 2020',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ]
    ]
],
[
    '_id'		=>	'zd-ellington-82476',
    'stub'		=>	'ellington',
    'contact'   =>  [
        'name' 		     =>  'Ellington',
        'email'          =>  'info@ellingtongroup.com',
        'phone'          =>  '+971 4 278 0888',
        'whatsapp_link'  =>  'https://api.whatsapp.com/send?phone=+971554327933&text=Hi%20,%20I%20want%20to%20buy%20this%20apartment%20.%20Please%20contact%20me%20back',
        'website'        =>	 '',
        'orn'            =>  '',
        'address'        =>  [
            'address_line_one'  =>  '',
            'state'             =>  '',
            'city'              =>  '',
            'country'           =>  '',
            'postal_code'       =>  ''
        ]
    ],
    'image' 	 =>  [
        'logo'		=>	'https://drive.google.com/uc?export=view&id=17b2gf3F28g5hAIr0pFsyB7d1AoNobkru',
        'banner'    =>  'https://lh3.googleusercontent.com/0adGjZhUW8WghBCdJam4-FjaaTuAJT9KfUI96QIDHWmQEuTPaGWI0Ac091zzD7X57vgIaZc2QkAOXZre436Ba8Ad5twsRmAmBUlRyBLRnj4-Y08oY8Ip6x1YxzlV3v9-pE8Tg3SRUyuK5XH8zl614WTc9kBrDfCOaYEfUeS80qycexxa5JiJCgdJxGBXthCFM0Oj79V5adnxqv8wZHQ6fIBSR4z8yQfc1eeDU42h6PU1786Ot_AqaxEgytjYEpcIEZK6zmCv6jyIrSHZZ9WbBUi9qOCd4MOIDQSbgZ2R5MEhJsFm5jVOkCi-9EZ5YI_6alsLgnTZ-nKlkewtUvj1M4gvhxT69TYuA2Jzu-zdQcRC7mPI4TDWpuG1ky2SKrV6Jw6_M3BX3witBQ49myQnMY3FHnxHX-GVCyrQSo4xOYe5JMftgcGLaCLCl8oz1k8VpbnCoZfv2ue7tHIgRqhthb7dPhLlDGkEy-0DxNmCQu3Lf2EOa1BahLD-6oT9rqtJb3BMBLkTe02QL8j2kOdpUidYCn3IqqG5HAcW6SKx62UXKwDXDsjtj0JpUYoI0UXF=w1440-h701',
        'gallery'   =>  []
    ],
    'write_up'   =>  [
        'description'   =>  'Founded in 2014, Ellington endeavours to craft beautiful environments for exceptionally high-quality lifestyles. Inspired by art and reflective of their owners’ aspirations, Ellington residences are classic in feel but contemporary of vision, canvases to be filled with the experience and exuberance of life. Our customers’ refined tastes inspire us to develop architecturally compelling residences that consistently transcend fashions and trends. Ellington\'s current projects include high-rise luxury residences and multi-family communities in Dubai, located in the prestigious MBR City, Downtown Dubai, Palm Jumeirah and upcoming Jumeirah Village Circle. Where contemporary meets classic, Ellington envisions a new way of living for discerning homeowners in Dubai and beyond.'
    ],
    'settings'   =>  [
        'status'    =>  true,
        'approved'  =>  true,
        'role_id'   =>  5,
        'chat'      =>  false,
        'lead'      =>  [
            'admin'      =>  [
                'email'          =>  'info@ellingtongroup.com',
                'receive_emails' =>  true,
                'cc'             =>  []
            ],
            'call_tracking'   =>  [
                'track'       =>  false,
                'phone'       =>  ''
            ]
        ]
    ],
    'featured'  =>  [
        'status'    =>  false,
        'from'      =>  null,
        'to'        =>  null
    ],
    'projects'	=>   [
        [
            '_id'						=>	'zj-wilton-terraces-II-78653',
            'ref_no'                    =>  'wilton-terraces-II-78653',
            'name'                      =>	'Wilton Terraces II',
            'web_link'                  =>  '',
            'area' 					    =>	'Mohammad Bin Rashid City',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.178328,
                'lng'                       =>  55.308754
            ],
            'image'                     =>  [
                'banner'                    =>  'https://lh3.googleusercontent.com/Ed4tFGM_Uktg0JQp2bZ4X3ONAYUffBN0dzHCLS7Nt1ri_VcGIA_5kaS2DaadLe4k10uylqCDGicjKT9XkzOIh0bi5cpsSnaxSG2Z47jzGM8JgAhdOZNZ4z-H89wXl1wtenzMnWBibnlCeF4MpMz7mG6yRH68SG1t5Q0nC3p9-cVNo2LXgwIH2DzhXxM88QH6nLho_5GH9Kd3ixaSX8kiMlW9vZ2Rf9B7CGKf5kAD-J3pyyetsiiSU2vT7EQ2RsVvYlwKAOEpEKccvmeSoDWyQF5v58lkwSHVY2dYZTtHaPPnWSuwOKu9uMYJkEmFkA0dTnp1erKrS_xEHHUxDIyepaPOC1WJj71AulGAmX7HrfGsQAPSZ58Z4lZYUaxqqSCt3B2cBofxUarecBg1yZvowRd7JqAH6NW677MIgba3KNDnePvymKgqHedIRO4uC6nk8HKUyhchCdOWO9WVKmAX26Ym99-xXlFDElX7rVAG0Dglg-PIlSjTzGPZoLYI2MylPpqjFEQH-hWcegC_FAvUFYBllSGM30pDtUNzxVcQONcH4uD2HxPDQ5OxLcRTmUvj=w1440-h701',
                'floor_plans'               =>  [
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1sRWa1MhpUvSDWmwefK4fFAeQffkzL20w'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1rkGLhxVdOGfkTxiesFgYAkYENrtZ8bdU'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=10TSUzD9lYqRrabxCYSoLy9TxYcKzwlFF'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1n_l8h9Ql9Qm48ourERZGEwO6tNmy7Tdg'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://lh3.googleusercontent.com/4eCTvTp626WasxASLCuZHOXGq_hbPSYYjNw1H5IaV2wJjlsvoAKfVD1ljuqA94i_3iX-iQuOucKEL1FQiOlv1T96uh4ZtXMBwtEvvdr65xDWLShSzMejAxHhkVHdkiE2cgenKdQTPn9ahOw4Eqa85qHHICpvQCYzwseyJI4QbAu8gv8jNG-mySaybXM-eVpVyGrVy2q9RtzhVUA7azXXaJ-6YtvPov9CJIbRWSwg-O9UOA6QfQZ9OLGw2oFvmdo9w8B-I7QL0BInKwreEaIAR1oKzJuHsrNwng1erxz4PYx2YKo2szGgmyuatX_1G53X8LKh54lge7kGhE3y2G-0C0sM0IaHhJGVCbT1oGH7KAZXqrh8ZECritiJZVnQXM8aIVRENGkSXawHv6oSdGh7nwMZZAolOWTAafkKRbDrdngK9kXCWq3rz9guAhADA8i7h6Gy_2tZ961lcVB9MTLOewOnugOTsy1srPcHtOfmzvg7RlFNUcpLDQWYYi7njKym6N6WsoOl_JdlzxEwSGoCxsyINiqA4UDaFxlKjFK0-dWs91s22n5UrbYH5rvPKTUo=w1440-h701',
                    'https://lh3.googleusercontent.com/BNHJvHeydkm6LsuGtwUTA4SDCJOJNNsyiHqVUqeKS5Rfo2MLM2VvoBSYWVj-tfCz-HWzI2uaLYIJvRgYTZDZvVxiXBiEKFfZO9TFoAWymRg4rGnzbk00uM1_QAfPkduZGvWq7FPh3JBzeVpHbs6fE8dmgloh43IBbAqskQ80bgv7MzD5_DnbHIYNB11Oi0xlNlD1uXlw_A-0fOIEC2wzvL7J3ic3zm22DW-dm660j8Fb-Y-1L5oRTAMbjiKfH1dXkbt9bqf4uKYesdHGF3MQt5p2m5uv-Jcz9O_2YGvsBsLWsMOj3tYD1TzUpRCb-1s3YSZIfHk2RN17-KyK5gTLaCsKJjMIXmk7j0MCjrd4eZj0qqqSw905HGVRA5qTKmQ36zeBJmMPkl3rMjUlh6wTiA4K2SnXTNIBNw3vwrhVY2A-YdB_10uBXoTKXtATeffCSmISXsVoSZbSNwaBvynRP4VJnCx5V3Bu658kg68txzHwOeB5z10HaP9qUngCS8tdZLqyNQBIKGysdINpmfg_xcBSVPnZgnf8PzsvJA0POaLXFEyDBT2Om76Q-cqF6526=w1440-h701',
                    'https://lh3.googleusercontent.com/Rzme4yGZzOE4GIAKUbOT1SHRTMX0gNgnUc_pIIqO3truniVWlRy3NlM6bW1R8LeN0GHZN0RGdFI1J29uHTVJEOrsbeTerBR59ZTwsg-qhliOfv3WGrVv4QbmboUzI9iC5qziMjhQ1XOINBG0z2y4YtZuq-fjI8wkYWd_hRkUlHRd8G2J0CraDfqo8SaWl6axiz0ePD_oQ_n935oI1rqu0i1vC0A9nsAwXQgfoZqRS6oLB133ZsCjYn7elmAxbDW9iG758QsIvlUlDgfn7Nt9zgEgIVX674I2U3JAZWU8GBifgZzoUUBrtzlaKxwNNxrtjaLtNEO83D_HFe5PsLMVWr6DL41lGQcpXKnhOuJqs28jA-vpE15tAm4wktnDeS8HXEGZAaDgZN6HjhuPDPN9gcW2ExQNdoXJFyKH3lXRCxYBp53Y5X1Yb3fUzv427lEbUtnzfOPReeCEozZAfNtyOGUaS4zHfFW_uTzHUI-fvImBKk9OnMarQkqHZDa6-Slwvn6bbtQp15Cq3IX2lkF-cTSDf_gbFuNTegIJ9OsnrnV6GjVVhUvpOF49COIIvqCp=w1440-h701',
                    'https://lh3.googleusercontent.com/JQdrk9GaiG3djzLdKtcyBG32dGrsjEkrnK69vo8VMTtVjDmw3h14q3wigNbyXe3-Emr_44hwAN7WvHUBjcdzpM3HXXSTJe9U1FLlcTkAYRpcfxmLJKieW44yYk-KjBo_K3K6YsGLqbKEdgwcoAhCZSgFiZzT13qVsivzU-umbIRmGAkwbOYsqMTdIB_pxiZl8gS0r5F3Njh-ugPMrJIoO_j6TrUcP_1lk4WSDnJXe9Xy_rvk3Odp6QAk-8kgxbk_PZdOF0m2oZE3xxtjR2dSdd5g1qV1IY_NJrEaNVo-dAx86MnhXLtD0FJLseirgnBnqIjcNjlBfd9pEmhz77YCuqDGdmG1SQff9cdbXae9oBxIstmQ64NhZQC1PwbPWVaMaYXv47kqDcnsAgHgUI6D-EmKBP1lfVRH72-NH9eZIuTT5LryyvCYKum3otRpYUN3UNMKlzAep4YGvjGkXCYl2GNtCKmmCoUYyLpx09Ct57JZAe3lj1g7yyXwiv0f8QwIb_t_6Yo8UVIj81Y43ylgAjWC9JLQegY7kWrZEz5fxiDM5xzhtYFUFbEmZBbp0Wg8=w1440-h701',
                    'https://lh3.googleusercontent.com/nfb5lrX7W96o0w4xc3ppiaxokMVBNw4OzIwhFCKJOl7ZMcrpw-ZSVmlWU9gdTO5b4rAG0LkUTsNqghXDCLQhBg8AChqRjc-gR2YvVD-QYFi9zY6jLUZ0zMmGT1RaiCfJlZj3H4RIcRGwctSdARy88YvWJJjiNE_ROwCiCfzwHuSYgp-HBDjpeW4KV2OrTmViKFfa-9lxPOyAsFcV23o_sFHyD3JtWIpAiXhoryLAvZhJB1_LJ7a_NoexOYGbYo4bS4JFNFWSLpecE-1j5F08y1WGnnaL2lG7kDXpK8tJ7msiPYmtQCrlg2F9qxjK8kGVZGFw6jAhvkKlUSstzwJ4xv4r8RDNSs0NqfEdabHeJ-IEuMsdbfKEHuyPLS_LE_Y_WFOLAoRBQ9nL5qgGHEEAWVMomxte9LPsAqjlTkJs6SYtLHkStJqVQUyNLyQ5pYzx2wRC4VdqthiyFRG86X8IQ7OE6yUb2fJiciy7Ay7myColsHCMOPrLiwliTif2I2sbkXo-3huQzIPWcRK7hTquOwUbOofoBwZh03hYMmAD-uLhP5IVmSVK1rVLLn9wfuoY=w1440-h701',
                    'https://lh3.googleusercontent.com/Ed4tFGM_Uktg0JQp2bZ4X3ONAYUffBN0dzHCLS7Nt1ri_VcGIA_5kaS2DaadLe4k10uylqCDGicjKT9XkzOIh0bi5cpsSnaxSG2Z47jzGM8JgAhdOZNZ4z-H89wXl1wtenzMnWBibnlCeF4MpMz7mG6yRH68SG1t5Q0nC3p9-cVNo2LXgwIH2DzhXxM88QH6nLho_5GH9Kd3ixaSX8kiMlW9vZ2Rf9B7CGKf5kAD-J3pyyetsiiSU2vT7EQ2RsVvYlwKAOEpEKccvmeSoDWyQF5v58lkwSHVY2dYZTtHaPPnWSuwOKu9uMYJkEmFkA0dTnp1erKrS_xEHHUxDIyepaPOC1WJj71AulGAmX7HrfGsQAPSZ58Z4lZYUaxqqSCt3B2cBofxUarecBg1yZvowRd7JqAH6NW677MIgba3KNDnePvymKgqHedIRO4uC6nk8HKUyhchCdOWO9WVKmAX26Ym99-xXlFDElX7rVAG0Dglg-PIlSjTzGPZoLYI2MylPpqjFEQH-hWcegC_FAvUFYBllSGM30pDtUNzxVcQONcH4uD2HxPDQ5OxLcRTmUvj=w1440-h701',
                    'https://lh3.googleusercontent.com/l9G5yq1_ziT1O4t-tf34O61p_Gf7010_NXj7n-9unfpS9gWh5D6IUwPbrnIhhFdWsQHqLrZmlPBTrpsFi6b4Q8j3Wx4oH0cesDk1l_4OzTwzaYNKy1x0MWnUq8zUMulVtMOSzmX-hyZcjQEiuBQ_4t8NCVkJZbZ-GwXB_IyJEHuLzUsKijwrlld_OBcC3dwXF6MaJPiusdjZSUKoqvC7T-djWOyJ-V_K1zYjxngiZx1v7b8XngZ-Kqwq8eiBcL5Pl-GWW9s5wQY5urjSdNIbYCOpTIWRHiqMmh1YeQBcWPODwYYWWxFyVsIPU-himbQoT0RaFvB4BvYr9f692Gt2Ai-yJUq_qmpzR7xisr06Ht48sJfQ5sLx6sqQ2iPm4cYACZ-_NUEb2ofiP0Y53FNorcFL8H2Q04pdg0gCH-pm__9yidod_MEiNT-wxFRsAUwc6vOhmMnILeTL4X3KWCcd4_sJLAR35_E5GYNdBQOSE_9CKox7uHTng6VarGMBFz5gnykvlnRK0RcwvkZ8onSxzVRDCVCq8GyHr0bb6Nc40Wx2_CG__N8d7MGX8REG7A6m=w1440-h701'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/desktop/6"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/wilton-terraces-ii/gallery/mobile/6"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Located in Mohammed Bin Rashid Al Maktoum City, Wilton Terraces II is a contemporary residential development designed by the award-winning architectural firm Perkins+Wills. Comprising of two 12-storeyed towers, the development contains 283 bespoke one and two bedroom residences.Built with a focus on community living the towers of Wilton Terraces II are interconnected by a single podium at the base, creating a magnificent exterior that is unmistakably unique. Moreover, every house overlooks lush green gardens and is interspersed with public squares and soothing water bodies. To put it simply, this is where modern architecture and nature come together, to create an exceptional living experience.Crafted to exceed the most demanding expectations, Wilton Terraces II is an embodiment of uncompromising excellence. Each apartment is completely thought through and comes fitted with separate laundry cabinets, built-in wardrobes and cabinets for the bathrooms. What’s more, the storage units come with flexible shelves to offer maximum usage capacity. Furthermore, the kitchen is lined with custom cabinetry and polished countertops, furnished with fully integrated state-of-the-art refrigerator and signature sinks & fixtures. In other words, every house is designed to create the perfect setting for modern living.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  144,
                'type'                      =>  '1 & 2 Bedroom Apartments',
                'amenities'                 =>  [ 'Common courtyard', 'Common garden', 'Concierge service', 'Doorman', 'Garage', 'Gymnasium', 'High-speed internet', 'Kids club', 'Meeting room', 'Overflow pool', 'Reception service', 'Sauna', 'Security gate', 'Smart home technology', 'Steam room', 'Video security' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  996828,
                'emi'                       =>  996828,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Down Payment   | 10%   | On Booking",
                    "1st Instalment | 5%    | 8 Dec 2017",
                    "2nd Instalment | 5%    | 6 Feb 2018",
                    "3rd Instalment | 5%    | On 20% Construction Completion",
                    "4th Instalment | 5%    | On 30% Construction Completion",
                    "5th Instalment | 5%    | On 40% Construction Completion",
                    "6th Instalment | 5%    | On 50% Construction Completion",
                    "7th Instalment | 5%    | On 60% Construction Completion",
                    "8th Instalment | 5%    | On 70% Construction Completion",
                    "9th Instalment | 50%   | On Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  ''
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Q4 2019',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ],
        [
            '_id'						=>	'zj-belgravia-III-77383',
            'ref_no'                    =>  'belgravia-III-77383',
            'name'                      =>	'Belgravia III',
            'web_link'                  =>  '',
            'area' 					    =>	'Jumeirah Village Circle',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.059678,
                'lng'                       =>  55.199932
            ],
            'image'                     =>  [
                'banner'                    =>  'https://lh3.googleusercontent.com/Ps68iWh-VlCNWFGKg7xqGC8aIgx92hV4g5PUfgwnGSMvLKFBoo5SNuuWymHeQDxCkaW7m5i_ES_EGjWTh7GWHSM24EEiyYkGgdyL9YOIPfWXV3uF_0n7cq9UEsxLDy3OyD5C6Ib7dfbwuVmaxOVzDIdKNTk8CGuHKY-cUfOKHN_1TduJpTGNI3l1LzBlGOjtxwKGqWDRycw2mhq4Qvozk20GBzAV_KUOHrhoulAThdFtGKlc5H3lEhORvBhLs43RN2EAMgQN7rlxumNeNkYIpq9fQ3yzPEnQQsB4MxMRxcECQT6KJeB4nvp2oIwpcPsEbGwSg6ZSc45G9Bephzq7qbRHoSMkwEMy-yAd2nIA-UHj0vUh__XPxwAMldD5ZSfrcKuKHIdZSYEHG23EycYHibmOwJc725jVEYa0tHji4aorb8fJdsLTl-d4ON6odKX29ZFvOcD-9KmYS3A6rgKOqrEeLZl0DkEjpdsp662wS5NjRvaxfwIBJvtieWdxO3X_jPiw3jchuKGbh9JIxqa2nxw3dSlb7hivZA_G0yUfCl1rj3Ryt5wtE5X93de1MywR=w1440-h701',
                'floor_plans'               =>  [
                    [
                        'name'      =>      'Studio',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=17SSIg2BDe3ZquCnQlBC-WqTP9USeFx4X'
                            ],
                            [
                                'type'      =>      'Type 1-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1qW1PWgNmcvmYyNHpm8ooPpBDE_7zbbCQ'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1mubejSMLMuxHNK_SmVi8cAhThBo30KEB'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1N7ZQLBoghSE5_7Y4bkSBj0aqi6CnRDN9'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1BFFaOwE2JF17vhn_lMuFMfgMKmYuKKie'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1vQSOY0N5t1XRF7QVt-8ll04xVNvkJ-gR'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '1 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1SRnGuvSiE_Sth9sXgTnqnADqDT5JbcKJ'
                            ],
                            [
                                'type'      =>      'Type 1-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1MGPWE1cuxX1pvuU_z_WR1Pgv6kTKpt_T'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1qv5oou_GDxCZXFiJ7yhw1aNIuGHJ9JdB'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1dJljd1bgoENg5fqa8bYNsd5QZ0A78300'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1YAiyiA65SB_3Vnda5OlLAT0NZJPXWgBg'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ljvi_VMc1fYhU8DR8lEQbZFj3Eqg_1zq'
                            ],
                            [
                                'type'      =>      'Type 6',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ZhSVwhwLC5lFsgQ1gUp_bzyoYGP3cv3y'
                            ],
                            [
                                'type'      =>      'Type 7',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=18QDi9m_nnM52m_brGLdWA8dglf7TxG5j'
                            ],
                            [
                                'type'      =>      'Type 7-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1RVHnyWmtS_QLL0YeENECXSSOiB8oo_Ha'
                            ],
                            [
                                'type'      =>      'Type 8',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1rl9yT09uss_MEABkIsdOSLE55FJ59fsj'
                            ],
                            [
                                'type'      =>      'Type 9',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1yH3Q5H2GTeU5dG4Ao9mwpZkzYC0gDvyy'
                            ],
                            [
                                'type'      =>      'Type 10',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=18N74zC_xMt0S-g9WrrknuB16rwd7bwtU'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      '2 BHK',
                        'plans'     =>      [
                            [
                                'type'      =>      'Type 1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1HWVnbj-ijpRO0YE9_wDdF7mkgSN7DkaD'
                            ],
                            [
                                'type'      =>      'Type 1-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1BmrNKz-CDwpILwbiThvGnXs6EZAKchto'
                            ],
                            [
                                'type'      =>      'Type 1-2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1RoiPpKum6z57oKqF3fp69RcOT-oLR2Rk'
                            ],
                            [
                                'type'      =>      'Type 1-4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1S38JyWKw7-snhSm8MMh2ZMNTPXV__r7I'
                            ],
                            [
                                'type'      =>      'Type 2',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1U4FaUMVUXARTj_Q9s0rosQAGCpZ3iRKz'
                            ],
                            [
                                'type'      =>      'Type 2-1',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1U4FaUMVUXARTj_Q9s0rosQAGCpZ3iRKz'
                            ],
                            [
                                'type'      =>      'Type 3',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1vbj7OH0MH9R6OyPEfy82VciZRVY4GdjR'
                            ],
                            [
                                'type'      =>      'Type 4',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=13zQ40qddSJtBn4wj2SH9Ac4PBx8X-aKg'
                            ],
                            [
                                'type'      =>      'Type 5',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1VxwfYKTl1mqhx4r4Vfkr0BHp4alZu2YX'
                            ],
                            [
                                'type'      =>      'Type 8',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1X7tS1N-wIj1K2e9-Igd1pq-sAT2FSgfs'
                            ],
                            [
                                'type'      =>      'Type 9',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1z9MWSFohyuTDFYCseLpRB1GyyoA_Ew2S'
                            ],
                            [
                                'type'      =>      'Type 10',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1dBKiw0R0oFJ-1_roo6CrruXMrziPN3J5'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://lh3.googleusercontent.com/BEDD4YPyhhKnf7sSIQ3VYg0YqQn3mqWxJFTTnhQ-rNdbl1feFmOKzJVRO5lu07g4iPxERTNOOPbWM2ml5mWL8fVYFbYbokYrSkRtb5onYpg7cKvz-zxbS1YIQxifvXuyyDPvn_tW7xjbNp_KfsKqthmFHWHJ47ai1wDZsC-2vbOmhHqh6Iw8tQqBGM-cPZ-wnITh7FWTi_Bc9yCrFEwQr_YN5MReedycyz5Z-iIDx_UkyUxJBtBMb3Et9P3v4JwBPom_ur7Z_weSvCHkN1yKzM4xHh0pPdhkmJPvxzqMkctxlfL_kcxN0-pmeTVOWDjyHMafXnW5FduAM92P38tWFm09i9NP_2YxEPmBDH3Ew78kDr4ETqwRakBqELHkvScmc24WDU4tnttoGt02qJSzIunYLtIDTbadwY7B-uFGGzp9iIAYWlh5DiAbQrBqvk0tA3YVW0h3UaIxg9iNjpk-Oin79xp4vmgdIgyS38COKywmVeqA41T4Jp5lxtwaP40VoefnhooAz9MrF003xbxwwhFZF4dJMHgdsvkmAOaByvlNaY05R2WU2n2XoKjAVRS9=w1440-h701',
                    'https://lh3.googleusercontent.com/NDXETYGjVnQ4565GmWK19aDeFrzL3xhT1yltqAf91v3sqm37IBy-i6Q8oaQlTdqnSopilkHtSGe3xV-rAanyfG5NzqPZolavM8yC8YCpaZUylrazfvhYtMohjPZmZVuYQD7PmnxxHsYZj26iFxE1mPpd_HAR0aOBIFLNnTRlBu2BF8reIARPG_XcURx6L3ecOp-HvuHH14Wh22zUax7eFbR1W_y-uIFnDbc2a1IFAhQIJM_1wCDPaggSudsLL8leb1qQm3an-03tdHLyiUjgu2HfUsGGInT-OTh6jxWqgGXKx_Ni5FgZLOZRjgi7I8ir3qrhJSDD3NsA9yhC0wLVPXKi8YFhkJinmSjqFkH33XyV9wTnwPH8Y8GJOlT2d6T4KAl10duDoVJ_12Vea53jds1RdniJQAw7p-dc950whz3rAvAI4ZfNN3jtm-jps_eAOs8pUHPlIsCLKM3BxenMKKTgTMBh00RFP-LZbCR4P-bmFgW4l1Pb8gSJpGiFh90lMed0tmOs128Cz6W51ObdCUk_YOVzdBNYPehaecLDDs446NesgHjfb0ffnTMEBjpB=w1440-h701',
                    'https://lh3.googleusercontent.com/YnAWjzbCA6WbvTCrV77F1wvDrRgll6FsNMR_ssoPAMoa2GSUzJzCFYKpEj3JKvue8FPz9Fdjq61GVx408lm3SSju0RFfQjIeELjUpzu0K-k8gFAZw8joepuE6rI7DBXRTd00mJoV4tQZlgr1VCvY_ORzPFV1TNHZEo-6xPEm84cXEfTXhQHi9P3mehRj07C5TJNAgV5cFdg6EXUfrHINum47Dp2HzHd311UWHvFhoCFQYYL-P1hIDZdpdLSRfoibuej0aK7PnWvI-bEQ8lOB3wErgaXoyWgScHcnWC2bsk3dgqKv9kzgXyarEoDpfRec6599szaRWa9nOLUUoh387qlbN0ljJt8BczEx4QOlas4kjK-HQxyvEaXKcTjEpczUnnocpLFtCcC9e_phsYAf7lVhWoXUg8eWiEjyf27kVTk9IqjthhqGAmqFsr2rP5eXQ_AkGbutwipYLAegDZsvotJH6tVvpDcHG--Md-HjOKqHFjFQpTjJdxNXSclvHfHTWjqpLh2EuOrY7sh1wteL4Zt998dhzGEUWhN3G1Ki3dU4e0cR-p5-CJJaM8T_nJd-=w1440-h701',
                    'https://lh3.googleusercontent.com/2gHhKx-12tPYa6cQIVoWcYNzCm7NHRLl5WK9FTBS-KX_WdHHNBer-11lS3yv8bJv9PV9XBKoYNmVdlsuG2T39OTCSjfwltlSaBbEVd6SGMgIuFvBCNO0f2URaa7nCcBnZUyQFtEVoPSzMYyPO6qsfXwMK8Tdr_d1FoJpLLap1yuoGItu8rXgFeEPV741r5OD_x5nc2PvGCle3-bxUhSyV4fNK6a3kjCmlZ4QiDYs1vwlWNKDe4pMj3J8DTF6ErZs1FNpuTHysFx_X1vAmQ7U3veZR_e5lKfE0xqLmDoTGFZHE7duaKQihHhMdj35Am10_8NWbPB72QtZZsJCrigtuwTE7sy38PB8hF4OV-EJzzyYSiX42lGpowG9kbKHDNaqD72ri3OOpOznCq460rh3Dt8SCPOvyhhOdWdUtlb8aGDsh9pt5vm8OCoYQut06s9NYCNjN4-ppGr45hP5MLjU2bHFKSoDXxmAe5XixIMTGgZ3ZXxNAUPJLhEDYhGLwSBJ1IN8br5TXzhlQTa8s4u1b-bKfuNs6vU61eZcMXPadqEePFZs52hR2EU5qvxiSrYv=w1440-h701',
                    'https://lh3.googleusercontent.com/D56jZF3QEDpcKcW04hG9RetYvvFxjqwbHWvteLxNttOyJkkkhFNXuo1_FnyT8L9Ffcmax67hUK8RjkmIq11uGMqmFJAmj837kTKYLTmB27cSE-hBQpTX1tRbt7alPS9taKZzagb8PVeVMrSVcJmp7Jt_qaQz_Rk_7VCeOzVCf6tufy6v_BhJjcIqxzX-8k0eKghNQZ6isxJT0rh4a2V1QUSQlOvP5Gr7SqaMigENN_PhbDYnn0KWsAnoEj2YQp0JUjd4As26i1LJ9xmmgKwURYk_gs1Jb0doo2UUnoB8FgL2CCu9dZzTMDUwxa_NJq1qBbBxV7G2EcIjIH_qIhe9OiRFz_HEO3ywRfzMIXSTiAG2wQhfhRW6jn7mfvcX8IyYTY6Pb43MdcawHFa7y8bvD2lMuEYkTj2dN2LuEmKOTggtLC6yjRhgiGSaltXnHomm0V5dtGW9AjXECC4OhFJRKVCeYPXeJOfr-vC614Jrg7Hj4A6wLeBXj92DH733hG3EhGIHGcemP9vFZjITkYQ3LFj7jTVwPH9G-CWdvpNfKexLHsta5AEKyqqmdBKo3nnp=w1440-h701',
                    'https://lh3.googleusercontent.com/rAG36oYJZ0xZ8VK8wDMNqdYjxdsZB2yeVoFJwIDevfOYLVDBNGwmGsWtqKzWzMzAv3Ix1IGWtq6kTuhby7iQPYOiPqN7Mq13qUNnj6yHUfGNrWydcsaprFch_Alq3SbfqVR8zWdcGAZmVJv6ooTrEx13H9w0kNCwCCL7xVCAf8kZSYlQZYLCCovvHpC85_SQVlalZrg_oBYhQkBgeEEklQ9yKhRkFDjxE4Y_IOO1GiXa3w0dPnhCZNr9Hn4Ct_K15u_Gs2NDVP7Nnh8xe2Qw7Fyh2lJaV4N7WmpwLDC_xno_9LQq49v5ZXcpJLtVUK8Ycpl9vT8TvxlY_zRQvHRWUWoymZOOCul5KRXOcRzIH_L8u0hRYToKZXx8QRWn9Jc1F2PPf_cQBYpL-bBY-0x_L_0Nj8WTmMMSOAqR9lxzQhRThX5-q9f4s88yUjUE4NKLWWpyuW9sX6DyDBTPRpPCbQ36-MjzFJmihL-asbEEdWsRu6KoR-cI5vmpo-3U5EyxTU1X-qL3Zv96YMTD5GjRD0ohEAuSM-z9_GaJQo1lMVvzIUY1EI2QrCSuhLA0Q0su=w1440-h701',
                    'https://lh3.googleusercontent.com/VbpiY4wYZShzOGJGr8VJpd54uAdzXzW7AMYRY-tcdf21IL-r1vo817XGipIW-dAlsXpXdvpo3Zv_liF5iMbEqsI-TIN84IcrJjRctkr5vz5tJcnsWZk57-dQ_gIDpPn7jSbHoSdANzJzNiSeFaw3YdSNGDnSu9Lwfx_5C2J2UxVTThHc1C9XhSt4ae31zFsmj1q91z3nqJiSZ316XSrDrSmt47T8T7JTvR2wo2ivJKsjhYzasNk0UvvcreMQC1SOmCy1iZ1sun6eZrcId8bXAHdYwft0aUrtvG0Ke5-xv9Wh9Fj65sTzMIJm2CXhMLjglxtNEIHktb5mlTenxDFmz2ZWe92_La0KyzqbDpUBc3MwoujT9sDtwNvSdTgzNZKnR_XPPH1ZVPqCXlRny0jmqWVDOb_5EVFwSq7QEj1v3V-vaRtDRrIEJzQxbbgvIyQkwv1F4lHKSGTT65n3a0JEPGUM9ZAxMmcSImEMDiLB9UkS37tQ-jwxOn3xOkha5-iFObuclbamMEDMJz_HaFXvmaq226geZQokDDFM6cHhCnKZgsAD8aDjTi87JmvUuZjs=w1440-h701',
                    'https://lh3.googleusercontent.com/Ps68iWh-VlCNWFGKg7xqGC8aIgx92hV4g5PUfgwnGSMvLKFBoo5SNuuWymHeQDxCkaW7m5i_ES_EGjWTh7GWHSM24EEiyYkGgdyL9YOIPfWXV3uF_0n7cq9UEsxLDy3OyD5C6Ib7dfbwuVmaxOVzDIdKNTk8CGuHKY-cUfOKHN_1TduJpTGNI3l1LzBlGOjtxwKGqWDRycw2mhq4Qvozk20GBzAV_KUOHrhoulAThdFtGKlc5H3lEhORvBhLs43RN2EAMgQN7rlxumNeNkYIpq9fQ3yzPEnQQsB4MxMRxcECQT6KJeB4nvp2oIwpcPsEbGwSg6ZSc45G9Bephzq7qbRHoSMkwEMy-yAd2nIA-UHj0vUh__XPxwAMldD5ZSfrcKuKHIdZSYEHG23EycYHibmOwJc725jVEYa0tHji4aorb8fJdsLTl-d4ON6odKX29ZFvOcD-9KmYS3A6rgKOqrEeLZl0DkEjpdsp662wS5NjRvaxfwIBJvtieWdxO3X_jPiw3jchuKGbh9JIxqa2nxw3dSlb7hivZA_G0yUfCl1rj3Ryt5wtE5X93de1MywR=w1440-h701',
                    'https://lh3.googleusercontent.com/f8MemHb4sJ_KaDX0J_Z2U-r0qchJuyfqtLJZ4K8ylHNNAlng15xxsa6SYctwqa9dF9N_G1iDuHsmX5ARSQaBynX723ljXjuPY9nRhLW-uRsis8KwWfRDPOG6nKqixXX3FODOVcKXk0JqVvSndSa62nHgRYRrEoHc3iZg598GhBEuBqXtzg4auttI3TNbQsMNEPI0ge8Qzs8J4-E-V6CM5d8LBLx0N0Zuz4K-zRl9MwPrF7j3i9aZyk0kSDm6rH68v7_RZYqSuQng23c3NtdP0Sbo8AXUTPGK4lpZ_-PE8Ww2pWfDf7mZYShdZ51SbizcWkFJg-97JnLDIVAE3muVt6ZP93LTqUxXIGY_09fWVL7plsHOSENOVM4ZxXcFrSs_php3_IGpRZMW2CTYRtsgyHtA6n3qd3ApTPPuHumbui0RFfOQfCc23dBgAXZ5zOSmyZo9dLs5mFxQKRb7z6Bwva_1r-M_ju4u07-3VXdbSsV2hmg8-7567X_mgDqPfjlya1n-t-JO-ZdNSWgDGQUUFwcl5CVp_ScVt_3fUfdDNLbv2H97xJxFuZRb9xMhiUYV=w1440-h701'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/desktop/8"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/6",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/7",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/belgravia-iii/gallery/mobile/8"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'Belgravia 3 is an ongoing success story of the distinct Belgravia identity which has become synonymous with design-led properties in the city’s Jumeriah Village Circle district. With more than 500 happy families and over 50 nationalities living in a community of communities, modern one, two, and three bedroom residences, resort-style pool courtyards and superb ﬁtness centres feature in the ﬁrst two Belgravia developments with the same skilled craftsmanship and design philosophy promised for the third. Uniquely designed interiors, 3D art pieces by local artists and heightened ceilings in focal rooms also add to the Belgravia concept’s unique proposition.'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments, Townhouses',
                'count'                     =>  188,
                'type'                      =>  'Studios,1, 2 & Bedroom Apartments + 2 Bedroom Townhouses',
                'amenities'                 =>  [ 'Common courtyard', 'Common garden', 'Doorman', 'Gymnasium', 'High-speed internet', 'Reception service', 'Standard pool' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  507828,
                'emi'                       =>  507828,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Down Payment   | 10%   | On Booking",
                    "1st Instalment | 5%    | 60 Days after the reservation date",
                    "2nd Instalment | 5%    | 120 Days after the reservation date",
                    "3rd Instalment | 5%    | On 20% Construction Completion",
                    "4th Instalment | 5%    | On 30% Construction Completion",
                    "5th Instalment | 5%    | On 40% Construction Completion",
                    "6th Instalment | 5%    | On 50% Construction Completion",
                    "7th Instalment | 5%    | On 60% Construction Completion",
                    "8th Instalment | 5%    | On 70% Construction Completion",
                    "9th Instalment | 50%   | On Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  ''
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Q4 2019',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ],
        [
            '_id'						=>	'zj-dt1-63633',
            'ref_no'                    =>  'dt1-63633',
            'name'                      =>	'DT1',
            'web_link'                  =>  '',
            'area' 					    =>	'Downtown Dubai',
            'city'						=>	'Dubai',
            'coordinates'               =>  [
                'lat'                       =>  25.191594,
                'lng'                       =>  55.269418
            ],
            'image'                     =>  [
                'banner'                    =>  'https://lh3.googleusercontent.com/kZS2iLE1Vlm2G_H_6aXP4c5kbFVQYdoIcUL8JTERxMUjjxSydpTFQcUEzp2Hu3xrycPJWLQ8wHM6WHda-LFLiDJRILOAVUULBl5mO5YwWoVjqAolOz35fFP6vibyXOQVByPEmlVauE4ApZftQZVK2NSI8P10DHPRAw0QqH170ExyAjK8BudsT5Cx7E8yEsJMJBZe4UxEs1pUw4RfYTmcyOgwwve-BH0jKleDefWthQEyMVpHF-8kPtPtcp4czgbKP8WlodjL7gs483mQua7aQRZ1Bn3X0iI2HgYELtD-1oX84lsTAEMKTdxrGoaCOlDSA-xEGV-4ZIRzAJhCNC2ceN_S7-DIj9fHFwVCVt1n9KatmyR_jYrMbGK8eVbGl7Bmh6GW01ORvytTnCZo-yIf6gMdGJ8nIXL5ybavBQE2jYBixMnXHxocn5W078wq4msTzKeFs3Dw56SFIvyILYoiIMewRNfFPc-09CaCxTzVgBJbqFNddi83YdX50siyWI0f3GqkJ59r8hcRYDVp1Zepszi0Ruq5W2aivorUN55xIqAxLUd3RgjMg1-z9wp5X1zM=w1440-h701',
                'floor_plans'               =>  [
                    [
                        'name'      =>      'Floor 3',
                        'plans'     =>      [
                            [
                                'type'      =>      '301',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1OfDY0qmNQ4RXaiNtBOXUikNg4uD2kpq-'
                            ],
                            [
                                'type'      =>      '302',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1PL3zZg-EiK5yO39beOsKryq28BOKUCXb'
                            ],
                            [
                                'type'      =>      '303',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1ggLlgRfMvITdPvx5ik0tlvNkMiwEuOQ5'
                            ],
                            [
                                'type'      =>      '304',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=13tO2XR4BdUOtpJy5Ot9bgXQPTO_FksIb'
                            ],
                            [
                                'type'      =>      '305',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=13tO2XR4BdUOtpJy5Ot9bgXQPTO_FksIb'
                            ],
                            [
                                'type'      =>      '306',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1dPPGj4pH4M3jEYI90glC2MHtyuEk60GD'
                            ],
                            [
                                'type'      =>      '307',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1i6DWbFrROvucwxyAPUHD_12dgy4vNJpb'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      'Floor 4-15',
                        'plans'     =>      [
                            [
                                'type'      =>      '401',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=130z27mJpYhzmpGC7FbFTIyGzngvkb9XV'
                            ],
                            [
                                'type'      =>      '402',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1HmrOzUb2I6eLfVxXj4r5EpPUyNbqv30H'
                            ],
                            [
                                'type'      =>      '403',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1uBoywY4RKrQhgDT8U6jRvSazp2KVKFZK'
                            ],
                            [
                                'type'      =>      '404',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1dZUwjIEP9tUGsr_b_aCeDGjl1jbGEH6j'
                            ],
                            [
                                'type'      =>      '405',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1-ZITJjYZOMHj9TXG5JR6gMUXdce--edq'
                            ],
                            [
                                'type'      =>      '406',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1nsEQPt18DGAK7BcmL3fPoZEqBV7MiW_o'
                            ],
                            [
                                'type'      =>      '407',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1OpQbNR9t1lsax_JHatcQM2rD6hJX_5Ye'
                            ],
                            [
                                'type'      =>      '408',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1tux59n3PpsSHBHnfBogipkNDcCoaAM0P'
                            ],
                            [
                                'type'      =>      '409',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1eMp9JbApjGcM-snsaxJrHO-HByyxYvxv'
                            ],
                            [
                                'type'      =>      '410',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1Onyk3u3gpK3wUe8EUfA6pHXPwXAylUhV'
                            ]
                        ]
                    ],
                    [
                        'name'      =>      'Floor 16',
                        'plans'     =>      [
                            [
                                'type'      =>      '1601',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=130z27mJpYhzmpGC7FbFTIyGzngvkb9XV'
                            ],
                            [
                                'type'      =>      '1602',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1HmrOzUb2I6eLfVxXj4r5EpPUyNbqv30H'
                            ],
                            [
                                'type'      =>      '1603',
                                'img'       =>      'https://drive.google.com/uc?export=view&id=1uBoywY4RKrQhgDT8U6jRvSazp2KVKFZK'
                            ]
                        ]
                    ]
                ],
                'gallery'                   =>  [
                    'https://lh3.googleusercontent.com/Am0lxBf6j-qn9EH8RhebyiBleSBoaCwSrpSDwPdDVWVVxuCKzNBu2uAEBvRkMs8nTNnDbyTRdbObhn8Ju_Zrvzqgby6lbUWqfAN-8Q4mz27eJmsNUM3QhwH7GHyvfHCllMKLKvqjthil1ds4bQSHHyMenRvjQkrGDE2P2eY0VsXv02gG1f3FzlTu5hA-4Oe672OXyu9N6UJdxQcxthbBOQMRssVPmWSOwH-dSzUVpwVLKdD3Sv-59Yrw0qlndJsOrPjOKPtxMZF5DWAGG_3TW2WAV80Ef5-5hnSrBKgJeHA1Zi3ayH9Vu6Sx10pLyEO8ERHBJ640r94LfvZ0pQfI_996H1aCsaW1VncH_hAWz3ayedxzE8qDborgrwiucwaf_UiBOzGNLMN3Zg0G16uxRDUQELlzVgsz7eED-zDuEVGhp1Z02jsLw8O28v-LRWrKLV4PCFvGh4rXUT4TnuWAz-8iWsgj1qU9optUYmnecUpJCXWozls1LIDxLQVLyS9-qbN3J94_yuAwfiIsQMEqzwIMC1F1jYHHR1b80hUk8n7wRe7Qxmd46_ymUhczbcM9=w1440-h701',
                    'https://lh3.googleusercontent.com/lbq2ldQOgP5Orv8_Ql8opizGOl37f2pRJtSf5UbsLmTWB7OJjmEudX3tChugJIAytlggYgkYgNXnBQpCJjLmjq3bgSTQxQUZdBSFB8G2SEty8Had-Vgsh3ZLk7DQJpbQZFxJlO-c-qbyAU-0i4hOIVBVVcuwyR4Kuw9av3NMGPnsqSdIVBx-PgNDAORA7WmaXMw5_Frcv6pFjg7jEEZm9dhkmvlvvN2ozqRfpScLoviLTde8rc1PSctBm0m-crBI-gm2oZSuG1nY0dI_t06iqVeIdbBemtGRKaByVzZ0ixfhD5VMZqFnwrDY2uc-8ZGlnrcn2m64bxYkYbm2pQHrHYO6VgzyO-fCgPVufk2uAQQ3r6SvflFrYCaRaM7TFs3S0zVJin14JXzTDwOIIVegnqO3_XHjmGxW2aDVvhbMx5QJLfE-FnAg56NMd5gyBuHoEazuc2S8Zy9HKjv2Y2SZxkV2MpaFgbM99Bscj7zrMq6D6dogUygyGty132_FRU3hxXVNRyv0NRp1HvaCTS5dU_9qFLste7OVNQRll4TwBmMDajqNnVKmdMrIbf5WqMTM=w1440-h701',
                    'https://lh3.googleusercontent.com/eYtdwt2VdwGaeObOPZFcP0LOGo5Rqoa-aeKKr9ztnde0Qb58V36XiZ305SsxiG9v_oZN9ADOmeZBVcAl6Vxe9VkIyNt_OU-3ij8kNag_kz1Qr2VczecaWw10q2YpQrmzFxalV38szNaK_h4LNeXnabvXSFQZWp0_ZShbh5om8EJojqrgzV1OAD3yYroZU4BsuhZClfullD0IjoeiZTziHQt7yYdkM00nZSTTDScihafli08Jsf-DnRqK0MPNAe-PasCxHBU6le8a6Pn0U__hzY8gvhYnfwt0m5o4D4bCDFCPA4koKfm0oEsSyNhZjJxu7M45p9_UNOSABX40exypTb9_y4rLr6HmQqaeJDTpig6zX5tfQDRXfzIFqRAmLquj_nxFaIXvjAQE-kS04J8YlN-pX20EwThbOQjbIH4S4jEOOzETv0qxzDZQEsdxu9S2MWaZqRzJ46ydTIeTNYISTwSwJ2NNkMCKg2gMtmqJ94EeuQzdiMXPIgIaS3nTVvcnkazzIKhUSQ9MZJXwWd0q-jHtPVB0d-4kYJakwnyuod0qfeJXxPcXGSanfD33-SLV=w1440-h701',
                    'https://lh3.googleusercontent.com/_U6Jq6YusB80quHnLFhjFgGIP3Kcg-9WKjCOuYuB9fF8i2in58HG0TWMPAb9lS57IogGDAGugdiB7ORf1PCHaGW14JJVEMd_3gd0dGhgTHrhJTrRyH99R5tNFceOSktwJNqMkrLoZzl0kywdXQrJPWUkXs6hkgkdm87RbDH0ReB4X_JXxvS2Q3Rbj_LP5AgDkHDwL_9xD8YVES_jEcbHH8crPqmXfW1d_aSSFNw1owmzf7Gvx6jqeDqnlgHIaS2s5t3Ur8z_F6fHyUnvHDR3TneobEpUbXrDxl9GvvvWWusI3Ycah2fhsbRsjVusHPoFgD5xulwztoZ2EAzp0wnBcXe5iEb-Hu9BjGPL9JbSftGD3WkTwIKH5Li9S_HJUFuDsKDoXyTczpQBYEs9G56F-Hk9UA-GeYvfWC0Q_XAcsgN-QpuVVgCjuF-DDMgw-4mVvqzVPac_qllCQBrh2FVBi_UF1kG3pNosAfy3Pm7MmazIhCiHe-8vh11IzVyz51Tko7WBhBlh_l5DncrVSbiPhS_FQmb44clgsBT4YI9yLEa_5eEjAGjm-q-NANxFTMIx=w1440-h701',
                    'https://lh3.googleusercontent.com/aCWsWBh76_cjP2KoBNn9ET51C2SMJG24FCNhAZToc6fZW8SiSesbpxj97oZ1SL5-OAennEgdro6rHL_AXiWjopMKXRmsG5MoBR8lbNrUYYcfYLJCR84WlyAXQPzq3Q2meiG1pKMwe8o69HiGw88-ezUHGOEcaJwaR_5nOhBi-AgpUMvZdgPl0SBTJjiUlOYyCCstaER898rASuoYRgLkMUmXOqBgHBftCRWgPWve_uXvjAsrtI0UBqguBbJ9BOHfgBiZ-gW-5mIjkBNSeqiz2JKxq5oS4T9lT4Cf_CfZqFfAQYMwpvRanNmHKKuDgiiCW_Es_xhyrz-MLipgOFSXQxim_bUiXa81ZfUlHSByE4woW-4syW7CXc3qoZiWUbU53qV8KRhP0mSxd2InHlH11a20ZWvuSDETGp_894dEYtiN6evg8dcMlMUGi_BGvDceA3Kklb3-5oAXbqxbBDyrCYyD2p_F8kk5pkUqY-NsVjkHikU60oOzeION4LnSJN0lj9y9cIlYA9BpmtF9QLFUqTIcE7oTsYgrNQvZhDKGnGWudOc2Tk-5ZpmJQZVF6m5f=w1440-h701',
                    'https://lh3.googleusercontent.com/bub_loAY_qmLur5RSwJdnfOcaBdSWQgED6SnAWkrMF8h1lN3AaVn1_auiB1r9I15IpVGXL9GXARrqdu_LWNE5Rh0YEfUs3TXiKBZ9tDzygQcW8sRxdtbJzV65YNnxxwWYThwY5xOPPXOI5JX9PcvfdNdukJe7gprSEXRz_-nY2aC_YOV5XhhDqJJEZ8fBIa8p84G9Xk5FxvskLGOTbqyYBbBfE2ED_muepQOJusFJujPAE_KNRk2oJics-U-RLw75IQEFPvOa3Pz9uHKBiLSKhfDKewPdxsxGv0z6UlsdZF--2rrZNBV3Pyb9wc9karXUOKxWpE7nVMYNKcrkUE-zI86Za9ZbTHWICkvBDGrkl28PX173TB4a3wIgDOkCLMwvuV9kUou6s0rRG3fp06ShHKd83GESZ-o3AtMIgZVhFDflcdCzBnntrgCcLelUmgnjJHhgU8EclSI7IHoLn-8OBO-S9YNOkNQVf3jTE-kw4Q5CIy7Yj4xiWG_nkJH90PuEKiyoD2X9MZ9STzHRPMSb-9vS89yXA4qb7q96AUeu2ODyhCS2uUdzL_A78FmZ3Zg=w1440-h701',
                    'https://lh3.googleusercontent.com/kZS2iLE1Vlm2G_H_6aXP4c5kbFVQYdoIcUL8JTERxMUjjxSydpTFQcUEzp2Hu3xrycPJWLQ8wHM6WHda-LFLiDJRILOAVUULBl5mO5YwWoVjqAolOz35fFP6vibyXOQVByPEmlVauE4ApZftQZVK2NSI8P10DHPRAw0QqH170ExyAjK8BudsT5Cx7E8yEsJMJBZe4UxEs1pUw4RfYTmcyOgwwve-BH0jKleDefWthQEyMVpHF-8kPtPtcp4czgbKP8WlodjL7gs483mQua7aQRZ1Bn3X0iI2HgYELtD-1oX84lsTAEMKTdxrGoaCOlDSA-xEGV-4ZIRzAJhCNC2ceN_S7-DIj9fHFwVCVt1n9KatmyR_jYrMbGK8eVbGl7Bmh6GW01ORvytTnCZo-yIf6gMdGJ8nIXL5ybavBQE2jYBixMnXHxocn5W078wq4msTzKeFs3Dw56SFIvyILYoiIMewRNfFPc-09CaCxTzVgBJbqFNddi83YdX50siyWI0f3GqkJ59r8hcRYDVp1Zepszi0Ruq5W2aivorUN55xIqAxLUd3RgjMg1-z9wp5X1zM=w1440-h701',
                    'https://lh3.googleusercontent.com/ptNsnPwpl5QRebB9SOLKriSRAUqZLkIVXqe7SrIZc8kFj6KoJEi4kuVebkxvTEu5PXPs-fXZTDI8CXIVUd-ApNg7enKQ6CjapFQVrdWSFigFLQuOpByzdvR1HedKml4nsgCgxHwEpg9SLVelyDSd5Tpiu5gUwe_iTpESDmxUzpTGZZCGZrocQqF_dVVCzlQjyVwW-W5e0hcJyH9B_woE5404jPlbIW743sKABA2-NupgHPflL3CkMAHAcjvlrbnpywSoQ4SNQ4H52crS600oHoitxtCF67ZvKZSfbu39AaXNw0Iu5jb7MO1O5U4EjFTvsa2SoQ8JgZY5tqlXEQ4t68XR8aP8Y11DmAYm0AwzoE6XzgAKDbtKpHN8Otn9asNENuuZjHm0DtlUAZzsf2-tOpXxuVU1_k4H_PhI9eu_y6vDWGq-VkEnpEDVk4ZVEf-5jXs3LxwqX4Aba240aR7zkRg__6mrWdi77pSRusrSNHtGFMrykf1fZf_EjGdRZL5TEkQ_7yXq47CMeD_AQlG2YTYShV95UfmTa9v49XnLdWiBtq2Ityrh3Q3waTa-x2-R=w1440-h701'
                ],
                "portal" => [
                    "banner" => "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/banner",
                    "desktop" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/desktop/6"
                    ],
                    "mobile" => [
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/0",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/1",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/2",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/3",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/4",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/5",
                        "https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/developers/ellington/projects/dt1/gallery/mobile/6"
                    ]
                ]
            ],
            'writeup'                   =>  [
                'description'               =>  'BBQ area, Common courtyard, Common garden, Common roof deck, Common terrace, Concierge service, Doorman, Driver\'s quarters, Games room, Garage, Gymnasium, High-speed internet, Jacuzzi, Kids club, Meeting room, Private storage, Residents lounge area, Sauna, Security gate, Smart home technology, Standard pool, Steam room, Video security'
            ],
            'unit'                      =>  [
                'property_type'             =>  'Apartments',
                'count'                     =>  132,
                'type'                      =>  'Studios, 1, 2 & 3 Bedroom Apartments',
                'amenities'                 =>  [ 'BBQ area', 'Common courtyard', 'Common garden', 'Common roof deck', 'Common terrace', 'Concierge service', 'Doorman', 'Driver\'s quarters', 'Games room', 'Garage', 'Gymnasium', 'High-speed internet', 'Jacuzzi', 'Kids club', 'Meeting room', 'Private storage', 'Residents lounge area', 'Sauna', 'Security gate', 'Smart home technology', 'Standard pool', 'Steam room', 'Video security' ]
            ],
            'pricing'                   =>  [
                'starting'                  =>  1189828,
                'emi'                       =>  1189828,
                'per_sqft'                  =>  0,
                'payment_text'              =>  '',
                'payment_plans'             =>  [
                    "Down Payment   | 10% | On Booking",
                    "1st Instalment | 5%  | 60 Days after the reservation date",
                    "2nd Instalment | 5%  | 120 Days after the reservation date",
                    "3rd Instalment | 5%  | On 20% Construction Completion",
                    "4th Instalment | 5%  | On 30% Construction Completion",
                    "5th Instalment | 5%  | On 40% Construction Completion",
                    "6th Instalment | 5%  | On 50% Construction Completion",
                    "7th Instalment | 5%  | On 60% Construction Completion",
                    "8th Instalment | 5%  | On 70% Construction Completion",
                    "9th Instalment | 50% | On Completion"
                ]
            ],
            'video'                     =>  [
                'normal'                    =>  [
                    'link'                      =>  ''
                ],
                'degree'                    =>  [
                    'link'                      =>  ''
                ]
            ],
            'settings'                  =>  [
                'status'                    =>  1,
                'approved'                  =>  true,
                'lead'                      =>  [
                    'email'                     =>  ''
                ]
            ],
            'expected_completion_date'  =>  'Q2/Q3 2019',
            'status'					=>	'Under Construction',
            'timestamp'                 =>  [
                'created'                   =>  null,
                'updated'                   =>  null
            ]
        ]
    ]
]
]
];
