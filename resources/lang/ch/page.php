<?php

return [

    'global'    =>  [
        'hi'                      =>  '您好',
        'seo_title'               =>  "阿联酋首屈一指最棒的房地产网站 | ",
        'seo_description'         =>  "首屈一指的网站：Zoom房产为阿联酋最棒最大的房地产网站，为大众提供各式各样阿联酋住宅和商业物业的出租与销售。如果您正在阿联酋寻找新的住宅、房产投资、度假屋等，您都可以在我们的网站上找到您梦寐以求的房源。",
        'seo_h1'                  =>  '',
        'seo_h2'                  =>  '',
        'seo_h3'                  =>  '',
        'seo_image_alt'           =>  'Zoom房产',
        'to'                      =>  'to',
        'for'                     =>  'for',
        'in'                      =>  '的',
        'video'                   =>  '视频',
        'from'                    =>  'From',
        'sqft'                    =>  '平方英尺',
        'aed'                     =>  '迪拉姆',
        'uae'                     =>  '阿联酋',
        'emirate'                 =>  '酋长国',
        'search'                  =>  '搜索',
        'search_filter'           =>  '搜索／更多搜索选项',
        'advanced_search'         =>  '+ 高级搜索',
        'find'                    =>  '搜索',
        'rent_buy'                =>  '租赁／购买',
        'rent_property'           =>  'Rent Property',
        'buy_property'            =>  'Buy Property',
        'bedroom'                 =>  '{0}0浴室间数|{1} 浴室间数|[2,*] 浴室间数',
        'bedrooms'                 =>  'Bedrooms',
        'bathroom'                =>  '{0}0卧室间数|{1} 卧室间数|[2,*] 卧室间数',
        'price'                   =>  '价格',
        'area'                    =>  '区域',
        'bed'                     =>  '{0}大开间|{1} 卧|[2,*] 卧',
        'bath'                    =>  '{0}0 卫|{1} :bath 卫|[2,*] :bath 卫',
        'default_title'           =>  'An Amazing Property',
        'default_agency_name'     =>  'An Interesting Agency',
        'default_agency_title'    =>  '房产公司',
        'default_agent_name'      =>  'An Amazing Agent',
        'default_agent_title'     =>  'This Agent',
        'default_project_name'    =>  'An Amazing Project',
        'default_service_name'    =>  'Amazing Service',
        'about'                   =>  '关于',
        'properties_for'          =>  'xxx房源',
        'properties'              =>  '房源',
        'featured'                =>  '精选',
        'sponsored'                =>  'Sponsored ad',
        'verified'                =>  'Verified',
        'visit_wesite'            =>  'Visit Website',
        'more'                    =>  '更多...',
        'less'                    =>  '收起...',
        'agency'                  =>  '{0} 0中介 |{1} 中介',
        'agent'                   =>  '{0} 房产经纪人 |{1} 房产经纪人',
        'completion_status'       =>  '完成状态',
        'time'                    =>  '发布时间',
        'international'           =>  '国际地区',
        'studio'                  =>  '大开间',
        'most_popular'            =>  '最受欢迎',
        'popular_searches'        =>  '热门搜索',
        'types'                   =>  'Types',
        'available_unit'          =>  'Available units in',
        'cheque'                  =>  '{1} Cheque |[2,*] Cheques'
    ],
    'featured'              =>  [
        'residential'            =>  [
            'sale'                  =>  [
                'title'                 =>  '精选热销住宅房源'
            ],
            'rent'                  =>  [
                'title'                 =>  '精选住宅房源出租'
            ]
        ],
        'commercial'            =>  [
            'sale'                  =>  [
                'title'                 =>  '精选热销商用物业'
            ],
            'rent'                  =>  [
                'title'                 =>  '精选商用物业出租'
            ]
        ]

    ],
    'pagination' =>[
        'next'  =>  '下一页',
        'prev'  =>  '上一页'
    ],
    'menu_breadcrumbs' =>  [
        'home'            =>  '主页',
        'agent_finder'    =>  '搜索房产经纪人',
        'new_projects'    =>  '新楼盘',
        'budget_search'   =>  '价格范围搜索',
        'privacy_policy'  =>  '隐私条款',
        'create_alert'    =>  '订阅新楼盘／房源提醒',
        'home_services'   =>  '家政服务',
        'terms_conditions'=>  '服务条款',
        'agents_login'    =>  '房产经纪人登陆',
        'blog'            =>  '博客',
        'more'            =>  '更多',
        'for_brokers'     =>  '房产经纪人用户界面',
        'buy'             => '购买',
        'rent'            => '租赁',
        'residential'     => '住宅',
        'commercial'      => '商用物业',
        'commercial_buy'  => '商用购买',
        'commercial_rent' => '商用租赁',
        'search_property' => '搜索房源',
        'list_property' => 'List Property',
        'my_account'      => '我的帐户',
        'international'   =>  '国际地区'
    ],
    'footer'    =>  [
        'newsletter'  =>  [
            'title'       =>  '最新房产资讯订阅',
            'message'     =>  '输入您的电子邮箱，房产资讯实时更新，一网打尽。'
        ],
        'keep_in_touch'         =>  '联系我们',
        'trending_searches'     =>  '热门搜索',
        'popular_searches_item' =>  ':loc :type:for_rent_buy',
        'popular_areas'         =>  '热门区域',
        'trending_properties'   =>  '火爆房源',
        'social_media'      =>  [
            'facebook'          =>      'https://www.facebook.com/ZoomPropertyUAE/',
            'twitter'           =>      'https://twitter.com/zoompropertyuae',
            'instagram'         =>      'https://www.instagram.com/zoompropertyuae/',
            'googleplus'        =>      'https://plus.google.com/u/2/103251563061642583375'
        ]
    ],
    'card'      =>  [
        'actions'   =>  [
            'call'      =>  '电话',
            'email'     =>  '电子邮件',
            'chat'      =>  '在线联系房地产经纪人',
            'call_back' =>  '我们给您致电',
            'whatsapp'  =>  'Whatsapp'
        ]
    ],
    'form'      =>  [
        'contact'  => '联系',
        'more_details_title'  => '了解更多详情',
        'your_messagge'       =>  '您的留言',
        'default_messagge_property' =>  '您好, 我们在Zoom房产浏览到房源编号为 :reference的房源。请与我联系。谢谢。',
        'default_messagge_project'  =>  '您好，能否可以给我此楼盘的进一步介绍？',
        'send_email'          =>  '发送电子邮件',
        'view_phone_number'   =>  '查看联系电话',
        'all_fields_required' =>  'All fields are required.',
        'placeholders'        =>  [
            'first_name'          =>  '名',
            'last_name'           =>  '姓',
            'full_name'           =>  '全名',
            'email'               =>  '电子邮箱',
            'email_address'       =>  'Email Address',
            'phone'               =>  '电话号码',
            'min_budget'          =>  '最低价格',
            'max_budget'          =>  '最高价格',
            'preferred_locations' =>  '区域偏好',
            'password'            =>  '密码',
            'password_confirm'    =>  '确认密码',
            'password_current'    =>  '旧密码',
            'password_new'        =>  '新密码',
            'nationality'         =>  '国籍',
            'mobile'              =>  '移动电话',
            'mobile_number'       =>  '移动电话号码',
            'work_phone'          =>  '工作电话',
            'location'            =>  'Enter Areas, Cities and Buildings...',
            'cities_areas'        =>  'Areas and Cities...',
            'clear'               =>  '重新搜索',
            'clear_mobile'        =>  '清除',
            'filters'             =>  'Filters',
            'min'                 => [
                'price'                 => '最低价格',
                'bed'                   => '最少卧室间数',
                'bath'                  => '最少浴室间数',
                'area'                  => '最小面积',
            ],
            'max'                 => [
                'price'                 => '最高价格',
                'bed'                   => '最多卧室间数',
                'bath'                  => '最多浴室间数',
                'area'                  => '最大面积',
            ],
            'type'                => '房产类型',
            'furnishing'          => '带家具',
            'areas'               =>  '区域',
            'brokerage'           =>  '房产中介',
            'country'             =>  '国家',
            'search_service'      =>  '服务搜素',
            'search_developer'    =>  '开发商搜索',
            'areas_buildings'     =>  '区域',
            'budget'              =>  '价格',
            'completion_status'   =>  '竣工状态',
            'keyword'             =>  '关键字'
        ],
        'button'              =>  [
            'reset'               =>  '重设',
            'submit'              =>  '提交',
            'call'                =>  '联系',
            'call_now'            =>  '现在打电话',
            'send'                =>  '发送',
            'contact'             =>  '联系',
            'request_details'     =>  '索取详细资料',
            'request_call_back'   =>  '联系我们',
            'request_call_free_back'   =>  '要求免费回电',
            'lets_chat'           =>  '联系我们',
            'create_alert'        =>  'Create Alert',
            'register'            =>  '注册',
            'reset_password'      =>  '提交',
            'change_password'     =>  '重设密码',
            'update'              =>  '更新',
            'edit'                =>  '修改',
            'cancel'              =>  '取消',
            'start_favourite'     =>  '搜索并收藏',
            'set_alert'           =>  '设置房源提醒',
            'subscribe'           =>  '订阅',
            'new_search'          =>  '重新搜索'

        ],
        'ideal_property' =>[
            'first_message'  =>  '让我们为您找到心仪的房源',
            'second_message'  =>  '告知我们您的要求，我们为您寻找心仪房源。'
        ],
        'general_enquiry' =>[
            'btn' => '立即咨询',
            'message'  =>  '告知我们您心仪的房源类型与地理位置，我们为您联系5名房产经纪人。'
        ]
    ],
    'grid'      =>  [
        'sort'      =>  [
            'title'       =>  '搜索结果分类',
            'low_to_high' =>  '高到低',
            'high_to_low' =>  '低到高',
            'newest_to_oldest' =>  '新到旧',
            'oldest_to_newest' =>  '旧到新',
            'title_mobile' => 'Sort',
            'save_mobile' =>  '保存'
        ],
        'view'      =>  [
            'map'          =>  '地图界面',
            'list'         =>  '列表显示',
            'grid'         =>  '网格状显示'
        ],
        'button'      =>  [
            'alert'        =>  'Alert me',
            'save'         =>  '搜索结果保存'
        ],
        'search' => '搜索／更多搜索选项'
    ],
    'landing'   =>  [
        'message'          =>  'News & Articles',
        'message_mobile'   =>  'Quick property Search',
        'quick_access' => [
            'link_a' => '公寓出租',
            'link_b' => '公寓出售下'
        ],
        'other' =>  'Other',
        'hot_project' =>  'Hot Project',
        'latest_video' =>  'Latest Videos',
        'view_all' =>  'View All',
        'view_less' =>  'View Less',
        'views_neighborhood' =>  'views neighbourhood',
        'searches' =>  'Searches'
    ],
    'mortgage'          =>  [
        'title'             =>  '按揭计算器',
        'description'       =>  '数值按公寓平均每平方英尺的成本计算，并包括每月市政税。请使用选项更改计算数值。',
        'other_description' =>  'Calculations based on average cost per square feet for Apartments. This includes the monthly municipal tax. Use the editable fields to modify calculations appropriately.',
        'monthly_costs'     =>  '每月支出',
        'monthly_total'     =>  '每月总支出',
        'emi_title'         =>  '每月月付估算为',
        'monthly'           =>  '每月',
        'leave_details_message'  =>  'Leave us with your details. One of our agents will get back to you shortly.',
        'categories'        =>  [
            'mortgage'          =>  '按揭',
            'energy'            =>  '电、燃气',
            'water'             =>  '水'
        ],
        'calculations'      =>  [
            'message'              =>  '修改以上数值来更新每月总支出金额',
            'purchase_price'       =>  '购买价格',
            'deposit'              =>  '预付（％）',
            'repayment_term'       =>  '还款期限（年）',
            'interest_rate'        =>  '利息（％）',
            'municipal_fees'       =>  '市政税',
            'avg_electricity_cost' =>  '平均电费支出',
            'avg_gas_cost'         =>  '平均燃气支出',
            'avg_water_cost'       =>  '平均水支出',
            'down_payment'         =>  '首付',
            'finance_amount'       =>  'Finance Amount',
            'term'                 =>  '期限（年）',
            'interest'             =>  '盈利（％每年）',
            'loan_amount'          =>  '贷款总额',
            'purchase_price'       =>  '购买价格',
            'total_amount'         =>  '总支付价格',
            'total_interest'       =>  '总利润',
        ],
        'buy_only'          =>  '只从。。。购买此房源',
        'ma_powered_by'     =>  '按揭计算器由XXX提供技术支持',
        'subject_approval'  =>  '*数字均为估算，仅供参考。请以最终官方批准额度为准。 ',
        'powered_by'        =>  '由XXX提供技术支持',
        'get_finance'       =>  '家政理财小帮手',
        'estimated_mortgage'=>  '估计抵押',
    ],
    'landmarks'       =>  [
        'mosque'          =>  '清真寺',
        'hospital'        =>  '医院',
        'school'          =>  '学校',
        'gym'             =>  '健身房',
        'bank'            =>  '银行',
        'shopping_mall'   =>  '购物中心',
        'parking'         =>  '停车场',
        'park'            =>  '公园',
        'gas_station'     =>  '加油站'
    ],
    'modals'    =>  [
        'hints'     =>  [
            'name'           =>  '您的名',
            'last_name'      =>  '您的姓',
            'full_name'      =>  '您的名字',
            'email'          =>  '您的电子邮箱',
            'phone'          =>  '您的电话号码',
            'country'        =>  '国家',
            'request_call_back' => 'Request call back on:',
            'latest_info' => 'Please send me latest property information and news'
        ],
        'agent'     =>  [
            'call'      =>  [
                'message'   =>  'You will be speaking with',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'call_back' =>  [
                'message'   =>  '您好，我对您发布的一个房源感兴趣。',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'email'     =>   [
                'message'   =>  '您好，我对您发布的一个房源感兴趣。',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp'     =>   [
                'message'   =>  '您好，我对您发布的一个房源感兴趣。',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'consumer'    =>  [
            'alert'       =>  [
                'title'       =>  '让我们为您找到心仪的房源',
                'subtitle'    =>  '告知我们您的需要，我们为您寻找心仪房源。'
            ]
        ],
        'developer'   =>  [
            'call'        =>  [
                'message'   =>  '开发商联系电话'
            ]
        ],
        'property'    =>  [
            'alert'       =>  [
                'title'       =>  '房源提醒',
                'subtitle'    =>  '订阅电子邮件提醒，新楼盘抢先知晓！'
            ],
            'call'      =>  [
                'message'     =>  'You can contact',
                'ref_message' =>  '别忘了附上房源编号哦。',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.',
                'catch_msg'   =>  'To get a <span>FREE</span> call back from the agent please provide your details below.'
            ],
            'call_back' =>  [
                'message'     =>  '我们提供英语、阿拉伯语、印度语、西班牙语以及另外72种语种的语言服务',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ],
            'call_me' =>  [
                'message'     =>  'Hi, I found your property with ref: :reference on zoom property. Please call me back. Thank you.'
            ],
            'email' =>  [
                'message'     =>  '我们提供英语、阿拉伯语、印度语、西班牙语以及另外72种语种的语言服务',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp' =>  [
                'message'     =>  '您好，我在Zoom房产网站上看到您的广告，房源编号为:reference:。请与联系我，谢谢。 ',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'project'    =>  [
            'call'      =>  [
                'message'     =>  'You can contact',
                'ref_message' =>  'Contact number for the developer',
                'payload_msg' =>  'Hi, I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'call_back' =>  [
                'message'     =>  'We speak English, Arabic, Hindi, Spanish and 72 other languages',
                'payload_msg' =>  'Hi, I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'email' =>  [
                'message'     =>  'Hi, I would like to know more about this project.',
                'payload_msg' =>  'I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp' =>  [
                'message'     =>  'Hi, I saw your advert on Zoom Property, project: :name, please get back in touch',
                'payload_msg' =>  'Hi, I found your project :name on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'service'     =>  [
            'contact'     =>  [
                'message'     =>  'Hi, I found your services on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'preference'     =>  [
            'contact'     =>  [
                'message'     =>  '你好，我在寻找位于 :location 的房源。请联系我。谢谢。'
            ]
        ],
        'signin_register'  => [
            'title'       =>  '立即注册',
            'subtitle'    =>  '享受专属您的服务',
            'signin'      =>  '登陆',
            'forgot_password' =>  '忘记密码？',
            'account'     =>  '注册新帐号？',
            'already_registered'  =>  '已有帐号？',
            'signin_here'  =>  '登陆',
            'register_here'   =>  '注册',
            'register'    =>  '注册',
            'facebook'    =>  '脸书(Facebook)',
            'google'    =>  '谷歌(Google)',
            'signin_facebook'    =>  '使用脸书帐号登陆',
            'signin_google'      =>  '使用谷歌帐号登陆',
            'register_facebook' =>  '使用脸书帐号注册',
            'register_google'   =>  '使用谷歌帐号注册',
            'agree_terms' =>  '继续注册，您表示您同意我们的',
            'agree_terms_signin' =>  'By Signing with us, you agree to our',
            'privacy_policy'   =>  '隐私条款',
            'terms_conditions' =>  '服务条款',
            'go_back_signin'    =>  '返回并继续登陆',
            'promotions'  =>  [
                'update_profile'  =>  '修改您的个人资料与偏好',
                'get_alerts'      =>  '订阅新房源邮件提醒',
                'save_favourites' =>  '保存您最心仪的房源',
                'save_search'     =>  '保存您的搜索'
            ]
        ]
    ],
    'signin_register'  => [
        'title'       =>  '立即注册',
        'subtitle'    =>  '享受专属您的服务',
        'signin'      =>  '登陆',
        'forgot_password' =>  '忘记密码？',
        'account'     =>  '注册新帐号?',
        'already_registered'  =>  '已有帐号？',
        'signin_here'  =>  '登陆',
        'register_here'   =>  '注册',
        'register'    =>  '注册',
        'signin_facebook'    =>  '使用脸书帐号登陆',
        'signin_google'      =>  '使用谷歌帐号登陆',
        'register_facebook' =>  '使用脸书帐号注册',
        'register_google'   =>  '使用谷歌帐号注册',
        'agree_terms' =>  '继续注册，您表示您同意我们的',
        'privacy_policy'   =>  '隐私条款',
        'terms_conditions' =>  '服务条款',
        'go_back_signin'    =>  '返回并继续登陆',
        'promotions'  =>  [
            'update_profile'  =>  '修改您的个人资料与偏好',
            'get_alerts'      =>  '订阅新房源邮件提醒',
            'save_favourites' =>  '保存您最心仪的房源',
            'save_search'     =>  '保存您的搜索'
        ]
    ],
    'sections'  =>  [
        'banner'    =>  [
            'agent'   =>  [
                'title'   =>  '我们为您的房产保驾护航'
            ],
            'budget'   =>  [
                'title'      =>  'Let us know your budget',
                'subtitle'   =>  'We will pick the best property for you from the best location'
            ],
            'service'   =>  [
                'title'      =>  '轻松入住'
            ],
            'landing'   =>  [
                'title'      =>  '探索更多',
                'subtitle'   =>  '找到您心中完美的房产和易居社区'
            ]
        ],
        'chat'  =>  [
            'title' =>  'You are chatting with Agent'
        ],
        'dashboard'  =>  [
            'profile'               =>  '个人资料',
            'favourite_searches'    =>  '搜索结果收藏夹',
            'favourite_properties'  =>  '心仪房源',
            'alerts'                =>  '房源提醒',
            'email_chat'            =>  'Emails & Chats',
            'settings'              =>  '设置',
            'sign_out'              =>  '登出',
            'not_favourite_search'  =>  '您还未收藏任何搜索结果',
            'not_favourite_property'=>  '无心仪房产收藏记录',
            'not_alerts'            =>  '您无新房源提醒',
            'not_chats'             =>  '无聊天记录',
            'search_again'          =>  '重新搜索',
            'change_password'       =>  '密码重设',
            'subscribe'             =>  '订阅我们的资讯'
        ]
    ],
    'property'  =>  [
        'details'   =>  [
            'similar_properties' =>  '类似房源',
            'ref_no_title'        =>  '房源编号',
            'no_building'         =>  'Some building',
            'no_area'             =>  'Some area',
            'no_city'             =>  'Some city',
            'play'                =>  'Play',
            'view'                =>  'View',
            'tab_titles'          =>  [
                'gallery'             =>  '画廊',
                'agent_details'       =>  '房地产经纪人信息',
                'property_details'    =>  '房源信息',
                'map'                 =>  '地图与周边区域',
                'video'               =>  '视频',
                'video_degree'        =>  '360',
                'local_info'          =>  '周边简介'
            ],
            'not_location_provided' =>  '此房源地理位置:agency_name未提供',
            'amenities_title'   =>  '设施',
            'description'       =>  'Apartment description',
            'agent_card'          =>  [
                'marketed_by'         =>  '开发商',
                'name_title'          =>  '姓名',
                'license_title'       =>  '证书号',
                'agency_name_title'   =>  '中介',
                'view_all'            =>  '查看所有。。。的房源',
            ],
            'view_more'          =>  'View more',
            'view_less'          =>  'View less',
            'stats'         =>  [
                'area_stats'     =>  'Area Stats',
                'avg_sale'       =>  'Avg. Sale Price',
                'avg_floor'      =>  'Avg. Floor Space',
                'decreased'      =>  'decreased in last',
                'months'         =>  'months',
                'price'         =>  'Price',
                'trends'         =>  'trends',
                'by_property'   =>  'by property type'
            ],
            'expert_review'   =>  'Expert Review',
            'cheaper_area'   =>  'cheaper than similar listing in the area',
            'cheaper_bed'   =>  'cheaper than listing with the same no. of beds',
        ],
        'listings'  =>  [
            'properties_found'   =>  'Properties Found',
            'properties_no_found_title'   =>  '您的特定搜索未找到任何房源',
            'properties_no_found_subtitle'   =>  '以下房源也许您会感兴趣？'
        ]
    ],
    'agent'     =>  [
        'details'   =>  [
            'nationality'   =>  '国籍',
            'languages'     =>  '语言',
            'company'       =>  'Company',
            'license'       =>  '证书号',
            'properties_by' =>  '开发商',
            'listed_property'     =>  'Listed Properties',
            'agent_found'   =>  '{0}0房产经纪人|{1} 房产经纪人|[2,*] 房产经纪人'
        ]
    ],
    'budget'    =>  [
        'refine_search' =>  '重新搜索',
        'seo_title'     =>  'Budget Search of properties for :rent_buy: under AED :price_max:'
    ],
    'project'   =>  [
        'starting_from'  => 'Starting From',
        'details'   =>  [
            'by'  => '通过',
            'developed_by'  => '开发商',
            'contact'       => '联系',
            'view_all'      => '查看所有。。。的最新房源',
            'about'         => '项目信息',
            'development_progress' => '竣工进度',
            'expected_completion_date' => 'Delivery Date',
            'status' => 'Status',
            'progress' => [
                'foundations'   =>  'Foundations',
                'parking'       =>  '停车场',
                'building'      =>  '楼盘名称',
                'completion'    =>  '竣工状态'
            ],
            'features_title'  =>  'Features',
            'tab_titles'          =>  [
                'gallery'             =>  '画廊',
                'floor_plans'         =>  '户型图',
                'completion_status'   =>  'Completion Status',
                '3d_tour'             =>  '3D Tour',
                'details'             =>  '详情',
                'email'               =>  '电子邮箱',
                'map'                 =>  '地图',
                'about'               => 'About this project'
            ],
            'payment_plan'    =>  '支付方式',
            'view_brochure'    =>  'View Brochure',
            'download'    =>  'Download',
            'brochure'    =>  'Brochure',
            'read_more'    =>  'Read More',
            'read_less'    =>  'Read Less'
        ],
        'listings'  =>  [
            'new_projects'    =>  '{1} 新楼盘 |[2,*] 新楼盘 |{Many} 新楼盘',
            'move_in'         =>  'Ready to Move in'
        ]
    ],
    'services'    =>  [
        'listings'      =>   [
            'results_found'       =>   '{0} Services Found|{1} Service Found|[2,*] Services Found|{Many} Services Found',
            'default_description' =>   'We will, We will, Rock you!'
        ]
    ],
    'toast'    =>  [
        'error_default' =>   'Something went wrong, Please try later',
        'error_phone' =>   '请输入有效的电话号码。',
        'error_email' =>   '请输入有效的电子邮箱地址来重置您的密码。',
        'error_email_b' =>   '请输入有效的电子邮箱地址。',
        'success_request_sent'       =>   '您的信息已成功发送！我们稍后与您联系。',
        'success_alert_created' =>   '成功订阅新楼盘／房源提醒！',
        'success_password_changed' =>   '您的密码已成功重置！',
        'success_newsletter_subscribed' =>   '电子期刊订阅成功。',
        'success_search_saved' =>   'You have successfully saved this search',
        'success_signed_in' =>   '登陆成功。',
        'success_registered' =>   '注册并登陆成功。',
        'success_email_sent' =>   'An email has been sent to your registered email',
        'showing_results' =>   'Showing Results from'
    ],
    'results'   =>  [
        'results_found'             =>  '搜索结果',
        'no_results_found'          =>  '无搜索结果',
        'no_results_found_title'    =>  '您的特定搜索未找到任何房源',
        'no_results_found_subtitle' =>  '以下房源也许您会感兴趣？',
        'not_specified'             =>  'Not Specified'
    ],
    'currencies' => [
        [
            'label' => 'Arab Emirates Dirham',
            'code' => 'AED',
            'value' => '1',
            'flag' => 'ae'
        ],
        [
            'label' => 'United States Dollars',
            'code' => 'USD',
            'value' => '0.2723',
            'flag' => 'us'
        ],
        [
            'label' => 'Australian Dollars',
            'code' => 'AUD',
            'value' => '0.4036',
            'flag' => 'au'
        ],
        [
            'label' => 'Canadian Dollars',
            'code' => 'CAD',
            'value' => '0.3622',
            'flag' => 'ca'
        ],
        [
            'label' => 'British Pound Sterling',
            'code' => 'GBP',
            'value' => '0.2228',
            'flag' => 'vg'
        ],
        [
            'label' => 'Euro',
            'code' => 'EUR',
            'value' => '0.2456',
            'flag' => 'eu'
        ],
        [
            'label' => 'Chinese Yuan',
            'code' => 'RMB',
            'value' => '1.9501',
            'flag' => 'cn'
        ],
        [
            'label' => 'Saudi Arabian Riyal',
            'code' => 'SAR',
            'value' => '1.0212',
            'flag' => 'sa'
        ],
        [
            'label' => 'Indian Rupees',
            'code' => 'INR',
            'value' => '19.5433',
            'flag' => 'in'
        ],
        [
            'label' => 'Pakistan Rupees',
            'code' => 'PKR',
            'value' => '42.8917',
            'flag' => 'pk'
        ]
    ],
    'guideme'   => [
        'property_area' => 'Properties in Your Area',
        'menu_label' => 'Guide Me',
        'finding_location' => 'Finding your location ...',
        'enter_location' => 'Enter a Location',
        'view_map' => 'View Map',
        'enable_location' => 'Please Enable Your Location',
        'you_are_in' => 'You are in ',
        'no_results' => 'No results found',
        'no_results_area_increased' => 'Sorry there were no results for your selection so we’ve increased the search area',
        'showing_stats_for' => 'Showing you area stats for',
        'no_stats' => 'Area statistics are not available',
        'apartment' => 'Apartment',
        'loft_apartment' => 'Loft Apartment',
        'hotel_apartment' => 'Hotel Apartment',
        'flat_apartment' => 'Apartment',
        'whole_building' => 'Whole Building',
        'land' => 'Land',
        'full_floor' => 'Full Floor',
        'office_space' => 'Office Space',
        'office' => 'Office',
        'labor_camp' => 'Labor Camp',
        'villa' => 'Villa',
        'plot' => 'Plot',
        'bulk_units' => 'Bulk Units',
        'bungalow' => 'Bungalow',
        'compound' => 'Compound',
        'duplex' => 'Duplex',
        'half_floor' => 'Half Floor',
        'townhouse' => 'Townhouse',
        'retail' => 'Retail',
        'shop' => 'Shop',
        'show_room' => 'Show Room',
        'staff_accommodation' => 'Staff Accomomdation',
        'warehouse' => 'Warehouse',
        'factory' => 'Factory',
        'business_center' => 'Business Center',
        'penthouse' => 'Penthouse',
        'graph_label_a' => 'Month',
        'graph_label_b' => 'Price',
        'default_location' => 'Trade Center',
        'price' => 'Price',
        'average' => 'Avg.',
        'sqft' => 'Sqft',
        'area' => 'Area',
        'sale' => 'Sale',
        'rent' => 'Rent'
    ]

];
