<?php

return [
  'policy' => '<p> This privacy statement discloses the privacy practices data usage and retrieval on <a href="https://www.zoomproperty.com">www.zoomproperty.com</a>, <a href="https://agents.zoomproperty.com">agents.zoomproperty.com</a> and <a href="blog.zoomproperty.com">blog.zoomproperty.com</a> and any other sub-domains. </p>
  <p> If you have questions or concerns regarding this statement, you should contact us on <a href="mailto:hello@zoomproperty.com">hello@zoomproperty.com</a></p>
  <h5> 1. Introduction </h5>
  <p> Zoom Property is committed to protecting the privacy of its users. This statement outlines Zoom Property’s privacy practices and is incorporated into, and subject to Zoom Property\'s Terms of Use. </p>
  <p> The purpose of this statement is to inform you as to: </p>
  <ul>
      <li> What kinds of information Zoom Property collects from users of the Zoom Property website and how such information is collected. </li>
      <li> How the information is used by Zoom Property; </li>
      <li> Details of any disclosures made by Zoom Property of user information to third parties; </li>
      <li> Where we store your personal data; </li>
      <li> How you can access, update or delete any information collected about you by Zoom Property; and </li>
      <li> The security procedures implemented by Zoom Property to protect your personal information. </li>
  </ul>
  <p> Zoom Property may change this policy from time to time. </p>
  <p> If so, any such changes will be posted on this page, so that Zoom Property may keep its users informed of its information collection practices. </p>
  <p> Accordingly, we recommend that you consult this page frequently so that you are aware of our latest policy. </p>
  <p> You can update your profile information at any time by clicking on the ‘Sign in’ link in the navigation menu and logging in. </p>
  <h5> 2. User Information We Will Collect and Why We Collect it </h5>
  <p> When a user registers with Zoom Property or makes a real estate enquiry we may ask for the following information: </p>
  <p> Email Address </p>
  <ul>
      <li> to create your individual profile and log in </li>
      <li> so that we can send user property alerts based on requests and individual preferences set up </li>
      <li> so that we can send confirmation emails for enquiries that have been made </li>
      <li> to allow real estate brokers or home service vendors respond to enquiries that have been made </li>
      <li> for account verification purposes </li>
      <li> fraud detection and prevention against users or Zoom Property </li>
  </ul>
  <p> Your full name </p>
  <ul>
      <li> so that real estate brokers can contact you and respond to enquiries you have made </li>
      <li> so that we can deal with any customer services queries or issues you may have and verify your identity </li>
  </ul>
  <p> Telephone number </p>
  <ul>
      <li> so that real estate brokers can contact you and respond to enquiries you have made </li>
  </ul>
  <p> Real estate property enquiries made </p>
  <ul>
      <li> to provide a record of enquiries made for real estate brokers </li>
      <li> to allow users to see historical activity and previous searches if required </li>
      <li> to create a more tailored and personalised user experience </li>
      <li> for internal auditing purposes </li>
  </ul>
  <p> Registering and setting up a user profile on Zoom Property is not mandatory. Information provided may be used to improve the quality of the experience and provide relevant information. </p>
  <p> Users are able to add and update personal details and preferences, set up email alerts and save properties and property searches. </p>
  <p> We may also ask you for information when you make an enquiry or you report a problem with our site. If you contact us, we may keep a record of that correspondence. </p>
  <h5> 3. Agent/Brokerage Information We Collect and Why We Collect It </h5>
  <p> When a real estate agent or brokerage registers with Zoom Property we may ask for the following information: </p>
  <p> Email address </p>
  <ul>
      <li> to create your brokerage or individual agent profile and log in </li>
      <li> so that we can send real estate enquiries to the relevant individual or entity </li>
      <li> for account verification purposes </li>
      <li> fraud detection and prevention against users or Zoom Property </li>
  </ul>
  <p> Your full name </p>
  <ul>
      <li> so that we can promote individual real estate agents via the portal on the property detail and agent finder pages </li>
      <li> so that we can deal with any customer services queries or issues you may have been raised </li>
      <li> so that we can generate user ratings on your behalf </li>
  </ul>
  <p> Telephone number </p>
  <ul>
      <li> so that any real estate telephone enquiries can be forwarded to the correct brokerage or real estate agent </li>
  </ul>
  <p> Nationality &amp; Language </p>
  <ul>
      <li> for the purposes of providing information to users and populate sections of the agent finder pages </li>
  </ul>
  <p> Real estate property enquiries received </p>
  <ul>
      <li> to provide a record of enquiries made </li>
      <li> to allow agents and brokerages to see historical activity and previous enquiries </li>
      <li> for internal auditing purposes </li>
  </ul>
  <p> Credit card and billing details </p>
  <ul>
      <li> no credit card details are stored by Zoom Property instead we use a third party secure supplier to process payments </li>
  </ul>
  <p> Registering and setting up a user profile on Zoom Property is required to allow for the listing of properties on Zoom Property. Agents and brokerages are able to add details and edit and update profile information. </p>
  <p> We may also ask you for information when you make an enquiry of it you report a problem with our site. If you contact us, we may keep a record of that correspondence. </p>
  <h5> 4. Data We Share with 3<sup>rd</sup> Parties </h5>
  <p> We do not sell or intend to share any of our user’s personal details to third parties, excluding instances listed in this section. </p>
  <p> We share some data with the following third parties in order for us to carry out our business, deliver a good user experience and continue to improve our business. As our service evolves we may additional services to this list. </p>
  <p> Further information about 3rd parties that may store our user data can be found by reviewing the Cookie Policy below. </p>
  <p> Google Analytics, Search and Display </p>
  <p> We use Google Analytics to track anonymous data about our website and service usage. We also use Google advertising platforms to serve ads to prospective anonymised users and track performance. Google collects data via a cookie on our website. </p>
  <p> Adbutler </p>
  <p> We use this service to help track anonymous data relating to impressions and clicks for our online display advertising banners </p>
  <p> RTB House </p>
  <p> Uses cookies on our site to personalise content and ads across social media and publisher websites to create and serve relevant ads to prospective users. </p>
  <p> Zendesk </p>
  <p> When a support ticket or customer service request is left on Zoom Property the data provided is managed and shared with Zendesk </p>
  <p> PayTabs </p>
  <p> PayTabs is a payment processor who securely store and manage our online transactions. We will send them all information relating to payment details. </p>
  <p> Mailchimp &amp; SendGrid </p>
  <p> We use Mailchimp and SendGrid to send newsletters and emails to our customer database. We send both platforms the information then need to send these communications. </p>
  <p> Facebook, Instagram &amp; Twitter </p>
  <p> We use social channels to feed information to followers and fans and where permission is granted for multiple format targeting and re-targeting campaigns. The social channels target their own users but we provide anonymised cookie data for remarketing and targeting purposes. </p>
  <p> In the future and from time to time we may provide data to third parties not listed above. This data will be anonymised meaning individuals cannot be identified. </p>
  <p> Additional reasons why we may share user data include: </p>
  <ul>
      <li> as part of our legal or regulatory requirement, to cooperate with any law enforcement or legal obligations </li>
      <li> as part of any legal claims or complaints </li>
      <li> in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets </li>
      <li> if Zoom Property or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets </li>
      <li> to protect the rights, property, or safety of Zoom Property, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction </li>
  </ul>
  <h5> 5. How We Use Your Data for Marketing and Promotional Purposes </h5>
  <p> As part of our normal business practice we use the third parties listed above to help us advertise and promote our services to prospective new and existing users using your anonymised data. This is a critical part of the running of our business so you cannot opt out of this activity if you wish to use Zoom Property. </p>
  <p> We may also use your email address and data collected to provide information to you, such as to notifications when our site changes, newsletters, emails that you request, or to personalise the site in accordance with your requests. </p>
  <p> We will provide you with directions in all emails and newsletters on how to be taken off our newsletter list this will usually be a link directing you to our website allowing you to change your notification settings, but you can also send us an email to <a href="mailto: hello@zoomproperty.com">hello@zoomproperty.com</a> to let us know if you wish to change the notifications you receive. </p>
  <p> We may also use your data, to provide you with information about goods and services which may be of interest to you and we may contact you by electronic means (e-mail or SMS). </p>
  <p> If you do not want us to send you newsletters, you can unsubscribe in the Settings in your Profile or via the email. </p>
  <p> We may on occasion and where you have not opted out, email offers to you of products and services in conjunction with our partners. </p>
  <p> In addition to the uses described above we will also use information held about you in the following ways: </p>
  <ul>
      <li> To ensure that content from our website is presented in the most effective manner for you and for your computer </li>
      <li> To personalise communications and provide you with information, products or services that you request from us or which we feel may interest you, where you have consented to be contacted for such purposes </li>
      <li> To carry out our obligations arising from any contracts entered into between you and us or requests you have made </li>
      <li> To allow you to participate in interactive features of our service, when you choose to do so </li>
      <li> To allow us to place advertisements on 3rd party websites based on your browsing activity on our site </li>
      <li> Whilst browsing our site allow us to push relevant promotions and advertisements to you based on your specific interactions with the website </li>
      <li> To notify you about changes to our service </li>
  </ul>
  <h5> 6. Where We Store Your Personal Data </h5>
  <p> The data that we collect from you may be transferred to, and stored at, a destination outside the UAE and GCC. It may also be processed by staff operating outside the UAE or GCC who work for us or for one of our suppliers. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy. </p>
  <p> All information you provide to us is stored on our secure servers. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone. </p>
  <p> Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. </p>
  <p> We keep your data for as long as it is deemed necessary to uphold our obligations to deliver a better experience for users, meet regulations, for auditing purposes and to prevent fraud. We may keep some information even if you have requested for your profile to be removed. </p>
  <h5> 7. Your Rights over Your Data </h5>
  <p> You have the right to be informed about how we use and store your information, the right to access the personal information we store about you, the right to request the correction of inaccurate information and the right to opt out of non critical communications. </p>
  <p> You also have the ‘right to be forgotten’ and request the deletion of any personally identifiable information. To make this request please email <a href="mailto: hello@zoomproperty.com">hello@zoomproperty.com</a></p>
  <p> You can access and update your contact information by emailing <a href="mailto: hello@zoomproperty.com">hello@zoomproperty</a> or by accessing your profile online. However, to the extent that such information is also stored in other databases, we cannot always ensure that such corrections or deletions will immediately reach the other databases. </p>
  <h5> 8. Cookies And Other Tracking Methods </h5>
  <p> We currently store a limited amount of cookies when browsing our site to allow us to offer you the best experience from us and allow us to learn and improve what we offer you on the site. </p>
  <p> A cookie is a small text file which is used to differentiate yourself from other users each time you visit us. This information is stored on your hard drive, not on our site, and they do not hold any personally identifying information. </p>
  <p> We use cookies on our site for a number of reasons. By using Zoom Property you automatically opt-in to us using cookies when using our website. First, cookies can help us provide information that is targeted to your interests. Second, cookies allow us to better understand how users use our site, which in turn helps us focus our resources on features that are most popular with our users. You can opt-out of using cookies by changing your browser settings. However, should you decline our cookies, some parts of our site may not work properly. </p>
  <p> The cookies we use on ZoomProperty.com are as follows: </p>
  <ul>
      <li>
          Infrastructure Cookies
          <p> ci_cookie: This is used by our site to provide session specific information to our system </p>
      </li>
      <li>
          _gid Analytics Cookies
          <p> We use Google Analytics to view site usage statistics on the site which allows us to track site usage, understand visitor flow and help us improve the site. </p>
      </li>
      <li>
          _gac_UA-xxx
          <p> This cookie allows Us to understand how you arrived at zoomproperty.com and monitor which of Our marketing tools are most effective. </p>
      </li>
      <li>
          Paytabs
          <p> We use PayTabs as our payment processor. This cookie is required to understand basket and checkout flow throughout the site. </p>
      </li>
      <li>
          Logglytrackingsession
          <p> We use logging aggregator Loggly to compile any error logs which are experienced on the site. </p>
      </li>
      <li>
          Token
          <p> This cookie is used to understand the user state throughout the site and handle logged in user functions </p>
      </li>
      <li>
          Third party cookies
          <p> In some cases, You will find third party cookies stored on your computer from one or more of our trusted partners detailed above in section 4. </p>
      </li>
      <li>
          Targeted advertising
          <p> We work with several advertising platforms that provide targeted advertising solutions. You can Your privacy settings for individual partners below including: </p>
          <ol>
              <li> Bing </li>
              <li> Facebook </li>
              <li> Google </li>
              <li> Twitter </li>
          </ol>
      </li>
      <li style="margin-top: 2rem">
          Analytical Cookies
          <p> You may also find cookies stored on your computer which allows us to track your engagement with various campaigns provided by one of our trusted partners or affiliates programmes. These cookies are used to track success campaigns and do not store any personally identifiable information. If You would not like to be tracked for analytical purposes, you can disable sending of cookie data within Your browser. </p>
      </li>
      <li>
          Opting out
          <p> By using/browsing ZoomProperty.com you are approving the use of cookies as set out above and inline with our cookie policy. If you have an objection to our use of cookies you must either disable cookies on your browser or stop using ZoomProperty.com </p>
          <p> In addition to using cookies, in some circumstances we may track or collect information about your activities on our site by the numeric address assigned to the computer you are using (your IP address) or by the URLs that you come from or leave to. This information is collected and shared with our partners in an aggregated, non-personally identifiable basis. </p>
      </li>
  </ul>
  <h5> 9. Other Sites </h5>
  <p> The Zoom Property website may contain links and references to other websites and organisations, including websites owned or operated by Zoom Property\'s advertisers/partners. Please note that different rules may apply to the collection, use or disclosure of your information by third party service providers or vendors or any other sites you encounter on the Internet (even if these sites are branded with our branding or framed by our site). We encourage you to investigate and ask questions before disclosing information to third parties and to read their privacy policies. </p>
  <h5> 10. Limits on Our Abilities </h5>
  <p> Although your privacy is very important to us, due to the existing legal and technical environment, we cannot fully ensure that your private communications and other personally identifiable information will not be disclosed to third parties. For example, we may be forced to disclose information to the government or third parties under certain circumstances, or third parties may unlawfully intercept or access transmissions or private communications. Additionally, we can (and you authorise us to) disclose any information about you to private entities, law enforcement or other government officials as we, in our sole discretion, believe necessary or appropriate to investigate or resolve possible problems or inquiries. </p>
  <h5> 11. Security </h5>
  <p> We use industry standard efforts to safeguard the confidentiality of your personal identifiable information, such as firewalls and Secure Socket Layers where appropriate. </p>
  <h5> 12. Law and Jurisdiction </h5>
  <p> This Policy, as well as your access to the Website, is subject to and governed by the laws of the United Arab Emirates as applied in the Emirate of Dubai without giving effect to conflicts of law principles thereof. Any dispute regarding this Policy or our handling of your Personal Information and General Information shall be subject to the exclusive jurisdiction of the Courts in Dubai. </p>
  <h5> 12. Further Questions </h5>
  <p> Any further questions you have should be directed to <a href="mailto :hello@zoomproperty.com">hello@zoomproperty.com</a></p>'
];
