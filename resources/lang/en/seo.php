<?php

return [

    'project'    =>  [
        'zj-cherrywoods'    =>  [
            'title'                 =>  'Meraas Cherrywoods Townhouses: Cherrywoods Townhouses In Dubai',
            'description'           =>  'Meraas Cherrywoods Townhouses: New project launched by Meraas named as Cherrywoods, 3 & 4 bedroom premium townhouses from AED 1.4M at Al Qudra Road Dubai.'
        ],
        'zj-la-rive-port-de-la-mer'    =>  [
            'title'                 =>  'La Rive in La Mer by Meraas - Property For Sale In Port De La Mer',
            'description'           =>  'La Rive In Port De La Mer, Dubai:  A new project by Meraas, featuring luxurious 1 to 5 bedroom beach side apartments at Port De La Mer Jumeirah in Dubai, UAE'
        ],
        'zj-bvlgari'    =>  [
            'title'                 =>  'Bvlgari Residences By Meraas: Bvlgari Resorts & Residences Dubai',
            'description'           =>  'Bvlgari Residences By Meraas: Bulgari Resort & Residences Dubai is the world’s fifth Bulgari Hotels and Resorts property for sale in Jumeirah Bay Island, Dubai.'
        ],
        'zj-nikki-beach-residences'    =>  [
            'title'                 =>  'Meraas Nikki Beach Residences - Nikki Beach Residences In Dubai',
            'description'           =>  'Nikki Beach Residences: Meraas offers Nikki Beach 1/2/3 Bedroom apartments. Explore the various real estate options available at Nikki Beach Residences Dubai.'
        ]

    ]

];
