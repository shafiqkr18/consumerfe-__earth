<?php

return [

	'handover' => 'Handover',
    'i_am_intrested' => 'I\'m Interested',
    'form_bedrooms' => 'How many bedrooms?',
    'projects' => [
        'zj-la-rive-port-de-la-mer' => [
            'label_a' => '<span>Introducing <strong>La Rive</strong></span>A Freehold Master Community',
            'label_b' => '<span>1-5 Bedroom Beachfront Apartments</span> At La Rive From AED 1.2M'
        ],
        'zj-bluewaters-residences' => [
            'label_a' => '<span>Beachfront living at <strong>Bluewaters</strong></span>The most sought after island retreat in Dubai',
            'label_b' => '<span>1-4 Bedroom Beachfront Apartments</span> Starting From AED 2.1M'
        ],
        'zj-nikki-beach-residences' => [
            'label_a' => '<span>Beachfront Apartments and Townhouses</span>A new way of life',
            'label_b' => '<span>1-4 Bedroom Apartments</span> Starting From AED 2.4M'
        ],
        'zj-cherrywoods' => [
            'label_a' => '<span>Exclusive Collection of Freehold Townhouses</span>5 Year Payment Plan Available',
            'label_b' => '<span>3-4 Bedroom Spacious Townhouses</span> Starting from AED 1.2M'
        ],
        'zj-bvlgari' => [
            'label_a' => '<span>Resort and Residences</span>Experience a Lifestyle Reserved for the Privileged',
            'label_b' => '<span>Ready to move in units</span> Starting from AED 4.5M'
        ],
        'zj-port-de-la-mer' => [
            'label_a' => '<span>Resort and Residences</span>Experience a Lifestyle Reserved for the Privileged',
            'label_b' => '<span>Ready to move in units</span> Starting from AED 4.5M'
        ]
    ]

];
