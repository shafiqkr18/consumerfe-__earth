<?php

return [
  'h1'   => 'Zoom Property Terms and Conditions',
  'content' => '<p> ZoomProperty.com is a property portal for consumers, real estate agents and real estate brokerages. </p>
  <p> You may only use ZoomProperty.com under these Terms and Conditions and by using Zoom Property or signing up for a Zoom Property account you are deemed to accept these terms and conditions as amended from time to time. </p>
  <p> While every effort has been made to cover all elements of the Zoom Property service within these Terms and Conditions it is also expected that standard online practices must be followed and each individual/ company is expected to act in accordance with laws of the United Arab Emirates as applied in the Emirate of Dubai. </p>
  <p> If you have any questions about this User Agreement, please contact us on hello@zoomproperty.com </p>
  <p> Unless the context requires otherwise, the following terms shall have the following meanings in these Terms &amp; Conditions: </p>
  <ul>
      <li> "Account/Account holder/User" means any person, persons or company who have signed up to use, access or browse Zoom Property. </li>
      <li> "Pages/Sections" means sections of the Site separated to sort Services by structure. </li>
      <li> "Content" means information on the Site or Zoom Property. </li>
      <li> "Zoom Property" means including our parent, subsidiaries, affiliates, trading names, officers, directors, agents and employees and any sub-service and all domains and sub-domains </li>
      <li> "Zoom Property User" means the account holder or visitor who aims to search, select and enquire about properties or third party services using the Service. </li>
      <li> "Identity" means information relating to an individual given to prove identity. </li>
      <li> "Real Estate Broker" means the account holder who aims to sell stock/products by using the Service. </li>
      <li> "Service" means the advertised and intended operations of Zoom Property. </li>
      <li> "Site" means the website and online presence of Zoom Property. </li>
      <li> "Spam" means unwanted/unsolicited e-mail. </li>
      <li> "Staff member" means an employee or contractor for Zoom Property. </li>
      <li> "We/Us" means Zoom Property. </li>
      <li> "You" means the individual or person acting on behalf of a company. </li>
  </ul>
  <p> Clause, schedule and paragraph headings are for ease of reference only and do not affect the interpretation or construction of this Agreement. </p>
  <p> A person includes a natural person, corporate or unincorporated body (whether or not having separate legal personality). </p>
  <p> The Schedules form part of this Agreement and shall have effect as if set out in full in the body of this Agreement and any reference to this Agreement includes its Schedules. </p>
  <p> Words in the singular shall include the plural and vice versa. </p>
  <p> A reference to a statute or statutory provision is a reference to it as it is in force for the time being, taking account of any amendment, extension, or re-enactment and includes any subordinate legislation for the time being in force made under it. </p>
  <p> A reference to writing or written does not include faxes or e-mail, unless specified herein. </p>
  <p> Where the words include(s), including or in particular are used in this Agreement, they are deemed to have the words without limitation following them. </p>
  <p> Where the context permits, the words other and otherwise are illustrative and shall not limit the sense of the words preceding them. </p>
  <p> Any obligation in this Agreement on a person not to do something includes an obligation not to agree, allow, permit or acquiesce in that thing being done. </p>
  <p> References to Clauses, Sub-clauses and Schedules are to the Clauses and Schedules of this Agreement. </p>
  <h5> Eligibility </h5>
  <p> In using the Service you must at all times: </p>
  <ul>
      <li> Be able to enter into a legal contract. </li>
      <li> Be at least eighteen (18) years of age. </li>
      <li> Only hold one Zoom Property account at any time. </li>
      <li> Only use a valid e-mail address, which does not have a set expiry time/window. </li>
      <li> Ensure that all information provided is true, accurate and not misleading at the time of writing. </li>
  </ul>
  <p> In using the Service you must not at any time: </p>
  <ul>
      <li> Use someone else’s Zoom Property account. </li>
      <li> Gain access to another Zoom Property account. </li>
      <li> Buy or sell any Zoom Property account or account information. </li>
      <li> Impersonate another Zoom Property user or Zoom Property account holder </li>
      <li> Try to mislead or trick Zoom Property in order to gain any advantage. </li>
      <li> Post inaccurate, false, misleading, defamatory or libellous Content. </li>
      <li> Post information and/or Content onto the Site that may harm Zoom Property or any of its account holders, employees, consultants, officers, directors, shareholders, agents or any member of the public or any other person whomsoever or whatsoever. </li>
      <li> Manipulate or gain advantages over other Zoom Property users and/or account holders in a manner not intended by the services guidelines and/or help tools. </li>
      <li> Be able to enter into a legal contract. </li>
      <li> Be at least eighteen (18) years of age. </li>
      <li> Only hold one Zoom Property account at any time. </li>
      <li> Only use a valid e-mail address, which does not have a set expiry time/window. </li>
      <li> Ensure that all information provided is true, accurate and not misleading at the time of writing. </li>
  </ul>
  <h5> Zoom Property\'s rights </h5>
  <p> All information and Content on the Site remains the copyright and trademark (registered or unregistered) of Zoom Property and our advertisers. You may not copy, modify or distribute this content unless offered as a service. </p>
  <p> The information and/or content on the Site is the property of Zoom Property and our advertisers, unless offered as a Service, users and/or account holders are not permitted to distribute, download, store, spam or abuse any data obtained or gleaned from the Site. </p>
  <p> Zoom Property may at any time without notice to you: </p>
  <ul>
      <li> Terminate, delay, suspend, alter or completely remove the Service or any part thereof. </li>
      <li> Remove offensive, derogatory and/or abusive Content and close the account of any account holder reasonably suspected of producing, uploading such Content. </li>
      <li> Interrupt or delay the Service or any part thereof for such time as is necessary and for any reason related to the business operations of Zoom Property. </li>
      <li> Take technical and/or legal steps to remove any user and/or account holder entirely from the Site if, in its reasonable belief, the user/account holder is or has engaged in activities contrary to any of these Terms and Conditions. </li>
  </ul>
  <p> Zoom Property is not responsible for any loss any user or account holder may incur as a result of using the Service. </p>
  <p> Zoom Property has the right to use any Content and/or information submitted to the Site for any purpose including but not limited to, publicity, marketing, service improvements, including use on 3rd party applications. </p>
  <h5> User Communications </h5>
  <p> Zoom Property will only send out account information e-mails to users and/or account holders unless the user or account holder has opted out of such communications. </p>
  <p> Promotions, Updates, Changes, Advertising and Service updates, are not regarded as Spam. </p>
  <p> No account holder or user may use data obtained or gleaned from the Site to send out e-mails that do not directly relate to the Service. </p>
  <p> Any services on Zoom Property that allow users to communicate are offered in good faith with an understanding that any communications will be good natured and sent with good reason. We will not tolerate communication that has seemingly no reason and/or is an advertisement for a third party service/product. </p>
  <p> Any communication or data transferred over the Site may be scanned, filtered and monitored. You are responsible for any communication or actions from your Zoom Property account. </p>
  <p> Zoom Property will cooperate with any regulatory and/or law enforcement agency, from any country where necessary to provide or hand over information that is lawfully requested. </p>
  <h5> Abuse of Zoom Property service and interface </h5>
  <p> While we will make every effort to remove out of date information from the Site, we can accept no responsibility for any information on any page (including ‘content’ pages) that is not accurate or is out of date. </p>
  <p> You agree (as a user or account holder) that you will not, in a group or by yourself, authorise, employ, lease, knowingly allow or solicit: </p>
  <ul>
      <li> Any robot, spider or other automated means to access Zoom Property. </li>
      <li> Any service (free or paid for) that automates interactions with the Zoom Property services that are intended for genuine human involvement. </li>
      <li> Any action that imposes unreasonable or disproportionate loads on our technical infrastructure; we hold the authority to determine and define this action. </li>
      <li> Any action that removes, modifies or deletes any data from the Zoom Property servers outside intended means to do so by the service options. </li>
      <li> Any actions that copy, reproduce, modify, create derivative works from, distribute or publicly display any content from Zoom Property without express permission. </li>
      <li> Any unauthorised 3rd party tool that interferes (actual or attempted) with the service, and reduces or hinders use to yourself or any other users. </li>
      <li> Any activity that can or tries to bypass any security measures we may use to prevent or restrict access to the Site. </li>
  </ul>
  <h5> General Account Holder Information </h5>
  <p> The Site allows users to share personal information with us and with other users. </p>
  <p> Zoom Property will use its reasonable endeavours to keep your personal data private and secure. </p>
  <p> Zoom Property accepts no liability for any form of obtrusive, illegal, unintended, hacked activity that ultimately leads to the release of personal data. </p>
  <p> You may at times be shown the information of other users who Zoom Property believe share similar interests, have been involved in the same actions as yourself, or may have reason to be grouped together. While every effort is made to keep this sharing as targeted as possible, some users may abuse the system or Zoom Property may interpret inputted information incorrectly. For this reason Zoom Property can take no liability for how this information is shared correctly or incorrectly. </p>
  <p> Zoom Property is unable to fully safeguard or control how other account holders may use your information and for this reason we encourage you to evaluate the information you provide to the Site and understand, before submitting, how your data will be used. In return we expect you to respect other users data and treat it how you would treat your own. </p>
  <h5> Charges from Zoom Property </h5>
  <p> For the UAE, Zoom Property operates in UAE Dirhams. Any charges from Zoom Property will be made visible on the Site before any payment is taken (this may not include agreed recurring payments). There are various services where a fee may apply and each service clearly lists how this fee is applied. You will be responsible for any additional taxes or any other charges that occur from using the Service. </p>
  <p> Please see the appropriate section within these Terms &amp; Conditions relating to payments and/or fee charges for your desired activity with Zoom Property. </p>
  <h5> Use of all account content </h5>
  <p> By uploading, adding or submitting content to the Site, you grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, right to exercise any and all copyright, publicity, trade marks, database rights and intellectual property rights you have in the Content, in any media known now or in the future. In addition, you waive all moral rights you have in the Content to the fullest extent permitted by law. </p>
  <p> Zoom Property will not be held responsible for any inaccuracies in our search results or listed data. You will also remain responsible for any data submitted to Zoom Property and must ensure that you alter, amend or delete any information that is no longer relevant. </p>
  <p> You agree not to post, upload or supply in any way any Content (or links to content) that: </p>
  <ul>
      <li> Is obscene, fraudulent, indecent, discourteous, racially offensive or abusive; </li>
      <li> defames, abuses, harasses or threatens others; </li>
      contains any viruses, Trojan horses, worms, time bombs, cancelbots, or other disabling devices or other harmful component intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information;
      <li> advocates or encourages any illegal activity; </li>
      <li> violates the privacy of individuals, including other users of the Site; or </li>
      <li> violates any applicable local, state, national, or international law </li>
  </ul>
  <h5> General Liability/Indemnity understanding </h5>
  <p> In addition, and not replacing any other release from and/or limitation of liability under these Terms &amp; Conditions, Zoom Property will not be liable for: </p>
  <ul>
      <li> Any actions by users or account holders (including information posted, uploaded, items listed) that do not represent the Service </li>
      <li> Any actions by users or account holders which are in breach of any code of conduct or guidelines issued by us at any time. </li>
      <li> Any review, statistic or calculation that is not factually representative or correct. </li>
      <li> Any 3rd party service (e.g. payment) where Zoom Property has no control. </li>
      <li> Any sale and purchase conducted through the Site where the purchaser is another user or 3rd party. </li>
      <li> Any falsity in user-provided account information. </li>
      <li> Any business losses, such as loss of data, revenue, opportunity, reputation, goodwill, profits, or business interruption or for any losses which are not reasonably foreseeable by us arising, directly or indirectly, out of your use of or your inability to use Services. </li>
      <li> Any failure on Zoom Property’s behalf to act immediately or in reasonable time to a breach by you or others does not waive our right to act with respect to subsequent or similar breaches. </li>
  </ul>
  <h5> Data protection </h5>
  <p> Your personal data and privacy is important to Zoom Property. Please see our Privacy Policy that forms part of these Terms &amp; Conditions. </p>
  <h5> Advertisements </h5>
  <p> While many services and results on Zoom Property are formed ‘organically’, we may at any time and at our discretion alter elements of the Site to benefit companies or individuals who have purchased preferential rights to gain additional/favoured exposure. </p>
  <p> Zoom Property may place any form of advertisements at any place on the Site. </p>
  <p> To the extent reasonably practicable, we may indicate on the Site which goods and services are advertisements for any promotion that falls outside the Service. You are free at any time to select or click on these advertisements at your free will. </p>
  <h5> Links to and from other websites </h5>
  <p> Some services on Zoom Property have a high proportion of external links to third party websites over which we have no affiliation or control regarding their content. </p>
  <p> Third party websites are linked to the Site to provide information and are intended to be for your convenience. While every effort has been made to guide content posters (account holders) as to the correct information to link to, we have no control over and do not accept any responsibility and/or liability for any content, data or other elements you may encounter on any third party websites. </p>
  <p> Zoom Property may become integrated closely with various social networking websites (such as Facebook) and we similarly do not accept any responsibility and/or liability for any loss or damage that may arise from your use of any social networking website whether such use is through or in conjunction with the Site or not. </p>
  <p> All users and/or account holders are deemed to accept and understand that access to any third party websites is entirely at their own risk. </p>
  <h5> Release </h5>
  <p> We understand that due to the social nature of the Service your involvement with other users and/or account holders may extend beyond the Service and may extend outside the platform Zoom Property provides. For this reason you release Zoom Property from any claims, demands and damages of every kind associated with or arising out of or in connection with such extended use and/or involvement.- </p>
  <p> You hereby agree and undertake to compensate and/or indemnify Zoom Property for any misuse or abuse that breaches these Terms &amp; Conditions or your use/misuse of the Site which results in any losses, damages, fines, compensation or costs (including reasonable legal fees). </p>
  <p> Use of the Site does not at any time render you the partner, agent, joint venture, employee-employer, franchise-franchisee of Zoom Property. </p>
  <h5> Governing law </h5>
  <p> These terms and conditions are subject to and governed by the laws of the United Arab Emirates as applied in the Emirate of Dubai without giving effect to conflicts of law principles thereof. Any dispute regarding this Policy or our handling of your Personal Information and General Information shall be subject to the exclusive jurisdiction of the Courts in Dubai. </p>
  <h5> Buyer registration </h5>
  <h5 style="margin-top: 0;"> Why register? </h5>
  <p> In order to provide you with a more personalised experience and take full advantage of all features you are able to register for an account. Registration is also used to protect your personal details and provide you with an easy access point when needing to review previous activity, along with details relating to them. </p>
  <h5> How to register </h5>
  <p> Depending on the Zoom Property service, registration can be completed from many Site pages or on by using the service itself. You may be able to register through a third party account (for example Facebook) where you will grant us certain rights to collect information from this outside source. Should you delete this account and/or the 3rd party become unavailable for any reason, you may no longer be able to access your account. Zoom Property will request certain information from you including, but not limited to, your name, e-mail address and phone number and by entering into the registration process you authorise Zoom Property to collect, store and process this information in accordance with the Zoom Property Privacy Policy. </p>
  <h5> Passwords </h5>
  <p> You may be automatically assigned a password when registering your e-mail on the Site if you are not asked to provide one at registration. If you are automatically assigned a password, we strongly recommend that you change the issued password to one of your own choice. This password is very important to your use of the Service and to your security. Accordingly, you must keep the password confidential and you must refrain from letting others know about it at all times. You agree that any person to whom you disclose your password is authorised to act as your agent for the purposes of using the Service and you release Zoom Property of any liability from the consequences (financial or other) that may follow as well as giving the same undertaking in respect of such other person’s use of the Site as you give at “RELEASE” above. </p>
  <h5> Valid E-mail Address </h5>
  <p> Every account registered on the Site must have a valid and genuine e-mail address and/or 3rd Party account that links to a valid e-mail address of your own. Any accounts that have been registered without a valid e-mail will be terminated without notice. It is usual practice for Zoom Property to require verification by way of e-mail for any new e-mail account and/or change of e-mail. </p>
  <h5> Closing Accounts </h5>
  <p> We have the right at all times to close an account at any time, with no notice and with no obligation to fulfil any account order or recover any financial loss on your behalf howsoever suffered. </p>
  <h5> Multiple logins </h5>
  <p> While we accept people may use multiple logins by way of division between personal/business and other uses on the Site, we reserve the right to take action across all your accounts if required. </p>
  <h5> Zoom Property users protection </h5>
  <p> While Zoom Property will make every effort to create a fair, peaceful community, we rely on our user and/or account holders to make us aware of misconduct or additional issues related to any of the Service from any users and/or account holders. </p>
  <p> In order to achieve the safest environment for our Zoom Property Users we have created a set of Zoom Property Users guidelines: </p>
  <ul>
      <li> You should adopt normal internet-advised security measures before communicating or transacting online. </li>
      <li> You should always keep in mind that despite efforts, Zoom Property may not be able to fully vet every real estate brokerage or advertiser, and you should therefore exercise due care when making an enquiry and with any subsequent transaction </li>
      <li> You should take the time to read through and understand all third party terms and polices, before committing to any purchase or entering into any agreement. </li>
      <li> Your point of contact at all times is the third party real estate broker or service provider. </li>
      <li> Although Zoom Property tries to mitigate any listing errors of a property, you understand that mistakes may occur, and Zoom Property cannot be held liable for any incorrect information. </li>
  </ul>
  <p> Sending you E-mails: Zoom Property will e-mail with regards to certain activities on Zoom Property. </p>
  <p> Lost/ Stolen Account Information: It is your duty at all times to make us aware if your account information is lost or stolen. </p>
  <h5> Obligations </h5>
  <p> Seller Terms: You should note that real estate brokers and third party service providers will have a separate set of terms &amp; conditions </p>
  <p> Accurate Information: At all times Zoom Property tries to make sure that all information on the Site is accurate. We also require all account holders only submit accurate information. We cannot be liable for any user submitted information on the Site. </p>
  <p> Content of Information: It is the aim of Zoom Property to make the Content suitable for a family audience at all times. For this reason you are refrained from posting any Content which may be classed as unsuitable according to this standard and as adjudged by us at our sole discretion. </p>
  <h5> Notifications </h5>
  <p> We will aim to notify you by e-mail at all important instances throughout the Zoom Property process. Whilst we will do our best to make sure these notifications are regular and up to date we cannot be responsible for any impact they may have on your decision making process or usability of the Service. You have options to turn off certain e-mail notifications in your Account Settings page. We are not responsible for how these e-mails are delivered. </p>
  <h5> Account information </h5>
  <p> The information stored in your Zoom Property account is deemed as private and we will offer security measures to the best of our ability in order to keep this information secure. We do not hold your payment information on file however brokers and third party service providers will have access to your postal address, e-mail and further information required if you have made an enquiry. We have no responsibility and/or liability arising out of any brokers and third party service providers use of information you have provided. </p>
  <h5> Complaints/Problems </h5>
  <p> We understand that the Service, despite our best intentions, may at times have issues such as bugs and/or other conditions that will ultimately hinder the Service from performing as intended. We also are aware that with 3rd parties utilising the Site many aspects of our Service are beyond our control and we can accept no responsibility and/or liability arising out of in connection with this. </p>
  <p>For any complaints or problems please contact the support options available from the Site.</p>
  <h5> Zoom Property users vs broker/third party service provider agreement </h5>
  <p> It should be understood at all times whilst using Service that the consequent contract for purchase is between the Zoom Property Users and the brokers or third party service provider agreement. Zoom Property acts as facilitator of the enquiry or transaction only. </p>
  <h5> Law and Jurisdiction </h5>
  <p> This Policy, as well as your access to the Website, is subject to and governed by the laws of the United Arab Emirates as applied in the Emirate of Dubai without giving effect to conflicts of law principles thereof. Any dispute regarding this Policy or our handling of your Personal Information and General Information shall be subject to the exclusive jurisdiction of the Courts in Dubai. </p>'
];
