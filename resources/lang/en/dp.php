<?php

return [

	'handover' => 'Handover',
    'i_am_intrested' => 'I\'m Interested',
    'form_bedrooms' => 'How many bedrooms?',
    'projects' => [
        'zj-la-quinta' => [
            'label' => '<span>Register your interest now</span>'
        ],
        'zj-mudon-views' => [
            'label' => '<span>Register your interest now</span>'
        ],
        'zj-1-jbr' => [
            'label' => '<span>Register your interest now</span>'
        ]
    ]

];
