<?php

return [

    'global'    =>  [
        'hi'                      =>  'Hi',
        'seo_title'               =>  "UAE Best & No #1 Property Website",
        'seo_description'         =>  "No#1 Property Website: Zoom property is the best and largest real estate website in UAE. Providing a huge range of residential and commercial properties for rent and sale in UAE. If you are searching for a new home, an investment, a holiday house in UAE, you will find everything on our property website.",
        'seo_h1'                  =>  '',
        'seo_h2'                  =>  '',
        'seo_h3'                  =>  '',
        'seo_image_alt'           =>  'Zoom Property',
        'to'                      =>  'to',
        'for'                     =>  'for',
        'in'                      =>  'in',
        'video'                   =>  'Video',
        'from'                    =>  'From',
        'sqft'                    =>  'sqft',
        'sqft_long'               =>  'Sq. ft',
        'aed'                     =>  'AED',
        'uae'                     =>  'UAE',
        'emirate'                 =>  'Emirate',
        'search'                  =>  'Search',
        'search_filter'           =>  'Search/Filter',
        'advanced_search'         =>  '+ Advanced Search',
        'find'                    =>  'Find',
        'rent_buy'                =>  'Rent/Buy',
        'rent_property'           =>  'Rent Property',
        'buy_property'            =>  'Buy Property',
        'bedroom'                 =>  '{0} Bedrooms|{1} Bedroom|[2,*] Bedrooms',
        'bedrooms'                 =>  'Bedrooms',
        'bathroom'                =>  '{0} Bathrooms|{1} Bathroom|[2,*] Bathrooms',
        'price'                   =>  'Price',
        'area'                    =>  'Area',
        // 'bed'                     =>  '{0} Studio|{1} :bed Bed|[2,*] :bed Beds',
        'bed'                     =>  '{0} Studio|{1} :bed|[2,*] :bed',
        // 'bath'                    =>  '{0} Bath|{1} :bath Bath|[2,*] :bath Baths',
        'bath'                    =>  '{0} Bath|{1} :bath|[2,*] :bath',
        'default_title'           =>  'An Amazing Property',
        'default_agency_name'     =>  'An Interesting Agency',
        'default_agency_title'    =>  'The Agency',
        'default_agent_name'      =>  'An Amazing Agent',
        'default_agent_title'     =>  'This Agent',
        'default_project_name'    =>  'An Amazing Project',
        'default_service_name'    =>  'Amazing Service',
        'about'                   =>  'About',
        'properties_for'          =>  'Properties for',
        'properties'              =>  'Properties',
        'featured'                =>  'Featured',
        'sponsored'                =>  'Sponsored ad',
        'verified'                =>  'Verified',
        'visit_wesite'            =>  'Visit Website',
        'more'                    =>  'more...',
        'less'                    =>  'less...',
        'agency'                  =>  '{0} Agency |{1} Agencies',
        'agent'                   =>  '{0} Agent |{1} Agents',
        'completion_status'       =>  'Completion Status',
        'time'                    =>  'Time',
        'international'           =>  'International',
        'studio'                  =>  'Studio',
        'most_popular'            =>  'Most Popular',
        'popular_searches'        =>  'Popular Searches',
        'types'                   =>  'Types',
        'available_unit'          =>  'Available units in',
        'cheque'                  =>  '{1} Cheque |[2,*] Cheques'
    ],
    'featured'              =>  [
        'residential'            =>  [
            'sale'                  =>  [
                'title'                 =>  'Featured Residential Properties for Sale'
            ],
            'rent'                  =>  [
                'title'                 =>  'Featured Residential Properties for Rent'
            ]
        ],
        'commercial'            =>  [
            'sale'                  =>  [
                'title'                 =>  'Featured Commercial Properties for Sale'
            ],
            'rent'                  =>  [
                'title'                 =>  'Featured Commercial Properties for Sale'
            ]
        ]

    ],
    'pagination' =>[
        'next'  =>  'Next',
        'prev'  =>  'Prev'
    ],
    'menu_breadcrumbs' =>  [
        'home'            =>  'Home',
        'agent_finder'    =>  'Agent Finder',
        'new_projects'    =>  'New Projects',
        'budget_search'   =>  'Budget Search',
        'privacy_policy'  =>  'Privacy Policy',
        'create_alert'    =>  'Create Alert',
        'home_services'   =>  'Home Services',
        'terms_conditions'=>  'Terms and Conditions',
        'agents_login'    =>  'Agents Login',
        'blog'            =>  'Blog',
        'more'            =>  'More',
        'explore'         =>  'Explore',
        'for_brokers'     =>  'For Brokers',
        'buy'             =>  'Buy',
        'rent'            =>  'Rent',
        'residential'     =>  'Residential',
        'commercial'      =>  'Commercial',
        'commercial_buy'  =>  'Commercial Buy',
        'commercial_rent' =>  'Commercial Rent',
        'search_property' =>  'Search Property',
        'list_property'   =>  'List Property',
        'my_account'      =>  'My Account',
    ],
    'footer'    =>  [
        'newsletter'  =>  [
            'title'       =>  'Keep up to date',
            'message'     =>  'Leave us your email address and we will be sure to keep you informed.'
        ],
        'keep_in_touch'       =>  'Keep in touch',
        'trending_searches'    =>  'Trending Searches',
        'popular_searches_item' =>  ':type :for_rent_buy في :loc',
        'popular_areas'       =>  'Popular Areas',
        'trending_properties' =>  'Trending Properties',
        'social_media'      =>  [
            'facebook'          =>      'https://www.facebook.com/ZoomPropertyUAE/',
            'twitter'           =>      'https://twitter.com/zoompropertyuae',
            'instagram'         =>      'https://www.instagram.com/zoompropertyuae/',
            'googleplus'        =>      'https://plus.google.com/u/2/103251563061642583375'
        ]
    ],
    'card'      =>  [
        'actions'   =>  [
            'call'      =>  'Call',
            'email'     =>  'Email',
            'chat'      =>  'Chat',
            'call_back' =>  'Callback',
            'whatsapp'  =>  'Whatsapp'
        ]
    ],
    'form'      =>  [
        'contact'  => 'Contact',
        'more_details_title'  => 'Request for more details',
        'contact_agent_title'  => 'Contact Agent for more information',
        'your_messagge'       =>  'Your Message',
        'default_messagge_property' =>  'Hi, I found your property with ref: :reference on zoom property. Please contact me. Thank you.',
        'default_messagge_project'  =>  'Hi, I would like to know more about this project.',
        'send_email'          =>  'Send email',
        'view_phone_number'   =>  'View phone number',
        'placeholders'        =>  [
            'first_name'          =>  'First name',
            'last_name'           =>  'Last name',
            'full_name'           =>  'Name',
            'email'               =>  'Email',
            'email_address'       =>  'Email Address',
            'phone'               =>  'Phone',
            'phone_number'        =>  '+971',
            'min_budget'          =>  'Min Budget',
            'max_budget'          =>  'Max Budget',
            'preferred_locations' =>  'Preferred Locations',
            'password'            =>  'Password',
            'password_confirm'    =>  'Confirm Password',
            'password_current'    =>  'Current Password',
            'password_new'        =>  'New Password',
            'nationality'         =>  'Nationality',
            'mobile'              =>  'Mobile',
            'mobile_number'       =>  'Mobile Number',
            'work_phone'          =>  'Work Phone',
            'location'            =>  'Enter Areas, Cities and Buildings...',
            'cities_areas'        =>  'Areas and Cities...',
            'clear'               =>  'Clear filters',
            'clear_mobile'        =>  'Clear',
            'filters'             =>  'Filters',
            'min'                 => [
                'price'                 => 'Min Price',
                'bed'                   => 'Min Bed',
                'bath'                  => 'Min Bath',
                'area'                  => 'Min Area',
            ],
            'max'                 => [
                'price'                 => 'Max Price',
                'bed'                   => 'Max Bed',
                'bath'                  => 'Max Bath',
                'area'                  => 'Max Area',
            ],
            'type'                => 'Property Type',
            'furnishing'          => 'Furnishing',
            'areas'               =>  'All Areas',
            'brokerage'           =>  'All Brokerage',
            'country'             =>  'Country',
            'search_service'      =>  'Search for a service',
            'search_developer'    =>  'Search for Developer',
            'all_developer'       =>  'All Properties',
            'areas_buildings'     =>  'Areas and Buildings',
            'budget'              =>  'Budget',
            'completion_status'   =>  'Completion Status',
            'keyword'             =>  'Keyword'
        ],
        'button'              =>  [
            'reset'               =>  'Reset',
            'submit'              =>  'Submit',
            'call'                =>  'Call',
            'call_now'            =>  'Call Now',
            'call_agent'          =>  'Call Agent',
            'send'                =>  'Send',
            'contact'             =>  'Contact',
            'request_details'     =>  'Request Details',
            'request_call_back'   =>  'Request Callback',
            'request_call_free_back'   =>  'Request Free Callback',
            'lets_chat'           =>  'Lets Chat!',
            'create_alert'        =>  'Create Alert',
            'register'            =>  'Register',
            'reset_password'      =>  'Submit',
            'change_password'     =>  'Change Password',
            'update'              =>  'Update',
            'edit'                =>  'Edit',
            'cancel'              =>  'Cancel',
            'start_favourite'     =>  'Start search and favourite it',
            'set_alert'           =>  'Set Property Alert Now',
            'subscribe'           =>  'Subscribe',
            'new_search'          =>  'Start New Search',
            'phone'               =>  'Phone',

        ],
        'ideal_property' =>[
            'first_message'  =>  'Let us help you find your ideal property',
            'second_message'  =>  'Tell us what you need and we\'ll do the searching for you.'
        ],
        'general_enquiry' =>[
            'btn' => 'Enquire Now',
            'message'  =>  'Tell us what you\'re looking for and where and we\'ll contact 5 specialist agents on your behalf.'
        ]
    ],
    'grid'      =>  [
        'sort'      =>  [
            'title'       =>  'Sort Results',
            'low_to_high' =>  'low to high',
            'high_to_low' =>  'high to low',
            'price_low_to_high' =>  'Price (low)',
            'price_high_to_low' =>  'Price (high)',
            'beds_low_to_high' =>  'Beds(least)',
            'beds_high_to_low' =>  'Beds (most)',
            'area_low_to_high' =>  'Area (sqft) (least)',
            'area_high_to_low' =>  'Area (sqft) (most)',
            'time_low_to_high' =>  'Time (newest to oldest)',
            'time_high_to_low' =>  'Time (oldest to newest)',
            'newest_to_oldest' =>  'newest to oldest',
            'oldest_to_newest' =>  'oldest to newest',
            'title_mobile' => 'Sort',
            'save_mobile' =>  'Save'
        ],
        'view'      =>  [
            'map'          =>  'Map View',
            'list'         =>  'List View',
            'grid'         =>  'Grid View'
        ],
        'button'      =>  [
            'alert'        =>  'Alert me',
            'save'         =>  'Save Search'
        ],
        'search' => 'Search/Filter'
    ],
    'landing'   =>  [
        'message'          =>  'News & Articles',
        'message_mobile'   =>  'Quick property Search',
        'quick_access' => [
            'link_a' => 'Apartments for Rent under',
            'link_b' => 'Apartments for Sale under'
        ],
        'popular_property' =>  'Popular Property',
        'other' =>  'Other',
        'hot_project' =>  'Hot Project',
        'latest_video' =>  'Latest Videos',
        'view_all' =>  'View All',
        'view_less' =>  'View Less',
        'views_neighborhood' =>  'views neighbourhood',
        'searches' =>  'Searches'
    ],
    'mortgage'          =>  [
        'title'             =>  'Mortgage Calculator',
        'description'       =>  'If you are looking to own a property with the best mortgage solutions in Dubai or UAE, try using Zoom Property mortgage calculator. This mortgage calculator is designed to help property buyers in Dubai understand the monthly costs of a property mortgage and our portal also provides estimated transaction fees when buying a property in Dubai. It is a great tool to help determine monthly finance repayments as well as the additional fees which are required when buying a property with a mortgage in UAE.',
        'other_description' =>  'Calculations based on average cost per square feet for Apartments. This includes the monthly municipal tax. Use the editable fields to modify calculations appropriately.',
        'monthly_costs'     =>  'Monthly Costs',
        'monthly_total'     =>  'Monthly Total',
        'emi_title'         =>  'Approximate EMI is',
        'monthly'           =>  'month',
        'per_monthly'       =>  'per month',
        'leave_details_message'  =>  'Leave us with your details. One of our agents will get back to you shortly.',
        'categories'        =>  [
            'mortgage'          =>  'Mortgage',
            'energy'            =>  'Energy',
            'water'             =>  'Water'
        ],
        'calculations'      =>  [
            'message'              =>  'Edit the values above to adjust the monthly total',
            'purchase_price'       =>  'Purchase Price',
            'deposit'              =>  'Deposit (%)',
            'repayment_term'       =>  'Repayment Term (years)',
            'interest_rate'        =>  'Interest Rate (%)',
            'municipal_fees'       =>  'Municipal Fees',
            'avg_electricity_cost' =>  'Avg Electricity Cost',
            'avg_gas_cost'         =>  'Avg Gas Cost',
            'avg_water_cost'       =>  'Avg Water Cost',
            'down_payment'         =>  'Down Payment',
            'finance_amount'       =>  'Finance Amount',
            'term'                 =>  'Term (Years)',
            'interest'             =>  'Profit (% p.a.)',
            'loan_amount'          =>  'Loan Amount',
            'purchase_price'       =>  'Purchase Price',
            'total_amount'         =>  'Total Amount Paid',
            'total_interest'       =>  'Total Profit',
        ],
        'buy_only'          =>  'Buy this property from only',
        'ma_powered_by'     =>  'Mortgage calculator powered by',
        'subject_approval'  =>  '*These figures are indicative & subject to change on final approval',
        'powered_by'        =>  'Powered by',
        'get_finance'       =>  'Get Home Finance Now',
        'estimated_mortgage'=>  'Estimated Mortgage',
    ],
    'landmarks'       =>  [
        'mosque'          =>  'Mosque',
        'hospital'        =>  'Hospital',
        'school'          =>  'School',
        'gym'             =>  'Gym',
        'bank'            =>  'Bank',
        'shopping_mall'   =>  'Shopping Mall',
        'parking'         =>  'Parking',
        'park'            =>  'Park',
        'gas_station'     =>  'Gas Station'
    ],
    'modals'    =>  [
        'hints'     =>  [
            'name'           =>  'First Name',
            'last_name'      =>  'Last Name',
            'full_name'      =>  'Your Name',
            'email'          =>  'Your Email',
            'phone'          =>  'Your Phone',
            'country'        =>  'Country',
            'request_call_back' => 'Request call back on:',
            'latest_info' => 'Please send me latest property information and news'
        ],
        'agent'     =>  [
            'call'      =>  [
                'message'   =>  'You will be speaking with',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'call_back' =>  [
                'message'   =>  'Hi! I am interested in one of your listed properties.',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'email'     =>   [
                'message'   =>  'Hi! I am interested in one of your listed properties.',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp'     =>   [
                'message'   =>  'Hi! I am interested in one of your listed properties.',
                'payload_msg' =>  'Hi, I found your profile on Zoom Property. Please contact me. Thank you.'
            ],
            'review'     =>   [
                'message'   =>  'Write your comments here...',
            ],
        ],
        'consumer'    =>  [
            'alert'       =>  [
                'title'       =>  'Let us help you find your ideal property',
                'subtitle'    =>  'Tell us what you need and we\'ll do the searching for you.'
            ]
        ],
        'developer'   =>  [
            'call'        =>  [
                'message'   =>  'Contact number of the developer'
            ]
        ],
        'property'    =>  [
            'alert'       =>  [
                'title'       =>  'Create Property Alert',
                'subtitle'    =>  'Set up an email alert and be the first to know about the latest properties!'
            ],
            'call'      =>  [
                'message'     =>  'You can contact',
                'ref_message' =>  'Remember to mention the reference number',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.',
                'catch_msg'   =>  'To get a <span>FREE</span> call back from the agent please provide your details below.'
            ],
            'call_back' =>  [
                'message'     =>  'We speak English, Arabic, Hindi, Spanish and 72 other languages',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ],
            'call_me' =>  [
                'message'     =>  'Hi, I found your property with ref: :reference on zoom property. Please call me back. Thank you.'
            ],
            'email' =>  [
                'message'     =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp' =>  [
                'message'     =>  'Hi, I saw your advert on Zoom Property, ref: :reference, please get back in touch',
                'payload_msg' =>  'Hi, I found your property with ref: :reference on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'project'    =>  [
            'call'      =>  [
                'message'     =>  'You can contact',
                'ref_message' =>  'Contact number for the developer',
                'payload_msg' =>  'Hi, I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'call_back' =>  [
                'message'     =>  'We speak English, Arabic, Hindi, Spanish and 72 other languages',
                'payload_msg' =>  'Hi, I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'email' =>  [
                'message'     =>  'Hi, I would like to know more about this project.',
                'payload_msg' =>  'I found your Project :name on Zoom Property. Please contact me. Thank you.'
            ],
            'whatsapp' =>  [
                'message'     =>  'Hi, I saw your advert on Zoom Property, project: :name, please get back in touch',
                'payload_msg' =>  'Hi, I found your project :name on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'service'     =>  [
            'contact'     =>  [
                'message'     =>  'Hi, I found your services on Zoom Property. Please contact me. Thank you.'
            ]
        ],
        'preference'     =>  [
            'contact'     =>  [
                'message'     =>  'Hi, I am looking for a Property in :location. Please contact me. Thank you.'
            ]
        ],
        'signin_register'  => [
            'title'       =>  'Register with us',
            'subtitle'    =>  'Get a Personalised Experience',
            'signin'      =>  'Sign In',
            'forgot_password' =>  'Forgot password?',
            'account'     =>  'Don\'t have an account?',
            'already_registered'  =>  'Already Registered?',
            'signin_here'  =>  'Sign in here',
            'register_here'   =>  'Register Here',
            'register'    =>  'Register',
            'facebook'    =>  'Facebook',
            'google'    =>  'Google',
            'signin_facebook'    =>  'Sign in with Facebook',
            'signin_google'      =>  'Sign in with Google',
            'register_facebook' =>  'register with Facebook',
            'register_google'   =>  'register with Google',
            'agree_terms' =>  'By Registering with us, you agree to our',
            'agree_terms_signin' =>  'By Signing with us, you agree to our',
            'privacy_policy'   =>  'Privacy Policy',
            'terms_conditions' =>  'Terms & Conditions',
            'go_back_signin'    =>  'Go back to sign in',
            'promotions'  =>  [
                'update_profile'  =>  'Update your profile & preferences',
                'get_alerts'      =>  'Get Email Alerts for New Properties',
                'save_favourites' =>  'Save your Favourite Properties',
                'save_search'     =>  'Save your Searches'
            ]
        ]
    ],
    'sections'  =>  [
        'banner'    =>  [
            'agent'   =>  [
                'title'   =>  'Be in safe hands'
            ],
            'budget'   =>  [
                'title'      =>  'Let us know your budget',
                'subtitle'   =>  'We will pick the best property for you from the best location'
            ],
            'service'   =>  [
                'title'      =>  'Help yourself move in'
            ],
            'landing'   =>  [
                'title'      =>  'Discover more',
                'subtitle'   =>  'Find your perfect property'
            ]
        ],
        'chat'  =>  [
            'title' =>  'You are chatting with Agent'
        ],
        'dashboard'  =>  [
            'profile'               =>  'Profile',
            'favourite_searches'    =>  'Favourite Searches',
            'favourite_properties'  =>  'Favourite Properties',
            'alerts'                =>  'Property Alerts',
            'email_chat'            =>  'Emails & Chats',
            'settings'              =>  'Settings',
            'sign_out'              =>  'Sign Out',
            'not_favourite_search'  =>  'You dont have any favourite searches',
            'not_favourite_property'=>  'You dont have any favourite properties',
            'not_alerts'            =>  'You dont have any property alert',
            'not_chats'             =>  'Your chat history is empty',
            'search_again'          =>  'Search Again',
            'change_password'       =>  'Change your Password',
            'subscribe'             =>  'Subscribe to our newsletters'
        ]
    ],
    'property'  =>  [
        'details'   =>  [
            'similar_properties' =>  'Similar Properties',
            'ref_no_title'        =>  'Reference No',
            'no_building'         =>  'Some building',
            'no_area'             =>  'Some area',
            'no_city'             =>  'Some city',
            'play'                =>  'Play',
            'view'                =>  'View',
            'tab_titles'          =>  [
                'gallery'             =>  'Gallery',
                'agent_details'       =>  'Agent Details',
                'property_details'    =>  'Property Details',
                'map'                 =>  'Map and Nearby',
                'video'               =>  'Video',
                'video_degree'        =>  '360',
                'local_info'          =>  'Local Info'
            ],
            'not_location_provided' =>  ':agency_name has not provided a location for this property',
            'amenities_title'   =>  'Property Amenities',
            'description'       =>  'Apartment description',
            'agent_card'          =>  [
                'marketed_by'         =>  'Marketed By',
                'name_title'          =>  'Name',
                'license_title'       =>  'License',
                'agency_name_title'   =>  'Agency',
                'view_all'            =>  'View all properties by',
                'view_profile'            =>  'View Profile',
                'play_now'            =>  'Play Now',
            ],
            'view_more'          =>  'View more',
            'view_less'          =>  'View less',
            'stats'         =>  [
                'area_stats'     =>  'Area Stats',
                'avg_floor'      =>  'Avg. Floor Space',
                'decreased'      =>  'decreased in last',
                'increased'      =>  'increased in last',
                'months'         =>  'months',
                'price'         =>  'Price',
                'trends'         =>  'trends',
                'by_property'   =>  'by property type'
            ],
            'expert_review'   =>  'Expert Review',
            'cheaper_area'   =>  'cheaper than similar listing in the area',
            'cheaper_bed'   =>  'cheaper than listing with the same no. of beds',
        ],
        'listings'  =>  [
            'properties_found'   =>  'Properties Found',
            'properties_no_found_title'   =>  'We couldn\'t find any properties for your specific search.',
            'properties_no_found_subtitle'   =>  'Perhaps these would interest you instead?'
        ]
    ],
    'agent'     =>  [
        'details'   =>  [
            'area_specialist'   =>  'Area Specialist',
            'top_agents'        =>  'Top Agents',
            'nationality'       =>  'Nationality',
            'languages'         =>  'Languages',
            'company'           =>  'Company',
            'license'           =>  'License No',
            'properties_by'     =>  'Properties by',
            'listed_property'   =>  'Listed Properties',
            'featured_agent'    =>  'Featured Agent',
            'contact_agent'     =>  'Contact this agent',
            'about_me'          =>  'About me',
            'reviews'           =>  'Reviews',
            'reviews_me'        =>  'Reviews Me',
            'out_of'            =>  'out of',
            'featured_review'   =>  'Featured Review',
            'load_more'         =>  'Load More',
            'load_less'         =>  'Load Less',
            'agent_title'       =>  'Rate this agent',
            'my_properties'     =>  'My properties',
            'areas_covered'     =>  'Areas Covered',
            'specialty'         =>  'Specialty',
            'find_out_more'     =>  'Find out More',
            'agent_found'       =>  '{0} Agents Found|{1} Agent Found|[2,*] Agents Found',
            'rate'   =>  [
                'excelent'   =>  'Excellent',
                'good'       =>  'Good',
                'regular'    =>  'Regular',
                'bad'        =>  'Bad',
                'very_bad'   =>  'Very Bad',
            ]
        ]
    ],
    'budget'    =>  [
        'refine_search' =>  'Refine Search',
        'seo_title'     =>  'Budget Search of properties for :rent_buy: under AED :price_max:'
    ],
    'project'   =>  [
        'starting_from'  => 'Starting From',
        'details'   =>  [
            'by'  => 'by',
            'developed_by'  => 'Developed by',
            'contact'       => 'Contact',
            'location'      => 'Location',
            'view_all'      => 'View all latest properties from',
            'about'         => 'Description',
            'development_progress' => 'Development Progress',
            'expected_completion_date' => 'Delivery Date',
            'status' => 'Status',
            'mortgage_deposit' => 'Mortgage Deposit From',
            'progress' => [
                'foundations'   =>  'Foundations',
                'parking'       =>  'Parking',
                'building'      =>  'Building',
                'completion'    =>  'Completion'
            ],
            'features_title'  =>  'Features',
            'tab_titles'          =>  [
                'gallery'             =>  'Image Gallery',
                'floor_plans'         =>  'Floor Plans',
                'completion_status'   =>  'Completion Status',
                '3d_tour'             =>  '3D Tour',
                'details'             =>  'Details',
                'email'               =>  'Email',
                'map'                 =>  'Map',
                'about'               => 'About this project'
            ],
            'payment_plan'    =>  'Payment Plan',
            'view_brochure'    =>  'View Brochure',
            'download'    =>  'Download',
            'brochure'    =>  'Brochure',
            'read_more'    =>  'Read More',
            'read_less'    =>  'Read Less'
        ],
        'listings'  =>  [
            'new_projects'    =>  '{1} New Project Found |[2,*] New Projects Found |{Many} New Projects Found ',
            'move_in'         =>  'Ready to Move in'
        ]
    ],
    'services'    =>  [
        'listings'      =>   [
            'results_found'       =>   '{0} Services Found|{1} Service Found|[2,*] Services Found|{Many} Services Found',
            'default_description' =>   'We will, We will, Rock you!'
        ]
    ],
    'toast'    =>  [
        'error_default' =>   'Something went wrong, Please try later',
        'error_phone' =>   'Please enter a valid phone number.',
        'error_email' =>   'Please enter a valid email address to reset your password.',
        'error_email_b' =>   'Enter a valid Email address',
        'success_request_sent'       =>   'Your Request has been sent! someone will get in touch soon.',
        'success_alert_created' =>   'Alert Created successfully!',
        'success_password_changed' =>   'You password has been changed successfully!',
        'success_newsletter_subscribed' =>   'You have successfully subscribed to our newsletters.',
        'success_search_saved' =>   'You have successfully saved this search',
        'success_signed_in' =>   'You have signed In successfully',
        'success_registered' =>   'You have registered and signed In successfully',
        'success_email_sent' =>   'An email has been sent to your registered email',
        'showing_results' =>   'Showing Results from '
    ],
    'results'   =>  [
        'results_found'             =>  'Results Found',
        'no_results_found'          =>  'No Results Found',
        'no_results_found_title'    =>  'We couldn\'t find any results for your specific search.',
        'no_results_found_subtitle' =>  'Perhaps these would interest you instead?',
        'not_specified'             =>  'Not Specified'
    ],
    'currencies' => [
        [
            'label' => 'Arab Emirates Dirham',
            'code' => 'AED',
            'value' => '1',
            'flag' => 'ae'
        ],
        [
            'label' => 'United States Dollars',
            'code' => 'USD',
            'value' => '0.2723',
            'flag' => 'us'
        ],
        [
            'label' => 'Australian Dollars',
            'code' => 'AUD',
            'value' => '0.4036',
            'flag' => 'au'
        ],
        [
            'label' => 'Canadian Dollars',
            'code' => 'CAD',
            'value' => '0.3622',
            'flag' => 'ca'
        ],
        [
            'label' => 'British Pound Sterling',
            'code' => 'GBP',
            'value' => '0.2228',
            'flag' => 'vg'
        ],
        [
            'label' => 'Euro',
            'code' => 'EUR',
            'value' => '0.2456',
            'flag' => 'eu'
        ],
        [
            'label' => 'Chinese Yuan',
            'code' => 'RMB',
            'value' => '1.9501',
            'flag' => 'cn'
        ],
        [
            'label' => 'Saudi Arabian Riyal',
            'code' => 'SAR',
            'value' => '1.0212',
            'flag' => 'sa'
        ],
        [
            'label' => 'Indian Rupees',
            'code' => 'INR',
            'value' => '19.5433',
            'flag' => 'in'
        ],
        [
            'label' => 'Pakistan Rupees',
            'code' => 'PKR',
            'value' => '42.8917',
            'flag' => 'pk'
        ]
    ],
    'guideme'   => [
        'property_area' => 'Properties in Your Area',
        'menu_label' => 'Guide Me',
        'finding_location' => 'Finding your location ...',
        'enter_location' => 'Enter a Location',
        'view_map' => 'View Map',
        'enable_location' => 'Please Enable Your Location',
        'you_are_in' => 'You are in ',
        'no_results' => 'No results found',
        'no_results_area_increased' => 'Sorry there were no results for your selection so we’ve increased the search area',
        'showing_stats_for' => 'Showing you area stats for',
        'no_stats' => 'Area statistics are not available',
        'apartment' => 'Apartment',
        'loft_apartment' => 'Loft Apartment',
        'hotel_apartment' => 'Hotel Apartment',
        'flat_apartment' => 'Apartment',
        'whole_building' => 'Whole Building',
        'land' => 'Land',
        'full_floor' => 'Full Floor',
        'office_space' => 'Office Space',
        'office' => 'Office',
        'labor_camp' => 'Labor Camp',
        'villa' => 'Villa',
        'plot' => 'Plot',
        'bulk_units' => 'Bulk Units',
        'bungalow' => 'Bungalow',
        'compound' => 'Compound',
        'duplex' => 'Duplex',
        'half_floor' => 'Half Floor',
        'townhouse' => 'Townhouse',
        'retail' => 'Retail',
        'shop' => 'Shop',
        'show_room' => 'Show Room',
        'staff_accommodation' => 'Staff Accomomdation',
        'warehouse' => 'Warehouse',
        'factory' => 'Factory',
        'business_center' => 'Business Center',
        'penthouse' => 'Penthouse',
        'graph_label_a' => 'Month',
        'graph_label_b' => 'Price',
        'default_location' => 'Trade Center',
        'price' => 'Price',
        'average' => 'Avg.',
        'sqft' => 'Sqft',
        'area' => 'Area',
        'sale' => 'Sale',
        'rent' => 'Rent'
      ]
];
