<!doctype html>
@php
	$selectedLanguage	=	\App::getLocale();
	$search		=	include( resource_path( 'lang/'.$selectedLanguage.'/search.php' ) );
	$lang		=	include( resource_path( 'lang/'.$selectedLanguage.'/data.php' ) );
	$lang_static =	include( resource_path( 'lang/'.$selectedLanguage.'/page.php' ) );

	$keywords   =   include( storage_path( 'search/keywords.php' ) );
	$keywords	=	collect($keywords[ 'keywords' ]);
	$keywords	=	$keywords->pluck('value')->toArray();
	if(!empty($keywords)){
		array_set($search,'keywords',$keywords);
	}
	$search_data    =   include( storage_path( 'search/search.php' ) );
	if(!empty($search_data[ 'property' ][ 'types' ])){
		$types = array_map('kebab_case_custom', array_values(array_unique(array_flatten($search_data[ 'property' ][ 'types' ]))));
		array_set($search,'property.types',$types);
	}
@endphp

<html lang="en" style="visibility: visible; opacity: 1" dir="{{ ( $selectedLanguage === 'ar' ) ? 'rtl' : 'ltr' }}" class="{{ ( $selectedLanguage === 'ar' ) ? 'fa-dir-flip' : '' }}">
<head>
	@include( 'seo.header' )
	@yield( 'dataLayer' )
	@include( 'marketing.header' )
	{{-- <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans" rel="stylesheet" /> --}}
	@if( in_array( $selectedLanguage, [ 'ar' ] ) )
		<link href="{{ cdn_asset('semantic/dist/semantic.rtl.min.css') }}" rel="stylesheet" />
		<link href="{{ cdn_asset('font-awesome/font-awesome.rtl.css') }}" rel="stylesheet" />
	@else
		<link href="{{ cdn_asset('semantic/dist/semantic.min.css') }}" rel="stylesheet" />
	@endif
	<link href="{{ asset('assets/dependencies/slider.min.css') }}?v=19" rel="stylesheet" />
	<link href="{{ asset('assets/dependencies/intlTelInput.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/dependencies/magnific-popup.css') }}" rel="stylesheet" />
	<link rel="shortcut icon" href="{{ cdn_asset( 'assets/favicon.ico' ) }}" />
	@notmobile
		@if( in_array( $selectedLanguage, [ 'ar' ] ) )
		<link rel="stylesheet" href="{{ env('ASSET_URL').'assets/css/main.rtl.min.css' }}" />
		@else
		<link rel="stylesheet" href="{{ env('ASSET_URL').'assets/css/main.min.css' }}" />
		@endif
	@elsenotmobile
		@if( in_array( $selectedLanguage, [ 'ar' ] ) )
		<link rel="stylesheet" href="{{ env('ASSET_URL').'assets/css/_main.rtl.min.css' }}" />
		@else
		<link rel="stylesheet" href="{{ env('ASSET_URL').'assets/css/_main.min.css' }}" />
		@endif
	@endnotmobile
</head>

<body class="dimmable pushable" data-base-url="{{ env( 'APP_URL' ) }}" data-env-url="{{ env( 'APP_URL' ) }}">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ env( "APP_ENV" ) == 'production' ? env( "DATA_LAYER_ID_PRODUCTION" ) : env( "DATA_LAYER_ID_STAGING" ) }}"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="toast" style="display: none;"></div>
	@yield( 'content' )
	<div style="display:none">
		@yield( 'templates' )
	</div>
		<!-- Global variables -->
		<script>
			var searchJson 			=	{!! json_encode($search) !!},
				lang 				=	{!! json_encode($lang) !!},
				lang_static 		=	{!! json_encode($lang_static) !!},
				selectedLanguage 	=	{!! json_encode($selectedLanguage) !!},
				baseUrl 			=	'{{ env( "APP_URL" ) . \App::getLocale() . '/' }}',
				envUrl 				=	'{{ env( "APP_URL" ) }}',
				baseApiUrl 			=	'{{ env( "APP_URL" ) }}api/{{ env( "API_VERSION" ) }}/',
				cdnUrl 				=	'{{ env( "CDN_URL" ) }}',
				cdnImgVersion 	=	'{{ env( "CDN_IMG_VERSION" ) }}',
				page 				=	undefined,
				locations       	=   [];
			let config 				=	{}
		</script>
		@notmobile
			<script src="{{ env('ASSET_URL').'js/skeleton.min.js' }}"></script>
		@elsenotmobile
			<script src="{{ env('ASSET_URL').'js/_skeleton.min.js' }}"></script>
		@endnotmobile
		@yield( 'modals' )
		<!-- Global includes -->
		@notmobile
			@include( 'components.modals.signin_register.desktop_and_tablet' )
			@include( 'components.modals.property.alert.desktop_and_tablet' )
			@include( 'components.sections.dashboard.dashboard' )
			@include( 'components.sections.chat.desktop_and_tablet' )
		@elsenotmobile
			@include( 'components.sections.sidedrawer' )
			@include( 'components.sections.dashboard.dashboard' )
			@include( 'components.modals.signin_register.mobile' )
			@include( 'components.modals.property.alert.mobile' )
			@include( 'components.modals.general_enquiry.mobile' )
		@endnotmobile

	@desktop
		@include( 'components.modals.ideal_property.desktop_and_tablet' )
	@enddesktop

@yield( 'scripts' )

<script>
    $(document).ready(function(){
        $('.ui.pointing.dropdown').dropdown();
    });
</script>

</body>
</html>
