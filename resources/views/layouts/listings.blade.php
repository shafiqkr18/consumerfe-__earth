{{-- Base Template  --}}
@extends( 'wrappers.global' )

@section( 'content' )

    @yield( 'header' )

    @yield( 'search' )

    @yield( 'breadcrumbs' )

    <div id="listings_wrapper">
        @yield( 'page_content' )
    </div>

    @yield( 'footer' )

@endsection

@section( 'templates' )
    @yield( 'page_templates' )
@endsection

@section( 'modals' )
    @yield( 'page_modals' )
@endsection

@section( 'scripts' )
    @yield( 'page_scripts' )
@endsection
