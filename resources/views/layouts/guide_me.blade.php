@extends( 'layouts.landing' )

@section( 'title', __( 'page.global.seo_title' ) )

@section( 'description', __( 'page.global.seo_description' ) )

<link rel="stylesheet" href="{{ env('ASSET_URL').'font-map-icons/map-icons.min.css' }}">

@section( 'page_content' )

    @yield( 'guide_me' )

    @include( 'components.cards.property.guide_me' )

@endsection

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbjLiOgWkHgDFvd0bmAsnbHsf1x7wfpmM&libraries=places"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>

@section( 'page_scripts' )
<script>
$( document ).ready( function(){
    guide_me.__init();
});
</script>
@endsection
