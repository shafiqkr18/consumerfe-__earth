{{-- Base Template  --}}
@extends( 'wrappers.global' )

@section( 'content' )

    @yield( 'header' )

    @yield( 'banner' )

    @yield( 'page_content' )

    @yield( 'footer' )

@endsection

@section( 'templates' )
    @yield( 'page_templates' )
@endsection

@section( 'modals' )
    @yield( 'page_modals' )
@endsection

@section( 'scripts' )
    @yield( 'page_scripts' )
@endsection
