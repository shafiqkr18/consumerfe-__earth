<script>
    var dataLayer = [];
      dataLayer.push({
          'website_section'           :   'Agent Finder',
          'pagetype'                  :   'Search',
          'language' 				  :	  '{{\App::getLocale()}}',
          'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
          'device_type'               :   @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,
          'agency_id'                 :   '{{array_get($segments,3,'')}}',
          'areas'                     :   '{{str_before(array_get($segments,4,''), '-area')}}',
          'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
      });
</script>
