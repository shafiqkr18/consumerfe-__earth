<script>
    var dataLayer = [];
      dataLayer.push({
          'website_section'           :   'Agent Finder',
          'pagetype'                  :   'Community',
          'language' 				          :	  '{{\App::getLocale()}}',
          'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
          'device_type'               :   @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,

          'agency_id'                 :   '{{array_get($data,'agency._id','')}}',
          'agency_name'               :   '{{array_get($data,'agency.contact.name','')}}',
          'agent_id'                  :   '{{array_get($data,'_id','')}}',
          'agent_name'                :   '{{array_get($data,'contact.name','')}}',

          'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
      });
</script>
