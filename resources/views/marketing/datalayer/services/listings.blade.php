<script>
    var dataLayer = [];
    dataLayer.push({
        'website_section'           :   'Services',
        'pagetype'                  :   'Search',
        'language' 				    :	  '{{\App::getLocale()}}',
        'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
        'device_type'               :   @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,
        'service'                   :   '{{$service}}',
        'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
    });
</script>
