<script>
    var dataLayer = [];

    dataLayer.push({
        'website_section'           :   'Projects',
        'pagetype'                  :   'Product',
        'language' 				    :	 '{{ \App::getLocale() }}',
        'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
        'device_type'               :    @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,

        'loc_city_name'             :   '{{ array_get($data,'city','') }}',
        'loc_neighbourhood_name'    :   '{{ array_get($data,'area','') }}',
        'loc_breadcrumb'            :   '{{ snake_case_custom(strtolower(array_get($data,'city',''))) }};{{ snake_case_custom(strtolower(array_get($data,'area',''))) }}',
        'loc_city_id'               :   '{{ snake_case_custom(strtolower(array_get($data,'city',''))) }}',
        'loc_neighbourhood_id'      :   '{{ snake_case_custom(strtolower(array_get($data,'area',''))) }}',

        'project_name' 				:	'{{array_get($data,'name','')}}',
        'project_id' 				:	'{{array_get($data,'_id','')}}',
        'project_price'             :   '{{array_get($data,'pricing.starting')}}',
        'project_type'              :   '{{array_get($data,'unit.property_type')}}',
        'project_currency'          :   'AED',

        'developer_name'            :   '{{array_get($data,'developer.contact.name','')}}',
        'developer_id'              :   '{{array_get($data,'developer._id','')}}',

        'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
    });
</script>
