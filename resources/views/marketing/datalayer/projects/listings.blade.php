<script>
    var dataLayer = [];

    dataLayer.push({
      'website_section'           :   'Projects',
      'pagetype'                  :   'Search',
      'language' 				  :	   '{{ \App::getLocale() }}',
      'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
      'device_type'               :    @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,

      'developer_id'              :   '{{$query !== \App::getLocale() ? $query : ''}}',

      'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
    });
</script>
