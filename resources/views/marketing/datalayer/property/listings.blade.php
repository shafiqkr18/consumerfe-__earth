<script>

    var dataLayer = [];

    dataLayer.push({
        'website_section'           :       '{{ array_get($query, 'residential_commercial.0', 'Residential').' '.array_get($query, 'rent_buy.0', 'Sale') }}',
        'language' 				    :	    '{{ \App::getLocale() }}',
        'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
        'pagetype'                  :       'Search',
        'device_type'               :       @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,
        'furnishing'                :       '',

        'loc_city_name'             :       '{{implode(',',array_get( $query, 'city', []))}}',
        'loc_city_id'               :       '{{ snake_case_custom(strtolower(implode(',',array_get( $query, 'city', [])))) }}',
        'loc_neighbourhood_name'    :       '{{implode(',',array_get( $query, 'area', []))}}',
        'loc_neighbourhood_id'      :       '{{ snake_case_custom(strtolower(implode(',',array_get( $query, 'area', [])))) }}',
        'loc__name'                 :       '{{implode(',',array_get( $query, 'building', []))}}',
        'loc__id'                   :       '{{ snake_case_custom(strtolower(implode(',',array_get( $query, 'building', [])))) }}',
        'loc_breadcrumb'            :       '{{ snake_case_custom(strtolower(implode(',',array_get( $query, 'city', [])))) }};{{ snake_case_custom(strtolower(implode(',',array_get( $query, 'area', [])))) }};{{ snake_case_custom(strtolower(implode(',',array_get( $query, 'building', [])))) }}',

        'property_type'             :       '{{implode(',',array_get( $query, 'type', []))}}',
        'property_bed'              :       '{{array_get( $query, 'bedroom.min', 0).'-'.array_get( $query, 'bedroom.max', 0)}}',
        'property_area'             :       '{{array_get( $query, 'dimension.builtup_area.min', 0).'-'.array_get( $query, 'dimension.builtup_area.max', 0)}}',
        'property_price'            :       '{{array_get( $query, 'price.min', 0) == 2010 ? '0-'.array_get( $query, 'price.max', 0) : array_get( $query, 'price.min', 0).'-'.array_get( $query, 'price.max', 0)}}',
        'property_bath'             :       '{{array_get( $query, 'bathroom.min', 0).'-'.array_get( $query, 'bathroom.max', 0)}}',
        'property_furnishing'       :       '',
        'property_currency'         :       'AED',

        'user_status'               :       '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
    });

</script>
