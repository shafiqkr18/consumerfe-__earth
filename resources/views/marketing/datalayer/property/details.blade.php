<script>

    var dataLayer = [];

    dataLayer.push({
        'website_section'           :   '{{ array_get($data,'rent_buy','') }}',
        'pagetype'                  :   'Product',
        'language' 				    :	'{{ \App::getLocale() }}',
        'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
        'device_type'               :    @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,

        'loc_city_name'             :   '{{ array_get($data,'city','') }}',
        'loc_neighbourhood_name'    :   '{{ array_get($data,'area','') }}',
        'loc_name'                  :   '{{ array_get($data,'building','') }}',
        'loc_breadcrumb'            :   '{{ snake_case_custom(strtolower(array_get($data,'city',''))) }};{{ snake_case_custom(strtolower(array_get($data,'area',''))) }};{{ snake_case_custom(strtolower(array_get($data,'building',''))) }};',
        'loc_city_id'               :   '{{ snake_case_custom(strtolower(array_get($data,'city',''))) }}',
        'loc_neighbourhood_id'      :   '{{ snake_case_custom(strtolower(array_get($data,'area',''))) }}',
        'loc_id'                    :   '{{ snake_case_custom(strtolower(array_get($data,'building',''))) }}',

        'property_id'               :   '{{ array_get($data,'_id','') }}',
        'property_furnishing'       :   '{{ array_get($data,'type','') }}',
        'property_type'             :   '{{ array_get($data,'furnished','') == 0 ? 'Unfurnished' : array_get($data,'furnished','') == 1 ? 'Furnished' : 'Partly-furnished'}}',
        'property_bed'              :   '{{ array_get($data,'bedroom','') }}',
        'property_area'             :   '{{ array_get($data,'dimension.builtup_area','') }}',
        'property_price'            :   '{{ array_get($data,'price','') }}',
        'property_bath'             :   '{{ array_get($data,'bathroom','') }}',
        'property_currency'         :   'AED',

        'agent_name'                :   '{{ array_get($data,'agent.contact.name','') }}',
        'agent_id'                  :   '{{ array_get($data,'agent._id','') }}',
        'agency_id'                 :   '{{ array_get($data,'agent.agency._id','') }}',
        'agency_name'               :   '{{ array_get($data,'agent.agency.contact.name','') }}',

        'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
    });
</script>
