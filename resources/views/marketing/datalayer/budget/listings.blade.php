<script>
    var dataLayer = [];
    dataLayer.push({
        'website_section'           :   'Services',
        'pagetype'                  :   'Search',
        'language' 				    :	  '{{\App::getLocale()}}',
        'ipAddressZP' 				:	'{{\Session::has('zp-client-ip') ? \Session::get('zp-client-ip') : 'UNKNOWN' }}',
        'device_type'               :   @notmobile'desktop'@elsenotmobile'mobile'@endnotmobile,

        'loc_city_name'             :   '{{ array_get($query,'emirate','') }}',
        'loc_city_id'               :   '{{ snake_case_custom(strtolower(array_get($query,'emirate',''))) }}',
        'property_price'            :   '{{array_get( $query, 'price_min', 0).'-'.array_get( $query, 'price_max', 0)}}',
        'property_rent_buy'         :   '{{array_get( $query, 'rent_buy', '')}}',

        'user_status'               :   '{{\Session::has('user') ? 'Logged In' :'Not Logged In' }}'
    });
</script>
