<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer',"{{ env( "APP_ENV" ) == 'production' ? env( "DATA_LAYER_ID_PRODUCTION" ) : env( "DATA_LAYER_ID_STAGING" ) }}");</script>
<!-- End Google Tag Manager -->

@if(false)
<!-- Google Adsense -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-6930549487394371",
        enable_page_level_ads: true
    });
</script>
<!-- End of Google Adsense -->
@endif
