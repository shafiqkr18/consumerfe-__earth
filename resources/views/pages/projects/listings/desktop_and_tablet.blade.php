<div class="ui container" id="pages_projects_listings">
    <div class="ui grid">
        <div class="eleven wide column">
            <!-- Title & Info -->
            <div class="sixteen wide column">
                <h1 class="listings_total" data-listings_total="{{array_get($data,'total','')}}">{{ $seo_h1 }}</h1>
                @if(array_get($data,'total', 0) === 0)
                <div class="ui grid">
                    <div class="eleven wide column no_results">
                        <div class="ui card fluid grid">
                            <div class="ui grid">
                                <div class="twelve wide column">
                                    {{ __( 'page.results.no_results_found_title' ) }} <br />
                                    {{ __( 'page.results.no_results_found_subtitle' ) }}
                                </div>
                                <div class="four wide column">
                                    <button type="button" id="new_search" class="small ui purple button new_search" data-page="project" data-subpage="{{ strtolower($country) }}" onclick="search.new_search(this)"> {{ __( 'page.form.button.new_search' ) }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <!-- Title & Info -->

            <!-- Sorting and Views -->
            <div class="sixteen wide column">
                <div class="ui floating labeled icon dropdown button small ui purple purple" id="listings_sort_btn">
                    <i class="sort icon"></i>
                    <span class="text">{{ __( 'page.grid.sort.title' ) }}</span>
                    <div class="menu">
                        <div class="header"><i class="dollar sign icon"></i>{{ __( 'page.global.price' ) }} ({{ __( 'page.global.aed' ) }})</div> 
                        <div class="item"><i data-sort="--price-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.low_to_high' ) }}</div>
                        <div class="item"><i data-sort="--price-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.high_to_low' ) }}</div>
                    </div>
                </div>
                <button type="button" id="projects_map_view" class="small ui purple button projects_map_view" onclick="pages.projects.listings.map.__init()"><i class="map icon"></i> {{ __( 'page.grid.view.map' ) }}</button>
                <button type="button" id="projects_grid_view" class="small ui purple button projects_grid_view" onclick="pages.projects.listings.grid.__init()"><i class="th icon"></i> {{ __( 'page.grid.view.grid' ) }}</button>
                <button type="button" id="projects_list_view" class="small ui purple button projects_list_view" onclick="pages.projects.listings.list.__init()"><i class="list icon"></i> {{ __( 'page.grid.view.list' ) }}</button>
            </div>
            <!-- Sorting and Views -->

            <!-- Project Cards -->
            <div class="sixteen wide column" id="projects_card_grid">
                <div class="ui link two cards">
                    @foreach(array_get($data,'data',[]) as $key => $project)
                        @include( 'components.cards.projects' )
                    @endforeach
                </div>
            </div>
            <!-- Project Cards -->
        </div>
        <!-- Ad -->
        <div class="five wide column">
            <div class="ui sticky map">
                <div id="projects_listings_map"></div>
            </div>
            <div class="projects_listings_ad">
                <!-- Property listings page banner [async] -->
                <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                <script type="text/javascript">
                var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                var abkw = window.abkw || '';
                var plc317495 = window.plc317495 || 0;
                document.write('<'+'div id="placement_317495_'+plc317495+'"></'+'div>');
                AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 317495, [300,600], 'placement_317495_'+opt.place, opt); }, opt: { place: plc317495++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                </script>
            </div>
            {{-- General enquiry --}}
            <div class="ui sticky enquiry_project">
                @include( 'components.modals.developer.enquiry.desktop_and_tablet' )
            </div>
        </div>
        <!-- Ad -->
    </div>

    <!-- Pagination -->
    <div class="ui grid">
        <div class="eleven wide column">
            <ul id="pagination"></ul>
        </div>
    </div>
    <!-- Pagination -->

</div>
