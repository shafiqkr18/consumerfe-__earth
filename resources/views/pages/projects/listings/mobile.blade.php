<div class="ui container" id="pages_projects_listings">

    <div class="ui grid">
        <div class="sixteen wide column">
            <h1 class="listings_total" data-listings_total="{{array_get($data,'total','')}}">{{ $seo_h1 }}</h1>
            @if(array_get($data,'total', 0) === 0)
            <div class="no_results">
                <div class="ui card fluid grid">
                    <div class="ui grid">
                        <div class="sixteen wide column">
                            {{ __( 'page.results.no_results_found_title' ) }}<br />
                            {{ __( 'page.results.no_results_found_subtitle' ) }}
                        </div>
                        <div class="sixteen wide column">
                            <button type="button" id="new_search" class="small ui purple button new_search"  data-page="project" data-country="{{ strtolower($country) }}" onclick="search.new_search(this)"> {{ __( 'page.form.button.new_search' ) }}</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="sixteen wide column" id="projects_card_grid">
            <div class="ui link one cards">
                @foreach(array_get($data,'data',[]) as $key => $project)
                    @include( 'components.cards.projects' )
                @endforeach
            </div>
        </div>
    </div>

    <div class="ui grid">
        <div class="sixteen wide column mobile_paginate">
            <ul id="pagination"></ul>
        </div>
    </div>

</div>
