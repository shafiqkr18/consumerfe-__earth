@extends( 'layouts.details' )

@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.projects.details')
@endsection

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    @endnotmobile
@endsection

@section( 'search' )
@endsection

@section( 'breadcrumbs' )
     <!-- @notmobile
        @include( 'components.sections.breadcrumbs.projects.details' )
    @elsenotmobile
        @include( 'components.sections.breadcrumbs.projects.details' )
    @endnotmobile -->
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.projects.details.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.projects.details.mobile' )
    @endnotmobile
@endsection

@section( 'page_modals' )

    @notmobile
        @include( 'components.modals.custom_enquiry.desktop_and_tablet' )
        @include( 'components.modals.developer.call.desktop_and_tablet' )
        @include( 'components.modals.developer.email.desktop_and_tablet' )
        @include( 'components.modals.property.mortgage.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.modals.custom_enquiry.mobile' )
        @include( 'components.modals.developer.call.desktop_and_tablet' )
        @include( 'components.modals.developer.email.desktop_and_tablet' )
        @include( 'components.modals.developer.callback.mobile' )
        @include( 'components.modals.property.mortgage.desktop_and_tablet' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFzsV4eOaewWg9ShwUkNSLhv6KNtKAjAU&libraries=places"></script>

{{-- Setting form fields --}}
<script>
$( document ).ready( function(){
    @notmobile
        pages.projects.details.__init('{{strtolower($country)}}');
    @elsenotmobile
        pages.projects.details.__init('{{strtolower($country)}}');
    @endnotmobile
    validate.rules.lead.project.email.__init();
});
</script>
@endsection
