@php
    $developer_logo =   array_get( explode('@', array_get($data,'developer.contact.email','@')), '1', '');
    $project_logo   = array_get($data,'image.logo');
    $number         =   array_get($data,'developer.settings.lead.call_tracking.track',false) ? array_get($data,'developer.settings.lead.call_tracking.phone') : array_get($data,'developer.contact.phone');
    if($number[0] == 0){
        $number = substr($number,1);
    }
    $number         =   str_replace('971971','971','+971'.$number);
    array_set($data,'number',$number);
    $images         =   (array_has($data, 'image.portal.desktop') && !empty(array_get($data, 'image.portal.desktop',[]))) ? array_get($data, 'image.portal.desktop',[]) : array_get($data, 'image.gallery',[]);
    $images         =   empty($images) ? [cdn_asset('assets/img/default/no-agent-img.png')] : $images;

    $images_mobile  =   (array_has($data, 'image.portal.mobile') && !empty(array_get($data, 'image.portal.mobile',[]))) ? array_get($data, 'image.portal.mobile',[]) : array_get($data, 'image.gallery',[]);
    $images_mobile  =   empty($images_mobile) ? [cdn_asset('assets/img/default/no-agent-img.png')] : $images;
    $amenities      =   ($locale !== 'en' && count(array_get($data,'unit.amenities_'.$locale,[])) > 0 ) ? array_get($data,'unit.amenities_'.$locale) : array_get($data,'unit.amenities',[]);
    $payment_plans_locale   =  [];
    if($locale !== 'en' && array_has($data,'pricing.payment_plans_'.$locale,[]) ){
        $payment_plans_lang = array_get($data,'pricing.payment_plans_'.$locale,[]);
        foreach($payment_plans_lang as $key => $value){
            $payment_plans_locale[$key] =   str_replace('|','',$value);
        }
        $payment_plans_locale   =   array_filter($payment_plans_locale);
    }
    $payment_plans  =   ($locale !== 'en' && count($payment_plans_locale) > 0 ) ? array_get($data,'pricing.payment_plans_'.$locale) : array_get($data,'pricing.payment_plans',[]);
    $payment_plans  =   array_filter($payment_plans, function($v, $k) {return $v !== '||';}, ARRAY_FILTER_USE_BOTH);

    $select_fields          =   config('data.datalayer.fields.project');
    $data_for_datalayer     =   array_dot_only($data,$select_fields);
    array_set($data_for_datalayer,'subsource','desktop');
@endphp

<div id="pages_projects_details" data-project_id="{{array_get($data,'_id','')}}" data-project_name="{{array_get($data,'name','')}}">
    {{-- Banner --}}
    <div>
        <img class="f_p" data-type="listing" data-second="{{array_get($data,'image.banner')}}" src="{{cdn_asset('projects/'.array_get($data,'ref_no','').'/gallery/banner')}}" alt="{{$seo_image_alt}}" />
        <div class="ui card fluid grid">
            <div class="sixteen wide column">
                <div class="sixteen wide column">
                    {{__( 'page.form.contact' ) }}
                    {{ ( $locale !== 'en' && array_get($data,'developer.contact.name_'.$locale,'') != '' ) ? array_get($data,'developer.contact.name_'.$locale) : array_get($data,'developer.contact.name','') }}
                </div>
                <div class="ui form">
                    <form class="ui form" id="project_lead" onsubmit="return false;">
                        <div class="field">
                            <input id="project_lead_name" type="text" autocomplete="off" name="name" placeholder="{{__( 'page.form.placeholders.full_name' )}}*" value="">
                        </div>
                        <div class="field">
                            <input id="project_lead_email" type="email" autocomplete="off" name="email" placeholder="{{__( 'page.form.placeholders.email_address' )}}*" value="">
                        </div>
                        <div class="field">
                            <input type="tel" id="project_lead_phone" name="phone" placeholder="{{__( 'page.form.placeholders.phone_number' )}}*">
                        </div>
                        <div class="ui grid">
                            <div class="sixteen wide column" data-project_id="{{array_get($data,'_id','')}}">
                                <button type="button" class="ui submit button fluid purple" name="button" onclick="if($('#project_lead').form('is valid')){
                                    lead.project.email.__init({{json_encode($data_for_datalayer)}});$(this).addClass('track_email_submit');}else{console.log('form not validated!')}">{{__( 'page.form.button.request_details' ) }}
                                </button>
                            </div>
                            <div class="sixteen wide column" data-project_id="{{array_get($data,'_id','')}}">
                                <button type="button" class="ui button fluid purple track_call" name="button" onclick="pages.projects.details.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.form.button.call_now' ) }}</button>
                            </div>
                        </div>
                        {{-- <div class="ui error message"></div> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Logo and general title --}}
    <div class="ui container grid">
        <div class="three wide column">
            <img class="f_p" data-type="listing" data-second="{{array_get($data,'developer.contact.logo')}}" src="{{$project_logo}}" alt="{{$seo_image_alt}}" />
        </div>
        <div class="twelve wide column">
            <div class="ui grid">
                <div class="sixteen wide column">
                    @include( 'components.sections.breadcrumbs.projects.details' )
                </div>
                <div class="sixteen wide column">
                    <div>
                        <div>
                            @if(!empty(array_get($data,'pricing.starting',0)))
                                <span>
                                    {{__( 'page.global.from' )}}
                                    {{ ($locale !== 'en' && !empty(array_get($data,'pricing.currency_'.$locale,'')) ) ? array_get($data,'pricing.currency_'.$locale,'') : array_get($data,'pricing.currency','') }}
                                </span>
                                <span>
                                    {{number_format(array_get($data,'pricing.starting',0))}}
                                </span>
                            @endif
                        </div>
                        <h1>{{ $seo_h1 }}</h1>
                        <div>
                            <span>{{array_get($data,'area',''). ', '.array_get($data,'city',''). ', '.array_get($data,'country','') }}</span>
                        </div>
                        <div>
                            @if( $country !== 'International' )
                              @if(!empty(array_get($data,'pricing.starting',0)))
                                  <span>
                                      {{ trim(__('page.mortgage.estimated_mortgage')) }}
                                  </span>
                                  <span>
                                      {{number_format(((array_get($data,'pricing.starting') * 0.75 ) + (array_get($data,'pricing.starting') * 0.75 * 0.42)) / 300)}}
                                  </span>
                                  <span>{{ __( 'page.global.aed' ) }}/{{ __( 'page.mortgage.monthly' ) }}</span>
                              @endif
                            @endif
                        </div>
                    </div>
                    <div>
                        <div>
                            <img class="f_p" data-type="listing" data-second="{{array_get($data,'developer.contact.logo')}}" src="{{cdn_asset('assets/img/agency/logo').'/'.$developer_logo}}" alt="{{$seo_image_alt}}" />
                        </div>
                        <div>
                            <button class="small ui purple button track_call" onclick="pages.projects.details.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                            <button class="small ui purple button track_email" onclick="pages.projects.details.email.__init({{json_encode($data_for_datalayer)}});"><i class="envelope outline icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Secondary menu --}}
    <div class="ui container grid">
        <div class="sixteen wide column">
            <div class="ui secondary pointing menu" id="main-tab">
                <a href="#aboutproject" class="active item">{{__('page.project.details.tab_titles.about')}}</a>
                @if(!empty($payment_plans))
                    <a href="#paymentplan" class="item">{{__( 'page.project.details.payment_plan' )}}</a>
                @endif
                <a href="#imagegallery" class="item">{{__('page.project.details.tab_titles.gallery')}}</a>
                @if(array_has($data,'completion.image_desktop','') && !empty(array_get($data,'completion.image_desktop','')))
                    <a href="#completion" class="item">{{__( 'page.project.details.tab_titles.completion_status' )}}</a>
                @endif
                @if( $country !== 'International' )
                    <a href="#mortgage" class="item">{{__( 'page.mortgage.title' )}}</a>
                @endif
            </div>
        </div>
    </div>

    {{-- About Project --}}
    <div class="ui container grid" id="aboutproject">
        <div class="eight wide column" id="new_projects_sticky">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <span>{{ __( 'page.global.about' ).' ' }}</span>
                    <span>{{ ($locale !== 'en' && !empty(array_get($data,'name_'.$locale,'')) ) ? array_get($data,'name_'.$locale,'') : array_get($data,'name','').' ' }}</span>
                    <span>{{ __( 'page.project.details.by' ).' ' }}</span>
                    <span>{{ array_get($data,'developer.contact.name') }}</span>
                </div>
            </div>
            <div class="ui two column grid">
                <div class="row">
                    @if (!empty(array_get($data,'pricing.starting', 0)))
                        <div class="column">
                            <div class="bar"></div>
                            <div>
                                @if( $country !== 'International')
                                <span>{{ __( 'page.project.details.mortgage_deposit' ) }}</span>
                                <span>{{ number_format(array_get($data,'pricing.starting') * 0.20) }}&nbsp;{{ __( 'page.global.aed' ) }}</span>
                                @else
                                <span>{{ __( 'page.global.price' ) }} {{ __( 'page.global.from' ) }}</span>
                                <span>{{ number_format(array_get($data,'pricing.starting')) }}&nbsp;{{ ($locale !== 'en' && !empty(array_get($data,'pricing.currency_'.$locale,'')) ) ? array_get($data,'pricing.currency_'.$locale,'') : array_get($data,'pricing.currency','') }}</span>
                                @endif
                            </div>
                        </div>
                    @endif
                    @if(!empty(array_get($data,'unit.type',0)))
                    <div class="column">
                        <div class="bar"></div>
                        <div>
                            <span>{{ __( 'page.global.bedrooms' )}}</span>
                            <span>
                                {{ ($locale !== 'en' && !empty(array_get($data,'unit.type_'.$locale,'')) ) ? array_get($data,'unit.type_'.$locale,'') : array_get($data,'unit.type','') }}
                            </span>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="ui two column grid">
                <div class="row">
                    <div class="column">
                        <div class="bar"></div>
                        <div>
                            <span>{{ __('page.project.details.status') }}</span>
                            <span>{{ title_case(str_replace('_',' ',array_get($data,'status'))) }}</span>
                        </div>
                    </div>
                    <div class="column">
                        <div class="bar"></div>
                        <div>
                            <span>{{ __('page.project.details.expected_completion_date') }}</span>
                            <span>{{ array_get($data,'expected_completion_date') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eight wide column" id="view_map">
            <div class="ui grid">
                <div class="thirteen wide column" id="projects_details_map" data-lat="{{array_get($data,'coordinates.lat')}}" data-lng="{{array_get($data,'coordinates.lng')}}"></div>
                <div class="three wide column">
                    @foreach(__( 'page.landmarks' ) as $landmark)
                    <div class="ui checkbox" onclick="g_map.landmarks.show_hide_markers('{{snake_case($landmark)}}',!$(this).checkbox('is checked'))">
                        <input type="checkbox">
                        <label>{{$landmark}}</label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    {{-- Paymen Plan --}}
    <div class="ui container grid" id="paymentplan">
        @if(count($payment_plans) > 0)
            <div class="sixteen wide column">
                <h3>{{__( 'page.project.details.payment_plan' )}} </h3>
            </div>

            <div class="sixteen wide column">
                <div class="ui equal width grid container">
                    @foreach($payment_plans as $plan)
                        <div class="column">
                            @php
                                list($date,$percent,$plan)  =   explode("|",$plan);
                            @endphp
                            <div>
                                <h4>{{$percent}}</h4>
                            </div>
                            <div>
                                <span>{{$plan}}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>

    {{-- Video --}}
    <div class="ui container grid">
        <div class="sixteen wide column">
            <div class="ui pointing secondary menu" id="multimedia">
                @if(array_get($data,'video.normal.link',false))
                    <a class="active item" data-tab="{{__('page.global.video')}}" onclick="pages.projects.details.view_video_360('view_video');">{{__('page.global.video')}}</a>
                @endif
                @if(!empty(array_get($data,'image.floor_plans_file','')))
                <a class="item @if(array_get($data,'video.normal.link',false)) '' @else active @endif" data-tab="{{__('page.project.details.tab_titles.floor_plans')}}" >{{__('page.project.details.tab_titles.floor_plans')}}</a>
                @endif
                @if(array_get($data,'video.degree.link',false))
                    <a class="item" data-tab="{{__('page.project.details.tab_titles.3d_tour')}}" onclick="pages.projects.details.view_video_360('view_virtual_tour');">{{__('page.project.details.tab_titles.3d_tour')}}</a>
                @endif
            </div>

            @if(array_get($data,'video.normal.link',false))
                <div class="ui active tab segment" data-tab="{{__('page.global.video')}}">
                    <img src="{{array_random($images_mobile)}}" alt="{{$seo_image_alt}}">
                    <div class="viewwer">
                        @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.normal.link'), $matches ) )
                            <a class="popup-video-html5" href="https://www.youtube.com/watch?v={{ $matches[ 0 ] }}?autoplay=true"><span id="view_video"><i class="play circle outline icon"></i></span></a>
                        @else
                            <a class="popup-video-html5" href="{{ array_get($data,'video.normal.link') }}"><span id="view_video"><i class="play circle outline icon"></i></span></a>
                        @endif
                    </div>
                </div>
            @endif

            @if(!empty(array_get($data,'image.floor_plans_file','')))
                <div class="ui @if(array_get($data,'video.normal.link',false)) '' @else active @endif tab segment" data-tab="{{__('page.project.details.tab_titles.floor_plans')}}">
                    <img src="{{array_random($images_mobile)}}" alt="{{$seo_image_alt}}">
                    <div class="viewwer">
                        <a href="{{array_get($data,'image.floor_plans_file','')}}" target="_blank" class="ui button view-plan">{{__( 'page.property.details.view' ). ' '.__( 'page.project.details.tab_titles.floor_plans' )}}</a>
                    </div>
                </div>
            @endif

            @if(array_get($data,'video.degree.link',false))
                <div class="ui tab segment" data-tab="{{__('page.project.details.tab_titles.3d_tour')}}">
                    <img src="{{array_last($images_mobile)}}" alt="{{$seo_image_alt}}">
                    <div class="viewwer">
                        @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.degree.link'), $matches ) )
                            <a class="popup-video-html5" href="https://www.youtube.com/watch?v={{ $matches[ 0 ] }}?autoplay=true"><span id="view_video"><i class="play circle outline icon"></i></span></a>
                        @else
                            <a class="popup-video-html5" href="{{ array_get($data,'video.degree.link') }}"><span id="view_video"><i class="play circle outline icon"></i></span></a>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>

    {{-- Image Gallery --}}
    <div class="ui container grid" id="imagegallery">
        <div class="sixteen wide column">
            <h3>{{__('page.project.details.tab_titles.gallery')}} </h3>
        </div>
        <div class="sixteen wide column">
            <div id="galleryslider" class="slider" dir="ltr">
                <div class="slides">
                    @foreach($images as $index=>$image)
                    <div class="slide-item">
                        <img class="f_p" src="{{$image}}" alt="{{$seo_image_alt}}" />
                    </div>
                    @endforeach
                </div>
                <div class="slider-arrows"></div>
                <div class="slider-nav"></div>
            </div>
        </div>
    </div>

    {{-- Features --}}
    <div class="ui container grid" id="features">
        <div class="ten wide column">
            <div class="sixteen wide column">
                <h3>{{__( 'page.project.details.features_title' )}}</h3>
            </div>
            <div class="ui two column grid container list">
                <div class="row">
                    @foreach($amenities as $feature)
                        <div class="column">
                            <div class="item"><i class="check icon"></i> {{$feature}}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {{-- Brochure --}}
        <div class="six wide column">
            <div class="ui card" style="background-image: url({{array_random($images)}})">
                <div class="ui middle aligned sixteen column centered grid container">
                    @if(array_get($data,'files.brochure',false))
                    <div class="row one column">
                        <div class="column">
                            <span>{{__( 'page.project.details.download' )}}</span>
                            <h2>{{__( 'page.project.details.brochure' )}}</h2>
                            <a href="{{array_get($data,'files.brochure','')}}" target="_blank" class="ui button view-brochure">{{__( 'page.project.details.view_brochure' )}}</a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{-- Description --}}
    @php $lenght = strlen(array_get($data,'writeup.description','')); @endphp
    <div class="ui container grid" id="descrption">
        <div class="sixteen wide column">
            <h3>{{__( 'page.project.details.about' )}}</h3>
        </div>
        <div class="sixteen wide column">
            @if( $locale !== 'en' && array_get($data,'writeup.description_'.$locale,'') != '' )
                <div id="descriptxt" class="@if ($lenght > 500)descriptxt @else descriptxt-collapsed @endif" dir="ltr">{!! array_get($data,'writeup.description_'.$locale,'') !!}</div>
                <div>
                    @if ($lenght > 500)
                        <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_details', '7rem', 'descriptxt', 'angle', 'right', 'left', false, 'project.details.read_more', 'project.details.read_less');" class="read_more_details">
                        {{ __( 'page.project.details.read_more') }}
                        </a>
                        <i class="angle right icon"></i>
                    @endif
                </div>
            @else
                <div id="descriptxt" class="@if ($lenght > 500)descriptxt @else descriptxt-collapsed @endif" dir="ltr">{!! array_get($data,'writeup.description','') !!}</div>
                <div>
                    @if ($lenght > 500)
                        <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_details', '7rem', 'descriptxt', 'angle', 'right', 'left', false, 'project.details.read_more', 'project.details.read_less');" class="read_more_details">
                        {{ __( 'page.project.details.read_more') }}
                        </a>
                        <i id="angle" class="angle right icon"></i>
                    @endif
                </div>
            @endif
        </div>
    </div>

    {{-- Completion status --}}
    <div class="ui container grid" id="completion">
        @if(array_has($data,'completion.image_desktop','') && !empty(array_get($data,'completion.image_desktop','')))
        <div class="sixteen wide column">
            <h3>{{__( 'page.project.details.tab_titles.completion_status' )}}</h3>
        </div>
        <div class="sixteen wide column">
            <div class="ui middle aligned sixteen column centered grid container">
                    <img src="{{array_get($data,'completion.image_desktop','')}}" class="ui image">
            </div>
        </div>
        @endif
    </div>
    {{-- Mortgage Calculator --}}

    <div class="ui container grid" id="mortgage">
        @if( $country !== 'International' && !empty(array_get($data,'pricing.starting', 0)))
            <div class="six wide column">
                <div class="ui two column grid">
                    <div class="row">
                    <div class="sixteen wide column"><h3>{{__( 'page.mortgage.title' )}}</h3></div>
                    <div class="sixteen wide column"><span>{{__( 'page.mortgage.description')}}</span></div>
                    </div>
                </div>
            </div>

            <div class="ui card grid ten wide column">
                <div class="sixteen wide column">
                    <span>{{__( 'page.mortgage.calculations.purchase_price' ) }}</span>:
                    <span> {{ __( 'page.global.aed' ) }}</span>
                    <span id="purchase_price">{{number_format(array_get($data,'pricing.starting',0))}} </span>
                </div>

                <div class="ui large form">
                    <div class="two fields">
                        <div class="field">
                        <label>{{__( 'page.mortgage.calculations.down_payment' ) }} ({{ __( 'page.global.aed' ) }})</label>
                        <input id="emi_down_payment" type="number" value="{{array_get($data,'pricing.starting') * 0.20}}" />
                        </div>
                        <div class="field">
                        <label>{{__( 'page.mortgage.calculations.deposit' ) }}</label>
                        <input id="emi_deposit" type="number" value="20">
                        </div>
                    </div>
                </div>

                <div class="ui large form">
                    <div class="two fields">
                        <div class="field">
                        <label>{{__( 'page.mortgage.calculations.term' ) }}</label>
                        <input id="emi_years" type="number" value="20">
                        </div>
                        <div class="field">
                        <label>{{__( 'page.mortgage.calculations.interest' ) }}</label>
                        <input id="emi_interest" type="number" value="4.2">
                        </div>
                    </div>
                </div>

                <div class="ui large form">
                    <div class="two fields">
                        <div class="field">
                            <label>{{__( 'page.mortgage.powered_by' ) }}</label>
                            <img src="{{ cdn_asset('assets/img/eebank.png') }}" alt="{{$seo_image_alt}}" />
                            <label>{{__( 'page.mortgage.subject_approval' ) }}</label>
                        </div>
                        <div class="field">
                            <button type="button" class="ui button fluid purple" data-model="project" onclick="pages.property.details.mortgage.__init({{json_encode($data_for_datalayer)}});">{{__( 'page.mortgage.get_finance' ) }}</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    {{-- Similar property --}}
    @if(array_get($properties,'total', 0) !== 0)
    <div class="ui container grid" id="availableunit">
        <div class="sixteen wide column">
            <h3>{{__( 'page.global.available_unit' )}}&nbsp;{{ $seo_h1 }}</h3>
        </div>
        <div class="eleven wide column" id="property_card_list">
            <div class="ui one column grid" id="property_results">
                @foreach(array_get($properties,'data',[]) as $property)
                    @include( 'components.cards.property.desktop_and_tablet' )
                @endforeach
            </div>
        </div>
    </div>
    @endif
</div>
