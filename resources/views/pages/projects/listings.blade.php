@extends( 'layouts.listings' )

@section( 'title', $seo_title )

@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.projects.listings')
@endsection

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky container grid">
            <div class="sixteen wide column">
                @include( 'components.sections.search.projects' )
            </div>
            <h2 class="ui dividing header">
        </div>
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.projects.listings' )
    @elsenotmobile
        @include( 'components.sections.breadcrumbs.projects.listings' )
    @endnotmobile
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.projects.listings.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.projects.listings.mobile' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFzsV4eOaewWg9ShwUkNSLhv6KNtKAjAU&libraries=places"></script>

{{-- Setting form fields --}}
<script>
    $(document).mouseup(function(e){
        if(!$('.sharethis-inline-share-buttons').is(e.target)&&$('.sharethis-inline-share-buttons').has(e.target).length==0){
            $('.sharethis-inline-share-buttons').hide();
        }
    });
    $( document ).ready( function(){
        @notmobile
            pages.projects.listings.__init('{{strtolower($country)}}');
        @elsenotmobile
            pages.projects.listings.__init('{{strtolower($country)}}');
        @endnotmobile
    });
</script>
@endsection
