<div class="ui grid">
    @if($total == 0)
    <div class="sixteen wide column no_results">
        <div class="ui segment">
            <div>
                {{ __( 'page.property.listings.properties_no_found_title' ) }} <br />
                {{ __( 'page.property.listings.properties_no_found_subtitle' ) }}
            </div>
            <div>
                <button type="button" id="new_search" class="small ui purple button new_search" onclick="search.redirect_landing()"> {{ __( 'page.form.button.new_search' ) }}</button>
            </div>
        </div>
    </div>
    @endif
    <div class="sixteen wide column" id="property_card_grid">
        <div class="ui one column grid" id="property_results">
            @foreach($properties as $property)
                @include( 'components.cards.budget.mobile' )
            @endforeach
        </div>
    </div>
    @if($total !== 0)
    <button onclick="pages.budget.listings.refine.__init(this);" class="ui button fluid" type="button" name="button">{{__( 'page.budget.refine_search' )}}</button>
    @endif
</div>
@include( 'components.modals.budget.refine.mobile' )
