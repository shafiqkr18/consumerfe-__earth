<div class="ui grid">
    <div class="eleven wide column" id="property_card_list">
        <div class="ui one column grid" id="property_results">
            @if($total > 1)
                @foreach($properties as $property)
                    @include( 'components.cards.budget.desktop_and_tablet' )
                @endforeach
            @else
                <div class="sixteen wide column">
                    {{ __( 'page.property.listings.properties_no_found_title' ) }}
                </div>
            @endif
        </div>
    </div>
    <div class="five wide column">
        @if($total !== 0)
        <div class="ui grid">
            <div class="sixteen wide column">
                {{__( 'page.budget.refine_search' )}}
            </div>
            <div class="sixteen wide column">
                <div class="ui segment">
                    <div class="ui accordion">
                        @foreach($refinements as $refine)
                        @php
                          $area = preg_replace("/\([^)]+\)/","",array_get($refine,'area'));
                        @endphp
                        <div class="title" onclick="pages.budget.listings.sort.__init('{{kebab_case($area)}}');">
                            <i class="dropdown icon"></i>
                            {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get($refine,'area')), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get($refine,'area'))) : array_get($refine,'area') }} [{{array_get($refine,'total')}}]
                        </div>
                        <div class="content">
                            @if(count(array_get($refine,'bedroom',0))>0)
                            <p class="transition hidden">Bedrooms</p>
                                @foreach(array_get($refine,'bedroom') as $bed)
                                <button type="button" class="ui button" name="button" onclick="pages.budget.listings.sort.__init('{{kebab_case($area)}}-{{$bed}}-bed');">{{trans_choice(  __( 'page.global.bed', [ 'bed' => $bed ] ), $bed ) }}</button>
                                @endforeach
                            @endif
                            @if(count(array_get($refine,'bathroom',0))>0)
                            <p class="transition hidden">Bathrooms</p>
                                @foreach(array_get($refine,'bathroom') as $bath)
                                <button type="button" class="ui button" name="button" onclick="pages.budget.listings.sort.__init('{{kebab_case($area)}}-{{$bath}}-bath');">{{trans_choice(  __( 'page.global.bath', [ 'bath' => $bath ] ), $bath ) }}</button>
                                @endforeach
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
