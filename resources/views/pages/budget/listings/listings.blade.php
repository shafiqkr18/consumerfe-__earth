@php
  $total        =   count($properties);
  $properties   =   isset($properties['data']) && isset($properties['total']) && $properties['total'] == 0 ? $properties['data'] : $properties;
  $city         =  (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $query, 'city.0', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $query, 'city.0', '' ))) : array_get( $query, 'city.0', '' );
@endphp
<div class="ui container" id="pages_budget_listings">

    <div class="ui grid">

    </div>

    <div class="ui grid">
        <!-- SORT, SAVE SEARCH -->
    </div>

    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{{ __('data.seo_titles.body', ['type' => __('page.global.properties'), 'for_rent_buy' => __( 'data.rent_buy.for_'.snake_case_custom(array_get($query,'rent_buy.0'))), 'loc' => $city]) }}</h1>
        </div>
    </div>

    @section( 'property_card' )

        @notmobile
            @include('pages.budget.listings.desktop_and_tablet')
        @elsenotmobile
            @include('pages.budget.listings.mobile')
        @endnotmobile

    @endsection



    @yield( 'property_card' )

</div>
