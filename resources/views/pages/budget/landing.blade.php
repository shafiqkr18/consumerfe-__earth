@extends( 'layouts.landing' )

@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.budget.landing')
@endsection


@section( 'header' )
    @notmobile
    <div id="budget_search_header">
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'white' ] )
    </div>
    @elsenotmobile
    <div id="budget_search_header">
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    </div>
    @endnotmobile
@endsection

@section( 'banner' )
    @notmobile
        @include( 'components.sections.banner.budget' )
    @elsenotmobile
        @include( 'components.sections.banner.mobile.budget' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
    $( document ).ready( function(){
        @notmobile
            pages.budget.landing.__init();
        @elsenotmobile
            pages.budget.landing.__init();
        @endnotmobile
    });
</script>
@endsection
