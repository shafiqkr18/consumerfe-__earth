@extends( 'layouts.listings' )
@section( 'title', str_replace( ":price_max:", array_get( $_GET, 'price_max', 0 ), str_replace( ":rent_buy:", array_get( $_GET, 'rent_buy', 'Rent' ), __( 'page.budget.seo_title' ) ) ) )

@section( 'description', __( 'page.global.seo_description' ) )
@section( 'dataLayer' )
    @include('marketing.datalayer.budget.listings')
@endsection
@section( 'meta_tags' )
     
@endsection
@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky">
            <div class="ui container">
                @include( 'components.sections.search.budget' )
            </div>
        </div>
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.budget' )
    @elsenotmobile
        @include( 'components.sections.breadcrumbs.budget' )
    @endnotmobile
@endsection

@section( 'page_content' )
    @include('components.loader')
@endsection

@section( 'footer' )

    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile

@endsection

@section( 'page_scripts' )


{{-- Setting form fields --}}
<script>

    $( document ).ready( function(){

        @notmobile
            pages.budget.listings.__init();
        @elsenotmobile
            pages.budget.listings.__init();
        @endnotmobile

    });

</script>
@endsection
