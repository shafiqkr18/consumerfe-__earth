@extends( 'layouts.listings' )
@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile' )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky">
            <div class="ui container">
                @include( 'components.sections.search.property' )
            </div>
        </div>
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.privacy_policy' )
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'page_content' )
<div id="privacy_policy" class="ui container">
    <div class="ui grid">
        <div class="sixteen wide column">
            @php
                $policy = (Lang::has('privacy_policy.policy', \App::getLocale())) ?  __( 'privacy_policy.policy') : Lang::get('privacy_policy.policy',[],'en');
            @endphp
          {!! $policy !!}
        </div>
    </div>
</div>
@endsection

@section( 'footer' )

@notmobile
    @include( 'components.sections.footer.desktop_and_tablet' )
@elsenotmobile
@endnotmobile

@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
$( document ).ready( function(){
    @notmobile
        pages.landing.__init();
    @elsenotmobile
        pages.landing.__init();
    @endnotmobile
});
</script>
@endsection
