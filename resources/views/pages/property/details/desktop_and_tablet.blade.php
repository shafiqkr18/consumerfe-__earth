@php
    $ren_buy = array_get($data,'rent_buy', '') == 'Sale' ? 'buy' : strtolower(array_get($data,'rent_buy', ''));
    $ren_sale = strtolower(array_get($data,'rent_buy', ''));
    $residential_commercial = strtolower(array_get($data,'residential_commercial', ''));
    $images  = [];
    if(!empty(array_get($data,'images.portal',[]))){
        foreach(array_get($data,'images.portal',[]) as $key => $image)
            $images[] = cloudfront_asset(array_get($data ,'_id','').'_'.$key.'_banner');
    }
    elseif(!empty(array_get($data,'images.original',[]))){
        $images = array_get($data,'images.original',[]);
    }

    /*** GETTING ALL DATA IN ORDER TO REPLACE EMAILS AND PHONE NUMBERS FROM DESCRIPTIONS ***/
    $description = (\App::getLocale() !== 'en' && array_get($data,'writeup.description_'.\App::getLocale(),'') != '') ? array_get($data,'writeup.description_'.\App::getLocale()) : array_get( $data, 'writeup.description', '' );

    ### Forms info -START- ###
    $ref_no = array_get($data,'ref_no','');
    $agent_name = array_get($data,'agent.contact.name','');
    $agency_email = array_get($data,'agent.agency.contact.email','');
    $agency_name = array_get($data,'agent.agency.contact.name','');
    $_id = array_get($data,'_id','');
    ### Forms info -END- ###

    ### Call Tracking -START- ###
    $track  = array_get($data,'agent.agency.settings.lead.call_tracking.track', false);
    $number = $track ? array_get($data,'agent.agency.settings.lead.call_tracking.phone') : array_get($data,'agent.contact.phone','');
    $number = $number == '' ? array_get($data,'agent.agency.contact.phone','') : $number;
    if($number[0] == 0){
        $number = substr($number,1);
    }
    $number = str_replace('971971','971','+971'.$number);
    array_set($data,'number',$number);
    ### Call Tracking -END- ###

    ### Replacing emails and phone numbers  -START- ###
    $email_text    = Lang::has('page.form.send_email') ? __( 'page.form.send_email') : 'Send email';
    $phone_text    = Lang::has('page.form.view_phone_number') ? __( 'page.form.view_phone_number') : 'View phone number';
    $email_replace = '<span data-agent_contact_name="'.$agent_name.'" data-agency_contact_name="'.$agency_name.'" data-agency_contact_email="'.$agency_email.'" data-ref_no="'.$ref_no.'" data-_id="'.$_id.'"><a class="ui track_email" onclick="pages.landing.property.email.__init(this);"><i class="envelope icon"></i>'.$email_text.' </a></span>';
    $phone_replace = '<span data-agent_contact_name="'.$agent_name.'" data-agency_contact_name="'.$agency_name.'" data-agency_contact_email="'.$agency_email.'" data-ref_no="'.$ref_no.'" data-_id="'.$_id.'" data-agent_contact_phone="'.$number.'"><a class="ui track_call" onclick="pages.landing.property.call.__init(this);"><i class="phone volume icon"></i>'.$phone_text.'</a></span>';
    $description   = preg_replace('/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i',$email_replace,$description); // extract email
    $regex_number  = '/(?:\+971|00971|971|0|\(0\))(?:\s*|\-*)(?:50|51|52|54|55|56|58|2|4|6|7|9)[0-9 -]{7,11}/m';
    $description   = preg_replace($regex_number,$phone_replace,$description);
    ### Replacing emails and phone numbers  -END- ###

    $select_fields          =   config('data.datalayer.fields.property');
    $data_for_datalayer     =   array_dot_only($data,$select_fields);
    $amenities              =   array_get($data,'amenities',[]);
    $amenity_files          =   array_diff(scandir('assets/img/property/amenities/'), array('.', '..'));
    $amenity_icons          =   array_map(function($i){ return pathinfo($i, PATHINFO_FILENAME); },$amenity_files);

//var_dump($data);

@endphp

@if($data['settings']['status']==0)
<script>
  window.location.href = '/';
</script>
@endif

<div class="pages_property_details">
    <div class="ui grid container @if($data['settings']['status']==0){{ __( 'disabled' ) }}@endif">
        <div class="eleven wide column">
            {{-- Slider --}}
            <div>
                <div class="slider slides" dir="ltr">
                    <div class="slides">
                        @foreach($images as $key => $image)
                            <div class="slide-item">
                                <img class="f_p" src="{{ $image }}" alt="{{ $seo_image_alt }}" />
                            </div>
                        @endforeach
                    </div>
                    <div class="slider-arrows"></div>
                    <div class="slider-nav"></div>
                </div>
                {{-- Favorite and Share --}}
                <div>
                    <div>
                        <div>
                            <a><i class="heart icon {{ array_get($data,'favourite',false) ? 'favourite' : 'outline' }}" data-_id="{{$_id}}" {{ array_get($data,'favourite',false) ? 'data-_uid="'.array_get($data,'_id','').'"' : '' }} onclick="user.property.favourite.__init(this);"></i></a>
                        </div>
                        <div>
                             <!-- <a href="#" onclick="helpers.share.__init();" class="btn-share"><i class="share alternate icon"></i></a>
                             <div class="sharethis-inline-share-buttons"></div> -->
                        </div>
                    </div>
                </div>
                {{-- Video and 360--}}
                <div>
                    @if(array_get($data,'video.normal.link',false))
                    <div>
                        <div id="property_details_video">
                            @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.normal.link'), $matches ) )
                            <a class="popup-video-html5" href="https://www.youtube.com/watch?v={{ $matches[ 0 ] }}?autoplay=true">
                            @else
                            <a class="popup-video-html5" href="{{ array_get($data,'video.normal.link') }}">
                            @endif
                                <img src="{{cdn_asset('assets/img/property/play.png')}}" class="ui mini image">
                            </a>
                        </div>
                        <span>{{__( 'page.property.details.play' )}}</span>
                    </div>
                    @endif
                    @if(array_get($data,'video.degree.link',false))
                    <div>
                        <div id="property_details_360">
                            @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.degree.link'), $matches ) )
                            <a class="popup-video-html5" href="https://www.youtube.com/embed/{{ $matches[ 0 ] }}">
                            @else
                            <a class="popup-video-html5" href="{{ array_get($data,'video.degree.link') }}">
                            @endif
                                <img src="{{cdn_asset('assets/img/property/360.png')}}" class="ui mini image">
                            </a>
                        </div>
                        <span>{{__( 'page.property.details.view' )}}</span>
                    </div>
                    @endif
                </div>

            </div>

            {{-- Breadcrumbs --}}
            @include( 'components.sections.breadcrumbs.property.details' )

            <div class="ui grid container">
                <div class="sixteen wide column">
                    {{-- General info --}}
                    <div class="ui grid container">
                        <div class="eleven wide column">
                            <h1>{{ array_get( $data, 'writeup.title', '' ) }}</h1>
                            <div id="pages_property_details_data" data-_area="{{kebab_case(array_get( $data, 'area', '' ))}}">
                                <h2>
                                    {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}
                                    {{ (\App::getLocale() !== 'en' && Lang::has('data.rent_buy.for_'.snake_case_custom(array_get( $data, 'rent_buy', '' )), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom(array_get( $data, 'rent_buy', '' ))) : 'for ' . array_get( $data, 'rent_buy', '' ) }}
                                </h2>
                            </div>
                            <div>
                                {{-- {{ __( 'page.property.details.ref_no_title' ) }} : {{array_get( $data, 'ref_no', '' )}} | --}}
                                @if(array_get( $data, 'residential_commercial' ) == "Commercial" && array_get( $data, 'bedroom' ) >= 1 || array_get( $data, 'residential_commercial' ) == "Residential")
                                <i class="bed icon"></i> {{array_get( $data, 'bedroom') == 0 ? trans_choice( __( 'page.global.bed' ),0) : array_get( $data, 'bedroom')}}
                                @endif
                                @if(array_get( $data, 'bathroom' ) !== 0)
                                <i class="bath icon"></i> {{array_get( $data, 'bathroom') == 0 ? trans_choice( __( 'page.global.bath', [ 'bath' => 0 ] ),0) : array_get( $data, 'bathroom')}}
                                @endif
                                @if(array_get( $data, 'dimension.builtup_area') !== 0)
                                <i class="square icon"></i> {{array_get( $data, 'dimension.builtup_area', '' )}} {{__( 'page.global.sqft' )}}
                                @endif
                            </div>
                            <div>
                                {{-- {{ (\App::getLocale() !== 'en' && Lang::has('page.global.'.snake_case_custom('in'), \App::getLocale())) ? __( 'page.global.'.snake_case_custom('in')) : 'in' }} --}}
                                {{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $data, 'building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $data, 'building', '' ))).', ' : (empty(array_get( $data, 'building', '')) ? '' : array_get( $data, 'building') .', ') }}
                                {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $data, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $data, 'area', '' ))) : array_get( $data, 'area', __( 'page.property.details.no_area' ) ) }},
                                {{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $data, 'city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $data, 'city', '' ))) : array_get( $data, 'city', __( 'page.property.details.no_city' ) ) }}
                            </div>
                        </div>
                        <div class="five wide column">
                            <div class="get_price" data-price="{{array_get( $data, 'price', 0 )}}"><span class="currency">{{array_get( $data, 'price', 0 ) == 0 ? 'Price undisclosed' : __( 'page.global.aed' )}}</span> <span class="value">{{number_format(array_get( $data, 'price', 0 ))}}</span></div>
                            <div>
                              @if( array_get( $data, 'rent_buy', '' ) === 'Sale' )  <div><span>{{__( 'page.mortgage.estimated_mortgage' ) }}</span></div> @endif
                                @php
                                    $p      =   array_get($data,'price',1) - array_get($data,'price',1)*0.20;
                                    $i      =   4.2/(12*100);
                                    $n      =   20*12;
                                    $emi    =   ($p * $i * (1+$i)**$n)/((1+$i)**$n - 1);
                                @endphp
                              @if( array_get( $data, 'rent_buy', '' ) === 'Sale' )   <div class="get_price" data-price="{{$emi}}"><div><span class="currency">AED</span> <span class="value">{{number_format($emi)}}</span><span> / month</span></div></div> @endif
                                {{-- <a href="#" onclick="helpers.share.__init();" class="btn-share"><i class="share alternate icon"></i></a>
                                <div class="sharethis-inline-share-buttons"></div>
                                <a><i class="heart icon {{ array_get($data,'favourite',false) ? 'favourite' : 'outline' }}" data-_id="{{$_id}}" {{ array_get($data,'favourite',false) ? 'data-_uid="'.array_get($data,'_id','').'"' : '' }} onclick="user.property.favourite.__init(this);"></i></a> --}}
                            </div>
                        </div>
                    </div>

                    @if( !empty( $amenities ) && is_array( $amenities ))
                    <div class="ui grid container">
                        <div class="sixteen wide column">
                            <div><span>{{__( 'page.property.details.amenities_title' ) }}</span></div>
                        </div>
                        <div class="sixteen wide column">
                                <div>
                                    <ul class="internal_links" id="read_more_less_amenities">
                                        @foreach($amenities as $amenity)
                                            <li>
                                                @php
                                                $title = str_replace( array( '\'', '"', ',' , ';', '<', '>', "'", "/" ), ' ', $amenity);
                                                $title = strtolower($title);
                                                $amenity_name = strtolower( str_replace( '/', '', str_replace( ' ', '-', $amenity ) ) );
                                                $amenity_name = closest_string($amenity_icons,$amenity_name);
                                                @endphp
                                                <div>
                                                    <img src="{{cdn_asset('assets/img/property/amenities/'.$amenity_name.'.svg')}}" class="ui mini image" alt="{{$title}}">
                                                </div>
                                                <div>
                                                    <span>{{$title}}</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @if( count( $amenities ) > 6 )
                                <div>
                                    <a href="javascript:void(0);" onclick="helpers.view_more_amenities('read_more_amenities', '9rem', 'read_more_less_amenities', 'angle', 'down', 'up', true);" class="read_more_amenities">
                                        <div><i id="angle" class="angle down icon"></i></div>
                                        <div>
                                            <span>{{ __( 'page.property.details.view_more' ) }}</span>
                                        </div>
                                    </a>
                                </div>
                                @endif
                        </div>
                    </div>
                    @endif

                    {{-- Map and nearby --}}
                    <div class="ui grid container">
                        <div class="sixteen wide column">
                            <div><span>{{__( 'page.property.details.tab_titles.map' ) }}</span></div>
                        </div>

                        @php
                        $lat = is_null(array_get($data,'coordinates.lat',null)) ? 0 : array_get($data,'coordinates.lat',0);
                        $lng = is_null(array_get($data,'coordinates.lng',null)) ? 0 : array_get($data,'coordinates.lng',0);
                        @endphp
                        <div class="twelve wide column" id="property_details_map" data-lat="{{$lat}}" data-lng="{{$lng}}"></div>
                        <div class="four wide column">
                            @foreach(__( 'page.landmarks' ) as $key => $landmark)
                                <div class="ui checkbox" onclick="g_map.landmarks.show_hide_markers('{{snake_case($key)}}',!$(this).checkbox('is checked'))">
                                    <input type="checkbox">
                                    <label>{{$landmark}}</label>
                                </div>
                            @endforeach
                        </div>
                        <div class="no_loc"><span>{{__( 'page.property.details.not_location_provided', [ 'agency_name' => array_get($data,'agent.agency.contact.name',__( 'page.global.default_agency_name' ) ) ] )}}</span></div>
                    </div>

                    @php $lenght = strlen($description); @endphp
                    {{-- Description --}}
                    <div class="ui grid container">
                        @if( $description !== '' )
                            <div class="sixteen wide column">
                                <div>
                                    <span>
                                        {{ __( 'page.property.details.description' ) }}
                                    </span>
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                <div id="read_more_less_desc" class="@if ($lenght > 300)read_more_less_desc @else read_more_less_desc-collapsed @endif">
                                    {!! str_replace( "||", "<br />", preg_replace( ["/\|\|+/", "/\\n/"], ["||", "||"], trim( $description, "||" ) ) ) !!}
                                </div>
                                <div>

                                    @if ($lenght > 300)
                                        <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_details', '5rem', 'read_more_less_desc');" class="read_more_details">{{ __( 'page.property.details.view_more' ) }}</a>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>

                    {{-- Area Stats --}}
                    <div class="ui grid container">
                        <div class="sixteen wide column">
                            <div>
                                <span>
                                    {{-- {{ (\App::getLocale() !== 'ar') ? ((\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' )) : '' }} --}}
                                {{ __( 'page.property.details.stats.area_stats' ) }}
                                </span>
                            </div>
                        </div>

                        <div class="sixteen wide column">
                            <div>
                                <div>
                                    <div>
                                        <img src="{{cdn_asset('assets/img/guide_me/types/icon-'.strtolower(snake_case_custom(array_get( $data, 'type', '' ).'.png')))}}" class="ui image">
                                    </div>
                                    <div><span>{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}</span></div>
                                </div>
                                <div>
                                    <div>{{ __( 'page.guideme.average' ).' '.array_get( $data, 'rent_buy', '' ).' '.__( 'page.guideme.price' )}}</div>
                                    <div><h5>{{__( 'page.global.aed' ).' '. number_format(array_get( $stats, 'type.0.rent_buy.0.avg_price', 0 ))}}</h5></div>
                                    <div>
                                        @if (starts_with(array_get( $stats, 'type.0.rent_buy.0.percent'),'-'))
                                            <img src="{{cdn_asset('assets/img/property/low.jpg')}}" class="ui image">
                                        @else
                                            <img src="{{cdn_asset('assets/img/property/high.jpg')}}" class="ui image">
                                        @endif
                                        <span>{{array_get( $stats, 'type.0.rent_buy.0.percent', '0%' )}}</span>
                                        @if (starts_with(array_get( $stats, 'type.0.rent_buy.0.percent'),'-'))
                                            <span>{{ __( 'page.property.details.stats.decreased' )}} 12 &nbsp;</span>
                                        @else
                                        <span>{{ __( 'page.property.details.stats.increased' )}} 12 &nbsp;</span>
                                        @endif
                                        <span>{{ __( 'page.property.details.stats.months' )}}</span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>{{ __( 'page.property.details.stats.avg_floor' )}}</div>
                                <div>{{number_format(array_get( $stats, 'type.0.rent_buy.0.avg_builtup_area', 0 )).' '.__( 'page.global.sqft' )}}</div>
                            </div>
                        </div>

                        <div class="sixteen wide column">
                            {{-- <div>
                                <span>
                                    {{ __( 'page.property.details.stats.price' ) }}&nbsp;
                                </span>
                                <span>
                                    {{ __( 'page.property.details.stats.trends' ) }}&nbsp;
                                </span>
                                <span>
                                    {{ __( 'page.property.details.stats.by_property' ) }}&nbsp;
                                </span>
                                <span>
                                    {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}
                                </span>
                            </div> --}}
                        </div>

                        <div class="sixteen wide column">
                            {{-- <div>
                                <img src="{{env('ASSET_URL').'assets/img/property/chart.jpg'}}" class="ui image">
                            </div> --}}
                        </div>
                    </div>

                    {{-- Expert Review --}}
                    <div class="ui grid container">
                        <!-- <div class="sixteen wide column">
                            <div>
                                <span>
                                {{ __( 'page.property.details.expert_review' ) }}
                                </span>
                            </div>
                        </div>

                        <div class="sixteen wide column">
                            <div>
                                <span>23%</span>
                                <span>{{ __( 'page.property.details.cheaper_area' ) }}</span>
                            </div>

                            <div>
                                <span>25%</span>
                                <span>{{ __( 'page.property.details.cheaper_bed' ) }}</span>
                            </div>
                        </div> -->
                    </div>

                    {{-- Montlhy Costs --}}
                    <div class="ui grid container">
                        @if( array_get( $data, 'rent_buy', '' ) === 'Sale' )
                            <div class="sixteen wide column">
                                <div>
                                    <span>
                                    {{ __( 'page.mortgage.monthly_costs' ) }}
                                    </span>
                                </div>
                                <div>
                                    <p>{{__( 'page.mortgage.other_description' ) }}</p>
                                </div>
                            </div>

                            <div class="sixteen wide column" id="monthly_costs_wrapper" data-init-years="10" data-init-interest="3" data-init-down-payment="{{array_get($data,'price') * 0.20}}">
                                <div>
                                    <div><img class="ui image" src="{{cdn_asset('assets/img/property/mark.jpg')}}"></div>
                                    <div>
                                        <div>
                                            {{ __( 'page.global.aed' ) }}
                                        </div>
                                        <div><span id="property_monthly_value"></span></div>
                                        <div>
                                            {{__( 'page.mortgage.monthly_total' ) }}
                                        </div>
                                    </div>
                                    <div>{{__( 'page.mortgage.powered_by' ) }}</div>
                                    <div>
                                        <img class="ui small image" src="{{ cdn_asset('assets/img/eebank.png') }}" alt="{{ $seo_image_alt }}" />
                                    </div>
                                    <div>{{__( 'page.mortgage.subject_approval' ) }}</div>
                                </div>
                                <div>
                                    <div class="ui pointing secondary menu">
                                        <a class="item active" data-tab="mortgage"> {{__( 'page.mortgage.categories.mortgage' ) }}</a>
                                        <a class="item {{ ( array_get( $data, 'rent_buy', '' ) == 'Rent' ) ? 'active' : '' }}" data-tab="energy" onclick="pages.property.details.dewa.tabs.energy.__init();"> {{__( 'page.mortgage.categories.energy' ) }}</a>
                                        <a class="item" data-tab="water" onclick="pages.property.details.dewa.tabs.water.__init();"> {{__( 'page.mortgage.categories.water' ) }}</a>
                                    </div>

                                    {{-- Mortgage --}}
                                    <div class="ui tab segment active" data-tab="mortgage">
                                        <div class="ui grid container">
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.purchase_price' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                <span> {{ __( 'page.global.aed' ) }}</span>
                                                <span id="purchase_price">{{number_format(array_get($data,'price',0))}} </span>
                                            </div>
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.down_payment' ) }} ({{ __( 'page.global.aed' ) }})
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input id="emi_down_payment" type="number" value="{{array_get($data,'price',0) * 0.20}}" />
                                                </div>
                                            </div>
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.deposit' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input id="emi_deposit" type="number" value="20" placeholder="percentage">
                                                </div>
                                            </div>
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.repayment_term' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input id="emi_years" placeholder="years" type="number" value="20">
                                                </div>
                                            </div>
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.interest' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input id="emi_interest" placeholder="%" type="number" value="4.2">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- Energy --}}
                                    <div class="ui tab segment {{ ( array_get( $data, 'rent_buy', '' ) == 'Rent' ) ? 'active' : '' }}" data-tab="energy">
                                        <div class="ui grid container">
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.municipal_fees' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                {{ __( 'page.global.aed' ) }} <span>{{ number_format( $data['price'] * 0.005*0.05 ) }}</span>
                                            </div>
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.avg_electricity_cost' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input type="number" id="monthly_cost_energy_electricity" value="{{ number_format(array_get( $data, 'dimension.builtup_area', 0 ) * 0.22) }}">
                                                </div>
                                            </div>
                                            <div class="eight wide column">
                                                {{__( 'page.mortgage.calculations.avg_gas_cost' ) }}
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input type="number" id="monthly_cost_energy_gas" value="{{ number_format(array_get( $data, 'dimension.builtup_area', 0 ) * 0.02) }}">
                                                </div>
                                            </div>
                                            <div class="sixteen wide column">
                                                {{__( 'page.mortgage.calculations.message' ) }}
                                            </div>
                                        </div>
                                    </div>

                                    {{-- Water --}}
                                    <div class="ui tab segment" data-tab="water">
                                        <div class="ui grid container">
                                            <div class="eight wide column">
                                                <p>{{__( 'page.mortgage.calculations.avg_water_cost' ) }}</p>
                                            </div>
                                            <div class="eight wide column">
                                                <div class="ui form">
                                                    <input type="text" id="monthly_cost_water" value="{{ number_format(array_get( $data, 'dimension.builtup_area', 0 ) * 0.05) }}">
                                                </div>
                                            </div>
                                            <div class="sixteen wide column">
                                                {{__( 'page.mortgage.calculations.message' ) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button onclick="pages.property.details.mortgage.__init({{json_encode($data_for_datalayer)}});" name="button" class="ui button fluid purple" id="property_details_email_data" data-_id="{{$_id}}">
                                            {{__( 'page.mortgage.get_finance' ) }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    {{-- Similar property --}}
                    <div class="ui grid similar_properties">
                        <div class="sixteen wide column hide">
                            <div>
                                <span>
                                {{ __( 'page.property.details.similar_properties' ) }}
                                </span>
                            </div>
                        </div>
                        <div class="sixteen wide column" id="property_card_list">
                            {{-- <div class="single-item" style="overflow: hidden;"> --}}
                            <div class="ui one column grid" id="similar_properties" data-_id="{{$_id}}"></div>
                        </div>
                        <div class="sixteen wide column">
                            <div id="sliderDots"></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="five wide column">
            <div class="ui card fluid grid">
                <div></div>
                {{-- <div class="sixteen wide column"> --}}
                @php
                    $picture    =   !is_null(array_get($data,'agent.contact.picture')) ? array_get($data,'agent.contact.picture') : env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($data,'agent.contact.email','')));
                @endphp

                <div class="ui grid">
                    <div class="sixteen wide column">
                        <img class="ui tiny circular image" data-type="listing" src="{{$picture}}" onerror="this.style.display = 'none'" alt="{{ $seo_image_alt }}" />
                        <div>
                            <div>{{array_get($data,'agent.contact.name', __( 'page.global.default_agent_name' ))}}</div>
                            <div>{{$agency_name}}</div>
                            <div>
                                <div>{{__( 'page.property.details.agent_card.license_title' ) }}: # </div>
                                <div>
                                    @if(!empty(array_get($data,'agent.contact.license_no','')))
                                        {{array_get($data,'agent.contact.license_no','')}}
                                    @else
                                        {{ 'Not Specified' }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sixteen wide column">
                        <div>
                            @php
                                $view_all_params =  ['lng' => \App::getLocale(), 'rent_buy' => $ren_buy, 'residential_commercial' => $residential_commercial, 'properties-for-' . $ren_sale ];
                            @endphp
                            <a href="{{route('search-results-page',$view_all_params).'?agency_id='.array_get($data,'agent.agency._id')}}">
                                {{__( 'page.property.details.agent_card.view_all' ) }}
                            </a>
                        </div>
                        <img class="f_p ui tiny image" data-type="listing" alt="{{ $seo_image_alt }}" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"
                                        src="{{cdn_asset('agency/logo/'. array_get(explode('@',$agency_email), 1, ''))}}" alt="Zoom Property" />
                    </div>

                    <div class="sixteen wide column">
                        <h2 class="ui dividing header"></h2>
                    </div>

                    <div class="sixteen wide column">
                        <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                        <button class="small ui purple button track_callback" onclick="pages.landing.property.callback.__init({{json_encode($data_for_datalayer)}});"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button>
                    </div>

                    <div class="sixteen wide column">
                        <div>{{__( 'page.form.contact_agent_title' ) }}</div>
                        <form class="ui form" id="property_request_details" onsubmit="return false;">
                            <div class="ui form">
                                <div class="field">
                                    <input id="property_details_email_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*" name="name" type="text">
                                </div>
                                <div class="field">
                                    <input id="property_details_email_email" placeholder="{{__( 'page.form.placeholders.email' ) }}*" name="email" type="email">
                                </div>
                                <div class="field">
                                    <input type="tel" id="property_details_email_phone" type="number" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*">
                                </div>
                                <div class="field">
                                    <textarea id="property_details_email_textarea" rows="3">{{__( 'page.modals.property.email.message', [ 'reference' => $ref_no ] )}}</textarea>
                                </div>
                            </div>
                            <button class="ui button fluid purple" id="property_details_email_data" data-_id="{{$_id}}" onclick="
                            if($('#property_request_details').form('is valid')){
                                lead.property.details.email.__init(this);
                                $(this).addClass('track_email_submit');
                            } else {
                                console.log('form not validated!')
                            }"><i class="envelope icon"></i>{{__( 'page.form.send_email' ) }}</button>
                            {{-- <div class="ui error message"></div> --}}
                        </form>
                    </div>
                </div>
                {{-- </div> --}}
            </div>

            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="property_details_ad">
                        <!-- Home page MPU 1 [async] -->
                        <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                        <script type="text/javascript">
                        var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                        var abkw = window.abkw || '';
                        var plc317495 = window.plc317495 || 0;
                        document.write('<'+'div id="placement_317495_'+plc317495+'"></'+'div>');
                        AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 317495, [300,600], 'placement_317495_'+opt.place, opt); }, opt: { place: plc317495++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                        </script>
                    </div>
                </div>

                <div class="sixteen wide column">
                    <div class="property_details_ad_single">
                        <!-- Home page MPU 1 [async] -->
                        <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                        <script type="text/javascript">
                        var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                        var abkw = window.abkw || '';
                        var plc327478 = window.plc327478 || 0;
                        document.write('<'+'div id="placement_327478_'+plc327478+'" on></'+'div>');
                        AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 327478, [300,250], 'placement_327478_'+opt.place, opt); }, opt: { place: plc327478++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
