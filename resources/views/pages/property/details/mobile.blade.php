@php
    $ren_buy = array_get($data,'rent_buy', '') == 'Sale' ? 'buy' : strtolower(array_get($data,'rent_buy', ''));
    $ren_sale = strtolower(array_get($data,'rent_buy', ''));
    $residential_commercial = strtolower(array_get($data,'residential_commercial', ''));
    $images  = [];
    if(!empty(array_get($data,'images.portal',[]))){
        foreach(array_get($data,'images.portal',[]) as $key => $image)
            $images[] = cloudfront_asset(array_get($data ,'_id','').'_'.$key.'_banner');
    }
    elseif(!empty(array_get($data,'images.original',[]))){
        $images = array_get($data,'images.original',[]);
    }

    /*** GETTING ALL DATA IN ORDER TO REPLACE EMAILS AND PHONE NUMBERS FROM DESCRIPTIONS ***/
    $description = (\App::getLocale() !== 'en' && array_get($data,'writeup.description_'.\App::getLocale(),'') != '') ? array_get($data,'writeup.description_'.\App::getLocale()) : array_get( $data, 'writeup.description', '' );

    ### Forms info -START- ###
    $ref_no = array_get($data,'ref_no','');
    $agent_name = array_get($data,'agent.contact.name', __( 'page.global.default_agent_name' ));
    $agency_email = array_get($data,'agent.agency.contact.email','');
    $agency_name = array_get($data,'agent.agency.contact.name','');
    $_id = array_get($data,'_id','');
    ### Forms info -END- ###

    ### Call Tracking -START- ###
    $track  = array_get($data,'agent.agency.settings.lead.call_tracking.track', false);
    $number = $track ? array_get($data,'agent.agency.settings.lead.call_tracking.phone') : array_get($data,'agent.contact.phone','');
    $number = $number == '' ? array_get($data,'agent.agency.contact.phone','') : $number;
    if($number[0] == 0){
        $number = substr($number,1);
    }
    $number = str_replace('971971','971','+971'.$number);
    array_set($data,'number',$number);
    ### Call Tracking -END- ###

    ### Replacing emails and phone numbers  -START- ###
    $email_text    = Lang::has('page.form.send_email') ? __( 'page.form.send_email') : 'Send email';
    $phone_text    = Lang::has('page.form.view_phone_number') ? __( 'page.form.view_phone_number') : 'View phone number';
    $email_replace = '<span data-agent_contact_name="'.$agent_name.'" data-agency_contact_name="'.$agency_name.'" data-agency_contact_email="'.$agency_email.'" data-ref_no="'.$ref_no.'" data-_id="'.$_id.'"><a class="ui track_email" onclick="pages.landing.property.email.__init(this);"><i class="envelope icon"></i>'.$email_text.' </a></span>';
    $phone_replace = '<span data-agent_contact_name="'.$agent_name.'" data-agency_contact_name="'.$agency_name.'" data-agency_contact_email="'.$agency_email.'" data-ref_no="'.$ref_no.'" data-_id="'.$_id.'" data-agent_contact_phone="'.$number.'"><a class="ui track_call" onclick="pages.landing.property.call.__init(this);"><i class="phone volume icon"></i>'.$phone_text.'</a></span>';
    $description   = preg_replace('/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i',$email_replace,$description); // extract email
    $regex_number  = '/(?:\+971|00971|971|0|\(0\))(?:\s*|\-*)(?:50|51|52|54|55|56|58|2|4|6|7|9)[0-9 -]{7,11}/m';
    $description   = preg_replace($regex_number,$phone_replace,$description);
    ### Replacing emails and phone numbers  -END- ###

    $select_fields          =   config('data.datalayer.fields.property');
    $data_for_datalayer     =   array_dot_only($data,$select_fields);

    $amenities              =   array_get($data,'amenities',[]);
    #TODO load from CDN
    $amenity_files  =   array_diff(scandir('assets/img/property/amenities/'), array('.', '..'));
    $amenity_icons  =   array_map(function($i){ return pathinfo($i, PATHINFO_FILENAME); },$amenity_files);
@endphp
<div id="pages_property_details" @if($data['settings']['status']==0){{ __( 'class=disabled' ) }}@endif>
    {{-- Gallery --}}
    <div class="ui grid">
        <div class="slider" dir="ltr">
            <div class="slides">
                @foreach($images as $key => $image)
                    <div class="slide-item">
                        <img class="f_p" src="{{ $image }}" alt="{{ $seo_image_alt }}" />
                    </div>
                @endforeach
            </div>
            <div class="slider-arrows" dir="ltr"></div>
            <div class="slider-nav" dir="ltr"></div>
        </div>
        {{-- Favorite and Share --}}
        <div>
            <div class="ui selection dropdown" id="currency_dropdown">
                <input type="hidden" name="country" value="1">
                <i class="dropdown icon"></i>
                <div class="default text"></div>
                <div class="menu transition hidden">
                    @foreach( __( 'page.currencies' ) as $currency )
                    @if($loop->index === 0)
                        <div class="item active selected" data-currency="{{ array_get( $currency, 'code' ) }}" data-value="{{ array_get( $currency, 'value' ) }}"><i class="{{ array_get( $currency, 'flag' ) }} flag"></i>{{ ' '.array_get( $currency, 'code' ) }}</div>
                    @else
                    <div class="item" data-currency="{{ array_get( $currency, 'code' ) }}" data-value="{{ array_get( $currency, 'value' ) }}"><i class="{{ array_get( $currency, 'flag' ) }} flag"></i>{{ ' '.array_get( $currency, 'code' ) }}</div>
                    @endif
                    @endforeach
                </div>
            </div>
            <div>
                <div>
                    <a><i data-_id="{{$_id}}" {{ array_get($data,'favourite',false) ? 'data-_uid="'.array_get($data,'_id','').'"' : '' }} onclick="user.property.favourite.__init(this, true);" class="heart icon {{ array_get($data,'favourite',false) ? 'favourite' : 'outline' }}"></i></a>
                </div>
                {{-- <div>
                  <a href="javascript:void(0)" onclick="helpers.share.__init();" class="btn-share"><i class="share alternate icon"></i></a>
                </div> --}}
            </div>
            {{-- <div class="sharethis-inline-share-buttons"></div> --}}
        </div>
        {{-- Video and 360--}}
        <div>
            @if(array_get($data,'video.normal.link',false))
            <div>
                <div id="property_details_video">
                    @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.normal.link'), $matches ) )
                    <a class="popup-video-html5" href="https://www.youtube.com/watch?v={{ $matches[ 0 ] }}?autoplay=true">
                    @else
                    <a class="popup-video-html5" href="{{ array_get($data,'video.normal.link') }}">
                    @endif
                        <img src="{{cdn_asset('assets/img/property/play.png')}}" class="ui mini image">
                    </a>
                </div>
                <span>{{__( 'page.property.details.play' )}}</span>
            </div>
            @endif
            @if(array_get($data,'video.degree.link',false))
            <div>
                <span>{{__( 'page.property.details.view' )}}</span>
                <div id="property_details_360">
                    @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.degree.link'), $matches ) )
                    <a class="popup-video-html5" href="https://www.youtube.com/embed/{{ $matches[ 0 ] }}">
                    @else
                    <a class="popup-video-html5" href="{{ array_get($data,'video.degree.link') }}">
                    @endif
                        <img src="{{cdn_asset('assets/img/property/360.png')}}" class="ui mini image">
                    </a>
                </div>
            </div>
            @endif
        </div>
    </div>

    {{-- General info --}}
    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{{array_get( $data, 'price', 0 ) == 0 ? 'Price undisclosed' : __( 'page.global.aed' ).' '.number_format(array_get( $data, 'price', 0 ))}}</h1>
            <h3>{{ array_get( $data, 'writeup.title', '' ) }}</h3>
            <div>
                <div><span>{{__( 'page.mortgage.estimated_mortgage' ) }}</span></div>
                @php
                    $p      =   array_get($data,'price',1) - array_get($data,'price',1)*0.20;
                    $i      =   4.2/(12*100);
                    $n      =   20*12;
                    $emi    =   ($p * $i * (1+$i)**$n)/((1+$i)**$n - 1);
                @endphp
                <div class="get_price" data-price="{{$emi}}"><div><span class="currency">AED</span> <span class="value">{{number_format($emi)}}</span><span> / month</span></div></div>
            </div>
            <div id="pages_property_details_data" data-_area="{{kebab_case(array_get( $data, 'area', '' ))}}">
                <h3>
                    {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}
                    {{ (\App::getLocale() !== 'en' && Lang::has('data.rent_buy.for_'.snake_case_custom(array_get( $data, 'rent_buy', '' )), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom(array_get( $data, 'rent_buy', '' ))) : 'for ' . array_get( $data, 'rent_buy', '' ) }}
                </h3>
            </div>
            <div>
                {{-- {{ __( 'page.property.details.ref_no_title' ) }} : {{array_get( $data, 'ref_no', '' )}} | --}}
                @if(array_get( $data, 'residential_commercial' ) == "Commercial" && array_get( $data, 'bedroom' ) >= 1 || array_get( $data, 'residential_commercial' ) == "Residential")
                <i class="bed icon"></i> {{array_get( $data, 'bedroom') == 0 ? trans_choice( __( 'page.global.bed' ),0) : array_get( $data, 'bedroom')}}
                @endif
                @if(array_get( $data, 'bathroom' ) !== 0)
                <i class="bath icon"></i> {{array_get( $data, 'bathroom') == 0 ? trans_choice( __( 'page.global.bath', [ 'bath' => 0 ] ),0) : array_get( $data, 'bathroom')}}
                @endif
                @if(array_get( $data, 'dimension.builtup_area') !== 0)
                <i class="square icon"></i> {{array_get( $data, 'dimension.builtup_area', '' )}} {{__( 'page.global.sqft' )}}
                @endif
            </div>
            <div>
                {{-- {{ (\App::getLocale() !== 'en' && Lang::has('page.global.'.snake_case_custom('in'), \App::getLocale())) ? __( 'page.global.'.snake_case_custom('in')) : 'in' }} --}}
                {{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $data, 'building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $data, 'building', '' ))).', ' : (empty(array_get( $data, 'building', '')) ? '' : array_get( $data, 'building') .', ') }}
                {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $data, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $data, 'area', '' ))) : array_get( $data, 'area', __( 'page.property.details.no_area' ) ) }},
                {{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $data, 'city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $data, 'city', '' ))) : array_get( $data, 'city', __( 'page.property.details.no_city' ) ) }}
            </div>
        </div>
        {{-- button action --}}
        <div class="sixteen wide column" data-_id="{{$_id}}">
            {{-- <div class="ui equal width grid" data-_id="{{$_id}}"> --}}
                <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i></button>
                <button class="column small ui purple button track_callback" onclick="pages.landing.property.email.__init({{json_encode($data_for_datalayer)}});"><i class="envelope icon"></i></button>
                {{-- @if(mb_strlen($number) > 4 && str_contains($number, '9715')) --}}
                <button class="small ui purple button track_chat" onclick="pages.landing.property.whatsapp.__init({{json_encode($data_for_datalayer)}});"><i class="whatsapp icon"></i></button>
                {{-- @else --}}
                    {{-- <button class="small ui purple button track_callback" onclick="pages.landing.property.callback.__init({{json_encode($data_for_datalayer)}});"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button> --}}
                {{-- @endif --}}
            {{-- </div> --}}
        </div>
    </div>

    {{-- Menu tab --}}
    <div class="ui pointing secondary menu">
        <div class="item @if($data['settings']['status']==1){{ __( 'active' ) }}@else{{ __( 'hide' ) }}@endif" data-tab="agent-details">{{ __( 'page.property.details.tab_titles.agent_details' ) }}</div>
        @php
            $lat = is_null(array_get($data,'coordinates.lat',null)) ? 0 : array_get($data,'coordinates.lat',0);
            $lng = is_null(array_get($data,'coordinates.lng',null)) ? 0 : array_get($data,'coordinates.lng',0);
        @endphp
        <div class="item @if($data['settings']['status']==0){{ __( 'hide' ) }}@endif" data-tab="map_and_nearby" onclick="pages.property.details.map.__init({{$lat.','.$lng}});">{{ __( 'page.property.details.tab_titles.map' ) }}</div>

        {{-- @if(array_get($data,'video.normal.link',false))
            <div class="item @if($data['settings']['status']==0){{ __( 'hide' ) }}@endif" data-tab="video">{{ __( 'page.property.details.tab_titles.video' ) }}</div>
        @endif
        @if(array_get($data,'video.degree.link',false))
            <div class="item @if($data['settings']['status']==0){{ __( 'hide' ) }}@endif" data-tab="360">{{ __( 'page.property.details.tab_titles.video_degree' ) }}</div>
        @endif --}}
        {{-- <div class="item hide local_info_show" data-tab="local_info" onclick="pages.property.details.info.__init();">{{ __( 'page.property.details.tab_titles.local_info' ) }}</div> --}}
        {{-- <div class="@if($data['settings']['status']==0){{ __( 'active' ) }}@endif item" data-tab="similar-properties">{{ __( 'page.property.details.similar_properties' ) }}</div> --}}
    </div>

    {{-- Tab agent details --}}
    <div class="ui tab @if($data['settings']['status']==1){{ __( 'active' ) }}@endif" data-tab="agent-details">
        <div class="ui fluid grid">
            <div></div>
            @php
                $picture    =   !is_null(array_get($data,'agent.contact.picture')) ? array_get($data,'agent.contact.picture') : env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($data,'agent.contact.email','')));
            @endphp

            <div>
                <div class="ui card grid">
                    <div class="sixteen wide column">
                        <img class="ui small circular image" data-type="listing" src="{{$picture}}" alt="{{ $seo_image_alt }}" />
                        <div>
                            {{-- <div>{{array_get($data,'agent.contact.name', __( 'page.global.default_agent_name' ))}}</div> --}}
                            <div>{{$agent_name}}</div>
                            <div>{{$agency_name}}</div>
                            <div>
                                <div>{{__( 'page.property.details.agent_card.license_title' ) }}: # </div>
                                <div>
                                    @if(!empty(array_get($data,'agent.contact.license_no','')))
                                        {{array_get($data,'agent.contact.license_no','')}}
                                    @else
                                        {{ 'Not Specified' }}
                                    @endif
                                </div>
                            </div>
                            <div>
                                @php
                                    $view_all_params =  ['lng' => \App::getLocale(), 'rent_buy' => $ren_buy, 'residential_commercial' => $residential_commercial, 'properties-for-' . $ren_sale ];
                                @endphp
                                <a href="{{route('search-results-page',$view_all_params).'?agency_id='.array_get($data,'agent.agency._id')}}">
                                    {{__( 'page.property.details.agent_card.view_all' ) }}
                                </a>
                            </div>
                            <div>
                                <img class="f_p ui small image" data-type="listing" alt="{{ $seo_image_alt }}" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"
                                        src="{{cdn_asset('agency/logo/'. array_get(explode('@',$agency_email), 1, ''))}}" alt="Zoom Property" />
                            </div>
                        </div>
                    </div>

                    <div class="sixteen wide column">
                        <h2 class="ui dividing header"></h2>
                    </div>

                    <div class="sixteen wide column">
                        <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});">{{__( 'page.form.button.call_agent' ) }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Map and nearby --}}
    <div class="ui tab" data-tab="map_and_nearby">
        <div class="ui grid">
            <div class="ten wide column" id="property_details_map"></div>
            <div class="six wide column">
                @foreach(__( 'page.landmarks' ) as $key => $landmark)
                    <div class="ui checkbox" onclick="g_map.landmarks.show_hide_markers('{{snake_case($key)}}',!$(this).checkbox('is checked'))">
                        <input type="checkbox">
                        <label>{{$landmark}}</label>
                    </div>
                @endforeach
            </div>
            <div class="no_loc"></div>
            <div class="no_loc">{{__( 'page.property.details.not_location_provided', [ 'agency_name' => array_get($data,'agent.agency.contact.name',__( 'page.global.default_agency_name' ) ) ] )}}</div>
        </div>
    </div>

    {{-- Local info --}}
    <div class="ui tab hide local_info_show" data-tab="local_info">
        <div id="property_details_local_info"></div>
    </div>

    {{-- Amenities --}}
    <div class="ui container grid">
        @if( !empty( $amenities ) && is_array( $amenities ))
        @if( count( $amenities ) > 1 )
        <div class="sixteen wide column"><h3><div>{{__( 'page.property.details.amenities_title' ) }}</div></h3></div>
        <div class="ui two column grid container list">
            <div class="row" id="read_more_less_amenities">
                  @foreach($amenities as $amenity)
                    <div class="column">
                        <div class="item"><i class="check icon"></i> {{$amenity}}</div>
                    </div>
                    @endforeach
            </div>
            @if( count( $amenities ) > 10 )
                <div>
                    <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_amenities', '14rem', 'read_more_less_amenities');" class="read_more_amenities">{{ __( 'page.property.details.view_more' ) }}</a>
                </div>
            @endif
        </div>
        @endif
        @endif
    </div>
    {{-- Description --}}
    <div class="ui grid container">
        @if( $description !== '' )
            <div class="sixteen wide column">
                <div>
                    <span>
                        {{ __( 'page.project.details.about' ) }}
                    </span>
                </div>
            </div>
            <div class="sixteen wide column">
                <div id="read_more_less_desc" class="read_more_less_desc">
                    {!! str_replace( "||", "<br />", preg_replace( ["/\|\|+/", "/\\n/"], ["||", "||"], trim( $description, "||" ) ) ) !!}
                </div>
                <div>
                    <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_details', '7rem', 'read_more_less_desc');" class="read_more_details">{{ __( 'page.property.details.view_more' ) }}</a>
                </div>
            </div>
            @endif
        </div>

    {{-- Area Stats --}}
    <div class="ui grid container">
        <div class="sixteen wide column">
            <div>
                <span>
                    {{ __( 'page.property.details.stats.area_stats' ) }}
                </span>
            </div>
        </div>

        <div class="sixteen wide column">
            <div>
                <div>
                    <div>
                        <img src="{{cdn_asset('assets/img/guide_me/types/icon-'.strtolower(snake_case_custom(array_get( $data, 'type', '' ).'.png')))}}" class="ui mini image">
                    </div>
                    <div><span>{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}</span></div>
                </div>
                <div>
                    <div>{{ __( 'page.guideme.average' ).' '.array_get( $data, 'rent_buy', '' ).' '.__( 'page.guideme.price' )}}</div>
                    <div><h5>{{__( 'page.global.aed' ).' '. number_format(array_get( $stats, 'type.0.rent_buy.0.avg_price', 0 ))}}</h5></div>
                    <div>
                        @if (starts_with(array_get( $stats, 'type.0.rent_buy.0.percent'),'-'))
                            <img src="{{cdn_asset('assets/img/property/low.jpg')}}" class="ui image">
                        @else
                            <img src="{{cdn_asset('assets/img/property/high.jpg')}}" class="ui image">
                        @endif
                        @if (starts_with(array_get( $stats, 'type.0.rent_buy.0.percent'),'-'))
                            <span>{{ array_get( $stats, 'type.0.rent_buy.0.percent', '0%' ).' '.__( 'page.property.details.stats.decreased' ). ' 12 '.__( 'page.property.details.stats.months' )}}</span>
                        @else
                            <span>{{ array_get( $stats, 'type.0.rent_buy.0.percent', '0%' ).' '.__( 'page.property.details.stats.increased' ). ' 12 '.__( 'page.property.details.stats.months' )}}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div>
                <div>{{ __( 'page.property.details.stats.avg_floor' )}}</div>
                <div>{{number_format(array_get( $stats, 'type.0.rent_buy.0.avg_builtup_area', 0 )).' '.__( 'page.global.sqft' )}}</div>
            </div>
        </div>

        <div class="sixteen wide column">
            {{-- <div>
                <span>
                    {{ __( 'page.property.details.stats.price' ) }}&nbsp;
                </span>
                <span>
                    {{ __( 'page.property.details.stats.trends' ) }}&nbsp;
                </span>
                <span>
                    {{ __( 'page.property.details.stats.by_property' ) }}&nbsp;
                </span>
                <span>
                    {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}
                </span>
            </div> --}}
        </div>

        <div class="sixteen wide column">
            {{-- <div>
                <img src="{{env('ASSET_URL').'assets/img/property/chart.jpg'}}" class="ui image">
            </div> --}}
        </div>
    </div>

    {{-- Montlhy Costs --}}
    <div class="ui grid container">
        @if( array_get( $data, 'rent_buy', '' ) === 'Sale' )
            <div class="sixteen wide column">
                <div>
                    <span>
                    {{ __( 'page.mortgage.monthly_costs' ) }}
                    </span>
                </div>
                <div>
                    <p>{{__( 'page.mortgage.other_description' ) }}</p>
                </div>
            </div>

            <div class="sixteen wide column" id="monthly_costs_wrapper" data-init-years="10" data-init-interest="3" data-init-down-payment="{{array_get($data,'price') * 0.20}}">
                <div>
                    <div><img class="ui image" src="{{cdn_asset('assets/img/property/mark.png')}}"></div>
                    <div>
                        <div>
                            {{ __( 'page.global.aed' ) }}
                        </div>
                        <div><span id="property_monthly_value"></span></div>
                        <div>
                            {{__( 'page.mortgage.monthly_total' ) }}
                        </div>
                    </div>
                </div>
                <div>
                    <div class="ui pointing secondary menu">
                        <a class="item active" data-tab="mortgage"> {{__( 'page.mortgage.categories.mortgage' ) }}</a>
                        <a class="item {{ ( array_get( $data, 'rent_buy', '' ) == 'Rent' ) ? 'active' : '' }}" data-tab="energy" onclick="pages.property.details.dewa.tabs.energy.__init();"> {{__( 'page.mortgage.categories.energy' ) }}</a>
                        <a class="item" data-tab="water" onclick="pages.property.details.dewa.tabs.water.__init();"> {{__( 'page.mortgage.categories.water' ) }}</a>
                    </div>

                    <div class="ui tab segment active" data-tab="mortgage">
                        <div class="ui grid container">
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.purchase_price' ) }}:
                                <span> {{ __( 'page.global.aed' ) }}</span>
                                <span id="purchase_price">{{number_format(array_get($data,'price',0))}} </span>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.down_payment' ) }} ({{ __( 'page.global.aed' ) }})
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    <input id="emi_down_payment" type="number" value="{{array_get($data,'price',0) * 0.20}}" />
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.deposit' ) }}
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    {{-- <input id="deposite_amount_percentage" type="number" value="20" placeholder="percentage"> --}}
                                    <input id="emi_deposit" type="number" value="20" placeholder="percentage">
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.repayment_term' ) }}
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    {{-- <input id="monthly_cost_years" placeholder="years" type="number" value="20"> --}}
                                    <input id="emi_years" placeholder="years" type="number" value="20">
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.interest' ) }}
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    {{-- <input id="monthly_cost_interest" placeholder="%" type="number" value="4.2"> --}}
                                    <input id="emi_interest" placeholder="%" type="number" value="4.2">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ui tab segment {{ ( array_get( $data, 'rent_buy', '' ) == 'Rent' ) ? 'active' : '' }}" data-tab="energy">
                        <div class="ui grid container">
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.municipal_fees' ) }}
                                <span>{{ __( 'page.global.aed' ) }}</span>
                                <span>{{ number_format( $data['price'] * 0.005*0.05 ) }}</span>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.avg_electricity_cost' ) }}
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    <input type="number" id="monthly_cost_energy_electricity" value="{{ number_format(array_get( $data, 'dimension.builtup_area', 0 ) * 0.22) }}">
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.avg_gas_cost' ) }}
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    <input type="number" id="monthly_cost_energy_gas" value="{{ number_format(array_get( $data, 'dimension.builtup_area', 0 ) * 0.02) }}">
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.message' ) }}
                            </div>
                        </div>
                    </div>

                    <div class="ui tab segment" data-tab="water">
                        <div class="ui grid container">
                            <div class="sixteen wide column">
                                <p>{{__( 'page.mortgage.calculations.avg_water_cost' ) }}</p>
                            </div>
                            <div class="sixteen wide column content">
                                <div class="ui form">
                                    <input type="text" id="monthly_cost_water" value="{{ number_format(array_get( $data, 'dimension.builtup_area', 0 ) * 0.05) }}">
                                </div>
                            </div>
                            <div class="sixteen wide column">
                                {{__( 'page.mortgage.calculations.message' ) }}
                            </div>
                        </div>
                    </div>
                    <div class="ui grid">
                        <div class="sixteen wide column">
                            <button onclick="pages.property.details.mortgage.__init({{json_encode($data_for_datalayer)}})" name="button" class="ui button fluid purple" id="property_details_email_data" data-_id="{{$_id}}">
                                {{__( 'page.mortgage.get_finance' ) }}
                            </button>
                        </div>
                    </div>
                    <div class="ui grid">
                        <div class="sixteen wide column">
                            <div>{{__( 'page.mortgage.powered_by' ) }}</div>
                            <div>
                                <img class="ui small image" src="{{ cdn_asset('assets/img/eebank.png') }}" alt="{{ $seo_image_alt }}" />
                            </div>
                            <div>{{__( 'page.mortgage.subject_approval' ) }}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    {{-- Similar property --}}
    <div class="ui grid">
        <div class="sixteen wide column">
            <div>
                <span>
                    {{ __( 'page.property.details.similar_properties' ) }}
                </span>
            </div>
        </div>
        <div class="sixteen wide column" id="property_card_list">
            <div class="ui one column grid" id="similar_properties" data-_id="{{$_id}}"></div>
        </div>
    </div>


</div>
