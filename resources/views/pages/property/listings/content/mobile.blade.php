<div class="ui container" id="pages_property_listings_content">
    <div class="ui grid">
        <div class="sixteen wide column">
            <article>
                @foreach($seo_article as $key_section => $section)
                    @php
                        if(is_null(array_get($section,'title',null)) && is_null(array_get($section,'description',null)))
                            continue;
                        if(\App::getLocale() == 'en') {
                            $article_title      =   !empty(array_get($section,'title','')) ? array_get($section,'title') : '';
                            $article_desc       =   !empty(array_get($section,'description','')) ? array_get($section,'description') : '';
                            $article_image_alt  =   !empty(array_get($section,'image_alt','')) ? array_get($section,'image_alt') : '';
                        }
                        else{
                            $article_title      =   !empty(array_get($section,'title_'.\App::getLocale(),'')) ? array_get($section,'title_'.\App::getLocale()) : '';
                            $article_desc       =   !empty(array_get($section,'description_'.\App::getLocale(),'')) ? array_get($section,'description_'.\App::getLocale()) : '';
                            $article_image_alt  =   !empty(array_get($section,'image_alt_'.\App::getLocale(),'')) ? array_get($section,'image_alt_'.\App::getLocale()) : '';
                        }
                        $article_title  =   preg_replace('/(<[^>]+) style=".*?"/i', '$1', $article_title);
                        $article_desc   =   preg_replace('/(<[^>]+) style=".*?"/i', '$1', $article_desc);
                    @endphp
                    <div id="{{ $key_section }}">
                        <h2>{{ $article_title }}</h2>
                        <p>{!! $article_desc !!}</p>
                        @if(array_has($section, 'image'))
                            @php
                                $images             =   explode(',',array_get($section,'image'));
                                $article_image_alt  =   explode('|:|',$article_image_alt);
                            @endphp
                            @foreach($images as $key => $image)
                                @if(!str_contains($image, 'default-section-'))
                                  <figure><img src="{{$image}}" onerror="this.style.display = 'none'" alt="{{ !empty(array_get($article_image_alt,$key)) ? array_get($article_image_alt,$key) : $seo_image_alt }}" /></figure>
                                @endif
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </article>
        </div>
    </div>
</div>
