<div class="ui container" id="pages_property_listings_content">
    <div class="ui grid">
        <div class="eleven wide column">
            <article>
                @foreach($seo_article as $key_section => $section)
                    @php
                        if(is_null(array_get($section,'title',null)) && is_null(array_get($section,'description',null)))
                            continue;
                        if(\App::getLocale() == 'en') {
                            $article_title      =   !empty(array_get($section,'title','')) ? array_get($section,'title') : '';
                            $article_desc       =   !empty(array_get($section,'description','')) ? array_get($section,'description') : '';
                            $article_image_alt  =   !empty(array_get($section,'image_alt','')) ? array_get($section,'image_alt') : '';
                        }
                        else{
                            $article_title      =   !empty(array_get($section,'title_'.\App::getLocale(),'')) ? array_get($section,'title_'.\App::getLocale()) : '';
                            $article_desc       =   !empty(array_get($section,'description_'.\App::getLocale(),'')) ? array_get($section,'description_'.\App::getLocale()) : '';
                            $article_image_alt  =   !empty(array_get($section,'image_alt_'.\App::getLocale(),'')) ? array_get($section,'image_alt_'.\App::getLocale()) : '';
                        }
                        $article_title  =   preg_replace('/(<[^>]+) style=".*?"/i', '$1', $article_title);
                        $article_desc   =   preg_replace('/(<[^>]+) style=".*?"/i', '$1', $article_desc);
                    @endphp
                    <div id="{{ $key_section }}">
                        <h2>{{ $article_title }}</h2>
                        <p>{!! $article_desc !!}</p>
                        @if(array_has($section, 'image'))
                            @php
                                $images             =   explode(',',array_get($section,'image'));
                                $article_image_alt  =   explode('|:|',$article_image_alt);
                            @endphp
                            @foreach($images as $key => $image)
                                @if(!str_contains($image, 'default-section-'))
                                    <figure><img src="{{$image}}" onerror="this.style.display = 'none'" alt="{{ !empty(array_get($article_image_alt,$key)) ? array_get($article_image_alt,$key) : $seo_image_alt }}" /></figure>
                                @endif
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </article>
        </div>
        <div class="five wide column">
            <aside>
                <div class="property_listings_ad">
                    <!-- Property listings page banner [async] -->
                    <script type="text/javascript">
                    var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                    var abkw = window.abkw || '';
                    var plc327484 = window.plc327484 || 0;
                    document.write('<'+'div id="placement_327484_'+plc327484+'"></'+'div>');
                    AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 327484, [300,250], 'placement_327484_'+opt.place, opt); }, opt: { place: plc327484++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                    </script>
                </div>
                <div>
                    <form class="ui form" id="property_request_listings" onsubmit="return false;">
                        <h4>Request for more details</h4>
                        <span class="field"><input type="text" id="property_request_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" name="name" /></span>
                        <span class="field"><input type="email" id="property_request_email" placeholder="{{__( 'page.form.placeholders.email' ) }}" name="email" /></span>
                        <span class="field"><input type="tel" id="property_request_phone" name="phone" /></span>
                        <label for="message">Message *</label>
                        <textarea name="message" id="property_request_message" cols="30" rows="10" required>I am looking for {{ $seo_h1 }}. Please let me know about any suitable properties you have. Thank you.</textarea>
                        <button class="ui button fluid purple" id="property_request_data" data-title="{{$seo_h1}}" data-url="{{$request->getRequestUri()}}" onclick="
                            if($('#property_request_listings').form('is valid') && helpers.phonenumber.iti.isValidNumber()){
                                lead.property.request.__init(this);
                            } else {
                                console.log('form not validated!')
                            }">
                        {{__( 'page.form.button.submit' ) }}</button>
                        <div class="ui error message"></div>
                    </form>
                </div>
            </aside>
        </div>
    </div>
</div>
