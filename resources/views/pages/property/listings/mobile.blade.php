<div class="ui container" id="pages_property_listings">

    <!-- Sorting and Views -->
    <div class="ui grid">
        <div class="five wide column">
            <div class="ui selection dropdown" id="currency_dropdown">
                <input type="hidden" name="country" value="1">
                <i class="dropdown icon"></i>
                <div class="default text"></div>
                <div class="menu transition hidden">
                    @foreach( __( 'page.currencies' ) as $currency )
                    @if($loop->index === 0)
                        <div class="item active selected" data-currency="{{ array_get( $currency, 'code' ) }}" data-value="{{ array_get( $currency, 'value' ) }}"><i class="{{ array_get( $currency, 'flag' ) }} flag"></i>{{ ' '.array_get( $currency, 'code' ) }}</div>
                    @else
                    <div class="item" data-currency="{{ array_get( $currency, 'code' ) }}" data-value="{{ array_get( $currency, 'value' ) }}"><i class="{{ array_get( $currency, 'flag' ) }} flag"></i>{{ ' '.array_get( $currency, 'code' ) }}</div>
                    @endif
                    @endforeach
                </div>
            </div>


            {{-- <i class="vg flag"></i>
            <select id="currency_dropdown" class="ui search dropdown" onchange="other.change_currency( this );">

            </select> --}}
        </div>
        <div class="six wide column">
            <div class="ui floating labeled icon dropdown" id="listings_sort_btn">
                <i class="sort amount down icon"></i>
                <span class="text">{{ __( 'page.grid.sort.title_mobile' ) }}</span>
                <i class="caret down icon"></i>
                <div class="menu">
                    <div class="header"><i class="dollar sign icon"></i>{{ __( 'page.global.price' ) }} ({{ __( 'page.global.aed' ) }})</div>
                    <div class="item"><i data-sort="--price-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--price-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.high_to_low' ) }}</div>

                    <div class="header"><i class="bed icon"></i>{{trans_choice(  __( 'page.global.bedroom' ), 1 ) }}</div>
                    <div class="item"><i data-sort="--bedroom-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--bedroom-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.high_to_low' ) }}</div>

                    <div class="header"><i class="bath icon"></i>{{trans_choice(  __( 'page.global.bathroom' ), 1 ) }}</div>
                    <div class="item"><i data-sort="--bathroom-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--bathroom-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.high_to_low' ) }}</div>

                    <div class="header"><i class="square full icon"></i>{{ __( 'page.global.area' ) }} ({{ __( 'page.global.sqft' ) }})</div>
                    <div class="item"><i data-sort="--dimension.builtup_area-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--dimension.builtup_area-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.high_to_low' ) }}</div>

                    <div class="header"><i class="clock icon"></i>{{ __( 'page.global.time' ) }}</div>
                    <div class="item"><i data-sort="--timestamp.created-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.newest_to_oldest' ) }}</div>
                    <div class="item "><i data-sort="--timestamp.created-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.oldest_to_newest' ) }}</div>
                </div>
            </div>
        </div>
        <div class="five wide column" onclick="user.property.favourite.search.__init(this, 'mobile');">
            <i class="save icon"></i>
            <span class="text">{{ __( 'page.grid.sort.save_mobile' ) }}</span>
        </div>
    </div>

    <!-- Title & Info -->
    <div class="ui grid">
        <div class="sixteen wide column">
            <h1> {{ $seo_h1 }}</h1>
            @if(array_get($results,'total', 0) !== 0)
                <span class="counter listings_total hide" data-listings_total="{{array_get($results,'total','')}}">{{array_get($results,'total',47092)}}</span>
                <!-- <span>{{ __( 'page.property.listings.properties_found' ) }}</span> -->
            @endif
            @if(array_get($results,'total', 0) === 0)
                <div class="eleven wide column no_results">
                    <div class="ui segment">
                        <div>
                            {{ __( 'page.property.listings.properties_no_found_title' ) }} <br />
                            {{ __( 'page.property.listings.properties_no_found_subtitle' ) }}
                        </div>
                        <div>
                            <button type="button" id="new_search" class="small ui purple button new_search" onclick="search.redirect_landing()"> {{ __( 'page.form.button.new_search' ) }}</button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- Property Cards -->
    @section( 'property_card' )
    <div class="ui grid">
        <div class="sixteen wide column" id="property_card_list">
            <div class="ui one column grid" id="property_results">
                @php
                    $featuredData = count(array_get($featured,'data',[])) >= 5 ? array_random(array_get($featured,'data',[]), 5) : array_random(array_get($featured,'data',[]), count(array_get($featured,'data',[])));
                @endphp

                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                        @if($loop->index <= 3)
                            @include( 'components.cards.property.mobile' )
                        @endif
                    @endforeach
                @else
                    @foreach($featuredData as $property)
                        @if($loop->index <= 3)
                            @php
                                $featuredFlag = true;
                            @endphp
                            @include( 'components.cards.property.mobile' )
                        @endif
                    @endforeach
                @endif

                @if(array_get($agent,'total',0) !== 0)
                  @include( 'components.cards.agent.mobile' )
                @endif

                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                    @if($loop->index >= 4 && $loop->index <= 11)
                            @include( 'components.cards.property.mobile' )
                        @endif
                    @endforeach
                @else
                    @foreach($featuredData as $property)
                        @if($loop->index >= 4 && $loop->index <= 11)
                            @php
                                $featuredFlag = true;
                            @endphp
                            @include( 'components.cards.property.mobile' )
                        @endif
                    @endforeach
                @endif

                @if(array_get($project,'total',0) !== 0)
                    @include( 'components.cards.project.mobile' )
                @endif

                @foreach($featuredData as $property)
                    @if($loop->index >= 12)
                        @php
                        $featuredFlag = true;
                        @endphp
                        @include( 'components.cards.property.mobile' )
                    @endif
                @endforeach

                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                        @if($loop->index >= 12)
                            @include( 'components.cards.property.mobile' )
                        @endif
                    @endforeach
                @else
                    @foreach(array_get($results,'data',[]) as $property)
                        @php
                            if( array_get($property, 'featured.status') && starts_with(array_last($request->segments() ),'featured-') )
                                $featuredFlag = true;
                            else
                                $featuredFlag = false;
                        @endphp
                        @include( 'components.cards.property.mobile' )
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    @endsection

    @yield( 'property_card' )

    <!-- Pagination -->
    <div class="ui grid">
        <div class="sixteen wide column mobile_paginate">
            <ul id="pagination"></ul>
        </div>
    </div>

    <!-- Internal Links -->
    @include( 'components.sections.ilinks.header.mobile' )

</div>
 @php
    $article_exists = (\App::getLocale() == 'en') ? array_get($seo_article,'section_1.title','') : array_get($seo_article,'section_1.title_'.\App::getLocale(),'');
@endphp
@if( !empty( $seo_article ) && !empty( $article_exists ) )
    <!-- SEO Article -->
    @include( 'pages.property.listings.content.mobile' )
@endif 

<!-- Internal Links - Body -->
{{-- @include( 'components.sections.ilinks.body.mobile' ) --}}
