<div class="ui container" id="pages_property_listings">
    <!-- Title & Info -->
    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{{ $seo_h1 }}</h1>

            @if(array_get($results,'total', 0) !== 0)
            <span class="counter listings_total hide" data-listings_total="{{array_get($results,'total','')}}">{{array_get($results,'total',47092)}}</span>
            <!-- <span>{{ __( 'page.property.listings.properties_found' ) }}</span> -->
            @endif

            @if(array_get($results,'total', 0) === 0)
            <div class="eight wide column no_results">
                <div class="ui card grid">
                    <div class="twelve wide column">
                        {{ __( 'page.property.listings.properties_no_found_title' ) }} <br />
                        {{ __( 'page.property.listings.properties_no_found_subtitle' ) }}
                    </div>
                    <div class="four wide column">
                        <button type="button" id="new_search" class="small ui purple button new_search" onclick="search.redirect_landing()"> {{ __( 'page.form.button.new_search' ) }}</button>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

    <!-- Internal Links - Header -->
    @include( 'components.sections.ilinks.header.desktop_and_tablet' )

    <!-- Sorting and Views -->
    <div class="ui grid">
        <div class="eleven wide column">
            <!-- <select class="ui dropdown" id="listings_sort_btn"></select> -->
            <div class="ui floating labeled icon dropdown button small ui purple purple" id="listings_sort_btn">
                <i class="sort icon"></i>
                <span class="text">{{ __( 'page.grid.sort.title' ) }}</span>
                <div class="menu">
                    <!-- <div class="header"><i class="dollar sign icon"></i>{{ __( 'page.global.price' ) }} ({{ __( 'page.global.aed' ) }})</div> -->
                    <div class="item"><i data-sort="--price-asc--" class=""></i>{{ __( 'page.grid.sort.price_low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--price-desc--" class=""></i>{{ __( 'page.grid.sort.price_high_to_low' ) }}</div>

                    <!-- <div class="header"><i class="bed icon"></i>{{trans_choice(  __( 'page.global.bedroom' ), 1 ) }}</div> -->
                    <div class="item"><i data-sort="--bedroom-asc--" class=""></i>{{ __( 'page.grid.sort.beds_low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--bedroom-desc--" class=""></i>{{ __( 'page.grid.sort.beds_high_to_low' ) }}</div>

                  <!--  <div class="header"><i class="bath icon"></i>{{trans_choice(  __( 'page.global.bathroom' ), 1 ) }}</div>
                    <div class="item"><i data-sort="--bathroom-asc--" class="arrow up icon"></i>{{ __( 'page.grid.sort.low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--bathroom-desc--" class="arrow down icon"></i>{{ __( 'page.grid.sort.high_to_low' ) }}</div> -->

                    <!-- <div class="header"><i class="square full icon"></i>{{ __( 'page.global.area' ) }} ({{ __( 'page.global.sqft' ) }})</div> -->
                    <div class="item "><i data-sort="--dimension.builtup_area-asc--" class=""></i>{{ __( 'page.grid.sort.area_low_to_high' ) }}</div>
                    <div class="item"><i data-sort="--dimension.builtup_area-desc--" class=""></i>{{ __( 'page.grid.sort.area_high_to_low' ) }}</div>

                    <!-- <div class="header"><i class="clock icon"></i>{{ __( 'page.global.time' ) }}</div> -->
                    <div class="item"><i data-sort="--timestamp.created-desc--" class=""></i>{{ __( 'page.grid.sort.time_low_to_high' ) }}</div>
                    <div class="item "><i data-sort="--timestamp.created-asc--" class=""></i>{{ __( 'page.grid.sort.time_high_to_low' ) }}</div>
                </div>
            </div>
            <button type="button" id="property_map_view" class="small ui purple button property_map_view" onclick="pages.property.listings.map.__init()"><i class="map icon"></i> {{ __( 'page.grid.view.map' ) }}</button>
            <button type="button" id="property_list_view" class="small ui purple button property_list_view" onclick="pages.property.listings.list.__init()"><i class="list icon"></i> {{ __( 'page.grid.view.list' ) }}</button>
            <button type="button" id="property_grid_view" class="small ui purple button property_grid_view" onclick="pages.property.listings.grid.__init()"><i class="th icon"></i> {{ __( 'page.grid.view.grid' ) }}</button>
        </div>
        <div class="three wide column">
          {{-- Currency dropdown --}}
            <select id="currency_dropdown" class="ui search dropdown" onchange="other.change_currency( this );">
                @foreach( __( 'page.currencies' ) as $currency )
                    <option data-currency="{{ array_get( $currency, 'code' ) }}" data-value="{{ array_get( $currency, 'value' ) }}">{{ array_get( $currency, 'code' ).' - '.array_get( $currency, 'label' ) }}</option>
                @endforeach
            </select>
        </div>
        <div class="two wide column">
                <button type="button" class="small ui purple button" onclick="user.property.favourite.search.__init();"><i class="heart icon"></i> {{ __( 'page.grid.button.save' ) }}</button>
            </div>
        </div>



    <!-- Property Cards -->
    @section( 'property_card' )
    @notmobile
    <div class="ui grid">
        <div class="eleven wide column" id="property_card_list">
            <div class="ui one column grid" id="property_results">
                @php
                    $featuredData = count(array_get($featured,'data',[])) >= 5 ? array_random(array_get($featured,'data',[]), 5) : array_random(array_get($featured,'data',[]), count(array_get($featured,'data',[])));
                @endphp

                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                        @if($loop->index <= 1)
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @else
                    @foreach($featuredData as $property)
                        @if($loop->index <= 1)
                            @php
                            $featuredFlag = true;
                            @endphp
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @endif

                <div class="sixteen wide column">
                    <div id="property_listings_ad">
                        <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                        <script type="text/javascript">
                        var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                        var abkw = window.abkw || '';
                        var plc327480 = window.plc327480 || 0;
                        document.write('<'+'div id="placement_327480_'+plc327480+'"></'+'div>');
                        AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 327480, [728,90], 'placement_327480_'+opt.place, opt); }, opt: { place: plc327480++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                        </script>
                    </div>
                </div>


                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                        @if($loop->index >= 2 && $loop->index <= 3)
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @else
                    @foreach($featuredData as $property)
                        @if($loop->index >= 2 && $loop->index <= 3)
                            @php
                            $featuredFlag = true;
                            @endphp
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @endif

                @if(array_get($agent,'total',0) !== 0)
                    @include( 'components.cards.agent.desktop_and_tablet' )
                @endif

                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                        @if($loop->index >= 4 && $loop->index <= 7)
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @else
                    @foreach($featuredData as $property)
                        @if($loop->index >= 2 && $loop->index <= 3)
                            @php
                            $featuredFlag = true;
                            @endphp
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @endif

                @if(array_get($project,'total',0) !== 0)
                    @include( 'components.cards.project.desktop_and_tablet' )
                @endif

                @foreach($featuredData as $property)
                    @if($loop->index >= 8)
                        @php
                        $featuredFlag = true;
                        @endphp
                        @include( 'components.cards.property.desktop_and_tablet' )
                    @endif
                @endforeach

                @if( empty( $featuredData ) )
                    @foreach(array_get($results,'data',[]) as $property)
                        @if($loop->index >= 8)
                            @include( 'components.cards.property.desktop_and_tablet' )
                        @endif
                    @endforeach
                @else
                    @foreach(array_get($results,'data',[]) as $property)
                        @php
                            if( array_get($property, 'featured.status') && starts_with(array_last($request->segments() ),'featured-') )
                                $featuredFlag = true;
                            else
                                $featuredFlag = false;
                        @endphp
                        @include( 'components.cards.property.desktop_and_tablet' )
                    @endforeach
                @endif
            </div>
        </div>
        <div class="five wide column">
            <div class="ui rail">
                <div class="ui sticky gnral-stcky">
                    {{-- Sticky map --}}
                    <div class="ui sticky map">
                        <div id="property_listings_map"></div>
                        <div class="no_loc"></div>
                        <div class="no_loc text"></div>
                    </div>
                    {{-- Ilinks bedroom --}}
                    @include( 'components.sections.ilinks.bedroom.desktop_and_tablet' )
                    {{-- Ads --}}
                    <div class="ui sticky property_listings_ad">
                        <!-- Property listings page banner [async] -->
                        <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                        <script type="text/javascript">
                        var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                        var abkw = window.abkw || '';
                        var plc317495 = window.plc317495 || 0;
                        document.write('<'+'div id="placement_317495_'+plc317495+'"></'+'div>');
                        AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 317495, [300,600], 'placement_317495_'+opt.place, opt); }, opt: { place: plc317495++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                        </script>
                    </div>
                    {{-- Ilinks body --}}
                    @include( 'components.sections.ilinks.body.desktop_and_tablet' )
                </div>
            </div>
            
        </div>
    </div>
    @endnotmobile
    @endsection

    @yield( 'property_card' )

    <!-- Pagination -->
    <div class="ui grid">
        <div class="eleven wide column">
            <ul id="pagination"></ul>
        </div>
    </div>

</div>
<!-- SEO Article -->
 @php
    $article_exists = (\App::getLocale() == 'en') ? array_get($seo_article,'section_1.title','') : array_get($seo_article,'section_1.title_'.\App::getLocale(),'');
@endphp
@if( !empty( $seo_article ) && !empty( $article_exists ) )
    @include( 'pages.property.listings.content.desktop_and_tablet' )
@endif

<!-- SEO Links -->
 @php
    $links_exists = array_has($seo_links,'links.0') ? array_get($seo_links,'links.0') : '';
@endphp
@if( !empty( $seo_links ) && !empty( $links_exists ) )
    @include( 'components.sections.ilinks.popular.desktop_and_tablet' )
@endif
