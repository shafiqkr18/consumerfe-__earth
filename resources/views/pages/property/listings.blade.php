@extends( 'layouts.listings' )

@section( 'title', $seo_title )

@section( 'description', $seo_description )

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'dataLayer' )
    @include('marketing.datalayer.property.listings')
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky container grid">
            <div class="sixteen wide column">
                @include( 'components.sections.search.property' )
            </div>
            <h2 class="ui dividing header">
        </div>
    @elsenotmobile
    <div id="sticky_header" class="ui sticky fixed top">
        @include( 'components.awesomplete.property' )
    </div>
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @desktop
        @include( 'components.sections.breadcrumbs.property' )
    @enddesktop
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.property.listings.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.property.listings.mobile' )
    @endnotmobile
@endsection

@section( 'page_modals' )
    @notmobile
        @include( 'components.modals.agent.call.desktop_and_tablet' )
        @include( 'components.modals.agent.email.desktop_and_tablet' )
        @include( 'components.modals.agent.callback.desktop_and_tablet' )
        @include( 'components.modals.developer.call.desktop_and_tablet' )
        @include( 'components.modals.developer.email.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.modals.agent.call.mobile' )
        @include( 'components.modals.agent.email.mobile' )
        @include( 'components.modals.developer.email.desktop_and_tablet' )
    @endnotmobile
@endsection


@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )

    @php
    // dd($query);
    @endphp

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [

            @php
                $pos_no     =   1;
            @endphp
            {
                "@type": "ListItem",
                "position": {{ $pos_no }},
                "item": {

                    "@id": "{{ env( 'APP_URL' ) }}",
                    "name": "{{ __( 'page.menu_breadcrumbs.home' ) }}"

                }
            }

            @if( array_get( $query, 'residential_commercial.0', false ) && array_get( $query,'rent_buy.0',false ) )
                @php
                    $rb         =   array_get( $query, 'rent_buy.0' );
                    $rb_link    =   ( $rb === 'Sale' ) ? 'buy' : $rb;
                    $pos_no++;
                @endphp
                ,{
                    "@type": "ListItem",
                    "position": {{ $pos_no }},
                    "item": {

                        "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($query,'residential_commercial.0','')) }}/properties-for-{{ kebab_case_custom($rb) }}",
                        "name": "{{ array_get( $query, 'residential_commercial.0', false ) == 'Commercial' ? __( 'data.residential_commercial.'.snake_case_custom(array_get( $query, 'residential_commercial.0', '' ))) : ''}} {{ __( 'data.rent_buy.'.snake_case_custom($rb_link) ) }}"

                    }
                }
            @endif
            @if( !empty( array_get($query,'type','') ) )
                @php
                    $pt    =   kebab_case_custom( array_get($query,'type.0','') );

                    $pos_no++;
                @endphp
                ,{
                    "@type": "ListItem",
                    "position": {{ $pos_no }},
                    "item": {

                        "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($query,'residential_commercial.0','')) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}",
                        "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $query, 'type.0', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $query, 'type.0', '' ))) : array_get( $query, 'type.0', '' ) }}"

                    }
                }
            @endif
            @if( !empty( array_get($query,'city','') ) )
                @php
                    $pt    =   !empty( array_get($query,'type','') ) ? kebab_case_custom( array_get($query,'type.0','') ) : 'properties';

                    $pos_no++;
                @endphp
                ,{
                    "@type": "ListItem",
                    "position": {{ $pos_no }},
                    "item": {

                        "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($query,'residential_commercial.0','')) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom(array_get($query,'city.0','')) }}-city",
                        "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $query, 'city.0', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $query, 'city.0', '' ))) : array_get( $query, 'city.0', '' ) }}"

                    }
                }
            @endif
            @if( !empty( array_get($query,'area','') ) )
                @php
                    $pt    =   !empty( array_get($query,'type.0','') ) ? kebab_case_custom( array_get($query,'type.0','')) : 'properties';

                    $pos_no++;
                @endphp

                ,{
                    "@type": "ListItem",
                    "position": {{ $pos_no }},
                    "item": {

                        "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($query,'residential_commercial.0','')) }}/{{ kebab_case_custom(array_get($query,'city.0','')) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom(implode( '-or-', array_get($query,'area','') ) ) }}-area",
                        "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $query, 'area.0', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $query, 'area.0', '' ))) : array_get( $query, 'area.0', '' ) }}"

                    }
                }
            @endif
            @if( !empty( array_get($query,'building','') ) )
                @php
                    $pt    =   !empty( array_get($query,'type.0','') ) ? kebab_case_custom( array_get($query,'type.0','')) : 'properties';

                    $pos_no++;
                @endphp

                ,{
                    "@type": "ListItem",
                    "position": {{ $pos_no }},
                    "item": {

                        "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($query,'residential_commercial.0','')) }}/{{ kebab_case_custom(array_get($query,'city.0','')) }}/{{ kebab_case_custom(implode( '-or-', array_get($query,'area','') ), true ) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom(implode( '-or-', array_get($query,'building','') ), true ) }}-building",
                        "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $query, 'building.0', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $query, 'building.0', '' ))) : array_get( $query, 'building.0', '' ) }}"

                    }
                }
            @endif
        ]
    }
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFzsV4eOaewWg9ShwUkNSLhv6KNtKAjAU&libraries=places"></script>

{{-- Setting form fields --}}
<script>

    $( document ).ready( function(){

        @notmobile
            pages.property.listings.__init();
        @elsenotmobile
            // $('.ui.accordion').accordion();
            pages.property.listings.__init();
        @endnotmobile
        validate.rules.lead.listings.__init();
    });

</script>
@endsection
