@extends( 'layouts.details' )
@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.property.details')
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    @endnotmobile
@endsection

@section( 'meta_tags' )

@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky">
            <div class="ui container">
                @include( 'components.sections.search.property' )
            </div>
        </div>
    @elsenotmobile
    @endnotmobile
@endsection

{{-- @section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.property.details' )
    @elsenotmobile
        @include( 'components.sections.breadcrumbs.property.details' )
    @endnotmobile
@endsection --}}

@section( 'page_content' )
    @notmobile
        @include( 'pages.property.details.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.property.details.mobile' )
    @endnotmobile
@endsection

@section( 'page_templates' )
    @notmobile
        @include( 'components.cards.similar.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.cards.similar.mobile' )
    @endnotmobile
@endsection

@section( 'page_modals' )
    @notmobile
        @include( 'components.modals.property.mortgage.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.modals.property.mortgage.mobile' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
    {{--  Breadcrumb Type  --}}
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [

                @php
                    $pos_no = 1;
                @endphp

                {
                    "@type": "ListItem",
                    "position": {{ $pos_no }},
                    "item": {

                        "@id": "{{ env( 'APP_URL' ) }}",
                        "name": "{{ __( 'page.menu_breadcrumbs.home' ) }}"

                    }
                }
                @if( array_get( $data, 'residential_commercial', false ) && array_get( $data,'rent_buy',false ) )
                    @php
                        $rb         =   array_get( $data, 'rent_buy' );
                        $rb_link    =   ( $rb === 'Sale' ) ? 'buy' : $rb;
                        $pos_no++;
                    @endphp
                    ,{
                        "@type": "ListItem",
                        "position": {{ $pos_no }},
                        "item": {

                            "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get( $data, 'residential_commercial', '' )) }}/properties-for-{{ kebab_case_custom($rb) }}",
                            "name": "{{ array_get( $data, 'residential_commercial', false ) == 'Commercial' ? __( 'data.residential_commercial.'.snake_case_custom(array_get( $data, 'residential_commercial', '' ))) : ''}} {{ __( 'data.rent_buy.'.snake_case_custom($rb_link) ) }}"

                        }
                    }
                @endif
                @if( array_get($data,'type',false) )
                    @php
                        $pt_url    =   env( 'APP_URL' ).\App::getLocale().'/'.str_replace( '//', '/', kebab_case_custom($rb_link).'/'.kebab_case_custom(array_get( $data, 'residential_commercial', '' )).'/'.kebab_case_custom(array_get( $data, 'type','')).'-for-'.kebab_case_custom($rb) );

                        $pos_no++;
                    @endphp
                    ,{
                        "@type": "ListItem",
                        "position": {{ $pos_no }},
                        "item": {

                            "@id": "{{ $pt_url }}",
                            "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'type', '' ))) : array_get( $data, 'type', '' ) }}"

                        }
                    }
                @endif
                @if( array_get($data,'city',false) )
                    @php
                        $pt    =   array_get($data,'type',false) ? kebab_case_custom(array_get($data,'type','')) : 'properties';

                        $pos_no++;
                    @endphp
                    ,{
                        "@type": "ListItem",
                        "position": {{ $pos_no }},
                        "item": {

                            "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get( $data, 'residential_commercial','')) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom(array_get( $data, 'city','')) }}-city",
                            "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $data, 'city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $data, 'city', '' ))) : array_get( $data, 'city', '' ) }}"

                        }
                    }
                @endif
                @if( array_get($data,'area',false) )
                    @php
                        $pt    =   array_get($data,'type',false) ? kebab_case_custom( array_get($data,'type','') ) : 'properties';

                        $pos_no++;
                    @endphp

                    ,{
                        "@type": "ListItem",
                        "position": {{ $pos_no }},
                        "item": {

                            "@id": "{{ env( 'APP_URL' ) }}{{\App::getLocale()}}/{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($data,'residential_commercial','')) }}/{{ kebab_case_custom(array_get($data,'city','')) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom( array_get($data,'area',''), true ) }}-area",
                            "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $data, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $data, 'area', '' ))) : array_get( $data, 'area', '' ) }}"

                        }
                    }
                @endif
                @if( array_get($data,'building',false) )
                    @php
                        $pt    =   array_get($data,'type',false) ? kebab_case_custom(array_get($data,'type','')) : 'properties';

                        $pos_no++;
                    @endphp

                    ,{
                        "@type": "ListItem",
                        "position": {{ $pos_no }},
                        "item": {

                            "@id": "{{ env( 'APP_URL' ) . \App::getLocale() . '/' }}{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get($data,'residential_commercial','')) }}/{{ kebab_case_custom(array_get( $data, 'city', 'dubai' )) }}/{{ kebab_case_custom(array_get($data,'area',''),true) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom( array_get($data,'building',''), true ) }}-building",
                            "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $data, 'building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $data, 'building', '' ))) : array_get( $data, 'building', '' ) }}"

                        }
                    }
                @endif
            ]
        }
    </script>
    {{--  Residence Type  --}}
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Residence",
            "name": "{{ preg_replace('/\s+?(\S+)?$/', '', substr(array_get( $data, 'writeup.title', '' ), 0, 45)) }}",
            "alternateName":"{{ array_get( $data, 'type', '' ) }} for {{ ucwords( array_get( $data, 'rent_buy', '' ) ). ' in ' . array_get( $data, 'area', '' ) }}",
            "description": "{{strip_tags( array_get( $data, 'description', '' ) )}}",
            "url": "{{ secure_url( URL::route( 'property-details-page', [ 'lng' => \App::getLocale(), 'rent_buy' => kebab_case_custom( array_get( $data, 'rent_buy', '' ) ), 'residential_commercial' => kebab_case_custom( array_get( $data, 'residential_commercial', '' ) ), 'name' => str_replace( ' ', '-', preg_replace( '/[!\'#]/' , '' , strtolower(array_get( $data, 'writeup.title', '' )))), 'id' => array_get($data,'_id','') ] ) ) }}",

            @if ( array_get( $data, 'phone', false ) )
                "telephone": "{{ array_get( $data, 'phone', '' ) }}",
            @else
                "telephone" : "055-1234567",
            @endif

            @if( array_get( $data, 'images.original', false ) )
                "image": "{{array_get( $data, 'images.original.0', '' )}}",
            @endif

            "address":  {

                "@type": "PostalAddress",
                "streetAddress": "{{ rtrim(array_get( $data, 'building', '' ),',') }}",
                "addressLocality": "{{ array_get( $data, 'area', '' ) }}",
                "addressRegion": "{{ array_get( $data, 'city', '' ) }}",
                "addressCountry":  {
                    "@type": "Country",
                    "name": "{{ __( 'page.global.uae' ) }}"
                }
            },

            "containedIn":  {

                "@type": "Place",

                @if ( array_get( $data, 'building', false ))

                    "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $data, 'building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $data, 'building', '' ))) : array_get( $data, 'building', '' ) }}",
                    "branchCode": "{{ array_get( $data, '_id', '' ) }}",
                    "url": "{{ env( 'APP_URL' ) . \App::getLocale() . '/' }}{{ kebab_case_custom($rb_link) }}/{{ kebab_case_custom(array_get( $data, 'residential_commercial', '' )) }}/{{ kebab_case_custom(array_get( $data, 'city', 'dubai' )) }}/{{ kebab_case_custom(array_get( $data, 'area', '' ), true) }}/{{ $pt }}-for-{{ kebab_case_custom($rb) }}-in-{{ kebab_case_custom( array_get( $data, 'building', '' ), true ) }}-building",

                @else

                    "name": "{{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $data, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $data, 'area', '' ))) : array_get( $data, 'area', '' ) }}",
                    "branchCode": "{{ array_get( $data, '_id', '' ) }}",
                    "url": "{{ env( "APP_URL" ). \App::getLocale() }}/{{ kebab_case_custom(array_get( $data, 'area', '' ), true) }}-area-in-{{ kebab_case_custom(array_get( $data, 'city', '' ), true) }}-city",

                @endif

                @if( array_get($data,'coordinates.lat',false) && array_get($data,'coordinates.lng',false) )
                    "geo" : {
                        "@type": "GeoCoordinates",
                        "latitude": "{{ array_get($data,'coordinates.lat',-1) }}",
                        "longitude": "{{ array_get($data,'coordinates.lng',-1) }}"
                    },
                @endif

                @php
                $propertyImages = array_get( $data, 'images.original', [] );
                @endphp

                "photo":[
                    @foreach( $propertyImages as $key => $image )
                        {

                            "@type": "CreativeWork",
                            "url": "{{$image}}"
                        }
                        @if( $key != count( $propertyImages )- 1  )
                        ,
                        @endif
                    @endforeach
                ]
            }
        }
    </script>
    {{--  Offer Type  --}}
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Offer",
            "priceCurrency":"AED",
            "price":"{{ array_get($data,'price',0) }}",
            "availability": "{{ secure_url( URL::route( 'property-details-page', [ 'lng' => \App::getLocale(), 'rent_buy' => kebab_case_custom( array_get( $data, 'rent_buy', '' ) ), 'residential_commercial' => kebab_case_custom( array_get( $data, 'residential_commercial', '' ) ), 'name' => str_replace( ' ', '-', preg_replace( '/[!\'#]/' , '' , strtolower(array_get( $data, 'writeup.title', '' )))), 'id' => array_get( $data, '_id', '' ) ] ) ) }}"
        }
    </script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFzsV4eOaewWg9ShwUkNSLhv6KNtKAjAU&libraries=places"></script>
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c52870689f1bd00113e1bab&product=inline-share-buttons' async='async'></script>

{{-- Setting form fields --}}
<script>
    $(document).mouseup(function(e){
        if(!$('.sharethis-inline-share-buttons').is(e.target)&&$('.sharethis-inline-share-buttons').has(e.target).length==0){
            $('.sharethis-inline-share-buttons').hide();
        }
    });
    $( document ).ready( function(){
        @notmobile
            pages.property.details.__init();
        @elsenotmobile
            pages.property.details.__init();
        @endnotmobile
        validate.rules.lead.details.__init();
    });
</script>
@endsection
