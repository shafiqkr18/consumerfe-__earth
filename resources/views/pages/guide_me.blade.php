@php
    $selectedLanguage	=	\App::getLocale();
    $property = [];
    $markers = ['bank','gas_station','gym','hospital','mosque','park','parking','school','shopping_mall'];
@endphp
@extends( 'layouts.guide_me' )

@section( 'guide_me' )
<div>
    <div id="guide_me">

        <!-- Map -->

        <div>
            <div id="guideMeMap"></div>
        </div>

        <!-- Pages Wrap -->

        <div class="top_navigation_content">

            <div class="area_status_content">
                <div class="ui pointing secondary menu">
                    <div id="top_stats_bar"></div>
                </div>
                <div id="guide_me_area_stats_content"></div>
                <div class="ui tabular menu">
                    <a class="ui item active button" id="stats_tab_a" data-tab="a">{{ __('page.menu_breadcrumbs.buy') }}</a>
                    <a class="ui item button" id="stats_tab_b" data-tab="b">{{  __('page.menu_breadcrumbs.rent') }}</a>
                </div>
                <div class="ui tab active" id="stats_tab_content_a" data-tab="a">
                    <div>
                        <canvas id="guide_me_area_stats_a_trends"></canvas>
                    </div>
                </div>
                <div class="ui tab" id="stats_tab_content_b" data-tab="b">
                    <div>
                        <canvas id="guide_me_area_stats_b_trends"></canvas>
                    </div>
                </div>
                <div id="area_stats_notice"></div>
            </div>

            <div class="amenities_content">
                @foreach($markers as $marker)
                <div class="ui checkbox guide_me_amenities_checkbox" onclick="guide_me.map.landmarks.selected_markers('{{$marker}}',!$(this).checkbox('is checked'))">
                    <input type="checkbox" />
                    <div class="row">
                        <i class="big green check circle icon" id="guide_me_amenities_marker_{{$marker}}" style="display: none;"></i>
                        <img src="{{secure_asset('assets/img/guide_me/markers/'.$marker.'.png') }}" />
                    </div>
                    <div class="row">
                        <spam> {{ __('page.landmarks.'.$marker) }}</spam>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="view_on_map">
                <a href="#" onclick="guide_me.methods.menu.action(this, 'map');">{{ __('page.guideme.view_map') }}</a>
            </div>

        </div>

        <!-- Top header -->

        <div id="top_header">

            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="ui grid">
                        <div class="two wide column"><a href="/"><i class="angle left icon"></i></a></div>
                        <div class="twelve wide column">
                            <input class="autocomplete fluid" onfocus="this.value=''" placeholder="{{ __('page.guideme.enter_location') }}" value="{{ __('page.guideme.finding_location') }}" id="user-location" type="text">
                        </div>
                        <div class="two wide column">
                            <a href="#" onclick="guide_me.methods.clear_area_field();"><i class="close icon"></i></a>
                        </div>
                    </div>
                    <div class="ui pointing secondary menu on_map_only">
                        <div id="top_bar">
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Apartment', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/apartment.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.apartment') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Villa', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/villa.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.villa') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Townhouse', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/townhouse.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.townhouse') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Penthouse', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/penthouse.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.penthouse') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Hotel Apartment', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/hotel-apartment.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.hotel_apartment') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Duplex', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/duplex.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.duplex') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Compound', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/compound.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.compound') }}</figcaption>
                            </figure>
                            <figure class="item" onclick="guide_me.methods.prop_types_select('Loft Apartment', this);">
                                <div><img src="{{secure_asset('assets/img/guide_me/loft-apartment.png')}}" alt=""></div>
                                <figcaption>{{ __('page.guideme.loft_apartment') }}</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>

            <a onclick="guide_me.methods.top_bar.trigger();" class="on_map_only" id="top_bar_trigger"><i class="angle up icon"></i></a>

        </div>

        <!-- Properties -->

        <div class="ui grid" id="property_info">
            <a onclick="guide_me.methods.close_card_btn()"><i class="close icon"></i></a>
            <div class="sixteen wide column">
                <div id="guide_me_property_list" dir="ltr"></div>
            </div>
        </div>

        <!-- Rent & Buy buttons -->

        <div>
           <div class="ui grid gide_me_rent_buy_btn">
               <div class="eight wide column">
                   <button id="Sale" class="ui button fluid" onclick="guide_me.methods.rent_buy_btn('Sale', this)">{{ __('page.menu_breadcrumbs.buy') }}</button>
               </div>
               <div class="eight wide column">
                   <button id="Rent" class="ui button fluid" onclick="guide_me.methods.rent_buy_btn('Rent', this)">{{ __('page.menu_breadcrumbs.rent') }}</button>
               </div>
           </div>
        </div>

        <!-- Controls -->

        <div id="full_action_menu">
            <a href="#" onclick="guide_me.methods.filters.open();" id="show_filters_applied" class="on_map_only"><i class="filter icon"></i><span onclick="event.stopPropagation();guide_me.methods.filters.clear_all();" class="ui red circular label" style="display: none;"><i class="close icon"></i></span></a>
            <a href="#" onclick="guide_me.methods.zoom.in();" class="on_map_only"><i class="plus icon"></i></a>
            <a href="#" onclick="guide_me.methods.zoom.out();" class="on_map_only"><i class="minus icon"></i></a>
            <div id="action_sub_menu">
                <a href="#" onclick="guide_me.methods.menu.action(this, 'area_status');"><img src="{{secure_asset('assets/img/guide_me/icon-as.png')}}" alt="area status"/></a>
              {{-- UNCOMMENT TO BRING AMENITIES BACK <a href="#" onclick="guide_me.methods.menu.action(this, 'amenities');"><img src="{{secure_asset('assets/img/guide_me/icon-am.png')}}" alt="amenities"/></a> --}} 
                <a href="#" onclick="guide_me.methods.current_location();"><i class="location arrow icon"></i></a>
            </div>
            <a href="#" onclick="guide_me.methods.menu.trigger();" id="action_menu_trigger"><i class="bars icon"></i></a>
        </div>

        <!-- Refine form -->

        <div class="ui grid" id="refine_form">
            <div class="sixteen wide column">
                <i onclick="guide_me.methods.filters.close();" class="close icon"></i>
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <div>{{__('page.form.placeholders.budget')}}</div>
                        <div class="ui grid">
                            <div class="eight wide column">
                                <select class="ui dropdown fluid" id="guide_me_price_min"></select>
                            </div>
                            <div class="eight wide column">
                                <select class="ui dropdown fluid" id="guide_me_price_max"></select>
                            </div>
                        </div>
                    </div>
                    <div class="sixteen wide column">
                        <div>{{trans_choice(  __( 'page.global.bedroom'), 1 )}}</div>
                        <div class="ui grid">
                            <div class="eight wide column">
                                <select class="ui dropdown fluid" id="guide_me_bedroom_min"></select>
                            </div>
                            <div class="eight wide column">
                                <select class="ui dropdown fluid" id="guide_me_bedroom_max"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <button class="ui button fluid" type="button" name="button" onclick="guide_me.methods.refine_results();">{{__( 'page.global.search' ) }}</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
