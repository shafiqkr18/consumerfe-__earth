@extends( 'layouts.listings' )

@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.agent.listings')
@endsection

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile' )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky">
            {{-- @include( 'components.sections.search.agent' ) --}}
            @include( 'components.sections.banner.agent' )
        </div>
    @elsenotmobile
    <div>
        @include( 'components.sections.banner.mobile.agent' )
    </div>
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.agent.listings' )
    @elsenotmobile
        @include( 'components.sections.breadcrumbs.agent.listings' )
    @endnotmobile
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.agent.listings.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.agent.listings.mobile' )
    @endnotmobile
@endsection

@section( 'page_modals' )
    @notmobile
        @include( 'components.modals.agent.call.desktop_and_tablet' )
        @include( 'components.modals.agent.callback.desktop_and_tablet' )
        @include( 'components.modals.agent.email.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.modals.agent.call.mobile' )
        @include( 'components.modals.agent.callback.mobile' )
        @include( 'components.modals.agent.email.desktop_and_tablet' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
    $( document ).ready( function(){
        @notmobile
            pages.agent.listings.__init();
        @elsenotmobile
            pages.agent.listings.__init();
        @endnotmobile
    });
</script>
@endsection
