@extends( 'layouts.details' )

@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.agent.details')
@endsection

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile', [ 'logo' => 'purple' ] )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        {{-- <div id="sticky_header" class="ui sticky">
            <div class="ui container">
                @include( 'components.sections.search.agent' )
            </div>
        </div> --}}
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        {{-- @include( 'components.sections.breadcrumbs.agent.details' ) --}}
    @elsenotmobile
        {{-- @include( 'components.sections.breadcrumbs.agent.details' ) --}}
    @endnotmobile
@endsection

@section( 'page_templates' )
    @notmobile
        @include( 'components.cards.similar.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.cards.similar.mobile' )
    @endnotmobile
@endsection

@section( 'page_modals' )
    @notmobile
        @include( 'components.modals.agent.call.desktop_and_tablet' )
        @include( 'components.modals.agent.callback.desktop_and_tablet' )
        @include( 'components.modals.agent.email.desktop_and_tablet' )
        @include( 'components.modals.agent.review.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.modals.agent.call.mobile' )
        @include( 'components.modals.agent.callback.mobile' )
        @include( 'components.modals.agent.email.mobile' )
    @endnotmobile
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.agent.details.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.agent.details.mobile' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
    $( document ).ready( function(){
        @notmobile
            pages.agent.details.__init();
        @elsenotmobile
            pages.agent.details.__init();
        @endnotmobile
    });
</script>
@endsection
