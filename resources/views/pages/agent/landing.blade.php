@extends( 'layouts.landing' )

@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.agent.landing')
@endsection

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
    <div id="budget_search_header">
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'white' ] )
    </div>
    @elsenotmobile
        @include( 'components.sections.header.mobile' )
    @endnotmobile
@endsection

@section( 'banner' )
    @notmobile
        <div>
            @include( 'components.sections.banner.agent' )
        </div>
    @elsenotmobile
        <div>
            @include( 'components.sections.banner.mobile.agent' )
        </div>
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
    $( document ).ready( function(){
        @notmobile
            pages.agent.landing.__init();
        @elsenotmobile
            pages.agent.landing.__init();
        @endnotmobile
    });
</script>
@endsection
