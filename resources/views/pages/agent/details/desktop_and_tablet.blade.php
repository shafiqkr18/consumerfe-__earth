@php
  $review      = array_get($reviews,'data',[]);
 // $picture = empty(array_get($data,'contact.picture','')) || is_null(array_get($data,'contact.picture') || array_get($data,'contact.picture') == 'default') ?  'https://s3.ap-south-1.amazonaws.com/zp-uae-profile-images/'.urlencode(array_get($data,'contact.email')) : array_get($data,'contact.picture');
  $picture = env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($data,'contact.email','')));
  $track  = array_get($data,'agency.settings.lead.call_tracking.track',false);
  $number = $track ? array_get($data,'agency.settings.lead.call_tracking.phone') : array_get($data,'contact.phone','');
  $number = $number == '' ? array_get($data,'agency.contact.phone','') : $number;
  if($number[0] == 0){
    $number = substr($number,1);
  }
  $number = str_replace('971971','971','+971'.$number);
  array_set($data,'number',$number);

  $select_fields          =   config('data.datalayer.fields.agent');
  $data_for_datalayer     =   array_dot_only($data,$select_fields);

  $rateTotal = 0;
  $gral_rate = 0;
  if (count($review) > 0) {
    foreach ($review as $item) {
        $rateTotal += array_get($item, 'rate', 0);
    }
    $gral_rate = $rateTotal / count($review);
  }
  
@endphp
<div class="ui grid" id="pages_agent_details">
    <div style="background-image: url( '{{ cdn_asset( 'assets/img/banner/agent-search.png' ) }}' );">
        <div class="ui grid container">
            <div>
                <div>
                    <img class="f_p ui bordered image" data-type="listing" data-second="{{ cdn_asset('assets/img/default/no-agent-img.png') }}" src="{{$picture}}" alt="{{$seo_image_alt}}" />
                </div>
                <div>
                    <div>{{array_get($data,'contact.name',__( 'page.global.default_agent_name' ))}}</div>
                    
                    <div>
                    <div class="ui star rating" data-rating="{{$gral_rate}}" data-max-rating="5"></div>
                        <div>
                            @if (array_get($reviews,'total', 0) > 0)
                                <span>{{array_get($reviews,'total', 0)}}</span>
                                <span>{{ __('page.agent.details.reviews')}}</span>
                                <span>|</span>
                            @endif
                        </div>
                        <div>
                            {{--  
                            <span onclick="pages.agent.details.review.__init({{json_encode($data_for_datalayer)}});">{{ __('page.agent.details.reviews_me')}}</span>
                            <span>></span>
                            --}}
                        </div>
                    </div>
                    
                    <div class="item">{{__( 'page.agent.details.languages' )}}:
                        @if(!empty(array_get($data,'contact.languages','')))
                            {{implode(", ", !is_null(array_get($data,'contact.languages',null)) ? array_get($data,'contact.languages',[]) : [])}}
                        @else
                            {{ 'Not Specified' }}
                        @endif
                    </div>
                </div>
            </div>

            <div>
                <div>{{__( 'page.agent.details.contact_agent' )}}</div>
                <div data-_id="{{array_get($data,'_id','')}}" data-agent_contact_name="{{array_get($data,'contact.name','')}}" data-agency_contact_name="{{array_get($data,'agency.contact.name','')}}" data-agent_contact_phone="{{array_get($data,'number','')}}" class="ui buttons">
                    <button class="small ui purple button fa-dir-flip" onclick="pages.agent.listings.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                    {{-- <button class="small ui purple button fa-dir-flip" onclick="pages.agent.listings.callback.__init({{json_encode($data_for_datalayer)}});"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button> --}}
                    @notmobile
                        <button class="small ui purple button" onclick="pages.agent.listings.email.__init({{json_encode($data_for_datalayer)}});"><i class="envelope icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                    @elsenotmobile
                        <button class="small ui purple button" onclick="pages.agent.listings.whatsapp.__init({{json_encode($data_for_datalayer)}});"><i class="whatsapp icon"></i>{{__( 'page.card.actions.whatsapp' ) }}</button>
                    @endnotmobile
                </div>
            </div>
        </div>

    </div>

    <div class="ui grid container">
        <div class="eleven wide column">
            <div class="ui container grid">
                <div class="four wide column">
                </div>
                <div class="twelve wide column">
                    <div class="ui secondary pointing menu">
                        <a href="#aboutme" class="active item">{{__('page.agent.details.about_me')}}</a><span>|</span>
                        <a href="#properties" class="item">{{__( 'page.global.properties' )}}</a><span>|</span>
                        @if(array_get($reviews, 'total', 0) > 0)
                            <a href="#reviews" class="item">{{__('page.agent.details.reviews')}}</a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="ui container grid">
                <div class="sixteen wide column">
                    <div>{{__( 'page.agent.details.areas_covered' )}}</div>
                    <div class="ui four column grid">
                        @foreach (array_get($data, 'contact.areas', []) as $item)
                            <div class="column">
                                <span>{{$item}}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="ui container grid">
                @if (!empty(array_get($data, 'contact.specialty', [])))
                    <div class="sixteen wide column">
                        <div>{{__( 'page.agent.details.specialty' )}}</div>
                        <div class="ui four column grid">
                            @foreach (array_get($data, 'contact.specialty', []) as $item)
                                <div class="column">
                                    <span>{{$item}}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>

            @php $lenght = strlen(array_get($data,'contact.about','')); @endphp
            <div id="aboutme" class="ui container grid">
                @if((array_has($data, 'interview.youtube_id') && !empty(array_get($data, 'interview.youtube_id',''))) || (array_has($data, 'agency.interview.youtube_id') && !empty(array_get($data, 'agency.interview.youtube_id',''))))
                    <div class="eight wide column">
                        <img class="ui small image f_p" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"  src="http://i3.ytimg.com/vi/{{ array_get($data, 'interview.youtube_id') }}/maxresdefault.jpg" alt="{{$seo_image_alt}}" />
                        <a class="popup-video-youtube" href="https://www.youtube.com/watch?v={{array_get($data, 'interview.youtube_id')}}?autoplay=true">
                            <i class="icon play circle outline"></i>
                        </a>
                    </div>
                    <div class="eight wide column">
                        <div class="ui grid">
                            <div class="sixteen wide column">
                                <h3>{{__( 'page.global.about' ). ' ' . array_get($data,'contact.name',__( 'page.global.default_agent_name' ))}}</h3>
                            </div>
                            @if(!empty(array_get($data,'contact.about','')))
                            <div class="sixteen wide column">
                                <div id="descriptxt" class="@if ($lenght > 300)descriptxt @else descriptxt-collapsed @endif">{{array_get($data,'contact.about','')}}</div>
                                
                                <div>
                                    @if ($lenght > 300)
                                        <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_details', '5.3rem', 'descriptxt', 'angle', 'down', 'up', false, 'project.details.read_more', 'project.details.read_less');" class="read_more_details">
                                        {{ __( 'page.project.details.read_more') }}
                                        </a>
                                        <i id="angle" class="angle down icon"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                @else
                    <div></div>
                    <div class="sixteen wide column">
                        <div class="ui grid">
                            <div class="sixteen wide column">
                                <h3>{{__( 'page.global.about' ). ' ' . array_get($data,'contact.name',__( 'page.global.default_agent_name' ))}}</h3>
                            </div>
                            @if(!empty(array_get($data,'contact.about','')))
                            <div class="sixteen wide column">
                                <div id="descriptxt" class="@if ($lenght > 300)descriptxt @else descriptxt-collapsed @endif">{{array_get($data,'contact.about','')}}</div>
                                <div>
                                    @if ($lenght > 300)
                                        <a href="javascript:void(0);" onclick="helpers.view_more_less('read_more_details', '5.3rem', 'descriptxt', 'angle', 'down', 'up', false, 'project.details.read_more', 'project.details.read_less');" class="read_more_details">
                                        {{ __( 'page.project.details.read_more') }}
                                        </a>
                                        <i id="angle" class="angle down icon"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>

            <div id="properties" class="ui container grid">
                <div>{{__( 'page.agent.details.my_properties' )}}</div>
                <div class="sixteen wide column" id="property_card_list">
                    <div class="ui one column grid" id="agent_properties" data-_id="{{array_get($data,'_id','')}}"></div>
                </div>
            </div>

            {{-- Reviews --}}
            <div id="reviews" class="ui container grid">
                @if(array_get($reviews, 'total', 0) > 0)
                    <div class="sixteen wide column">{{__( 'page.agent.details.featured_review' )}} <span>({{array_get($reviews,'total', 0)}})</span></div>
                    
                    <div id="featured_review" class="@if (array_get($reviews, 'total', 0) < 4)load_auto @endif">
                        @foreach($review as $item)
                            <div class="ui reviews card grid">
                                <div class="three wide column">
                                    <div>
                                        <img class="f_p ui small circular image" data-type="listing" src="" data-second="{{cdn_asset('assets/img/default/no-agent-img.png')}}" alt="{{ $seo_image_alt }}" />
                                    </div>
                                </div>
                                <div class="thirteen wide column">
                                    <div>
                                        @if (array_get($item, 'rate', 0) == 5)
                                            {{__( 'page.agent.details.rate.excelent' )}}
                                        @elseif(array_get($item, 'rate', 0) == 4)
                                            {{__( 'page.agent.details.rate.good' )}}    
                                        @elseif(array_get($item, 'rate', 0) == 3)
                                            {{__( 'page.agent.details.rate.regular' )}}    
                                        @elseif(array_get($item, 'rate', 0) == 2)
                                            {{__( 'page.agent.details.rate.bad' )}}    
                                        @elseif(array_get($item, 'rate', 0) == 1)
                                            {{__( 'page.agent.details.rate.very_bad' )}}    
                                        @endif
                                    </div>
                                    <div>
                                        <div class="ui star rating" data-rating="{{array_get($item, 'rate')}}" data-max-rating="5"></div>
                                        <div>
                                            <span>{{array_get($item, 'rate').' '.__( 'page.agent.details.out_of' ).' 5,'}}</span>
                                            @php
                                                $date = date_create(array_get($item, 'created_at'));   
                                            @endphp
                                            <span>{{date_format($date,"F d, Y") }}</span>
                                        </div>
                                    </div>
                                    <div>{{array_get($item, 'ra_comment')}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                    <div>
                        @if (array_get($reviews, 'total', 0) > 4)
                            <a href="javascript:void(0);" onclick="helpers.view_more_less('view_more_reviews', '41rem', 'featured_review', '', '', '', false, 'agent.details.load_more', 'agent.details.load_less');" class="view_more_reviews">{{ __( 'page.agent.details.load_more' ) }}</a>
                        @endif
                    </div>
            </div>

        </div>
        <div class="five wide column">
            <div class="ui container grid">
                <div class="sixteen wide column">
                    <div>
                        <div>{{__( 'page.agent.details.company' )}}</div>
                        <div>{{array_get($data,'agency.contact.name',__( 'page.global.default_agency_name' ))}}</div>
                        <div class="item">{{__( 'page.agent.details.license' )}}:
                            <span>
                                @if(!empty(array_get($data,'contact.license_no','')))
                                    {{array_get($data,'contact.license_no','')}}
                                @else
                                    {{ 'Not Specified' }}
                                @endif
                            </span>
                        </div>
                        {{-- commented for next deployment --}}
                        {{-- <div><a href="#">{{__( 'page.agent.details.find_out_more' )}}</a></div> --}}
                    </div>
                    <div>
                        <img class="f_p"
                            data-type="listing"
                            alt="{{$seo_image_alt}}"
                            data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"
                            src="{{cdn_asset('agency/logo/'. array_get(explode('@',array_get($data,'agency.contact.email')), 1, ''))}}" alt="Zoom Property"/>
                    </div>
                </div>
                <div class="ui sticky enquiry">
                {{--     @include( 'components.modals.developer.enquiry.desktop_and_tablet' )--}}
                </div>
            </div>
        </div>
    </div>




</div>
