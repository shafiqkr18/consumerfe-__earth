<div class="ui container" id="pages_agent_listings">

    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{!! $seo_h1 !!}</h1>
            @if(array_get($data,'total', 0) === 0)
            <div class="no_results">
                <div class="ui card fluid grid">
                    <div class="ui grid">
                        <div class="sixteen wide column">
                            {{ __( 'page.results.no_results_found_title' ) }}<br />
                            {{ __( 'page.results.no_results_found_subtitle' ) }}
                        </div>
                        <div class="sixteen wide column">
                            <button type="button" id="new_search" class="small ui purple button new_search" data-page="agent" onclick="search.new_search(this)"> {{ __( 'page.form.button.new_search' ) }}</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="sixteen wide column" id="agent_card_grid">
            <div class="ui one column grid" id="render_agent_card">
                @foreach(array_get($data,'data',[]) as $key => $agent)
                    @if($loop->index <= 3)
                        @include( 'components.cards.agent' )
                    @endif
                @endforeach
            </div>

            @if(array_get($featured_agent,'total',0) !== 0)
                @include( 'components.cards.agent.featured.mobile' )
            @endif

            <div class="ui one column grid">
                @foreach(array_get($data,'data',[]) as $key => $agent)
                    @if($loop->index >= 4)
                        @include( 'components.cards.agent' )
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="ui grid">
        <div class="sixteen wide column mobile_paginate">
            <ul id="pagination"></ul>
        </div>
    </div>

    {{-- Hot Project Section --}}
    @if(isset($projects) && array_get($projects,'total',0) !== 0)
        <div class="ui grid">
            <div class="eight wide column">
                <h3>{{__( 'page.landing.hot_project' )}}</h3>
            </div>
            <div class="eight wide column">
                <a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">{{__( 'page.landing.view_all' )}} <i class="icon angle right"></i></a>
            </div>
        </div>

        <div id="hot-projects" class="ui grid">
            @foreach(array_get($projects,'data',[]) as $project)
                @include( 'components.cards.project.hot_project.mobile' )
            @endforeach
        </div>
    @endif

</div>
