<div class="ui container" id="pages_agent_listings">

    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{!! $seo_h1 !!}</h1>
            @if(array_get($data,'total', 0) === 0)
            <div class="ui grid">
                <div class="eleven wide column no_results">
                    <div class="ui card fluid grid">
                        <div class="ui grid">
                            <div class="twelve wide column">
                                {{ __( 'page.results.no_results_found_title' ) }} <br />
                                {{ __( 'page.results.no_results_found_subtitle' ) }}
                            </div>
                            <div class="four wide column">
                                <button type="button" id="new_search" class="small ui purple button new_search" data-page="agent" onclick="search.new_search(this)"> {{ __( 'page.form.button.new_search' ) }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="eleven wide column" id="agent_card_grid">
            <div class="ui two column grid" id="render_agent_card">
                @foreach(array_get($data,'data',[]) as $key => $agent)
                    @if($loop->index <= 3)
                        @include( 'components.cards.agent' )
                    @endif
                @endforeach
            </div>

            @if(array_get($featured_agent,'total',0) !== 0)
                @include( 'components.cards.agent.featured.desktop_and_table' )
            @endif

            <div class="ui two column grid">
                @foreach(array_get($data,'data',[]) as $key => $agent)
                    @if($loop->index >= 4)
                        @include( 'components.cards.agent' )
                    @endif
                @endforeach
            </div>
        </div>

        <div class="five wide column">
            <div>
                <!-- Agent listings page banner [async] -->
                <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                <script type="text/javascript">
                var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                var abkw = window.abkw || '';
                var plc317495 = window.plc317495 || 0;
                document.write('<'+'div id="placement_317495_'+plc317495+'"></'+'div>');
                AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 317495, [300,600], 'placement_317495_'+opt.place, opt); }, opt: { place: plc317495++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                </script>
            </div>

            <div>
                <!-- Agent listings page banner 2 [async] -->
                <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                <script type="text/javascript">
                var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                var abkw = window.abkw || '';
                var plc327478 = window.plc327478 || 0;
                document.write('<'+'div id="placement_327478_'+plc327478+'" on></'+'div>');
                AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 327478, [300,250], 'placement_327478_'+opt.place, opt); }, opt: { place: plc327478++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                </script>
            </div>
        </div>
    </div>

    <div class="ui grid">
        <div class="eleven wide column">
            <ul id="pagination"></ul>
        </div>
    </div>
    
    <div class="ui grid">
        <div class="eleven wide column">
            {{-- Hot Project Section --}}
            @if(isset($projects) && array_get($projects,'total',0) !== 0)
            <div class="ui grid">
                <div class="eight wide column">
                <h3>{{__( 'page.landing.hot_project' )}}</h3>
                </div>
                <div class="eight wide column">
                    <a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">{{__( 'page.landing.view_all' )}}</a>
                </div>
            </div>
            <div id="hot-projects" class="ui grid container">
                @foreach(array_get($projects,'data',[]) as $project)
                    @include( 'components.cards.project.hot_project.desktop_and_tablet' )
                @endforeach
            </div>
            @endif
        </div>
    </div>

</div>
