@extends( 'layouts.listings' )
@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
@notmobile
    @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
@elsenotmobile
    @include( 'components.sections.header.mobile' )
@endnotmobile
@endsection

@section( 'search' )
    @notmobile
    <div id="sticky_header" class="ui sticky">
        <div class="ui container">
            @include( 'components.sections.search.property' )
        </div>
    </div>
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.terms_and_conditions' )
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'page_content' )
<div id="terms_and_conditions" class="ui container">
    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{!! $seo_h1 !!}</h1>
            @php
                $content = (Lang::has('terms_conditions.content', \App::getLocale())) ? __( 'terms_conditions.content') : Lang::get('terms_conditions.content',[],'en');
            @endphp
            {!! $content !!}
        </div>
    </div>
    @endsection

    @section( 'footer' )

    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'page_scripts' )
    {{-- Setting form fields --}}
    <script>
    $( document ).ready( function(){
        @notmobile
            pages.landing.__init();
        @elsenotmobile
            pages.landing.__init();
        @endnotmobile
    });
    </script>
@endsection
