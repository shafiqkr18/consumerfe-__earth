@php $links = get_links(); @endphp
<div id="page_content">
    <div class="ui container">
        <div class="ui grid">
            <div class="sixteen wide column">
                <a class="item" href="{{ array_has($takeover,'web_link') ? \App::getLocale().array_get($takeover,'web_link') : '#'}}">
                     <img src="{{ array_has($takeover,'image_mobile') ? array_get($takeover,'image_mobile') : cdn_asset( 'assets/img/banner/landing-mobile.png' ) }}" alt="{{ $seo_image_alt }}" />
                </a>
            </div>
        </div>

        {{-- Searcher --}}
        <div class="ui grid">
            <div>
                <div class="ui grid">
                    <div class="ui buttons">
                        <button id="Sale" class="ui button rent_buy_btn active">{{ __('page.menu_breadcrumbs.buy') }}</button>
                        <button id="Rent" class="ui button rent_buy_btn">{{ __('page.menu_breadcrumbs.rent') }}</button>
                    </div>
                </div>
            </div>
            <div class="sixteen wide column">
                <div class="search_autocomplete">
                    <input id="property_suburb_landing" class="autocomplete fluid" placeholder="{{__('page.form.placeholders.location')}}"  type="text" />
                    <input id="property_suburb_landing_hidden" type="hidden" />
                    <div class="ui basic label" onclick="pages.landing.search.__init('Sale');">
                        <i class="search icon"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui grid">
            <div class="sixteen wide column">
                <div id="landing_ad">
                    <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                    <script type="text/javascript">
                    var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                    var abkw = window.abkw || '';
                    var plc317497 = window.plc317497 || 0;
                    document.write('<'+'div id="placement_317497_'+plc317497+'"></'+'div>');
                    AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 317497, [320,100], 'placement_317497_'+opt.place, opt); }, opt: { place: plc317497++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                    </script>
                </div>
            </div>
        </div>

        {{-- Popular Area --}}
        <div class="ui grid">
            <div class="sixteen wide column">
                {{__( 'page.landing.message_mobile' )}}
            </div>

            @include( 'components.sections.ilinks.home.quick_access.mobile' )
        </div>

        {{-- Hot Project Section --}}
        @if(isset($projects) && count($projects) !== 0)
            <div class="ui grid">
                <div class="eight wide column">
                    <h3>{{__( 'page.landing.hot_project' )}}</h3>
                </div>
                <div class="eight wide column">
                    <a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">{{__( 'page.landing.view_all' )}} <i class="icon angle right"></i></a>
                </div>
            </div>

            <div id="hot-projects" class="ui grid">
                @foreach($projects as $project)
                    @include( 'components.cards.project.hot_project.mobile' )
                @endforeach
            </div>
        @endif
        {{-- Guide me COMMENTED UNTIL RELEASE OF THE FEATURE APPROVED --}}
     <div class="ui grid">
            {{-- <div class="sixteen wide column">
                <h3>{{__( 'page.guideme.property_area' )}}</h3>
            </div> --}}
    </div>

        <div class="ui grid">
           {{-- <div class="sixteen wide column">
                <img class="ui fluid image" src="{{env('ASSET_URL').'assets/img/home/guideme.jpg'}}" alt="">
                <div class="layer"></div>
                <a class="ui primary button" href="{{route('guide-me', [ 'lng'=>\App::getLocale() ])}}">{{ __( 'page.guideme.menu_label' ) }}</a>
            </div> --}}
               
        </div>

        {{-- News & Articles --}}
        <div class="ui grid">
            <div class="sixteen wide column">
                <div class="ui grid">
                    <div class="eight wide column">
                        <h3>{{__( 'page.landing.message' )}}</h3>
                    </div>
                    <div class="eight wide column">
                        <a href="https://blog.zoomproperty.com/news/" target="_blank">{{__( 'page.landing.view_all' )}}<i class="icon angle right"></i></a>
                    </div>
                </div>

                <div class="ui grid container">
                    <div id="blog_wrapper">
                        @if(\App::getLocale() == 'en')
                        <div class="ui equal width grid" id="blog_content"></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        {{-- Popular Property searches --}}
        <div class="ui grid container">
            <div class="sixteen wide column">
                @if(!empty($popular_links))
                    @include( 'components.sections.ilinks.home.mobile' )
                @endif
            </div>
        </div>
    </div>
</div>
