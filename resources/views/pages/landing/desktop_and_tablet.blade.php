

<div id="page_content" class="ui container">

    {{-- @section( 'property_card' ) --}}

    @notmobile
    @if(!empty($popular_links))
    <div class="ui grid container">
        {{-- Popular areas --}}
        @include( 'components.sections.ilinks.home.quick_access.desktop_and_tablet' )
        {{-- AdButler --}}
        <div class="five wide column">
            <div class="landing_listings_ad">
                <!-- Property listings page banner [async] -->

                <!-- Home Page MPU 3a [async] -->
                <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                <script type="text/javascript">
                var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                var abkw = window.abkw || '';
                var plc444895 = window.plc444895 || 0;
                document.write('<'+'div id="placement_444895_'+plc444895+'"></'+'div>');
                AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 444895, [300,600], 'placement_444895_'+opt.place, opt); }, opt: { place: plc444895++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                </script>

            </div>
        </div>

    </div>
    @endif

    <div class="ui grid container">
        <h2 class="ui dividing header"></h2>
    </div>

    <div class="ui grid container">
        <div class="eleven wide column">
            {{-- Hot Project Section --}}
            @if(isset($projects) && count($projects) !== 0)
            <div class="ui grid">
                <div class="eight wide column">
                <h3>{{__( 'page.landing.hot_project' )}}</h3>
                </div>
                <div class="eight wide column">
                    <a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">{{__( 'page.landing.view_all' )}}</a>
                </div>

            </div>
            <div id="hot-projects" class="ui grid container">
                @foreach($projects as $project)
                    @include( 'components.cards.project.hot_project.desktop_and_tablet' )
                @endforeach
            </div>
            @endif

            {{-- Latest Videos Section --}}
            @if(isset($publicityvideos) && array_get($publicityvideos,'total',0) !== 0)





            <div class="ui grid">
                <div class="eight wide column">
                    <h3>{{__( 'page.landing.latest_video' )}}</h3>
                </div>
                {{-- <div class="eight wide column">
                    <a href="#">{{__( 'page.landing.view_all' )}}</a>
                </div> --}}
            </div>
            <div class="ui grid container">
                @foreach(array_get($publicityvideos,'data',[]) as $publicityvideo)
                <div class="ui five wide column reveal">
                    <div class="visible content">
                        <img class="ui small image f_p" data-type="listing"  src="{{array_get($publicityvideo,'thumbnail')}}" alt="{{$seo_image_alt}}" />
                        <div class="layer"></div>
                        @if(!str_contains( array_get($publicityvideo, 'youtube_id'), 'http') )
                            <a class="popup-video-youtube" href="https://www.youtube.com/watch?v={{ array_get($publicityvideo, 'youtube_id') }}?autoplay=true">
                        @else
                            <a class="popup-video-youtube" href="{{ array_get($publicityvideo, 'youtube_id') }}">
                        @endif
                            <i class="icon play circle"></i>
                        </a>
                        <h5>{{array_get($publicityvideo, 'title')}}</h5>
                    </div>
              </div>
                @endforeach
            </div>
            @endif
        </div>
        
        <div class="five wide column">
            <div class="landing_listings_ad">
                <!-- Property listings page banner [async] -->
                <!-- Home Page MPU 3 [async] -->
                <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                <script type="text/javascript">
                var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                var abkw = window.abkw || '';
                var plc444851 = window.plc444851 || 0;
                document.write('<'+'div id="placement_444851_'+plc444851+'"></'+'div>');
                AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 444851, [300,600], 'placement_444851_'+opt.place, opt); }, opt: { place: plc444851++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                </script>
            </div>
        </div>
    </div>

    {{-- News and Articles --}}
    <div class="ui grid container">
        <div class="eleven wide column">
            <div class="ui grid">
                <div class="eight wide column">
                    <h3>{{__( 'page.landing.message' )}}</h3>
                </div>
                <div class="eight wide column">
                    <a href="https://blog.zoomproperty.com/news/" target="_blank">{{__( 'page.landing.view_all' )}}</a>
                </div>
            </div>

            <div class="ui grid container">
                <div id="blog_wrapper">
                    @if(\App::getLocale() == 'en')
                    <div class="ui equal width grid" id="blog_content"></div>
                    @endif
                </div>
            </div>
        </div>
        <div class="five wide column">
            <div class="sixteen wide column">
                <div class="landing_listings_ad_single">
                    <!-- small ad banner -->

                    <!-- Listing right hand MPU [async] -->
                    <!-- Home page MPU 1 [async] -->
                    <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
                    <script type="text/javascript">
                    var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                    var abkw = window.abkw || '';
                    var plc327478 = window.plc327478 || 0;
                    document.write('<'+'div id="placement_327478_'+plc327478+'"></'+'div>');
                    AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 327478, [300,250], 'placement_327478_'+opt.place, opt); }, opt: { place: plc327478++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
                    </script>
                </div>
            </div>
        </div>
    </div>

    <div class="ui grid container">
        <h2 class="ui dividing header"></h2>
    </div>









    @if(!empty($popular_links))
    @include( 'components.sections.ilinks.home.desktop_and_tablet' )
    @endif

    @endnotmobile

    {{-- @endsection --}}

    {{-- @yield( 'property_card' ) --}}

</div>
