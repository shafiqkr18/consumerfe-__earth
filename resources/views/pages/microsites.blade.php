<!doctype html>
@php
    $locale	=	\App::getLocale();
    $path   =   'assets/microsites/img/amenities/';
    // Get icon names as array of strings
    $files  =   array_diff(scandir($path), array('.', '..'));
    $icons  =   array_map(function($i){ return pathinfo($i, PATHINFO_FILENAME); },$files);
    $links  =   get_links();
    $localization = array_has($microsite, 'localization') ? array_get($microsite,'localization') : [];
@endphp

<html lang="en" dir="{{ ( $locale === 'ar' ) ? 'rtl' : 'ltr' }}" class="{{ ( $locale === 'ar' ) ? 'fa-dir-flip' : '' }}">
<head>

    <title>{{ array_get($data,'writeup.title') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('marketing.datalayer.projects.details')
    @include( 'marketing.header' )

    <link href="{{ cdn_asset( 'assets/favicon.ico' ) }}?v=6" rel="shortcut icon" />
    <link href="{{ cdn_asset('assets/dependencies/intlTelInput.min.css') }}?v=6" rel="stylesheet" />
    @if( in_array( $locale, [ 'ar' ] ) )
		<link href="{{ cdn_asset('semantic/dist/semantic.rtl.min.css') }}" rel="stylesheet" />
		<link href="{{ cdn_asset('font-awesome/font-awesome.rtl.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ secure_asset('assets/microsites/css/styles.rtl.min.css') }}?v=6" />
	@else
		<link href="{{ cdn_asset('semantic/dist/semantic.min.css') }}?v=6" rel="stylesheet" />
        <link rel="stylesheet" href="{{ secure_asset('assets/microsites/css/styles.min.css') }}?v=6" />
	@endif

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W69347X');</script>
    <!-- End Google Tag Manager -->

</head>
<body class="microsite">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W69347X"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="projects_details" data-id="{{ array_get($data,'_id',false) }}" data-name="{{ array_get($data,'writeup.title') }}" class="above-the-fold">

        <div class="background-banner" style="background-image:url('{{array_get($data,'image.banner')}}')"></div>

        <!-- <div id="toast_microsite" style="display: none;"></div> -->

        <header>
            <div class="ui container">
                <div class="ui grid">
                    <div class="six wide column">
                        <div class="branding">
                            <a href="{{ env( 'APP_URL' ).$locale.'/'.array_get($microsite,'developer.slug') }}">
                                <img src="{{ array_get($data,'developer.contact.logo','') }}" alt="Logo" class="inverted" />
                            </a>
                        </div>
                    </div>
                    <div class="ten wide column">
                        <div class="phone_no">
                            <bdo>
                                <a href="tel:+{{ array_get($data,'developer.settings.lead.call_tracking.phone') }}" class="call-lead track_call">
                                    <i class="phone icon"></i>
                                    <span data-phone="{{ (array_get($data,'developer.settings.lead.call_tracking.track') === true ) ? array_get($data,'developer.settings.lead.call_tracking.phone') : array_get($data,'developer.contact.phone') }}">
                                        {{ __('page.form.button.call') }}
                                    </span>
                                </a>
                            </bdo>
                            @if(count($localization) > 1)
                            <bdo class="localization">
                                <a href="{{ array_get($links,'en') }}" class="{{ ( \App::getLocale() === 'en' && in_array('en',$localization) ) ? 'hide' : 'item' }}">
                                    <i class="gb flag"></i><span>English</span>
                                </a>
                                <a href="{{ array_get($links,'ar') }}" class="{{ ( \App::getLocale() === 'ar' && in_array('ar',$localization) ) ? 'hide' : 'item' }}">
                                    <i class="ae flag"></i><span style="vertical-align:top;">عربى</span>
                                </a>
                            </bdo>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="banner">
            <div class="ui container">
                <div class="ui grid">
                    <div class="eight wide computer sixteen wide mobile column">
                        <div class="text">
                            @php
                                $project_titles     =   [];
                                $titles['en']       =  array_get($microsite,'titles',[]);
                                if($locale != 'en'){
                                    $titles[$locale]    =  array_get($microsite,'titles_'.$locale,[]);
                                }
                                $projects           =  array_get($microsite,'projects',[]);
                                foreach($projects as $key => $project){
                                    foreach (range(0, 3) as $i){
                                        $project_titles[$project][$i]   =   ($locale !== 'en' && !empty($titles[$locale][$project][$i]) ) ? $titles[$locale][$project][$i] : (!empty($titles['en'][$project][$i]) ? $titles['en'][$project][$i] : '');
                                    }
                                }
                                $titles =   array_get($project_titles,array_get($data,'_id',''));
                            @endphp
                            <h2><span>{{ array_has($titles,0) ? array_get($titles,0) : '' }}</span>{{ array_has($titles,1) ? array_get($titles,1) : '' }}</h2>
                            <h3><span>{{ array_has($titles,2) ? array_get($titles,2) : '' }}</span>{{ array_has($titles,3) ? array_get($titles,3) : '' }}</h3>
                        </div>
                        <form class="form" id="bookingForm">
                            <fieldset>
                                <input type="text" name="name" id="customer-name" placeholder="{{ __('page.form.placeholders.full_name') }} *" required />
                                <input type="email" name="email" id="customer-email" placeholder="{{ __('page.form.placeholders.email') }} *" required />
                            </fieldset>
                            <fieldset>
                                <input type="tel" name="phone" id="customer-phone" placeholder="{{ __('page.form.placeholders.phone') }} *" required />
                                <select name="bedrooms" id="customer-interest" required>
                                    <option value="">{{ __('microsites.form_bedrooms') }} *</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </fieldset>
                            <fieldset>
                                <input class="track_email" type="submit" value="{{ __('microsites.i_am_intrested') }}">
                                <div id="loader"></div>
                                <span id="toast_microsite" style="display: none;"></span>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="projects-tabs" id="projectsTabs">
            @php
                $projects   =   array_get($microsite,'projects');
                $counter    =   1;
            @endphp
            @foreach ( $projects as $key => $project )
                @php
                    $ref_no     =   str_after($project, 'zj-');
                    $logo       =   'https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/projects/'.$ref_no.'/logo/'.$ref_no;
                    $thumb      =   'https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/projects/'.$ref_no.'/gallery/mobile/0';
                @endphp
                <div @if( $counter%2 === 0 ) class="even" @endif>
                    <a href="{{ env( 'APP_URL' ).$locale.'/'.array_get($microsite,'developer.slug') }}/{{ $project }}">
                        <span class="logo">
                            <img src="{{ $logo }}" alt="" class="{{ $project }}" />
                        </span>
                        <span class="thumb">
                            <img src="{{ $thumb }}" alt="" class="{{ $project }}" />
                        </span>
                    </a>
                </div>
                @php $counter++; @endphp
            @endforeach
        </div>

    </div>

    <div class="payment-plans">
        <div class="ui container">
            <div class="ui grid centered">
                <div class="fourteen wide computer sixteen wide mobile column">
                    <h3>{{ __('page.project.details.payment_plan') }}</h3>
                    <div class="box-wrap">
                        @foreach ( array_get($data,'pricing.payment_plans',[]) as $plan )
                            @php
                                $detail = explode( '|', $plan );
                                $i = strpos($detail[1], ' ');
                                $amount = ($i == false) ? $detail[1] : current(explode( ' ', $detail[1] ));
                            @endphp
                            <div class="box">
                                <span class="label">{{ $detail[0] }}</span>
                                <!-- Getting first word only -->
                                <span class="amount">{{ $amount }}</span>
                                <span class="label"><strong>{{ $detail[2] }}</strong></span>
                            </div>
                        @endforeach
                    </div>
                    <p>* @if( array_get($data,'expected_completion_date', false) !== 'Completed' ) {{ __('meraas.handover') }} {{ array_get($data,'expected_completion_date', false) }} @else {{ array_get($data,'expected_completion_date', false) }} @endif</p>
                </div>
            </div>
        </div>
    </div>

    <div class="about-text">
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <h3>Overview</h3>
                    {!! preg_replace('/(<[^>]+) style=".*?"/i', '$1', str_replace( '<p>&nbsp;</p>', '', str_replace( 'div>', 'p>', array_get($data,'writeup.description') ) ) ) !!}
                    <hr>
                    <div class="features">
                        @foreach ( array_get($data,'unit.amenities',[]) as $amenity )
                            @php
                                $amenity_name = strtolower( str_replace( '/', '', str_replace( ' ', '-', $amenity ) ) );

                                $amenity_name = closest_string($icons,$amenity_name);
                                $src = secure_asset('assets/microsites/img/amenities/'.$amenity_name.'.png');

                                //if(in_array($amenity_name,$icons)){
                                //    $src = secure_asset('assets/microsites/img/amenities/'.$amenity_name.'.png');
                                //}
                                //else{
                                //    $src = secure_asset('assets/microsites/img/amenities/amenity.png');
                                //}
                            @endphp
                            <span><img src="{{$src}}" alt="{{$amenity}}"> {{$amenity}} </span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider">
        <div class="slides">
            @foreach ( array_get($data,'image.portal.desktop',[]) as $image )
                <div style="background: url({{ $image }}) center center no-repeat;"></div>
            @endforeach
        </div>
    </div>

    <div class="medias">
        <div class="ui container">
            <div class="ui grid centered">
                <div class="fourteen wide computer sixteen wide mobile column">
                    @if( preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", array_get($data,'video.normal.link'), $matches ) )
                        <figure>
                            <div class="ui video" data-source="youtube" data-id="{{ $matches[ 0 ] }}" ></div>
                        </figure>
                    @else
                    @endif
                </div>
                <div class="fourteen wide computer sixteen wide mobile column">
                    <div class="floor-plans">
                        <h3>{{ __('page.project.details.tab_titles.floor_plans') }}</h3>
                        <div class="ui top attached tabular menu">
                            @foreach ( array_get($data,'image.floor_plans',[]) as $floor_plan )
                                <div class="item {{ $loop->first ? 'active' : '' }}" data-tab="tab-{{ $loop->index }}">{{array_get($floor_plan,'name','')}}</div>
                            @endforeach
                        </div>
                        @foreach ( array_get($data,'image.floor_plans',[]) as $floor_plan )
                            <div class="ui bottom attached {{ $loop->first ? 'active' : '' }} tab segment" data-tab="tab-{{ $loop->index }}">
                                <div class="layouts">
                                    @foreach(array_get($floor_plan,'plans',[]) as $plan)
                                    <div><img src="{{array_get($plan,'img','')}}" alt="{{array_get($plan,'type','')}}"></div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="map_canvas" data-lat="{{ array_get($data,'coordinates.lat',false) }}" data-lng="{{ array_get($data,'coordinates.lng',false) }}"></div>

    <footer>
        <div class="ui container">
            <div class="ui grid">
                <div class="five wide computer sixteen wide mobile column">
                    <div class="logo">
                        <a href="{{ env( 'APP_URL' ) }}">
                            <img src="{{ cdn_asset('sprites.png') }}" style="object-fit:none;object-position:0 -52px;width: 111px;height: 56px;" alt="Zoom Property">
                        </a>
                    </div>
                </div>
                <div class="six wide computer sixteen wide mobile column">
                    <ul class="social-menu">
                        <li>
                            <a href="{{config('data.footer.links.facebook')}}" target="_blank">
                                <i class="facebook f icon"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{config('data.footer.links.twitter')}}" target="_blank">
                                <i class="twitter icon"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{config('data.footer.links.instagram')}}" target="_blank">
                                <i class="instagram icon"></i>
                            </a>
                        </li>
                        @if(false)
                        <li>
                            <a href="{{config('data.footer.links.google')}}" target="_blank">
                                <i class="google plus g icon"></i>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="five wide computer sixteen wide mobile column">
                    <div class="apps">
                        <a href="{{config('data.footer.links.app_store')}}" target="_blank">
                            <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" style="object-fit:none;object-position:-115px -43px;width: 120px;height: 40px;" class="responsive-img">
                        </a>
                        <a href="{{config('data.footer.links.play_store')}}" target="_blank">
                            <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" style="object-fit:none;object-position:-99px 0;width: 134px;height: 40px;" class="responsive-img">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script>
        var baseApiUrl  =   '{{ env( "APP_URL" ) }}api/{{ env( "API_VERSION" ) }}/',
            envUrl 	    =	'{{ env( "APP_URL" ) }}',
            cdnUrl 				=	'{{ env( "CDN_URL" ) }}',
            locale      =   {!! json_encode($locale) !!};
    </script>

    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBFzsV4eOaewWg9ShwUkNSLhv6KNtKAjAU'></script>
    <script type="text/javascript" src="{{ secure_asset('assets/microsites/js/script.min.js') }}?v=3"></script>

</body>
</html>
