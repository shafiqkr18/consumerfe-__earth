@extends( 'layouts.listings' )
@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.services.listings')
@endsection

@section('meta_tags')
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'purple' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile' )
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky">
            <div class="ui container">
                @include( 'components.sections.search.services' )
            </div>
        </div>
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'breadcrumbs' )
    @notmobile
        @include( 'components.sections.breadcrumbs.services' )
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.services.listings.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.services.listings.mobile' )
    @endnotmobile
@endsection

@section( 'page_modals' )
    @notmobile
        @include( 'components.modals.services.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.modals.services.mobile' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
    $( document ).ready( function(){
        @notmobile
            pages.services.__init();
        @elsenotmobile
            pages.services.__init();
        @endnotmobile
    });
</script>
@endsection
