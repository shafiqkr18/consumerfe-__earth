<div class="ui container" id="pages_services_landing">
    <div class="ui grid">
        <div class="sixteen wide column" id="services_card">
            <div class="ui link three cards">
                @foreach( $data as $service_type )
                <a class="card" href="services/{{array_get($service_type,'_id','')}}/uae">
                    <div class="image"><img data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" src="{{array_get($service_type,'image')}}" alt="{{$seo_image_alt}}" /></div>
                    <div class="content">
                        <div class="header">
                            {{ ($locale !== 'en' && Lang::has('data.ancillary.'.snake_case_custom(array_get($service_type,'name')), $locale)) ? __( 'data.ancillary.'.snake_case_custom(array_get($service_type,'name'))) : array_get($service_type,'name','' )}}
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
