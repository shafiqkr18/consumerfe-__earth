<div class="ui container" id="pages_services_listings">

    <div class="ui grid">
        <div class="sixteen wide column">
            <h1>{{ $seo_h1 }}</h1>
            <div class="ui link one cards">
                @foreach(array_get($data,'data',[]) as $key => $service)
                @php
                    $name   =   ($locale !== 'en' && array_get($service,'contact.name_'.$locale) != '') ? array_get($service,'contact.name_'.$locale) : array_get($service,'contact.name',__( 'page.global.default_service_name' ));
                    $title  =   ($locale !== 'en' && array_get($service,'writeup.title_'.$locale) != '') ? array_get($service,'writeup.title_'.$locale) : array_get($service,'writeup.title',__( 'page.global.default_service_name' ));
                    $desc   =   ($locale !== 'en' && array_get($service,'writeup.description_'.$locale) != '') ? array_get($service,'writeup.description_'.$locale) : array_get($service,'writeup.description',__( 'page.services.listings.default_description' ));
                @endphp
                <div class="card">
                    <div class="ui grid">
                        <div class="five wide column">
                            <div class="image">
                                <img class="f_p" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" src="{{array_get($service,'contact.logo','')}}" alt="{{$seo_image_alt}}" />
                            </div>
                        </div>
                        <div class="eleven wide column">
                            <div class="content">
                                <div class="header">{{$title}}</div>
                                <div class="description">{{$desc}}</div>
                            </div>
                            <div class="extra content">
                                <span>
                                    <a href="{{array_get($service,'contact.website','#')}}" target="_blank">{{__( 'page.global.visit_wesite' )}}</a>
                                </span>
                                <span class="right floated" data-service_contact_name="{{$name}}" data-service_logo="{{array_get($service,'contact.logo','')}}" data-service_description="{{$desc}}" data-service_id="{{array_get($service,'_id','')}}">
                                    <button type="button" class="small ui purple button" name="button" onclick="pages.services.listings.contact.__init(this);">{{__( 'page.form.button.contact' )}}</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>
