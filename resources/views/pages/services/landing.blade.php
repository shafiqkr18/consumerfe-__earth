@extends( 'layouts.landing' )

@section( 'title', $seo_title )
@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.services.landing')
@endsection

@section('meta_tags')
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
    <div id="budget_search_header">
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'white' ] )
    </div>
    @elsenotmobile
        @include( 'components.sections.header.mobile' )
    @endnotmobile
@endsection

@section( 'banner' )
    @notmobile
        <div id="services_banner">
            @include( 'components.sections.banner.services' )
        </div>
    @endnotmobile
@endsection

@section( 'search' )
    @notmobile
        <div id="sticky_header" class="ui sticky">
            <div class="ui container">
                @include( 'components.sections.search.services' )
            </div>
        </div>
    @elsenotmobile

    @endnotmobile
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.services.landing.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.services.landing.mobile' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>
    $( document ).ready( function(){
        @notmobile
            pages.services.__init();
        @elsenotmobile
            pages.services.__init();
        @endnotmobile
    });
</script>
@endsection
