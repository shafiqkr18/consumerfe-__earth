<!doctype html>
@php
    $selectedLanguage	=	\App::getLocale();
    $search		=	config( 'search' );
@endphp

<html lang="{{ $selectedLanguage }}" dir="{{ ( $selectedLanguage === 'ar' ) ? 'rtl' : 'ltr' }}">
<head>
    <title>{{ $seo_title }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{ $seo_description }}" />

    <link href="{{ cdn_asset( 'semantic/dist/semantic.min.css') }}?v=1" rel="stylesheet" />
    <link rel="shortcut icon" href="{{ cdn_asset( 'assets/favicon.ico' ) }}" />
    <link rel="stylesheet" href="{{ secure_asset('assets/real_choice/styles.min.css') }}?v=2.2" />

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W69347X');</script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe id="gtm-iframe" src="https://www.googletagmanager.com/ns.html?id=GTM-W69347X"
                      height="0" width="0"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

</head>
<body>

    <div class="above-the-fold">

        <header>
            <div class="ui container">
                <div class="ui grid">
                    <div class="four wide computer sixteen wide mobile column">
                        <ul class="social-menu">
                            <li><a href="https://www.facebook.com/realchoicedubai/" target="_blank" class="fb"></a></li>
                            <li><a href="https://twitter.com/realchoicedubai" target="_blank" class="tw"></a></li>
                            <li><a href="https://www.linkedin.com/company/real-choice-real-estate-brokers-llc" target="_blank" class="in"></a></li>
                            <li><a href="https://www.instagram.com/realchoicedubai/" target="_blank" class="it"></a></li>
                        </ul>
                        <!--<div class="phone_no mobile only"><a href="tel:+971 424 51703"><bdo dir="ltr">+971 424 51703</bdo></a></div>-->
                    </div>
                    <div class="eight wide computer sixteen wide mobile column">
                        <div class="branding"><a href="#"><img src="{{ secure_asset('assets/real_choice/img/logo.png') }}" alt="Real Choice Logo"></a></div>
                    </div>
                    <div class="four wide computer only column">
                        <div class="phone_no">
                            <!--<a href="tel:+971 424 51703"><bdo dir="ltr">+971 424 51703</bdo></a>-->
                            <a href="{{ env('APP_URL') }}{{ ( $selectedLanguage === 'ar' ) ? 'en' : 'ar' }}/real-choice"><img src="{{ secure_asset('assets/real_choice/img/' . ( ( $selectedLanguage === 'ar' ) ? 'en' : 'ar' ) . '.jpg') }}" alt="{{$seo_image_alt}}">{{ ( $selectedLanguage === 'ar' ) ? 'EN' : 'AR' }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="banner">
            <div class="ui container">
                <div class="ui grid">
                    <div class="column">
                        <div class="text">
                            <img src="{{ secure_asset('assets/real_choice/img/banner-text-' . $selectedLanguage . '.png') }}" alt="Marina Vista at EMAAR Beach Front">
                        </div>
                    </div>
                </div>
                <div class="ui grid">
                    <div class="three wide computer sixteen wide mobile column">
                        <div class="blue-box">
                            {{ __('real_choice.book_for') }} <span><img src="{{ secure_asset('assets/real_choice/img/text-5percent.png') }}" alt="5%"></span> <small>{{ __('real_choice.rera_no') }}</small>
                        </div>
                    </div>
                    <div class="five wide computer only column">&nbsp;</div>
                    <div class="eight wide computer sixteen wide mobile column">
                        <div class="text2">
                            <h5>{{ __('real_choice.estimated_completion_date') }}</h5>
                            <h3>{{ __('real_choice.book_from') }} <strong>{{ __('real_choice.cost') }}</strong></h3>
                            <h5>{{ __('real_choice.book_now') }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-wrap">
        <div class="ui container">
            <div class="ui grid">
                <form class="column" id="bookingForm" method="post" action="">
                    <h3>{{ __('real_choice.book_viewing') }}</h3>
                    <fieldset>
                        <input type="text" autocomplete="off" name="name" id="customer-name" placeholder="{{ __('real_choice.full_name') }}" required>
                        <input type="email" autocomplete="off" name="email" id="customer-email" placeholder="{{ __('real_choice.email') }}" required>
                        <input type="tel" autocomplete="off" name="phone" id="customer-phone" placeholder="{{ __('real_choice.telephone') }}" required>
                        <select name="interest" id="customer-interest" required>
                            <option value="">{{ __('real_choice.option') }}</option>
                            <option value="a">{{ __('real_choice.option_a') }}</option>
                            <option value="b">{{ __('real_choice.option_b') }}</option>
                            <option value="c">{{ __('real_choice.option_c') }}</option>
                        </select>
                        <input type="submit" class="track_property_email_submit" value="{{ __('real_choice.submit') }}">
                    </fieldset>
                    <fieldset>
                        <input type="checkbox" name="newsletter" value="Yes" checked id="customer-newsletter">
                        <label for="customer-newsletter">{{ __('real_choice.register_me') }}</label>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

    <div class="payment-plans">
        <div class="ui container">
            <div class="ui grid centered">
                <div class="fourteen wide computer sixteen wide mobile column">
                    <h3>{{ __('real_choice.payment_plans') }}</h3>
                    <div class="ui grid centered">
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.1st_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.1st_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.2nd_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.2nd_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.3rd_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.3rd_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.4th_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.4th_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.5th_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.5th_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.6th_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.6th_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.7th_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.7th_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.8th_label') }}</span>
                                <span class="amount">5<span>%</span></span>
                                <span class="label2">{{ __('real_choice.8th_label2') }}</span>
                            </div>
                        </div>
                        <div class="five wide computer eight wide tablet sixteen wide mobile column">
                            <div class="box">
                                <span class="label">{{ __('real_choice.9th_label') }}</span>
                                <span class="amount">60<span>%</span></span>
                                <span class="label2">{{ __('real_choice.9th_label2') }}</span>
                            </div>
                        </div>
                    </div>
                    <!--<p>{{ __('real_choice.estimated_completion') }}</p>-->
                    <div class="image"><img src="{{ secure_asset('assets/real_choice/img/logo-emaar.png') }}" alt="Emaar logo"></div>
                    <p>{{ __('real_choice.brought_to_you') }}</p>
                    <div class="image"><img src="{{ secure_asset('assets/real_choice/img/logo.png') }}" alt="Real choice logo"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="medias">
        <div class="ui">
            <div class="ui grid">
                <div class="eight wide computer sixteen wide mobile column">
                    <figure>
                        <div class="ui video" data-source="youtube" data-id="xQZHB9umV6s" data-image="{{ secure_asset('assets/real_choice/img/video_thumb.jpg') }}"></div>
                        <span class="overlay">{{ __('real_choice.video') }}</span>
                    </figure>
                </div>
                <div class="eight wide computer sixteen wide mobile column">
                    <div class="slider">
                        <div class="slides" dir="ltr">
                            <div><img src="{{ secure_asset('assets/real_choice/img/1.png') }}" alt="floor plan 1"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/2.png') }}" alt="floor plan 2"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/3.png') }}" alt="floor plan 3"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/4.png') }}" alt="floor plan 4"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/5.png') }}" alt="floor plan 5"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/6.png') }}" alt="floor plan 6"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/7.png') }}" alt="floor plan 7"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/8.png') }}" alt="floor plan 8"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/9.png') }}" alt="floor plan 9"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/10.png') }}" alt="floor plan 10"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/11.png') }}" alt="floor plan 11"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/12.png') }}" alt="floor plan 12"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/13.png') }}" alt="floor plan 13"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/14.png') }}" alt="floor plan 14"></div>
                            <div><img src="{{ secure_asset('assets/real_choice/img/15.png') }}" alt="floor plan 15"></div>
                        </div>
                        <span class="overlay">{{ __('real_choice.floor_plans') }}</span>
                        <div id="dots"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="ui container">
            <div class="ui grid">
                <div class="eight wide computer sixteen wide mobile column">
                    <h3>{{ __('real_choice.newsletter') }}</h3>
                    <form action="#" method="post" id="subscribeForm">
                        <fieldset>
                            <input type="email" id="user-email" placeholder="{{ __('real_choice.email') }}" required>
                            <input type="submit" value="{{ __('real_choice.submit') }}">
                        </fieldset>
                    </form>
                    <ul class="social-menu">
                        <li><a href="https://www.facebook.com/realchoicedubai/" target="_blank" class="fb"></a></li>
                        <li><a href="https://twitter.com/realchoicedubai" target="_blank" class="tw"></a></li>
                        <li><a href="https://www.linkedin.com/company/real-choice-real-estate-brokers-llc" target="_blank" class="in"></a></li>
                        <li><a href="https://www.instagram.com/realchoicedubai/" target="_blank" class="it"></a></li>
                    </ul>
                </div>
                <div class="four wide computer only column">&nbsp;</div>
                <div class="four wide computer sixteen wide mobile column">
                    <h3>{{ __('real_choice.information') }}</h3>
                    <address>{{ __('real_choice.address') }}<br/><br/>
                        <span>Tel:</span> +971-4-2451703<br/>
                        <span>Fax:</span> +971-4-3588090<br/>
                        <span>Email:</span> info@dubaifirsthome.com<br/>
                        <span>Web:</span> www.dubaifirsthome.com
                    </address>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript" src="{{ secure_asset('assets/real_choice/video.min.js') }}"></script>
    <script>
        jQuery(document).ready(function( $ ){

            $('#bookingForm').on( 'submit', function(e){
                e.preventDefault();
                var name        =   jQuery('#customer-name').val(),
                    email       =   jQuery('#customer-email').val(),
                    phone       =   jQuery('#customer-phone').val(),
                    interest    =   jQuery('#customer-interest').val(),
                    newsletter  =   jQuery('#customer-newsletter').prop('checked');
                jQuery.ajax({
                    method: 'post',
                    url: "{{ env( 'APP_URL' ) }}api/earth/enquiry/book_viewing",
                    data: {
                        name: name,
                        email: email,
                        phone: phone,
                        interest: interest,
                        newsletter: newsletter
                    },
                    async: false,
                    headers: { 'x-api-key': 'NDaqOxp7IY3SZA2csTNoZ4V9jzzET3J43f32BOav' },
                    success:function(response){
                        alert( 'Your request is submitted successfully!' );
                        document.getElementById("bookingForm").reset();
                    },
                    error:function (error) {
                        alert( 'Something went wrong! Try again.' );
                        document.getElementById("bookingForm").reset();
                    }
                });
            });

            $('#subscribeForm').on( 'submit', function(e){
                e.preventDefault();
                var email       =   jQuery('#user-email').val();
                jQuery.ajax({
                    method: 'post',
                    url: "https://gua31qothi.execute-api.ap-south-1.amazonaws.com/beta/user/newsletter/subscribe/" + email,
                    async: true,
                    crossDomain: true,
                    headers: { 'x-api-key': 'NDaqOxp7IY3SZA2csTNoZ4V9jzzET3J43f32BOav' },
                    success:function(response){
                        alert( 'Your are subscribed successfully!' );
                        document.getElementById("subscribeForm").reset();
                    },
                    error:function (error) {
                        alert( 'Something went wrong! Try again.' );
                        document.getElementById("subscribeForm").reset();
                    }
                });
            });

            $('.slides').slick({
                arrows: false,
                dots: true,
                appendDots: '#dots',
                autoplay: true,
                autoplaySpeed: 2000
            });

            $('.ui.video').video();

        });
    </script>

</body>
</html>
