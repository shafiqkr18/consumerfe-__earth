@extends( 'layouts.landing' )
@section( 'title', __( 'page.global.seo_title' ) )

@section( 'description', __( 'page.global.seo_description' ) )
@section( 'meta_tags' )
     
@endsection
@section( 'page_content' )
<div id="reset_password">
    <div class="">
        <div class="ui grid">
            <div class="sixteen wide column">
                <div class="ui card container">
                    <div class="">
                        <img class="img-responsive" src="{{ cdn_asset('assets/img/logo-purple.png') }}" alt="Zoom Property" />
                    </div>
                    <div class="ui form">
                        <div class="">
                            We have recieved your reset password request. Please enter your new password:
                        </div>
                        <div class="ui input fluid" id="reset_password_email">
                            <input type="text" name="" placeholder="Enter your email" value="">
                        </div>
                        <div class="ui input fluid" id="reset_password_new_password">
                            <input type="password" name="" placeholder="New password" value="">
                        </div>
                        <div class="ui input fluid" id="reset_password_confirm_password">
                            <input type="password" name="" placeholder="Confirm password" value="">
                        </div>
                    </div>
                    <p id="change_password_error" class="error"></p>
                    <button type="button" onclick="pages.reset_password.change_password(this);" class="ui button fluid purple" name="button">Change password</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section( 'page_scripts' )
{{-- Setting form fields --}}
<script>

$( document ).ready( function(){
    @notmobile
        pages.reset_password.__init();
    @elsenotmobile
        pages.reset_password.__init();
    @endnotmobile
});

</script>
@endsection
