<!doctype html>
@php
    $selectedLanguage	=	\App::getLocale();
    $search		=	config( 'search' );
@endphp

<html lang="{{ $selectedLanguage }}" dir="{{ ( $selectedLanguage === 'ar' ) ? 'rtl' : 'ltr' }}">
<head>

    <title>{{ $seo_title }}</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{ $seo_description }}" />

    <link rel="shortcut icon" href="{{ cdn_asset( 'assets/favicon.ico' ) }}" />

    @if( $selectedLanguage === 'ar' )
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
    @else
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @endif

    <link rel="stylesheet" href="{{ secure_asset('assets/scholarship/styles.css') }}" />

</head>
<body>

    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-6">
                    <a href="#">
                        <img src="{{ secure_asset('assets/scholarship/images/logo.png') }}">
                    </a>
                </div>
                <div class="col-md-7 text-center main-nav">
                    <nav>
                        <ul>
                            <li><a href="#who_we_are">Who are we?</a></li>
                            <li><a href="#scholarship">Scholarship</a></li>
                            <li><a href="#how_to_apply">How to Apply?</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-3 col-6 social-nav">
                    <ul class="text-right">
                        <li><a href="https://www.facebook.com/ZoomPropertyUAE/" class="fb" target="_blank"></a></li>
                        <li><a href="https://twitter.com/zoompropertyuae" class="tw" target="_blank"></a></li>
                        {{--<li><a href="#" class="lnk" target="_blank"></a></li>--}}
                        {{--<li><a href="#" class="yt" target="_blank"></a></li>--}}
                        <li><a href="https://www.instagram.com/zoompropertyuae/" class="insta" target="_blank"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="main-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner-text">
                        <h2>Supporting students<br>WORLDWIDE</h2>
                        <p>Here at Zoom Property, we want to<br>help you with your studies.</p>
                        <a href="#apply_now" class="butn">Apply Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wwr-wrapper" id="who_we_are">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-2"></div>
                <div class="col-md-10 text-right">
                    <a href="#">
                        <img src="{{ secure_asset( 'assets/scholarship/images/video-thumbnil.jpg' ) }}">
                    </a>
                    <div class="wwr-text text-left">
                        <article>
                            <h2>Who are we?</h2>
                            <p>Hey there! We’re ZoomProperty, an online real estate portal for those looking to buy or rent properties in the UAE. If you are searching for a new home, an investment, a holiday house, or a luxurious villa in Dubai you will find everything on our property portal.</p>
                            <a href="{{env('APP_URL')}}" class="butn" target="_blank">visit website</a>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scholarship-wrapper" id="scholarship">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Scholarship Details</h2>
                    <p>We know education in today’s world is expensive. Therefore, here at Zoom Property, we want to help you with your studies. We are therefore giving away $1,000 Scholarships so that you can pay your tuition fee, buy books or whatever you need to buy during your school.</p>
                    <p>We are looking for Students who have helped their classmates or community in any way. For example, you have started an online campaign in your school campus on social awareness. Whatever you have done for others we would love to hear about it.</p>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="scholr-text text-left">
                        <article>
                            <h3>Eligibility</h3>
                            <ul>
                                <li class="e-i">This scholarship is only open for students attending high school / college or a university during 2019-2020.</li>
                                <li class="e-ii">Students studying in accredited colleges or universities are also eligible to avail this scholarship.</li>
                                <li class="e-iii">Students from all fields of study and GPAs above (2.7) are allowed to apply.</li>
                            </ul>
                            <a href="#apply_now" class="butn">Apply Now</a>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="apply-wrapper" id="how_to_apply">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>How to Apply?</h2>
                    <p>To apply, you need to submit a short piece of 700 words in writing. Choose one of the following<br> topics from the given list.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 ">
                    <fieldset>
                        <figure>
                            <img src="{{ secure_asset( 'assets/scholarship/images/img-2.png' ) }}">
                            <span>01</span>
                        </figure>
                        <article>
                            <p>Provide an example or case study of how you have helped your community, your school or your fellow students in the past</p>
                        </article>
                    </fieldset>
                </div>
                <div class="col-md-4 ">
                    <fieldset>
                        <figure>
                            <img src="{{ secure_asset( 'assets/scholarship/images/img-3.png' ) }}">
                            <span>02</span>
                        </figure>
                        <article>
                            <p>Provide an overview of what extra-curricular activities you take part in, outside of school or college, which helps with your personal development and learning.</p>
                        </article>
                    </fieldset>
                </div>
                <div class="col-md-4 ">
                    <fieldset>
                        <figure>
                            <img src="{{ secure_asset( 'assets/scholarship/images/img-4.png' ) }}">
                            <span>03</span>
                        </figure>
                        <article>
                            <p>What would your advice be to new students who are just starting out at your school to help them in their education and development?</p>
                        </article>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Recipients will be selected based on the overall strength of their application materials.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="form-wrapper" id="apply_now">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form" id="scholarshipForm">
                        <h3>Please Submit your application</h3>
                        <form action="#">
                            <div class="clearfix">
                                <div class="float-left">
                                    <label>Email address *</label>
                                    <input type="email" name="email" id="scholarship-email" placeholder="Your email" required>
                                </div>
                                <div class="float-right">
                                    <label>Name *</label>
                                    <input type="text" name="name" id="scholarship-name" placeholder="Your name" required>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="float-left">
                                    <label>What college are you attending? *</label>
                                    <input type="text" name="college" id="scholarship-college"  placeholder="Your answer" required>
                                </div>
                                <div class="float-right">
                                    <label>What degree and major are you pursuing? *</label>
                                    <input type="text" name="degree" id="scholarship-degree" placeholder="Your answer" required>
                                </div>
                            </div>
                            <div>
                                <label>What is your current GPA? *</label>
                                <input type="text" name="gpa" id="scholarship-gpa" placeholder="Your answer" required>
                            </div>
                            <div>
                                <label>Do you have additional awards, experiences, skills, or extracurriculars that you'd like to<br>include in your application? (Optional)</label>
                                <input type="text" name="additional_info" id="scholarship-aditional" placeholder="Your answer" required>
                            </div>
                            <div>
                                <label>Select your Article Title</label>
                            </div>
                            <div class="clearfix check-boxes">
                                <input type="checkbox" name="checklist" class="float-left" id="scholarship-checklist">
                                <label class="float-right" for="checklist">What is your checklist or guidelines you follow while searching for an apartment for rent? Consider the following points in your mind(Budget, Location & Negotiation).</label>
                            </div>
                            <div class="clearfix check-boxes">
                                <input type="checkbox" name="trends" class="float-left" id="scholarship-trends">
                                <label class="float-right" for="trends">What is the latest real estate marketing trends, best practices, and orthodox methods, that gives you the best ROI (Return on investment)?</label>
                            </div>
                            <div class="clearfix check-boxes">
                                <input type="checkbox" name="advice" id="scholarship-advice" class="float-left">
                                <label class="float-right" for="advice">What would be your housing advice for students at your school: Write a piece of advice you would give to first-time student renters?</label>
                            </div>
                            <div class="clearfix">
                                <label for="essay">Submit an essay of 700-1000 words *</label>
                                <input type="file" name="file" id="scholarship-essay" required>
                                <input type="hidden" name="MAX_FILE_SIZE" value="9485760" />
                            </div>
                            <div>
                                <input type="submit" name="submit" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="deadline-sec text-center clearfix">
                        <div class="dl-heading">
                            <h3>Deadlines</h3>
                        </div>
                        <div class="clearfix float-left text-left">
                            <span class="float-left ">01</span>
                            <h4 class="float-left">Last day of write up submission:<strong>1st November 2019.</strong></h4>
                        </div>
                        <div class="clearfix float-left text-left">
                            <span class="float-left">02</span>
                            <h4 class="float-left" >Screening of Applications: <strong>15th November 2019.</strong></h4>
                        </div>
                        <div class="clearfix float-left text-left">
                            <span class="float-left">03</span>
                            <h4 class="float-left" >Winner Announcement:<strong>15th December 2019.</strong></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container according-wrap">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>Scholarship FAQs</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne">When is the scholarship application deadline?</h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    The scholarship is awarded once a year. Winner will be selected between 1st - 15th December.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0" data-toggle="collapse" data-target="#collapseTwo">How will application information be used?</h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0" data-toggle="collapse" data-target="#collapseThree">When will the winner be chosen and how will they be notified?</h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @php
        $menu_links     =   array_get($popular_links, 'menu');
        $content_links  =   array_get($popular_links, 'content');
    @endphp

    <div class="most-popular-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ __( 'page.global.most_popular' ) }}</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row tabs-sec no-gutters" id="v-pills-container">
                <div class="col-md-4">
                    <div class="nav flex-column nav-pills" id="v-pills-tab mp-tab" role="tablist" aria-orientation="vertical">
                        @php $i = 0; @endphp
                        @foreach($menu_links as $menu_type => $menu_rent_buy_value)
                        @if($loop->index !== 3)
                        @foreach($menu_rent_buy_value as $menu_key => $menu_value)
                        <a class="nav-link {{ $i == 0 ? 'active' : '' }}" title="{{ array_get($menu_value, 'title', '') }}" id="{{ $menu_key }}-tab" data-toggle="pill" href="#{{ $menu_key }}" role="tab" aria-controls="{{ $menu_key }}" aria-selected="true">
                             {{ array_get($menu_value, 'title', '') }}
                        </a>
                        @php $i++; @endphp
                        @endforeach
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content" id="v-pills-tabContent">
                        @php $j = 0; @endphp
                        @foreach($content_links as $content_key => $content_value)
                        @if($loop->index !== 3)
                        <div class="tab-pane fade show {{ $j == 0 ? 'active' : '' }} clearfix" id="{{ $content_key }}" role="tabpanel" aria-labelledby="{{ $content_key }}-tab">
                            @foreach($content_value as $r_b_k => $r_b_v)
                            @foreach($r_b_v as $k => $residential_commercial)
                            <ul>
                                @foreach($residential_commercial as $key => $value)
                                <li>
                                    <a href="{{ array_get($value, 'link', '') }}" title="{{ array_get($value, 'title', '') }}">
                                        {{ str_limit(array_get($value, 'label', ''),35) }}
                                        ({{ array_get($value, 'count', '') }})
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            @endforeach
                            @endforeach
                        </div>
                        @php $j++; @endphp
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-6">
                    <p>&copy; <a href="https://zoomproperty.com">zoomproperty.com</a></p>
                </div>
                <div class="col-md-6 col-6">
                    <div class="social-nav">
                        <ul class="text-right">
                            <li><a href="https://www.facebook.com/ZoomPropertyUAE/" class="fb" target="_blank"></a></li>
                            <li><a href="https://twitter.com/zoompropertyuae" class="tw" target="_blank"></a></li>
                            {{--<li><a href="#" class="lnk" target="_blank"></a></li>--}}
                            {{--<li><a href="#" class="yt" target="_blank"></a></li>--}}
                            <li><a href="https://www.instagram.com/zoompropertyuae/" class="insta" target="_blank"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        jQuery(document).ready(function( $ ){

            $('#scholarshipForm').on( 'submit', function(e){
                e.preventDefault();
                var data    =   {
                    name        :   jQuery('#scholarship-name').val(),
                    email       :   jQuery('#scholarship-email').val(),
                    college     :   jQuery('#scholarship-college').val(),
                    degree      :   jQuery('#scholarship-degree').val(),
                    gpa         :   jQuery('#scholarship-gpa').val(),
                    aditional   :   jQuery('#scholarship-aditional').val(),
                    checklist   :   jQuery('#scholarship-checklist').is(':checked') ? 'checklist or guidelines': '',
                    trends      :   jQuery('#scholarship-trends').is(':checked') ? 'latest marketing trends': '',
                    advice      :   jQuery('#scholarship-advice').is(':checked') ? 'housing advice': '',
                },
                file = jQuery('#scholarship-essay')[0].files[0];
                var formData = new FormData();
                formData.append('data', JSON.stringify(data));
                formData.append('file', file);
                jQuery.ajax({
                    method: 'post',
                    url: "{{ env( 'APP_URL' ) }}api/earth/enquiry/scholarship",
                    data: formData,
                    processData: false,
                    contentType: false,
                    async: false,
                    headers: { 'x-api-key': 'NDaqOxp7IY3SZA2csTNoZ4V9jzzET3J43f32BOav' },
                    success:function(response){
                        alert( 'Your request is submitted successfully!' );
                        // document.getElementById("scholarshipForm").reset();
                    },
                    error:function (error) {
                        alert( 'Something went wrong! Try again.' );
                        // document.getElementById("scholarshipForm").reset();
                    }
                });
            });

        });
    </script>

</body>
</html>
