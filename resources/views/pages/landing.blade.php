@extends( 'layouts.landing' )
@section( 'title', $seo_title )

@section( 'description', $seo_description )

@section( 'dataLayer' )
    @include('marketing.datalayer.landing')
@endsection

@section( 'meta_tags' )
    <meta name="robots" @if(env('APP_ENV')=='production') content="noodp" @else content="noindex" @endif />
@endsection

@section( 'header' )
    @notmobile
        @include( 'components.sections.header.desktop_and_tablet', [ 'logo' => 'white' ] )
    @elsenotmobile
        @include( 'components.sections.header.mobile' )
    @endnotmobile
@endsection

@section( 'banner' )
    @notmobile
        @include( 'components.sections.banner.landing' )
    @elsenotmobile
    @endnotmobile
@endsection

@section( 'page_content' )
    @notmobile
        @include( 'pages.landing.desktop_and_tablet' )
    @elsenotmobile
        @include( 'pages.landing.mobile' )
    @endnotmobile
@endsection

@section( 'footer' )
    @notmobile
        @include( 'components.sections.footer.desktop_and_tablet' )
    @elsenotmobile
        @include( 'components.sections.footer.mobile' )
    @endnotmobile
@endsection

@section( 'page_scripts' )
<script>
    $( document ).ready( function(){
        @notmobile
            pages.landing.__init();
        @elsenotmobile
            pages.landing.__init();
        @endnotmobile
    });
</script>
@endsection
