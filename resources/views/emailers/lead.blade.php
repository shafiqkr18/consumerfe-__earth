<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta charset="utf-8">
  <!-- utf-8 works for most cases -->
  <meta name="viewport" content="width=device-width">
  <!-- Forcing initial-scale shouldn't be necessary -->
  <meta http-equiv="x-ua-compatible" content="IE=edge">
  <!-- Use the latest (edge) version of IE rendering engine -->
  <meta name="x-apple-disable-message-reformatting">
  <!-- Disable auto-scale in iOS 10 Mail entirely -->
  <title></title>
  <!-- The title tag shows in email notifications, like Android 4.4. -->
  <!-- Web Font / @font-face : BEGIN -->
  <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
  <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
  <!--[if mso]>
  <style>
  * {
  font-family:sans-serif !important;
}
</style>
<![endif]-->
<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
<!--[if !mso]><!-->
<!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
<!--<![endif]-->
<!-- Web Font / @font-face : END -->
<!-- CSS Reset : BEGIN -->
<!-- CSS Reset : END -->
<!-- Progressive Enhancements : BEGIN -->
<!-- Progressive Enhancements : END -->
<!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
<!--[if gte mso 9]>
<xml>
<o:OfficeDocumentSettings>
<o:AllowPNG />
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->

<style type="text/css">
html,body{
  margin:0 auto !important;
  padding:0 !important;
  height:100% !important;
  width:100% !important;
}
*{
  -ms-text-size-adjust:100%;
  -webkit-text-size-adjust:100%;
}
div[style*=margin: 16px 0]{
  margin:0 !important;
}
table,td{
  mso-table-lspace:0 !important;
  mso-table-rspace:0 !important;
}
table{
  border-spacing:0 !important;
  border-collapse:collapse !important;
  table-layout:fixed !important;
  margin:0 auto !important;
}
table table table{
  table-layout:auto;
}
img{
  -ms-interpolation-mode:bicubic;
}
[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
  border-bottom:0 !important;
  cursor:default !important;
  color:inherit !important;
  text-decoration:none !important;
  font-size:inherit !important;
  font-family:inherit !important;
  font-weight:inherit !important;
  line-height:inherit !important;
}
.a6S{
  display:none !important;
  opacity:.01 !important;
}
img.g-img+div{
  display:none !important;
}
.button-link{
  text-decoration:none !important;
}
@media only screen and (min-device-width: 375px) and (max-device-width: 413px){
  .email-container{
    min-width:375px !important;
  }
}
@media screen and (max-width: 480px){
  div>u div .gmail{
    min-width:100vw;
  }
}
html,body{
  margin:0 auto !important;
  padding:0 !important;
  height:100% !important;
  width:100% !important;
}
*{
  -ms-text-size-adjust:100%;
  -webkit-text-size-adjust:100%;
}
div[style*=margin: 16px 0]{
  margin:0 !important;
}
table,td{
  mso-table-lspace:0 !important;
  mso-table-rspace:0 !important;
}
table{
  border-spacing:0 !important;
  border-collapse:collapse !important;
  table-layout:fixed !important;
  margin:0 auto !important;
}
table table table{
  table-layout:auto;
}
img{
  -ms-interpolation-mode:bicubic;
}
[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
  border-bottom:0 !important;
  cursor:default !important;
  color:inherit !important;
  text-decoration:none !important;
  font-size:inherit !important;
  font-family:inherit !important;
  font-weight:inherit !important;
  line-height:inherit !important;
}
.a6S{
  display:none !important;
  opacity:.01 !important;
}
img.g-img+div{
  display:none !important;
}
.button-link{
  text-decoration:none !important;
}
@media only screen and (min-device-width: 375px) and (max-device-width: 413px){
  .email-container{
    min-width:375px !important;
  }
}
@media screen and (max-width: 480px){
  div>u div .gmail{
    min-width:100vw;
  }
}
.button-td,.button-a{
  transition:all 100ms ease-in;
}
@media screen and (max-width: 600px){
  .email-container{
    width:100% !important;
    margin:auto !important;
  }
}
@media screen and (max-width: 600px){
  .fluid{
    max-width:100% !important;
    height:auto !important;
    margin-left:auto !important;
    margin-right:auto !important;
  }
}
@media screen and (max-width: 600px){
  .stack-column,.stack-column-center{
    display:block !important;
    width:100% !important;
    max-width:100% !important;
    direction:ltr !important;
  }
}
@media screen and (max-width: 600px){
  .stack-column-center{
    text-align:center !important;
  }
}
@media screen and (max-width: 600px){
  .center-on-narrow{
    text-align:center !important;
    display:block !important;
    margin-left:auto !important;
    margin-right:auto !important;
    float:none !important;
  }
}
@media screen and (max-width: 600px){
  table.center-on-narrow{
    display:inline-block !important;
  }
}
@media screen and (max-width: 600px){
  .email-container p{
    font-size:13px !important;
  }
}
</style>
</head>

@php

if( array_has( $mail, 'property.property_ref_no' ) ){
  $propertyInfo = '<tr><td bgcolor="#ffffff" style="padding:20px 10px 20px;text-align:center;"><h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;text-align:center;"><a href="'.$mail[ 'property_link' ].'" target="_blank" style="text-decoration:none;color:#5d0e8b;font-weight:bold;">'.
    $mail[ 'property' ][ 'title' ].'</a></h1></td></tr>';

    $propertyInfo .= '<tr><td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;"><p style="margin:0;color:#555555;text-align:center;font-size: 13px;">';

      if( !empty( $mail[ 'property' ][ 'property_ref_no' ] ) )
      $propertyInfo .= 'Reference No: '.$mail[ 'property' ][ 'property_ref_no' ];

      if( !empty( $mail[ 'property' ][ 'suburb' ] ) )
      $propertyInfo .= '<br>'.$mail[ 'property' ][ 'suburb' ];

      if( !empty( $mail[ 'property' ][ 'city' ] ) )
      $propertyInfo .= ', '.$mail[ 'property' ][ 'city' ];

      $propertyInfo .= '</p></td></tr>';
      $propertyInfo .= '<tr><td bgcolor="#ffffff" style="padding:0 10px 20px;font-family:sans-serif;font-size:16px;line-height:140%;color:#555555;text-align:center;"><p style="margin:0;color:#555555;text-align:center;font-size:13px">';

        if( !empty( $mail[ 'property' ][ 'property_type' ] ) )
        $propertyInfo .= $mail[ 'property' ][ 'property_type' ];

        if( !empty( $mail[ 'property' ][ 'bedroom' ] ) )
        $propertyInfo .= ' | '.$mail[ 'property' ][ 'bedroom' ].' Bed'.( $mail[ 'property' ][ 'bedroom' ] > 1  ? 's ' : ' ' );

        if( !empty( $mail[ 'property' ][ 'bathroom' ] ) )
        $propertyInfo .= ' | '.$mail[ 'property' ][ 'bathroom' ].' Bath'.( $mail[ 'property' ][ 'bathroom' ] > 1 ? 's ' : ' ' );

        if( !empty( $mail[ 'property' ][ 'builtup_area' ] ) )
        $propertyInfo .= ' | '.$mail[ 'property' ][ 'builtup_area' ].' sq.ft';

        $propertyInfo .= '</p></td></tr>';
      }
      @endphp

      <body width="100%" bgcolor="#fff" style="margin: 0; mso-line-height-rule: exactly;">
        <center style="width:100%;background:#fff;text-align:left;">
          <!-- Email Header : BEGIN -->
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
            <tr>
              <td style="padding:10px 0;text-align:center;">
                <img src="https://gallery.mailchimp.com/9e8bad10f4764df10d34cb5e3/images/9b77fb59-9a07-4bca-8a9d-4b96152df92c.png" width="100" height="50" alt="alt_text" border="0" style="height: auto; background: #fff; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
              </td>
            </tr>
          </table>
          <!-- Email Header : END -->
          <!-- Email Body : BEGIN -->
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
              <td bgcolor="#ffffff" style="padding:20px 10px 20px;text-align:center;">
                <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#333333;font-weight:normal;text-align:center;">
                  {{ $mail[ 'title' ] }} {{ $mail[ 'receiver_name' ] }}!
                </h1>
              </td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" style="padding:0 10px 20px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                <p style="margin:0;color:#555555;text-align:center;">{{ $mail[ 'content' ] }}</p>
              </td>
            </tr>

            @if( !empty( $mail[ 'button_link' ]) && !empty($mail[ 'button_label' ] ) )
            <tr>
              <td bgcolor="#ffffff" style="padding:10px 10px 0px;;font-family:sans-serif;font-size:15px;line-height:140%;color:#555555;text-align:center;">
                <a href="{{ $mail[ 'button_link' ] }}" style="background:#5d0e8b;border:15px solid #5d0e8b;font-family:sans-serif;font-size:12px;line-height:1.1;margin:0 auto;text-align:center;text-decoration:none;display:block;width:50%;border-radius:2px;" class="button-a">
                  <span style="color:#ffffff;" class="button-link">{{ $mail[ 'button_label' ] }}</span>
                </a>
              </td>
            </tr>
            @endif

            <!-- 1 Column Text + Button : BEGIN -->
            @if( $mail[ 'send_to' ] === 'consumer' )
            @if( !empty( $mail[ 'agent' ][ 'email' ] ) )
            <tr>
              <td bgcolor="#ffffff" style="padding:20px 10px 0px;text-align:center;">
                <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:bold;text-align:center;">{{ $mail[ 'agent' ][ 'name' ] }}</h1>
              </td>
            </tr>
            @endif
            @if( !empty( $mail[ 'agent' ][ 'email' ] ) )
            <tr>
              <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                <p style="margin:0;color:#555555;text-align:center;">
                  @if( !empty( $mail[ 'agent' ][ 'email' ] ) )
                  {{ $mail[ 'agent' ][ 'email' ] }}
                  @elseif( !empty( $mail[ 'agent' ][ 'phone' ] ) )
                  | {{ $mail[ 'agent' ][ 'phone' ] }} |
                  @endif
                </p>
              </td>
            </tr>
            @endif
            @endif

            @if( $mail[ 'send_to' ] !== 'consumer' )
            <tr>
              <td bgcolor="#ffffff" style="padding:20px 10px 0px;text-align:center;">
                <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:bold;text-align:center;">{{ $mail[ 'lead' ][ 'name' ] }}</h1>
              </td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                <p style="margin:0;color:#555555;text-align:center;">
                  {{ !empty ($mail[ 'lead' ][ 'email' ]) && $mail[ 'lead' ][ 'email' ] !== 'anonymous@domain.com' ? $mail[ 'lead' ][ 'email' ] : '' }}
                  {{ !empty ($mail[ 'lead' ][ 'phone' ]) ? '| '.$mail[ 'lead' ][ 'phone' ]. ' |' : '' }}
                </p>
              </td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" style="padding:0 10px 20px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                <p style="margin:0;color:#555555;text-align:center;">{{ $mail[ 'lead' ][ 'message' ] }}</p>
              </td>
            </tr>
            @endif
            <!-- 1 Column Text + Button : END -->
            <!-- Background Image with Text : END -->
            <!-- 2 Even Columns : BEGIN -->
            <!-- 2 Even Columns : END -->

            {{-- consumer --}}
            @php
            if( $mail[ 'send_to' ] == 'consumer' && !empty( $propertyInfo ) )
            echo $propertyInfo;
            @endphp

            <!-- 3 Even Columns : BEGIN -->
            @if( !empty($mail[ 'property' ][ 'images' ]) )
            <tr>
              <td bgcolor="#ffffff" align="center" valign="top" style="padding:20px 10px 0px;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                  <tr>
                    <!-- Column : BEGIN -->
                    <td width="33%" class="stack-column-center">
                      <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td style="text-align:center; padding: 5px;">
                            <img src="{{ $mail[ 'property' ][ 'images' ][ 0 ] }}" width="140" height="140" alt="Zoom Property" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
                          </td>
                        </tr>
                      </table>
                    </td>
                    <!-- Column : END -->
                    <!-- Column : BEGIN -->
                    <td width="33%" class="stack-column-center">
                      <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td style="text-align:center; padding: 5px;">
                            <img src="{{ $mail[ 'property' ][ 'images' ][ 1 ] }}" width="140" height="140" alt="Zoom Property" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
                          </td>
                        </tr>
                      </table>
                    </td>
                    <!-- Column : END -->
                    <!-- Column : BEGIN -->
                    <td width="33%" class="stack-column-center">
                      <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td style="text-align:center; padding: 5px;">
                            <img src="{{ $mail[ 'property' ][ 'images' ][ 2 ] }}" width="140" height="140" alt="Zoom Property" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
                          </td>
                        </tr>
                      </table>
                    </td>
                    <!-- Column : END -->
                  </tr>
                </table>
              </td>
            </tr>
            @endif

            {{-- agent / agency  --}}
            @php
            if( $mail[ 'send_to' ] !== 'consumer' && !empty( $propertyInfo ) )
            echo $propertyInfo;
            @endphp

            @if( $mail[ 'send_to' ] === 'consumer' AND !empty( $mail[ 'property' ][ 'description' ] ) )
            <tr>
              <td bgcolor="#ffffff" style="padding:20px 10px 30px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                <p style="margin:0;color:#555555;text-align:center;font-size:13px;">{{ substr( strip_tags( $mail[ 'property' ][ 'description' ] ), 0, 300 ).'...' }}</p>
              </td>
            </tr>
            @endif

            @if( !empty($mail[ 'preferences' ]) && count( array_filter($mail[ 'preferences' ]) ) > 0 )
            <tr>
              <td bgcolor="#ffffff" style="padding:20px 10px 10px;text-align:center;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="1" align="center" width="500" style="margin:auto;" class="email-container">
                  <tr>
                    <th colspan="2" bgcolor="#ffffff" style="padding:10px 30px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:center;color:#555555;"> Preferences </p>
                    </th>
                  </tr>
                  @if( !empty( $mail[ 'preferences' ][ 'suburb' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Suburb </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">{{ $mail[ 'preferences' ][ 'suburb' ] }}</p>
                    </td>
                  </tr>
                  @endif
                  @if( !empty( $mail[ 'preferences' ][ 'city' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> City </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">{{ $mail[ 'preferences' ][ 'city' ] }}</p>
                    </td>
                  </tr>
                  @endif
                  @if( !empty( $mail[ 'preferences' ][ 'rent_buy' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Rent / Buy </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">{{ $mail[ 'preferences' ][ 'rent_buy' ] }}</p>
                    </td>
                  </tr>
                  @endif
                  @if( !empty( $mail[ 'preferences' ][ 'property_type' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Property Type </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">{{ $mail[ 'preferences' ][ 'property_type' ] }}</p>
                    </td>
                  </tr>
                  @endif
                  @if( !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] ) || !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'max' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Bedroom </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">
                        @if( !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] ) && !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'max' ] ) )
                        @if( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] == $mail[ 'preferences' ][ 'bedroom' ][ 'max' ] )
                        {{ $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] }} @if( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] != 'Studio' ) beds @endif
                        @else
                        {{ $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] }} to {{ $mail[ 'preferences' ][ 'bedroom' ][ 'max' ] }} beds
                        @endif
                        @elseif( !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] ) )
                        {{ $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] }} @if( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] != 'Studio' ) beds min @else min @endif
                        @elseif( !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'max' ] ) )
                        {{ $mail[ 'preferences' ][ 'bedroom' ][ 'max' ] }} @if( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] != 'Studio' ) beds max @else min max @endif
                        @else
                        Not Specified
                        @endif
                      </p>
                    </td>
                  </tr>
                  @endif
                  @if( !empty( $mail[ 'preferences' ][ 'bathroom' ][ 'min' ] ) || !empty( $mail[ 'preferences' ][ 'bathroom' ][ 'max' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Bathroom </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">
                        @if( !empty( $mail[ 'preferences' ][ 'bathroom' ][ 'min' ] ) && !empty( $mail[ 'preferences' ][ 'bathroom' ][ 'max' ] ) )
                        @if( $mail[ 'preferences' ][ 'bathroom' ][ 'min' ] == $mail[ 'preferences' ][ 'bathroom' ][ 'max' ] )
                        {{ $mail[ 'preferences' ][ 'bathroom' ][ 'min' ] }} baths
                        @else
                        {{ $mail[ 'preferences' ][ 'bathroom' ][ 'min' ] }} to {{ $mail[ 'preferences' ][ 'bathroom' ][ 'max' ] }} baths
                        @endif
                        @elseif( !empty( $mail[ 'preferences' ][ 'bedroom' ][ 'min' ] ) )
                        {{ $mail[ 'preferences' ][ 'bathroom' ][ 'min' ] }} baths min
                        @elseif( !empty( $mail[ 'preferences' ][ 'bathroom' ][ 'max' ] ) )
                        {{ $mail[ 'preferences' ][ 'bathroom' ][ 'max' ] }} baths max
                        @else
                        Not Specified
                        @endif
                      </p>
                    </td>
                  </tr>
                  @endif
                  @if( !empty( $mail[ 'preferences' ][ 'price' ][ 'min' ] ) || !empty( $mail[ 'preferences' ][ 'price' ][ 'max' ] ) )
                  <tr>
                    <td bgcolor="#ffffff" style="padding:10px 5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Price </p>
                    </td>
                    <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                      <p style="margin:0;text-align:left;font-size:13px;color:#555555;">
                        @if( !empty( $mail[ 'preferences' ][ 'price' ][ 'min' ] ) && !empty( $mail[ 'preferences' ][ 'price' ][ 'max' ] ) )
                        @if( $mail[ 'preferences' ][ 'price' ][ 'min' ] == $mail[ 'preferences' ][ 'price' ][ 'max' ] )
                        AED {{ $mail[ 'preferences' ][ 'price' ][ 'min' ] }}
                        @else
                        AED {{ $mail[ 'preferences' ][ 'price' ][ 'min' ] }} to AED {{ $mail[ 'preferences' ][ 'price' ][ 'max' ] }}
                        @endif
                        @elseif( !empty( $mail[ 'preferences' ][ 'price' ][ 'min' ] ) )
                        AED {{ $mail[ 'preferences' ][ 'price' ][ 'min' ] }}  min
                        @elseif( !empty( $mail[ 'preferences' ][ 'price' ][ 'max' ] ) )
                        AED {{ $mail[ 'preferences' ][ 'price' ][ 'max' ] }} max
                        @else
                        Not Specified
                        @endif
                      </p>
                    </td>
                  </tr>
                  @endif
                </table>
              </td>
            </tr>
            @endif

            <!-- 3 Even Columns : END -->
            <tr>
              <td align="center">
                <center style="height:2px;background:#d5d5d5;width:100%;"></center>
              </td>
            </tr>
          </table>
          <!-- Email Body : END -->
          <!-- Email Footer : BEGIN -->
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width:680px;font-family:sans-serif;color:#888888;font-size:13px;line-height:140%;">
            <tr>
              <td style="padding:0px 0px 10px 0px;width:50%;font-family:sans-serif;font-size:13px;line-height:140%;text-align:center;color:#888888;" class="x-gmail-data-detectors">
                <br>
                <br>
                &#169; Copyright {{ date( 'Y' ) }} Zoom Property. All rights reserved
                <br>
                <br>
              </td>
            </tr>

            <tr>
              <td style="padding:0px 0px 40px 0px;width:50%;font-family:sans-serif;font-size:13px;line-height:140%;text-align:center;color:#888888;" class="x-gmail-data-detectors">
                <a href="{{config('data.footer.links.facebook')}}">Facebook</a>  <span style="margin:0;">|</span>
                <a href="{{config('data.footer.links.twitter')}}">Twitter</a>  <span style="margin:0;">|</span>
                <a href="{{config('data.footer.links.instagram')}}">Instagram</a>  <span style="margin:0;">|</span>
                <a href="{{config('data.footer.links.google')}}">Google Plus</a>
              </td>
            </tr>
          </table>
          <!-- Email Footer : END -->
        </center>
      </body>

      </html>
