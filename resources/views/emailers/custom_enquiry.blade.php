<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
    <style>
      * {
      font-family:sans-serif !important;
      }
    </style>
    <![endif]-->
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
    <!--<![endif]-->
    <!-- Web Font / @font-face : END -->
    <!-- CSS Reset : BEGIN -->
    <!-- CSS Reset : END -->
    <!-- Progressive Enhancements : BEGIN -->
    <!-- Progressive Enhancements : END -->
    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG />
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

<style type="text/css">
		html,body{
			margin:0 auto !important;
			padding:0 !important;
			height:100% !important;
			width:100% !important;
		}
		*{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		div[style*=margin: 16px 0]{
			margin:0 !important;
		}
		table,td{
			mso-table-lspace:0 !important;
			mso-table-rspace:0 !important;
		}
		table{
			border-spacing:0 !important;
			border-collapse:collapse !important;
			table-layout:fixed !important;
			margin:0 auto !important;
		}
		table table table{
			table-layout:auto;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
			border-bottom:0 !important;
			cursor:default !important;
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.a6S{
			display:none !important;
			opacity:.01 !important;
		}
		img.g-img+div{
			display:none !important;
		}
		.button-link{
			text-decoration:none !important;
		}
	@media only screen and (min-device-width: 375px) and (max-device-width: 413px){
		.email-container{
			min-width:375px !important;
		}

}	@media screen and (max-width: 480px){
		div>u div .gmail{
			min-width:100vw;
		}

}		html,body{
			margin:0 auto !important;
			padding:0 !important;
			height:100% !important;
			width:100% !important;
		}
		*{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		div[style*=margin: 16px 0]{
			margin:0 !important;
		}
		table,td{
			mso-table-lspace:0 !important;
			mso-table-rspace:0 !important;
		}
		table{
			border-spacing:0 !important;
			border-collapse:collapse !important;
			table-layout:fixed !important;
			margin:0 auto !important;
		}
		table table table{
			table-layout:auto;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
			border-bottom:0 !important;
			cursor:default !important;
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.a6S{
			display:none !important;
			opacity:.01 !important;
		}
		img.g-img+div{
			display:none !important;
		}
		.button-link{
			text-decoration:none !important;
		}
	@media only screen and (min-device-width: 375px) and (max-device-width: 413px){
		.email-container{
			min-width:375px !important;
		}

}	@media screen and (max-width: 480px){
		div>u div .gmail{
			min-width:100vw;
		}

}		.button-td,.button-a{
			transition:all 100ms ease-in;
		}
	@media screen and (max-width: 600px){
		.email-container{
			width:100% !important;
			margin:auto !important;
		}

}	@media screen and (max-width: 600px){
		.fluid{
			max-width:100% !important;
			height:auto !important;
			margin-left:auto !important;
			margin-right:auto !important;
		}

}	@media screen and (max-width: 600px){
		.stack-column,.stack-column-center{
			display:block !important;
			width:100% !important;
			max-width:100% !important;
			direction:ltr !important;
		}

}	@media screen and (max-width: 600px){
		.stack-column-center{
			text-align:center !important;
		}

}	@media screen and (max-width: 600px){
		.center-on-narrow{
			text-align:center !important;
			display:block !important;
			margin-left:auto !important;
			margin-right:auto !important;
			float:none !important;
		}

}	@media screen and (max-width: 600px){
		table.center-on-narrow{
			display:inline-block !important;
		}

}	@media screen and (max-width: 600px){
		.email-container p{
			font-size:13px !important;
		}

}</style></head>

    @php

    $propertyInfo = '<tr><td bgcolor="#ffffff" style="padding:20px 10px 20px;text-align:center;"><h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;text-align:center;">'.
    $mail[ 'project' ][ 'title' ].'</h1></td></tr>';

    $propertyInfo .= '<tr><td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;"><p style="margin:0;color:#555555;text-align:center;font-size: 13px;">';

    if( !empty( $mail[ 'project' ][ 'property_ref_no' ] ) )

        $propertyInfo .= 'Reference No: '.$mail[ 'project' ][ 'property_ref_no' ];

    if( !empty( $mail[ 'project' ][ 'suburb' ] ) )

        $propertyInfo .= '<br>'.$mail[ 'project' ][ 'suburb' ];

    if( !empty( $mail[ 'project' ][ 'city' ] ) )

        $propertyInfo .= ', '.$mail[ 'project' ][ 'city' ];

    $propertyInfo .= '</p></td></tr>';
    $propertyInfo .= '<tr><td bgcolor="#ffffff" style="padding:0 10px 20px;font-family:sans-serif;font-size:16px;line-height:140%;color:#555555;text-align:center;"><p style="margin:0;color:#555555;text-align:center;font-size:13px">';

    if( !empty( $mail[ 'project' ][ 'starting_price' ] ) )

        $propertyInfo .= ' '.$mail[ 'project' ][ 'currency' ] .' '.$mail[ 'project' ][ 'starting_price' ] . ' starting price';

    $propertyInfo .= '</p></td></tr>';

    @endphp

  <body width="100%" bgcolor="#fff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width:100%;background:#fff;text-align:left;">
      <!-- Email Header : BEGIN -->
      <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
        <tr>
          <td style="padding:10px 0;text-align:center;">
            <img src="https://gallery.mailchimp.com/9e8bad10f4764df10d34cb5e3/images/9b77fb59-9a07-4bca-8a9d-4b96152df92c.png" width="100" height="50" alt="Zoom Property" border="0" style="height: auto; background: #fff; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
          </td>
        </tr>
      </table>
      <!-- Email Header : END -->
      <!-- Email Body : BEGIN -->
      <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
        <!-- 1 Column Text + Button : BEGIN -->
        <tr>
          <td bgcolor="#ffffff" style="padding:20px 10px 20px;text-align:center;">
            <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#333333;font-weight:normal;text-align:center;">
                Custom Enquiry!
            </h1>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding:20px 10px 0px;text-align:center;">
            <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:bold;text-align:center;">{{ $mail[ 'lead' ][ 'name' ] }}</h1>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">{{ !empty ($mail[ 'lead' ][ 'email' ]) && $mail[ 'lead' ][ 'email' ] !== 'anonymous@domain.com' ? $mail[ 'lead' ][ 'email' ] : '' }}  {{  !empty ($mail[ 'lead' ][ 'phone' ]) ? '| '.$mail[ 'lead' ][ 'phone' ]. ' |' : '' }}</p>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 20px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">{{ $mail[ 'lead' ][ 'message' ] }}</p>
          </td>
        </tr>
        <tr>
            <td style="text-align:center;padding:0 10px 20px" class="button-td">
            </td>
        </tr>

        <!-- 1 Column Text + Button : END -->
        <!-- Background Image with Text : END -->
        <!-- 2 Even Columns : BEGIN -->
        <!-- 2 Even Columns : END -->

        @if(!empty($mail[ 'project' ][ 'images' ]))
        <!-- 3 Even Columns : BEGIN -->
        <tr>
          <td bgcolor="#ffffff" align="center" valign="top" style="padding:20px 10px 0px;">
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <!-- Column : BEGIN -->
                <td width="33%" class="stack-column-center">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td style="text-align:center; padding: 5px;">
                        <img src="{{ $mail[ 'project' ][ 'images' ][ 0 ] }}" width="140" height="140" alt="Zoom Property" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
                      </td>
                    </tr>
                  </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td width="33%" class="stack-column-center">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td style="text-align:center; padding: 5px;">
                        <img src="{{ $mail[ 'project' ][ 'images' ][ 1 ] }}" width="140" height="140" alt="Zoom Property" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
                      </td>
                    </tr>
                  </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <td width="33%" class="stack-column-center">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td style="text-align:center; padding: 5px;">
                        <img src="{{ $mail[ 'project' ][ 'images' ][ 2 ] }}" width="140" height="140" alt="Zoom Property" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size:13px; line-height: 140%; color: #555555;">
                      </td>
                    </tr>
                  </table>
                </td>
                <!-- Column : END -->
              </tr>
            </table>
          </td>
        </tr>
				@endif
				@php
				  echo $propertyInfo;
				@endphp
				@if(!empty($mail[ 'custom_data' ][ 'rooms' ]))
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">
              How many rooms are you looking to invest in? :
              {{ $mail[ 'custom_data' ]['rooms'] }}
            </p>
          </td>
        </tr>
        @endif
        @if(!empty($mail[ 'custom_data' ][ 'when' ]))
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">
              When are you looking to buy? :
              {{ $mail[ 'custom_data' ]['when'] }}
            </p>
          </td>
        </tr>
        @endif
        @if(!empty($mail[ 'custom_data' ][ 'times' ]))
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">
              Have you invested in Dubai properties before or this is the first time? :
              {{ $mail[ 'custom_data' ]['times'] }}
            </p>
          </td>
        </tr>
        @endif
        @if(!empty($mail[ 'custom_data' ][ 'investor_job' ]))
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">
              Are you an investor or job seeker? :
              {{ $mail[ 'custom_data' ]['investor_job'] }}
            </p>
          </td>
        </tr>
        @endif
        @if(!empty($mail[ 'custom_data' ][ 'where' ]))
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">
              Are you looking to purchase a property in Dubai? :
              {{ $mail[ 'custom_data' ]['where'] }}
            </p>
          </td>
        </tr>
        @endif
        @if(!empty($mail[ 'custom_data' ][ 'when_now' ]))
        <tr>
          <td bgcolor="#ffffff" style="padding:0 10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;color:#555555;text-align:center;">
              Do you want to buy property now or later? :
              {{ $mail[ 'custom_data' ]['when_now'] }}
            </p>
          </td>
        </tr>
        @endif
        <!-- 3 Even Columns : END -->
        <tr>
          <td align="center">
            <center style="height:2px;background:#d5d5d5;width:100%;"></center>
          </td>
        </tr>
      </table>
      <!-- Email Body : END -->
      <!-- Email Footer : BEGIN -->
      <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width:680px;font-family:sans-serif;color:#888888;font-size:13px;line-height:140%;">
        <tr>
          <td style="padding:0px 0px 10px 0px;width:50%;font-family:sans-serif;font-size:13px;line-height:140%;text-align:center;color:#888888;" class="x-gmail-data-detectors">
            <br>
            <br>
            &#169; Copyright {{ date( 'Y' ) }} Zoom Property. All rights reserved
            <br>
            <br>
          </td>
          </tr>

          <tr>
          <td style="padding:0px 0px 40px 0px;width:50%;font-family:sans-serif;font-size:13px;line-height:140%;text-align:center;color:#888888;" class="x-gmail-data-detectors">
                <a href="{{ __( 'page.footer.social_media.facebook' ) }}">Facebook</a>  <span style="margin:0;">|</span>  
                <a href="{{ __( 'page.footer.social_media.twitter' ) }}">Twitter</a>  <span style="margin:0;">|</span>  
                <a href="{{ __( 'page.footer.social_media.instagram' ) }}">Instagram</a>  <span style="margin:0;">|</span>  
                <a href="{{ __( 'page.footer.social_media.googleplus' ) }}">Google Plus</a>
          </td>
          </tr>
        </table>
        <!-- Email Footer : END -->
      </center>
    </body>
</html>
