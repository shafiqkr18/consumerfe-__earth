<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="utf-8">
        <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width">
        <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <!-- Use the latest (edge) version of IE rendering engine -->
        <meta name="x-apple-disable-message-reformatting">
        <!-- Disable auto-scale in iOS 10 Mail entirely -->
        <title></title>
        <!-- The title tag shows in email notifications, like Android 4.4. -->
        <!-- Web Font / @font-face : BEGIN -->
        <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
        <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
        <!--[if mso]>
        <style>
          * {
          font-family:sans-serif !important;
          }
        </style>
        <![endif]-->
        <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
        <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
        <!--<![endif]-->
        <!-- Web Font / @font-face : END -->
        <!-- CSS Reset : BEGIN -->
        <!-- CSS Reset : END -->
        <!-- Progressive Enhancements : BEGIN -->
        <!-- Progressive Enhancements : END -->
        <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
        <!--[if gte mso 9]>
        <xml>
          <o:OfficeDocumentSettings>
            <o:AllowPNG />
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->

        <style type="text/css">
    		html,body{
    			margin:0 auto !important;
    			padding:0 !important;
    			height:100% !important;
    			width:100% !important;
    		}
    		*{
    			-ms-text-size-adjust:100%;
    			-webkit-text-size-adjust:100%;
    		}
    		div[style*=margin: 16px 0]{
    			margin:0 !important;
    		}
    		table,td{
    			mso-table-lspace:0 !important;
    			mso-table-rspace:0 !important;
    		}
    		table{
    			border-spacing:0 !important;
    			border-collapse:collapse !important;
    			table-layout:fixed !important;
    			margin:0 auto !important;
    		}
    		table table table{
    			table-layout:auto;
    		}
    		img{
    			-ms-interpolation-mode:bicubic;
    		}
    		[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
    			border-bottom:0 !important;
    			cursor:default !important;
    			color:inherit !important;
    			text-decoration:none !important;
    			font-size:inherit !important;
    			font-family:inherit !important;
    			font-weight:inherit !important;
    			line-height:inherit !important;
    		}
    		.a6S{
    			display:none !important;
    			opacity:.01 !important;
    		}
    		img.g-img+div{
    			display:none !important;
    		}
    		.button-link{
    			text-decoration:none !important;
    		}
            @media only screen and (min-device-width: 375px) and (max-device-width: 413px){
        		.email-container{
        			min-width:375px !important;
        		}
            }
            @media screen and (max-width: 480px){
        		div>u div .gmail{
        			min-width:100vw;
        		}
            }
            html,body{
    			margin:0 auto !important;
    			padding:0 !important;
    			height:100% !important;
    			width:100% !important;
    		}
    		*{
    			-ms-text-size-adjust:100%;
    			-webkit-text-size-adjust:100%;
    		}
    		div[style*=margin: 16px 0]{
    			margin:0 !important;
    		}
    		table,td{
    			mso-table-lspace:0 !important;
    			mso-table-rspace:0 !important;
    		}
    		table{
    			border-spacing:0 !important;
    			border-collapse:collapse !important;
    			table-layout:fixed !important;
    			margin:0 auto !important;
    		}
    		table table table{
    			table-layout:auto;
    		}
    		img{
    			-ms-interpolation-mode:bicubic;
    		}
    		[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
    			border-bottom:0 !important;
    			cursor:default !important;
    			color:inherit !important;
    			text-decoration:none !important;
    			font-size:inherit !important;
    			font-family:inherit !important;
    			font-weight:inherit !important;
    			line-height:inherit !important;
    		}
    		.a6S{
    			display:none !important;
    			opacity:.01 !important;
    		}
    		img.g-img+div{
    			display:none !important;
    		}
    		.button-link{
    			text-decoration:none !important;
    		}
        	@media only screen and (min-device-width: 375px) and (max-device-width: 413px){
        		.email-container{
        			min-width:375px !important;
        		}
            }
            @media screen and (max-width: 480px){
        		div>u div .gmail{
        			min-width:100vw;
        		}
            }
            .button-td,.button-a{
    			transition:all 100ms ease-in;
    		}
            @media screen and (max-width: 600px){
        		.email-container{
        			width:100% !important;
        			margin:auto !important;
        		}
            }
            @media screen and (max-width: 600px){
        		.fluid{
        			max-width:100% !important;
        			height:auto !important;
        			margin-left:auto !important;
        			margin-right:auto !important;
        		}
            }
            @media screen and (max-width: 600px){
        		.stack-column,.stack-column-center{
        			display:block !important;
        			width:100% !important;
        			max-width:100% !important;
        			direction:ltr !important;
        		}
            }
            @media screen and (max-width: 600px){
        		.stack-column-center{
        			text-align:center !important;
        		}
            }
            @media screen and (max-width: 600px){
        		.center-on-narrow{
        			text-align:center !important;
        			display:block !important;
        			margin-left:auto !important;
        			margin-right:auto !important;
        			float:none !important;
        		}
            }
            @media screen and (max-width: 600px){
        		table.center-on-narrow{
        			display:inline-block !important;
        		}
            }
            @media screen and (max-width: 600px){
        		.email-container p{
        			font-size:17px !important;
        		}
            }
        </style>
    </head>
    <body width="100%" bgcolor="#fff" style="margin: 0; mso-line-height-rule: exactly;">
        <center style="width:100%;background:#fff;text-align:left;">
        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
        <tr>
            <td style="padding:10px 0;text-align:center;">
                <img src="https://gallery.mailchimp.com/9e8bad10f4764df10d34cb5e3/images/9b77fb59-9a07-4bca-8a9d-4b96152df92c.png" width="100" height="50" alt="Zoom Property" border="0" style="height: auto; background: #fff; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
            </td>
        </tr>
        </table>
        <!-- Email Header : END -->
        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
        <!-- 1 Column Text + Button : BEGIN -->
        <tr>
          <td bgcolor="#ffffff" style="padding:20px 40px 0px;text-align:center;">
            <h1 style="margin:0;font-family:sans-serif;font-size:20px;line-height:125%;color:#333333;font-weight:normal;">Hi {{ $mail[ 'receiver_name' ] }}</h1>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding:10px 40px 30px;font-family:sans-serif;font-size:14px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;">You have successfully saved this search!</p>
          </td>
        </tr>
        <!-- 1 Column Text + Button : BEGIN -->
        <tr>
          <td align="center">
            <center style="height:2px;background:#d5d5d5;width:100%;"></center>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding:20px 40px 0px;text-align:center;">
            <h1 style="margin:0;font-family:sans-serif;font-size:20px;line-height:125%;color:#5d0e8b;font-weight:normal;">2 Bed Apartment in Dubai Marina</h1>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" style="padding:0 40px 20px;font-family:sans-serif;font-size:15px;line-height:140%;color:#555555;text-align:center;">
            <p style="margin:0;">Price: More than AED 60,000</p>
          </td>
        </tr>
        <tr>
          <td align="center">
            <center style="height:2px;background:#d5d5d5;width:100%;"></center>
          </td>
        </tr>
        <!-- 1 Column Text + Button : END -->
        <!-- Background Image with Text : END -->
        <!-- 2 Even Columns : BEGIN -->
        <!-- 2 Even Columns : END -->
        <!-- 3 Even Columns : BEGIN -->
        <tr>
          <td bgcolor="#ffffff" valign="top" style="padding:10px 10px 10px;">
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <!-- Column : BEGIN -->
                <td width="40%" class="stack-column-center">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td style="text-align:left;">
                        <img src="http://watermark.propspace.com/watermark?c_id=3012&amp;l_id=1576257625061484&amp;aid=1462654&amp;id=15065934199799707&amp;wmt=1501653666&amp;image=28_09_2017-14_30_03-3011-4996017749a0b200b0c1742b2c2351e7.jpeg" alt="Zoom Property" width="100%" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="60%">
                  <table role="presentation" cellspacing="0" cellpadding="10" border="0">
                    <tr>
                      <td bgcolor="#ffffff" style="padding:20px 40px 0px;text-align:left;">
                        <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;">Awesome 2 Bedroom Apartment</h1>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="padding:0 40px 10px;font-family:sans-serif;font-size:12px;line-height:140%;color:#555555;text-align:left;">
                        <p style="margin:0;">Reference No: OBG-A-12345</p>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="padding:0 40px 0px;font-family:sans-serif;font-size:12px;line-height:140%;color:#555555;text-align:left;">
                        <p style="margin:0;">Buidling Name, Area, Dubai</p>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="padding:0 40px 0px;font-family:sans-serif;font-size:14px;line-height:140%;color:#555555;text-align:left;">
                        <p style="margin:0;">Apartment  |  2 Bed  |  2 Bath  |  1998 sqft</p>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align:left;">
                        <a style="background:#5d0e8b;border:15px solid #5d0e8b;font-family:sans-serif;font-size:12px;line-height:1.1;margin:0 auto;text-align:center;text-decoration:none;display:block;width:80%;border-radius:4px;" class="button-a">
                          <span style="color:#ffffff;" class="button-link">Contact the Agent</span>
                        </a>
                      </td>
                    </tr>
                  </table>
                </td>

                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <!-- Column : END -->
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ffffff" valign="top" style="padding:10px 10px 10px;">
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <!-- Column : BEGIN -->
                <td width="40%" class="stack-column-center">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td style="text-align:left;">
                        <img src="http://watermark.propspace.com/watermark?c_id=3012&amp;l_id=1576257625061484&amp;aid=1462654&amp;id=15065934199799707&amp;wmt=1501653666&amp;image=28_09_2017-14_30_03-3011-4996017749a0b200b0c1742b2c2351e7.jpeg" alt="Zoom Property" width="100%" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="60%">
                  <table role="presentation" cellspacing="0" cellpadding="10" border="0">
                    <tr>
                      <td bgcolor="#ffffff" style="padding:20px 40px 0px;text-align:left;">
                        <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;">Awesome 2 Bedroom Apartment</h1>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="padding:0 40px 10px;font-family:sans-serif;font-size:12px;line-height:140%;color:#555555;text-align:left;">
                        <p style="margin:0;">Reference No: OBG-A-12345</p>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="padding:0 40px 10px;font-family:sans-serif;font-size:12px;line-height:140%;color:#555555;text-align:left;">
                        <p style="margin:0;">Buidling Name, Area, Dubai</p>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#ffffff" style="padding:0 40px 0px;font-family:sans-serif;font-size:14px;line-height:140%;color:#555555;text-align:left;">
                        <p style="margin:0;">Apartment  |  2 Bed  |  2 Bath  |  1998 sqft</p>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align:left;">
                        <a style="background:#5d0e8b;border:15px solid #5d0e8b;font-family:sans-serif;font-size:12px;line-height:1.1;margin:0 auto;text-align:center;text-decoration:none;display:block;width:80%;border-radius:4px;" class="button-a">
                          <span style="color:#ffffff;" class="button-link">Contact the Agent</span>
                        </a>
                      </td>
                    </tr>
                  </table>
                </td>
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <!-- Column : END -->
                <!-- Column : BEGIN -->
                <!-- Column : END -->
              </tr>
            </table>
          </td>
        </tr>
        <!-- 3 Even Columns : END -->
        <tr>
          <td align="center">
            <center style="height:2px;background:#d5d5d5;width:100%;"></center>
          </td>
        </tr>
        </table>
        <!-- Email Body : END -->
        <!-- Email Footer : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width:680px;font-family:sans-serif;color:#888888;font-size:12px;line-height:140%;">
        <tr>
          <td style="padding:0px 0px 0px 40px;width:50%;font-family:sans-serif;font-size:12px;line-height:140%;text-align:left;color:#888888;" class="x-gmail-data-detectors">
            <br>
            <br>
            &#169; Copyright {{ date( 'Y' ) }} Zoom Property. All rights reserved
            <br>
            <br>
          </td>
          <td style="padding:0px 40px 0 0px;width:50%;font-family:sans-serif;font-size:12px;line-height:140%;text-align:right;color:#888888;" class="x-gmail-data-detectors">
                <br>
                <br>
                    <a href="{{ __( 'page.footer.social_media.facebook' ) }}">Facebook</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.twitter' ) }}">Twitter</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.instagram' ) }}">Instagram</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.googleplus' ) }}">Google Plus</a>
                <br>
                <br>
            </td>
          </tr>
        </table>
        <!-- Email Footer : END -->
        </center>
    </body>
</html>
