<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
    <style>
    * {
    font-family:sans-serif !important;
}
</style>
<![endif]-->
<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
<!--[if !mso]><!-->
<!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
<!--<![endif]-->
<!-- Web Font / @font-face : END -->
<!-- CSS Reset : BEGIN -->
<!-- CSS Reset : END -->
<!-- Progressive Enhancements : BEGIN -->
<!-- Progressive Enhancements : END -->
<!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
<!--[if gte mso 9]>
<xml>
<o:OfficeDocumentSettings>
<o:AllowPNG />
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->

<style type="text/css">
    html,body{
        margin:0 auto !important;
        padding:0 !important;
        height:100% !important;
        width:100% !important;
    }
    *{
        -ms-text-size-adjust:100%;
        -webkit-text-size-adjust:100%;
    }
    div[style*=margin: 16px 0]{
        margin:0 !important;
    }
    table,td{
        mso-table-lspace:0 !important;
        mso-table-rspace:0 !important;
    }
    table{
        border-spacing:0 !important;
        border-collapse:collapse !important;
        table-layout:fixed !important;
        margin:0 auto !important;
    }
    table table table{
        table-layout:auto;
    }
    img{
        -ms-interpolation-mode:bicubic;
    }
    [x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
        border-bottom:0 !important;
        cursor:default !important;
        color:inherit !important;
        text-decoration:none !important;
        font-size:inherit !important;
        font-family:inherit !important;
        font-weight:inherit !important;
        line-height:inherit !important;
    }
    .a6S{
        display:none !important;
        opacity:.01 !important;
    }
    img.g-img+div{
        display:none !important;
    }
    .button-link{
        text-decoration:none !important;
    }
    @media only screen and (min-device-width: 375px) and (max-device-width: 413px){
        .email-container{
            min-width:375px !important;
        }
    }
    @media screen and (max-width: 480px){
        div>u div .gmail{
            min-width:100vw;
        }
    }
    html,body{
        margin:0 auto !important;
        padding:0 !important;
        height:100% !important;
        width:100% !important;
    }
    *{
        -ms-text-size-adjust:100%;
        -webkit-text-size-adjust:100%;
    }
    div[style*=margin: 16px 0]{
        margin:0 !important;
    }
    table,td{
        mso-table-lspace:0 !important;
        mso-table-rspace:0 !important;
    }
    table{
        border-spacing:0 !important;
        border-collapse:collapse !important;
        table-layout:fixed !important;
        margin:0 auto !important;
    }
    table table table{
        table-layout:auto;
    }
    img{
        -ms-interpolation-mode:bicubic;
    }
    [x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{
        border-bottom:0 !important;
        cursor:default !important;
        color:inherit !important;
        text-decoration:none !important;
        font-size:inherit !important;
        font-family:inherit !important;
        font-weight:inherit !important;
        line-height:inherit !important;
    }
    .a6S{
        display:none !important;
        opacity:.01 !important;
    }
    img.g-img+div{
        display:none !important;
    }
    .button-link{
        text-decoration:none !important;
    }
    @media only screen and (min-device-width: 375px) and (max-device-width: 413px){
        .email-container{
            min-width:375px !important;
        }
    }
    @media screen and (max-width: 480px){
        div>u div .gmail{
            min-width:100vw;
        }
    }
    .button-td,.button-a{
        transition:all 100ms ease-in;
    }
    @media screen and (max-width: 600px){
        .email-container{
            width:100% !important;
            margin:auto !important;
        }
    }
    @media screen and (max-width: 600px){
        .fluid{
            max-width:100% !important;
            height:auto !important;
            margin-left:auto !important;
            margin-right:auto !important;
        }
    }
    @media screen and (max-width: 600px){
        .stack-column,.stack-column-center{
            display:block !important;
            width:100% !important;
            max-width:100% !important;
            direction:ltr !important;
        }
    }
    @media screen and (max-width: 600px){
        .stack-column-center{
            text-align:center !important;
        }
    }
    @media screen and (max-width: 600px){
        .center-on-narrow{
            text-align:center !important;
            display:block !important;
            margin-left:auto !important;
            margin-right:auto !important;
            float:none !important;
        }
    }
    @media screen and (max-width: 600px){
        table.center-on-narrow{
            display:inline-block !important;
        }
    }
    @media screen and (max-width: 600px){
        .email-container p{
            /* font-size:13px !important; */
        }
    }
</style>
</head>
<body width="100%" bgcolor="#fff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width:100%;background:#fff;text-align:left;">
        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
            <tr>
                <td style="padding:10px 0;text-align:center;">
                    <img src="https://gallery.mailchimp.com/9e8bad10f4764df10d34cb5e3/images/9b77fb59-9a07-4bca-8a9d-4b96152df92c.png" width="100" height="50" alt="Zoom Property" border="0" style="height: auto; background: #fff; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                </td>
            </tr>
        </table>
        <!-- Email Header : END -->
        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin:auto;" class="email-container">
            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td align="center">
                    <center style="height:2px;background:#d5d5d5;width:100%;"></center>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding:20px 10px 10px;text-align:center;">
                    <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;text-align:center;">
                        Mortgage Calculator Enquiry
                    </h1>
                </td>
            </tr>
            <!-- Project -->
            <tr>
                <td bgcolor="#ffffff" style="padding:10px 10px;text-align:center;">
                    @if( $mail[ 'enquiry' ][ 'type' ] == 'project' )
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="400" style="margin-top:20px" class="email-container">
                        <tr>
                            <th bgcolor="#ffffff" style="padding:5px 0px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;color:#555555;"> {{ $mail[ 'project' ][ 'developer' ][ 'name' ] }} Project </p>
                            </th>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding:10px 10px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;text-align:center;">
                                    <a href="{{ $mail[ 'project' ][ 'link' ] }}" target="_blank" style="text-decoration:none;color:#5d0e8b;font-weight:bold;"> {{ $mail[ 'project' ][ 'title' ] }} </a>
                                </h1>
                            </td>
                        </tr>
                    </table>
                    @endif
                </td>
            </tr>
            <!-- EMI -->
            <tr>
                <td bgcolor="#ffffff" style="padding:10px 10px;text-align:center;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="1" align="center" width="500" style="margin:auto;" class="email-container">
                        @if( !empty( $mail[ 'emi' ][ 'loan_amount' ] ) )
                        <tr>
                            <th colspan="2" bgcolor="#ffffff" style="padding:10px 30px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;color:#555555;"> EMI Details </p>
                            </th>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'loan_amount' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Loan Amount </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> AED {{ number_format( $mail[ 'emi' ][ 'loan_amount' ] ) }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'monthly' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> Monthly EMI </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> AED {{ number_format( $mail[ 'emi' ][ 'monthly' ] ) }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'down_payment' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> EMI Down Payment </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> AED {{ number_format( $mail[ 'emi' ][ 'down_payment' ] ) }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'interest_rate' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> EMI Profit Rate </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'emi' ][ 'interest_rate' ] }}% per annum</p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'emi_term' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> EMI Term </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'emi' ][ 'emi_term' ] }} Years </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'total_amount' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> EMI Total Amount </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> AED {{ number_format( $mail[ 'emi' ][ 'total_amount' ] ) }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'emi' ][ 'total_interest' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> EMI Total Interest </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> AED {{ number_format( $mail[ 'emi' ][ 'total_interest' ] ) }} </p>
                            </td>
                        </tr>
                        @endif
                    </table>
                </td>
            </tr>
            <!-- User -->
            <tr>
                <td bgcolor="#ffffff" style="padding:20px 10px 10px;text-align:center;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="1" align="center" width="500" style="margin:auto;" class="email-container">
                        @if( !empty( $mail[ 'user' ][ 'contact' ][ 'email' ] ) && $mail[ 'user' ][ 'contact' ][ 'email' ] !== 'anonymous@domain.com' )
                        <tr>
                            <th colspan="2" bgcolor="#ffffff" style="padding:10px 30px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;color:#555555;"> User Details </p>
                            </th>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'user' ][ 'contact' ][ 'first_name' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> First Name </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'user' ][ 'contact' ][ 'first_name' ] }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'user' ][ 'contact' ][ 'last_name' ] ))
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Last Name </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'user' ][ 'contact' ][ 'last_name' ] }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'user' ][ 'contact' ][ 'email' ] ) && $mail[ 'user' ][ 'contact' ][ 'email' ] !== 'anonymous@domain.com' )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Email </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'user' ][ 'contact' ][ 'email' ] }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'user' ][ 'contact' ][ 'mobile_number' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Mobile Number </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'user' ][ 'contact' ][ 'mobile_number' ] }} </p>
                            </td>
                        </tr>
                        @endif
                        @if( !empty( $mail[ 'user' ][ 'contact' ][ 'work_number' ] ) )
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:11px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Work Number </p>
                            </td>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:112px;line-height:140%;color:#555555;text-align:left;">
                                <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'user' ][ 'contact' ][ 'work_number' ] }} </p>
                            </td>
                        </tr>
                        @endif
                    </table>
                </td>
            </tr>
            <!-- Property -->
            <tr>
                <td bgcolor="#ffffff" style="padding:20px 10px 10px;text-align:center;">
                    @if( $mail[ 'enquiry' ][ 'type' ] == 'property' )
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin-top:20px;" class="email-container">
                        <tr>
                            <th bgcolor="#ffffff" style="padding:10px 0px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;text-align:center;color:#555555;"> Property </p>
                            </th>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding:10px 30px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                <h1 style="margin:0;font-family:sans-serif;font-size:16px;line-height:125%;color:#5d0e8b;font-weight:normal;text-align:center;">
                                    <a href="{{ $mail[ 'property' ][ 'link' ] }}" target="_blank" style="text-decoration:none;color:#5d0e8b;font-weight:bold;"> {{ $mail[ 'property' ][ 'title' ] }} </a>
                                </h1>
                            </td>
                        </tr>

                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;color:#555555;text-align:center;font-size:13px;">

                                    @isset( $mail[ 'property' ][ 'ref_no' ] )
                                        Reference No: {{ $mail[ 'property' ][ 'ref_no' ] }} |
                                    @endisset

                                    @isset( $mail[ 'property' ][ 'area' ] )
                                        {{ $mail[ 'property' ][ 'area' ] }},
                                    @endisset

                                    @isset( $mail[ 'property' ][ 'city' ] )
                                        {{ $mail[ 'property' ][ 'city' ] }},
                                    @endisset

                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding:5px 5px 5px 20px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                <p style="margin:0;color:#555555;text-align:center;font-size:13px;">

                                    @isset( $mail[ 'property' ][ 'type' ] )
                                        {{ $mail[ 'property' ][ 'type' ].' | ' }}
                                    @endisset

                                    @isset( $mail[ 'property' ][ 'bedroom' ] )
                                        {{ $mail[ 'property' ][ 'bedroom' ].' Bed'.( $mail[ 'property' ][ 'bedroom' ] > 1  ? 's ' : ' ' ).' | ' }}
                                    @endisset

                                    @isset( $mail[ 'property' ][ 'bathroom' ] )
                                        {{ $mail[ 'property' ][ 'bathroom' ].' Bath'.( $mail[ 'property' ][ 'bedroom' ] > 1 ? 's ' : ' ' ).' | ' }}
                                    @endisset

                                    @isset( $mail[ 'property' ][ 'builtup_area' ] )
                                        {{ $mail[ 'property' ][ 'builtup_area' ].' sq.ft' }}
                                    @endisset

                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding:20px 10px 10px;text-align:center;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="1" align="center" width="600" style="margin-top:20px;" class="email-container">
                                    @if( !empty( $mail[ 'property' ][ 'agency' ][ 'email' ] ) )
                                    <tr>
                                        <th colspan="2" bgcolor="#ffffff" style="padding:10px 30px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;color:#555555;"> Agency Details </p>
                                        </th>
                                    </tr>
                                    @endif
                                    @if( !empty( $mail[ 'property' ][ 'agency' ][ 'name' ] ) )
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Name </p>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'property' ][ 'agency' ][ 'name' ] }} </p>
                                        </td>
                                    </tr>
                                    @endif
                                    @if( !empty( $mail[ 'property' ][ 'agency' ][ 'email' ] ) )
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Email </p>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'property' ][ 'agency' ][ 'email' ] }} </p>
                                        </td>
                                    </tr>
                                    @endif
                                    @if( !empty( $mail[ 'property' ][ 'agency' ][ 'phone' ] ) )
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Phone </p>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'property' ][ 'agency' ][ 'phone' ] }} </p>
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" style="padding:20px 10px 10px;text-align:center;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="1" align="center" width="600" style="margin-top:20px;" class="email-container">
                                    @if( !empty( $mail[ 'property' ][ 'agent' ][ 'email' ] ) )
                                    <tr>
                                        <th colspan="2" bgcolor="#ffffff" style="padding:10px 30px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Agent Details </p>
                                        </th>
                                    </tr>
                                    @endif
                                    @if( !empty( $mail[ 'property' ][ 'agent' ][ 'name' ] ) )
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Name </p>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'property' ][ 'agent' ][ 'name' ]  }} </p>
                                        </td>
                                    </tr>
                                    @endif
                                    @if( !empty( $mail[ 'property' ][ 'agent' ][ 'email' ] ) )
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Email </p>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'property' ][ 'agent' ][ 'email' ]  }} </p>
                                        </td>
                                    </tr>
                                    @endif
                                    @if( !empty( $mail[ 'property' ][ 'agent' ][ 'phone' ] ) )
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:center;font-size:13px;color:#555555;"> Phone </p>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding:5px;font-family:sans-serif;font-size:13px;line-height:140%;color:#555555;text-align:center;">
                                            <p style="margin:0;text-align:left;font-size:13px;color:#555555;"> {{ $mail[ 'property' ][ 'agent' ][ 'phone' ]  }} </p>
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </td>
                        </tr>
                    </table>
                    @endif
                </td>
            </tr>
            <!-- Column : END -->
        </table>
        <!-- Email Body : END -->
        <!-- Email Footer : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width:680px;font-family:sans-serif;color:#888888;font-size:12px;line-height:140%;">
            <tr>
                <td align="center">
                    <center style="height:2px;background:#d5d5d5;width:100%;"></center>
                </td>
            </tr>
            <tr>
                <td style="padding:0px 0px 10px 0px;width:50%;font-family:sans-serif;font-size:12px;line-height:140%;text-align:center;color:#888888;" class="x-gmail-data-detectors">
                    <br>
                    <br>
                    &#169; Copyright {{ date( 'Y' ) }} Zoom Property. All rights reserved
                    <br>
                    <br>
                </td>
            </tr>

            <tr>
                <td style="padding:0px 0px 40px 0px;width:50%;font-family:sans-serif;font-size:12px;line-height:140%;text-align:center;color:#888888;" class="x-gmail-data-detectors"><a href="https://www.facebook.com/Zoom-Property-924405101041566/">Facebook</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.facebook' ) }}">Facebook</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.twitter' ) }}">Twitter</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.instagram' ) }}">Instagram</a>  <span style="margin:0;">|</span>  
                    <a href="{{ __( 'page.footer.social_media.googleplus' ) }}">Google Plus</a>
                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->
    </center>
</body>
</html>
