@php
    $links                      =   get_links();
    $locale                     =   \App::getLocale();
    $alternate_links            =   array_except($links,$locale);
    $url_without_page_param     =   remove_url_params(array_get($links,$locale),['page']);
    $canonical_link             =   starts_with(\Request::segment(5),'zp-') ? array_get($links,'en') : $url_without_page_param;
    $listings_page              =   (\Request::segment(2) == 'buy' || \Request::segment(2) == 'rent') && is_null(\Request::segment(5)) ? true : false;
    if($listings_page){
        $total_pages = (int)ceil(array_get($results,'total', 0) / 20);
        $parts = parse_url(array_get($links,$locale));
        parse_str(array_get($parts,'query'), $query);
        $current_page      =  (int)array_get($query, 'page',1);
        $canonical_link    =  array_get($links,$locale);
        $next_link         =  $current_page == $total_pages ? false : ($current_page == 1 ? (str_contains(array_get($links,$locale),'?') ? array_get($links,$locale).'&page=2' : array_get($links,$locale).'?page=2') : str_replace('page='.$current_page, 'page='.($current_page+1),array_get($links,$locale)));
        $prev_link         =  $current_page == 1 ? false : ($current_page == 2 ? $url_without_page_param : str_replace('page='.$current_page, 'page='.($current_page-1),array_get($links,$locale)));
    }
@endphp
<title>@yield( 'title' )</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="@yield( 'description' )" />
@yield('meta_tags')
<meta name="google-site-verification" content="DI-l8wfbTAzAIFcm4N6WN3E5AD3Dpu8QtBiq9pOFfzU" />
<meta name="yandex-verification" content="c21edba1f740553b" />
@notmobile
<meta name=viewport content="width=device-width, initial-scale=1" />
@elsenotmobile
<meta name="theme-color" content="#5d0e8b">
<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
@endnotmobile
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Zoom Property - @yield( 'title' )" />
<meta property="og:description" content="@yield( 'description' )" />
<meta property="og:url" content="{{ url()->current() }}" />
<meta property="og:site_name" content="Zoom Property" />
<meta property="og:image" content="https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/assets/img/seo/seo_meta_tag_header.jpg"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@zoompropertyuae"/>
<meta name="twitter:title" content="zoomproperty.com | Get Perfect Place To Live In  | Homes are Life"/>
<meta name="twitter:description" content="Get Perfect Place To Live In, residential properties and commercial real estate for sale and to rent in the UAE. Start your property search today on zoomproperty.com."/>
<meta name="twitter:image" content="https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/assets/img/seo/seo_meta_tag_header.jpg"/>
<meta name="twitter:image:alt" content="zoomproperty.com | Get Perfect Place To Live In  | Homes are Life">
<meta itemProp="name" content="zoomproperty.com | Get Perfect Place To Live In  | Homes are Life"/>
<meta itemProp="description" content="Get Perfect Place To Live In, residential properties and commercial real estate for sale and to rent in the UAE. Start your property search today on zoomproperty.com."/>
<meta itemProp="image" content="https://earth-consumerfe-assets.s3.ap-south-1.amazonaws.com/assets/img/seo/seo_meta_tag_header.jpg"/>
<meta itemProp="url" content="{{ url()->current() }}"/>
@if($listings_page)
    @if($prev_link)
    <link rel="prev" href="{{ $prev_link }}" />
    @endif
    @if($next_link)
    <link rel="next" href="{{ $next_link }}" />
    @endif
@endif
<link rel="canonical" href="{{ $canonical_link }}" />
@foreach($alternate_links as $alternate_locale => $alternate_link)
<link rel="alternate" href="{{ $alternate_link }}" hreflang="{{ $alternate_locale }}" />
@endforeach
<link rel="alternate" href="{{ array_get($links,'en') }}" hreflang="x-default" />
