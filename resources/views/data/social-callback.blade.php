@php $selectedLanguage	=	\App::getLocale(); @endphp
<script>
    var suburbsJson 	=	{!! json_encode( __('search.suburb')) !!},
        baseUrl 			=	'{{ env( "APP_URL" ) . \App::getLocale() . '/' }}',
        envUrl 				=	'{{ env( "APP_URL" ) }}',
        baseApiUrl 			=	'{{ env( "APP_URL" ) }}api/{{ env( "API_VERSION" ) }}/',
        cdnUrl 				=	'https://staging-assets.zoomproperty.com/';
</script>
@notmobile
    <script src="{{ env('ASSET_URL').'js/skeleton.min.js' }}"></script>
@elsenotmobile
    <script src="{{ env('ASSET_URL').'js/_skeleton.min.js' }}"></script>
@endnotmobile

<script>
    $( document ).ready( function(){

        user.auth.social.callback.__init( JSON.parse( '{!! json_encode( $result ) !!}' ));

    });
</script>
