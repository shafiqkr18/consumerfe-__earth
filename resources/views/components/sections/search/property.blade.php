<div id="property_search" class="ui container grid">
    <div class="thirteen wide column">
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="four wide stretched column">
                    <select class="ui dropdown" id="property_rent_buy"></select>
                </div>
                <div class="twelve wide stretched column">
                    <input class="autocomplete" data-multiple id="property_suburb" type="text" placeholder="{{__( 'page.form.placeholders.location' ) }}">
                    <input id="property_suburb_hidden" type="hidden" />
                    <input id="property_suburb_hidden_b" type="hidden" />
                </div>
            </div>
        </div>
        <div class="sixteen wide column advanced_search">
            <div class="ui grid">
                <div class="four wide stretched column">
                    <select class="ui dropdown" id="property_type"></select>
                </div>
                <div class="four wide stretched column">
                    <div class="ui grid">
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_bedroom_min"></select>
                        </div>
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_bedroom_max"></select>
                        </div>
                    </div>
                </div>
                <div class="four wide stretched column">
                    <div class="ui grid">
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_bathroom_min"></select>
                        </div>
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_bathroom_max"></select>
                        </div>
                    </div>
                </div>
                <div class="four wide stretched column">
                    <div class="ui grid">
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_area_min"></select>
                        </div>
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_area_max"></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column advanced_search">
            <div class="ui grid">
                <div class="four wide stretched column">
                    <div class="ui grid">
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_price_min"></select>
                        </div>
                        <div class="eight wide stretched column">
                            <select class="ui dropdown" id="property_price_max"></select>
                        </div>
                    </div>
                </div>
                <div class="four wide stretched column">
                    <select class="ui dropdown" id="property_furnishing"></select>
                </div>
                <div class="four wide stretched column">
                    <select class="ui dropdown" id="completion_status"></select>
                </div>
                <div class="four wide stretched column">
                    <div class="ui input">
                        <input id="keyword" placeholder="{{__('page.form.placeholders.keyword')}}" name="keyword" type="text" />
                    </div>
                </div>
            </div>
        </div>
        <div class="ui grid">
            <div class="ten wide column"></div>
            <div class="six wide column">
                <span onclick="helpers.filter.property();">{{__( 'page.form.placeholders.clear' ) }}</span>
                <a href="javascript:void(0);" onclick="pages.landing.search.toggle(this)">{{__( 'page.global.advanced_search' ) }}</a>
            </div>
        </div>
    </div>
    <div class="three wide column">
        <button type="button" name="button" class="massive ui purple button" onclick="search.property.regular.__init(this);">{{__( 'page.global.search' ) }}</button>
    </div>
</div>
