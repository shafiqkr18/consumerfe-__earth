<div class="ui grid" id="agent_search">
    <div class="thirteen wide column">
        <div class="ui grid">
            <div class="six wide stretched column">
                <select class="ui dropdown" id="agent_brokerage"></select>
            </div>
            <div class="five wide stretched column">
                <select class="ui dropdown" id="agent_areas"></select>
            </div>
            <div class="five wide stretched column">
                <div class="ui input">
                    <input id="keyword" placeholder="{{__('page.form.placeholders.keyword')}}" name="keyword" type="text" />
                </div>
            </div>
        </div>
        <div>
            <div class="sixteen wide column right aligned">
                <span onclick="helpers.filter.agent();">{{__( 'page.form.placeholders.clear' ) }}</span>
            </div>
        </div>
    </div>
    <div class="three wide column">
        <button type="button" name="button" class="massive ui purple button" onclick="search.agent.__init(this);">{{__( 'page.global.search' ) }}</button>
    </div>
</div>
