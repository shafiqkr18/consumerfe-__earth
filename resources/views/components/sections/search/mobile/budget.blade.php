<div id="budget_search">
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="sixteen wide stretched column">
                    <select class="ui dropdown" id="budget_city"></select>
                </div>
                <div class="sixteen wide stretched column">
                    <select class="ui dropdown" id="property_rent_buy"></select>
                </div>
                <div class="sixteen wide stretched column">
                    <select class="ui dropdown" id="property_price_min"></select>
                </div>
                <div class="sixteen wide stretched column">
                    <select class="ui dropdown" id="property_price_max"></select>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <button type="button" name="button" class="massive ui purple button" onclick="search.property.budget.__init(this);">{{__( 'page.global.search' ) }}</button>
        </div>
    </div>
</div>
