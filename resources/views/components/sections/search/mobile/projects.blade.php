<div id="projects_search" class="ui grid container">
    <div class="sixteen wide column">
        <div class="ui left icon input">
            <input id="property_suburb_project" type="text" class="autocomplete fluid" placeholder="{{__('page.form.placeholders.location')}}">
            <input id="property_suburb_hidden" type="hidden" />
            <i class="search icon"></i>
        </div>
    </div>
    <div class="ui grid container">
        <div class="eight wide column">
            <div onclick="pages.projects.listings.search.__init('Sale');">
                <span><i class="sliders horizontal icon"></i></span>
                <span>{{__('page.form.placeholders.filters')}}</span>
            </div>
        </div>
        <div class="eight wide column">
            <div>
                <span><i class="long arrow alternate up icon"></i></span>
                <span><i class="long arrow alternate down icon"></i></span>
                <span>{{__('page.global.featured')}}</span>
            </div>
        </div>
    </div>
</div>
