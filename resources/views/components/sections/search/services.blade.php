<div id="services_search" class="ui grid">
    <div class="row">
        <div class="thirteen wide column">
            <div class="ui grid">
                <div class="sixteen wide stretched column">
                    <div class="ui search selection dropdown" id="services_service">
                        <i class="dropdown icon"></i>
                        <div class="default text"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="three wide column">
            <button id="search" type="button" name="button" class="massive ui purple button" onclick="search.services.__init(this);">{{__( 'page.global.search' ) }}</button>
        </div>
    </div>
</div>
