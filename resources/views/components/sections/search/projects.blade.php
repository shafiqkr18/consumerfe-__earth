<div id="projects_search" class="ui grid container">
    <div class="row">
        <div class="thirteen wide column">
            <div class="sixteen wide column">
                <div class="ui grid">
                    <div class="ten wide stretched column">
                        <select class="ui dropdown" id="projects_project"></select>
                    </div>
                    <div class="six wide stretched column">
                        <select class="ui dropdown" id="completion_status"></select>
                    </div>
                </div>
            </div>
            <div class="sixteen wide column advanced_search">
                <div class="ui grid">
                    <div class="ten wide stretched column">
                        <input class="autocomplete" id="project_location" type="text" placeholder="{{__( 'page.form.placeholders.cities_areas' ) }}" />
                    </div>
                    <div class="six wide stretched column">
                        <div class="ui input">
                            <input id="keyword" placeholder="{{__('page.form.placeholders.keyword')}}" name="keyword" type="text" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="ten wide column"></div>
                <div class="six wide column">
                    <span onclick="helpers.filter.project();">{{__( 'page.form.placeholders.clear' ) }}</span>
                    <a href="javascript:void(0);" onclick="pages.landing.search.toggle(this)">{{__( 'page.global.advanced_search' ) }}</a>
                </div>
            </div>
        </div>
        <div class="three wide column">
            <button id="search" type="button" name="button" data-country="{{strtolower($country)}}" class="massive ui purple button" onclick="search.projects.__init(this);">{{ __( 'page.global.search' ) }}</button>
        </div>
    </div>
</div>
