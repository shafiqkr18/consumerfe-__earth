<div id="services_banner">
    <a>
        <div style="background-image: url( '{{ cdn_asset( 'assets/img/banner/services.jpg' ) }}' );">
            <!-- Banner -->
        </div>
    </a>

    <div>
        <div class="ui container">
            <!-- Tagline -->
            <div class="row">
                <div class="sixteen wide column">
                    <h1>{{ $seo_h1 }}</h1>
                </div>
            </div>

            <!-- Search -->
            @include( 'components.sections.search.services' )
        </div>
    </div>

</div>
