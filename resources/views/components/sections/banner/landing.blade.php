<div id="landing_search" style="background-image: url({{ array_has($takeover,'image_desktop') ? array_get($takeover,'image_desktop') : cdn_asset( 'assets/img/banner/landing.jpg' ) }})">

    <div>
        <div class="ui grid container">

            <!-- Tagline & Subtagline -->
            <div class="row">
                <div class="sixteen wide column">
                    <h1>{{__( 'page.sections.banner.landing.subtitle' ) }}</h1>
                </div>
            </div>

            <!-- Search -->
            @include( 'components.sections.search.property' )

        </div>

    </div>
</div>
