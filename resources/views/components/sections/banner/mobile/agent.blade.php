<div id="agent_banner">
    <div style="background-image: url( '{{ cdn_asset( 'assets/img/banner/agent-search.png' ) }}' );">
    </div>

    <div>
        <div class="ui container">
            <!-- Tagline & Subtagline -->
            <div class="row">
                <div class="sixteen wide column">
                    <h1>{{ __( 'page.sections.banner.agent.title' ) }}</h1>
                </div>
            </div>

            <!-- Search -->
            @include( 'components.sections.search.mobile.agent' )
        </div>
    </div>

</div>
