<div id="budget_banner">
    <a>
        <div style="background-image: url( '{{ cdn_asset( 'assets/img/banner/budget_search.jpg' ) }}' );">
            <!-- Banner -->
        </div>
    </a>

    <div>
        <div class="ui container">
            <!-- Tagline & Subtagline -->
            <div class="row">
                <div class="sixteen wide column">
                    <h1>{{__( 'page.sections.banner.budget.title' ) }}</h1>
                </div>
                <div class="sixteen wide column">
                    <h2>{{__( 'page.sections.banner.budget.subtitle' ) }}</h2>
                </div>
            </div>

            <!-- Search -->
            @include( 'components.sections.search.budget' )
        </div>
    </div>

</div>
