<div id="footer">
    
    {{-- Subscribtion --}}
    <div class="ui container">
        <div class="ui fluid">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div>{{__( 'page.footer.newsletter.title' )}}</div>
                </div>
                <div class="sixteen wide column">
                    <div class="ui grid">
                        <div class="sixteen wide column stretched">
                            <div class="ui icon input">
                                {{-- <i class="paper plane icon"></i> --}}
                                <input id="newsletter_email" type="text" name="" placeholder="{{__( 'page.form.placeholders.email' )}}" value="">
                            </div>
                        </div>
                        <div class="sixteen wide column stretched">
                            <button onclick="pages.landing.newsletter.__init()" type="button" class="ui button" name="button">{{__( 'page.form.button.subscribe' ) }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="ui container">
            <div class="ui grid">
                <!-- Popular Links -->
                <div class="sixteen wide column">
                    <div>{{__( 'page.footer.trending_searches' )}}</div>
                    <div class="ui list">
                        @include( 'components.sections.popular.searches' )
                    </div>
                </div>
                <div class="sixteen wide column">
                    <div>{{__( 'page.footer.popular_areas' )}}</div>
                    <div class="ui list">
                        @include( 'components.sections.popular.areas' )
                    </div>
                </div>
                <div class="sixteen wide column">
                    <div>{{__( 'page.footer.trending_properties' )}}</div>
                    <div class="ui list">
                        @include( 'components.sections.popular.properties' )
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    {{-- keep in touch --}}
    <div>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    {{-- <div>{{__( 'page.footer.keep_in_touch' )}}</div> --}}
                    <a href="{{ env( 'APP_URL' ) }}">
                        <img src="{{ cdn_asset('sprites.png') }}" style="object-fit:none;object-position:0 -52px;width: 111px;height: 56px;" alt="Zoom Property" />
                    </a>
                    {{-- <a href="mailto:hello@zoomproperty.com">
                        <div>
                            <i class="envelope icon"></i>
                            <span>hello@zoomproperty.com</span>
                        </div>
                    </a> --}}
                    <div>
                        <a href="{{config('data.footer.links.facebook')}}" target="_blank">
                            <i class="facebook f icon"></i>
                        </a>
                    </div>
                    <div>
                        <a href="{{config('data.footer.links.twitter')}}" target="_blank">
                            <i class="twitter icon"></i>
                        </a>
                    </div>
                    <div>
                        <a href="{{config('data.footer.links.instagram')}}" target="_blank">
                            <i class="instagram icon"></i>
                        </a>
                    </div>
                    {{-- <div>
                        <a href="{{config('data.footer.links.youtube')}}" target="_blank">
                            <i class="youtube icon"></i>
                        </a>
                    </div> --}}
                    <div>
                        <a href="{{config('data.footer.links.linkedin')}}" target="_blank">
                            <i class="linkedin icon"></i>
                        </a>
                    </div>
                    {{-- <div>
                        @if(false)
                        <a href="{{config('data.footer.links.google')}}" target="_blank">
                            <i class="google plus g icon"></i>
                        </a>
                        @endif
                    </div> --}}
                    {{-- <div class="ui grid">
                        <div class="eight wide column">
                            <a href="{{config('data.footer.links.app_store')}}" target="_blank">
                                <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" style="object-fit:none;object-position:-115px -43px;width: 120px;height: 40px;" class="responsive-img" />
                            </a>
                        </div>
                        <div class="eight wide column">
                            <a href="{{config('data.footer.links.play_store')}}" target="_blank">
                                <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" style="object-fit:none;object-position:-99px 0;width: 134px;height: 40px;" class="responsive-img" />
                            </a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    {{-- Terms and conditions --}}
    <div>
        <div class="ui container grid">
            <div class="sixteen wide column">
                <ul>
                    <li>
                        <a target="_blank" href="{{route('terms-and-conditions', ['lng' => \App::getLocale()])}}">
                            {{__( 'page.menu_breadcrumbs.terms_conditions' )}}
                        </a>
                        &nbsp;&nbsp;|
                    </li>
                    <li>
                        <a target="_blank" href="{{route('privacy-policy', ['lng' => \App::getLocale()])}}">
                            {{__( 'page.menu_breadcrumbs.privacy_policy' )}}
                        </a>
                        &nbsp;&nbsp;|
                    </li>
                    <li>
                        <a href="https://agents.zoomproperty.com/">
                            {{__( 'page.menu_breadcrumbs.for_brokers' )}}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="sixteen wide column">
                <ul>
                    <li><a href="#">© zoomproperty.com</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
