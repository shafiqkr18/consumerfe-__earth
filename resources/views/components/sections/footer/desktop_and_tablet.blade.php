<div id="footer">
    <div>
        <div class="ui container">
            <div class="ui grid">
                <!-- Popular Links -->
                <div class="four wide column">
                    <h4>{{__( 'page.footer.trending_searches' )}}</h4>
                    <div class="ui list">
                        @include( 'components.sections.popular.searches' )
                    </div>
                </div>
                <!-- Popular area -->
                <div class="four wide column">
                    <h4>{{__( 'page.footer.popular_areas' )}}</h4>
                    <div class="ui list">
                        @include( 'components.sections.popular.areas' )
                    </div>
                </div>
                {{-- Popular property --}}
                <div class="four wide column">
                    <h4>{{__( 'page.footer.trending_properties' )}}</h4>
                    <div class="ui list">
                        @include( 'components.sections.popular.properties' )
                    </div>
                </div>
                <div class="four wide column">
                    <div class="ui grid">
                        <div class="sixteen wide column">
                            <h4>{{__( 'page.footer.newsletter.title' ) }}</h4>
                        </div>
                        <div class="sixteen wide column stretched">
                            <div class="ui icon input">
                                <i class="paper plane icon"></i>
                                <input id="newsletter_email" type="text" name="" placeholder="{{__( 'page.form.placeholders.email' )}}" value="">
                            </div>
                        </div>
                        <div class="sixteen wide column stretched">
                            <button onclick="pages.landing.subscribe_for_newsletter(this)" type="button" class="ui button" name="button">{{__( 'page.form.button.subscribe' ) }}</button>
                        </div>
                    </div>
                    {{-- <div class="ui grid">
                        <div class="eight wide column">
                            <a href="{{config('data.footer.links.app_store')}}" target="_blank">
                                <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" style="object-fit:none;object-position:-115px -43px;width: 120px;height: 40px;" class="responsive-img" />
                            </a>
                        </div>
                        <div class="eight wide column">
                            <a href="{{config('data.footer.links.play_store')}}" target="_blank">
                                <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" style="object-fit:none;object-position:-99px 0;width: 134px;height: 40px;" class="responsive-img" />
                            </a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="ui container grid">
            <div class="two wide column">
                <a href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">
                    <img src="{{ cdn_asset('assets/img/logo-purple.png') }}" style="width: 65px;height: 32px;" alt="Zoom Property" />
                </a>
            </div>
            <div class="three wide column">
                <div>
                    <a href="{{config('data.footer.links.facebook')}}" target="_blank">
                        <i class="facebook f icon"></i>
                    </a>
                </div>
                <div>
                    <a href="{{config('data.footer.links.twitter')}}" target="_blank">
                        <i class="twitter icon"></i>
                    </a>
                </div>
                <div>
                    <a href="{{config('data.footer.links.instagram')}}" target="_blank">
                        <i class="instagram icon"></i>
                    </a>
                </div>
                <div>
                    <a href="{{config('data.footer.links.youtube')}}" target="_blank">
                        <i class="youtube icon"></i>
                    </a>
                </div>
                <div>
                    <a href="{{config('data.footer.links.linkedin')}}" target="_blank">
                        <i class="linkedin icon"></i>
                    </a>
                </div>
                <div>
                    @if(false)
                    <a href="{{config('data.footer.links.google')}}" target="_blank">
                        <i class="google plus g icon"></i>
                    </a>
                    @endif
                </div>
            </div>
            <div class="seven wide column">
                <ul>
                    <li>
                        <a href="{{route('terms-and-conditions',['lng' => \App::getLocale() ])}}">
                            {{__( 'page.menu_breadcrumbs.terms_conditions' )}}
                        </a>
                        &nbsp;&nbsp;|
                    </li>
                    <li>
                        <a href="{{route('privacy-policy',['lng' => \App::getLocale() ])}}">
                            {{__( 'page.menu_breadcrumbs.privacy_policy' )}}
                        </a>
                        &nbsp;&nbsp;|
                    </li>
                    <li>
                        <a href="https://agents.zoomproperty.com/">
                            {{__( 'page.menu_breadcrumbs.for_brokers' )}}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="four wide column">
                <ul>
                    <li><a href="javascript:void(0);">&copy; 2018 - {{ date('Y') }} zoomproperty.com</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
