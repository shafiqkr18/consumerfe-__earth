@php $links = get_links(); @endphp
<div id="sidedrawer_mobile" class="ui vertical sidebar menu left">
  <div class="ui container" dir="{{ \App::getLocale() == 'ar' ? 'rtl' : 'ltr'}}">
    <div class="ui grid">
      <a href="{{ env( 'APP_URL' ). \App::getLocale() }}">
        <img src="{{ cdn_asset('sprites.png') }}" alt="Zoom Property" />
      </a>
    </div>
    <div class="ui grid">
      <div class="sixteen wide column">
        <div class="ui list">
          <a href="javascript:void(0)" class="state_li" onclick="user.dashboard.trigger.__init();"><div class="item">{{__( 'page.menu_breadcrumbs.my_account' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="javascript:void(0)" onclick="pages.landing.search.__init('Sale');"><div class="item">{{__( 'page.menu_breadcrumbs.search_property' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">
            <div class="item">
              {{__( 'page.menu_breadcrumbs.new_projects')}}
              ({{ (Lang::has('page.global.'.snake_case_custom('uae'), \App::getLocale())) ? __('page.global.'.snake_case_custom('uae')) : 'UAE' }})
              <i class="caret right alternate icon"></i>
            </div>
          </a>
          <a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'international' ])}}">
            <div class="item">
              {{__( 'page.menu_breadcrumbs.new_projects' )}}
              ({{ (Lang::has('page.global.'.snake_case_custom('international'), \App::getLocale())) ? __('page.global.'.snake_case_custom('international')) : 'International' }})
              <i class="caret right alternate icon"></i>
            </div>
          </a>
          <a href="{{route('budget-search', [ 'lng'=>\App::getLocale() ])}}"><div class="item">{{__( 'page.menu_breadcrumbs.budget_search' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="{{route('agent-finder-listings', [ 'lng'=>\App::getLocale(),'city' => 'uae', 'agency'=>'all-agency', 'suburb'=>'all-suburbs'  ])}}"><div class="item">{{__( 'page.menu_breadcrumbs.agent_finder' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="javascript:void(0)" onclick="user.property.alert.__init(this, 'mobile');"><div class="item">{{__( 'page.menu_breadcrumbs.create_alert' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="{{route('services', [ 'lng'=>\App::getLocale() ])}}"><div class="item">{{__( 'page.menu_breadcrumbs.home_services' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="https://blog.zoomproperty.com/"><div class="item">{{__( 'page.menu_breadcrumbs.blog' )}}<i class="caret right alternate icon"></i></div></a>
          <a onclick="helpers.redirect('Sale');" href="javascript:void(0)"><div class="item">{{__( 'page.menu_breadcrumbs.buy' )}}<i class="caret right alternate icon"></i></div></a>
          <a onclick="helpers.redirect('Rent');" href="javascript:void(0)"><div class="item">{{__( 'page.menu_breadcrumbs.rent' )}}<i class="caret right alternate icon"></i></div></a>
          <a onclick="helpers.redirect('Commercial-Sale');" href="javascript:void(0)"><div class="item">{{__( 'page.menu_breadcrumbs.commercial_buy' )}}<i class="caret right alternate icon"></i></div></a>
          <a onclick="helpers.redirect('Commercial-Rent');" href="javascript:void(0)"><div class="item">{{__( 'page.menu_breadcrumbs.commercial_rent' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="https://agents.zoomproperty.com/"><div class="item">{{__( 'page.menu_breadcrumbs.agents_login' )}}<i class="caret right alternate icon"></i></div></a>
          <a href="javascript:void(0)" class="state_lo" onclick="pages.signin.modal.__init();"><div class="item">{{__( 'page.modals.signin_register.signin' )}}<i class="sign in alternate icon"></i></div></a>
        </div>
      </div>
      <div class="sixteen wide column">
        {{-- TODO Uncomment when chinese is ready to deploy
        <div class="ui equal width grid">
          <div class="column">
            <div class="item {{ \App::getLocale() == 'en' ? 'active' : ''}}"><a href="{{ array_get($links,'en') }}">En</a></div>
          </div>
          <div class="column">
            <div class="item {{ \App::getLocale() == 'ar' ? 'active' : ''}}"><a href="{{ array_get($links,'ar','en') }}">عربى</a></div>
          </div>
          <div class="column">
            <div class="item {{ \App::getLocale() == 'ch' ? 'active' : ''}}"><a href="{{ array_get($links,'ch','en') }}">中文</a></div>
          </div>
        </div>
        --}}
        {{-- TODO Delete when chinese is ready to deploy --}}
        <div class="ui grid">
          <div class="eight wide column">
            <div class="item {{ \App::getLocale() == 'en' ? 'active' : ''}}"><a href="{{ array_get($links,'en') }}">En</a></div>
          </div>
          <div class="eight wide column">
            <div class="item {{ \App::getLocale() == 'ar' ? 'active' : ''}}"><a href="{{ array_get($links,'ar','en') }}">عربى</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
