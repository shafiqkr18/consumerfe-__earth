@php $links = get_links(); @endphp
<nav id="header_transparent">
    <div class="ui grid container">
        <div class="two wide column">
            <a href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">
                <img src="{{ cdn_asset('assets/img/logo-purple.png') }}" alt="Zoom Property" />
            </a>
        </div>

        <div class="fourteen wide column">

            <div class="ui grid container">
                <div class="sixteen wide column">
                    <div class="ui menu inverted">
                      <!--  <div class="item"><a href="{{config('data.footer.links.facebook')}}" target="_blank"><i class="facebook f icon"></i></a></div>
                        <div class="item"><a href="{{config('data.footer.links.twitter')}}" target="_blank"><i class="twitter icon"></i></a></div>
                        <div class="item"><a href="{{config('data.footer.links.instagram')}}" target="_blank"><i class="instagram icon"></i></a></div>
                        <div class="item"><a href="{{config('data.footer.links.youtube')}}" target="_blank"><i class="youtube icon"></i></a></div>
                        <div class="item"><a href="{{config('data.footer.links.linkedin')}}" target="_blank"><i class="linkedin icon"></i></a></div>
                      --><div class="item highightar"><a href="https://agents.zoomproperty.com/">{{__( 'page.menu_breadcrumbs.list_property' )}}</a></div>
                        <div class="item state_lo"><a href="javascript:void(0)" onclick="pages.signin.modal.__init();">{{__( 'page.modals.signin_register.signin' )}}</a></div>
                        <div class="item state_li"><a href="javascript:void(0)" onclick="user.dashboard.trigger.__init();">{{__('page.global.hi')}} <span class="dashboard"></span>!</a></div>
                        {{-- TODO Uncomment when chinese is ready to deploy
                        <div class="ui simple dropdown item">
                        <div class="{{ ( \App::getLocale() === 'en' ) ? 'hide' : 'item' }}"><a href="{{ array_get($links,'en') }}">En</a></div>
                        <div class="{{ ( \App::getLocale() !== 'en' ) ? 'hide' : 'item' }}"><a href="{{ array_get($links,'ar','en') }}">عربى</a></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="{{ ( \App::getLocale() === 'en' || \App::getLocale() === 'ar') ? 'hide' : 'item' }}"><a href="{{ array_get($links,'ar','en') }}">عربى</a></div>
                            <div class="{{ ( \App::getLocale() === 'ch') ? 'hide' : 'item' }}"class="item"><a href="{{ array_get($links,'ch','en') }}">中文</a></div>
                        </div>
                        </div>
                        --}}
                        {{-- TODO Delete when chinese is ready to deploy --}}
                        <div class="{{ ( \App::getLocale() === 'en' ) ? 'hide' : 'item' }}"><a href="{{ array_get($links,'en') }}">En</a></div>
                        <div class="{{ ( \App::getLocale() === 'ar' ) ? 'hide' : 'item' }}"><a href="{{ array_get($links,'ar','en') }}">عربى</a></div>
                    </div>
                </div>

                <div class="computer only sixteen wide column">
                    <div class="ui menu inverted">
                        @if(empty($internal_links))
                        <div class="item"><a onclick="helpers.redirect('Sale');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.buy' )}}</a></div>
                        <div class="item"><a onclick="helpers.redirect('Rent');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.rent' )}}</a></div>
                        @else
                            @include( 'components.sections.ilinks.nav.desktop_and_tablet' )
                        @endif
                        <div class="ui simple pointing dropdown item">
                            {{__( 'page.menu_breadcrumbs.new_projects' )}}
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="item"><a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">
                                    {{ Lang::has('data.country.'.snake_case_custom('United Arab Emirates (UAE)')) ? __( 'data.country.'.snake_case_custom('United Arab Emirates (UAE)')) : 'United Arab Emirates (UAE)' }}
                                </a></div>
                                <div class="item"><a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'international' ])}}">
                                    {{ (Lang::has('page.global.international_projects', \App::getLocale())) ? __('page.global.international_projects') : __('page.global.international') }}
                                </a></div>
                            </div>
                        </div>
                        <div class="ui simple pointing dropdown item">
                            {{__( 'page.menu_breadcrumbs.commercial' )}}
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="item"><a onclick="helpers.redirect('Commercial-Sale');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.commercial_buy' )}}</a></div>
                                <div class="item"><a onclick="helpers.redirect('Commercial-Rent');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.commercial_rent' )}}</a></div>
                            </div>
                        </div>
                        {{-- <div class="item"><a href="{{route('budget-search', [ 'lng'=>\App::getLocale() ])}}">{{__( 'page.menu_breadcrumbs.budget_search' )}}</a></div> --}}
                        <div class="item"><a href="{{route('agent-finder-listings', [ 'lng'=>\App::getLocale(),'city' => 'uae', 'agency'=>'all-agency', 'suburb'=>'all-suburbs' ])}}">{{__( 'page.menu_breadcrumbs.agent_finder' )}}</a></div>
                        <div class="item"><a href="https://blog.zoomproperty.com/">{{__( 'page.menu_breadcrumbs.blog' )}}</a></div>
                        <div class="ui simple pointing top right dropdown item">
                            {{__( 'page.menu_breadcrumbs.explore' )}}
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="item"><a href="{{route('services', [ 'lng'=>\App::getLocale() ])}}">{{__( 'page.menu_breadcrumbs.home_services' )}}</a></div>
                                <div class="item"><a href="javascript:void(0)" onclick="user.property.alert.__init(this);">{{__( 'page.menu_breadcrumbs.create_alert' )}}</a></div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="tablet only sixteen wide column">
                    <div class="ui menu inverted">
                        <div class="item"><a onclick="helpers.redirect('Sale');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.buy' )}}</a></div>
                        <div class="item"><a onclick="helpers.redirect('Rent');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.rent' )}}</a></div>
                        <div class="ui simple dropdown item">
                            {{__( 'page.menu_breadcrumbs.new_projects' )}}
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="item"><a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'uae' ])}}">{{ 'United Arab Emirates (UAE)' }}</a></div>
                                <div class="item"><a href="{{route('new-projects-list', [ 'lng'=>\App::getLocale(), 'country'=>'international' ])}}">{{ 'International' }}</a></div>
                            </div>
                        </div>
                        <div class="ui simple dropdown item">
                            {{__( 'page.menu_breadcrumbs.commercial' )}}
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="item"><a onclick="helpers.redirect('Commercial-Sale');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.commercial_buy' )}}</a></div>
                                <div class="item"><a onclick="helpers.redirect('Commercial-Rent');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.commercial_rent' )}}</a></div>
                            </div>
                        </div>
                        <div class="ui simple dropdown item">
                            {{__( 'page.menu_breadcrumbs.more' )}}
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="item"><a href="{{route('services', [ 'lng'=>\App::getLocale() ])}}">{{__( 'page.menu_breadcrumbs.home_services' )}}</a></div>
                                <div class="item"><a href="https://blog.zoomproperty.com/">{{__( 'page.menu_breadcrumbs.blog' )}}</a></div>
                                <div class="item"><a href="{{route('budget-search', [ 'lng'=>\App::getLocale() ])}}">{{__( 'page.menu_breadcrumbs.budget_search' )}}</a></div>
                                <div class="item"><a href="{{route('agent-finder-listings', [ 'lng'=>\App::getLocale(),'city' => 'uae', 'agency'=>'all-agency', 'suburb'=>'all-suburbs' ])}}">{{__( 'page.menu_breadcrumbs.agent_finder' )}}</a></div>
                                <div class="item"><a href="javascript:void(0)" onclick="user.property.alert.__init(this);">{{__( 'page.menu_breadcrumbs.create_alert' )}}</a></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
