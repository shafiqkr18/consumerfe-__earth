@php $links = get_links(); @endphp
<nav>
    <div class="ui grid">
        <div class="three wide column ui large secondary menu">
            <a class="launch item" onclick="pages.drawer.__init();"><i class="content icon"></i></a>
        </div>
        <div class="ten wide column">
            <a class="item" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}"><img src="{{cdn_asset('assets/img/logo-purple.png')}}" alt="Zoom Property" /></a>
        </div>
        <div class="three wide column">
            <i class="globe icon"></i>
            <select name="language" id="language" onchange="other.change_language( this );">
                <option data-lang="{{ array_get($links,'en') }}" value="en">EN</option>
                <option data-lang="{{ array_get($links,'ar') }}" value="ar">عربى</option>
            </select>
            {{-- <a href="javascript:void(0)" hidden class="state_li" onclick="user.dashboard.trigger.__init();"><i class="icon user outline"></i></a> --}}
            {{-- <a href="javascript:void(0)" class="state_lo" onclick="pages.signin.modal.__init();"><i class="icon user outline"></i></a> --}}
        </div>
    </div>
</nav>
