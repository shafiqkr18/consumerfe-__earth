<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
          <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
          <i class="right chevron icon divider"></i>
          <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/'. 'budget-search' }}">{{__( 'page.menu_breadcrumbs.budget_search' ) }}</a>
          <i class="right chevron icon divider"></i>
          <a class="active section">{{__( 'page.global.properties' ) }}</a>
        </div>
    </div>
</div>
