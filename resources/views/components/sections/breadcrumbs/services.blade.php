<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
            <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/'. 'services' }}">{{__( 'page.menu_breadcrumbs.home_services' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="active section">
              {{ ($locale !== 'en' && Lang::has('data.ancillary.'.snake_case_custom(str_replace('-',' ',str_after($service,'zst-'))), $locale)) ? __( 'data.ancillary.'.snake_case_custom(str_replace('-',' ',str_after($service,'zst-')))) : title_case(str_replace('-',' ',str_after($service,'zst-')))}}
          </a>
        </div>
    </div>
</div>
