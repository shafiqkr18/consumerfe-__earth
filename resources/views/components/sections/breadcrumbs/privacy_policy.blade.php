<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
          <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
          <i class="right chevron icon divider"></i>
          <a class="active section">{{__( 'page.menu_breadcrumbs.privacy_policy' ) }}</a>
        </div>
    </div>
</div>
