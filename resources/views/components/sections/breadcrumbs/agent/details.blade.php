<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
            <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{ route('agent-finder-listings', ['lng' => \App::getLocale(), 'city' => 'uae', 'agency'=>'all-agency', 'suburb'=>'all-suburbs']) }}">{{trans_choice(  __( 'page.global.agent' ), 1 ) }}</a>
            <i class="right chevron icon divider"></i>
            @if(array_has($data,'agency.contact.name'))
                <a class="section" href="{{ route('agent-finder-listings', ['lng' => \App::getLocale(), 'city' => 'uae', 'agency'=>array_get($data,'agency._id'), 'suburb'=>'all-suburbs']) }}">{{array_get($data,'agency.contact.name')}}</a>
                <i class="right chevron icon divider"></i>
            @endif
            <a class="active section">{{array_get($data,'contact.name','An Amazing Agent')}}</a>
        </div>
    </div>
</div>
