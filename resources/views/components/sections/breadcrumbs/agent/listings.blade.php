<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
          <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
          <i class="right chevron icon divider"></i>
          <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/agents-search/uae/all-agency/all-suburbs' }}">{{__( 'page.menu_breadcrumbs.agent_finder' ) }}</a>
          <i class="right chevron icon divider"></i>
          <a class="active section">{{trans_choice(  __( 'page.global.agent' ), 1 ) }}</a>
        </div>
    </div>
</div>
