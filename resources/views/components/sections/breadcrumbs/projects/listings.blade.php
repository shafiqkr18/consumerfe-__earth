<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
            <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
            <i class="right chevron icon divider"></i>
            @if(!empty($developer))
                <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/' . 'projects/' . strtolower($country) }}">
                    {{__( 'page.menu_breadcrumbs.new_projects' ) }}
                    <i class="right chevron icon divider"></i>
                    {{ (Lang::has('page.global.'.snake_case_custom($country), $locale)) ? __('page.global.'.snake_case_custom($country)) : $country }}
                </a>
                <i class="right chevron icon divider"></i>
                <a class="section active">{{ $developer }}</a>
            @else
                <a class="section active">
                    {{__( 'page.menu_breadcrumbs.new_projects' ) }}
                    <i class="right chevron icon divider"></i>
                    {{ (Lang::has('page.global.'.snake_case_custom($country), $locale)) ? __('page.global.'.snake_case_custom($country)) : $country }}
                </a>
            @endif
        </div>
    </div>
</div>
