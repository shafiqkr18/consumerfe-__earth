<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
            <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/' . 'projects/' . strtolower($country) }}">{{__( 'page.menu_breadcrumbs.new_projects' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/' . 'projects/' . strtolower($country) }}">
                {{ (Lang::has('page.global.'.snake_case_custom($country), $locale)) ? __('page.global.'.snake_case_custom($country)) : $country }}
            </a>
            <i class="right chevron icon divider"></i>
            @if(array_has($data,'developer.contact.name'))
                <a class="section" href="{{ env( 'APP_URL' ) . \App::getLocale() . '/' . 'projects/' . strtolower($country) . '/' . array_get($data,'developer._id') }}">
                    {{ array_get($data,'developer.contact.name') }}
                </a>
                <i class="right chevron icon divider"></i>
            @endif
            <a class="active section">{{ array_get($data,'name','') }}</a>
        </div>
    </div>
</div>
