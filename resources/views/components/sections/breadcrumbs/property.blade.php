@php
 $rent_buy  = (array_get( $query, 'rent_buy.0', 'Sale' ) == 'Sale' ) ? 'Buy' : array_get( $query, 'rent_buy.0' );
 $residential_commercial  = array_get( $query, 'residential_commercial.0' );
 if(isset($query['city'])){
   $cities = [];
   foreach($query['city'] as $key => $city){
       $cities[]= (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
   }
 }
 if(isset($query['area'])){
   $areas = [];
   foreach($query['area'] as $key => $area){
       $areas[]= (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
   }
 }
 if(isset($query['building'])){
   $buildings = [];
   foreach($query['building'] as $key => $building){
       $buildings[]= (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
   }
  }
@endphp
<div id="breadcrumbs">
    <div class="ui container">
        <div class="ui small breadcrumb">
            <a class="section" href="{{ env( 'APP_URL' ) . (\App::getLocale() != 'en' ? \App::getLocale() : '') }}">{{__( 'page.menu_breadcrumbs.home' ) }}</a>
            {{-- @if(isset($query['residential_commercial']) && isset($query['rent_buy']))
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)).'/'.strtolower($residential_commercial).'/properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ])}}">
                {{ $residential_commercial == 'Commercial' ? __( 'data.residential_commercial.'.snake_case_custom($residential_commercial) ) : ''}}
                {{__( 'data.rent_buy.'.snake_case_custom($rent_buy) )}}
            </a>
            @endif --}}
            @if(isset($query['type']))
            @if(!isset($query['city']))
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ])}}"> {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $query, 'type.0', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $query, 'type.0', '' ))) : array_get( $query, 'type.0', '' ) }}</a>
            @elseif(isset($query['city']) && !isset($query['area']))
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ])}}">{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $query, 'type.0', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $query, 'type.0', '' ))) : array_get( $query, 'type.0', '' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'-city'}}">{{implode(' & ',$cities )}}</a>
            @elseif(isset($query['area']) && !isset($query['building']))
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ])}}">{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $query, 'type.0', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $query, 'type.0', '' ))) : array_get( $query, 'type.0', '' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'-city'}}">{{implode(' & ',$cities )}}</a>
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )), 'city'=>strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['area'] ))).'-area'}}">{{implode(' & ',$areas )}}</a>
            @elseif(isset($query['building']))
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ])}}">{{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $query, 'type.0', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $query, 'type.0', '' ))) : array_get( $query, 'type.0', '' ) }}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'-city'}}">{{implode(' & ',$cities )}}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )), 'city'=>strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['area'] ))).'-area'}}">{{implode(' & ',$areas )}}</a>
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )), 'city'=>strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))), strtolower(str_replace(' ', '-',implode('-or-',$query['area'] ))).'/'.strtolower(kebab_case(array_get( $query, 'type.0', '' ))).'-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['building'] ))).'-building'}}">{{implode(' & ',$buildings )}}</a>
            @endif
            @else
            @if(isset($query['city']) && !isset($query['area']))
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.'properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'-city'}}">{{implode(' & ',$cities )}}</a>
            @elseif(isset($query['area']) && !isset($query['building']))
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.'properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'-city'}}">{{implode(' & ',$cities )}}</a>
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )), 'city'=>strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'/'.'properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['area'] ))).'-area'}}">{{implode(' & ',$areas )}}</a>
            @elseif(isset($query['building']))
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )).'/'.'properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'-city'}}">{{implode(' & ',$cities )}}</a>
            <i class="right chevron icon divider"></i>
            <a class="section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )), 'city'=>strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))).'/'.'properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['area'] ))).'-area'}}">{{implode(' & ',$areas )}}</a>
            <i class="right chevron icon divider"></i>
            <a class="active section" href="{{route('search-results-page', [ 'lng'=>\App::getLocale(), 'rent_buy'=>strtolower(kebab_case($rent_buy)), 'residential_commercial'=>strtolower(array_get( $query, 'residential_commercial.0', '' )), 'city'=>strtolower(str_replace(' ', '-',implode('-or-',$query['city'] ))), strtolower(str_replace(' ', '-',implode('-or-',$query['area'] ))).'/'.'properties-for-'.strtolower(kebab_case(array_get( $query, 'rent_buy.0', '' ))) ]).'-in-'.strtolower(str_replace(' ', '-',implode('-or-',$query['building'] ))).'-building'}}">{{implode(' & ',$buildings )}}</a>
            @endif
            @endif
        </div>
    </div>
</div>
