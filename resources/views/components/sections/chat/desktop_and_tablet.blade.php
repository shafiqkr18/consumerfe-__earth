<div id="chat_wrapper">
    <div>
        <div>
            <span>{{__( 'page.sections.chat.title' ) }}</span>
        </div>
        <div>
            <a href="javascript:void(0)" id="chat_minimize" onclick="chat.minimize();"><i class="window minimize outline icon"></i></a>
            <a href="javascript:void(0)" id="chat_maximize" onclick="chat.maximize();"><i class="window maximize outline outline icon"></i></a>
            <a href="javascript:void(0)" onclick="chat.close();"><i class="window close outline icon"></i></a>
        </div>
    </div>
    <div class="chat">
        <div class="chat-container">
            <div class="chat-listcontainer">
                <ul class="chat-message-list"></ul>
            </div>
        </div>
        <div class="ui grid">
            <div class="thirteen wide column">
                <div class="ui form">
                    <textarea placeholder="Type your message" rows="1"></textarea>
                </div>
            </div>
            <div class="three wide column">
                <button type="button" class="ui button fluid" name="button"><i class="paper plane icon"></i></button>
            </div>
        </div>
    </div>
</div>
