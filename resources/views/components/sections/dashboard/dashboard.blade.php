@notmobile
    @include( 'components.sections.dashboard.desktop_and_tablet' )
    @include( 'components.modals.property.call.desktop_and_tablet' )
    @include( 'components.modals.property.callback.desktop_and_tablet' )
    @include( 'components.modals.property.email.desktop_and_tablet' )

@elsenotmobile
    @include( 'components.sections.dashboard.mobile' )
    @include( 'components.modals.property.call.mobile' )
    @include( 'components.modals.property.callback.mobile' )
    @include( 'components.modals.property.email.mobile' )
    @include( 'components.modals.property.property_search' )
@endnotmobile
