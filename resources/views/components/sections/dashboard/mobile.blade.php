<div class="ui bottom sidebar user_dashboard" id="user_dashboard">

    <div class="ui grid container">
        <div class="four wide column">
            <a href="javascript:void(0)" onclick="pages.dashboard.back.__init();" class="item user_dashboard_back_arrow hide"><i class="arrow left icon"></i></a>
        </div>
        <div class="eight wide column">
            <img src="{{cdn_asset('sprites.png')}}" style="object-fit:none;object-position:0 -52px;width: 111px;height: 56px; background: 50% 50%" alt="Zoom Property" class="ui small centered image">
        </div>
        <div class="four wide column">
            <a href="javascript:void(0)" class="item hide"><i class="remove icon"></i></a>
        </div>
    </div>

    <div class="ui grid user_dashboard_list">
        <div class="sixteen wide column">
            <div class="ui list">
                <a href="javascript:void(0)" onclick="pages.dashboard.profile.__init();"><div class="item">{{__( 'page.sections.dashboard.profile' ) }}<i class="caret right alternate icon"></i></div></a>
                <a href="javascript:void(0)" onclick="pages.dashboard.favourite.__init('search');"><div class="item">{{__( 'page.sections.dashboard.favourite_searches' ) }}<i class="caret right alternate icon"></i></div></a>
                <a href="javascript:void(0)" onclick="pages.dashboard.favourite.__init('property');"><div class="item">{{__( 'page.sections.dashboard.favourite_properties' ) }}<i class="caret right alternate icon"></i></div></a>
                <a href="javascript:void(0)" onclick="pages.dashboard.favourite.__init('alerts');"><div class="item">{{__( 'page.sections.dashboard.alerts' ) }}<i class="caret right alternate icon"></i></div></a>
                <a href="javascript:void(0)" onclick="pages.dashboard.settings.__init();"><div class="item">{{__( 'page.sections.dashboard.settings' ) }}<i class="caret right alternate icon"></i></div></a>
                <a href="javascript:void(0)" onclick="user.auth.logout.__init('mobile');"><div class="item">{{__( 'page.sections.dashboard.sign_out' ) }}<i class="caret right alternate icon"></i></div></a>
            </div>
        </div>
    </div>

    <div class="ui hide grid user_dashboard_profile">
        <div class="ui grid container">
            <div class="sixteen wide column">
                <div class="ui input form fluid disabled" id="profile_update_name">
                    <input name="name" type="text" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="">
                </div>
            </div>
            <div class="sixteen wide column">
                <div class="ui input form fluid disabled" id="profile_update_phone">
                    <input name="number" type="number" placeholder="{{__( 'page.form.placeholders.phone' ) }}" value="">
                </div>
            </div>
            <div class="sixteen wide column">
                <div class="ui input form fluid disabled">
                    <input name="email" type="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="">
                </div>
            </div>
            <div class="sixteen wide column">
                <div class="ui input form fluid disabled" id="profile_update_nationality">
                    <input name="nationality" type="text" placeholder="{{__( 'page.form.placeholders.nationality' ) }}" value="" />
                </div>
            </div>
            <div class="sixteen wide column">
                <button onclick="user.dashboard.profile.update(this);" id="profile_update_btn" type="button" class="ui button disabled" name="button">{{__( 'page.form.button.update' ) }}</button>
                <button onclick="user.dashboard.profile.edit(this);" id="profile_edit_btn" type="button" class="ui button" name="button">{{__( 'page.form.button.edit' ) }}</button>
                <button onclick="user.dashboard.profile.cancel(this);" id="profile_cancel_btn" type="button" class="ui button hide" name="button">{{__( 'page.form.button.cancel' ) }}</button>
            </div>
        </div>
    </div>

    <div class="ui hide grid user_dashboard_fav_searches container">
        <div id="favourite_searches">
            @include( 'components.cards.favourite_search' )
        </div>
    </div>

    <div class="ui hide grid user_dashboard_fav_properties container">
        <div id="favourite_properties">
            @include( 'components.cards.favourite_property' )
        </div>
    </div>

    <div class="ui hide grid user_dashboard_fav_alerts container">
        <div id="user_alerts">
            @include( 'components.cards.user_alerts' )
        </div>
    </div>

    <div class="ui hide grid user_dashboard_settings">
        <div class="ui grid">
            <div class="ten wide column">
                <p>{{__( 'page.sections.dashboard.subscribe' ) }}</p>
            </div>
            <div class="six wide column">
                <div class="ui toggle checkbox">
                  <input type="checkbox" name="public">
                  <label></label>
                </div>
            </div>
        </div>
        <div class="ui grid form">
            <div class="sixteen wide column">
                <div>{{__( 'page.sections.dashboard.change_password' ) }}</div>
            </div>
            <div class="sixteen wide column">
                <div class="ui input fluid disabled" id="change_password_old_password">
                    <input type="password" placeholder="{{__( 'page.form.placeholders.password_current' ) }}" value="">
                </div>
            </div>
            <div class="sixteen wide column">
                <div class="ui input fluid disabled" id="change_password_password">
                    <input type="password" placeholder="{{__( 'page.form.placeholders.password_new' ) }}" value="">
                </div>
            </div>
            <div class="sixteen wide column">
                <div class="ui input fluid disabled" id="change_password_password_confirmation">
                    <input type="password" placeholder="{{__( 'page.form.placeholders.password_confirm' ) }}" value="">
                </div>
            </div>
            <p id="change_password_error" class="error"></p>
            <div class="sixteen wide column">
                <button onclick="user.dashboard.password.update(this);" id="change_password_update_btn" type="button" class="ui button disabled" name="button">{{__( 'page.form.button.change_password' ) }}</button>
                <button onclick="user.dashboard.password.edit(this);" id="change_password_edit_btn" type="button" class="ui button" name="button">{{__( 'page.form.button.edit' ) }}</button>
                <button onclick="user.dashboard.password.cancel(this);" id="change_password_cancel_btn" type="button" class="ui button hide" name="button">{{__( 'page.form.button.cancel' ) }}</button>
            </div>
        </div>
    </div>

    <div id="toast_dashboard" style="display: none;"></div>
</div>
