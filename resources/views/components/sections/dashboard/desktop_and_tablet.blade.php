<div class="ui inverted right vertical sidebar menu user_dashboard" id="user_dashboard">
    <div class="row">
        <div class="ui grid">
            <div class="four wide column">
                <p class='name'></p>
                <a class="item active" data-tab="first">
                    {{__( 'page.sections.dashboard.profile' ) }}
                </a>
                <a class="item" data-tab="second" onclick="user.property.favourite.property.searches();">
                    {{__( 'page.sections.dashboard.favourite_searches' ) }}
                </a>
                <a class="item" data-tab="third" onclick="user.property.favourite.property.properties();">
                    {{__( 'page.sections.dashboard.favourite_properties' ) }}
                </a>
                <a class="item" data-tab="fourth" onclick="user.property.alert.property.properties();">
                    {{__( 'page.sections.dashboard.alerts' ) }}
                </a>
                <a class="item" data-tab="fifth">
                    {{__( 'page.sections.dashboard.settings' ) }}
                </a>
                <a class="item" onclick="user.auth.logout.__init();">
                    {{__( 'page.sections.dashboard.sign_out' ) }}
                </a>
            </div>
            <div class="twelve wide column">
                <div class="ui grid">
                    <div class="sixteen wide column">

                        <div class="ui bottom attached tab segment active" data-tab="first">
                            <div class="ui grid">
                                <div class="eight wide column">
                                    <div class="ui input fluid disabled" id="profile_update_name">
                                        <input name="name" type="text" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="" />
                                    </div>
                                </div>
                                <div class="eight wide column">
                                    <div class="ui input fluid disabled" id="profile_update_phone">
                                        <input name="number" type="number" placeholder="{{__( 'page.form.placeholders.phone' ) }}" value="" />
                                    </div>
                                </div>
                                <div class="eight wide column">
                                    <div class="ui input fluid disabled">
                                        <input name="email" type="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="" />
                                    </div>
                                </div>
                                <div class="eight wide column">
                                    <div class="ui input fluid disabled" id="profile_update_nationality">
                                        <input name="nationality" type="text" placeholder="{{__( 'page.form.placeholders.nationality' ) }}" value="" />
                                    </div>
                                </div>
                                <div class="sixteen wide column">
                                    <button onclick="user.dashboard.profile.update(this);" id="profile_update_btn" type="button" class="ui button disabled" name="button">{{__( 'page.form.button.update' ) }}</button>
                                    <button onclick="user.dashboard.profile.edit(this);" id="profile_edit_btn" type="button" class="ui button" name="button">{{__( 'page.form.button.edit' ) }}</button>
                                    <button onclick="user.dashboard.profile.cancel(this);" id="profile_cancel_btn" type="button" class="ui button hide" name="button">{{__( 'page.form.button.cancel' ) }}</button>
                                </div>
                            </div>
                        </div>

                        <div class="ui bottom attached tab segment" data-tab="second">
                            <div class="ui grid" id="favourite_searches">
                                @include( 'components.cards.favourite_search' )
                            </div>
                        </div>

                        <div class="ui bottom attached tab segment" data-tab="third">
                            <div class="ui grid">
                                <div class="sixteen wide column">
                                    <div class="ui one column grid" id="favourite_properties">
                                        @include( 'components.cards.favourite_property' )
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ui bottom attached tab segment" data-tab="fourth">
                            <div class="ui grid" id="user_alerts">
                                @include( 'components.cards.user_alerts' )
                            </div>
                        </div>

                        <div class="ui bottom attached tab segment" data-tab="fifth">
                            <div class="ui grid">
                                <div class="fourteen wide column">
                                    <p>{{__( 'page.sections.dashboard.subscribe' ) }}</p>
                                </div>
                                <div class="two wide column">
                                    <div class="ui toggle checkbox">
                                      <input type="checkbox" name="public">
                                      <label></label>
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid">
                                <div class="sixteen wide column">
                                    <div>{{__( 'page.sections.dashboard.change_password' ) }}</div>
                                </div>
                                <div class="six wide column">
                                    <div class="ui input fluid disabled" id="change_password_old_password">
                                        <input type="password" placeholder="{{__( 'page.form.placeholders.password_current' ) }}" value="" />
                                    </div>
                                </div>
                                <div class="five wide column">
                                    <div class="ui input fluid disabled" id="change_password_password">
                                        <input type="password" placeholder="{{__( 'page.form.placeholders.password_new' ) }}" value="" />
                                    </div>
                                </div>
                                <div class="five wide column">
                                    <div class="ui input fluid disabled" id="change_password_password_confirmation">
                                        <input type="password" placeholder="{{__( 'page.form.placeholders.password_confirm' ) }}" value="" />
                                    </div>
                                </div>
                                <p id="change_password_error" class="error"></p>
                                <div class="sixteen wide column">
                                    <button onclick="user.dashboard.password.update(this);" id="change_password_update_btn" type="button" class="ui button disabled" name="button">{{__( 'page.form.button.change_password' ) }}</button>
                                    <button onclick="user.dashboard.password.edit(this);" id="change_password_edit_btn" type="button" class="ui button" name="button">{{__( 'page.form.button.edit' ) }}</button>
                                    <button onclick="user.dashboard.password.cancel(this);" id="change_password_cancel_btn" type="button" class="ui button hide" name="button">{{__( 'page.form.button.cancel' ) }}</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="toast_dashboard" style="display: none;"></div>
</div>
