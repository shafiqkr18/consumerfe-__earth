@php
    $internal_links_header      =   isset( $internal_links[ 'header' ] ) ? $internal_links[ 'header' ] : [];
@endphp
<div class="ui grid">
    @if( !empty( $internal_links_header ) )
    <div class="sixteen wide column" id="read_more_less">
        <ul class="internal_links" id="property_type_links">
            @foreach($internal_links_header as $internal_link_header)
                @php
                    $name    =   $internal_link_header['name'];
                    $title   =   $internal_link_header['title'];
                    $link    =   $internal_link_header['link'];
                @endphp
                <li>
                    <a href="{{ $link }}" title="{{$title}}">
                        {{ str_limit($name, $limit = 21, $end = '...') }}
                    </a>
                </li>
            @endforeach
        </ul>
        @if( count( $internal_links_header ) > 8 )
        <div>
            <a href="javascript:void(0);" onclick="helpers.read_more_less('read_more', '', 'read_more_less');" class="read_more">{{ __( 'page.global.more' ) }}</a>
        </div>
        @endif
    </div>
    @endif
</div>
