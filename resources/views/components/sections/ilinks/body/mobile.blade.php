@php
    $internal_links_nearby        =   isset( $internal_links[ 'nearby' ] ) ? $internal_links[ 'nearby' ] : [];
@endphp
<div class="ui container" id="internal_links_accordion">
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui fluid accordion">
                @include( 'components.sections.ilinks.bedroom.mobile' )
                @if( !empty( $internal_links_nearby ) )
                    @php
                        $nearby_title       =   $internal_links_nearby['title'];
                        $size               =   isset( $internal_links_nearby['content'] ) ? count($internal_links_nearby['content']) >= 10 ? 10 :  count($internal_links_nearby['content']) : 0;
                        $nearby_contents    =   isset( $internal_links_nearby['content'] ) ? array_random($internal_links_nearby['content'], $size) : [];
                    @endphp
                    @if( !empty( $nearby_contents ) )
                        <div class="title"> <i class="dropdown icon"></i> <span>{{$nearby_title}}</span></div>
                        <div class="ui list content">
                            @foreach( $nearby_contents as $content )
                                <div class="item">
                                    <a href="{{ $content[ 'link' ] }}" title="{{ $content[ 'title' ] }}">
                                        {{ str_limit($content['name'], $limit = 43, $end = '...') }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
                @include( 'components.sections.ilinks.popular.mobile' )
                <!-- Popular Links -->
                <div class="title"><i class="dropdown icon"></i> <span>{{__( 'page.footer.trending_searches' )}}</span></div>
                <div class="ui list content">
                    @include( 'components.sections.popular.searches' )
                </div>
                <div class="title"><i class="dropdown icon"></i> <span>{{__( 'page.footer.popular_areas' )}} </span></div>
                <div class="ui list content">
                    @include( 'components.sections.popular.areas' )
                </div>
                <div class="title"><i class="dropdown icon"></i> <span>{{__( 'page.footer.trending_properties' )}} </span></div>
                <div class="ui list content">
                    @include( 'components.sections.popular.properties' )
                </div>
            </div>
        </div>
    </div>
</div>
