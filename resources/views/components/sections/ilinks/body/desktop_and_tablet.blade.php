@php
    $internal_links_nearby    = isset( $internal_links[ 'nearby' ] ) ? $internal_links[ 'nearby' ] : [];
    $internal_links_alternate = isset( $internal_links[ 'alternate' ] ) ? $internal_links[ 'alternate' ] : [];
@endphp

<div>
@if( !empty( $internal_links_nearby ) )
    @php
        $nearby_title    = isset( $internal_links_nearby['title'] ) ? $internal_links_nearby['title'] : 'Near by areas';
        $size            = isset( $internal_links_nearby['content'] ) ? count($internal_links_nearby['content']) >= 10 ? 10 :  count($internal_links_nearby['content']) : 0;
        $nearby_contents = isset( $internal_links_nearby['content'] ) ? array_random($internal_links_nearby['content'], $size) : [];
    @endphp
    @if( !empty( $nearby_contents ) )
        <div>{{ $nearby_title }}</div>
        <ul>
            @foreach($nearby_contents as $content)
                <li>
                    <a href="{{ $content['link'] }}" title="{{$content['title']}}">
                        {{ str_limit($content['name'], $limit = 43, $end = '...') }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
@endif

@if( !empty( $internal_links_alternate ) )
    @php
        $alternate_title       =   isset( $internal_links_alternate['title'] ) ? $internal_links_alternate['title'] : '';
        $alternate_contents    =   isset( $internal_links_alternate['name'] ) ? $internal_links_alternate['name'] : [];
    @endphp
    @if( !empty( $alternate_contents ) )
        <span>{!! $alternate_contents !!}</span>
    @endif
@endif
</div>
