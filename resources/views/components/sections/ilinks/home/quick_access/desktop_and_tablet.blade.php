@php
$quick_access_rent =  array_values(array_get($popular_links, 'quick_access.rent'));
$quick_access_buy =  array_values(array_get($popular_links, 'quick_access.buy'));
@endphp
<div class="eleven wide column">
      <div class="sixteen wide column">
            <div id="rentbuy_area" class="ui secondary pointing menu">
                  <h2 class="active item" data-tab="rentProperty">{{__('page.global.rent_property')}}</h2>
                  <h2 class="item" data-tab="buyProperty">{{__( 'page.global.buy_property' )}}</h2>
            </div>
      </div>

      <div class="ui grid container active tab segment" data-tab="rentProperty">
            @foreach($quick_access_rent as $rent_quick)
            @if($loop->index !== 0 && !empty(array_get($rent_quick,'areas',[])))
            <div class="ui five wide column fade reveal">
                  <div class="visible content">
                        <img src="{{cdn_asset('assets/img/home/img'.$loop->index.'.jpg')}}" class="ui small image">
                        <div class="info">
                              <h5>{{array_get($rent_quick,'city')}}</h5>
                              <h5>{{array_get($rent_quick,'property_type')}}</h5>
                              <span>{{__('page.landing.views_neighborhood')}} <i class="icon angle right"></i></span>
                        </div>
                  </div>
                  <div class="hidden content">
                        <ul>
                              @foreach(array_get($rent_quick,'areas',[]) as $area)
                              @if($loop->index < 5)
                                    <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                              @endif
                              @endforeach
                        </ul>
                  </div>
            </div>
            @endif
            @endforeach
            <div class="ui five wide column fade reveal">
                  <div class="visible content">
                        <img src="{{env('ASSET_URL').'assets/img/home/img6.jpg'}}" class="ui small image">
                        <div class="info">
                              <h5>{{array_get($quick_access_rent,'0.city')}}</h5>
                              <h5>{{array_get($quick_access_rent,'0.property_type')}}</h5>
                              <span>{{__('page.landing.views_neighborhood')}} <i class="icon angle right"></i></span>
                        </div>
                  </div>
                  <div class="hidden content">
                        <ul>
                              @foreach(array_get($quick_access_rent,'0.areas') as $area)
                              <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                              @endforeach
                        </ul>
                  </div>

            </div>


            <div id="listing_leaderboard" align="left" style="padding: 0px;">

            <!-- Home Page Leaderboard [async] -->
            <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
            <script type="text/javascript">
            var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
            var abkw = window.abkw || '';
            var plc444889 = window.plc444889 || 0;
            document.write('<'+'div id="placement_444889_'+plc444889+'"></'+'div>');
            AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 444889, [728,90], 'placement_444889_'+opt.place, opt); }, opt: { place: plc444889++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
            </script>
            </div>



      </div>

      <div class="ui grid container tab segment" data-tab="buyProperty">
            @foreach($quick_access_buy as $buy_quick)
            @if($loop->index !== 0 && !empty(array_get($buy_quick,'areas',[])))
            <div class="ui five wide column fade reveal">
                  <div class="visible content">
                        <img src="{{env('ASSET_URL').'assets/img/home/img'.$loop->index.'.jpg'}}" class="ui small image">
                        <div class="info">
                              <h5>{{array_get($buy_quick,'city')}}</h5>
                              <h5>{{array_get($buy_quick,'property_type')}}</h5>
                              <span>{{__('page.landing.views_neighborhood')}}<i class="icon angle right"></i></span>
                        </div>
                  </div>
                  <div class="hidden content">
                        <ul>
                              @foreach(array_get($buy_quick,'areas',[]) as $area)
                                    @if($loop->index < 5)
                                          <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                                    @endif
                              @endforeach
                        </ul>
                  </div>
            </div>
            @endif
            @endforeach
            <div class="ui five wide column fade reveal">
                  <div class="visible content">
                        <img src="{{env('ASSET_URL').'assets/img/home/img6.jpg'}}" class="ui small image">
                        <div class="info">
                              <h5>{{array_get($quick_access_buy,'0.city')}}</h5>
                              <h5>{{array_get($quick_access_buy,'0.property_type')}}</h5>
                              <span>{{__('page.landing.views_neighborhood')}}<i class="icon angle right"></i></span>
                        </div>
                  </div>
                  <div class="hidden content">
                        <ul>
                              @foreach(array_get($quick_access_buy,'0.areas') as $area)
                              <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                              @endforeach
                        </ul>
                  </div>
  </div>
            <div id="listing_leaderboard" align="left" style="padding: 0px;">

            <!-- Home Page Leaderboard [async] -->
            <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
            <script type="text/javascript">
            var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
            var abkw = window.abkw || '';
            var plc444889 = window.plc444889 || 0;
            document.write('<'+'div id="placement_444889_'+plc444889+'"></'+'div>');
            AdButler.ads.push({handler: function(opt){ AdButler.register(173210, 444889, [728,90], 'placement_444889_'+opt.place, opt); }, opt: { place: plc444889++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
            </script>
            </div>



        </div>

</div>
