@php
$quick_access_rent =  array_values(array_get($popular_links, 'quick_access.rent'));
$quick_access_buy =  array_values(array_get($popular_links, 'quick_access.buy'));
@endphp
<div class="sixteen wide column">
    <div id="most_popular_area">
        <div class="ui secondary pointing menu buyrent_popular">
            <a class="active item" data-tab="buyProperty">{{__( 'page.menu_breadcrumbs.buy' )}}<br/><span>{{__('page.footer.popular_areas')}}</span></a>
            <a class="item" data-tab="rentProperty">{{__('page.menu_breadcrumbs.rent')}}<br/><span>{{__('page.footer.popular_areas')}}</span></a>
        </div>
        <div class="ui grid container tab segment popular active" data-tab="buyProperty">
            <div class="ui two column grid">
                <div class="row">
                    @foreach($quick_access_buy as $buy_quick)
                        @if($loop->index !== 0 && !empty(array_get($buy_quick,'areas',[])))
                            @if(array_get($buy_quick,'city') == 'Dubai' || array_get($buy_quick,'city') == 'Abu Dhabi')
                            @if (array_get($buy_quick,'property_type') == 'Apartments')
                            <div class="column">
                                <h4>{{array_get($buy_quick,'city').' '.array_get($buy_quick,'property_type')}}</h4>
                                <div>
                                <ul id="{{'read_more_less_desc'.$loop->index}}" class="@if (count(array_get($buy_quick,'areas',[])) <= 5) auto-height @endif">
                                        @foreach(array_get($buy_quick,'areas',[]) as $area)
                                            <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                                        @endforeach
                                    </ul>
                                    @if (count(array_get($buy_quick,'areas',[])) > 5)
                                        <span onclick="helpers.view_more_less('{{'read_more_details'.$loop->index}}', '6.6rem', '{{'read_more_less_desc'.$loop->index}}', '{{'angle'.$loop->index}}', 'down', 'up', false, 'landing.view_all', 'landing.view_less');" class="{{'read_more_details'.$loop->index}}">{{__('page.landing.view_all')}}</span>
                                        <i id="{{'angle'.$loop->index}}" class="angle down icon"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @endif
                        @endif
                    @endforeach
                    <div class="column">
                        <h4>{{array_get($quick_access_buy,'0.city').' '.array_get($quick_access_buy,'0.property_type')}}</h4>
                        <div>
                            <ul id="read_more_less_desc_buy" class="@if (count(array_get($quick_access_buy,'0.areas',[])) <= 5) auto-height @endif">
                                @foreach(array_get($quick_access_buy,'0.areas',[]) as $area)
                                    <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                                @endforeach
                            </ul>
                            @if (count(array_get($quick_access_buy,'0.areas',[])) > 5)
                                <span onclick="helpers.view_more_less('read_more_details_buy', '7rem', 'read_more_less_desc_buy', 'angle_buy', 'down', 'up', false, 'landing.view_all', 'landing.view_less');" class="read_more_details_buy">{{__('page.landing.view_all')}}</span>
                                <i id="angle_buy" class="angle down icon"></i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui grid container tab segment popular" data-tab="rentProperty">
            <div class="ui two column grid">
                <div class="row">
                    @foreach($quick_access_rent as $rent_quick)
                        @if($loop->index !== 0 && !empty(array_get($rent_quick,'areas',[])))
                            @if(array_get($rent_quick,'city') == 'Dubai' || array_get($rent_quick,'city') == 'Abu Dhabi')
                            @if (array_get($rent_quick,'property_type') == 'Apartments')
                            <div class="column">
                                <h4>{{array_get($rent_quick,'city').' '.array_get($rent_quick,'property_type')}}</h4>
                                <div>
                                    <ul id="{{'read_more_less_desc_rent'.$loop->index}}" class="@if (count(array_get($rent_quick,'areas',[])) <= 5) auto-height @endif">
                                        @foreach(array_get($rent_quick,'areas',[]) as $area)
                                            <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                                        @endforeach
                                    </ul>
                                    @if (count(array_get($rent_quick,'areas',[])) > 5)
                                    <span onclick="helpers.view_more_less('{{'read_more_rent_details'.$loop->index}}', '7rem', '{{'read_more_less_desc_rent'.$loop->index}}', '{{'angle_rent'.$loop->index}}', 'down', 'up', false, 'landing.view_all', 'landing.view_less');" class="{{'read_more_rent_details'.$loop->index}}">{{__('page.landing.view_all')}}</span>
                                    <i id="{{'angle_rent'.$loop->index}}" class="angle down icon"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @endif
                        @endif
                    @endforeach
                    <div class="column">
                        <h4>{{array_get($quick_access_rent,'0.city').' '.array_get($quick_access_rent,'0.property_type')}}</h4>
                        <div>
                            <ul id="read_more_less_desc_rent" class="@if (count(array_get($quick_access_rent,'0.areas',[])) <= 5) auto-height @endif">
                                @foreach(array_get($quick_access_rent,'0.areas',[]) as $area)
                                    <li><a href="{{array_get($area,'link')}}">{{array_get($area,'label')}}</a></li>
                                @endforeach
                            </ul>
                            @if (count(array_get($rent_quick,'0.areas',[])) > 5)
                                <span onclick="helpers.view_more_less('read_more_details_rent', '7rem', 'read_more_less_desc_rent', 'angle_rent', 'down', 'up', false, 'landing.view_all', 'landing.view_less');" class="read_more_details_rent">{{__('page.landing.view_all')}}</span>
                                <i id="angle_rent" class="angle down icon"></i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>