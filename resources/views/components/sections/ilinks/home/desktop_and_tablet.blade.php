@php
$menu_links     =   array_get($popular_links, 'menu');
$content_links  =   array_get($popular_links, 'content');
@endphp
<div class="ui grid container" id="most_popular_wrapper">
    <div class="eight wide column">
        <div class="ui container">
            <div class="sixteen wide column">
                <h2>{{  __( 'page.landing.popular_property' ) }}</h2>
                <h2>{{  __( 'page.landing.searches' ). ' '. __( 'page.global.for' ) }}</h2>
                <h2>{{  __( 'page.guideme.sale' ) }}</h2>
            </div>
        </div>
        <div id="popular-buy" class="ui secondary pointing menu">
            @php $i = 0; @endphp
            @foreach($menu_links as $menu_key => $menu_value)
            <h3 class="item {{ $i == 0 ? 'active' : '' }}" title="{{ array_get($menu_value["buy"], 'title', '') }}" data-tab="{{ $menu_key }}">
                {{ $menu_key }}
            </h3>
            @php $i++; @endphp
            @endforeach
        </div>

        @php $j = 0; @endphp
        @foreach($content_links as $content_key => $content_value)
        <div class="ui grid container tab segment popular {{ $j == 0 ? 'active' : '' }}" data-tab="{{ $content_key }}">
            <h4>{{ $content_key.' '.__( 'page.global.for' ).' '.__( 'page.guideme.sale' ) }}</h4>
            @foreach($content_value["buy"] as $k => $residential_commercial)
            <ul>
                @foreach($residential_commercial as $key => $value)
                @if(!empty($value))
                <li>
                    <a href="{{ array_get($value, 'link', '') }}" title="{{ array_get($value, 'title', '') }}">
                        {{ str_limit(array_get($value, 'label', ''),40) }}
                        ({{ array_get($value, 'count', '') }})
                    </a>
                </li>
                @endif
                @endforeach
            </ul>
            @endforeach
        </div>
        @php $j++; @endphp
        @endforeach
    </div>

    <div class="eight wide column">
        <div class="ui container">
            <div class="sixteen wide column">
                <h2>{{  __( 'page.landing.popular_property' ) }}</h2>
                <h2>{{  __( 'page.landing.searches' ). ' '. __( 'page.global.for' ) }}</h2>
                <h2>{{  __( 'page.guideme.rent' ) }}</h2>
            </div>
        </div>

        <div id="popular-rent" class="ui secondary pointing menu">
            @php $i = 0; @endphp
            @foreach($menu_links as $menu_key => $menu_value)
            <h3 class="item {{ $i == 0 ? 'active' : '' }}" title="{{ array_get($menu_value["rent"], 'title', '') }}" data-tab="{{ $menu_key }}">
                {{ $menu_key }}
            </h3>
            @php $i++; @endphp
            @endforeach
        </div>

        @php $j = 0; @endphp

        @foreach($content_links as $content_key => $content_value)
        <div class="ui grid container tab segment popular {{ $j == 0 ? 'active' : '' }}" data-tab="{{ $content_key }}">
            <h4>{{ $content_key.' '.__( 'page.global.for' ).' '.__( 'page.guideme.rent' ) }}</h4>
            @foreach($content_value["rent"] as $k => $residential_commercial)
            <ul>
                @foreach($residential_commercial as $key => $value)
                @if(!empty($value))
                <li>
                    <a href="{{ array_get($value, 'link', '') }}" title="{{ array_get($value, 'title', '') }}">
                        {{ str_limit(array_get($value, 'label', ''),40) }}
                        ({{ array_get($value, 'count', '') }})
                    </a>
                </li>
                @endif
                @endforeach
            </ul>
            @endforeach
        </div>
        @php $j++; @endphp
        @endforeach
    </div>
</div>
