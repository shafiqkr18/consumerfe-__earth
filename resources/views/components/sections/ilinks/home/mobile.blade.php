@php
$menu_links     =   array_get($popular_links, 'menu');
$content_links  =   array_get($popular_links, 'content');
@endphp

<div id="most_popular_wrapper">
    <div class="sixteen wide column"> {{  __( 'page.landing.popular_property' ).' '. __( 'page.landing.searches' ) }} </div>
    {{-- <div class="sixteen wide column"> {{ __( 'page.global.most_popular' ) }} </div> --}}
    <div class="sixeen wide column">
        <div class="ui secondary pointing menu">
            @php $i = 0; @endphp
            @foreach($menu_links as $menu_key => $menu_value)
            <div class="item {{ $i == 0 ? 'active' : '' }}" data-tab="{{ $menu_key }}">
                {{ $menu_key }}
            </div>
            @php $i++; @endphp
            @endforeach
        </div>


        @php $j = 0; @endphp
        @foreach($content_links as $content_key => $content_value)
        <div class="ui grid container tab segment area {{ $j == 0 ? 'active' : '' }}" data-tab="{{ $content_key }}">
            <div>
                <h6>{{ $content_key.' '.__( 'page.global.for' ).' '.__( 'page.guideme.sale' ) }}</h6>
                <ul id="{{'read_more_less_sale'.$loop->index}}" class="">
                    @foreach($content_value["buy"] as $k => $residential_commercial)
                    @foreach($residential_commercial as $key => $value)
                    @if($loop->index < 10)
                    <li>
                        <a href="{{ array_get($value, 'link', '') }}" title="{{ array_get($value, 'title', '') }}">
                            {{ str_limit(array_get($value, 'label', ''),40) }}
                            ({{ array_get($value, 'count', '') }})
                        </a>
                    </li>
                    @endif
                    @endforeach
                    @endforeach
                </ul>
                <div>
                    <span onclick="helpers.view_more_less('{{'read_more_details_sale'.$loop->index}}', '8rem', '{{'read_more_less_sale'.$loop->index}}', '{{'angle_sale'.$loop->index}}', 'down', 'up', false, 'landing.view_all', 'landing.view_less')" class="{{'read_more_details_sale'.$loop->index}}">{{__('page.landing.view_all')}}</span>
                    <i id="{{'angle_sale'.$loop->index}}" class="angle down icon"></i>
                </div>
            </div>
            <div>
                <h6>{{ $content_key.' '.__( 'page.global.for' ).' '.__( 'page.guideme.rent' ) }}</h6>
                <ul id="{{'read_more_less_rent'.$loop->index}}" class="">
                    @foreach($content_value["rent"] as $k => $residential_commercial)
                    @foreach($residential_commercial as $key => $value)
                    @if($loop->index < 10)
                    <li>
                        <a href="{{ array_get($value, 'link', '') }}" title="{{ array_get($value, 'title', '') }}">
                            {{ str_limit(array_get($value, 'label', ''),40) }}
                            ({{ array_get($value, 'count', '') }})
                        </a>
                    </li>
                    @endif
                    @endforeach
                    @endforeach
                </ul>
                <div>
                    <span onclick="helpers.view_more_less('{{'read_more_details_rent'.$loop->index}}', '8rem', '{{'read_more_less_rent'.$loop->index}}', '{{'angle_rent'.$loop->index}}', 'down', 'up', false, 'landing.view_all', 'landing.view_less')" class="{{'read_more_details_rent'.$loop->index}}">{{__('page.landing.view_all')}}</span>
                    <i id="{{'angle_rent'.$loop->index}}" class="angle down icon"></i>
                </div>
            </div>
        </div>
        @php $j++; @endphp
        @endforeach

    </div>
</div>
