@if( !empty( $seo_links ) )
    <div class="active title"> <i class="dropdown icon"></i> <span>{{ __( 'page.global.popular_searches' ) }}</span></div>
    <div class="active ui list content">
        @foreach($seo_links['links'] as $key => $link)
            @php
                if(\App::getLocale() == 'en') {
                    $title  =   !empty(array_get($seo_links,'titles.'.$key,'')) ? array_get($seo_links,'titles.'.$key,'') : '';
                    $name   =   !empty(array_get($seo_links,'names.'.$key,'')) ? array_get($seo_links,'names.'.$key,'') : '';
                }
                else{
                    $title  =   !empty(array_get($seo_links,'titles_'.\App::getLocale().'.'.$key,'')) ? array_get($seo_links,'titles_'.\App::getLocale().'.'.$key,'') : '';
                    $name   =   !empty(array_get($seo_links,'names_'.\App::getLocale().'.'.$key,'')) ? array_get($seo_links,'names_'.\App::getLocale().'.'.$key,'') : '';
                }
                $link   =   env('APP_URL').\App::getLocale().$link;
                $status =   array_get($seo_links,'links_status.'.$key,0) ? array_get($seo_links,'links_status.'.$key,'') : 0;
            @endphp
            @if($status == 1)
            <div class="item">
                <a href="{{ $link }}" title="{{ $title }}">
                    {{ str_limit($name, $limit = 40, $end = '...') }}
                </a>
            </div>
            @endif
        @endforeach
    </div>
@endif
