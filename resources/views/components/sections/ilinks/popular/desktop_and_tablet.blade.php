<div class="ui container" id="popular_searches">
    <div class="ui grid">
        <div class="sixteen wide column">
        @if( !empty( $seo_links ) )
            <h4>{{ __( 'page.global.popular_searches' ) }}</h4>
            <ul>
                @foreach($seo_links['links'] as $key => $link)
                    @php
                        if(\App::getLocale() == 'en') {
                            $title  =   !empty(array_get($seo_links,'titles.'.$key,'')) ? array_get($seo_links,'titles.'.$key,'') : '';
                            $name   =   !empty(array_get($seo_links,'names.'.$key,'')) ? array_get($seo_links,'names.'.$key,'') : '';
                        }
                        else{
                            $title  =   !empty(array_get($seo_links,'titles_'.\App::getLocale().'.'.$key,'')) ? array_get($seo_links,'titles_'.\App::getLocale().'.'.$key,'') : '';
                            $name   =   !empty(array_get($seo_links,'names_'.\App::getLocale().'.'.$key,'')) ? array_get($seo_links,'names_'.\App::getLocale().'.'.$key,'') : '';
                        }
                        $link   =   env('APP_URL').\App::getLocale().$link;
                        $status =   array_get($seo_links,'links_status.'.$key,0) ? array_get($seo_links,'links_status.'.$key,'') : 0;
                    @endphp
                    @if($status == 1)
                    <li>
                        <a href="{{ $link }}" title="{{ $title }}">
                            {{ str_limit($name, $limit = 40, $end = '...') }}
                        </a>
                    </li>
                    @endif
                @endforeach
            </ul>
        @endif
        </div>
    </div>
</div>
