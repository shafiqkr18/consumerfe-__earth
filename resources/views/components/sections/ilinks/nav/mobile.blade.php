@php
    $internal_links_nav             =   array_get($internal_links, 'nav', []);
    $internal_links_nav_header      =   array_get($internal_links_nav, 'head', 'Properties');
    $internal_links_nav_buy         =   array_get($internal_links_nav, 'sale', []);
    $internal_links_nav_rent        =   array_get($internal_links_nav, 'rent', []);
    $query_residential_commercial   =   strtolower(array_get($query,'residential_commercial.0', 'Residential'));
    $residential_lang               =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom('Residential'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom('Residential')) : ucfirst('Residential');
    $commercial_lang                =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom('Commercial'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom('Commercial')) : ucfirst('Commercial');
    $chunk                          =   4;
@endphp

<!-- Buy -->
@if(!empty($internal_links_nav_buy))
    @php
        $commercial_buy         =   array_get($internal_links_nav_buy, 'commercial', []);
        $residential_buy        =   array_get($internal_links_nav_buy, 'residential', []);
        $is_buy_hide_buttons    =   (empty($residential_buy) || empty($commercial_buy)) ? true : false;
    @endphp
    <div class="eight wide column center aligned">
        <div class="ui pointing dropdown top left item">
            <i class="home icon"></i>
            {{ __( 'page.menu_breadcrumbs.buy' ) }}
            <i class="dropdown icon"></i>
            <div class="menu right aligned">
                <div class="header">
                    <div class="ui buttons">
                        <button class="ui button {{ ($query_residential_commercial == 'residential' ? 'purple' : '') }} {{ $is_buy_hide_buttons ? 'hide' : '' }}" data-value="residential" onclick="helpers.navToggle(this);">{{ $residential_lang }}</button>
                        <button class="ui button {{ ($query_residential_commercial == 'commercial' ? 'purple' : '') }} {{ $is_buy_hide_buttons ? 'hide' : '' }}" data-value="commercial" onclick="helpers.navToggle(this);">{{ $commercial_lang }}</button>
                    </div>
                </div>
                <div class="header"><div class="text">{{ $internal_links_nav_header }}</div></div>
                @foreach($internal_links_nav_buy as $key => $residential_commercial)
                    @php
                        $count_buy  =   count($residential_commercial);
                        $show_hide  =   $query_residential_commercial != $key ? 'hide' : 'show';
                        if($is_buy_hide_buttons){
                            if($key == 'residential'){
                                $show_hide  =   !empty($residential_buy) ? 'show' : 'hide';
                            }
                            elseif($key == 'commercial'){
                                $show_hide  =   !empty($commercial_buy) ? 'show' : 'hide';
                            }
                        }
                    @endphp
                    <div class="row {{ $key }} {{ $count_buy > 10 ? 'nav-scrollbar' : '' }} {{ $show_hide }}">
                        @foreach($residential_commercial as $types)
                            <div class="item">
                                <a href="{{ array_get($types,'link', '') }}" title="{{ array_get($types,'title', '') }}">
                                {{ array_get($types,'name', '') }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@else
    <div class="eight wide column center aligned">
        <div class="ui pointing dropdown top left item">
            <i class="home icon"></i>
            <a onclick="helpers.redirect('Sale');" href="javascript:void(0)">{{ __( 'page.menu_breadcrumbs.buy' ) }}</a>
        </div>
    </div>
@endif

<!-- Rent -->
@if(!empty($internal_links_nav_rent))
    @php
        $commercial_rent        =   array_get($internal_links_nav_rent, 'commercial', []);
        $residential_rent       =   array_get($internal_links_nav_rent, 'residential', []);
        $is_rent_hide_buttons   =   (empty($residential_rent) || empty($commercial_rent)) ? true : false;
    @endphp
    <div class="eight wide column center aligned">
        <div class="ui pointing dropdown top right item">
            <i class="home icon"></i>
            {{ __( 'page.menu_breadcrumbs.rent' ) }}
            <i class="dropdown icon"></i>
            <div class="menu left aligned">
                <div class="header">
                    <div class="ui buttons">
                        <button class="ui button left attached {{ ($query_residential_commercial == 'residential' ? 'purple' : '') }} {{ $is_rent_hide_buttons ? 'hide' : '' }}" data-value="residential" onclick="helpers.navToggle(this);">{{ $residential_lang }}</button>
                        <button class="ui button right attached {{ ($query_residential_commercial == 'commercial' ? 'purple' : '') }} {{ $is_rent_hide_buttons ? 'hide' : '' }}" data-value="commercial" onclick="helpers.navToggle(this);">{{ $commercial_lang }}</button>
                    </div>
                </div>
                <div class="header"><div class="text">{{ $internal_links_nav_header }}</div></div>
                @foreach($internal_links_nav_rent as $key => $residential_commercial)
                    @php
                        $count_rent =   count($residential_commercial);
                        $show_hide  =   $query_residential_commercial != $key ? 'hide' : 'show';
                        if($is_rent_hide_buttons){
                            if($key == 'residential'){
                                $show_hide  =   !empty($residential_rent) ? 'show' : 'hide';
                            }
                            elseif($key == 'commercial'){
                                $show_hide  =   !empty($commercial_rent) ? 'show' : 'hide';
                            }
                        }
                    @endphp
                    <div class="row {{ $key }} {{ $count_rent > 10 ? 'nav-scrollbar' : '' }} {{ $show_hide }}">
                        @foreach($residential_commercial as $types)
                            <div class="item">
                                <a href="{{ array_get($types,'link', '') }}" title="{{ array_get($types,'title', '') }}">
                                {{ array_get($types,'name', '') }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@else
    <div class="eight wide column center aligned">
        <div class="ui pointing dropdown top left item">
            <i class="home icon"></i>
            <a onclick="helpers.redirect('Rent');" href="javascript:void(0)">{{ __( 'page.menu_breadcrumbs.rent' ) }}</a>
        </div>
    </div>
@endif
