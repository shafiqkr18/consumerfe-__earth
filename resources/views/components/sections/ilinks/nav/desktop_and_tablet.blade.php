@php
    $internal_links_nav             =   array_get($internal_links, 'nav', []);
    $internal_links_nav_header      =   array_get($internal_links_nav, 'head', 'Properties');
    $internal_links_nav_buy         =   array_get($internal_links_nav, 'sale', []);
    $internal_links_nav_rent        =   array_get($internal_links_nav, 'rent', []);
    $query_residential_commercial   =   strtolower(array_get($query,'residential_commercial.0', 'Residential'));
    $residential_lang               =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom('Residential'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom('Residential')) : ucfirst('Residential');
    $commercial_lang                =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom('Commercial'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom('Commercial')) : ucfirst('Commercial');
    $chunk                          =   7;
@endphp

<!-- Buy -->
@if(!empty($internal_links_nav_buy))
    @php
        $commercial_buy         =   array_get($internal_links_nav_buy, 'commercial', []);
        $residential_buy        =   array_get($internal_links_nav_buy, 'residential', []);
        $is_buy_hide_buttons    =   (empty($residential_buy) || empty($commercial_buy)) ? true : false;
    @endphp
    <div class="ui pointing dropdown top left item">
        {{ __( 'page.menu_breadcrumbs.buy' ) }}
        <i class="dropdown icon"></i>
        <div class="menu">
            <div class="header">{{ $internal_links_nav_header }}</div>
                @foreach($internal_links_nav_buy as $key => $residential_commercial)
                    @php
                        $count_buy  =   count($residential_commercial);
                        $show_hide  =   $query_residential_commercial != $key ? 'hide' : 'show';
                        if($is_buy_hide_buttons){
                            if($key == 'residential'){
                                $show_hide  =   !empty($residential_buy) ? 'show' : 'hide';
                            }
                            elseif($key == 'commercial'){
                                $show_hide  =   !empty($commercial_buy) ? 'show' : 'hide';
                            }
                        }
                        $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        $columns = $f->format(round($count_buy / $chunk));
                    @endphp
                <div class="ui {{ $columns }} column relaxed divided grid row {{ $key }} {{ $show_hide }}">
                    @php $count = 1; @endphp
                    @foreach($residential_commercial as $types)
                        @if($count % $chunk == 1)
                        <div class="column"><div class="ui link list">
                        @endif
                            <div class="item">
                                <a href="{{ array_get($types,'link', '') }}" title="{{ array_get($types,'title', '') }}">
                                {{ array_get($types,'name', '') }}
                                </a>
                            </div>
                        @if($count % $chunk == 0)
                        </div></div>
                        @endif
                        @php $count++; @endphp
                    @endforeach
                    @if($count % $chunk != 1) </div></div> @endif
                </div>
                @endforeach
            <div class="footer">
                <div class="ui buttons">
                    <button class="ui button left attached {{ ($query_residential_commercial == 'residential' ? 'purple' : '') }} {{ $is_buy_hide_buttons ? 'hide' : '' }}" data-value="residential" onclick="helpers.navToggle(this);">{{ $residential_lang }}</button>
                    <button class="ui button right attached {{ ($query_residential_commercial == 'commercial' ? 'purple' : '') }} {{ $is_buy_hide_buttons ? 'hide' : '' }}" data-value="commercial" onclick="helpers.navToggle(this);">{{ $commercial_lang }}</button>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="item"><a onclick="helpers.redirect('Sale');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.buy' )}}</a></div>
@endif

<!-- Rent -->
@if(!empty($internal_links_nav_rent))
    @php
        $commercial_rent        =   array_get($internal_links_nav_rent, 'commercial', []);
        $residential_rent       =   array_get($internal_links_nav_rent, 'residential', []);
        $is_rent_hide_buttons   =   (empty($residential_rent) || empty($commercial_rent)) ? true : false;
    @endphp
    <div class="ui pointing dropdown top left item">
        {{ __( 'page.menu_breadcrumbs.rent' ) }}
        <i class="dropdown icon"></i>
        <div class="menu">
            <div class="header">{{ $internal_links_nav_header }}</div>
            @foreach($internal_links_nav_rent as $key => $residential_commercial)
                @php
                    $count_rent =   count($residential_commercial);
                    $show_hide  =   $query_residential_commercial != $key ? 'hide' : 'show';
                    if($is_rent_hide_buttons){
                        if($key == 'residential'){
                            $show_hide  =   !empty($residential_rent) ? 'show' : 'hide';
                        }
                        elseif($key == 'commercial'){
                            $show_hide  =   !empty($commercial_rent) ? 'show' : 'hide';
                        }
                    }
                    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                    $columns = $f->format(round($count_rent / $chunk));
                @endphp
                <div class="ui {{ $columns }} column relaxed divided grid row {{ $key }} {{ $show_hide }}">
                    @php $count = 1; @endphp
                    @foreach($residential_commercial as $types)
                        @if($count % $chunk == 1)
                        <div class="column"><div class="ui link list">
                        @endif
                            <div class="item">
                                <a href="{{ array_get($types,'link', '') }}" title="{{ array_get($types,'title', '') }}">
                                {{ array_get($types,'name', '') }}
                                </a>
                            </div>
                        @if($count % $chunk == 0)
                        </div></div>
                        @endif
                        @php $count++; @endphp
                    @endforeach
                    @if($count % $chunk != 1) </div></div> @endif
                </div>
            @endforeach
            <div class="footer">
                <div class="ui buttons">
                    <button class="ui button left attached {{ ($query_residential_commercial == 'residential' ? 'purple' : '') }} {{ $is_rent_hide_buttons ? 'hide' : '' }}" data-value="residential" onclick="helpers.navToggle(this);">{{ $residential_lang }}</button>
                    <button class="ui button right attached {{ ($query_residential_commercial == 'commercial' ? 'purple' : '') }} {{ $is_rent_hide_buttons ? 'hide' : '' }}" data-value="commercial" onclick="helpers.navToggle(this);">{{ $commercial_lang }}</button>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="item"><a onclick="helpers.redirect('Rent');" href="javascript:void(0)">{{__( 'page.menu_breadcrumbs.rent' )}}</a></div>
@endif
