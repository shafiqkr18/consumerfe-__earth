@php
    $internal_links_bedroom       =   isset( $internal_links[ 'bedroom' ] ) ? $internal_links[ 'bedroom' ] : [];
@endphp

@if( !empty( $internal_links_bedroom ) )
    @php
        $bedroom_title       =   $internal_links_bedroom['title'];
        $bedroom_contents    =   isset( $internal_links_bedroom['content'] ) ? $internal_links_bedroom['content'] : [];
    @endphp
    @if( !empty( $bedroom_contents ) )
        <div class="active title"> <i class="dropdown icon"></i> <span>{{$bedroom_title}}</span></div>
        <div class="active ui list content">
            @foreach( $bedroom_contents as $content )
                <div class="item">
                    <a href="{{ $content[ 'link' ] }}" title="{{ $content[ 'title' ] }}">
                        {{ str_limit($content['name'], $limit = 43, $end = '...') }}
                    </a>
                </div>
            @endforeach
        </div>
    @endif
@endif
