@php
    $internal_links_bedroom        =   isset( $internal_links[ 'bedroom' ] ) ? $internal_links[ 'bedroom' ] : [];
@endphp

<div>
@if( !empty( $internal_links_bedroom ) )
    @php
        $bedroom_title       =   isset( $internal_links_bedroom['title'] ) ? $internal_links_bedroom['title'] : __( 'page.global.types' );
        $bedroom_contents    =   isset( $internal_links_bedroom['content'] ) ? $internal_links_bedroom['content'] : [];
    @endphp
    @if( !empty( $bedroom_contents ) )
        <div>{{ $bedroom_title }}</div>
        <ul>
            @foreach($bedroom_contents as $content)
                <li>
                    <a href="{{ $content['link'] }}" title="{{ $content['title'] }}">
                        {{ str_limit($content['name'], $limit = 40, $end = '...') }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
@endif
</div>
