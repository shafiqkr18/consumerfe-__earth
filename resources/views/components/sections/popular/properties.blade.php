@php
    $trending = [
        'The Views' =>  'Dubai',
        'Arjan' =>  'Dubai',
        'Jumeirah Beach Residences (JBR)' => 'Dubai',
        'Meadows' =>  'Dubai',
        'Dubai Creek Harbour (The Lagoons)' => 'Dubai',
        'Arabian Ranches' =>  'Dubai',
        'Al Reem Island' =>  'Abu Dhabi',
        'Muwaileh' =>  'Sharjah'
    ];
@endphp

@foreach($trending as $area => $city)
    <div class="item">
        @php
            $url_params =   ['lng' => \App::getLocale(), 'rent_buy' => 'buy', 'residential_commercial' => 'residential', 'city' => kebab_case_custom($city,true), 'properties-for-sale-in-' . kebab_case_custom($area,true) . '-area'];
            $rc         =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom('Residential'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom('Residential')) : 'Residential';
            $ar         =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
            $ct         =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
            $title      =   $rc.' '.strtolower(__( 'page.global.properties')).' '.strtolower(__( 'data.rent_buy.for_sale')).' '.__( 'page.global.in').' '.$ar. ', ' . $ct;
        @endphp
        <a href="{{route('search-results-page',$url_params)}}" title="{{ $title }}">
            {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area }}
        </a>
    </div>
@endforeach
