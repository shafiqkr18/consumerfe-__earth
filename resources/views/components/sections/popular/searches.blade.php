@php
    $rent_buy_arr       =   [ 'Rent' => 'Rent', 'Buy' => 'Sale' ];
    $property_type_arr  =   [ 'Villa', 'Apartment' ];
    $city_arr           =   [ 'Dubai', '' ];
@endphp

@foreach($rent_buy_arr as $rent_buy => $rent_sale)
    @foreach($property_type_arr as $property_type)
        @foreach($city_arr as $city)
            <div class="item">
                @php
                    $url_params =   ['lng' => \App::getLocale(), 'rent_buy' => strtolower($rent_buy), 'residential_commercial' => 'residential', kebab_case_custom($property_type).'-for-'.strtolower($rent_sale).( !empty($city) ? '-in-'.kebab_case_custom($city).'-city' : '' ) ];
                    $pt         =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom($property_type), \App::getLocale())) ? __( 'data.type.'.snake_case_custom($property_type)) : $property_type;
                    $rs         =   (\App::getLocale() !== 'en' && \Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale)) : 'for '.ucfirst($rent_sale);
                    $ct         =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
                    $title      =   $pt.' '.strtolower($rs).' '.__( 'page.global.in').' '.( !empty($ct) ? $ct : __( 'page.global.uae') );
                @endphp
                <a href="{{route('search-results-page',$url_params)}}" title="{{ $title }}">
                    {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom($property_type), \App::getLocale()))
                    ? __('page.footer.popular_searches_item', ['type' => __( 'data.type.'.snake_case_custom($property_type)), 'for_rent_buy' => __( 'data.rent_buy.for_'.strtolower($rent_sale)), 'loc' => ( !empty($city) ? ( Lang::has('data.city.'.strtolower($city), \App::getLocale()) ? __('data.city.'.strtolower($city)) : $city ) : __( 'page.global.uae') )] )
                    : $title }}
                </a>
            </div>
        @endforeach
    @endforeach
@endforeach
