@php
    $popular = [    'The Palm Jumeirah' => 'Dubai',
                    'Jumeirah Beach Residences (JBR)' => 'Dubai',
                    'Town Square' => 'Dubai',
                    'Dubai Marina' => 'Dubai',
                    'Dubai South (Dubai World Central)' => 'Dubai',
                    'Downtown Dubai' => 'Dubai',
                    'Al Nahda' => 'Sharjah',
                    'Dubai Silicon Oasis' => 'Dubai' ];
@endphp

@foreach($popular as $area => $city)
    <div class="item">
        @php
            $url_params =   ['lng' => \App::getLocale(), 'rent_buy' => 'buy', 'residential_commercial' => 'residential', 'city' => kebab_case_custom($city,true), 'properties-for-sale-in-' . kebab_case_custom($area,true) . '-area'];
            $rc         =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom('Residential'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom('Residential')) : 'Residential';
            $ar         =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
            $ct         =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
            $stub       =   \Lang::has('data.seo_titles.seo_h1_stub', \App::getLocale()) ? __( 'data.seo_titles.seo_h1_stub') : ":residential_commercial: :property_type: :rent_sale: in :area:";
            $title      =   str_replace( ":featured:", '', str_replace( ":bedroom:", '', str_replace( ":residential_commercial:", $rc, str_replace( ":rent_sale:", __( 'data.rent_buy.for_sale'), str_replace( ":property_type:", __( 'page.global.properties'), str_replace( ":area:", $ar, $stub ) ) ) ) ) );
            $title      =   preg_replace('/\s+/', ' ', trim($title));
        @endphp
        <a href="{{route('search-results-page',$url_params)}}" title="{{ $title }}">
            {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area }}
        </a>
    </div>
@endforeach
