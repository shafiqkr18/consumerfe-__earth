
<div id="property_search_mobile" class="ui grid container">
    <div class="sixteen wide column">
        <div class="ui left icon input">
            <input id="property_suburb_listings" type="text" class="autocomplete fluid" placeholder="{{__('page.form.placeholders.location')}}">
            <input id="property_suburb_listings_hidden" type="hidden" />
            <span onclick="pages.property.listings.search.__init('Sale');">
                <i class="sliders horizontal icon" ></i>
            </span>
        </div>
    </div>
</div>

