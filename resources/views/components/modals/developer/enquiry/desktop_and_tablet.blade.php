<form class="sixteen wide column" id="modal_developer_enquiry" onsubmit="return false;">
    <div class="ui grid">
        <div class="sixteen wide column">
            <span>{{__( 'page.form.general_enquiry.btn' )}}</span>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.full_name' ) }}</label> --}}
                        <input id="modal_developer_enquiry_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*" name="name" type="text">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.email' ) }}</label> --}}
                        <input id="modal_developer_enquiry_email" placeholder="{{__( 'page.form.placeholders.email' ) }}*" name="email" type="email">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.phone' ) }}</label> --}}
                        <input type="tel" id="modal_developer_enquiry_phone" type="number" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>Message</label> --}}
                        <textarea id="modal_developer_enquiry_message" name="message" rows="3" placeholder="{{__( 'page.form.your_messagge' ) }}*"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <span>{{__( 'validation.required_all' )}}*</span>
        </div>
        <div class="sixteen wide column">
            <button class="ui submit button" id="modal_developer_enquiry_data" data-title="{{$seo_h1}}" data-url="{{$request->getRequestUri()}}" onclick="if($('#modal_developer_enquiry').form('is valid')){
                lead.project.request.__init(this);$(this).addClass('track_email_submit');}else{console.log('form not validated!')}">
                <i class="envelope outline icon"></i>{{__( 'page.form.send_email' ) }}
            </button>
        </div>
    </div>
</form>
