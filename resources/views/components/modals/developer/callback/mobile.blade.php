<form class="ui form modal developer_callback" id="modal_developer_callback" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        <div class="ui pointing below label">{{__( 'page.modals.hints.name' ) }}</div>
                        <input id="modal_developer_callback_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" name="name" type="text">
                    </div>
                    <div class="field sixteen wide column">
                        <div class="ui pointing below label">{{__( 'page.modals.hints.phone' ) }}</div>
                        <input type="tel" id="modal_developer_callback_phone" type="number" name="phone">
                    </div>
                </div>
            </div>
            <div><p class="modal_fields_required">{{__( 'page.modals.property.call_back.message' ) }}</p></div>
        </div>
        <div class="sixteen wide column stretched">
            <button class="ui submit button" id="modal_developer_callback_data" data-subsource="@desktop{{'desktop'}}@elsedesktop{{'mobile'}}@enddesktop"
            onclick="
                if($('#modal_developer_callback').form('is valid')){
                    lead.project.callback.__init(this);
                    $(this).addClass('track_callback_submit');
                } else {
                    console.log('form not validated!')
                }">
                {{__( 'page.form.button.request_call_back' ) }}
            </button>
        </div>
    </div>
    <div class="ui error message"></div>
</form>
<script>
    validate.rules.lead.project.callback.__init();
</script>
