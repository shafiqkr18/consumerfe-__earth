<div class="ui form special tiny modal developer_call" id="modal_developer_call">
    <i class="close icon"></i>
    <div class="ui grid container">
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="one wide column">
                    <div class="bar"></div>
                </div>
                <div class="fourteen wide column">
                    <div>
                        <span>T:</span>
                        <div id="modal_developer_call_developer_contact_phone" dir="ltr" class="track_call_submit"
                            onclick="lead.project.call.__init(this);"></div>
                    </div>
                    <div>
                        <span id="modal_developer_call_project_name"></span>
                    </div>
                    <div>
                        <span id="modal_developer_call_contact_name">,</span>
                    </div>
                    {{-- <p>{{__( 'page.property.details.ref_no_title' ) }}: <span id="modal_developer_call_ref_no"></span>
                    </p> --}}
                </div>
            </div>
        </div>
    </div>
</div>


