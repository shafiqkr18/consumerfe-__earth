<form class="ui form modal developer_email" id="modal_developer_email" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <span>{{__( 'page.form.more_details_title' )}}</span>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.full_name' ) }}</label> --}}
                        <input id="modal_developer_email_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*" name="name" type="text">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.email' ) }}</label> --}}
                        <input id="modal_developer_email_email" placeholder="{{__( 'page.form.placeholders.email' ) }}*" name="email" type="email">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.phone' ) }}</label> --}}
                        <input type="tel" id="modal_developer_email_phone" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>Message</label> --}}
                        <textarea id="modal_developer_email_textarea" rows="3">{{__( 'page.modals.project.email.message', [ 'reference' => ''] ) }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <span>{{__( 'validation.required_all' )}}*</span>
        </div>
        <div class="sixteen wide column">
            <button class="ui submit button" id="modal_developer_email_data" onclick="if($('#modal_developer_email').form('is valid')){
                lead.project.email.__init(this);$(this).addClass('track_email_submit');}else{console.log('form not validated!')}">
                <i class="envelope outline icon"></i>{{__( 'page.form.send_email' ) }}
            </button>
        </div>
    </div>
</form>
<script>
    validate.rules.lead.project.email.__init();
</script>
