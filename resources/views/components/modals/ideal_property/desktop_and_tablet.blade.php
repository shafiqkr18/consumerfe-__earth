<div class="ui modal ideal_property" id="modal_ideal_property">
    <i class="close icon"></i>
    <form class="ui grid" action="#">
        <div class="sixteen wide column">
            <p>{{__( 'page.form.ideal_property.first_message' ) }} <small>{{__( 'page.form.ideal_property.second_message' ) }}</small></p>
        </div>
        <div class="sixteen wide stretched column">
            <div class="ui input">
                <input name="name" id="ideal_property_name" type="text" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="" required>
            </div>
        </div>
        <div class="sixteen wide stretched column">
            <div class="ui input">
                <input name="email" id="ideal_property_email" type="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="" required>
            </div>
        </div>
        <div class="sixteen wide stretched column">
            <div class="ui input">
                <input name="phone" id="ideal_property_phone" type="text" placeholder="{{__( 'page.form.placeholders.phone' ) }}" value="" required>
            </div>
        </div>
        <div class="sixteen wide stretched column">
            <div class="ui input">
                <input name="budget" id="ideal_property_budget" type="text" placeholder="{{__( 'page.form.placeholders.max_budget' ) }}" value="">
            </div>
        </div>
        <div class="sixteen wide stretched column">
            <select name="type" id="ideal_property_type" class="ui dropdown selection">
                <option value="">{{ __( 'page.global.rent_buy' ) }}</option>
                @foreach( __( 'search.property.rent_buy' ) as $key => $value )
                    @if( !str_contains( $value['name'], 'Short Term Rent') || !str_contains( $value['name'], 'إجار قصير المدى'))
                        <option value="{{ $value['value'] }}" @if($loop->index == 0) selected @endif>{{ $value['name'] }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="sixteen wide stretched column">
            <div class="ui input">
                <input type="text" class="autocomplete" id="ideal_property_location" name="location" placeholder="{{__( 'page.form.placeholders.preferred_locations' )}}" value="">
            </div>
        </div>
        <div class="sixteen wide stretched column">
            <button onclick="other.ideal_property.email(this);" type="button" class="ui button">{{__( 'page.form.button.submit' ) }}</button>
        </div>
    </form>
</div>
