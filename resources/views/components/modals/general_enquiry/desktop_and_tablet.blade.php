<div id="modal_general_enquiry" class="sixteen wide column">
  <div class="ui two column grid general_enquiry_container">
    <div class="column">
      <button class="massive ui button track_email" onclick="other.general_enquiry.slide_context();">
        {{__( 'page.form.general_enquiry.btn' ) }}
      </button>
    </div>
    <div class="column">
      <div class="ui grid">
        <div class="sixteen wide column">
          <p>{{__( 'page.form.general_enquiry.message' ) }}</p>
        </div>
        <div class="sixteen wide stretched column">
          <div class="ui input">
            <input name="name" id="ge_name" type="text" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="">
          </div>
        </div>
        <div class="sixteen wide stretched column">
          <div class="ui input">
            <input name="email" id="ge_email" type="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="">
          </div>
        </div>
        <div class="sixteen wide stretched column">
          <div class="ui input">
            <input name="phone" id="ge_phone" type="tel" value="">
          </div>
        </div>
        <div class="sixteen wide stretched column">
          <div class="ui ge dropdown selection">
            <input type="hidden" name="type" id="ge_type">
            <i class="dropdown icon"></i>
            <div class="default text">{{ __( 'page.form.placeholders.type' ) }}</div>
            <div class="menu">
              @foreach( __( 'search.property.property_type.residential.sale' ) as $key => $value )
              <div class="item" data-value="{{ $value['value'] }}" @if($loop->index == 0) selected @endif>{{ $value['name'] }}</div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="sixteen wide stretched column">
          <div class="ui ge dropdown selection">
            <input type="hidden" name="rent_buy" id="ge_rent_buy">
            <i class="dropdown icon"></i>
            <div class="default text">{{ __( 'page.global.rent_buy' ) }}</div>
            <div class="menu">
              @foreach( __( 'search.property.rent_buy' ) as $key => $value )
              @if( !str_contains( $value['name'], 'Short Term Rent') || !str_contains( $value['name'], 'إجار قصير المدى'))
              <div class="item" data-value="{{ $value['value'] }}" @if($loop->index == 0) selected @endif>{{ $value['name'] }}</div>
              @endif
              @endforeach
            </div>
          </div>
        </div>
        <div class="sixteen wide stretched column">
          <div class="ui input">
            <input type="text" class="autocomplete" id="ge_location" name="location" placeholder="{{__( 'page.form.placeholders.preferred_locations' )}}" value="">
          </div>
        </div>
        <div class="sixteen wide stretched column">
          <button class="ui submit button" id="modal_general_enquiry_data" onclick="if($('#modal_general_enquiry').form('is valid')){lead.preferences.__init(this); $(this).addClass('track_email_submit');
        }else{console.log('form not validated!'); $('#modal_general_enquiry_error').attr('style','display:block');}">{{__( 'page.form.button.submit' ) }}
        </button>
    </div>
    <div class="ui error message" id="modal_general_enquiry_error"></div>
  </div>
</div>
</div>
</div>
