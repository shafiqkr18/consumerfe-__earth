<div class="ui modal mobile_property_search" id="modal_mobile_property_search">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="four wide column ui large secondary menu">
            {{-- <a href="javascript:void(0)" hidden class="state_li" onclick="user.dashboard.trigger.__init();"><i class="icon user outline"></i></a> --}}
            {{-- <a href="javascript:void(0)" class="state_lo" onclick="pages.signin.modal.__init();"><i class="icon user outline"></i></a> --}}
        </div>
        <div class="eight wide column">
            <a class="item">{{__('page.menu_breadcrumbs.search_property')}}</a>
        </div>
        <div class="four wide column"></div>
    </div>
    <div class="ui grid scrolling content">

        <div id="rent_buy_criteria" class="sixteen wide column clearfix">
            <span class="Sale active" href="#">BUY</span>
            <span class="Rent" href="#">RENT</span>
            <span class="Term" href="#">SHORT TERM</span>
        </div>

      <div class="sixteen wide column">
            <input class="autocomplete fluid" placeholder="{{__('page.form.placeholders.location')}}" id="property_suburb" type="text" />
            <input id="property_suburb_hidden" type="hidden" />
            <i class="map marker alternate icon"></i>
            <a href="#" onclick="pages.landing.dropdown.clear_area_field();"><i class="close icon"></i></a>
      </div>

        <div class="sixteen wide column">
            <select class="ui dropdown fluid" id="property_type"></select>
        </div>

        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_bedroom_min"></select>
                </div>
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_bedroom_max"></select>
                </div>
            </div>
        </div>

        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_bathroom_min"></select>
                </div>
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_bathroom_max"></select>
                </div>
            </div>
        </div>

        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_area_min"></select>
                </div>
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_area_max"></select>
                </div>
            </div>
        </div>

        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_price_min"></select>
                </div>
                <div class="eight wide column">
                    <select class="ui dropdown fluid" id="property_price_max"></select>
                </div>
            </div>
        </div>

        <div class="sixteen wide column">
            <select class="ui dropdown fluid" id="property_furnishing"></select>
        </div>

        <div class="sixteen wide column">
            <select class="ui dropdown fluid" id="completion_status"></select>
        </div>

        <div class="sixteen wide column">
            <div class="ui input fluid">
                <input id="keyword" placeholder="{{__('page.form.placeholders.keyword')}}" name="keyword" type="text" style="border-radius:0;" />
            </div>
        </div>

        <div></div>

    </div>
    <div class="ui grid">
        <div class="six wide column">
            <button class="ui button fluid" type="button" name="button" onclick="helpers.filter.property();">{{__( 'page.form.placeholders.clear_mobile' ) }}</button>
        </div>
        <div class="ten wide column">
            <button class="ui button fluid" type="button" name="button" onclick="search.property.regular.__init(this);">{{__( 'page.global.search' ) }}</button>
        </div>
    </div>
</div>
