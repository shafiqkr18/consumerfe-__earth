<div class="ui form special tiny modal property_call" id="modal_property_call">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <p class="modal_fields_required">{!!__( 'page.modals.property.call.catch_msg' ) !!}</p>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.name' ) }}</label> --}}
                        <input id="modal_property_call_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*"
                            name="name" type="text">
                    </div>
                    <div class="field sixteen wide column">
                        <label>{{__( 'page.modals.hints.request_call_back' ) }}</label>
                        <input type="tel" id="modal_property_call_phone" type="number" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- {{__( 'page.modals.hints.email' ) }} --}}
                        <input id="modal_property_call_email" placeholder="{{__( 'page.form.placeholders.email' ) }}*"
                            name="email" type="email">
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <button class="ui submit button" id="modal_property_call_data" onclick="
            if($('#modal_property_call').form('is valid')){
                lead.property.call_request_back.__init(this);
                $(this).addClass('track_callback_submit');
            } else {
                console.log('form not validated!')
            }">
                <i class="phone volume icon"></i>
                {{__( 'page.form.button.request_call_free_back' ) }}
            </button>
        </div>
        <div class="sixteen wide column">
            <input id="modal_property_call_latest_info" placeholder="{{__( 'page.form.placeholders.email' ) }}"
                name="latest_info" type="checkbox" checked>
            <label>{{__( 'page.modals.hints.latest_info' ) }}</label>
        </div>
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="one wide column">
                    <div class="bar"></div>
                </div>
                <div class="fourteen wide column">
                    <div>
                        <span>T:</span>
                        <div id="modal_property_call_agent_contact_phone" dir="ltr" class="track_call_submit"
                            onclick="lead.property.call.__init(this, true)"></div>
                    </div>
                    <div>
                        <span id="modal_property_call_agent_name"></span>
                    </div>
                    <div>
                        <span id="modal_property_call_agency_name">,</span>
                    </div>
                    <p>{{__( 'page.property.details.ref_no_title' ) }}: <span id="modal_property_call_ref_no"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    validate.rules.lead.call.__init();
</script>
