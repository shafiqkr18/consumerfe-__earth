<form class="ui form modal property_email" id="modal_property_email" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <span>{{__( 'page.form.more_details_title' )}}</span>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.full_name' ) }}</label> --}}
                        <input id="modal_property_email_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*" name="name" type="text">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.email' ) }}</label> --}}
                        <input id="modal_property_email_email" placeholder="{{__( 'page.form.placeholders.email' ) }}*" name="email" type="email">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.phone' ) }}</label> --}}
                        <input type="tel" id="modal_property_email_phone" type="number" name="phone">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>Message</label> --}}
                        <textarea id="modal_property_email_textarea" rows="3">{{__( 'page.modals.property.email.message', [ 'reference' => ''] ) }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <span>{{__( 'validation.required_all' )}}*</span>
        </div>
        <div class="sixteen wide column">
            <button class="ui submit button" id="modal_property_email_data" onclick="if($('#modal_property_email').form('is valid')){
                lead.property.email.__init(this);$(this).addClass('track_email_submit');}else{console.log('form not validated!')}">
                <i class="envelope outline icon"></i>{{__( 'page.form.send_email' ) }}
            </button>
        </div>
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="four wide column">
                    <img class="ui small circular image f_p" id="modal_property_email_agent_img" data-type="modal" data-second="{{ cdn_asset('assets/img/default/no-agent-img.png') }}" src="" alt="" />
                </div>
                <div class="twelve wide column">
                    <div>
                        <span>{{trans_choice(  __( 'page.global.agent' ), 0 ) }}:</span>
                    </div>
                    <div>
                        <span id="modal_property_email_agent_name">,</span>
                    </div>
                    <div>
                        <span id="modal_property_email_agency_name"></span>
                    </div>
                    <p>{{__( 'page.property.details.ref_no_title' ) }}: <span id="modal_property_email_ref_no"></span></p>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    validate.rules.lead.email.__init();
</script>
