<form class="ui form modal property_alert" id="modal_property_alert" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">{{__( 'page.modals.consumer.alert.title' ) }}</div>
        <div><p>{{__( 'page.modals.consumer.alert.subtitle' ) }}</p></div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="eight wide stretched column">
                        <input type="text" name="name" id="property_alert_name" autocomplete='name' placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="">
                    </div>
                    <div class="eight wide stretched column">
                        <input type="email" name="email" id="property_alert_email" autocomplete='email' placeholder="{{__( 'page.form.placeholders.email' ) }}" value="">
                    </div>
                </div>
                <div class="ui grid">
                    <div class="eight wide stretched column">
                        <select name="rent_buy" class="ui dropdown" id="property_alert_rent_buy"></select>
                    </div>
                    <div class="eight wide stretched column">
                        <select name="frequency" class="ui dropdown" id="property_alert_frequency"></select>
                    </div>
                </div>
                <div class="ui grid">
                    <div class="eight wide stretched column">
                        <select name="price_min" class="ui dropdown" id="property_alert_price_min"></select>
                    </div>
                    <div class="eight wide stretched column">
                        <select name="price_max" class="ui dropdown" id="property_alert_price_max"></select>
                    </div>
                    <div class="sixteen wide stretched column">
                        <div class="ui multiple search selection dropdown" id="property_alert_suburb">
                            <i class="dropdown icon"></i>
                            <div class="default text"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column stretched">
            <button onclick="lead.property.alert.__init(this);" class="ui submit button">{{__( 'page.form.button.submit' ) }}</button>
        </div>
    </div>
</form>
