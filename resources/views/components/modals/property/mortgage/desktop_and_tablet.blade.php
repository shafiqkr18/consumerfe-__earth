<form class="ui modal form property_mortgage" id="modal_property_mortgage" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="ui grid">
            <div class="sixteen wide column">
                <div>{{__( 'page.mortgage.powered_by' ) }}</div>
                <div>
                    <img src="{{ cdn_asset('assets/img/eebank.png') }}" alt="Zoom Property" />
                </div>
            </div>
            <div class="sixteen wide column">
                {{__( 'page.mortgage.emi_title' ) }}
                <span>{{__( 'page.global.aed' ).' ' }}
                </span><span id="project_emi_monthly"></span> {{__( 'page.mortgage.per_monthly' ) }}
                <div class="hide" id="project_emi_interest_rate"></div>
                <div class="hide" id="project_emi_term"></div>
            </div>
            <div class="sixteen wide column">
                <div><img src="{{env('ASSET_URL').'assets/img/property/purchase.jpg'}}" class="ui image"></div>
                <div>
                    {{__( 'page.mortgage.calculations.purchase_price' ) }}
                    <div><span>{{__( 'page.global.aed' ) }}</span> <span id="project_loan_amount"></span></div>
                </div>
            </div>
            <div class="eight wide column">
                <div></div>
                <div>{{__( 'page.mortgage.calculations.down_payment' ) }}</div>
                <div id="project_emi_down_payment"></div>
            </div>
            <div class="eight wide column">
                <div></div>
                <div>{{__( 'page.mortgage.calculations.finance_amount' ) }}</div>
                <div id="project_emi_finance_amount"></div>
            </div>
            <div class="eight wide column">
                <div></div>
                <div>{{__( 'page.mortgage.calculations.total_amount' ) }}</div>
                <div id="project_emi_total_amount"></div>
            </div>
            <div class="eight wide column">
                <div></div>
                <div>{{__( 'page.mortgage.calculations.total_interest' ) }}</div>
                <div id="project_emi_total_interest"></div>
            </div>
            <div class="sixteen wide column">
                {{__( 'page.mortgage.leave_details_message' ) }}
            </div>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        <input name="first_name" id="mortgage_user_name" type="text" placeholder="{{__( 'page.form.placeholders.first_name' ) }}*" value="" />
                    </div>
                    <div class="field sixteen wide column">
                        <input name="last_name" id="mortgage_user_name_last" type="text" placeholder="{{__( 'page.form.placeholders.last_name' ) }}*" value="" />
                    </div>
                    <div class="field sixteen wide column">
                        <input name="email" id="mortgage_user_email" type="email" placeholder="{{__( 'page.form.placeholders.email' ) }}*" value="" />
                    </div>
                    <div class="field sixteen wide column">
                        <input type="tel" id="mortgage_user_phone" type="number" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*">
                    </div>
                    <div class="field sixteen wide column">
                        <input name="home_phone" id="mortgage_user_work_phone" type="number" placeholder="{{__( 'page.form.placeholders.work_phone' ) }}" value="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column stretched">
            <button class="ui button" id="mortgage_lead" onclick="
                if($('#modal_property_mortgage').form('is valid')){
                    lead.mortgage.__init(this);
                } else{
                    console.log('form not validated!')
                }" data-model="" data-_id="{{array_get($data,'_id')}}">
                <i class="envelope outline icon"></i>
                {{__( 'page.mortgage.get_finance' ) }}
            </button>
        </div>
    </div>
</form>
<script>
    validate.rules.lead.mortgage.__init();
</script>
