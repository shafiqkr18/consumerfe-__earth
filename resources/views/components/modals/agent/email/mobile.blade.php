<form class="ui form modal agent_email" id="modal_agent_email" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <span>{{__( 'page.form.more_details_title' )}}</span>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.full_name' ) }}</label> --}}
                        <input id="modal_agent_email_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*" name="name" type="text">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.email' ) }}</label> --}}
                        <input id="modal_agent_email_email" placeholder="{{__( 'page.form.placeholders.email' ) }}*" name="email" type="email">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>{{__( 'page.modals.hints.phone' ) }}</label> --}}
                        <input type="tel" id="modal_agent_email_phone" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*">
                    </div>
                    <div class="field sixteen wide column">
                        {{-- <label>Message</label> --}}
                        <textarea id="modal_agent_email_textarea" rows="3">{{__( 'page.modals.agent.email.message' ) }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <span>{{__( 'validation.required_all' )}}*</span>
        </div>
        <div class="sixteen wide column">
            <button class="ui button" id="modal_agent_email_data" data-subsource="@desktop{{'desktop'}}@elsedesktop{{'mobile'}}@enddesktop" onclick="
                if($('#modal_agent_email').form('is valid')){ lead.agent.email.__init(this); $(this).addClass('track_email_submit'); } else {
                    console.log('form not validated!') }">
                <i class="envelope outline icon"></i>
                {{__( 'page.form.button.send' ) }}
            </button>
        </div>
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="four wide column">
                    <img class="ui tiny circular image f_p" id="modal_agent_email_agent_img" data-type="modal" data-second="{{ cdn_asset('assets/img/default/no-agent-img.png') }}" src="" alt="Zoom property" />
                </div>
                <div class="twelve wide column">
                    <div>
                        <span>{{trans_choice(  __( 'page.global.agent' ), 0 ) }}:</span>
                    </div>
                    <div>
                        <span id="modal_agent_email_agent_name">,</span>
                    </div>
                    <div>
                        <span id="modal_agent_email_agency_name"></span>
                    </div>
                    <p>{{__( 'page.property.details.ref_no_title' ) }}: <span id="modal_agent_email_ref_no"></span></p>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    validate.rules.lead.agent.email.__init();
</script>
