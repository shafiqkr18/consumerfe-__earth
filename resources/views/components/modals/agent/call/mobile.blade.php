<div class="ui form special tiny modal agent_call" id="modal_agent_call">
    <i class="close icon"></i>
    <div class="ui grid container">
        <div class="sixteen wide column">
            <div class="ui grid">
                <div class="one wide column">
                    <div class="bar"></div>
                </div>
                <div class="fourteen wide column">
                    <div>
                        <span>T:</span>
                        <div id="modal_agent_call_agent_contact_phone" dir="ltr" class="track_call_submit"
                            onclick="lead.agent.call.__init(this)"></div>
                    </div>
                    <div>
                        <span id="modal_agent_call_agent_name"></span>
                    </div>
                    <div>
                        <span id="modal_agent_call_agency_name">,</span>
                    </div>
                    {{-- <p>{{__( 'page.property.details.ref_no_title' ) }}: <span id="modal_agent_call_ref_no"></span>
                    </p> --}}
                </div>
            </div>
        </div>
    </div>
</div>