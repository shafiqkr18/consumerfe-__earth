<form class="ui form modal agent_review" id="modal_agent_review" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <span>{{__( 'page.agent.details.agent_title' )}}</span>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div id="modal_agent_review_rate" class="ui massive star rating" data-max-rating="5"></div>
                    <label id="expression"></label>
                    <div class="field sixteen wide column">
                        <textarea id="modal_agent_review_textarea" rows="8" placeholder="{{__( 'page.modals.agent.review.message' ) }}"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="sixteen wide column">
            <button class="ui button" id="modal_agent_review_data" data-subsource="@desktop{{'desktop'}}@elsedesktop{{'mobile'}}@enddesktop" onclick="
                if($('#modal_agent_review').form('is valid')){ lead.agent.email.__init(this); $(this).addClass('track_review_submit'); } else {
                    console.log('form not validated!') }">
                <i class="star outline icon"></i>
                {{__( 'page.form.button.submit' ) }}
            </button>
        </div>
    </div>
</form>
<script>
    validate.rules.agent.review.__init();
</script>
