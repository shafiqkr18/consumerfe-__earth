<form class="ui form special tiny modal agent_callback" id="modal_agent_callback" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <span>{{__( 'page.form.button.request_call_back' ) }}</span>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="field sixteen wide column">
                        <input id="modal_agent_callback_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}*" name="name" type="text" />
                    </div>
                    <div class="field sixteen wide column">
                        <input type="tel" id="modal_agent_callback_phone" name="phone" placeholder="{{__( 'page.form.placeholders.phone' ) }}*"/>
                    </div>
                    <div class="field sixteen wide column">
                        <p class="modal_fields_required">{{__( 'page.modals.property.call_back.message' ) }}</p>
                    </div>
                    <div class="field sixteen wide column">
                        <button class="ui button" id="modal_agent_callback_data" onclick="
                            if($('#modal_agent_callback').form('is valid') && helpers.phonenumber.iti.isValidNumber()){
                                lead.agent.callback.__init(this);
                                $(this).addClass('track_callback_submit');
                            } else {
                                console.log('form not validated!')
                                
                            }">
                            <i class="phone icon"></i>
                            {{__( 'page.form.button.request_call_back' ) }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    validate.rules.lead.agent.callback.__init();
</script>