<form class="ui form modal services_contact" id="modal_services_contact" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="ui grid">
            <div class="six wide column">
                <img id="modal_services_contact_logo" src="" alt="{{$seo_image_alt}}" />
            </div>
            <div class="ten wide column">
                <div id="modal_services_contact_name"></div>
                <p id="modal_services_contact_description"></p>
            </div>
        </div>
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                    <div class="eight wide column field">
                        <input id="modal_services_contact_user_name" type="text" name="name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="">
                    </div>
                    <div class="eight wide column field">
                        <input id="modal_services_contact_user_email" type="email" name="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="">
                    </div>
                    <div class="field sixteen wide column">
                        <input type="tel" id="modal_services_contact_user_phone" name="phone">
                    </div>
                    <div class="sixteen wide column field">
                        <textarea id="modal_services_contact_user_message" rows="2" type="text" placeholder="Message" value="">{{__( 'page.modals.service.contact.message' ) }}</textarea>
                    </div>
                </div>
                <div>

                </div>
            </div>
        </div>
        <div class="sixteen wide column stretched">
            <button id="modal_services_contact_data" onclick="
            if($('#modal_services_contact').form('is valid')){
                lead.services.email.__init(this);
                $(this).addClass('track_email_submit');
            } else {
                console.log('form not validated!')
            }" class="ui button">{{__( 'page.form.button.send' ) }}</button>
        </div>
    </div>
</form>
<script>
    validate.rules.lead.service.__init();
</script>
