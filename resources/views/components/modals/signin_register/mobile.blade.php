<div class="ui modal mobile_signin_register" id="modal_mobile_signin_register">
    <i class="close icon"></i>

    <div class="ui grid">
        <div class="sixteen wide column">
            <div> {{__( 'page.modals.signin_register.title' ) }} </div>
            <div> {{__( 'page.modals.signin_register.subtitle' ) }} </div>
            <div>
                <img src="{{cdn_asset('assets/img/logo-white.png')}}" alt="Zoom Property" />
            </div>
        </div>
    </div>

    <div class="ui grid">
        <div class="sixteen wide column">

            <div class="ui top attached tabular menu">
                <a class="item active" data-tab="signin">{{__( 'page.modals.signin_register.signin' ) }}</a>
                <a class="item" data-tab="register">{{__( 'page.modals.signin_register.register' ) }}</a>
            </div>

            <form class="ui form bottom attached tab segment active" data-tab="signin" id="modal_signin" onsubmit="return false;">
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <div class="sixteen wide column">
                            <div class="field ui fluid icon input">
                                <input type="text" name="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="" />
                                <i class="user icon"></i>
                            </div>
                            <div class="field ui fluid icon input">
                                <input type="password" name="password" placeholder="{{__( 'page.form.placeholders.password' ) }}" value="" />
                                <i class="lock icon"></i>
                            </div>
                            <div class="sixteen wide column"><div id="signin_error" class="ui error message"></div></div>
                        </div>
                        <div class="sixteen wide column">
                            <button type="button" class="ui submit button fluid" name="button" onclick="if($('#modal_signin').form('is valid')){ user.auth.login.__init(this);} else {console.log('form not validated!')}">
                                {{__( 'page.modals.signin_register.signin' ) }}
                            </button>
                        </div>
                        <div class="ui grid">
                            <div class="sixteen wide column">
                                <p><a data-tab="forgot_password" class="item_forgot_password">{{__( 'page.modals.signin_register.forgot_password' ) }}</a></p>
                            </div>
                            <div class="sixteen wide column">
                                <p>-or continue with-</p>
                            </div>
                            <div class="eight wide column">
                                <button type="button" onclick="user.auth.social.facebook.__init();" class="ui button fluid">{{__( 'page.modals.signin_register.facebook' ) }}</button>
                            </div>
                            <div class="eight wide column">
                                <button type="button" onclick="user.auth.social.google.__init();" class="ui button fluid">
                                    {{__( 'page.modals.signin_register.google' ) }}
                                </button>
                            </div>
                            <div class="sixteen wide column">
                                <p>
                                    {{__( 'page.modals.signin_register.agree_terms_signin' ) }}
                                    <span>
                                        <a target="_blank" href="{{route('privacy-policy', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.privacy_policy' ) }}</a>
                                        {{ (Lang::has('page.global.and', \App::getLocale())) ? __( 'page.global.and' ) : "&" }}
                                        <a target="_blank" href="{{route('terms-and-conditions', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.terms_conditions' ) }}</a>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <form class="ui form bottom attached tab segment" data-tab="register" id="modal_register" onsubmit="return false;">
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <div class="sixteen wide column">
                            <div class="field ui fluid icon input">
                                <input type="text" name="full_name" placeholder="{{__( 'page.form.placeholders.full_name' ) }}" value="">
                                <i class="user icon"></i>
                            </div>
                            <div class="field ui fluid icon input">
                                <input type="email" name="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="">
                                <i class="envelope icon"></i>
                            </div>
                            <div class="field ui fluid icon input">
                                <input type="password" name="password" placeholder="{{__( 'page.form.placeholders.password' ) }}" value="">
                                <i class="lock icon"></i>
                            </div>
                            <div class="field ui fluid icon input">
                                <input type="password" name="confirm_password" placeholder="{{__( 'page.form.placeholders.password_confirm' ) }}" value="">
                                <i class="lock icon"></i>
                            </div>
                        </div>
                        <div class="sixteen wide column"><div id="register_error" class="ui error message"></div></div>
                        <div class="sixteen wide column">
                            <button onclick="if($('#modal_register').form('is valid')){ user.auth.register.__init(this);} else {console.log('form not validated!')}" type="button" class="ui submit button fluid" name="button">
                                {{__( 'page.modals.signin_register.register' ) }}
                            </button>
                        </div>

                        <div class="ui grid">
                            <div class="sixteen wide column"></div>
                            <div class="sixteen wide column">
                                <p>-or continue with-</p>
                            </div>
                            <div class="eight wide column">
                                <button type="button" onclick="user.auth.social.facebook.__init();" class="ui button fluid">{{__( 'page.modals.signin_register.facebook' ) }}</button>
                            </div>
                            <div class="eight wide column">
                                <button type="button" onclick="user.auth.social.google.__init();" class="ui button fluid">{{__( 'page.modals.signin_register.google' ) }}</button>
                            </div>
                            <div class="sixteen wide column">
                                <p>
                                    {{__( 'page.modals.signin_register.agree_terms' ) }}
                                    <span>
                                        <a target="_blank" href="{{route('privacy-policy', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.privacy_policy' ) }}</a>
                                        {{ (Lang::has('page.global.and', \App::getLocale())) ? __( 'page.global.and' ) : "&" }}
                                        <a target="_blank" href="{{route('terms-and-conditions', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.terms_conditions' ) }}</a>
                                    </span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

            <form class="ui form bottom attached tab segment" data-tab="forgot_password" id="modal_forgot_password" onsubmit="return false;">
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <h4> {{__( 'page.modals.signin_register.forgot_password' ) }} </h4>
                        <div class="sixteen wide column">
                            <div class="field ui fluid icon input">
                                <input class="validate" id="forgot_password_input_confirm_password" type="email" name="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" value="">
                                <i class="envelope icon"></i>
                            </div>
                        </div>
                        <div id="password_error" class="ui error message"></div>
                        <div class="sixteen wide column">
                            <button id="forgot_password_submit_btn" onclick="if($('#modal_forgot_password').form('is valid')){ user.auth.password.forgot(this);} else {console.log('form not validated!')}" type="button" class="ui submit button fluid" name="button">
                                {{__( 'page.form.button.reset_password' ) }}
                            </button>
                        </div>

                        <div class="ui grid">
                            <div class="sixteen wide column">
                                <p><a href="javascript:void(0)" data-tab="signin" class="item_back_to_signin"><i class="arrow left icon"></i>{{__( 'page.modals.signin_register.go_back_signin' ) }}</a></p>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script async>
    validate.rules.auth.login.__init();
    validate.rules.auth.register.__init();
    validate.rules.auth.forgot_password.__init();
</script>
