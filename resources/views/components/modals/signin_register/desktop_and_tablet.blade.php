<div class="ui modal signin_register" id="modal_signin_register">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="eight wide column">
            <div>
                <div> {{__( 'page.modals.signin_register.title' ) }} </div>
                <div> {{__( 'page.modals.signin_register.subtitle' ) }} </div>
                <ul>
                    <li><i class="check icon"></i> {{__( 'page.modals.signin_register.promotions.update_profile' ) }} </li>
                    <li><i class="check icon"></i> {{__( 'page.modals.signin_register.promotions.get_alerts' ) }} </li>
                    <li><i class="check icon"></i> {{__( 'page.modals.signin_register.promotions.save_favourites' ) }} </li>
                    <li><i class="check icon"></i> {{__( 'page.modals.signin_register.promotions.save_search' ) }} </li>
                </ul>
            </div>
        </div>
        <form class="eight wide column ui form" id="modal_signin" onsubmit="return false;">
            <div>{{__( 'page.modals.signin_register.signin' ) }}</div>
            <div class="ui grid">
                <div class="field eight wide stretched column">
                    <div class="ui input">
                        <input type="text" name="email" placeholder="{{__( 'page.form.placeholders.email' ) }}" />
                    </div>
                </div>
                <div class="field eight wide stretched column">
                    <div class="ui input">
                        <input type="password" id="signin_input_password" name="password" placeholder="{{__( 'page.form.placeholders.password' ) }}" />
                    </div>
                </div>
                <div class="sixteen wide column"><div id="signin_error" class="ui error message"></div></div>
            </div>
            <div class="ui grid">
                <div class="ten wide column">
                    <a href="javascript:void(0)" onclick="pages.components.modal.signin.toggle.__init('modal_forgot_password')"> {{__( 'page.modals.signin_register.forgot_password' ) }} </a>
                </div>
                <div class="six wide column">
                    <button class="ui submit button" id="signin_submit_btn" onclick="if($('#modal_signin').form('is valid')){ user.auth.login.__init(this);} else {console.log('form not validated!')}">
                        {{__( 'page.modals.signin_register.signin' ) }}
                    </button>
                </div>
            </div>
            <div class="ui grid">
                <div class="sixteen wide column">
                    <p> {{__( 'page.modals.signin_register.account' ) }} <a href="javascript:void(0)" onclick="pages.components.modal.signin.toggle.__init('modal_register')">{{__( 'page.modals.signin_register.register_here' ) }}</a></p>
                </div>
            </div>
            <div class="ui grid">
                <div class="eight wide column">
                    <button type="button" onclick="user.auth.social.facebook.__init();" class="ui button fluid">
                        <i class="facebook f icon"></i> {{__( 'page.modals.signin_register.signin_facebook' ) }}
                    </button>
                </div>
                <div class="eight wide column">
                    <button type="button" onclick="user.auth.social.google.__init();" class="ui button fluid">
                        <i class="google icon"></i> {{__( 'page.modals.signin_register.signin_google' ) }}
                    </button>
                </div>
            </div>
            <div class="ui grid">
                <div class="sixteen wide column">
                    <span>{{__( 'page.modals.signin_register.agree_terms' ) }}
                        <a target="_blank" href="{{route('privacy-policy', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.privacy_policy' ) }}</a>
                        {{ (Lang::has('page.global.and', \App::getLocale())) ? __( 'page.global.and' ) : "&" }}
                        <a target="_blank" href="{{route('terms-and-conditions', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.terms_conditions' ) }}</a>
                    </span>
                </div>
            </div>
        </form>
        <form class="eight wide column ui form" id="modal_register" onsubmit="return false;">
            <div> {{__( 'page.modals.signin_register.register' ) }} </div>
            <div class="ui grid">
                <div class="field eight wide stretched column">
                    <div class="ui input">
                        <input class="validate" name="full_name" type="text" placeholder="{{__( 'page.form.placeholders.full_name' ) }}">
                    </div>
                </div>
                <div class="field eight wide stretched column">
                    <div class="ui input">
                        <input class="validate" name="email" type="email" placeholder="{{__( 'page.form.placeholders.email' ) }}">
                    </div>
                </div>
                <div class="field eight wide stretched column">
                    <div class="ui input">
                        <input class="validate" name="password" type="password" placeholder="{{__( 'page.form.placeholders.password' ) }}">
                    </div>
                </div>
                <div class="field eight wide stretched column">
                    <div class="ui input">
                        <input class="validate" id="register_input_confirm_password" name="confirm_password" type="password" placeholder="{{__( 'page.form.placeholders.password_confirm' ) }}">
                    </div>
                </div>
                <div class="sixteen wide column"><div id="register_error" class="ui error message"></div></div>
            </div>
            <div class="ui grid">
                <div class="ten wide column">
                    <p> {{__( 'page.modals.signin_register.already_registered' ) }}
                        <a onclick="pages.components.modal.signin.toggle.__init('modal_signin')" class="toggle-menu" data-form="signin-form">{{__( 'page.modals.signin_register.signin_here' ) }}</a>
                    </p>
                </div>
                <div class="six wide column">
                    <button id="register_submit_btn" onclick="if($('#modal_register').form('is valid')){ user.auth.register.__init(this);} else {console.log('form not validated!')}" type="submit" class="ui button">{{__( 'page.form.button.register' ) }}</button>
                </div>
            </div>
            <div class="ui grid">
                <div class="eight wide column">
                    <button type="button" onclick="user.auth.social.facebook.__init();" class="ui button fluid">
                        <i class="facebook f icon"></i> {{__( 'page.modals.signin_register.register_facebook' ) }}
                    </button>
                </div>
                <div class="eight wide column">
                    <button type="button" onclick="user.auth.social.google.__init();" class="ui button fluid">
                        <i class="google icon"></i> {{__( 'page.modals.signin_register.register_google' ) }}
                    </button>
                </div>
            </div>
            <div class="ui grid">
                <div class="sixteen wide column">
                    <span>{{__( 'page.modals.signin_register.agree_terms' ) }}
                        <a target="_blank" href="{{route('privacy-policy', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.privacy_policy' ) }}</a>
                        {{ (Lang::has('page.global.and', \App::getLocale())) ? __( 'page.global.and' ) : "&" }}
                        <a target="_blank" href="{{route('terms-and-conditions', ['lng'=>\App::getLocale()])}}">{{__( 'page.modals.signin_register.terms_conditions' ) }}</a>
                    </span>
                </div>
            </div>
        </form>
        <form class="eight wide column ui form" id="modal_forgot_password" onsubmit="return false;">
            <div> {{__( 'page.modals.signin_register.forgot_password' ) }} </div>
            <div class="ui grid">
                <div class="field sixteen wide column">
                    <div class="ui input">
                        <input id="forgot_password_input_confirm_password" type="email" name="email" id="reset-email" placeholder="{{__( 'page.form.placeholders.email' ) }}">
                    </div>
                </div>
                <div class="nine wide column">
                    <a href="javascript:void(0)" onclick="pages.components.modal.signin.toggle.__init('modal_signin')"><i class="arrow left icon"></i>{{__( 'page.modals.signin_register.go_back_signin' ) }} </a>
                </div>
                <div class="seven wide column">
                    <button id="forgot_password_submit_btn" onclick="if($('#modal_forgot_password').form('is valid')){ user.auth.password.forgot(this); } else { console.log('form not validated!') }" class="ui button">
                        {{__( 'page.form.button.reset_password' ) }}
                    </button>
                </div>
                <div class="sixteen wide column"><div id="password_error" class="ui error message"></div></div>
            </div>
        </form>
    </div>
</div>
<script async>
    validate.rules.auth.login.__init();
    validate.rules.auth.register.__init();
    validate.rules.auth.forgot_password.__init();
</script>
