<form class="ui form modal custom_enquiry" id="modal_custom_enquiry" onsubmit="return false;">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui form">
                <div class="ui grid">
                   @if(array_get($data,'_id') == 'zj-golf-links' || array_get($data,'_id') == 'zj-golf-place-ii' || array_get($data,'_id') == 'zj-creek-palace' || array_get($data,'_id') == 'zj-emerald-hills'  || array_get($data,'_id') == 'zj-burj-crown' || array_get($data,'_id') == 'zj-urbana-iii')
                   <div class="field sixteen wide column" id="modal_custom_enquiry_investor_job">
                       <label>Are you an investor or job seeker?</label>
                       <div class="ui radio checkbox">
                           <input type="radio" value="Investor" name="investor_job" checked="checked">
                           <label>Investor</label>
                       </div>
                       <div class="ui radio checkbox">
                           <input type="radio" value="Job Seeker" name="investor_job">
                           <label>Job Seeker</label>
                       </div>
                   </div>
                   <div class="field sixteen wide column" id="modal_custom_enquiry_where">
                       <label>Are you looking to purchase a property in Dubai?</label>
                       <div class="ui radio checkbox">
                           <input type="radio" value="Yes" name="where" checked="checked">
                           <label>Yes</label>
                       </div>
                       <div class="ui radio checkbox">
                           <input type="radio" value="No" name="where">
                           <label>No</label>
                       </div>
                   </div>
                   <div class="field sixteen wide column" id="modal_custom_enquiry_when_now">
                       <label>Do you want to buy property now or later?</label>
                       <div class="ui radio checkbox">
                           <input type="radio" value="Now" name="when_now" checked="checked">
                           <label>Now</label>
                       </div>
                       <div class="ui radio checkbox">
                           <input type="radio" value="Later" name="when_now">
                           <label>Later</label>
                       </div>
                   </div>
                    @else
                    <div class="field sixteen wide column" id="modal_custom_enquiry_rooms">
                        <label>How many rooms are you looking to invest in?</label>
                        @if(array_get($data,'_id') == 'zj-south-beach-holiday-homes')
                        <div class="ui radio checkbox">
                            <input type="radio" value="1 bedroom (AED1.4m starting price)" name="rooms" checked="checked">
                            <label>1 bedroom (AED1.4m starting price)</label>
                        </div>
                        <div class="ui radio checkbox">
                            <input type="radio" value="2 bedroom (AED2.6m starting price)" name="rooms">
                            <label>2 bedroom (AED2.6m starting price)</label>
                        </div>
                        <div class="ui radio checkbox">
                            <input type="radio" value="3 bedroom (AED4m starting price)" name="rooms">
                            <label>3 bedroom (AED4m starting price)</label>
                        </div>
                        @elseif(array_get($data,'_id') == 'zj-the-valley-by-emaar')
                        <div class="ui radio checkbox">
                            <input type="radio" value="3 bedroom (AED1.17m starting price)" name="rooms" checked="checked">
                            <label>3 bedroom (AED1.17m starting price)</label>
                        </div>
                        <div class="ui radio checkbox">
                            <input type="radio" value="2 bedroom (AED2.6m starting price)" name="rooms">
                            <label>4 bedroom (AED1.45m starting price)</label>
                        </div>
                        @endif
                    </div>
                    <div class="field sixteen wide column" id="modal_custom_enquiry_when">
                        <label>When are you looking to buy?</label>
                        <div class="ui radio checkbox">
                            <input type="radio" value="ASAP" name="when" checked="checked">
                            <label>ASAP</label>
                        </div>
                        <div class="ui radio checkbox">
                            <input type="radio" value="Within 2 months" name="when">
                            <label>Within 2 months</label>
                        </div>
                        <div class="ui radio checkbox">
                            <input type="radio" value="After 2 months" name="when">
                            <label>After 2 months</label>
                        </div>
                    </div>
                    <div class="field sixteen wide column" id="modal_custom_enquiry_times">
                        <label>Have you invested in Dubai properties before or this is the first time?</label>
                        <div class="ui radio checkbox">
                            <input type="radio" value="First time" name="times" checked="checked">
                            <label>First time</label>
                        </div>
                        <div class="ui radio checkbox">
                            <input type="radio" value="Invested before" name="times">
                            <label>Invested before</label>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="sixteen wide column stretched">
            <button onclick="other.custom_enquiry.__init(this);" type="button" class="ui button">{{__( 'page.form.button.submit' ) }}</button>
        </div>
    </div>
    <div class="ui error message"></div>
</form>
