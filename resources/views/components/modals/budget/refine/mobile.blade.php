<div class="ui modal refine_search" id="modal_refine_search">
    <i class="close icon"></i>
    <div class="ui grid">
        <div class="sixteen wide column">
            {{__( 'page.budget.refine_search' )}}
        </div>
    </div>
    <div class="five wide column">
        <div class="ui grid">
            <div class="sixteen wide column">
                <div class="ui segment">
                    <div class="ui accordion">
                        @foreach($refinements as $refine)
                        @php
                          $area = preg_replace("/\([^)]+\)/","",array_get($refine,'area'));
                        @endphp
                        <div class="title" onclick="pages.budget.listings.sort.__init('{{kebab_case($area)}}', this);">
                            <i class="dropdown icon"></i>
                            {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get($refine,'area')), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get($refine,'area'))) : array_get($refine,'area') }} [{{array_get($refine,'total')}}]
                        </div>
                        <div class="content">
                            @if(count(array_get($refine,'bedroom',0))>0)
                            <p class="transition hidden">Bedrooms</p>
                                @foreach(array_get($refine,'bedroom') as $bed)
                                <button type="button" class="ui button budget_sort_btn" name="button" onclick="pages.budget.listings.sort.__init('{{kebab_case($area)}}-{{$bed}}-bed', this);">{{ $bed == 0 ? 'Studio' : $bed.' Bed' }}</button>
                                @endforeach
                            @endif
                            @if(count(array_get($refine,'bathroom',0))>0)
                            <p class="transition hidden">Bathrooms</p>
                                @foreach(array_get($refine,'bathroom') as $bath)
                                <button type="button" class="ui button budget_sort_btn" name="button" onclick="pages.budget.listings.sort.__init('{{kebab_case($area)}}-{{$bath}}-bath', this);">{{ $bath.' Bath' }}</button>
                                @endforeach
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
