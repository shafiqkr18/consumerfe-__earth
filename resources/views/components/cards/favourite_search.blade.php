@isset($favResults)
    @if(!empty(array_get($favResults, 'data', [])))
        @foreach(array_get($favResults, 'data') as $search)
        @php
            $prefix     =   false;
        @endphp

        <div class="ui grid card fluid">
            <div class="row">
                <div class="fourteen wide column">
                    <div>
                        @if(array_has($search,'property.bedroom.max'))
                            @if(array_has($search,'property.bedroom.min') && array_has($search,'property.bedroom.max') && array_get($search, 'property.bedroom.min') !== array_get($search, 'property.bedroom.max'))
                                {{ array_get($search, 'property.bedroom.min') == 0 ? 'Studio' : array_get($search, 'property.bedroom.min')}} to
                            @endif
                            {{array_get($search, 'property.bedroom.max')}} bedroom
                            @php
                                $prefix     =   true;
                            @endphp
                        @elseif(array_has($search,'property.bedroom.min'))
                            {{array_get($search, 'property.bedroom.min')}} bedroom
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif
                        @if(array_has($search,'property.type'))
                            @if(is_array(array_get($search,'property.type')))
                                {{ implode( ", ", array_get($search, 'property.type') ) }}
                            @else
                                {{array_get($search, 'property.type')}}
                            @endif
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif
                        @if(array_has($search,'property.rent_buy'))
                            @if( $prefix )
                                for
                            @endif
                            @if(array_has($search,'property.residential_commercial'))
                                @if(is_array(array_get($search,'property.residential_commercial')))
                                    {{array_get($search, 'property.residential_commercial.0')}}
                                @else
                                    {{array_get($search, 'property.residential_commercial')}}
                                @endif
                                @if(is_array(array_get($search,'property.rent_buy')))
                                    {{ implode( " and ", array_get($search, 'property.rent_buy') ) }}
                                @else
                                    {{array_get($search, 'property.rent_buy')}}
                                @endif
                            @endif
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif
                        @if(array_has($search,'property.building'))
                            @if( $prefix )
                                in
                            @endif
                            @if(is_array(array_get($search,'property.building')))
                                {{ implode(" or ", array_get($search,'property.building')) }}
                            @else
                                {{array_get($search,'property.building')}}
                            @endif
                            @php
                                $prefix     =   true;
                            @endphp
                        @elseif(!array_has($search,'property.building') && array_has($search,'property.area'))
                            @if( $prefix )
                                in
                            @endif
                            @if(is_array(array_get($search,'property.area')))
                                {{ implode(" or ", array_get($search,'property.area')) }}
                            @else
                                {{array_get($search,'property.area')}}
                            @endif
                            @php
                                $prefix     =   true;
                            @endphp
                        @elseif(!array_has($search,'property.building') && !array_has($search,'property.area') && array_has($search,'property.city'))
                            @if( $prefix )
                                in
                            @endif
                            @if(is_array(array_get($search,'property.city')))
                                {{ implode(" or ", array_get($search,'property.city')) }}
                            @else
                                {{array_get($search,'property.area')}}
                            @endif
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif
                        @if(array_has($search,'property.price.min'))
                            Price greater than AED
                            {{ number_format(array_get($search,'property.price.min')) }}
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif
                        @if(array_has($search,'property.price.max'))
                            Price less than AED
                            {{ number_format(array_get($search,'property.price.max') ) }}
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif
                        @if(array_has($search,'property.bathroom.max'))
                            @if( $prefix )
                                with
                            @endif
                            @if(array_has($search,'property.bathroom.min') && array_has($search,'property.bathroom.max') && array_get($search, 'property.bathroom.min') !== array_get($search, 'property.bathroom.max'))
                                {{array_get($search, 'property.bathroom.min')}} to
                            @endif
                                {{array_get($search, 'property.bathroom.max')}} bathroom
                            @php
                                $prefix     =   true;
                            @endphp
                        @elseif(array_has($search,'property.bathroom.min'))
                            @if( $prefix )
                                with
                            @endif
                            {{array_get($search, 'property.bathroom.min')}} bathroom
                            @php
                                $prefix     =   true;
                            @endphp
                        @endif

                    </div>
                    <div><a href="{{array_get( $search, 'property.url', '' )}}">{{__( 'page.sections.dashboard.search_again' )}}</a></div>
                </div>
                <div class="two wide column">
                    <i class="trash icon" data-_id="{{array_get( $search, '_id', '' )}}" onclick="user.property.unfavourite.search(this);"></i>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="no_listing">
            <img src="{{ cdn_asset( 'assets/img/no_listing/fav_search.png' ) }}" alt="Zoom Property" />
            <div> {{__( 'page.sections.dashboard.not_favourite_search' ) }} </div>
            <div class="ui grid">
                <div class="sixteen wide column">
                    <button type="button" name="button"> <a href="{{ env( 'APP_URL' ) . \App::getLocale() }}">{{__( 'page.form.button.start_favourite' ) }}</a></button>
                </div>
            </div>
        </div>
    @endif
@endisset
