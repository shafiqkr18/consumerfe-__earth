@isset($favResults)
@if(!empty(array_get($favResults, 'data', [])))
@foreach(array_get($favResults, 'data') as $data)
    @php
        $property_title	 =	( !empty( array_get($data,'property.writeup.title','') ) ) ? strtolower( preg_replace('/-+/', '-', preg_replace( '/[^a-zA-Z0-9\-]/', '-', trim( array_get($data,'property.writeup.title','') ) ) ) ) : '';
    @endphp

    <div class="column p_c {{kebab_case(array_get($data,'property.area'))}} {{kebab_case(array_get($data,'property.area')).'-'.array_get($data,'property.bedroom')}}-bed {{kebab_case(array_get($data,'property.area')).'-'.array_get($data,'property.bathroom')}}-bath">
        <div class="ui segment">
            <div>
                <div class="jss370">
                    <div class="jss263">

                        <a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($data,'property.rent_buy',''))),'residential_commercial'=>strtolower(array_get($data,'property.residential_commercial','')),'name'=>$property_title,'id'=>array_get($data,'property._id','')]) }}">
                            <img alt="Zoom Property" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" data-second="{{ array_get($data,'property.images.original.0') }}" class="f_p" src="{{ cloudfront_asset(array_get($data ,'property._id','').'_0_desktop') }}" alt="...">
                        </a>
                        <div>

                        </div>
                    </div>
                    <div>
                        <div>
                            <i data-_id="{{array_get($data,'_id','')}}" onclick="user.property.unfavourite.property(this);" class="trash icon"></i>
                        </div>
                        <div dir="ltr"><a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($data,'property.rent_buy',''))),'residential_commercial'=>strtolower(array_get($data,'property.residential_commercial','')),'name'=>$property_title,'id'=>array_get($data,'property._id','')]) }}">{{array_get($data,'property.writeup.title','')}}</a></div>
                        <div><a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($data,'property.rent_buy',''))),'residential_commercial'=>strtolower(array_get($data,'property.residential_commercial','')),'name'=>$property_title,'id'=>array_get($data,'property._id','')]) }}">
                            {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $data, 'property.type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $data, 'property.type', '' ))) : array_get( $data, 'property.type', '' ) }}
                            {{ (\App::getLocale() !== 'en' && Lang::has('data.rent_buy.for_'.snake_case_custom(array_get( $data, 'property.rent_buy', '' )), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom(array_get( $data, 'property.rent_buy', '' ))) : 'for ' . array_get( $data, 'property.rent_buy', '' ) }}
                            </a>
                        </div>
                        <div>
                            @if(array_get( $data, 'property.residential_commercial' ) !== "Commercial" || (array_get( $data, 'property.bedroom' ) !== 0 && array_get( $data, 'property.bedroom' ) !== -1))
                            <i class="bed icon flip"></i> {{ strtolower( trans_choice( __( 'page.global.bed', [ 'bed' => array_get($data,'property.bedroom') ] ) , array_get($data,'property.bedroom','') ) )}}
                            @endif
                            @if(array_get( $data, 'property.bathroom' ) !== 0)
                            <i class="bath icon"></i> {{ strtolower( trans_choice( __( 'page.global.bath', [ 'bath' => array_get($data,'property.bathroom') ] ), array_get($data,'property.bathroom','') ) )}}
                            @endif
                            @if(array_get( $data, 'property.dimension.builtup_area') !== 0)
                            <i class="square icon"></i> {{array_get($data,'property.dimension.builtup_area')}} {{__( 'page.global.sqft' )}}
                            @endif
                        </div>
                        <div>
                            @if(array_get($data,'property.building','') != '')
                                {{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $data, 'property.building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $data, 'property.building', '' ))) : array_get( $data, 'property.building', '' ) }},
                            @endif
                            {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $data, 'property.area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $data, 'property.area', '' ))) : array_get( $data, 'property.area', '' ) }},
                            {{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $data, 'property.city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $data, 'property.city', '' ))) : array_get( $data, 'property.city', '' ) }}
                        </div>
                        <div data-agent_contact_name="{{array_get($data,'property.agent.contact.name')}}" data-agency_contact_name="{{array_get($data,'property.agent.agency.contact.name')}}" data-ref_no="{{array_get($data,'property.ref_no')}}" data-agent_contact_phone="{{array_get($data,'property.agent.contact.phone')}}" data-_id="{{array_get($data,'property._id')}}">
                            <button class="small ui purple button" onclick="pages.landing.property.call.__init(this);"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                            <button class="small ui purple button" onclick="pages.landing.property.callback.__init(this);"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button>
                            @notmobile
                            <button class="small ui purple button" onclick="pages.landing.property.email.__init(this);"><i class="envelope icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                            @elsenotmobile
                            <button class="small ui purple button" onclick="pages.landing.property.whatsapp.__init(this);"><i class="whatsapp icon"></i>{{__( 'page.card.actions.whatsapp' ) }}</button>
                            @endnotmobile
                        </div>
                    </div>
                    <div>
                        <div>
                            @if(array_get($data,'property.price',0) < 2000)
                                <div>Undisclosed</div>
                            @else
                                <div><span>{{__( 'page.global.aed' ) }}</span> {{number_format(array_get($data,'property.price',''))}}</div>
                            @endif
                        </div>
                        <p>{{array_get($data,'property.ref_no')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endforeach
    @else
    <div class="no_listing">
        <img src="{{ cdn_asset( 'assets/img/no_listing/fav_property.png' ) }}" alt="Zoom Property" />
        <div> {{__( 'page.sections.dashboard.not_favourite_property' ) }} </div>
        <div class="ui grid">
            <div class="sixteen wide column">
                <button type="button" name="button"> <a href="{{ env( 'APP_URL' ) . \App::getLocale() }}">{{__( 'page.form.button.start_favourite' ) }} </a></button>
            </div>
        </div>
    </div>
    @endif
@endisset
