<div id="property_card_template">
    <div class="column p_c dashboard_card">
        <div class="ui segment">
            <div>
                <div class="jss370">
                    <div class="jss263">
                        <a href="%property.web_link%" rel="nofollow">
                            <img alt="Zoom Property" data-type="listing" data-second="%property.images.original:0:1%" class="f_p" src="{{ cloudfront_asset() }}%property._id%_0_desktop" />
                        </a>
                        <div>
                            <img alt="Zoom Property" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" class="f_p" src="%agency.logo%" />
                        </div>
                    </div>

                    <div>
                        <div>
                            <div class="get_price" data-price="%property.price%">
                                <span>{{__( 'page.global.aed' ) }}</span>
                                <span>%property.price%</span>
                            </div>
                        </div>

                        <div>
                            @notmobile
                            <i data-_id="%property._id%" onclick="user.property.favourite.__init(this);" class="heart outline icon track_favourite"></i>
                            @elsenotmobile
                            <i data-_id="%property._id%" onclick="user.property.favourite.__init(this, true);" class="heart outline icon track_favourite"></i>
                            @endnotmobile
                        </div>
                        <div><a href="%property.web_link%" rel="nofollow">%property.writeup.title%</a></div>
                        <div><a href="%property.web_link%" rel="nofollow">%property.type% %property.rent_buy% </a></div>
                        <div>
                            %property.bedroom%
                            %property.bathroom%
                            <i class="square icon" id="sq_similar_%property.dimension.builtup_area%"></i> %property.dimension.builtup_area%
                        </div>
                        <div>
                            %property.building%
                            %property.area%,
                            %property.city%
                        </div>
                        <div data-_id="%property._id%">
                            <button class="small ui purple button track_call" id="t_call_similar_%property._id%"><i class="phone volume icon"></i></button>
                            {{-- <button class="small ui purple button track_callback" id="t_callback_similar_%property._id%">{{__( 'page.card.actions.call_back' ) }}</button> --}}
                            <button class="small ui purple button track_email" id="t_email_similar_%property._id%"><i class="envelope icon"></i></button>
                            {{-- @notmobile
                            @elsenotmobile --}}
                            <button class="small ui purple button track_chat" id="t_chat_similar_%property._id%"><i class="whatsapp icon"></i></button>
                            {{-- @endnotmobile --}}
                        </div>
                        {{-- <div>
                            <p>%property.ref_no%</p>
                        </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
