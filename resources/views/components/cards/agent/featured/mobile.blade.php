@php

    $agent      = array_get($featured_agent,'data.0',[]);

    $ref_no = array_get($agent,'ref_no','');
    $agent_name = array_get($agent,'contact.name','');
    $agency_email = array_get($agent,'agency.contact.email','');
    $agency_name = array_get($agent,'agency.contact.name','');
    $_id = array_get($agent,'_id','');

//   $picture = env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($agent,'contact.email','')));
    $track  = array_get($agent,'agency.settings.lead.call_tracking.track',false);
    $number = $track ? array_get($agent,'agency.settings.lead.call_tracking.phone') : array_get($agent,'contact.phone','');
    $number = $number == '' ? array_get($agent,'agency.contact.phone','') : $number;
    if($number[0] == 0){
        $number = substr($number,1);
    }
    $number = str_replace('971971','971','+971'.$number);
    array_set($agent,'number',$number);

    $select_fields          =   config('data.datalayer.fields.agent');
    $data_for_datalayer     =   array_dot_only($agent,$select_fields);
@endphp
<div id="featured_card_agent">
    <div class="ui container grid">
        <div class="sixteen wide column">
            <h3>{{ __( 'page.agent.details.featured_agent' )}}</h3>
        </div>
    </div>
    <div class="ui fluid grid">
        <div></div>
        @php
            $picture    =   !is_null(array_get($agent,'contact.picture')) ? array_get($agent,'contact.picture') : env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($agent,'contact.email','')));
        @endphp

        <div>
            <div class="ui card grid">
                <div class="sixteen wide column">
                    <img class="ui small circular image" data-type="listing" src="{{$picture}}" alt="{{ $seo_image_alt }}" />
                    <div>
                        <div>{{$agent_name}}</div>
                        <div>{{$agency_name}}</div>
                        <div>
                            <div>{{__( 'page.property.details.agent_card.license_title' ) }}: # </div>
                            <div>
                                @if(!empty(array_get($data,'agent.contact.license_no','')))
                                    {{array_get($data,'agent.contact.license_no','')}}
                                @else
                                    {{ 'Not Specified' }}
                                @endif
                            </div>
                        </div>
                        <div>
                            @php
                                $view_all_params =  ['lng' => \App::getLocale(), 'rent_buy' => 'buy', 'residential_commercial' => 'residential', 'properties-for-sale' ];
                            @endphp
                            <a href="{{route('search-results-page',$view_all_params).'?agency_id='.array_get($data,'agent.agency._id')}}">
                                {{__( 'page.property.details.agent_card.view_all' ) }}
                            </a>
                        </div>
                        <div>
                            <img class="f_p ui small image" data-type="listing" alt="{{ $seo_image_alt }}" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"
                                    src="{{cdn_asset('agency/logo/'. array_get(explode('@',$agency_email), 1, ''))}}" alt="Zoom Property" />
                        </div>
                    </div>
                </div>

                <div class="sixteen wide column">
                    <h2 class="ui dividing header"></h2>
                </div>

                <div class="sixteen wide column">
                    <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});">{{__( 'page.form.button.call_agent' ) }}</button>
                </div>
            </div>
        </div>
    </div>
</div>