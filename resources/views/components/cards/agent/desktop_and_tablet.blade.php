@php

  $agent      = array_get($agent,'data.0',[]);
//   $picture = env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($agent,'contact.email','')));
  $track  = array_get($agent,'agency.settings.lead.call_tracking.track',false);
  $number = $track ? array_get($agent,'agency.settings.lead.call_tracking.phone') : array_get($agent,'contact.phone','');
  $number = $number == '' ? array_get($agent,'agency.contact.phone','') : $number;
  if($number[0] == 0){
    $number = substr($number,1);
  }
  $number = str_replace('971971','971','+971'.$number);
  array_set($agent,'number',$number);

  $select_fields          =   config('data.datalayer.fields.agent');
  $data_for_datalayer     =   array_dot_only($agent,$select_fields);
@endphp
<div id="featured_agent">
    <div class="ui grid">
        <div class="column">
            <div>
                {{__( 'page.agent.details.area_specialist' )}}
                {{-- <a href="{{route('agent-finder-details', ['lng'=>\App::getLocale(),'name'=>kebab_case(array_get($agent,'contact.name')), 'id'=>array_get($agent,'_id')])}}">view agent profile <i class="caret right icon"></i></a> --}}
            </div>
            <div>
                <div class="ui grid">
                    <div class="four wide column">
                        <img class="ui image f_p" data-second="{{ cdn_asset('assets/img/default/no-agent-img.png') }}" src="{{ array_get($agent,'contact.picture') }}" alt="{{$seo_image_alt}}">
                    </div>
                    <div class="eight wide column">
                        <h5>{{array_get($agent,'contact.name',__( 'page.global.default_agent_name' ))}}</h5>
                        <h6>Assistant Manager - Sales</h6>
                        <a href="{{route('agent-finder-details', ['lng'=>\App::getLocale(),'name'=>kebab_case(array_get($agent,'contact.name')), 'id'=>array_get($agent,'_id')])}}">{{__( 'page.property.details.agent_card.view_profile' ) }} <i class="angle right icon"></i></a>
                        <p>
                            <span>{{__( 'page.agent.details.company' )}}:&nbsp;</span>{{array_get($agent,'agency.contact.name',__( 'page.global.default_agency_name' ))}}
                        </p>
                        <p>
                            <span>{{__( 'page.agent.details.nationality' )}}:</span>
                            @if(!empty(array_get($agent,'contact.nationality','')))
                            {{array_get($agent,'contact.nationality','Citizen')}}
                            @else
                                {{__( 'page.results.not_specified' )}}
                            @endif
                        </p>
                        <p>
                            <span>{{__( 'page.agent.details.languages' )}}:</span>
                            @if(!empty(array_get($agent,'contact.languages','')))
                                {{implode(", ", !is_null(array_get($agent,'contact.languages',null)) ? array_get($agent,'contact.languages',[]) : [])}}
                            @else
                                {{__( 'page.results.not_specified' )}}
                            @endif
                        </p>
                    </div>
                    <div class="three wide column">
                        <img class="f_p" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"
                            src="{{cdn_asset('agency/logo/'. array_get(explode('@',array_get($agent,'agency.contact.email')), 1, ''))}}" alt="Zoom Property"/>
                    </div>
                    <div class="sixteen wide column">
                        <div>
                            @if(array_has($agent, 'interview.youtube_id') && !empty(array_get($agent, 'interview.youtube_id','')))
                                <a href="https://www.youtube.com/watch?v={{array_get($agent, 'interview.youtube_id')}}?autoplay=true" class="ui secondary basic button track_interview popup-video-html5"><i class="play circle outline icon"></i></a>
                            @endif
                            <button class="ui purple basic button track_call" onclick="pages.agent.listings.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                            <button class="ui purple basic button track_callback" onclick="pages.agent.listings.callback.__init({{json_encode($data_for_datalayer)}});"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button>
                            <button class="ui purple basic button track_email" onclick="pages.agent.listings.email.__init({{json_encode($data_for_datalayer)}});"><i class="envelope outline icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
