@php
    $property_title			=	( !empty( array_get($property,'writeup.title','') ) ) ? strtolower( preg_replace('/-+/', '-', preg_replace( '/[^a-zA-Z0-9\-]/', '-', trim( array_get($property,'writeup.title','') ) ) ) ) : '';

    ### Call Tracking -START- ###
    $track  = array_get($property,'agent.agency.settings.lead.call_tracking.track', false);
    $number = $track ? array_get($property,'agent.agency.settings.lead.call_tracking.phone') : array_get($property,'agent.contact.phone','');
    $number = $number == '' ? array_get($property,'agent.agency.contact.phone','') : $number;
    if(is_array($number) && $number[0] == 0){
        $number = substr($number,1);
    }
    $number = str_replace('971971','971','+971'.$number);
    array_set($property,'number',$number);
    ### Call Tracking -END- ###

    $select_fields          =   config('data.datalayer.fields.property');
    $data_for_datalayer     =   array_dot_only($property,$select_fields);

@endphp
<div class="column p_c {{kebab_case(array_get($property,'area'))}} {{kebab_case(array_get($property,'area')).'-'.array_get($property,'bedroom')}}-bed {{kebab_case(array_get($property,'area')).'-'.array_get($property,'bathroom')}}-bath">
    @php
        $lat = is_null(array_get($property,'coordinates.lat',null)) ? 0 : array_get($property,'coordinates.lat',0);
        $lng = is_null(array_get($property,'coordinates.lng',null)) ? 0 : array_get($property,'coordinates.lng',0);
    @endphp
    <div class="ui segment" data-agency="{{array_get($property,'agent.agency.contact.name','')}}" data-coordinates_lat="{{$lat}}" data-coordinates_lng="{{$lng}}" data-title="{{array_get($property,'writeup.title','')}}" data-rent_buy="{{array_get( $property, 'rent_buy', '' )}}" data-bedroom="{{array_get($property,'bedroom','')}}" data-bathroom="{{array_get($property,'bathroom','')}}" data-sqft="{{array_get($property,'dimension.builtup_area','')}}"
         data-type="{{array_get( $property, 'type', '' )}}"
         data-price="{{number_format(array_get($property,'price',''))}}"
         data-ref="{{array_get($property,'ref_no','')}}" data-building="{{ array_get($property,'building','').',' }}"
         data-area="{{array_get($property,'area','')}}" data-city="{{array_get($property,'city','')}}" data-href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">
        <div>
            <div class="jss370">
                <div class="jss263">
                    <a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">
                        @notmobile
                        <img id="image-{{array_get($property,'_id')}}" class="f_p" alt="{{$seo_image_alt}}" data-type="listing" data-second= "{{ array_get($property,'images.original.0') }}" src="{{ cloudfront_asset(array_get($property ,'_id','').'_0_desktop') }}" />
                        @elsenotmobile
                        <img id="image-{{array_get($property,'_id')}}" class="f_p" alt="{{$seo_image_alt}}" data-type="listing" data-second= "{{ array_get($property,'images.original.0') }}" src="{{ cloudfront_asset(array_get($property ,'_id','').'_0_mobile') }}" />
                        @endnotmobile
                    </a>
                    <div>
                        @php
                            $agency_logo    =   array_get( explode('@', array_get($property,'agent.agency.contact.email')), '1', '');
                        @endphp
                        <img alt="{{$seo_image_alt}}" class="f_p" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" src="{{cdn_asset('agency/logo/'.$agency_logo)}}" />
                    </div>
                    @if(isset($featuredFlag) && $featuredFlag)
                        <span>{{__( 'page.global.featured')}}</span>
                    @endif
                </div>

                <div>
                    <div>
                        @if(array_get($property,'price',0) < 2000)
                            <div>Undisclosed</div>
                        @else
                        <div class="get_price" data-price="{{array_get($property,'price','')}}">
                                <span class="currency">{{__( 'page.global.aed' ) }}</span>
                                <span class="value">{{number_format(array_get($property,'price',''))}}</span>
                                @if(array_get($property,'cheques',0) !== 0)
                                <p><i class="check circle outline icon"></i> {{array_get($property,'cheques',0)}} {{trans_choice(  __( 'page.global.cheque' ), array_get($property,'cheques',0) ) }} </p>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div>
                        @notmobile
                        <i data-_id="{{array_get($property,'_id','')}}" {{ array_get($property,'favourite',false) ? 'data-_uid="'.array_get($property,'_id','').'"' : '' }} onclick="user.property.favourite.__init(this);" class="heart icon track_favourite {{ array_get($property,'favourite',false) ? 'favourite' : 'outline' }}"></i>
                        @elsenotmobile
                        <i data-_id="{{array_get($property,'_id','')}}" {{ array_get($property,'favourite',false) ? 'data-_uid="'.array_get($property,'_id','').'"' : '' }} onclick="user.property.favourite.__init(this, true);" class="heart icon track_favourite {{ array_get($property,'favourite',false) ? 'favourite' : 'outline' }}"></i>
                        @endnotmobile
                    </div>
                    <div><a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">{{array_get($property,'writeup.title','')}}</a></div>
                    <div><a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">
                      {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $property, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $property, 'type', '' ))) : array_get( $property, 'type', '' ) }}
                      {{-- {{ (\App::getLocale() !== 'en' && Lang::has('data.rent_buy.for_'.snake_case_custom(array_get( $property, 'rent_buy', '' )), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom(array_get( $property, 'rent_buy', '' ))) : 'for ' . array_get( $property, 'rent_buy', '' ) }} --}}
                    </a></div>
                    <div>
                        @if(array_get( $property, 'residential_commercial' ) == "Commercial" && array_get( $property, 'bedroom' ) >= 1 || array_get( $property, 'residential_commercial' ) == "Residential")
                        <i class="bed icon"></i>
                        {{ strtolower( trans_choice(  __( 'page.global.bed', [ 'bed' => array_get($property,'bedroom','') ] ) , array_get($property,'bedroom','') ) )}}
                        @endif
                        @if(array_get( $property, 'bathroom' ) !== 0)
                        <i class="bath icon"></i> {{ strtolower( trans_choice(  __( 'page.global.bath', [ 'bath' => array_get($property,'bathroom','') ] ), array_get($property,'bathroom','') ) )}}
                        @endif
                        @if(array_get( $property, 'dimension.builtup_area') !== 0)
                        <i class="square icon"></i> {{number_format(array_get($property,'dimension.builtup_area',''))}}
                        @endif
                    </div>
                    <div>
                      @if(array_get($property,'building','') != '')
                          {{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $property, 'building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $property, 'building', '' ))) : array_get( $property, 'building', '' ) }},
                      @endif
                      {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $property, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $property, 'area', '' ))) : array_get( $property, 'area', '' ) }},
                      {{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $property, 'city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $property, 'city', '' ))) : array_get( $property, 'city', '' ) }}
                    </div>

                    {{--<div data-_id="{{array_get($property,'_id','')}}" class="ui buttons">--}}
                        {{--<button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});">--}}
                            {{--@notmobile--}}
                            {{--<i class="phone volume icon"></i>--}}
                            {{--@elsenotmobile--}}
                            {{--@endnotmobile--}}
                            {{--{{__( 'page.card.actions.call' ) }}--}}
                        {{--</button>--}}
                        {{--<button class="small ui purple button track_callback" onclick="pages.landing.property.callback.__init({{json_encode($data_for_datalayer)}});">--}}
                            {{--@notmobile--}}
                            {{--<i class="phone icon"></i>--}}
                            {{--@elsenotmobile--}}
                            {{--@endnotmobile--}}
                            {{--{{__( 'page.card.actions.call_back' ) }}--}}
                        {{--</button>--}}
                        {{--@notmobile--}}
                        {{--<button class="small ui purple button track_email" onclick="pages.landing.property.email.__init({{json_encode($data_for_datalayer)}});">--}}
                            {{--@notmobile--}}
                            {{--<i class="envelope icon"></i>--}}
                            {{--@elsenotmobile--}}
                            {{--@endnotmobile--}}
                            {{--{{__( 'page.card.actions.email' ) }}--}}
                        {{--</button>--}}
                        {{--@elsenotmobile--}}
                        {{--@if(mb_strlen($number) > 4 && str_contains($number, '9715'))--}}
                            {{--<button class="small ui purple button track_chat" onclick="pages.landing.property.whatsapp.__init({{json_encode($data_for_datalayer)}});">--}}
                                {{--@notmobile--}}
                                {{--<i class="whatsapp icon"></i>--}}
                                {{--@elsenotmobile--}}
                                {{--@endnotmobile--}}
                                {{--{{__( 'page.card.actions.whatsapp' ) }}--}}
                            {{--</button>--}}
                        {{--@else--}}
                            {{--<button class="small ui purple button track_email" onclick="pages.landing.property.email.__init({{json_encode($data_for_datalayer)}});">--}}
                                {{--@notmobile--}}
                                {{--<i class="envelope icon"></i>--}}
                                {{--@elsenotmobile--}}
                                {{--@endnotmobile--}}
                                {{--{{__( 'page.card.actions.email' ) }}--}}
                            {{--</button>--}}
                        {{--@endif--}}
                        {{--@endnotmobile--}}
                    {{--</div>--}}

                    <div data-_id="{{array_get($property,'_id','')}}">
                        {{-- <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button> --}}
                        {{-- <button class="small ui purple button track_callback" onclick="pages.landing.property.callback.__init({{json_encode($data_for_datalayer)}});"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button> --}}
                        {{-- @notmobile --}}
                        <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i></button>
                        {{-- @elsenotmobile
                        @if(mb_strlen($number) > 4 && str_contains($number, '9715')) --}}
                        <button class="small ui purple button track_email" onclick="pages.landing.property.email.__init({{json_encode($data_for_datalayer)}});"><i class="envelope icon"></i></button>
                        <button class="small ui purple button track_chat" onclick="pages.landing.property.whatsapp.__init({{json_encode($data_for_datalayer)}});"><i class="whatsapp icon"></i></button>
                        {{-- @else --}}
                        {{-- @endif
                        @endnotmobile --}}
                        {{-- <p>{{array_get($property,'ref_no','')}}</p> --}}
                    </div>



                    {{-- <div data-_id="{{array_get($property,'_id','')}}" class="ui buttons">
                        <button class="small ui purple button ">Enquire Now</button>
                        <button class="small ui button "><i class="whatsapp icon"></i>WhatsApp</button>
                    </div>
                    <div>
                        <p>{{array_get($property,'ref_no','')}}</p>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
