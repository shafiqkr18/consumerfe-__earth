<div id="guide_me_property_card_template" style="display:none;">
    <div class="column p_c">
        <div class="ui segment">
            <div>
                <div class="jss370">

                    <div class="jss263">
                        <a href="%property.web_link%" rel="nofollow">
                            <img alt="Zoom Property" data-type="listing" onerror="this.onerror=null;this.src='%property.images.original:0:1%';" data-second="%property.images.original:0:1%" class="f_p" data-lazy="{{cloudfront_asset()}}%property._id%_0_mobile" src="" />
                        </a>
                    </div>

                    <div>
                        <div>
                            <div><a href="%property.web_link%" rel="nofollow">%property.writeup.title%</a></div>
                            <div><a href="%property.web_link%" rel="nofollow">%property.type% %property.rent_buy% </a></div>
                            <div><div><span>{{__( 'page.global.aed' ) }}</span> %property.price%</div></div>
                        </div>
                        {{--<div>--}}
                            {{--<i class="bed icon"></i> %property.bedroom%--}}
                            {{--<i class="bath icon"></i> %property.bathroom%--}}
                            {{--<i class="square icon"></i> %property.dimension.builtup_area% {{__( 'page.global.sqft' )}}--}}
                        {{--</div>--}}
                        <div>
                            %property.building%
                            %property.area%,
                            %property.city%
                        </div>
                    </div>

                    <div></div>

                    <div>
                        <div class="ui buttons" data-agent_contact_name="%property.agent.contact.name%" data-agency_contact_name="%property.agent.agency.contact.name%" data-ref_no="%property.ref_no%" data-agent_contact_phone="%property.agent.contact.phone%" data-_id="%property._id%">
                            <button class="small ui purple button track_call" id="t_call_similar_%property._id%">{{__( 'page.card.actions.call' ) }}</button>
                            <button class="small ui purple button track_callback" id="t_callback_similar_%property._id%">{{__( 'page.card.actions.call_back' ) }}</button>
                            <button class="small ui purple button track_chat" id="t_chat_similar_%property._id%">{{__( 'page.card.actions.whatsapp' ) }}</button>
                        </div>
                        <div>
                            <p>%property.ref_no%</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
