<a class="card" href="{{route('new-project-details', ['lng' => \App::getLocale(), 'id' => array_get($project,'_id','')])}}" data-coordinates_lat="{{array_get($project,'coordinates.lat','')}}" data-coordinates_lng="{{array_get($project,'coordinates.lng','')}}">
    <div class="image">
        <img class="f_p" data-type="listing" data-second="{{array_get($project,'image.banner')}}" src="{{array_get($project,'image.portal.mobile.0')}}" alt="{{$seo_image_alt}}" />
        <div class="layer"></div>
        @php
         $percent = array_has($project,'completion.percentage') ? array_get($project,'completion.percentage') : 0;
        @endphp
        @if ($percent !== 0 && $percent !== 100)
            <div class="ui green tiny progress" data-percent="{{$percent}}">
                <div class="bar">
                <div class="progress"></div>
                </div>
                <div class="label">{{__( 'page.form.placeholders.completion_status' )}}</div>
            </div>
        @elseif ($percent == 100 || array_get($project,'status') == 'ready_to_move_in')
            <div class="move-in">
                <span><i class="check icon"></i></span>
                <div class="label">{{__( 'page.project.listings.move_in' )}}</div>
            </div>
        @endif
        @if(array_get($project,'featured.status',false))
            <span class="featured">{{strtolower(__( 'page.global.featured'))}}</span>
        @endif
        {{-- Share --}}
        <!-- <div class="share_{{array_get($project,'_id','')}} share_btn" onclick="helpers.share.__init();">
            <span><i class="share alternate icon"></i></span>
            <div class="sharethis-inline-share-buttons"></div>
        </div> -->
    </div>

    <div class="content">
        <div class="ui grid">
            <div class="twelve wide column">
                <div>
                    @if(!empty(array_get($project,'pricing.starting',0)))
                        <span>
                            {{__( 'page.global.from' )}}
                            {{ ($locale !== 'en' && !empty(array_get($project,'pricing.currency_'.$locale,'')) ) ? array_get($project,'pricing.currency_'.$locale,'') : array_get($project,'pricing.currency','') }}
                        </span>
                        <span>
                            {{number_format(array_get($project,'pricing.starting',0))}}
                        </span>
                    @endif
                </div>
                <div>
                    {{ ($locale !== 'en' && array_get($project,'writeup.title_'.$locale,'') != '') ? array_get($project,'writeup.title_'.$locale) : array_get($project,'writeup.title',__( 'page.global.default_project_name' )) }}
                </div>
                <div>
                    <span>{{array_get($project,'unit.property_type','').' '. __( 'page.global.for' ).' '. __( 'page.guideme.sale' )}}</span>
                </div>
                <div>
                    {{ ($locale !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get($project,'area','')), $locale)) ? __( 'data.area.'.snake_case_custom(array_get($project,'area',''))) : array_get($project,'area','') }},
                    {{ ($locale !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get($project,'city','')), $locale)) ? __( 'data.city.'.snake_case_custom(array_get($project,'city',''))) : array_get($project,'city','') }},
                    {{ ($locale !== 'en' && Lang::has('data.country.'.snake_case_custom(array_get($project,'country','')), $locale)) ? __( 'data.country.'.snake_case_custom(array_get($project,'country',''))) : array_get($project,'country','') }}
                </div>
            </div>
            @php
                $developer_logo    =   array_get( explode('@', array_get($project,'developer.contact.email')), '1', '');
            @endphp
            <div class="four wide column">
                <img class="f_p" data-type="listing" data-second="{{array_get($project,'developer.contact.logo')}}" src="{{cdn_asset('agency/logo/'.$developer_logo)}}" alt="{{$seo_image_alt}}" />
            </div>
        </div>
    </div>
</a>
