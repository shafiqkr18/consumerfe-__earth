@php
    //$picture    =   empty(array_get($agent,'contact.picture','')) || is_null(array_get($agent,'contact.picture') || array_get($agent,'contact.picture') == 'default') ?  'https://s3.ap-south-1.amazonaws.com/zp-uae-profile-images/'.urlencode(array_get($agent,'contact.email')) : array_get($agent,'contact.picture');
    $picture    =   env( 'PROFILE_S3_URL' ).urlencode(strtolower(array_get($agent,'contact.email','')));
    $track      =   array_get($agent,'agency.settings.lead.call_tracking.track',false);
    $number     =   $track ? array_get($agent,'agency.settings.lead.call_tracking.phone') : array_get($agent,'contact.phone','');
    $number     =   $number == '' ? array_get($agent,'agency.contact.phone','') : $number;
    if($number[0] == 0){
        $number     =   substr($number,1);
    }
    $number =   str_replace('971971','971','+971'.$number);
    array_set($agent, 'number', $number);

    $select_fields          =   config('data.datalayer.fields.agent');
    $data_for_datalayer     =   array_dot_only($agent,$select_fields);
@endphp
<div class="column">
    <div class="ui grid agent_jss370">
        <div class="five wide column agent_jss263">
            <a href="{{route('agent-finder-details', ['lng'=>\App::getLocale(),'name'=>kebab_case(array_get($agent,'contact.name')), 'id'=>array_get($agent,'_id')])}}">
                <img alt="{{$seo_image_alt}}" class="f_p ui bordered image" data-type="listing" data-second="{{ cdn_asset('assets/img/default/no-agent-img.png') }}" src="{{$picture}}">
            </a>
        </div>
        <div class="seven wide column">
            <div>
                <a href="{{route('agent-finder-details', ['lng'=>\App::getLocale(),'name'=>kebab_case(array_get($agent,'contact.name')), 'id'=>array_get($agent,'_id')])}}">
                    {{array_get($agent,'contact.name')}}
                </a>
            </div>
            <div>
                <a href="{{route('agent-finder-details', ['lng'=>\App::getLocale(),'name'=>kebab_case(array_get($agent,'contact.name')), 'id'=>array_get($agent,'_id')])}}">
                    {{array_get($agent,'agency.contact.name')}}
                </a>
            </div>
            <div>
                @php
                $view_all_params =  ['lng' => \App::getLocale(), 'rent_buy' => 'rent', 'residential_commercial' => 'residential', 'properties-for-rent' ];
                @endphp
                <a href="{{route('search-results-page',$view_all_params).'?agent_id='.array_get($agent,'_id')}}">{{ __( 'page.agent.details.listed_property' ) }}</a>
            </div>
        </div>
        <div class="four wide column">
            <img class="f_p" data-type="listing" alt="{{$seo_image_alt}}" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}"
            src="{{ cdn_asset('agency/logo/'.urlencode(array_get(explode('@',array_get($agent,'agency.contact.email')),1))) }}" />
        </div>
        <div class="sixteen wide column">
            <div data-_id="{{array_get($agent,'_id','')}}">
                <button class="small ui purple button" onclick="pages.agent.listings.call.__init({{json_encode($data_for_datalayer)}});">
                    <i class="phone volume icon"></i>
                    @notmobile
                        {{__( 'page.card.actions.call' ) }}
                    @endnotmobile
                </button>
                <button class="small ui purple button" onclick="pages.agent.listings.email.__init({{json_encode($data_for_datalayer)}});">
                    <i class="envelope outline icon"></i>
                    @notmobile
                        {{__( 'page.card.actions.email' ) }}
                    @endnotmobile
                </button>
                @notmobile
                @elsenotmobile
                    {{-- @if(mb_strlen($number) > 4 && str_contains($number, '9715')) --}}
                        <button class="small ui purple button" onclick="pages.agent.listings.whatsapp.__init({{json_encode($data_for_datalayer)}});">
                            <i class="whatsapp icon"></i>
                        </button>
                    {{-- @endif --}}
                @endnotmobile
            </div>
        </div>
    </div>
</div>
