@php
$developer_logo    =   array_get( explode('@', array_get($project,'developer.contact.email')), '1', '');
$location          =   ($locale !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get($project,'area','')), $locale)) ? __( 'data.area.'.snake_case_custom(array_get($project,'area',''))) : array_get($project,'area','').',';
$location          .=  ($locale !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get($project,'city','')), $locale)) ? __( 'data.city.'.snake_case_custom(array_get($project,'city',''))) : array_get($project,'city','').',';
$location          .=  ($locale !== 'en' && Lang::has('data.country.'.snake_case_custom(array_get($project,'country','')), $locale)) ? __( 'data.country.'.snake_case_custom(array_get($project,'country',''))) : array_get($project,'country','');
@endphp
<div class="ui twelve wide column reveal">
    <a href="{{route('new-project-details', ['lng' => \App::getLocale(), 'id' => array_get($project,'_id','')])}}" data-coordinates_lat="{{array_get($project,'coordinates.lat','')}}" data-coordinates_lng="{{array_get($project,'coordinates.lng','')}}">
        <div class="visible content">
            <img class="ui small image f_p" data-type="listing" data-second="{{array_get($project,'image.banner')}}" src="{{array_get($project,'image.portal.mobile.0')}}" alt="{{$seo_image_alt}}" />
            <div class="layer"></div>
            <div>
                <img class="f_p" data-type="listing" data-second="{{array_get($project,'developer.contact.logo')}}" src="{{cdn_asset('agency/logo/'.$developer_logo)}}" alt="{{$seo_image_alt}}" />
            </div>
            <span>{{strtolower(__( 'page.global.featured'))}}</span>
            <h5>{{ ($locale !== 'en' && array_get($project,'name_'.$locale,'') != '') ? array_get($project,'name_'.$locale) : array_get($project,'name',__( 'page.global.default_project_name' )) }}</h5>
            <p>
                {{ str_limit($location, $limit = 35, $end = '...') }}
            </p>
        </div>
    </a>
</div>