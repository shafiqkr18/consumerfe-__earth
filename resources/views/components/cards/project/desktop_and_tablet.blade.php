@php
    $project      = array_get($project,'data.0',[]);
    $developer_logo =   array_get( explode('@', array_get($project,'developer.contact.email','@')), '1', '');
    $select_fields          =   config('data.datalayer.fields.project');
    $data_for_datalayer     =   array_dot_only($project,$select_fields);
    array_set($data_for_datalayer,'subsource','desktop');
@endphp
<div id="sponsored">
    <div class="ui segment">
        <div>
            <div class="jss370">
                <div class="jss263">
                    <a href="#">
                        <img class="f_p" data-type="listing" data-second="{{array_get($project,'image.banner')}}" src="{{array_get($project,'image.portal.mobile.0')}}" alt="{{$seo_image_alt}}" />
                    </a>
                    <span><img  class="f_p" alt="" data-type="listing" src="{{env('ASSET_URL').'assets/img/home/sponsored.png'}}" />{{strtolower(__( 'page.global.sponsored'))}}</span>
                </div>
                <div>
                    <div>
                        <h4>
                            @if(!empty(array_get($project,'pricing.starting',0)))
                            {{__( 'page.project.starting_from' )}}
                            {{ ($locale !== 'en' && !empty(array_get($project,'pricing.currency_'.$locale,'')) ) ? array_get($project,'pricing.currency_'.$locale,'') : array_get($project,'pricing.currency','') }}
                            {{number_format(array_get($project,'pricing.starting',0))}}
                            @endif
                        </h4>
                    </div>
                    <div>
                        <span>
                            {{ ( $locale !== 'en' && array_get($project,'writeup.title_'.$locale,'') != '' ) ? array_get($project,'writeup.title_'.$locale) : array_get($project,'writeup.title','') }}
                            {{__( 'page.project.details.by' )}}
                            {{ ( $locale !== 'en' && array_get($project,'developer.contact.name_'.$locale,'') != '' ) ? array_get($project,'developer.contact.name_'.$locale) : array_get($project,'developer.contact.name','') }}
                        </span>
                    </div>
                    <div>
                    <span>{{array_get($project,'pricing.payment_plans.0','')}}</span>
                    </div>
                    <div>
                        <img class="f_p" data-type="listing" data-second="{{array_get($project,'developer.contact.logo')}}" src="{{cdn_asset('agency/logo/'.$developer_logo)}}" alt="{{$seo_image_alt}}" />
                    </div>

                    <div>
                        <button class="small ui purple basic button track_call" onclick="pages.projects.details.call.__init({{json_encode($data_for_datalayer)}});"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                        @notmobile
                        <button class="small ui purple basic button track_email" onclick="pages.projects.details.email.__init({{json_encode($data_for_datalayer)}});"><i class="envelope outline icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                        @elsenotmobile
                            <button class="small ui purple basic button track_chat" onclick="pages.projects.details.whatsapp.__init({{json_encode($data_for_datalayer)}});"><i class="whatsapp icon"></i>{{__( 'page.card.actions.whatsapp' ) }}</button>
                        @endnotmobile
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
