@isset($alerts)
  @if( !empty(array_get($alerts, 'data', [])))
  @foreach(array_get($alerts, 'data') as $alert)
    <div class="ui grid card fluid">
        <div class="row">
            <div class="fourteen wide column">
                <div>
                  <div>
                      Properties {{ !empty(array_get($alert, 'data.suburb', '')) ? 'in '.array_get($alert, 'data.suburb') : ''}} {{ !empty(array_get($alert, 'data.rent_buy', '')) ? 'for '.array_get($alert, 'data.rent_buy') : ''}}
                  </div>
                  <div>
                      <span>AED </span>{{array_get($alert, 'data.price.min', '')}} - {{array_get($alert, 'data.price.max', '')}}
                  </div>
                  <div>
                      Frequency {{ title_case(array_get($alert, 'frequency', ''))}}
                  </div>
                </div>
                <div><a href=""></a></div>
            </div>
            <div class="two wide column">
                <i class="trash icon" data-_frequency="{{array_get($alert, 'frequency', '')}}" data-_id="{{array_get( $alert, '_id', '' )}}" onclick="user.property.alert.unsubscribe.alert(this);"></i>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="no_listing">
        <img src="{{ cdn_asset( 'assets/img/no_listing/email_alert.png' ) }}" alt="Zoom Property" />
        <div> {{__( 'page.sections.dashboard.not_alerts' ) }} </div>
        <div class="ui grid">
            <div class="sixteen wide column">
                <button type="button" name="button"> <a href="javascript:void(0)" onclick="user.property.alert.__init(this);">{{__( 'page.form.button.set_alert' ) }} </a></button>
            </div>
        </div>
    </div>
    @endif
@endisset
