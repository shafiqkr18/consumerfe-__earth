NOT IN USE

<div id="property_card_template">
    <div class="column p_c dashboard_card">
        <div class="ui segment">
            <div>
                <div class="jss370">

                    <div class="jss263">
                        <a href="%property.web_link%" rel="nofollow">
                            <img alt="Zoom Property" data-type="listing" data-second="%property.images.original:0:1%" class="f_p" src="{{ cloudfront_asset() }}%property._id%_0_desktop" alt="..." />
                        </a>
                        <div>
                            <img class="f_p" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" src="%agency.logo%" />
                        </div>
                    </div>

                    <div>
                        <div>
                            @notmobile
                                <i data-_uid="%_id%" data-_id="%property._id%" onclick="user.property.favourite.__init(this);" class="heart outline icon track_favourite"></i>
                            @elsenotmobile
                                <i data-_uid="%_id%" data-_id="%property._id%" onclick="user.property.favourite.__init(this, true);" class="heart outline icon track_favourite"></i>
                            @endnotmobile
                        </div>
                        <div dir="ltr"><a href="%property.web_link%" rel="nofollow">%property.writeup.title%</a></div>
                        <div><a href="%property.web_link%" rel="nofollow">%property.type% %property.rent_buy% </a></div>
                        <div>
                            <i class="bed icon"></i> %property.bedroom%
                            <i class="bath icon"></i> %property.bathroom%
                            <i class="square icon"></i> %property.dimension.builtup_area%
                        </div>
                        <div>
                            %property.building%
                            %property.area%,
                            %property.city%
                        </div>
                        <div data-agent_contact_name="%property.agent.contact.name%" data-agency_contact_name="%property.agent.agency.contact.name%" data-ref_no="%property.ref_no%" data-agent_contact_phone="%property.agent.contact.phone%" data-_id="%property._id%">
                            <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init(this);"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                            <button class="small ui purple button track_callback" onclick="pages.landing.property.callback.__init(this);"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button>
                            @notmobile
                            <button class="small ui purple button track_email" onclick="pages.landing.property.email.__init(this);"><i class="envelope icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                            @elsenotmobile
                            <button class="small ui purple button track_chat" onclick="pages.landing.property.whatsapp.__init(this);"><i class="whatsapp icon"></i>{{__( 'page.card.actions.whatsapp' ) }}</button>
                            @endnotmobile
                        </div>
                    </div>

                    <div>
                        <div>
                            <div><span>{{__( 'page.global.aed' ) }}</span> %property.price%</div>
                        </div>
                        <p>%property.ref_no%</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
