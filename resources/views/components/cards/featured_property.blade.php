@php
    $property_title			=	( !empty( array_get($property,'writeup.title','') ) ) ? strtolower( preg_replace('/-+/', '-', preg_replace( '/[^a-zA-Z0-9\-]/', '-', trim( array_get($property,'writeup.title','') ) ) ) ) : '';
@endphp
<div class="column">
    <div class="ui segment" data-coordinates_lat="{{array_get($property,'coordinates.lat','')}}" data-coordinates_lng="{{array_get($property,'coordinates.lng','')}}">
        <div>
            <div class="jss370">
                <div class="jss263">
                    <a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">
                        <img class="f_p" src="{{array_get($property,'image','')}}" alt="Zoom Property">
                    </a>
                    <div>
                        @php
                            $agency_logo    =   array_get( explode('@', array_get($property,'agent.agency.contact.email')), '1', '');
                        @endphp
                        <img class="f_p" data-type="listing" data-second="{{cdn_asset('assets/img/default/no-img-available.jpg')}}" src="{{cdn_asset('agency/logo/'.$agency_logo)}}" />
                    </div>
                </div>
                <div>
                    <div>
                        <i data-ref_no="{{array_get($property,'ref_no','')}}" onclick="user.property.favourite.__init(this);" class="heart icon track_favourite"></i>
                    </div>
                    <div dir="ltr"><a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">{{array_get($property,'writeup.title','')}}</a></div>
                    <div><a href="{{route('property-details-page', ['lng'=>\App::getLocale(),'rent_buy'=>strtolower(kebab_case(array_get($property,'rent_buy',''))),'residential_commercial'=>strtolower(array_get($property,'residential_commercial','')),'name'=>$property_title,'id'=>array_get($property,'_id','')]) }}">
                      {{ (\App::getLocale() !== 'en' && Lang::has('data.type.'.snake_case_custom(array_get( $property, 'type', '' )), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $property, 'type', '' ))) : array_get( $property, 'type', '' ) }}
                      {{ (\App::getLocale() !== 'en' && Lang::has('data.rent_buy.for_'.snake_case_custom(array_get( $property, 'rent_buy', '' )), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom(array_get( $property, 'rent_buy', '' ))) : 'for ' . array_get( $property, 'rent_buy', '' ) }}
                    </a></div>
                    <div>
                        @if(array_get( $property, 'residential_commercial' ) == "Commercial" && array_get( $property, 'bedroom' ) >= 1 || array_get( $property, 'residential_commercial' ) == "Residential")
                        <i class="bed icon"></i> {{ strtolower( trans_choice(  __( 'page.global.bed', [ 'bed' => array_get($property,'bedroom','') ] ) , array_get($property,'bedroom','') ) )}}
                        @endif
                        @if(array_get( $property, 'bathroom' ) !== 0)
                        <i class="bath icon"></i> {{ strtolower( trans_choice(  __( 'page.global.bath', [ 'bath' => array_get($property,'bathroom','') ] ), array_get($property,'bathroom','') ) )}}
                        @endif
                        @if(array_get( $property, 'dimension.builtup_area') !== 0)
                        <i class="square icon"></i> {{array_get($property,'dimension.builtup_area','')}} {{__( 'page.global.sqft' )}}
                        @endif
                    </div>
                    <div>
                      @if(array_get($property,'building','') != '')
                        {{ (\App::getLocale() !== 'en' && Lang::has('data.building.'.snake_case_custom(array_get( $property, 'building', '' )), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $property, 'building', '' ))) : array_get( $property, 'building', '' ) }},
                        @endif
                        {{ (\App::getLocale() !== 'en' && Lang::has('data.area.'.snake_case_custom(array_get( $property, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $property, 'area', '' ))) : array_get( $property, 'area', '' ) }},
                        {{ (\App::getLocale() !== 'en' && Lang::has('data.city.'.snake_case_custom(array_get( $property, 'city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $property, 'city', '' ))) : array_get( $property, 'city', '' ) }}
                    </div>
                    <div data-agent_contact_name="{{array_get($property,'agent.contact.name','')}}" data-agency_contact_name="{{array_get($property,'agent.agency.contact.name','')}}" data-ref_no="{{array_get($property,'ref_no','')}}" data-_id="{{array_get($property,'_id','')}}" data-agent_contact_phone="{{array_get($property,'agent.contact.phone','')}}">
                        <button class="small ui purple button track_call" onclick="pages.landing.property.call.__init(this);"><i class="phone volume icon"></i>{{__( 'page.card.actions.call' ) }}</button>
                        <button class="small ui purple button track_callback" onclick="pages.landing.property.callback.__init(this);"><i class="phone icon"></i>{{__( 'page.card.actions.call_back' ) }}</button>
                        @notmobile
                        <button class="small ui purple button track_email" onclick="pages.landing.property.email.__init(this);"><i class="envelope icon"></i>{{__( 'page.card.actions.email' ) }}</button>
                        @elsenotmobile
                        <button class="small ui purple button track_chat" onclick="pages.landing.property.whatsapp.__init(this);"><i class="whatsapp icon"></i>{{__( 'page.card.actions.whatsapp' ) }}</button>
                        @endnotmobile
                    </div>
                </div>
                <div>
                    <div>
                        <div><span>{{__( 'page.global.aed' ) }}</span> {{number_format(array_get($property,'price',''))}}</div>
                    </div>
                    <p>{{array_get($property,'ref_no','')}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
