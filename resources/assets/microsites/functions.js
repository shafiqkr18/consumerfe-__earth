jQuery(document).ready(function( $ ){

    var mapLat  = $('#map_canvas').data('lat'),
        mapLng  = $('#map_canvas').data('lng'),
        p_id    = $('#projects_details').data('id'),
        p_name  = $('#projects_details').data('name');

    var device  = ( isMobileDevice() === true ) ? 'mobile' : 'desktop';
    var isRTL   = (locale == 'ar') ? true : false;

    function initMap() {
        var uluru = {lat: mapLat, lng: mapLng};
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 14,
            center: uluru,
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    initMap();

    var phoneField = document.querySelector("#customer-phone");
    var iti = intlTelInput(phoneField, {
        initialCountry: "ae",
        separateDialCode: true,
        utilsScript: cdnUrl + 'assets/dependencies/utils.js'
    });

    $('#bookingForm').on( 'submit', function(e){
        e.preventDefault();
        if( iti.isValidNumber() ) {
            var name        =   $('#customer-name').val(),
                email       =   $('#customer-email').val(),
                phone       =   $('#customer-phone').val(),
                interest    =   $('#customer-interest').val();
            var data = {
                action: 'email',
                source: 'consumer',
                subsource: device,
                message: "Hi, I am interested in a " + interest + " bedroom apartment in " + p_name + ". Please contact me. Thank you.",
                user: {
                    contact: {
                        name: name,
                        email: email,
                        phone: phone
                    }
                },
                project: {
                    _id: p_id
                }
            };
            run_ajax( data );
        } else {
            $('#toast_microsite').html('Please enter a valid mobile no.');
            $('#toast_microsite').fadeIn();
            $("#toast_microsite").css('visibility', 'visible');
            setTimeout(function(){
                $('#toast_microsite').fadeOut();
            }, 3000);
        }
    });

    $('.slides').slick({
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        cssEase: 'linear',
        rtl: isRTL
    });

    $('.ui.video').video();

    function init_slider(){
        $('.layouts').each(function(){
            $(this).slick({
                arrows: true,
                dots: false,
                autoplay: false,
                autoplaySpeed: 2000,
                rtl: isRTL
            });
        });
    }

    init_slider();

    $('.menu .item').tab({
        onLoad: function(){
            $('.layouts').slick('unslick');
            init_slider();
        }
    });

    $('.projects-tabs').slick({
        arrows: true,
        dots: false,
        slidesToShow: 5,
        rtl: isRTL,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.call-lead').on('click', function(e){
        e.preventDefault();
        var phone = $(this).find('span').data('phone'),
            userData = JSON.parse(localStorage.getItem('user_o'));
        $(this).find('span').html(phone);
        var data = {
            action: 'call',
            source: 'consumer',
            subsource: device,
            message: 'Hi, I found your Project on zoom property. Please contact me. Thank you.',
            project: {
                _id: p_id
            }
        };
        if( userData == null )  {
            data.user = {
                contact: {
                    name: 'anonymous',
                    email: 'anonymous@domain.com'
                }
            };
        } else {
            data.user = {
                contact: {
                    name: userData.name,
                    email: userData.email
                }
            };
        }
        if( device === 'mobile' ) {
            window.location.href = 'tel:'+phone;
        }
        dataLayer.push({ event: 'track_call', data: data });
        dataLayer.push({ event: 'track_call_submit', data: data });
        run_ajax( data );
    });

    function run_ajax( data ){

        $.ajax({
            method: 'post',
            url: baseApiUrl + "lead/project",
            data: data,
            async: false,
            headers: { 'x-api-key': 'NDaqOxp7IY3SZA2csTNoZ4V9jzzET3J43f32BOav' },
            beforeSend: function( xhr ){
                if( data.action === 'email' ) {
                    $('#loader').html('<i class="spinner icon"></i> Submitting ...');
                }
            },
            success:function(response){
                if( data.action === 'call' ){
                    dataLayer.push({ event: 'track_call_submit', data: data });
                } else if( data.action === 'email' ){
                    $('#loader').html('');
                    $('#toast_microsite').html('Your Request has been sent! someone will get in touch soon.');
                    $('#toast_microsite').fadeIn();
                    $("#toast_microsite").css('visibility', 'visible');
                    setTimeout(function(){
                        $('#toast_microsite').fadeOut();
                    }, 3000);
                    dataLayer.push({ event: 'track_email_submit', data: data });
                    document.getElementById("bookingForm").reset();
                }
            },
            error:function (error) {
                console.log( 'Something went wrong! Try again. ' + error );
            }
        });

    }

    function isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    }

});
