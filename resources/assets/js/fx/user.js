var user = {
    __init:   function(){
        $(".user_dashboard").sidebar('setting', 'transition', 'overlay');
        setTimeout(function(){ $(".pusher").css({height:'auto'}); }, 2000);
    },
    newsletter: {
        __init: function(trigger){
            if ( !_.isEmpty($( "#newsletter_email" ).val()) && /\S+@\S+\.\S+/.test($( "#newsletter_email" ).val()) ) {
                // Subscribe to newsletter
                email      =   $( "#newsletter_email" ).val();
                user.newsletter.subscribe(email);
                $(trigger).html('Subscribed!');
            }
            else{
                helpers.toast.__init( _.get(lang_static, 'toast.error_email_b') );
            }
        },
        subscribe: function(email){
            axios.post(baseApiUrl + 'user/newsletter/subscribe/'+email,[]).then(function (response) {
                $("#newsletter_email").val('');
                helpers.toast.__init( _.get(lang_static, 'toast.success_newsletter_subscribed') );
            }).catch(function (error) {
            });
        }
    },
    property: {
        unfavourite: {
            __init: function(trigger){
                var state   =   helpers.user.state.is_logged_in();

                // #1
                if(state[0]){
                    $(trigger).addClass('loading');
                    // #2
                    var modal_id    =   $(trigger).attr("data-_uid"),
                    user_id         =   _.get(state[1],'id','');
                    url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;
                    // #3
                    axios.delete(url).then(function (response) {

                        $(trigger).removeClass('favourite');
                        $(trigger).removeClass('loading');
                        $(trigger).addClass('outline');
                        // $(trigger).closest(".dashboard_card").remove();
                        $(trigger).removeAttr("data-_uid");

                    }).catch(function (error) {});
                } else {

                    if(mobile === true){
                        pages.signin.modal.__init();
                    } else {
                        helpers.trigger.modal.__init('signin_register', 'fade up');
                    }

                }
            },
            search: function(trigger) {
                var state   =   helpers.user.state.is_logged_in();

                $(trigger).addClass('loading');

                var modal_id    =   $(trigger).attr("data-_id"),
                user_id         =   _.get(state[1],'id','');
                url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;

                axios.delete(url).then(function (response) {
                    user.property.favourite.property.searches();
                }).catch(function (error) {});

            },
            property: function(trigger) {

                var state   =   helpers.user.state.is_logged_in();

                $(trigger).addClass('loading');

                var modal_id    =   $(trigger).attr("data-_id"),
                user_id         =   _.get(state[1],'id','');
                url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;

                axios.delete(url).then(function (response) {
                    user.property.favourite.property.properties();
                }).catch(function (error) {});

            }
        },
        favourite: {
            __init: function(trigger, mobile){

                var state   =   helpers.user.state.is_logged_in();

                // #1
                if(state[0]){

                    $(trigger).addClass('loading');

                    // Check if favourite or unfavourite
                    if( $(trigger).attr("data-_uid") ){
                        // Unfavourite
                        user.property.unfavourite.__init(trigger);

                    } else {

                        // #2
                        var modal_id    =   $(trigger).attr("data-_id"),
                        user_id         =   _.get(state[1],'id','');
                        url             =   baseApiUrl + 'user/' + user_id + '/favourite/property/' + modal_id;
                        // #3
                        axios.post(url).then(function (response) {
                            $(trigger).removeClass("outline").addClass("favourite");
                            $(trigger).removeClass('loading');
                            $(trigger).attr("data-_uid", response.data._id);
                        }).catch(function (error) {
                            $(trigger).removeClass('loading');
                        });

                    }

                } else {

                    if(mobile === true){

                        pages.signin.modal.__init();

                    } else {

                        helpers.trigger.modal.__init('signin_register', 'fade up');

                    }

                }

            },
            search: {
                __init: function(trigger, is_mobile){
                    /**
                    * trigger = this
                    * 1.   Check if user_logged else show login modal
                    * 2.   Build payload URL
                    * 3.   Call Api
                    */

                    // #1
                    if(helpers.user.loggedin.__init('user')){
                        //#3
                        var user_ = JSON.parse(helpers.store.read('user_o'));
                        var user_id = _.get(user_,'id');
                        var payload = {};

                        var previous_search = search.property.regular.deconstruct.url(window.location.href.replace(baseUrl,''));

                        if(previous_search){
                            payload = previous_search
                        } else {
                            payload = helpers.store.stringify({
                                rent_buy: 'Sale',
                                residential_commercial: ['Residential']
                            });
                        }

                        axios.post( baseApiUrl + "user/"+ user_id +"/favourite/property", helpers.clean.junk(payload)).then(function (response) {
                            helpers.toast.__init( _.get(lang_static, 'toast.success_search_saved') );
                        }).catch(function (error) {
                        });

                    } else {

                        if(is_mobile === 'mobile'){

                            pages.signin.modal.__init();

                        } else {

                            helpers.trigger.modal.__init('signin_register', 'fade up');
                        }
                        //#2

                    }
                }
            },
            property: {
                properties: function() {

                    var state   =   helpers.user.state.is_logged_in();

                    if(state[0]){

                        var payload = {};
                        payload.subtype = ['model'];

                        var url = baseUrl+"users/"+_.get(state[1],'id','-')+"/favourite/property?query=" + JSON.stringify(helpers.clean.junk(payload));

                        axios.get(url).then( function( response ) {

                            $('#favourite_properties').html(response.data);

                            helpers.lazy_load();

                        }).catch(function (error) {});

                    }

                },
                searches: function(){
                    var state   =   helpers.user.state.is_logged_in();

                    var payload = {
                        subtype : ['search']

                    }
                    var url = baseUrl+"users/"+_.get(state[1],'id','-')+"/favourite/property?query=" + JSON.stringify(payload);

                    axios.get(url).then( function( response ) {

                        $('#favourite_searches').html(response.data);

                    }).catch(function (error) {
                    });

                }
            }
        },
        alert: {
            __init: function(trigger, is_mobile) {

                if(helpers.user.loggedin.__init('user')){

                    $('#property_alert_rent_buy').dropdown({
                        values: pages.landing.dropdown.data.rent_buy,
                        placeholder: 'Rent or Buy',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.rent_buy = [];
                                lead.property.alert.store_variables.rent_buy.push(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            user.property.alert.rent_buy(value);
                        }
                    });

                    $('#property_alert_price_min').dropdown({
                        values: pages.landing.dropdown.data.price.sale,
                        placeholder: 'Min Price',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && (lead.property.alert.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.price.min = parseInt(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if ($('#property_alert_rent_buy').dropdown('get text') === 'Buy' || $('#property_alert_rent_buy').dropdown('get text') === 'Commercial Buy') {
                                var rent_type = pages.landing.dropdown.data.price.sale
                            } else {
                                var rent_type = pages.landing.dropdown.data.price.rent
                            }
                            pages.landing.dropdown.callback.from(value, '#property_alert_price_max', rent_type, 'price');
                        }
                    });
                    $('#property_alert_price_min').dropdown('set selected', '100000');

                    $('#property_alert_price_max').dropdown({
                        values: pages.landing.dropdown.data.price.sale,
                        placeholder: 'Max Price',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && (lead.property.alert.store_variables.price.max != parseInt(value)) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.price.max = parseInt(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if ($('#property_alert_rent_buy').dropdown('get text') === 'Buy' || $('#property_alert_rent_buy').dropdown('get text') === 'Commercial Buy') {
                                var rent_type = pages.landing.dropdown.data.price.sale
                            } else {
                                var rent_type = pages.landing.dropdown.data.price.rent
                            }
                            pages.landing.dropdown.callback.to(value, '#property_alert_price_min', rent_type, 'price');
                        }
                    });
                    $('#property_alert_price_max').dropdown('set selected', '1000000');

                    setTimeout(function() {
                        $('#property_alert_suburb').dropdown({
                            values: pages.agent.dropdown.data.areas,
                            placeholder: 'Areas',
                            maxSelections: 1,
                            action: function(text, value, element) {
                                $(this).dropdown('set selected', value);
                                areas = value.split(',');
                                locations = areas[areas.length - 1];
                                lead.property.alert.store_variables.area.push(locations);
                                lead.property.alert.store_variables.area = [_.uniq(lead.property.alert.store_variables.area).pop()];
                            }
                        });
                    }, 500);

                    $('#property_alert_frequency').dropdown({
                        values: pages.landing.dropdown.data.alert.frequency,
                        placeholder: 'Frequency',
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value)) {
                                lead.property.alert.store_variables.frequency = [];
                                lead.property.alert.store_variables.frequency.push(value);
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                        }
                    });
                    $('#property_alert_frequency').dropdown('set selected', 'Weekly');

                    $('.ui.modal.property_alert').modal('setting', 'transition', 'horizontal flip').modal('show');

                } else {

                    if(is_mobile === 'mobile'){

                        pages.signin.modal.__init();

                    } else {

                        helpers.trigger.modal.__init('signin_register', 'fade up');
                    }

                }

            },
            rent_buy: function(value) {

                if (value === 'Sale' || value === 'Commercial-Sale') {

                    $('#property_alert_price_min, #property_alert_price_max').empty();

                    $.each(pages.landing.dropdown.data.price.sale, function(i, item) {
                        $('#property_alert_price_min, #property_alert_price_max').append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    $('#property_alert_price_min, #property_alert_price_max').dropdown('clear');

                } else if (value === 'Rent' || value === 'Commercial-Rent' || value === 'Short-term-Rent') {

                    $('#property_alert_price_min, #property_alert_price_max').empty();

                    $.each(pages.landing.dropdown.data.price.rent, function(i, item) {
                        $('#property_alert_price_min, #property_alert_price_max').append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    $('#property_alert_price_min, #property_alert_price_max').dropdown('clear');

                }
            },
            property: {
                properties: function() {

                    var state   =   helpers.user.state.is_logged_in();

                    if(state[0]){

                        var payload = {};
                        payload.frequency = ['daily', 'monthly', 'weekly', 'yearly'];

                        var url = baseUrl+"users/"+_.get(state[1],'id','-')+"/alert/property?query=" + JSON.stringify(helpers.clean.junk(payload));

                        axios.get(url).then( function( response ) {

                            $('#user_alerts').html( response.data);
                        }).catch(function (error) {});

                    }

                }
            },
            unsubscribe: {
                alert: function(trigger){
                    var state   =   helpers.user.state.is_logged_in();

                    $(trigger).addClass('loading');

                    var frequency       =   $(trigger).attr("data-_frequency");

                    var payload = {};
                    payload.frequency = [frequency];

                    var modal_id    =   $(trigger).attr("data-_id"),
                    user_id         =   _.get(state[1],'id','');
                    url             =   baseApiUrl + 'user/' + user_id + '/alert/property/' + modal_id + '?query=' + JSON.stringify(helpers.clean.junk(payload));

                    axios.delete(url).then(function (response) {
                        user.property.alert.property.properties();
                    }).catch(function (error) {});

                }
            }
        }
    },
    auth: {
        login: {
            __init: function(trigger){

                $(trigger).addClass('loading');

                //#1    User login API call
                let data = {
                    email: $('#modal_signin').find('input[name=email]').val(),
                    password: $('#modal_signin').find('input[name=password]').val()
                }

                axios.post(baseApiUrl + 'user/auth/login', data).then(function (response) {
                    $('#signin_error').html('');
                    //1# init user dashboard
                    $('.item').tab();
                    $('.ui.form .error.message').hide();

                    //2# prepare object for user data in localstorage
                    var user_data   = user.session.data(response);
                    var data        = helpers.store.stringify(user_data);
                    var encrypt     = helpers.crypto.encrypt(data);

                    //3# write user data to localstorage
                    helpers.store.write('user', encrypt);
                    helpers.store.write('user_o', JSON.stringify(user_data));

                    // Initiate Login State
                    eval('pages.'+page+'.state()');
                    //4# close modal
                    $('.ui.modal.signin_register').modal('hide');
                    $('.ui.modal.mobile_signin_register').modal('hide');

                    helpers.toast.__init( _.get(lang_static, 'toast.success_signed_in') );
                    $(trigger).removeClass('loading');

                }).catch(function (error) {
                    $("#modal_signin").trigger('reset');
                    $('.ui.form .error.message').show();
                    var errs    =   error.response.data.errors;
                    var errors  =   '';
                    for(var i in errs){
                        errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                    }
                    $('#signin_error').html('<div class="ui error message" style="display:block">'+errors+'</div>');
                    $('#signin_error').html(error.response.data.message);
                    $(trigger).removeClass('loading');

                });
            }
        },
        logout: {
            __init: function(type){

                if(type === 'mobile'){
                    $('.user_dashboard').sidebar('toggle');
                } else {
                    $('.right.vertical.sidebar').sidebar('toggle');
                }
                axios.get(baseApiUrl + 'user/auth/logout').then(function (response) {
                    helpers.depopulate.modals();
                }).catch(function (error) {});
                helpers.store.clear();
                eval('pages.'+page+'.state()');

            }
        },
        register: {
            __init: function(trigger){

                $(trigger).addClass('loading');

                let data = {
                    name: $('#modal_register').find('input[name=full_name]').val(),
                    email: $('#modal_register').find('input[name=email]').val(),
                    password: $('#modal_register').find('input[name=password]').val(),
                    password_confirmation: $('#modal_register').find('input[name=confirm_password]').val()
                }

                axios.post(baseApiUrl + 'user/auth/register', data).then(function (response) {

                    $('#register_error').html('');

                    let payload = {
                        email: data.email,
                        password: data.password
                    }

                    axios.post(baseApiUrl + 'user/auth/login', payload).then(function (response) {
                        //1# init user dashboard
                        $('.item').tab();

                        //2# prepare object for user data in localstorage
                        var user_data   = user.session.data(response);
                        var data        = helpers.store.stringify(user_data);
                        var encrypt     = helpers.crypto.encrypt(data);

                        //3# write user data to localstorage
                        helpers.store.write('user', encrypt);
                        helpers.store.write('user_o', JSON.stringify(user_data));

                        // Initiate Login State
                        eval('pages.'+page+'.state()');
                        //4# close modal
                        $('.ui.modal.signin_register').modal('hide');
                        $('.ui.modal.mobile_signin_register').modal('hide');

                        $(trigger).removeClass('loading');
                        helpers.toast.__init( _.get(lang_static, 'toast.success_registered') );


                    }).catch(function (error) {
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                        $(trigger).removeClass('loading');
                    });

                }).catch(function (error) {
                    $("#modal_register").trigger('reset');
                    if(error.response.data.errors){
                        // error.response.data.errors.email
                        var errs    =   error.response.data.errors;
                        var errors  =   '';
                        for(var i in errs){
                            errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                        }
                        $('#register_error').html('<div class="ui error message">'+errors+'</div>');
                        // $('#register_error').html('Please enter all required fields.');
                    }else{
                        $('#register_error').html(error.response.data.message);
                    }
                    $(trigger).removeClass('loading');
                });

            }
        },
        password: {
            forgot: function(trigger){

                $(trigger).addClass('loading');

                var url = baseApiUrl + 'user/auth/forgot';
                let data = {
                    email: $('#modal_forgot_password').find('input[name=email]').val()
                }

                axios.post(url, data).then(function (response) {

                    $('#password_error').html('');

                    //4# close modal
                    $('.ui.modal.signin_register').modal('hide');
                    $('.ui.modal.mobile_signin_register').modal('hide');

                    helpers.toast.__init( _.get(lang_static, 'toast.success_email_sent') );
                    $(trigger).removeClass('loading');

                }).catch(function (error) {
                    $("#modal_forgot_password").trigger('reset');
                    if(error.response.data.errors){
                        $('.ui.form .error.message').show();
                        var errs    =   error.response.data.errors;
                        var errors  =   '';
                        for(var i in errs){
                            errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                        }
                        $('#password_error').html(errors);
                    }else{
                        $('#password_error').html(error.response.data.message);
                    }
                    $(trigger).removeClass('loading');
                });

            }
        },
        social: {
            google: {
                __init: function(){

                    var signinWin;

                    signinWin = window.open(envUrl + "login/google", "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);

                    signinWin.focus();

                    $('.ui.modal.signin_register').modal('setting', 'transition', 'fade up').modal('hide');

                    return false;

                }
            },
            facebook: {
                __init: function(){

                    var signinWin;

                    signinWin = window.open(envUrl + "login/facebook", "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 500 + ",top=" + 200);

                    signinWin.focus();

                    $('.ui.modal.signin_register').modal('setting', 'transition', 'fade up').modal('hide');

                    return false;

                }
            },
            callback: {
                __init	: 		function( data ){

                    axios.post( baseApiUrl + "user/auth/social", {
                        provider       :   data.provider,
                        provider_id    :   data.provider_id,
                        name           :   data.name,
                        email          :   data.email,
                        avatar         :   data.avatar
                    }).then(function ( response ) {

                        //1 init user dashboard
                        $('.item').tab();

                        //2 prepare object for user data in localstorage
                        var user_data   = {
                            email: response.data.contact.email,
                            name: response.data.contact.name,
                            id: response.data._id,
                            login: true
                        }

                        //3 close window
                        window.close();

                        //4 write data
                        window.opener.user.auth.social.opener.__init(user_data);

                    }).catch( function( error ) {
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    });

                }
            },
            opener:{
                __init: function(user_data){

                    var data        = helpers.store.stringify(user_data);
                    var encrypt     = helpers.crypto.encrypt(data);

                    helpers.store.write('user', encrypt);
                    helpers.store.write('user_o', JSON.stringify(user_data));

                    // Initiate Login State
                    eval('pages.'+page+'.state()');

                    helpers.toast.__init( _.get(lang_static, 'toast.success_signed_in') );

                }
            }
        }
    },
    dashboard : {
        trigger: {
            __init      :   function(el){
                $('.user_dashboard').sidebar('toggle');
                $('.user_dashboard').sidebar('setting', 'onShow', function(){
                    $(".pusher").css({height:'100%',overflow:'hidden'});
                });
                $('.user_dashboard').sidebar('setting', 'onHidden', function(){
                    $(".pusher").css({height:'auto',overflow:'visible'});
                });
            }
        },
        password: {
            update: function(trigger){

                $(trigger).addClass('loading');

                var password = $('#change_password_password > input').val(),
                confirmPassword = $("#change_password_password_confirmation > input").val();

                $('#change_password_error').html('');

                var state   =   helpers.user.state.is_logged_in();

                var url = baseApiUrl + 'user/' + _.get(state[1],'id','') + '/auth/change';

                var payload = {
                    old_password: $('#change_password_old_password > input').val(),
                    password: password,
                    password_confirmation: confirmPassword
                }

                axios.put(url, payload).then(function(response){

                    $(trigger).removeClass('loading');
                    $('#change_password_error').html('');
                    $('#change_password_cancel_btn').addClass('hide');
                    $('#change_password_edit_btn').removeClass('hide');
                    $('#change_password_update_btn').addClass('disabled');

                    helpers.toast_dashboard.__init( _.get(lang_static, 'toast.success_password_changed') );

                    $('#change_password_old_password > input').val('');
                    $('#change_password_password > input').val('');
                    $("#change_password_password_confirmation > input").val('');

                }).catch(function (error) {
                    $(trigger).removeClass('loading');

                    if(error.response.data.errors){
                        // error.response.data.errors.email
                        var errs    =   error.response.data.errors;
                        var errors  =   '';
                        for(var i in errs){
                            errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                        }
                        $('#change_password_error').html('<div class="ui error message" style="display:block">'+errors+'</div>');
                    }else{
                        $('#change_password_error').html(error.response.data.message);
                    }
                });

            },
            cancel: function(trigger){
                $('#change_password_error').html('');

                $('#change_password_edit_btn').removeClass('hide');
                $('#change_password_cancel_btn').addClass('hide');
                $('#change_password_cancel_btn').addClass('disabled');
                $('#change_password_update_btn').addClass('disabled');

                $('#change_password_old_password').addClass('disabled');
                $('#change_password_password').addClass('disabled');
                $('#change_password_password_confirmation').addClass('disabled');

                $('#change_password_old_password > input').val('');
                $('#change_password_password > input').val('');
                $("#change_password_password_confirmation > input").val('');
            },
            edit: function(trigger){
                $('#change_password_edit_btn').addClass('hide');
                $('#change_password_cancel_btn').removeClass('hide');
                $('#change_password_cancel_btn').removeClass('disabled');
                $('#change_password_update_btn').removeClass('disabled');

                $('#change_password_old_password').removeClass('disabled');
                $('#change_password_password').removeClass('disabled');
                $('#change_password_password_confirmation').removeClass('disabled');

                //on enter script
                var input = document.getElementById('change_password_password_confirmation');

                input.addEventListener("keyup", function(event) {
                    event.preventDefault();
                    if (event.keyCode === 13) {
                        document.getElementById('change_password_update_btn').click();
                    }
                });

            }
        },
        profile: {
            update: function(trigger){

                $(trigger).addClass('loading');

                var state   =   helpers.user.state.is_logged_in();

                var url = baseApiUrl + 'user/' + _.get(state[1],'id','');

                var payload = {
                    contact:{
                        name: $('#profile_update_name > input').val(),
                        phone: $('#profile_update_phone > input').val(),
                        nationality: $('#profile_update_nationality > input').val()
                    }
                }

                axios.put(url, payload).then(function(response){

                    $(trigger).removeClass('loading');
                    $('#profile_cancel_btn').addClass('hide');
                    $('#profile_edit_btn').removeClass('hide');
                    $('#profile_update_btn').addClass('disabled');

                    //1# prepare object for user data in localstorage
                    var user_data   =   user.session.data(response);
                    var data        =   helpers.store.stringify(user_data);
                    var encrypt     =   helpers.crypto.encrypt(data);

                    //2# write user data to localstorage
                    helpers.store.write('user', encrypt);
                    helpers.store.write('user_o', JSON.stringify(user_data));

                    user.dashboard.profile.cancel(trigger);

                }).catch(function (error) {
                    helpers.toast_dashboard.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');

                    $('#profile_cancel_btn').addClass('hide');
                    $('#profile_edit_btn').removeClass('hide');
                    $('#profile_update_name').addClass('disabled');
                    $('#profile_update_phone').addClass('disabled');
                    $('#profile_update_nationality').addClass('disabled');
                    $('#profile_update_btn').addClass('disabled');

                });

                helpers.toast_dashboard.__init('Profile updated successfully');

            },
            cancel: function(trigger){
                $('#profile_edit_btn').removeClass('hide');
                $('#profile_cancel_btn').addClass('hide');
                $('#profile_cancel_btn').addClass('disabled');
                $('#profile_update_btn').addClass('disabled');

                $('#profile_update_name').addClass('disabled');
                $('#profile_update_phone').addClass('disabled');
                $('#profile_update_nationality').addClass('disabled');
            },
            edit: function(trigger){
                $('#profile_edit_btn').addClass('hide');
                $('#profile_cancel_btn').removeClass('hide');
                $('#profile_cancel_btn').removeClass('disabled');
                $('#profile_update_btn').removeClass('disabled');

                $('#profile_update_name').removeClass('disabled');
                $('#profile_update_phone').removeClass('disabled');
                $('#profile_update_nationality').removeClass('disabled');
            }
        }
    },
    session:{
        data: function(response){
            var user_data   = {
                email: response.data.contact.email,
                name: response.data.contact.name,
                number: response.data.contact.phone,
                nationality: response.data.contact.nationality,
                id: response.data._id,
                login: true
            }
            return user_data;
        }
    }
}

$('.item').tab();
