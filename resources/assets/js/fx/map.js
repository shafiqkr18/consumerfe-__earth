/**
 *
 *      Google Maps related f(x)s
 *
 */

var g_map   =   {
    center              :   {
		lat				   : 	25.2048,
		lng				   : 	55.2708
	},
    map_container       :   '',
    results_container   :   '',
    allMarkers		    :	[],
    allResults		    :	[],
    mapTypesList	    :	[ 'mosque', 'hospital', 'school', 'gym', 'bank', 'shopping_mall', 'parking', 'park', 'gas_station' ],
    selectedIcon        :   envUrl + 'assets/img/map/property-marker.png',
    propertyIcon        :   envUrl + 'assets/img/map/property-marker.png',
    map 		        : 	{
        map                 :   null,
        invoke              :   function( container = g_map.map_container ){
            g_map.map.map   =   new google.maps.Map( document.getElementById( container ), {
                center				  : 		g_map.center,
                scrollwheel			  : 		false,
                zoom				  : 		(container.indexOf( 'results-map' ) > -1 )? 14 : 15
            });
            return g_map.map.map;
        },
        change_center       :   function( lat, lng ){
            g_map.map.map.setCenter( new google.maps.LatLng( lat, lng ) )
        }
    },
    //Property List Card
    property_map_list		:	{
		invoke					:		function(){
            let mapListCard			=		document.getElementsByClassName( 'property_card_list_map' );
			for (let i = 0; i < mapListCard.length; i++) {
                mapListCard[i].addEventListener( 'mouseover', function() {
                    var lat = $(this).data( 'lat' ),
                        lng = $(this).data( 'lng' );
                    if( lat === 0 && lng === 0 ){
                        $( '#results-no-map' ).css( 'visibility', 'visible' );
                    }
                    else {
                        // Populate content of infowindow wrapper with div data
                        let propertyCard    	=  		this.querySelector( '.property_list' ),
                            propertyMarker;
                        if ( typeof propertyCard.id !== 'undefined' ){
                            $( '#infowindow-property-name' ).html( propertyCard.getAttribute( 'data-name' ) );
                            $( '#infowindow-property-location' ).html( propertyCard.getAttribute( 'data-location' ) );
                            $( '#infowindow-property-price' ).html( propertyCard.getAttribute( 'data-price' ) );
                            $( '#infowindow-property-bed' ).html( propertyCard.getAttribute( 'data-bedroom' ) );
                            $( '#infowindow-property-bath' ).html( propertyCard.getAttribute( 'data-bathroom' ) );
                            $( '#infowindow-property-price' ).html( propertyCard.getAttribute( 'data-price' ) );
                            $( '#infowindow-property-area' ).html( propertyCard.getAttribute( 'data-area' ) );
                            $( '#infowindow-property-agent-name' ).html( propertyCard.getAttribute( 'data-agent' ) );
                            $( '#infowindow-property-agency-name' ).html( propertyCard.getAttribute( 'data-agency' ) );
                            g_map.infoWindow.infoWindow.setContent( $( '#infowindow-wrapper' ).html() );
                            for (let i = 0; i < g_map.allMarkers.length; i++) {
                                if( g_map.allMarkers[i].pid === propertyCard.getAttribute( 'id' ).split( '-' )[1] ){
                                    propertyMarker 	= g_map.allMarkers[i];
                                    g_map.allMarkers[i].setIcon(g_map.selectedIcon);
                                    g_map.infoWindow.infoWindow.open(g_g_map.map.map, propertyMarker);
                                    g_map.landmarks.highlight_in_list_of_results(propertyCard.id.split('-')[1], "");
                                }
                            }
                        }
                    }
                });

                mapListCard[i].addEventListener( 'mouseout', function() {
                    $( '#results-no-map' ).css( 'visibility', 'hidden' );
                    let propertyCard = this.querySelector( '.property_list' ),
                            propertyMarker;
                    g_map.infoWindow.infoWindow.setContent( "" );
                    for (let i = 0; i < g_map.allMarkers.length; i++) {
                        if (g_map.allMarkers[i].pid === propertyCard.getAttribute('id').split('-')[0]) {
                            g_map.allMarkers[i].setIcon( g_map.propertyIcon );
                            google.maps.event.addListener(g_map.infoWindow.infoWindow, 'closeclick', function () {
                                g_map.allMarkers[i].setIcon(g_map.propertyIcon);
                            });
                        }
                    }
                    g_map.infoWindow.infoWindow.close();
                });
            }
		}
	},

    marker 				:	function( location = g_map.center, map = g_map.map.map ){

        // init marker
        var marker  =   new google.maps.Marker({

            map					: 		map,
            animation			: 		google.maps.Animation.DROP,
            position			: 		{ lat: location.lat, lng: location.lng },
            icon: (location.hasOwnProperty('pid') || window.location.href.indexOf('search') > -1 )  ? g_map.propertyIcon : g_map.selectedIcon,
            pid                 :       location.pid

        });

        // event handling
        //
        // mouse over - show infowindow with DOM data

        if( marker.pid ){

            marker.addListener('click', function () {

                let self = this;

                for (let i = 0; i < g_map.allMarkers.length; i++) {

                    g_map.allMarkers[i].setIcon(g_map.propertyIcon);

                }

                google.maps.event.addListener(g_map.infoWindow.infoWindow, 'closeclick', function () {

                    self.setIcon(g_map.propertyIcon);

                });

                g_map.landmarks.highlight_in_list_of_results(self.pid);

                self.setIcon(g_map.selectedIcon);

                if (typeof self.pid !== 'undefined') {

                    // Populate content of infowindow wrapper with div data
                    var propertyCard = g_map.results_container.find('#list-' + self.pid + '-property-card'),
                        residential_commercial = _.startsWith( 'commercial', _.toLower(propertyCard.attr( 'data-rent-buy' ))) ? 'commercial' : 'residential';
                        rent_buy = _.toLower(propertyCard.attr( 'data-rent-buy' )) === 'buy' ? 'sale' : 'rent';
                        property_name = _.toLower(propertyCard.attr('data-name').split( ' ' ).join( '-' ).replace( /[$?|{}()]/g, '-' ).replace( /-+/g, '-' ));

                    $('#infowindow-property-name').html(propertyCard.attr('data-name'));
                    $('#infowindow-property-location').html(propertyCard.attr('data-location'));
                    $('#infowindow-property-bed').html(propertyCard.attr('data-bedroom'));
                    $('#infowindow-property-bath').html(propertyCard.attr('data-bathroom'));
                    $('#infowindow-property-price').html(propertyCard.attr('data-price'));
                    $('#infowindow-property-agent-name').html(propertyCard.attr('data-agent'));
                    $('#infowindow-property-agency-name').html(propertyCard.attr('data-agency'));

                    document.getElementById( 'infowindow-redirect' ).href = baseUrl + rent_buy +'/' + residential_commercial + '/' + property_name + '/' + self.pid;

                    g_map.infoWindow.infoWindow.setContent($('#infowindow-wrapper').html());

                    g_map.infoWindow.infoWindow.open(g_map.map.map, self);

                    g_map.landmarks.highlight_in_list_of_results(self.pid);

                }

            });

        }

		//Pushing marker to array
        g_map.allMarkers.push( marker );

        return marker;

    },

    places_service 	    : 	function( map = g_map.map.map ){

        return new google.maps.places.PlacesService( map );

    },

    infoWindow 			: 	{

        infoWindow          :   '',

        invoke              :   function(){

            g_map.infoWindow.infoWindow     =   new google.maps.InfoWindow();

            return g_map.infoWindow.infoWindow;

        }
    },

    landmarks           :   {

        unique_landmark_type    :   function( locationTypes, queryTypes ){
            for (var i = 0; i < locationTypes.length; i++) {
                for (var j = 0; j < queryTypes.length; j++) {
                    if ( queryTypes[j] === locationTypes[i] ){
                        return queryTypes[j];
                    }
                }
            }
        },

        create_landmark_markers     :   function ( places, display = false, type, map = g_map.map.map ) {
            for (var i = 0; i < places.length; i++) {
                var place 		= 	places[i],
                    marker 		= 	new google.maps.Marker({
                        id 					:		place.id,
                        name				:		place.name,
                        map					: 		map,
                        types				:		place.types,
                        filter 				:		g_map.landmarks.unique_landmark_type( place.types, g_map.mapTypesList ),
                        icon: cdnUrl + 'assets/img/map/map_markers/' + 'location-' + type +'.png',
                        position			: 		place.geometry.location,
                        visible             :       display,
                    });
                g_map.allMarkers.push( marker );
            }
        },

        locate_landmarks    :   function( center = g_map.center, landmarkTypes = g_map.mapTypesList, radius = 1500 ){
            for (let k = 0; k < landmarkTypes.length; k++ ){
                let eachLandmarkType = landmarkTypes[k];
                // Locate nearby landmarks in radius of 2KM
                g_map.places_service().nearbySearch({
                    location: center,
                    radius: radius,
                    types: [eachLandmarkType]
                }, function (results, status, pagination) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        g_map.landmarks.create_landmark_markers(results);
                        if (pagination.hasNextPage) {
                            pagination.nextPage();
                        }
                    }
                });

            }
        },

        remove_all_landmark_markers     :   function( markers ){

            for ( var i = 0; i < markers.length; i++ ) {

                markers[i].setMap( null );

            }

        },

        show_hide_markers       :   function( type, value ){
            if( value === true ) {
                g_map.places_service().nearbySearch({
                    location: g_map.center,
                    radius: 1500,
                    types: [type]
                }, function (results, status, pagination) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        g_map.landmarks.create_landmark_markers(results, value, type);
                        // if (pagination.hasNextPage) {
                        //     pagination.nextPage();
                        // }
                    }
                });
            } else {
                for (var i = 0; i < g_map.allMarkers.length; i++) {
                    if( g_map.allMarkers[i].filter === type ) {
                        g_map.allMarkers[i].setVisible(false);
                    }
                }
            }
        },

        highlight_in_list_of_results    :   function( pid, clickTrigger="marker" ){

            // remove previous highlights
            $( '#map-container .property-card-list' ).removeClass('animate');

			if( clickTrigger === "marker" ){

	            $('html, body').scrollTop($("#map-view").find('#' + pid + '-property-tile-list').offset().top - 240);

			}

            // Scale item
            g_map.results_container.find( '#' + pid + '-property-tile-list .property-card-list' ).addClass('animate');

        },

    },

    /**
     * Initialize the map
     * @param  {JSON} center    Center of the map [property id]
     * @param  {array} locations lat,lng
     * @param  {string} mapContainer Id of div in which to invoke the map
     * @param  {string} resultsContainer Id of div in which to hold the results
     * @return {[type]}           [description]
     */
    __init        :   function( center, locations, mapContainer, resultsContainer ){
        g_map.map_container         =   mapContainer;
        g_map.results_container     =   $( '#' + resultsContainer );
        // Identify center
        g_map.center                =   center;
        // Invoke map
        g_map.map.invoke();
        // Identify map center
        g_map.marker();
        g_map.landmarks.locate_landmarks(center);
        // Invoke locations
        if( locations.length > 0 ){
            for( var i = 0; i < locations.length; i++ ){
                g_map.marker( locations[ i ] );
            }
        }
        // Invoke Info Window
        g_map.infoWindow.invoke();
		//
		g_map.property_map_list.invoke();
		$( '.btn-directions' ).on( 'click', function() {
            let d   =   document.getElementById( 'property-details' ).getAttribute( 'data-dest' );
			window.open( 'https://www.google.com/maps/?q=' + d, '_blank' );
		})
    }
};
