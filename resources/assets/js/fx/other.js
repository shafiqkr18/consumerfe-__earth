var other = {
    __init  :   function(){
    },
    blog: {
        listings: function(){
            /**
             * 1.   call blogs api
             */
            axios.get( baseUrl+'blog/recent' ).then( function( response ) {
                $( '#blog_content' ).html( response.data );
                $('#blog_content').find('.blog_card').slice(2, 4).remove();
                $('.blog_card').addClass('column');

                var url = $('.blog_card>a').attr('href');
                $('.blog_card').append('<div class="read"><a href="'+ url + '" target="_blank">Read more <i class="long arrow alternate right icon"></i></a></div>');
                $('.blog_card>a').removeAttr('href').removeAttr('target');

                // $('.blog_card>a').removeAttr('href').removeAttr('target');
                // $('.blog_card').append("<div class='read'><a href=" + baseUrl + 'projects/uae' + " target='_blank'>Read more <i class='long arrow alternate right icon'></i></a></div>");
                $( "#blog_content:nth-child(1)" ).append( "<div class='col l4 m4 s4 column'><a><div style='background-image: url(" + cdnUrl + 'assets/img/banner/landing_new_project.png' + ");'></div><div>Checkout our new projects!</div></a><div class='read'><a href=" + baseUrl + 'projects/uae' + " target='_blank'>Read more <i class='long arrow alternate right icon'></i></a></div></div>" );
            }).catch( function( error ) {
            });
        }
    },
    ideal_property: {
        email: function(trigger, e){
            var url = baseApiUrl+'enquiry/ideal_property';
            var payload = {
                name: $('#ideal_property_name').val(),
                email: $('#ideal_property_email').val(),
                phone: $('#ideal_property_phone').val(),
                budget: $('#ideal_property_budget').val(),
                type: $('#ideal_property_type').val(),
                location: $('#ideal_property_location').val()
            }
            if( helpers.phonenumber.iti.isValidNumber() ) {
                $(trigger).addClass('loading');
                axios.post( url, payload ).then( function( response ) {
                    $(trigger).removeClass('loading');
                    if( response.data.error ) {
                        helpers.toast.__init( response.data.error );
                    } else {
                        dataLayer.push({ event: 'track_email_submit', data: payload });
                        $('.ui.modal.ideal_property').modal('hide');
                        helpers.toast.__init( response.data.success );
                    }
                }).catch( function( error ) {
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });
            } else {
                helpers.toast.__init( _.get(lang_static, 'toast.error_phone') );
            }
        }
    },
    custom_enquiry: {
        __init: function(trigger){

            $(trigger).addClass('loading');
            var temp = helpers.store.temp_data.read(),
            payload = temp.payload;

            payload.custom_form = temp.settings.custom_form;
            payload.custom_data = {
                    when: $('#modal_custom_enquiry_when').find('[name="when"]:checked').val(),
                    times: $('#modal_custom_enquiry_times').find('[name="times"]:checked').val(),
                    rooms: $('#modal_custom_enquiry_rooms').find('[name="rooms"]:checked').val(),
                    when_now: $('#modal_custom_enquiry_when_now').find('[name="when_now"]:checked').val(),
                    investor_job: $('#modal_custom_enquiry_investor_job').find('[name="investor_job"]:checked').val(),
                    where: $('#modal_custom_enquiry_where').find('[name="where"]:checked').val()
                };

            _.pull(temp, 'payload');
            var data_trigger = temp;

            helpers.store.temp_data.write = data_trigger;

            //1.
            var url = baseApiUrl + 'lead/project';
            //2.
            axios.post(url, payload).then(function (response) {
                helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                //Remove loading to the Button
                $('.ui.modal.custom_enquiry').modal('setting', 'transition', 'fade up').modal('hide');
                $(trigger).removeClass('loading');
                $('#project_lead').removeClass('loading');
                var project_data = helpers.store.temp_data.read();
                other.datalayer('project','email_submit', data_trigger);
            }).catch(function (error) {
                helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                //Remove loading to the Button
                $(trigger).removeClass('loading');
                $('.ui.modal.custom_enquiry').modal('setting', 'transition', 'fade up').modal('hide');
                $('#project_lead').removeClass('loading');
            });

        }
    },
    general_enquiry: {
      slide_context: function(){
          if($('.general_enquiry_container').hasClass("active")){
            $('.general_enquiry_container').removeClass('active');
            $('.general_enquiry_container').removeClass('popOut');
            $('.general_enquiry_container').addClass('slideIn');
            $('.general_enquiry_container').addClass('moveIn');
          }
          else{
            $('.general_enquiry_container').removeClass('moveIn');
            $('.general_enquiry_container').addClass('slideOut');
            $('.general_enquiry_container').addClass('popOut');
            $('.general_enquiry_container').addClass('active');
          }
      }
    },
    change_currency: function( trigger ){
        var value = $(trigger).children('option:selected').data('value'),
            currency = $(trigger).children('option:selected').data('currency');
        helpers.store.write( 'selectedCurrency', JSON.stringify( {currency: currency, value: value} ) );
        $( '.get_price' ).each( function( index ){
            var current_price = $(this).data('price'),
                new_price = new Intl.NumberFormat('en-AE', { maximumFractionDigits: 0 }).format( _.multiply(value,helpers.parseNumberCustom(current_price)) );
            $(this).find('.value').html( new_price );
            $(this).find('.currency').html( currency );
        } );
    },
    change_currency_mobile: function( currency, value ){
        var frmcurrency = currency.substr(currency.length - 3);
        helpers.store.write( 'selectedCurrency', JSON.stringify( {currency: frmcurrency, value: value} ) );
        $( '.get_price' ).each( function( index ){
            var current_price = $(this).data('price'),
                new_price = new Intl.NumberFormat('en-AE', { maximumFractionDigits: 0 }).format( _.multiply(value,helpers.parseNumberCustom(current_price)) );
            $(this).find('.value').html( new_price );
            $(this).find('.currency').html( frmcurrency );
            // only for tester push
        } );
    },
    update_currency: function(){
        var dataJSON = helpers.store.read( 'selectedCurrency' ),
            data = JSON.parse( dataJSON ),
            value = _.isNull(data) || _.isUndefined(data) ? '1' : data.value,
            currency = _.isNull(data) || _.isUndefined(data) ? 'AED' : data.currency;
        $('#currency_dropdown').children('option[data-currency='+currency+']').attr('selected','selected');
        $( '.get_price' ).each( function( index ){
            var current_price = $(this).data('price'),
                new_price = new Intl.NumberFormat('en-AE', { maximumFractionDigits: 0 }).format( _.multiply(value,helpers.parseNumberCustom(current_price)) );
            $(this).find('.value').html( new_price );
            $(this).find('.currency').html( currency );
        } );
    },
    datalayer: function(model,eventName, data){
        /** Default data */
        info = dataLayer[0];
        /** Data per model */
        switch(model) {
          case 'property':
            final_data = {
                //Website
                website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : _.get(data, 'residential_commercial') + '-' +_.get(data, 'rent_buy'),
                pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Product',
                device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,
                //Location
                loc_city_name          : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.get(data, 'city') : _.get(info, 'loc_city_name'),
                loc_neighbourhood_name : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.get(data, 'area') : _.get(info, 'loc_neighbourhood_name'),
                loc_name               : _.has(data, 'building') && !_.isEmpty(_.get(data, 'building')) ? _.get(data, 'building') : _.get(info, 'loc_name'),
                loc_breadcrumb         : ((_.has(data, 'city') && !_.isEmpty(_.get(data, 'city'))) || (_.has(data, 'area') && !_.isEmpty(_.get(data, 'area'))) || (_.has(data, 'building') && !_.isEmpty(_.get(data, 'building')))) ? _.snakeCase(_.toLower(_.get(data, 'city','')))+';'+_.snakeCase(_.toLower(_.get(data, 'area','')))+';'+_.snakeCase(_.toLower(_.get(data, 'building',''))) : _.get(info, 'loc_breadcrumb'),
                loc_city_id            : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.snakeCase(_.toLower(_.get(data, 'city'))) :_.get(info, 'loc_city_id'),
                loc_neighbourhood_id   : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.snakeCase(_.toLower(_.get(data, 'area'))) : _.get(info, 'loc_neighbourhood_id'),
                loc_id                 : _.has(data, 'building') && !_.isEmpty(_.get(data, 'building')) ? _.snakeCase(_.toLower(_.get(data, 'building'))) : _.get(info, 'loc_id'),
                //Property
                property_id            : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'property_id'),
                property_furnishing    : _.has(data, 'furnished') ? (_.get(data, 'furnished') == 0 ? 'Unfurnished' : (_.get(data, 'furnished') == 1 ? 'Furnished' : 'Partly-furnished')) : _.get(info, 'property_furnishing'),
                property_type          : _.has(data, 'type') && !_.isEmpty(_.get(data, 'type')) ? _.get(data, 'type') : _.get(info, 'property_type'),
                property_bed           : _.has(data, 'bedroom') ? _.get(data, 'bedroom') : _.get(info, 'property_bed'),
                property_area          : _.has(data, 'dimension.builtup_area') ? _.get(data, 'dimension.builtup_area') : _.get(info, 'property_area'),
                property_price         : _.has(data, 'price') ? _.get(data, 'price') : _.get(info, 'property_price'),
                property_bath          : _.has(data, 'bathroom') ? _.get(data, 'bathroom') : _.get(info, 'property_bath'),
                property_currency      : _.has(info, 'property_currency') ? _.get(info, 'property_currency') : 'AED',
                //Agent
                agent_id               : _.has(data, 'agent._id') ? _.get(data, 'agent._id') : _.get(info, 'agent_id'),
                agent_name             : _.has(data, 'agent.contact.name') ? _.get(data, 'agent.contact.name') : _.get(info, 'agent_name'),
                agency_id              : _.has(data, 'agent.agency._id') ? _.get(data, 'agent.agency._id') : _.get(info, 'agency_id'),
                agency_name            : _.has(data, 'agent.agency.contact.name') ? _.get(data, 'agent.agency.contact.name') : _.get(info, 'agency_name')
            }
            break;
          case 'project':
              final_data = {
                  //Website
                  website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : 'Projects',
                  pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Product',
                  device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                  language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,
                  //Location
                  loc_city_name          : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.get(data, 'city') : _.get(info, 'loc_city_name'),
                  loc_neighbourhood_name : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.get(data, 'area') : _.get(info, 'loc_neighbourhood_name'),
                  loc_breadcrumb         : ((_.has(data, 'city') && !_.isEmpty(_.get(data, 'city'))) || (_.has(data, 'area') && !_.isEmpty(_.get(data, 'area')))) ? _.snakeCase(_.toLower(_.get(data, 'city','')))+';'+_.snakeCase(_.toLower(_.get(data, 'area',''))) : _.get(info, 'loc_breadcrumb'),
                  loc_city_id            : _.has(data, 'city') && !_.isEmpty(_.get(data, 'city')) ? _.snakeCase(_.toLower(_.get(data, 'city'))) :_.get(info, 'loc_city_id'),
                  loc_neighbourhood_id   : _.has(data, 'area') && !_.isEmpty(_.get(data, 'area')) ? _.snakeCase(_.toLower(_.get(data, 'area'))) : _.get(info, 'loc_neighbourhood_id'),
                  //Project
                  project_id             : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'projecy_id'),
                  project_name           : _.has(data, 'name') && !_.isEmpty(_.get(data, 'name')) ? _.get(data, 'name') : _.get(info, 'project_name'),
                  project_price          : _.has(data, 'pricing.starting') ? _.get(data, 'pricing.starting') : _.get(info, 'project_price'),
                  project_type           : _.has(data, 'unit.property_type') ? _.get(data, 'unit.property_type') : _.get(info, 'project_type'),
                  project_currency       : _.has(info, 'project_currency') ? _.get(info, 'project_currency') : 'AED',
                  //Developer
                  developer_id               : _.has(data, 'developer._id') ? _.get(data, 'developer._id') : _.get(info, 'developer_id'),
                  developer_name             : _.has(data, 'developer.contact.name') ? _.get(data, 'developer.contact.name') : _.get(info, 'developer_name'),
              }
            break;
          case 'agent':
              final_data = {
                  //Website
                  website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : 'Agent Finder',
                  pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Community',
                  device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                  language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,

                  //Agent
                  agent_id               : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'agent_id'),
                  agent_name             : _.has(data, 'contact.name') && !_.isEmpty(_.get(data, 'contact.name')) ? _.get(data, 'contact.name') : _.get(info, 'agent_name'),
                  agency_id              : _.has(data, 'agency._id') ? _.get(data, 'agency._id') : _.get(info, 'agency_id'),
                  agency_name            : _.has(data, 'agency.contact.name') ? _.get(data, 'agency.contact.name') : _.get(info, 'agency_name'),
              }
            break;
          case 'service':
              final_data = {
                  //Website
                  website_section        : _.has(info, 'website_section') ? _.get(info, 'website_section') : 'Services',
                  pagetype               : _.has(info, 'pagetype') ? _.get(info, 'pagetype') : 'Services',
                  device_type            : _.has(info, 'device_type') ? _.get(info, 'device_type') : 'mobile',
                  language               : _.has(info, 'language') ? _.get(info, 'language') : selectedLanguage,

                  //Service
                  service_id             : _.has(data, '_id') ? _.get(data, '_id') : _.get(info, 'service_id'),
                  service_name           : _.has(data, 'contact.name') && !_.isEmpty(_.get(data, 'contact.name')) ? _.get(data, 'contact.name') : _.get(info, 'service_name'),
              }
            break;
        }
        //User
        let user = JSON.parse(helpers.store.read('user_o'));
        user === null ? final_data.user_status = 'Not Logged In' : final_data.user_status = 'Logged In';
        //Event
        final_data.event = eventName;
        dataLayer.push(final_data);
    },
    change_language: function( trigger ){
        var language = $(trigger).children("option:selected").val();
        $('#language').val(language);
        window.location.href = language;
    },
    update_language: function() {
        let url =   new URI( window.location.href );
        var lang = url.segment(2);
        if (lang == '' || lang == 'en') {
            $('#language').val('en');
        } else if (lang == 'ar') {
            $('#language').val('ar');    
        }
    }
}
