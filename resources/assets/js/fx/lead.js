var lead = {
    property: {
        store: {
            save : '',
            retrive: function(){
                return lead.property.store.save;
            }
        },
        details: {
            email: {
                __init: function(trigger){

                    //Add loading to the Button
                    $(trigger).addClass('loading');

                    var number = helpers.phonenumber.iti.getNumber();

                    var payload = {
                        action: 'email',
                        source: 'consumer',
                        subsource: 'desktop',
                        message: $('#property_details_email_textarea').val(),
                        property:{
                            _id: $(trigger).attr("data-_id")
                        },
                        user: {
                            contact: {
                                name: $('#property_details_email_name').val(),
                                email: $('#property_details_email_email').val(),
                                phone: number
                            }
                        }
                    }

                    var url = baseApiUrl + 'lead/property';

                    axios.post(url, payload).then(function (response) {
                        //Add loading to the Button
                        $(trigger).removeClass('loading');
                        $('.ui.modal.property_email').modal('hide');
                        helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                        other.datalayer('property','email_submit', payload);
                    }).catch(function (error) {
                        //Add loading to the Button
                        $(trigger).removeClass('loading');
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    });

                }
            }
        },
        call : {
            __init: function(trigger, mobile){

                payload = lead.property.store.retrive();

                var url = baseApiUrl + 'lead/property';

                axios.post(url, payload).then(function (response) {

                    if(mobile){
                        //gtm push on submit
                        var property_data = helpers.store.temp_data.read();
                        other.datalayer('porperty','call_submit', property_data);
                    }

                }).catch(function (error) {});

                if(mobile){
                    window.location.href = 'tel:'+$(trigger).html();
                }

            }
        },
        whatsapp : {
            __init: function(payload){

                var url = baseApiUrl + 'lead/property';

                axios.post(url, payload).then(function (response) {
                    var property_data = helpers.store.temp_data.read();
                    other.datalayer('property','whatsapp_submit', property_data);
                }).catch(function (error) {});

            }
        },
        email: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: $('#modal_property_email_textarea').val(),
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_property_email_name').val(),
                            email: $('#modal_property_email_email').val(),
                            phone: number
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/property';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_email').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();

                    other.datalayer('property','email_submit', property_data);

                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        callback: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber(),
                    user = JSON.parse(helpers.store.read('user_o'));

                var payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.property.callback.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.callback.payload_msg'), {reference: $(trigger).attr("data-ref_no")}) : 'Hi, I found your property with ref: ' + $(trigger).attr("data-ref_no") + ' on Zoom Property. Please contact me. Thank you.',
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_property_callback_name').val(),
                            email: _.has(user, 'email') ? user.email : 'anonymous@domain.com',
                            phone: number
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/property';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_callback').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();

                    other.datalayer('property','callback_submit', property_data);

                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        call_request_back: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber(),
                    user_o = JSON.parse(helpers.store.read('user_o'));

                let payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.property.callback.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.callback.payload_msg'), {reference: $(trigger).attr("data-ref_no")}) : 'Hi, I found your property with ref: ' + $(trigger).attr("data-ref_no") + ' on Zoom Property. Please contact me. Thank you.',
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_property_call_name').val(),
                            email: _.has(user_o, 'email') ? user_o.email : $('#modal_property_call_email').val(),
                            phone: number
                        }
                    }
                }

                //Create alert if send me latest property information and news is active
                if($('#modal_property_call_latest_info').is(':checked')){
                  user.newsletter.subscribe(payload.user.contact.email);
                }

                //1.
                var url = baseApiUrl + 'lead/property';
                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_callback').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();

                    other.datalayer('property','callback_submit', property_data);

                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        mortgage: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var work_number = parseInt($('#mortgage_user_work_phone').val());

                var number = helpers.phonenumber.iti.getNumber();

                var work_phone = work_number;


                var payload = {
                    emi:{
                        loan_amount: helpers.parseNumberCustom($( '#project_loan_amount' ).text()),
                        monthly: helpers.parseNumberCustom($( '#project_emi_monthly' ).text()),
                        down_payment: helpers.parseNumberCustom($( '#project_emi_down_payment' ).text()),
                        total_amount: helpers.parseNumberCustom($( '#project_emi_total_amount' ).text()),
                        total_interest: helpers.parseNumberCustom($( '#project_emi_total_interest' ).text()),
                        interest_rate: parseFloat($( '#emi_interest' ).val()),
                        term: helpers.parseNumberCustom($( '#project_emi_term' ).text())
                    },
                    property:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            first_name: $('#mortgage_user_name').val(),
                            last_name: $('#mortgage_user_name').val(),
                            email: $('#mortgage_user_email').val(),
                            mobile_number: number,
                            work_number: work_phone
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/property/mortgage';

                //2.
                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');

                    //gtm push on submit
                    var property_data = helpers.store.temp_data.read();
                    other.datalayer('mortgage_submit', property_data);
                    $('.ui.modal.property_mortgage').modal('hide');
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_mortgage').modal('hide');
                });

            }
        },
        request: {
            __init: function(trigger){

                var payload = {
                    user: {
                        contact: {
                            name: $('#property_request_name').val(),
                            email: $('#property_request_email').val(),
                            phone: $('#property_request_phone').val(),
                        }
                    },
                    message: $('#property_request_message').val(),
                    url: $(trigger).attr('data-url'),
                    title: $(trigger).attr('data-title')

                }
                var url = baseApiUrl + 'lead/property/listings';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $("#property_request_listings").trigger('reset');

                    //gtm push on submit
                    // var property_data = helpers.store.temp_data.read();
                    // dataLayer.push({ event: 'track_property_mortgage_submit', data: property_data });
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                });

            }
        },
        alert: {
            __init: function(trigger){

                $(trigger).addClass('loading');

                var payload = {
                    data: {
                        area: lead.property.alert.store_variables.area.toString(),
                        rent_buy: lead.property.alert.store_variables.rent_buy.toString(),
                        price: {
                            min: lead.property.alert.store_variables.price.min,
                            max: lead.property.alert.store_variables.price.max
                        }
                    },
                    consumer: {
                        name : $('#property_alert_name').val(),
                        email: $('#property_alert_email').val()
                    },
                    frequency: lead.property.alert.store_variables.frequency.toString()
                }
                var user = JSON.parse(helpers.store.read('user_o'));
                var url = baseApiUrl + 'user/' + user.id + '/alert/property';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_alert_created') );
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_alert').modal('hide');
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_alert').modal('hide');
                });


            },
            variables   :   function(){
                return {
                    area		       	:   [],
                    rent_buy	        :	[],
                    frequency           :   [],
                    price				:		{
                        min			    :		undefined,
                        max			    :		undefined,
                    }
                }
            }
        }
    },
    agent: {
        store: {
            save : '',
            retrive: function(){
                return lead.agent.store.save;
            }
        },
        call : {
            __init: function(trigger){

                payload = lead.agent.store.retrive();

                /**
                * 1.   build URL
                * 2.   Api call with payload and URL
                * 3.   Show toast on success or Error
                */

                var agent_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/agent';

                //2.
                axios.post(url, payload).then(function (response) {}).catch(function (error) {});

                if(trigger){
                    other.datalayer('agent','call_submit',agent_data);
                    window.location.href = 'tel:'+$(trigger).html();
                }

            }
        },
        callback: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.agent.call_back.payload_msg') ? _.get(lang_static, 'modals.agent.call_back.payload_msg') : 'Hi, I found your profile on Zoom Property. Please contact me. Thank you.',
                    agent:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            name: $('#modal_agent_callback_name').val(),
                            phone: number
                        }
                    }
                }

                var agent_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/agent';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.agent_callback').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    other.datalayer('agent','callback_submit', agent_data);
                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        email: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: $('#modal_agent_email_textarea').val(),
                    agent:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact : {
                            name: $('#modal_agent_email_name').val(),
                            email: $('#modal_agent_email_email').val(),
                            phone: number
                        }
                    }
                }

                var agent_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/agent';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.agent_email').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    other.datalayer('agent', 'email_submit', agent_data);
                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        },
        whatsapp : {
            __init: function(payload){

                var url = baseApiUrl + 'lead/agent';

                axios.post(url, payload).then(function (response) {
                    other.datalayer('agent', 'whatsapp_click', payload);
                }).catch(function (error) {});

            }
        }
    },
    project: {
        call : {
            __init: function(trigger){

                var user    =   JSON.parse(helpers.store.read('user_o'));

                $data_temp = helpers.store.temp_data.read();

                if(!_.isEmpty($data_temp) && !_.isUndefined($data_temp.developer))
                    data_trigger = $data_temp;

                var url     =   baseApiUrl + 'lead/project';

                var name = !_.isEmpty($(trigger).attr("data-project_name")) ? $(trigger).attr("data-project_name") : (!_.isEmpty(data_trigger.name) ? data_trigger.name : '');

                var payload = {
                    action: 'call',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : (!_.isEmpty(data_trigger.subsource) ? data_trigger.subsource : 'desktop'),
                    message: _.has(lang_static, 'modals.project.call.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.project.call.payload_msg'), {name: name}) : 'Hi, I found your Project ' + name + ' on zoom property. Please contact me. Thank you.',
                    project: {
                        _id: !_.isEmpty($(trigger).attr("data-project_id")) ? $(trigger).attr('data-project_id') : data_trigger._id
                    },
                    user: {
                        contact: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        }
                    }
                }

                if (payload.user.contact.phone.length === 0) {
                    delete payload.user.contact.phone
                }

                helpers.store.temp_data.write = payload;

                axios.post(url, payload).then(function (response) {}).catch(function (error) {});

                //gtm push on submit
                var project_data = helpers.store.temp_data.read();

                if(trigger){
                    other.datalayer('project','call_submit', project_data);
                    window.location.href = 'tel:'+$(trigger).html();
                }
            }
        },
        email: {
            __init: function(data_trigger){
                //Add loading to the Button
                $('#project_lead').addClass('loading');
                $data_temp = helpers.store.temp_data.read();
                if(_.isUndefined(data_trigger.developer) && !_.isEmpty($data_temp) && !_.isUndefined($data_temp.developer))
                    data_trigger = $data_temp;
                var number = helpers.phonenumber.iti.getNumber();

                var name = !_.isEmpty($('project_lead_name').attr("data-project_name")) ? $('project_lead_name').attr("data-project_name") : (!_.isEmpty(data_trigger.name) ? data_trigger.name : '');

                var msg = _.has(lang_static, 'modals.project.email.payload_msg') ? ' ' + helpers.trans_value(_.get(lang_static, 'modals.project.email.payload_msg'), {name: name}) : '';
                msg = !_.isUndefined($('#project_lead_textarea')) && !_.isEmpty($('#project_lead_textarea').val()) ? $('#project_lead_textarea').val() : (!_.isUndefined($('#modal_developer_email_textarea')) ? $('#modal_developer_email_textarea').val() : msg);

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty(data_trigger.subsource) ? data_trigger.subsource : 'desktop',
                    message: msg,
                    project:{
                        _id: data_trigger._id
                    },
                    user: {
                        contact: {
                            name: !_.isUndefined($('#project_lead_name').val()) && !_.isEmpty($('#project_lead_name').val()) ? $('#project_lead_name').val() : $('#modal_developer_email_name').val(),
                            email: !_.isUndefined($('#project_lead_email').val())&& !_.isEmpty($('#project_lead_email').val()) ? $('#project_lead_email').val() : $('#modal_developer_email_email').val(),
                            phone: number
                        }
                    }
                }
                if(data_trigger.settings.custom_form){
                    $('.ui.modal.custom_enquiry').modal({
                        onHide: function(){
                            $('#project_lead').removeClass('loading');
                        }
                    }).modal('show');
                    data_trigger.payload = payload;
                    helpers.store.temp_data.write = data_trigger;
                }
                else{
                    helpers.store.temp_data.write = data_trigger;

                    //1.
                    var url = baseApiUrl + 'lead/project';
                    //2.
                    axios.post(url, payload).then(function (response) {
                        helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                        //Remove loading to the Button
                        $('#project_lead').removeClass('loading');
                        var project_data = helpers.store.temp_data.read();
                        other.datalayer('project','email_submit', data_trigger);
                    }).catch(function (error) {
                        helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                        //Remove loading to the Button
                        $(trigger).removeClass('loading');
                    });
                }

            }
        },
        callback: {
            __init: function(trigger){
                //Add loading to the Button
                $('#project_lead').addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'callback',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: _.has(lang_static, 'modals.project.callback.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.project.callback.payload_msg'), {name: $('#pages_projects_details').attr('data-project_name')}) : 'Hi, I found your Project ' + $('#pages_projects_details').attr('data-project_name') + ' on zoom property. Please contact me. Thank you.',
                    project:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            name: $('#modal_developer_callback_name').val(),
                            phone: number
                        }
                    }
                }
                helpers.store.temp_data.write = payload;

                var url = baseApiUrl + 'lead/project';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    $('#project_lead').removeClass('loading');
                    $('.ui.modal.developer_callback').modal('setting', 'transition', 'fade up').modal('hide');

                    //gtm push on submit
                    var project_data = helpers.store.temp_data.read();

                    other.datalayer('project','callback_submit', project_data);

                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');
                    $('.ui.modal.developer_callback').modal('setting', 'transition', 'fade up').modal('hide');
                });

            }
        },
        whatsapp : {
            __init: function(payload){

                var url = baseApiUrl + 'lead/project';

                axios.post(url, payload).then(function (response) {
                    other.datalayer('project', 'whatsapp_click', payload);
                }).catch(function (error) {});

            }
        },
        mortgage: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var work_number = parseInt($('#mortgage_user_work_phone').val());

                var country = $('#modal_mortgage_email_country').attr('data-country-code');

                var work_phone = country + work_number;


                var payload = {
                    emi:{
                        loan_amount: helpers.parseNumberCustom($( '#project_loan_amount' ).text()),
                        monthly: helpers.parseNumberCustom($( '#project_emi_monthly' ).text()),
                        down_payment: helpers.parseNumberCustom($( '#project_emi_down_payment' ).text()),
                        total_amount: helpers.parseNumberCustom($( '#project_emi_total_amount' ).text()),
                        total_interest: helpers.parseNumberCustom($( '#project_emi_total_interest' ).text()),
                        interest_rate: parseFloat($( '#emi_interest' ).val()),
                        term: helpers.parseNumberCustom($( '#project_emi_term' ).text())
                    },
                    project:{
                        _id: $(trigger).attr("data-_id")
                    },
                    user: {
                        contact: {
                            first_name: $('#mortgage_user_name').val(),
                            last_name: $('#mortgage_user_name').val(),
                            email: $('#mortgage_user_email').val(),
                            mobile_number: number,
                            work_number: work_phone
                        }
                    }
                }

                //1.
                var url = baseApiUrl + 'lead/project/mortgage';

                //2.
                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');

                    //gtm push on submit
                    var project_data = helpers.store.temp_data.read();
                    other.datalayer('mortgage_submit', project_data);
                    $('.ui.modal.property_mortgage').modal('hide');
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.property_mortgage').modal('hide');
                });

            }
        },
        request: {
            __init: function(trigger){
                var payload = {
                    user: {
                        contact: {
                            name: $('#modal_developer_enquiry_name').val(),
                            email: $('#modal_developer_enquiry_email').val(),
                            phone: $('#modal_developer_enquiry_phone').val(),
                        }
                    },
                    message: $('#modal_developer_enquiry_message').val(),
                    url: $(trigger).attr('data-url'),
                    title: $(trigger).attr('data-title')

                }
                var url = baseApiUrl + 'lead/project/listings';

                axios.post(url, payload).then(function (response) {
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $("#modal_developer_enquiry").trigger('reset');
                    dataLayer.push({ event: 'track_email_submit'});
                }).catch(function (error) {
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                    $(trigger).removeClass('loading');
                });

            }
        }
    },
    services: {
        email: {
            __init: function(trigger){

                //Add loading to the Button
                $(trigger).addClass('loading');

                var number = helpers.phonenumber.iti.getNumber();

                var payload = {
                    action: 'email',
                    source: 'consumer',
                    subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                    message: $('#modal_services_contact_user_message').val(),
                    service: {
                        _id: $(trigger).attr("data-service_id")
                    },
                    user: {
                        contact: {
                            name: $('#modal_services_contact_user_name').val(),
                            email: $('#modal_services_contact_user_email').val(),
                            phone: number
                        }
                    }
                }

                var service_data = helpers.store.temp_data.read();

                //1.
                var url = baseApiUrl + 'lead/service';

                //2.
                axios.post(url, payload).then(function (response) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    $('.ui.modal.services_contact').modal('hide');
                    helpers.toast.__init( _.get(lang_static, 'toast.success_request_sent') );
                    other.datalayer('service','contact_submit', service_data);
                }).catch(function (error) {
                    //Remove loading to the Button
                    $(trigger).removeClass('loading');
                    helpers.toast.__init( _.get(lang_static, 'toast.error_default') );
                });

            }
        }
    },
    mortgage: {
        __init: function(trigger){
            if($(trigger).attr('data-model') == 'property'){
                return lead.property.mortgage.__init(trigger);
            } else {
                return lead.project.mortgage.__init(trigger);
            }
        }
    },
    preferences: {
        __init: function(trigger){

            //#1.- Add loading to the Button
            $(trigger).addClass('loading');
            //#2.- Getting phone number
            var number = helpers.phonenumber.iti.getNumber();
            //#3.- Building payload
            var locations   = $('#ge_location').val();
            var rent_buy_val = _.isUndefined($('#ge_rent_buy')) ? '' : $('#ge_rent_buy').val();
            //#3.1.- Building location payload
            locations  =   _.isUndefined(locations) ? [] : locations.split('-');
            var building = null, area = null, city = null, residential_commercial = null, rent_buy = null;
            if(locations.length === 1){
                city = _.get(locations,0);
            }
            else if(locations.length === 2){
                city = _.get(locations,1);
                area = _.get(locations,0);
            }
            else if(locations.length === 3){
                city     = _.get(locations,2);
                area     = _.get(locations,1);
                building = _.get(locations,0);
            }
            //#3.2.- Building Residential/Commercial rent_buy type payload
            if(!_.includes( rent_buy_val, '-' ) || (_.startsWith( _.lowerCase(rent_buy_val), 'short') ) ){
                residential_commercial = 'Residential';
                rent_buy = _.replace(rent_buy_val, /-/g, ' ');
            }
            else{
                residential_commercial = 'Commercial';
                rent_buy = rent_buy_val.split(/-(.+)/);
                rent_buy = _.replace(rent_buy[1], /-/g,' ');
            }
            var payload = {
                action: 'email',
                source: 'consumer',
                subsource: !_.isEmpty($(trigger).attr("data-subsource")) ? $(trigger).attr("data-subsource") : 'desktop',
                message: _.has(lang_static, 'modals.preference.contact.message') ? helpers.trans_value(_.get(lang_static, 'modals.preference.contact.message'), {location: _.join(locations, ',')}) : 'Hi, I am looking for a Property in ' + _.join(locations, ',') + '. Please contact me. Thank you.',
                user: {
                    contact : {
                        name: $('#ge_name').val(),
                        email: $('#ge_email').val(),
                        phone: number
                    }
                },
                preferences: {
                    bedroom: {
                        min: 0,
                        max: 0
                    },
                    bathroom: {
                        min: 0,
                        max: 0
                    },
                    price: {
                        min: 0,
                        max: 0
                    },
                    city: city,
                    type: $('#ge_type').val(),
                    rent_buy: rent_buy,
                    residential_commercial: residential_commercial,
                    area: _.isNull(area) ? area : [area],
                    building: _.isNull(building) ? building : [building]
                },
                property: {
                    city: city,
                    type: $('#ge_type').val(),
                    rent_buy: rent_buy,
                    residential_commercial: residential_commercial,
                    area: area,
                    building: building
                }
            }

            //4.- API call
            var url = baseApiUrl + 'lead/property';
            axios.post(url, payload).then(function (response) {

                //Remove loading to the Button
                $(trigger).removeClass('loading');

                helpers.toast.__init('Your Request has been sent! someone will get in touch soon.');
                // other.general_enquiry.slide_context();

                //gtm push on submit
                var property_data = payload;

                other.datalayer('property','email_submit', property_data);

            }).catch(function (error) {
                //Remove loading to the Button
                $(trigger).removeClass('loading');
                helpers.toast.__init('Something went wrong, Please try later');
            });
        }
    }
}

lead.property.alert.store_variables     =   new lead.property.alert.variables();
