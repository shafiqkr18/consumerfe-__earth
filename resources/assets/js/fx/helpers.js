var helpers  =   {
    phonenumber: {
        init: function(id, separateDialCode=true){
            helpers.phonenumber.input = document.querySelector("#" + id);

            if(helpers.phonenumber.iti){
                //Destroy if already init
                helpers.phonenumber.iti.destroy();
            }

            helpers.phonenumber.iti = intlTelInput(helpers.phonenumber.input, {
                initialCountry: "ae",
                separateDialCode: separateDialCode
            });
        }
    },
    redirect: function(type){
        search.property.regular.store_variables     =   new search.property.regular.variables();
        search.property.regular.store_variables.rent_buy = [];
        search.property.regular.store_variables.rent_buy.push(type);

        //Check for Property Type from Dropdown
        if($('#property_type').dropdown('get value') && $('#property_type').dropdown('get value').length){
            search.property.regular.store_variables.type = [];
            search.property.regular.store_variables.type.push($('#property_type').dropdown('get value'));
        }

        search.property.regular.__init();
    },
    unshift: function(data){
        var temp_value = '';
        data.bathroom_min = data.bathroom.slice(0);
        data.bathroom_max = data.bathroom.slice(0);
        data.bedroom_min = data.bedroom.slice(0);
        data.bedroom_max = data.bedroom.slice(0);
        data.area_min = data.area.slice(0);
        data.area_max = data.area.slice(0);
        data.price_sale_min = data.price.sale.slice(0);
        data.price_sale_max = data.price.sale.slice(0);
        data.price_rent_min = data.price.rent.slice(0);
        data.price_rent_max = data.price.rent.slice(0);
        temp_value = _.has(lang_static, 'form.placeholders.min.bed') ? _.get(lang_static, 'form.placeholders.min.bed') : 'Min Bed',
        data.bedroom_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.bed') ? _.get(lang_static, 'form.placeholders.max.bed') : 'Max Bed',
        data.bedroom_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.min.bath') ? _.get(lang_static, 'form.placeholders.min.bath') : 'Min Bath',
        data.bathroom_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.bath') ? _.get(lang_static, 'form.placeholders.max.bath') : 'Max Bath',
        data.bathroom_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.min.area') ? _.get(lang_static, 'form.placeholders.min.area') : 'Min Area',
        data.area_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.area') ? _.get(lang_static, 'form.placeholders.max.area') : 'Max Area',
        data.area_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
        data.price_sale_min.unshift({name: temp_value, value: ''});
        data.price_rent_min.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
        data.price_sale_max.unshift({name: temp_value, value: ''});
        data.price_rent_max.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.type') ? _.get(lang_static, 'form.placeholders.type') : 'Property Type',
        data.property_type.commercial.rent.unshift({name: temp_value, value: ''});
        data.property_type.commercial.sale.unshift({name: temp_value, value: ''});
        data.property_type.residential.rent.unshift({name: temp_value, value: ''});
        data.property_type.residential.sale.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.furnishing') ? _.get(lang_static, 'form.placeholders.furnishing') : 'Furnishing',
        data.furnishing.unshift({name: temp_value, value: ''});
        temp_value = _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
        data.completion_status.unshift({name: temp_value, value: ''});
        return data;

    },
    filter: {
        property: function(){
            $('#property_type').dropdown('clear');
            $('#property_bedroom_min').dropdown('clear');
            $('#property_bedroom_max').dropdown('clear');
            $('#property_bathroom_min').dropdown('clear');
            $('#property_bathroom_max').dropdown('clear');
            $('#property_area_min').dropdown('clear');
            $('#property_area_max').dropdown('clear');
            $('#property_furnishing').dropdown('clear');
            $('#property_price_min').dropdown('clear');
            $('#property_price_max').dropdown('clear');
            $('#property_suburb').val('');
            $('#property_suburb_hidden').val('');
            $('#completion_status').dropdown('clear');
            $('#keyword').val('');
            $('#property_rent_buy').dropdown('set selected', 'Buy');

            last_segment        =   window.location.href.split( '/' ).pop();
            new_last_segment    =   last_segment.replace( 'featured-', '' );
            newUrl              =   window.location.href.replace( last_segment, new_last_segment);
            newUrl              =   new URI( newUrl );
            newUrl              =   newUrl.query('').toString();

            history.pushState({}, null, newUrl);
            helpers.store.remove('property_search');
            search.property.regular.store_variables     =   new search.property.regular.variables();
        },
        project: function(){
            $('#projects_project').dropdown('clear');
            $('#completion_status').dropdown('clear');
            $('#project_location').val('');
            $('#keyword').val('');

            newUrl              =   window.location.href;
            newUrl              =   new URI( newUrl );
            newUrl              =   newUrl.query('').toString();

            history.pushState({}, null, newUrl);
            helpers.store.remove('project_search');
            search.projects.store_variables     =   new search.projects.variables();
        },
        agent: function(){
            $('#agent_brokerage').dropdown('clear');
            $('#agent_areas').dropdown('clear');
            $('#keyword').val('');

            newUrl              =   window.location.href;
            newUrl              =   new URI( newUrl );
            newUrl              =   newUrl.query('').toString();

            history.pushState({}, null, newUrl);
            helpers.store.remove('agent_search');
            search.agent.store_variables     =   new search.agent.variables();
        }
    },
    home:{
        property: '/' + selectedLanguage,
        project: {
            uae: '/' + selectedLanguage + '/projects/uae',
            international: '/' + selectedLanguage + '/projects/international'
        },
        agent: '/' + selectedLanguage + '/agents-search/uae/all-agency/all-suburbs',
        service: '/' + selectedLanguage + '/services'
    },
    parseNumberCustom : function(number_string) {
        var new_number = parseInt(number_string.toString().replace(/[^0-9\.]/g, ''));
        return new_number;
    },
    lazy_load   :   function(){
        $('.f_p').each(function() {
            if($(this).parents('#property_card_template').length != 1){

                var cdn_img    =   $(this).attr('src');
                var scd_img    =   $(this).attr('data-second');
                //Gallery or single
                var type     =   $(this).attr('data-type');
                helpers.fallback_img(cdn_img, scd_img, type, this);

            }
        });
    },
    fallback_img: function(first_img, second_img, type, trigger){

        helpers.imageExists(first_img, function(exists) {
            if(exists === false) {
                helpers.imageExists(second_img, function(sec_exists) {
                    if(sec_exists === true) {
                        $(trigger).attr("src", second_img);
                    } else {
                        if(type == 'gallery'){
                            $(trigger).parent().remove();
                        }else{
                            $(trigger).attr("src", cdnUrl + 'assets/img/default/no-img-available.jpg');
                        }
                    }
                }, trigger);
            }
        }, trigger);

    },
    imageExists: function(url, callback) {
        var img = new Image();
        img.onload = function() { callback(true); };
        img.onerror = function() { callback(false); };
        img.src = url;
    },
    modal_image_load: function(first_img, second_img, trigger){
        helpers.imageExists(first_img, function(exists) {
            if(exists === false) {
                helpers.imageExists(second_img, function(sec_exists) {
                    if(sec_exists === true) {
                        $(trigger).attr("src", second_img);
                    } else {
                        $(trigger).attr("src", cdnUrl + 'assets/img/default/no-agent-img.png');
                    }
                }, trigger);
            } else {
                $(trigger).attr("src", first_img);
            }
        }, trigger);
    },
    clear: {
        errors: function(){
            $('#register_error, #password_error, #signin_error').html('');
        }
    },
    reset_emi_form      :   function(){

        var form_wrapper    =   $( '#mortgage_calculator_wrapper' );

        $( '#emi_years' ).val( form_wrapper.data( 'init-years' ));
        $( '#emi_down_payment' ).val( form_wrapper.data( 'init-down-payment' ));
        $( '#emi_interest' ).val( form_wrapper.data( 'init-interest' ));

    },
    toast: {
        __init(msg){
            $('#toast').html(msg);
            $('#toast').fadeIn();
            $("#toast").css('visibility', 'visible');
            setTimeout(function(){
                $('#toast').fadeOut();
            }, 3000);
        }
    },
    toast_dashboard: {
        __init(msg){
            $('#toast_dashboard').html(msg);
            $('#toast_dashboard').fadeIn();
            $("#toast_dashboard").css('visibility', 'visible');
            setTimeout(function(){
                $('#toast_dashboard').fadeOut();
            }, 3000);
        }
    },
    share:{
        __init: function(){
            if( !$(".sharethis-inline-share-buttons, .st-btn").is(":visible")) {
                $('.sharethis-inline-share-buttons, .st-btn').show();

            } else {
                $('.sharethis-inline-share-buttons, .st-btn').hide();
            }

        }
    },
    sort: {
        __init: function(){

            $('#listings_sort_btn').dropdown({
                placeholder: 'Sort Results',
                action: function(text, value, element){

                    var sort_type = text.match("--(.*)--"),
                    url = new URI( window.location.href );

                    window.location.href = url.setSearch( 'sort', sort_type[1] ).toString();

                }
            });

        }
    },
    pagination: {
        __init: function(mobile){

            var listings_length  =   Math.ceil( $( '.listings_total' ).attr( 'data-listings_total' ) / 20 );
            listings_length      =  listings_length > 500 ? 500 : listings_length;

            if( listings_length > 1 ){

                var url 		    =     new URI( window.location.href ),
                current_page 	=     url.search(true).page ? url.search(true).page : 1;

                // Pagination trigger
                $('#pagination').twbsPagination({
                    totalPages: listings_length,
                    visiblePages: mobile ? 3 : 8,
                    initiateStartPageClick: false,
                    paginationClass: 'pages',
                    prev: _.has(lang_static, 'pagination.prev') ? _.get(lang_static, 'pagination.prev') : 'Prev',
                    next: _.has(lang_static, 'pagination.next') ? _.get(lang_static, 'pagination.next') : 'Next',
                    startPage: parseInt( current_page ),
                    onPageClick: function ( event, page ) {
                        if( page == 1 ){
                            window.location.href 	=	url.removeSearch( 'page' ).toString();
                        } else {
                            window.location.href    =   url.setSearch( 'page', page ).toString();
                        }
                    }
                });

            } else {
                $('#pagination').hide();
            }

        }
    },
    validate    :   {
        __init      :   function( ){

        },
        email: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email.toLowerCase());
        }
    },
    sanitize    :   {
    },
    format    :   {
    },
    store : {
        read: function(data){
            return localStorage.getItem(data);
        },
        write: function(name, data){
            localStorage.setItem(name, data);
        },
        parse: function(data){
            return JSON.parse(data);
        },
        stringify: function(data){
            return JSON.stringify(data);
        },
        clear: function(){
            localStorage.clear();
        },
        remove: function(item){
            localStorage.removeItem(item);
        },
        temp_data : {
            read: function(){
                return helpers.store.temp_data.write;
            },
            write : ''
        }
    },
    crypto : {
        encrypt: function(data){
            return CryptoJS.AES.encrypt(data, "My Secret Passphrase");
        },
        decrypt: function(data){
            return CryptoJS.AES.decrypt(data, "My Secret Passphrase");
        },
        string: function(data){
            return data.toString(CryptoJS.enc.Utf8);
        }
    },
    populate: {
        modals: function(data){
            var fullname = _.get(data,'name','').split(' ');
            // 1.   Call Back
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings')
            .find('input[name=name]').val(_.get(data,'name',''));
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings')
            .find('input[name=phone]').val(_.get(data,'number',''));
            $('#modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_email, #project_lead, #modal_ideal_property, #property_request_listings')
            .find('input[name=email]').val(_.get(data,'email',''));

            $('#modal_property_mortgage').find('input[name=first_name]').val(fullname[0]);
            $('#modal_property_mortgage').find('input[name=last_name]').val(fullname[1]);

            // 2.   Dashboard
            $('#user_dashboard').find('p.name').html(_.get(data,'name',''));
            $('#user_dashboard').find('input[name=name]').val(_.get(data,'name',''));
            $('#user_dashboard').find('input[name=email]').val(_.get(data,'email',''));
            $('#user_dashboard').find('input[name=number]').val(_.get(data,'number',''));
            $('#user_dashboard').find('input[name=nationality]').val(_.get(data,'nationality',''));

            //3. Property alert
            $('#modal_property_alert').find('input[name=name]').val(_.get(data,'name',''));
            $('#modal_property_alert').find('input[name=email]').val(_.get(data,'email',''));

            //4. Projects callback
            $('#modal_developer_callback_name').find('input[name=name]').val(_.get(data,'name',''));
            $('#modal_developer_callback_phone').find('input[name=phone]').val(_.get(data,'number',''));

        },
        header: function(data){
            $('.dashboard').html(_.get(data.name.split(' '),'0',''));
        }
    },
    depopulate: {
        modals: function(){
            // 1. Lead
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings').find('input[name=name]').val('');
            $('#modal_property_callback, #modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_callback, #modal_agent_email, #project_lead, #modal_ideal_property, #modal_developer_callback, #property_request_listings').find('input[name=phone]').val('');
            $('#modal_property_email, #modal_property_mortgage, #property_request_details, #modal_services_contact, #modal_agent_email, #project_lead, #modal_ideal_property, #property_request_listings').find('input[name=email]').val('');

            // 2.   Dashboard
            $('#user_dashboard').find('p.name').html('');
            $('#user_dashboard').find('input[name=name]').val('');
            $('#user_dashboard').find('input[name=email]').val('');
            $('#user_dashboard').find('input[name=number]').val('');
            $('#user_dashboard').find('input[name=nationality]').val('');

            //3. Property alert
            $('#modal_property_alert').find('input[name=name]').val('');
            $('#modal_property_alert').find('input[name=email]').val('');

            //4. Projects callback
            $('#modal_developer_callback_name').find('input[name=name]').val('');
            $('#modal_developer_callback_phone').find('input[name=email]').val('');

            //5. Clear Auth Forms
            $("#modal_signin").trigger('reset');
            $("#modal_register").trigger('reset');
            $("#modal_forgot_password").trigger('reset');
        },
        header: function(){

        }
    },
    user: {
        state: {
            is_logged_in: function(){
                if(helpers.store.read('user')){
                    try{
                        return [true, helpers.store.parse(helpers.crypto.string(helpers.crypto.decrypt(helpers.store.read('user'))))];
                    }
                    catch(err){
                        return [false, []];
                    }
                }
                return [false, []];
            }
        },
        loggedin : {
            __init: function(name){

                /**
                * #1. Read localstorage
                * #2. decrypto data
                * #3. toString data
                * #4. parse data
                * #5. check login status
                */

                if(helpers.store.read(name)){

                    //#1
                    var read = helpers.store.read(name);

                    //#2
                    var decrypt = helpers.crypto.decrypt(read);

                    //#3
                    var string = helpers.crypto.string(decrypt);

                    //#4
                    var user = helpers.store.parse(string);

                    //#5
                    return user.login;

                } else {

                    return false;

                }

            }
        }
    },
    trigger: {
        modal: {
            __init: function(type, transition){
                $('.ui.modal.' + type).modal('setting', 'transition', transition ? transition : 'fade up').modal('show');

                if(type === 'signin_register'){
                    var input = document.getElementById("signin_input_password");

                    input.addEventListener("keyup", function(event) {
                        event.preventDefault();
                        if (event.keyCode === 13) {
                            document.getElementById("signin_submit_btn").click();
                        }
                    });
                }
            }
        }
    },
    events: {
        bind : {
            mouseenter : function(class_name){
                $(class_name).mouseenter(function(){
                });
            }
        },
        unbind : {
            mouseenter : function(class_name){
                $(class_name).unbind('mouseenter mouseleave');
            }
        }
    },
    clean   :   {
        junk    :   function(el){
            return _.isObject(el) ? helpers.clean.internal(el) : el;
        },
        internal    :   function(el){
            return _.transform(el, function(result, value, key) {
                var isCollection = _.isObject(value);
                var cleaned = isCollection ? helpers.clean.internal(value) : value;
                if ( !_.isBoolean(cleaned) && !_.isNumber(cleaned) && (_.isUndefined(cleaned) || _.isEmpty(cleaned)) ) {
                    return;
                }
                _.isArray(result) ? result.push(cleaned) : (result[key] = cleaned);
            });
        }
    },
    indexof: function(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] == value) {
                return i;
            }
        }
        return null;
    },
    bind    :   function(data,binds,template_o,sink){

        var limiter = '%';
        try{
            // Clean the sink
            $('#'+sink).html('');
            // Fill the sink
            for(var j=0; j<data.length; j++){

                if(_.get(data,j+'.property.agent.agency.settings.lead.call_tracking.track'))
                _.set(data, j+'.property.agent.contact.phone', _.get(data,j+'.property.agent.agency.settings.lead.call_tracking.phone'));
                else if (_.get(data,j+'.property.agent.contact.phone', '') == '') {
                    _.set(data,j+'.property.agent.contact.phone', _.get(data,j+'.property.agent.agency.contact.phone'));
                }
                var template = template_o,
                bind_data = [];
                bind_data[j] = _.get(data,j+'.property');

                for(var b=0; b<binds.length; b++){

                    // Hook
                    var hook = limiter+_.get(binds,b,'')+limiter;

                    if(/\d/.test(_.get(binds,b))){
                        var bbind = eval(_.get(binds,b));
                    }
                    else{
                        var bbind = _.get(binds,b);
                    }

                    // Bind
                    var bind   =   _.get(_.get(data,j),bbind);

                    var residential_commercial = _.get(_.get(data,j),'property.residential_commercial');
                    var bedroom = _.get(_.get(data,j),'property.bedroom');
                    var bathroom = _.get(_.get(data,j),'property.bathroom');

                    if(_.get(binds,b,'') == 'property.web_link'){
                        var bind = baseUrl  +_.get(_.get(data,j),'property.rent_buy').replace(/[\W_]/g, '-').toLowerCase()+'/'
                        +_.get(_.get(data,j),'property.residential_commercial').toLowerCase()+'/'
                        +_.get(_.get(data,j),'property.writeup.title').replace(/[\W_]/g, "-").replace(/-{2,}/g, '-').toLowerCase()+'/'
                        +_.get(_.get(data,j),'property._id');
                    }
                    if(_.get(binds,b,'') == 'agency.logo'){
                        var agency_logo     =   _.get(_.get(data,j),'property.agent.agency.contact.email').split('@')[1];
                        var bind            =   cdnUrl + 'agency/logo/' + agency_logo;
                    }
                    if(_.get(binds,b,'') == 'property.price'){
                        var bind  =   (_.get(_.get(data,j),'property.price')).toLocaleString('en');
                    }
                    if(_.get(binds,b,'') == 'property.building'){
                        var building = _.get(_.get(data,j),'property.building');
                        if(!_.isEmpty(building)){
                            var bind  =  _.has(lang, 'building.'+_.snakeCase(building)) ? _.get(lang, 'building.'+_.snakeCase(building)) + ',' : building + ',';
                        }
                    }
                    if(_.get(binds,b,'') == 'property.area'){
                        var area = _.get(_.get(data,j),'property.area');
                        var bind  =  _.has(lang, 'area.'+_.snakeCase(area)) ? _.get(lang, 'area.'+_.snakeCase(area)) : area;
                    }
                    if(_.get(binds,b,'') == 'property.city'){
                        var city = _.get(_.get(data,j),'property.city');
                        var bind  =  _.has(lang, 'city.'+_.snakeCase(city)) ? _.get(lang, 'city.'+_.snakeCase(city)) : city;
                    }
                    if(_.get(binds,b,'') == 'property.type'){
                        var type = _.get(_.get(data,j),'property.type');
                        var bind  =  _.has(lang, 'type.'+_.snakeCase(type)) ? _.get(lang, 'type.'+_.snakeCase(type)) : type;
                    }
                    if(_.get(binds,b,'') == 'property.rent_buy'){
                        var rent_buy = _.get(_.get(data,j),'property.rent_buy');
                        var bind  =  _.has(lang, 'rent_buy.'+ _.snakeCase('for ' + rent_buy)) ? _.get(lang, 'rent_buy.'+ _.snakeCase('for_' + rent_buy)) : rent_buy;
                    }
                    if(_.get(binds,b,'') == 'property.bedroom'){
                        var bed = {
                            'bed': _.get(_.get(data,j),'property.bedroom')
                        }
                        var trans = helpers.trans_choice( _.get(lang_static, 'global.bed'),_.get(_.get(data,j),'property.bedroom') , bed );
                        var bind  =  '<i class="bed icon"></i> ' + (_.has(lang_static, 'global.bed') ? _.toLower(trans) : _.get(_.get(data,j),'property.bedroom'));
                        bind = (residential_commercial !== "Commercial" || (bedroom !== 0 && bedroom !== -1)) ? bind : '';
                    }
                    if(_.get(binds,b,'') == 'property.bathroom'){
                        var bath = {
                            'bath': _.get(_.get(data,j),'property.bathroom')
                        }
                        var trans = helpers.trans_choice( _.get(lang_static, 'global.bath'),_.get(_.get(data,j),'property.bathroom') , bath );
                        var bind  =  '<i class="bath icon"></i> ' + (_.has(lang_static, 'global.bath') ? _.toLower(trans) : _.get(_.get(data,j),'property.bathroom'));
                        bind = bathroom !== 0 ? bind : '';
                    }
                    if(_.get(binds,b,'') == 'property.dimension.builtup_area'){
                        var sqft  = _.has(lang_static, 'global.sqft') ? _.get(lang_static, 'global.sqft') : 'sqft';
                        var bind  =  (_.get(_.get(data,j),'property.dimension.builtup_area') == 0) ? '' : _.get(_.get(data,j),'property.dimension.builtup_area') +' '+ sqft;
                        if(_.get(_.get(data,j),'property.dimension.builtup_area') == 0){
                            $("#sq_similar_").hide();
                        }
                        else{
                            $("#sq_similar_"+_.get(_.get(data,j),'property.dimension.builtup_area')).show();
                        }
                    }

                    var regex = new RegExp( limiter+_.get(binds,b)+':(.*?)'+limiter );

                    if(template.match(regex)){
                        var inner_regex = new RegExp( limiter+_.get(binds,b)+':0:1'+limiter );
                        if(template.match(inner_regex)){
                            var hook    =   limiter+_.get(binds,b,'')+':0:1'+limiter;
                            var bind    =  _.get(_.get(_.get(data,j),_.get(binds,b)), 0);
                        }
                    }

                    template = template.split( hook ).join( bind );
                }
                $('#'+sink).append(template);
                //Binding all click actions
                let current = _.get(bind_data,j);
                $("#t_call_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.call.__init(current);
                });
                $("#t_callback_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.callback.__init(current);
                });
                $("#t_email_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.email.__init(current);
                });
                $("#t_chat_similar_"+_.get(bind_data,j+'._id')).on('click', function() {
                    pages.landing.property.whatsapp.__init(current);
                });


            }
        }
        catch(err){
        }

    },
    read_more_less : function(className, height, idname){

        var elem = $("." + className).text();
        if (elem == "more..." || elem == _.get(lang_static, 'global.more')) {
            var text  =  _.has(lang_static, 'global.less') ? _.get(lang_static, 'global.less') : "less...";
            $("." + className).text(text);
            $('#' + idname).css('height', 'auto');
            $('#' + idname + ' ul li').css('display','inline-block');
        } else {
            var text  =  _.has(lang_static, 'global.more') ? _.get(lang_static, 'global.more') : "more...";
            $("." + className).text(text);
            $('#' + idname).css('height', height );
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
        }

    },
    read_more_less_new : function(className, height, idname){
        var elem = $("." + className).text();
        if (elem == _.get(lang_static, 'global.more') || elem == _.get(lang_static, 'project.details.read_more') ) {
            var text  =  _.has(lang_static, 'project.details.read_less') ? _.get(lang_static, 'project.details.read_less') : "Read Less";
            $("." + className).text(text);
            $('#' + idname).css('height', 'auto');
            $('#' + idname).addClass('descriptxt-collapsed');
            $('#' + idname + ' ul li').css('display','inline-block');
            $("." + className).parent().css('padding-top', '1rem');
        } else {
            var text  =  _.has(lang_static, 'project.details.read_more') ? _.get(lang_static, 'project.details.read_more') : "Read More";
            $("." + className).text(text);
            $('#' + idname).css('height', height );
            $('#' + idname).removeClass('descriptxt-collapsed');
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
            $("." + className).parent().css('padding-top', '1rem');
        }

    },
    view_more_less : function(className, height, idname, idIcon='', iconCurrentClass='', iconExpectedClass='', extraBehavior=false, label_more = 'property.details.view_more', label_less = 'property.details.view_less'){
        var elem = $.trim($("." + className).text());
        if (elem == $.trim(_.get(lang_static, label_more))) {
            var text  =  _.has(lang_static, label_less) ? _.get(lang_static, label_less) : "View less";
            $("." + className).text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconCurrentClass).addClass(iconExpectedClass);
            }
            $('#' + idname).css('height', 'auto');
            $('#' + idname).addClass(idname+'-collapsed');
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', 'auto' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
        } else {
            var text  =  _.has(lang_static, label_more) ? _.get(lang_static, label_more) : "View more";
            $("." + className).text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconExpectedClass).addClass(iconCurrentClass);
            }
            $('#' + idname).css('height', height );
            $('#' + idname).removeClass(idname+'-collapsed');
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', '9rem' );
                $("." + className).parent().parent().css('top', '-76px' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
        }

    },
    view_more_amenities : function(className, height, idname, idIcon='', iconCurrentClass='', iconExpectedClass='', extraBehavior=false, label_more = 'property.details.view_more', label_less = 'property.details.view_less'){
        var elem = $.trim($("." + className + " span").text());

        if (elem == $.trim(_.get(lang_static, label_more))) {
            var text  =  _.has(lang_static, label_less) ? _.get(lang_static, label_less) : "View less";
            $("." + className + " span").text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconCurrentClass).addClass(iconExpectedClass);
            }
            $('#' + idname).css('height', 'auto');
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', 'auto' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
        } else {
            var text  =  _.has(lang_static, label_more) ? _.get(lang_static, label_more) : "View more";
            $("." + className + " span").text(text);
            if (idIcon !== '') {
                $('#'+idIcon).removeClass(iconExpectedClass).addClass(iconCurrentClass);
            }
            $('#' + idname).css('height', height );
            if (extraBehavior) {
                $('#' + idname).parent().parent().css('height', '9rem' );
                $("." + className).parent().css('top', '-76px' );
            }
            $('#' + idname + ' ul li').css('display','inline-block');
            $('#' + idname + ' ul li:nth-child(n+8)').css('display','none');
        }

    },
    trans_choice: function(key, count = 1, replace = {}){

        let translations = key.split('|');
        let translation  = '';
        var BreakException = {};
        try {
            translations.forEach((trans) => {
                if(_.startsWith(trans, '{'+count+'}')){
                    translation = _.trim(_.replace(_.replace(trans, /\[(.+?)\]/g, ''),/\{(.+?)\}/g,'' ));
                    throw BreakException;
                }
                else if((_.includes(trans,'[') && _.includes(trans, ']'))){

                    var range = trans.substring(trans.lastIndexOf("[") + 1,trans.lastIndexOf("]"));
                    var nums = range.split(',');
                    if(((count >= _.get(nums,0)) && (_.get(nums,1) == '*')) || (count <= _.get(nums,1))){
                        translation = _.trim(_.replace(_.replace(trans, /\[(.+?)\]/g, ''),/\{(.+?)\}/g,'' ));
                    }
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e !== BreakException) throw e;
        }
        for (var placeholder in replace) {
            translation = translation.replace(`:${placeholder}`, replace[placeholder]);
        }
        return translation;
    },
    trans_value: function(data, replace = {}){
        let translation  = '';
        var BreakException = {};
        try {
            for (var var_trans in replace) {
                translation =  _.replace(data, ':'+var_trans, replace[var_trans]);
            }
            return translation;
        } catch (e) {
            if (e !== BreakException) throw e;
        }
    },
    navToggle: function(e){
        let res_comm    =   $(e).attr("data-value");
        $(e).parent().children().removeClass('purple');
        $(e).addClass('purple');
        let rent_buy_menu = $(e).parent().parent().parent();
        rent_buy_menu.find('.row').removeClass('show');
        rent_buy_menu.find('.row').addClass('hide');
        rent_buy_menu.find('.row.'+res_comm).removeClass('hide');
        rent_buy_menu.find('.row.'+res_comm).addClass('show');
    }
}


//ON CHANGE EVENTS

$("#emi_years").change(function(){

    if($(this).val() > 25){
        $(this).val(25);
    } else if($(this).val() < 1){
        $(this).val(1);
    }

});


$("#emi_interest").change(function(){

    if($(this).val() > 5){
        $(this).val(5);
    } else if($(this).val() < 1){
        $(this).val(1);
    }

});
