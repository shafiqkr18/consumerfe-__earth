var search  =   {
    property:   {
        bindings    :   [ 'property.web_link','property.title','property.writeup.title','property.type','property.rent_buy','property.bedroom','property.bathroom','property.price','property.building','property.city','property.area','property.ref_no','property.dimension.builtup_area','property.images.original','property.residential_commercial','property._id','property.agent.contact.name','property.agent.agency.contact.name','property.agent.agency.contact.email','property.agent.contact.phone','agency.logo','_id'],
        template    :   $("#property_card_template").html(),
        regular     :   {
            __init      :   function(el){

                //Add loading to the Search Button
                $(el).addClass('loading');

                // Check for city, area, building
                var values = $('#property_suburb_hidden').val();
                
                search.property.regular.store_variables.suburb.value = values;
                search.property.regular.store_variables.suburb.label = $('#property_suburb_hidden_b').val();

                if( !_.isUndefined(values) && !_.isEmpty(values) ){

                    areas = values.split(', ');
                    areas.length = (areas.length)-1;
                    $.each(areas, function( index, value ){

                        locations  =   value.split('-');
                        if(locations.length === 1){
                            if(!_.includes(search.property.regular.store_variables.city, locations[0])){
                                search.property.regular.store_variables.city.push(locations[0]);
                            }
                        } else if(locations.length === 2){
                            if(!_.includes(search.property.regular.store_variables.city, locations[1])){
                                search.property.regular.store_variables.city.push(locations[1]);
                            }
                            if(!_.includes(search.property.regular.store_variables.area, locations[0])){
                                search.property.regular.store_variables.area.push(locations[0]);
                            }
                        } else if(locations.length === 3){
                            if(!_.includes(search.property.regular.store_variables.city, locations[2])){
                                search.property.regular.store_variables.city.push(locations[2]);
                            }
                            if(!_.includes(search.property.regular.store_variables.area, locations[1])){
                                search.property.regular.store_variables.area.push(locations[1]);
                            }
                            if(!_.includes(search.property.regular.store_variables.building, locations[0])){
                                search.property.regular.store_variables.building.push(locations[0]);
                            }
                        }

                    });

                    if(areas.length > 1){
                        all_locations   =   values.replace(/,\s*$/, "");
                        all_locations   =   encodeURIComponent(window.btoa(all_locations));
                        search.property.regular.store_variables.query_string.location = all_locations;
                    }

                }

                if(!_.isEmpty($('#keyword').val())){
                    search.property.regular.store_variables.query_string.keyword = $('#keyword').val();
                }

                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

                var qrsObj_prev     =   {};
                if(_.has(qrsObj_url_prev,'agency_id')){
                    qrsObj_prev.agency_id   =   qrsObj_url_prev.agency_id;
                }
                if(_.has(qrsObj_url_prev,'sort')){
                    qrsObj_prev.sort        =   qrsObj_url_prev.sort;
                }

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                    url         =   url.setSearch( qrsObj );
                    url         =   url.setSearch( qrsObj_prev );
                
                window.location.href =  url.toString();

            },
            prepopulate     :   function(){

                // Get payload
                var payload_data = JSON.parse( helpers.store.read('property_search') );

                // Translate Location while switching between languages
                if(!_.isEmpty(payload_data) && _.includes(payload_data,'suburb.value')){

                    areas = _.get(payload_data,'suburb.value').split(', ');
                    areas.length = (areas.length)-1;
                    payload_data.suburb.label = '';
                    $.each(areas, function( index, value ){
                        selectedLoc = _.filter(searchJson.property.location, function(loc) {
                            return loc.value === value;
                        });
                        if(!_.includes(selectedLoc,'0')){
                            payload_data.suburb.label += _.get(selectedLoc,'0.label') + ', ';
                        }
                    });

                }

                if(!payload_data){
                    payload_data = search.property.regular.deconstruct.url(window.location.href.replace(baseUrl,''));
                }

                // Populate form
                search.property.regular.populate_form(payload_data);
            },
            populate_form   :   function(payload){

                pages.landing.dropdown.callback.rent_buy(payload.rent_buy[0]);

                setTimeout(function(){
                    $('#property_rent_buy').dropdown('set selected', _.get(payload,'rent_buy[0]'));
                    $('#property_suburb').val(_.get(payload,'suburb.label'));
                    $('#property_suburb_hidden').val(_.get(payload,'suburb.value'));
                    $('#property_suburb_hidden_b').val(_.get(payload,'suburb.label'));
                    $('#property_type').dropdown('set selected', _.get(payload,'type[0]'));
                    $('#property_bedroom_min').dropdown('set selected', _.get(payload,'bedroom.min'));
                    $('#property_bedroom_max').dropdown('set selected', _.get(payload,'bedroom.max'));
                    $('#property_bathroom_min').dropdown('set selected', _.get(payload,'bathroom.min'));
                    $('#property_bathroom_max').dropdown('set selected', _.get(payload,'bathroom.max'));
                    $('#property_area_min').dropdown('set selected', _.get(payload,'dimension.builtup_area.min'));
                    $('#property_area_max').dropdown('set selected', _.get(payload,'dimension.builtup_area.max'));
                    $('#property_price_min').dropdown('set selected', _.get(payload,'price.min'));
                    $('#property_price_max').dropdown('set selected', _.get(payload,'price.max'));
                    $('#property_furnishing').dropdown('set selected', _.get(payload,'query_string.furnishing'));
                    $('#completion_status').dropdown('set selected', _.get(payload,'query_string.status'));
                    $('#keyword').val(_.get(payload,'query_string.keyword'));
                }, 100);

            },
            variables   :   function(){
                return {
                    ref_no      :   [],
                    permit_no   :   [],
                    building    :   [],
                    city		:	[],
                    area		:	[],
                    coordinates :   {
                        lat         :   undefined,
                        lng         :   undefined
                    },
                    dimension   :   {
                        builtup_area    :   {
                            min             :   undefined,
                            max             :   undefined
                        },
                        plot_size       :   {
                            min             :   undefined,
                            max             :   undefined
                        }
                    },
                    agent           :   {
                        contact         :   {
                            email           :   undefined
                        },
                        _id             :   undefined
                    },
                    amenities           :   [],
                    rent_buy	        :	[],
                    rental_frequency    :   [],
                    residential_commercial : [],
                    type       :       [],
                    settings    :   {
                        verified    :   undefined,
                        chat        :   undefined,
                        status      :   undefined
                    },
                    featured : {
                        status  : undefined
                    },
                    primary_view    :   [],
                    bedroom		        :		{
                        min             :       undefined,
                        max             :       undefined,
                    },
                    bathroom			:		{
                        min			    :		undefined,
                        max			    :		undefined,
                    },
                    price				:		{
                        min			    :		undefined,
                        max			    :		undefined,
                    },
                    // furnishing	:	[],
                    size    :   20,
                    page    :   1,
                    consumer_email  :   undefined,
                    agent   :   {
                        contact     :   {
                            email       :   undefined,
                        },
                        agency  :   {
                            contact     :   {
                                email       :   undefined
                            }
                        },
                        _id         :   undefined
                    },
                    sort            :   undefined,
                    featured        :   undefined,
                    suburb          :   {
                        label: undefined,
                        value: undefined
                    },
                    query_string    :   {
                        furnishing      :	undefined,
                        status          :   undefined,
                        keyword         :   undefined,
                        location        :   undefined
                    }
                }
            },
            construct   :   {
                url     :   function(search_inputs){

                    if( ( search_inputs.city.length > 1 ) && ( search_inputs.city[0] === " " ) ) {
                        var cityArray = _.slice( search_inputs.city, 1 );
                    } else {
                        var cityArray = search_inputs.city;
                    }

                    var urlObj = {
                        suburb: search_inputs.area.join('-or-') || null,
                        building: search_inputs.building.join('-or-') || null,
                        city: cityArray.join('-or-') || null,
                        rent_buy: _.isEmpty(search.property.regular.store_variables.rent_buy) ? 'Sale' : search_inputs.rent_buy.pop() || null,
                        // residential_commercial:
                        property_type: search_inputs.type.join('-or-') || null,
                        price_min: search_inputs.price.min || null,
                        price_max: search_inputs.price.max || null,
                        area_min: search_inputs.dimension.builtup_area.min || null,
                        area_max: search_inputs.dimension.builtup_area.max || null,
                        bedroom_min: (search_inputs.bedroom.min == 0) ? 'studio' : search_inputs.bedroom.min || null,
                        bedroom_max: (search_inputs.bedroom.max == 0) ? 'studio' : search_inputs.bedroom.max || null,
                        bathroom_min: search_inputs.bathroom.min || null,
                        bathroom_max: search_inputs.bathroom.max || null
                        // furnishing: search_inputs.furnishing || null
                    };

                    search.property.regular.store_variables.suburb = search_inputs.suburb;

                    search.property.regular.store_variables.rent_buy = [];
                    search.property.regular.store_variables.rent_buy.push(urlObj.rent_buy);

                    // 1-to-4-bedroom
                    // -apartment
                    // -for-rent-or-sale
                    // -in-Dubai-Marina-or-JBR-area
                    // -in-dubai-city
                    // -less-than-500000-aed
                    // -greater-than-100000-aed
                    // -with-1-to-10-bathroom
                    // Saving Previous Search
                    var previous_search 	=	{};
                    // Constructing query
                    var query = [];
                    // Setting Bedrooms
                    var bedroom_str = [];

                    if (!_.isNull(urlObj.bedroom_min)){
                        bedroom_str.push(urlObj.bedroom_min);
                    }

                    if (!_.isNull(urlObj.bedroom_max)) {
                        if( urlObj.bedroom_max != urlObj.bedroom_min ){
                            if ( !_.isNull(urlObj.bedroom_min) && !_.isEmpty(bedroom_str) ){
                                bedroom_str.push('to');
                            }
                            bedroom_str.push(urlObj.bedroom_max);
                        }
                    }

                    if (!_.isEmpty(bedroom_str)) {
                        bedroom_str.push('bedroom');
                        query['bedroom'] = bedroom_str.join("-");
                    }


                    // Setting Property_Type
                    if (!_.isNull(urlObj.property_type)) {
                        var all_selected_property_types		=	urlObj.property_type.toLowerCase().trim().split(" ");
                        query['type'] 				        = 	all_selected_property_types.join("-");
                    }


                    // Rent or Sale
                    if (!_.isNull(urlObj.rent_buy)) {
                        if (!_.isUndefined(urlObj.rent_buy)){
                            query['rent_buy'] = urlObj.rent_buy.toLowerCase();
                        }
                    }

                    // Suburbs
                    if (!_.isNull(urlObj.suburb)) {
                        var all_selected_suburbs 	=	urlObj.suburb.toLowerCase().replace(/ *\([^)]*\) */g, "").trim().split(' ').join('-');
                        query['area'] 		= 	all_selected_suburbs + "-area";
                    }

                    // Buildings
                    if (!_.isNull(urlObj.building)) {
                        var all_selected_buildings = urlObj.building.toLowerCase().replace(/ *\([^)]*\) */g, "").trim().split(' ').join('-');
                        query['building'] 				 = 	all_selected_buildings + "-building";
                    }


                    // Cities
                    if (!_.isNull(urlObj.city)) {
                        var all_selected_cities 	=	urlObj.city.toLowerCase().trim().split(' ');
                        query['city'] 				= 	all_selected_cities.join('-') + "-city";
                    }


                    // Price min
                    if (!_.isNull(urlObj.price_min)){
                        query['price_min'] = "price-greater-than-" + urlObj.price_min + "-aed";
                    }

                    // Price max
                    if (!_.isNull(urlObj.price_max)){
                        query['price_max'] = "price-less-than-" + urlObj.price_max + "-aed";
                    }

                    // Area min
                    if (!_.isNull(urlObj.area_min)){
                        query['area_min'] = "builtup-area-greater-than-" + urlObj.area_min + "-sqft";
                    }

                    // Area max
                    if (!_.isNull(urlObj.area_max)){
                        query['area_max'] = "builtup-area-less-than-" + urlObj.area_max + "-sqft";
                    }


                    // Bathrooms
                    var bathroom_str = [];
                    if (!_.isNull(urlObj.bathroom_min)){
                        bathroom_str.push(urlObj.bathroom_min);
                    }

                    if (!_.isNull(urlObj.bathroom_max)) {
                        if (_.isNull(urlObj.bathroom_min)){
                            bathroom_str.push(0);
                        }
                        bathroom_str.push('to')
                        bathroom_str.push(urlObj.bathroom_max);
                    }
                    if (!_.isEmpty(bathroom_str)) {
                        bathroom_str.push('bathroom');
                        query['bathroom'] = bathroom_str.join("-");
                    }
                    // Generating URL
                    var url 		= [];
                    var buckets 	= [];
                    var seo_buckets = [ 'rent_buy', 'city', 'area', 'building' ];
                    var delete_from_query 	=	[];

                    if( _.intersection(seo_buckets, _.keys(query)).length - 1 == 0 ){
                        var length = _.intersection(seo_buckets, _.keys(query)).length;
                    }
                    else{
                        var length = _.intersection(seo_buckets, _.keys(query)).length - 1
                    }
                    for( var u=0; u<(length); u++ ){

                        var v = seo_buckets[u];

                        if( v === 'rent_buy' && !_.isUndefined( query[v] ) ){
                            if( !_.includes( query[v], '-' ) || (_.startsWith( _.lowerCase(query[v]), 'short') ) ){
                                query[v]	=	"residential-" + query[v];
                            }
                            var p 	=	query[v].split(/-(.+)/);
                            url.push( ( p[1] == 'sale' ) ?  'buy' : p[1] );
                            url.push( p[0] );
                            query[v] 	=	p[1];
                        }
                        else{
                            if( v === 'city' && !_.isUndefined( query[v] ) ){
                                url.push( query[v].replace("-city","") );
                            }
                            if( v === 'area' && !_.isUndefined( query[v] ) ){
                                url.push( query[v].replace("-area","") );
                            }
                            if( v === 'building' && !_.isUndefined( query[v] ) ){
                                url.push( query[v].replace("-building","") );
                            }
                            delete_from_query.push( v );
                        }

                    }

                    url = url.join("/");

                    for( u=0; u<delete_from_query.length; u++ ){
                        delete query[delete_from_query[u]];
                    }
                    // Prefixes
                    var prefixes = [];

                    prefixes[0] = [];
                    prefixes[1] = [];
                    prefixes[2] = [];
                    prefixes[3] = [];
                    prefixes[4] = [];
                    prefixes[5] = [];
                    prefixes[6] = [];
                    prefixes[7] = [];
                    prefixes[8] = [];
                    prefixes[9] = [];
                    prefixes[10] = [];
                    prefixes[0]['field'] = "bedroom";
                    prefixes[0]['prefix'] = "";
                    prefixes[1]['field'] = "type";
                    prefixes[1]['prefix'] = "-";
                    prefixes[2]['field'] = "rent_buy";
                    prefixes[2]['prefix'] = "-for-";
                    prefixes[3]['field'] = "area";
                    prefixes[3]['prefix'] = "-in-";
                    prefixes[4]['field'] = "building";
                    prefixes[4]['prefix'] = "-in-";
                    prefixes[5]['field'] = "city";
                    prefixes[5]['prefix'] = "-in-";
                    prefixes[6]['field'] = "price_min";
                    prefixes[6]['prefix'] = "-";
                    prefixes[7]['field'] = "price_max";
                    prefixes[7]['prefix'] = "-";
                    prefixes[8]['field'] = "area_min";
                    prefixes[8]['prefix'] = "-";
                    prefixes[9]['field'] = "area_max";
                    prefixes[9]['prefix'] = "-";
                    prefixes[10]['field'] = "bathroom";
                    prefixes[10]['prefix'] = "-with-";


                    // Storing in Local Storage
                    if( !_.isEmpty( previous_search ) ){
                        localStorage.setItem( 'last_search', JSON.stringify(previous_search) );
                    }

                    // Construct String
                    var url_string = [];
                    first_one = true;
                    for (var i = 0; i < prefixes.length; i++) {
                        var field = prefixes[i]['field'];
                        var prefix = prefixes[i]['prefix'];

                        if (field in query) {
                            if (!first_one) {
                                url_string.push(prefix + query[field]);
                            }
                            else {
                                url_string.push(query[field]);
                                first_one = false;
                            }

                        }
                    }

                    last_segment = window.location.href.split( '/' ).pop();
                    search.property.regular.store_variables.featured = _.startsWith(last_segment,'featured-') ? 'featured' : undefined;
                    var last_slug   =   ( !_.isEmpty(search.property.regular.store_variables.featured) ? 'featured-' : '' ) + 'properties-for-';

                    var slug = ( !_.isNull(urlObj.property_type) || !_.isNull(urlObj.bedroom_min) || !_.isNull(urlObj.bedroom_max) ) ? '' : last_slug;

                    url	 =	_.endsWith( url, "-area" ) ? url.replace("-area","") : url;

                    url += "/"+slug+url_string.join("-").replace(" ", "-").replace(/-+/g, "-");

                    url = url.replace(/-+/g, "-");

                    var urlPath = baseUrl + url;
                    // hotfix for residential//commercial/properties
                    // if( !_.includes(urlPath, 'city') ){
                    //     urlPath 	=	urlPath.replace( "residential/properties", "residential-properties" ).replace( "commercial/properties", "commercial-properties" );
                    // }

                    return urlPath;

                }
            },
            deconstruct :   {
                buckets         :   function(url){

                    // Query to return
                    var query = new search.property.regular.variables;
                    // Url Parts
                    url_parts = url.split('/');
                    // Checking if buckets exist
                    if( url_parts.length === 1 ){
                        // No Buckets
                        var fragments_url   =   '';
                        var buckets_url     =   url_parts;
                    }
                    else{
                        // Yes Buckets
                        var fragments_url       =   url_parts.pop();
                        var buckets_url         =   url_parts;
                    }
                    // Buckets
                    var buckets     =   [ 'rent_buy', 'residential_commercial', 'city', 'area', 'building' ];
                    // Assign buckets
                    for( var i=0; i<buckets_url.length; i++ ){
                        var query_field     =   buckets[i];
                        var query_value     =   buckets_url[i];

                        if(query_field == 'rent_buy'){
                            // Typecasting buy, sale
                            if(query_value == 'buy'){
                                query_value = 'sale';
                            }
                            query.rent_buy.push(_.startCase(query_value));
                        }
                        if(query_field == 'residential_commercial'){
                            query.residential_commercial.push(_.startCase(query_value));
                        }
                        if(query_field == 'city'){
                            query.city = _.startCase(query_value.replace(/-/g,' ')).split(' Or ');
                        }
                        if(query_field == 'area'){
                            query.area = _.startCase(query_value.replace(/-/g,' ')).split(' Or ');
                        }
                        if(query_field == 'building'){
                            query.building = _.startCase(query_value.replace(/-/g,' ')).split(' Or ');
                        }
                    }
                    // Return bucketed query
                    return [query,fragments_url];
                },
                query_string    :   function(query,string){
                    // Check if query string exists
                    if(_.includes(string,'?')){
                        var query_string    =   string.split('?').pop().split('&');
                        for(var i=0; i<query_string.length; i++){
                            parts = query_string[i].split('=');
                            var key = parts[0];
                            var value = parts[1];
                            if(key == 'page'){
                                query.page = parseInt(value);
                            }
                            else if(key == 'sort'){
                                query.sort = value;
                            }
                            else if(key == 'consumer_email'){
                                query.consumer_email = decodeURIComponent(value);
                            }
                            else if(key == 'agent_id'){
                                query.agent._id = decodeURIComponent(value);
                            }
                            else if(key == 'agent_email'){
                                query.agent.contact.email = [ decodeURIComponent(value) ];
                            }
                            else if(key == 'agency_id'){
                                query.agent.agency._id = decodeURIComponent(value);
                            }
                            else if(key == 'agency_email'){
                                query.agent.agency.contact.email = [ decodeURIComponent(value) ];
                            }
                            else if(key == 'furnishing'){
                                query.furnished = parseInt(value);
                            }
                            else if(key == 'status'){
                                query.completion_status = value;
                            }
                            else if(key == 'keyword'){
                                query.keyword = decodeURIComponent(value);
                            }

                        }
                        // Remove query from fragments_url
                        string = string.split('?')[0];
                    }
                    return [query,string];
                },
                fragments       :   function(query,url){
                    // Hooking the url to identify start
                    url     =   ":"+url;
                    //  Getting fixed hooks first
                    //  PRICE
                    if(_.includes(url,'aed')){
                        // Check for min, max
                        var match   =   url.match("price-less-than-(.*)-aed");
                        if(match){
                            // Max present
                            query.price.max = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                        var match   =   url.match("price-greater-than-(.*)-aed");
                        if(match){
                            // Min present
                            query.price.min = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                    }
                    // Builtup Area
                    if(_.includes(url,'builtup-area')){
                        // Check for min, max
                        var match   =   url.match("builtup-area-less-than-(.*)-sqft");
                        if(match){
                            // Max present
                            query.dimension.builtup_area.max = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                        var match   =   url.match("builtup-area-greater-than-(.*)-sqft");
                        if(match){
                            // Min present
                            query.dimension.builtup_area.min = parseInt(match[1]);
                            // Trim URL
                            url = _.trimEnd(url.replace(match[0],'').replace('--','-').replace(':-',':'),'-');
                        }
                    }
                    // Dynamic Hooks
                    // BEDROOM
                    if(_.includes(url,'bedroom')){
                        if(_.includes(url,'studio')){
                            url = url.replace('studio',0);
                        }
                        // Check for start
                        var match   =   url.match("with-(.*)-bedroom");
                        if(!match){
                            // Check for mid
                            match   =   url.match(":(.*)-bedroom");
                        }
                        if(match){
                            var beds = match[1].match(/\d/g);
                            if(beds.length==1){
                                query.bedroom.min = parseInt(beds[0]);
                                query.bedroom.max = parseInt(beds[0]);
                            }
                            else if(beds.length==2){
                                query.bedroom.min = parseInt(beds[0]);
                                query.bedroom.max = parseInt(beds[1]);
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // BATHROOM
                    if(_.includes(url,'bathroom')){
                        // Check for mid
                        var match   =   url.match("with-(.*)-bathroom");
                        if(!match){
                            // Check for start
                            match   =   url.match(":(.*)-bathroom");
                        }
                        if(match){
                            var baths = match[1].match(/\d/g);
                            if(baths.length==1){
                                query.bathroom.min = parseInt(baths[0]);
                                query.bathroom.max = parseInt(baths[0]);
                            }
                            else if(baths.length==2){
                                query.bathroom.min = parseInt(baths[0]);
                                query.bathroom.max = parseInt(baths[1]);
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // Area
                    if(_.includes(url,'area')){
                        // Check for mid
                        var match   =   url.match("in-(.*)-area");
                        if(!match){
                            match   =   url.match(":(.*)-area");
                        }
                        if(match){
                            var areas = match[1].replace(/-/g,' ').split(' or ');
                            for(var i=0; i<areas.length; i++){
                                query.area.push(decodeURIComponent(_.trim(areas[i])));
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // Building
                    if(_.includes(url,'building')){
                        // Check for mid
                        var match   =   url.match("in-(.*)-building");
                        if(!match){
                            match   =   url.match(":(.*)-building");
                        }
                        if(match){
                            var buildings = match[1].replace(/-/g,' ').split(' or ');
                            for(var i=0; i<buildings.length; i++){
                                query.building.push(decodeURIComponent(_.trim(buildings[i])));
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    // City
                    if(_.includes(url,'city')){
                        // Check for mid
                        var match   =   url.match("in-(.*)-city");
                        if(!match){
                            match   =   url.match(":(.*)-city");
                        }
                        if(match){
                            var buildings = match[1].replace(/-/g,' ').split(' or ');
                            for(var i=0; i<buildings.length; i++){
                                query.city.push(decodeURIComponent(_.startCase(_.trim(buildings[i]))));
                            }
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    //RENT_BUY
                    if(_.includes(url, 'rent') || _.includes(url, 'sale')){
                        // Check for mid
                        var match   =   url.match("for-(.*)");
                        if(!match){
                            match   =   url.match(":(.*)");
                        }
                        if(match){
                            query.rent_buy.push(_.startCase(match[1]));
                            query.rent_buy  =   _.uniq(query.rent_buy);
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(match[0],'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }

                    // TYPE
                    if(!_.includes(url, 'properties')){
                        // Check for mid
                        match   =   url.match(":(.*)");
                        all_prop_types = searchJson.property.types;
                        matched_type = '';

                        for( var i = 0; i < all_prop_types.length; i++ ){
                            let matc   =  url.match(all_prop_types[i]);
                            if(!_.isEmpty(matc)){
                                matched_type   =   matc[0];
                            }
                        }
                        if(!_.isEmpty(matched_type)){
                            query.type.push(_.startCase(matched_type.replace('-',' ')));
                            // Trim URL
                            // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                            url = _.trimEnd(url.replace(':','::').replace(matched_type,'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                        }
                    }
                    else{
                        if(_.includes(url,'residential')){
                            query.residential_commercial.push('Residential');
                        }
                        else if(_.includes(url,'commercial')){
                            query.residential_commercial.push('Commercial');
                        }
                        else if(_.includes(url,'short')){
                            query.residential_commercial.push('Short Term Rent');
                        }
                        query.residential_commercial    =   _.uniq(query.residential_commercial);
                    }

                    // Check for mid
                    match   =   url.match(":(.*)");
                    all_keywords = searchJson.keywords;
                    matched_keyword = '';

                    for( var i = 0; i < all_keywords.length; i++ ){
                        let matc   =  url.match(all_keywords[i]);
                        if(!_.isEmpty(matc)){
                            matched_keyword   =   matc[0];
                        }
                    }

                    if(!_.isEmpty(matched_keyword)){
                        query.keyword = _.startCase(matched_keyword.replace('-',' '));
                        query.keyword_in_path = 1;
                        // Trim URL
                        // Adding :: to start hook because the upper match matches the start hook in line match   =   url.match(":(.*)-bedroom");
                        url = _.trimEnd(url.replace(':','::').replace(matched_keyword,'').replace('--','-').replace(':-',':').replace('::',':'),'-');
                    }

                    if(_.includes(url, 'featured')){
                        query.featured = {};
                        query.featured.status = true;
                    }
                    return query;
                },
                url             :   function(url){
                    var query,fragments;
                    // Extract buckets
                    parts       =   search.property.regular.deconstruct.buckets(url);
                    query       =   parts[0];
                    fragments   =   parts[1];
                    // Extract Query String Params
                    parts       =   search.property.regular.deconstruct.query_string(query,fragments);
                    query       =   parts[0];
                    fragments   =   parts[1];
                    // Extract other fragments
                    query       =   search.property.regular.deconstruct.fragments(query,fragments);

                    return query;
                }
            },
            find      :   function(el){

                // Check for city, area, building
                var values = el['value'];
                search.property.regular.store_variables.suburb.value = values;
                // search.property.regular.store_variables.suburb.label = el['label'];

                if( !_.isUndefined(values) && !_.isEmpty(values) ){

                    areas = values;

                    locations  =   areas.split('-');
                    if(locations.length === 1){
                        if(!_.includes(search.property.regular.store_variables.city, locations[0])){
                            search.property.regular.store_variables.city.push(locations[0]);
                        }
                    } else if(locations.length === 2){
                        if(!_.includes(search.property.regular.store_variables.city, locations[1])){
                            search.property.regular.store_variables.city.push(locations[1]);
                        }
                        if(!_.includes(search.property.regular.store_variables.area, locations[0])){
                            search.property.regular.store_variables.area.push(locations[0]);
                        }
                    } else if(locations.length === 3){
                        if(!_.includes(search.property.regular.store_variables.city, locations[2])){
                            search.property.regular.store_variables.city.push(locations[2]);
                        }
                        if(!_.includes(search.property.regular.store_variables.area, locations[1])){
                            search.property.regular.store_variables.area.push(locations[1]);
                        }
                        if(!_.includes(search.property.regular.store_variables.building, locations[0])){
                            search.property.regular.store_variables.building.push(locations[0]);
                        }
                    }

                    // if(areas.length > 1){
                    //     all_locations   =   areas.replace(/,\s*$/, "");
                    //     all_locations   =   encodeURIComponent(window.btoa(all_locations));
                    //     search.property.regular.store_variables.query_string.location = all_locations;
                    // }

                }
                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
                
                var qrsObj_prev     =   {};

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                    url         =   url.setSearch( qrsObj );
                    url         =   url.setSearch( qrsObj_prev );

                window.location.href =  url.toString();

            }
        },
        budget      :   {
            __init      :   function(el){

                //Add loading to the Search Button
                $(el).addClass('loading');

                // 1.   Construct URL
                var url     =   search.property.budget.construct.url(search.property.budget.store_variables);

                //Writing the search to localstorage
                helpers.store.write('property_budget_search', helpers.store.stringify(search.property.budget.store_variables));

                // 2.   Redirect
                window.location.href =  url;

            },
            prepopulate     :   function(){
                // Get payload
                var payload = search.property.budget.deconstruct.url(window.location.href.replace(baseUrl,''));

                // Populate form
                search.property.budget.populate_form(payload);
            },
            populate_form   :   function(payload){

                pages.landing.dropdown.callback.rent_buy(payload.property.rent_buy);

                $('#property_rent_buy').dropdown('set selected', payload.property.rent_buy);
                $('#budget_city').dropdown('set selected', payload.property.city);

                setTimeout(function() {
                    $('#property_price_min').dropdown('set selected', payload.property.price.min);
                    $('#property_price_max').dropdown('set selected', payload.property.price.max);
                }, 100);

            },
            variables   :   function(){
                return {
                    city                :       [],
                    rent_buy	        :		[],
                    price				:		{
                        min				:		undefined,
                        max				:		undefined
                    }
                }
            },
            construct: {
                url: function(search_inputs){
                    var residential_commercial  =  _.startsWith( _.lowerCase(search_inputs.rent_buy), 'commercial') ? 'Commercial' : 'Residential';
                    var is_short_term           =  _.includes( _.lowerCase(search_inputs.rent_buy), 'short') ? true : false;
                    var data = {
                        city: search_inputs.city.length ? search_inputs.city : 'Dubai',
                        rent_buy: _.endsWith( _.lowerCase(search_inputs.rent_buy), 'sale') ? 'Sale' : ( is_short_term ? search_inputs.rent_buy.toString().replace('Commercial-','') : 'Rent' ),
                        res_comm: residential_commercial,
                        price: {
                            min: search_inputs.price.min ? search_inputs.price.min : _.endsWith( _.lowerCase(search_inputs.rent_buy), 'rent') ? 10000 : 100000,
                            max: search_inputs.price.max ? search_inputs.price.max : _.endsWith( _.lowerCase(search_inputs.rent_buy), 'rent') ? 1000000 : 50000000
                        }
                    }
                    var url = baseUrl + 'budget-search/results?residential_commercial=' + data.res_comm + '&rent_buy=' + data.rent_buy + '&price_min=' + data.price.min + '&price_max=' + data.price.max + '&emirate=' + data.city;

                    return url;
                }
            },
            deconstruct: {
                url: function(){
                    var url = window.location.href;

                    //construct payload for validation
                    var payload = {
                        property : {}
                    };

                    //RENT_BUY
                    if(_.includes(url, '=Commercial')){
                        payload.property.rent_buy = 'Commercial-' + url.split('rent_buy=').pop().split('&price')[0];
                    } else if(_.includes(url, '=Residential')){
                        payload.property.rent_buy = url.split('rent_buy=').pop().split('&price')[0];
                    }

                    //City
                    if(url.match("emirate=(.*)")){
                        var city = url.match("emirate=(.*)")[1];
                        payload.property.city = decodeURI(city);
                    }

                    //PRICE
                    if(url.match("price_min=(.*)")){
                        var prices = url.match("price_min=(.*)")[1].match(/^\d+|\d+\b|\d+(?=\w)/g);
                        payload.property.price = {};
                        payload.property.price.min = parseInt(prices[0]);
                        payload.property.price.max = parseInt(prices[1]);
                    }

                    return payload;

                }
            },
            listings: {
                __init    :   function(){

                    var url = window.location.href.replace('results','data');

                    axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {
                        // Populate
                        $('#listings_wrapper').html(response.data);
                        // Activate Accordian
                        $('.ui.accordion').accordion();
                        // Lazy Load
                        helpers.lazy_load();
                    }).catch( function( error ) {
                    });

                }
            }
        },
        listings    :   function(payload,sink){

            // Check for rent_buy
            if(_.includes(_.get(payload,'rent_buy.0',''), 'Commercial')){
                var parts = _.get(payload,'rent_buy.0','').split(/-(.+)/);
                payload.rent_buy                    =   [];
                payload.residential_commercial      =   [];
                payload.rent_buy.push(_.get(parts,'1','Sale').replace(/-/g, ' '));
                payload.residential_commercial.push(_.get(parts,'0','Commercial'));
            }
            else if(!_.isEmpty(_.get(payload,'rent_buy.0',undefined))){
                var rent_buy                        =   _.get(payload,'rent_buy.0','').replace(/-/g, ' ');
                payload.rent_buy                    =   [];
                payload.residential_commercial      =   [];
                payload.rent_buy.push(rent_buy);
                payload.residential_commercial.push('Residential');
            }

            // Check for city, area, building
            var value = $('#property_suburb_hidden').val();
            // var a = search.property.regular.store_variables.suburb[0];
            if( !_.isUndefined(value) && !_.isEmpty(value) ){
                areas      =   value.split(',');
                locations  =   areas[areas.length -1 ];
                locations  =   locations.split('-');
                if(locations.length === 1){
                    if(!_.includes(search.property.regular.store_variables.city, locations[0])){
                        search.property.regular.store_variables.city.push(locations[0]);
                    }
                } else if(locations.length === 2){
                    if(!_.includes(search.property.regular.store_variables.city, locations[1])){
                        search.property.regular.store_variables.city.push(locations[1]);
                    }
                    if(!_.includes(search.property.regular.store_variables.area, locations[0])){
                        search.property.regular.store_variables.area.push(locations[0]);
                    }
                } else if(locations.length === 3){
                    if(!_.includes(search.property.regular.store_variables.city, locations[2])){
                        search.property.regular.store_variables.city.push(locations[2]);
                    }
                    if(!_.includes(search.property.regular.store_variables.area, locations[1])){
                        search.property.regular.store_variables.area.push(locations[1]);
                    }
                    if(!_.includes(search.property.regular.store_variables.building, locations[0])){
                        search.property.regular.store_variables.building.push(locations[0]);
                    }
                }
            }

            var url = baseApiUrl+"property?query=" + JSON.stringify(helpers.clean.junk(payload));
            // #3
            axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {

                // 1.   Identify Data
                var data    =   response.data.data;
                // 2.   TODO Validation for if data exists
                var property = _.map(data,function(item){
                    var t = {};
                    t.property  = item;
                    t._id       = _.get(item,'_id','');
                    return t;
                });

                // 3.   Initiate Bind
                if(sink == 'featured_properties'){
                    if(property.length == 0){
                        payload.featured.status = false;
                        search.property.listings(payload, 'featured_properties');
                    }else{
                        property = _.shuffle(property);
                        property = _.take(property, 2);
                        helpers.bind(property, search.property.bindings, search.property.template, sink);
                    }
                }else{
                    if(property.length > 0){ $('#' + sink).parent().siblings().removeClass('hide'); }
                    helpers.bind(property, search.property.bindings, search.property.template, sink);
                }
                // 4.   Lazy Load
                helpers.lazy_load();

            }).catch( function( error ) {
            });
        },
        similar     :   function(id,sink){
            var url = baseApiUrl+"property/"+id+"/similar";
            // #3
            axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {
                // 1.   Identify Data
                // TODO Validation for if data exists
                var data    =   response.data.data;
                var property = _.map(data,function(item){
                    var t = {};
                    t.property  = item;
                    t._id       = _.get(item,'_id','');
                    return t;
                });
                $( "#property_card_list" ).prev().removeClass( "hide" );
                // 3.   Initiate Bind
                helpers.bind(property,search.property.bindings,search.property.template,sink);
                // 4.   Lazy Load
                helpers.lazy_load();

            }).catch( function( error ) {
            });
        },
        
    },
    agent:  {
        __init: function(el) {

            //Add loading to the Search Button
            $(el).addClass('loading');

            if(!_.isEmpty($('#keyword').val())){
                search.agent.store_variables.query_string.keyword = $('#keyword').val();
            }

            // 1.   Construct URL
            var url     =   search.agent.construct.url(search.agent.store_variables);

            //Writing the search to localstorage
            helpers.store.write('agent_search', helpers.store.stringify(search.agent.store_variables));

            //  2.1.    Get new query string object
            var qrsObj  =   search.agent.store_variables.query_string;
            for (var propName in qrsObj) {
                if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                    delete qrsObj[propName];
                }
            }

            //  2.2.  Retain search with previous query string
            var url_prev        =   new URI( window.location.href );
            var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
            var qrsObj_prev     =   {};
            if(_.has(qrsObj_url_prev,'sort')){
                qrsObj_prev.sort        =   qrsObj_url_prev.sort;
            }

            //  2.3.  Construct URL with new and previous query strings
            var url         =   new URI( url );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );
            // 3.   Redirect
            window.location.href =   url.toString();

        },
        prepopulate     :   function(){

            // var url = window.location.href.replace(baseUrl,'');
            // var payload = {};
            // var parts = url.split('/');
            // var area = parts[parts.length - 1];
            // payload.agent_brokerage = url.match("uae/(.*)/")[1];
            // payload.agent_areas = area.split('-area')[0].split('-').join(' ');

            var url         =   new URI( window.location.href );
            var payload     =   {};

            // Path segments
            // Brokerage
            if(!_.isEmpty(url.segment(3))){
                payload.agent_brokerage = url.segment(3);
            }
            // Area
            if(!_.isEmpty(url.segment(4))){
                var area    =   url.segment(4);
                area        =   _.endsWith( area, "-area" ) ? area.replace("-area","") : area;
                payload.agent_areas = _.startCase(area);
            }

            // Query string
            query_string    =   url.query(true);

            if(url.hasQuery("keyword")){
                payload.keyword = query_string.keyword;
            }

            // Populate form
            search.agent.populate_form(payload);
        },
        populate_form   :   function(payload){
            $('#agent_brokerage').dropdown('set selected', _.get(payload,'agent_brokerage'));
            $('#agent_areas').dropdown('set selected', _.get(payload,'agent_areas'));
            $('#keyword').val(_.get(payload,'keyword'));
        },
        variables   :   function(){
            return {
                agency      :   [],
                area        :   [],
                city        :   [],
                building    :   [],
                query_string: {
                    keyword     :   undefined
                }
            }
        },
        construct   :   {
            url     :   function(urlObj){

                var url = '';

                //All inputs empty
                if(_.isEmpty(urlObj.agency) && _.isEmpty(urlObj.area)) {
                    var all_empty = 'agents-search/uae/all-agency/all-suburbs';
                    url += all_empty;
                }

                if(!_.isEmpty(urlObj.agency) && _.isEmpty(urlObj.area)) {
                    url += 'agents-search/uae/' + urlObj.agency + '/all-suburbs';

                } else if(_.isEmpty(urlObj.agency) && !_.isEmpty(urlObj.area)) {

                        var area = 'agents-search/uae/all-agency' + '/' + urlObj.area + '-area';

                        url += area.replace(/\s/g, '-');

                } else if(!_.isEmpty(urlObj.agency) && !_.isEmpty(urlObj.area)) {

                    var agency = urlObj.agency;

                    var area = 'agents-search/uae/' + agency + '/' + urlObj.area + '-area';

                    url += area.replace(/\s/g, '-');

                }

                var urlPath = baseUrl + url;

                return urlPath;

            }
        }
    },
    agency:  {
        __init      :       function(){
            search.agency.listings();
        },
        variables   :   {
            agency	 :		{
                name :   null
            }
        }
    },
    projects: {
        __init: function(el){
            //Add loading to the Search Button
            $(el).addClass('loading');

            let country =   $(el).attr('data-country');

            // Check for city and area
            var value   =   $('#project_location').val();
            if( !_.isUndefined(value) && !_.isEmpty(value) ){

                search.projects.store_variables.location = value;

                areas      =   value.split(',');
                locations  =   areas[areas.length -1 ];
                locations  =   locations.split('-');

                if(locations.length === 1){
                    if(!_.includes(search.projects.store_variables.query_string.country, locations[0])){
                        search.projects.store_variables.query_string.country.push(locations[0].replace(/ *\([^)]*\) */g, "").trim());
                    }
                }
                else if(locations.length === 2){
                    if(!_.includes(search.projects.store_variables.query_string.city, locations[0])){
                        search.projects.store_variables.query_string.city.push(locations[0].replace(/ *\([^)]*\) */g, "").trim());
                    }
                    if(!_.includes(search.projects.store_variables.query_string.country, locations[1])){
                        search.projects.store_variables.query_string.country.push(locations[1].replace(/ *\([^)]*\) */g, "").trim());
                    }
                }
                else if(locations.length === 3){
                    if(!_.includes(search.projects.store_variables.query_string.area, locations[0])){
                        search.projects.store_variables.query_string.area.push(locations[0].replace(/ *\([^)]*\) */g, "").trim());
                    }
                    if(!_.includes(search.projects.store_variables.query_string.city, locations[1])){
                        search.projects.store_variables.query_string.city.push(locations[1].replace(/ *\([^)]*\) */g, "").trim());
                    }
                    if(!_.includes(search.projects.store_variables.query_string.country, locations[2])){
                        search.projects.store_variables.query_string.country.push(locations[2].replace(/ *\([^)]*\) */g, "").trim());
                    }
                }
            }

            search.projects.store_variables.country_segment = country;

            if(!_.isEmpty($('#keyword').val())){
                search.projects.store_variables.query_string.keyword = $('#keyword').val();
            }

            //  1.  Construct URL
            var url     =   search.projects.construct.url(search.projects.store_variables);

            //  Writing search to the localstorage
            helpers.store.write('project_search_'+country, helpers.store.stringify(search.projects.store_variables));

            //  2.1.    Get new query string object
            var qrsObj  =   search.projects.store_variables.query_string;
            for (var propName in qrsObj) {
                if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                    delete qrsObj[propName];
                }
            }

            //  2.2.  Retain search with previous query string
            var url_prev        =   new URI( window.location.href );
            var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

            var qrsObj_prev     =   {};
            if(_.has(qrsObj_url_prev,'sort')){
                qrsObj_prev.sort        =   qrsObj_url_prev.sort;
            }

            //  2.3.  Construct URL with new and previous query strings
            var url         =   new URI( url );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );

            // 3.   Redirect
            window.location.href =  baseUrl + url.toString();
        },
        construct: {
            url     :   function(urlObj){
                var url = urlObj.developer ? 'projects/' + urlObj.country_segment + '/' + urlObj.developer : 'projects/' + urlObj.country_segment;
                return url;
            }
        },
        prepopulate     :   function(country){
            // Get payload
            var payload = JSON.parse(localStorage.getItem("project_search_"+country));

            // Populate form
            if(payload){
                search.projects.populate_form(payload);
            }
        },
        populate_form   :   function(payload){
            $('#projects_project').dropdown('set selected', payload.developer);
            $('#completion_status').dropdown('set selected', payload.query_string.status);
            $('#keyword').val(_.get(payload,'query_string.keyword'));
            $('#project_location').val(_.get(payload,'location'));
        },
        variables   :   function(){
            return {
                developer       :   undefined,
                location        :   undefined,
                query_string    :   {
                    status          :   undefined,
                    keyword         :   undefined,
                    area            :   [],
                    city            :   [],
                    country         :   [],
                    country_segment :   undefined
                }
            }
        }
    },
    services: {
        __init: function(el){
            $(el).addClass('loading');
            search.services.listings($(el).attr('data-_id'));
        },
        listings: function(type) {
            var url = type ? 'services/' + type + '/uae' : 'services';
            window.location.href = baseUrl + url;
        }
    },
    redirect_landing: function() {
        helpers.store.remove('property_search');
        window.location.href = baseUrl
    },
    new_search: function(el) {
        let page = $(el).attr('data-page');
        let subpage = $(el).attr('data-subpage');
        helpers.filter[page]();
        // window.location.href = helpers.home[page];
        window.location.href = !_.isEmpty(subpage) ? helpers.home[page][subpage] : helpers.home[page];
    }
}

search.property.regular.store_variables     =   new search.property.regular.variables();
search.property.budget.store_variables      =   new search.property.budget.variables();
search.agent.store_variables                =   new search.agent.variables();
search.projects.store_variables             =   new search.projects.variables();
