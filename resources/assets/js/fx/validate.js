var validate = {
    rules: {
        lead: {
            email: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_email').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            callback: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_callback').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            call: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_call').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            details: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#property_request_details').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            listings: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                        return helpers.phonenumber.iti.isValidNumber();
                    };

                    $('#property_request_listings').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    }
                                    ,
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            project: {
                email:{
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#project_lead').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                        $('#modal_developer_email').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                },
                enquiry:{
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_developer_enquiry').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                },
                                message: {
                                    identifier: 'message',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your message'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                },
                callback:{
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_developer_callback').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                }

            },
            mortgage: {
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_property_mortgage').form({
                        fields: {
                            mortgage_user_name: {
                                identifier: 'mortgage_user_name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your first name'
                                    }
                                ]
                            },
                            mortgage_user_name_last: {
                                identifier: 'mortgage_user_name_last',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your last name'
                                    }
                                ]
                            },
                            mortgage_user_email: {
                                identifier: 'mortgage_user_email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            mortgage_user_phone: {
                                identifier: 'mortgage_user_phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            agent: {
                callback: {
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_agent_callback').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                },
                email: {
                    __init: function(){
                        $.fn.form.settings.rules.phone_format_valid = function() {
                          return helpers.phonenumber.iti.isValidNumber();
                        };
                        $('#modal_agent_email').form({
                            fields: {
                                name: {
                                    identifier: 'name',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your name'
                                        }
                                    ]
                                },
                                email: {
                                    identifier: 'email',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your email'
                                        }
                                    ]
                                },
                                phone: {
                                    identifier: 'phone',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : 'Please enter your phone number'
                                        },
                                        {
                                            type   : 'phone_format_valid',
                                            prompt : 'Please enter a valid number'
                                        }
                                    ]
                                }
                            }
                        });
                    }
                }
            },
            service:{
                __init: function(){
                    $.fn.form.settings.rules.phone_format_valid = function() {
                      return helpers.phonenumber.iti.isValidNumber();
                    };
                    $('#modal_services_contact').form({
                        fields: {
                            name: {
                                identifier: 'name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            phone: {
                                identifier: 'phone',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your phone number'
                                    },
                                    {
                                        type   : 'phone_format_valid',
                                        prompt : 'Please enter a valid number'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            general_enquiry:{
              __init: function(){
                  $.fn.form.settings.rules.phone_format_valid = function() {
                    return helpers.phonenumber.iti.isValidNumber();
                  };
                  $('#modal_general_enquiry').form({
                      fields: {
                          name: {
                              identifier: 'name',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please enter your name'
                                  }
                              ]
                          },
                          email: {
                              identifier: 'email',
                              rules: [
                                  {
                                      type   : 'email',
                                      prompt : 'Please enter a valid email'
                                  }
                              ]
                          },
                          type: {
                              identifier: 'type',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please select a property type'
                                  }
                              ]
                          },
                          rent_buy: {
                              identifier: 'rent_buy',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please select Rent or Buy'
                                  }
                              ]
                          },
                          location: {
                              identifier: 'location',
                              rules: [
                                  {
                                      type   : 'empty',
                                      prompt : 'Please select a location'
                                  }
                              ]
                          },
                          phone: {
                              identifier: 'phone',
                              rules: [
                                  {
                                      type   : 'phone_format_valid',
                                      prompt : 'Please enter a valid number'
                                  }
                              ]
                          }
                      }
                  });
              }
            }
        },
        auth: {
            login: {
                __init: function(){
                    $('#modal_signin').form({
                        fields: {
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            password: {
                                identifier: 'password',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your password'
                                    },
                                    {
                                        type   : 'minLength[6]',
                                        prompt : 'Your password must be at least {ruleValue} characters'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            register: {
                __init: function(){
                    $('#modal_register').form({
                        fields: {
                            full_name: {
                                identifier: 'full_name',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your full name'
                                    }
                                ]
                            },
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            },
                            password: {
                                identifier: 'password',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your password'
                                    },
                                    {
                                        type   : 'minLength[6]',
                                        prompt : 'Your password must be at least {ruleValue} characters'
                                    }
                                ]
                            },
                            confirm_password: {
                                identifier: 'confirm_password',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please confirm your password'
                                    },
                                    {
                                        type   : 'minLength[6]',
                                        prompt : 'Your password must be at least 6 characters'
                                    }
                                ]
                            }
                        }
                    });
                }
            },
            forgot_password: {
                __init: function(){
                    $('#modal_forgot_password').form({
                        fields: {
                            email: {
                                identifier: 'email',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your email'
                                    }
                                ]
                            }
                        }
                    });
                }
            }
        }
    }
}
