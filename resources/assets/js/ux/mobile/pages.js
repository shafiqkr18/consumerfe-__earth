var pages = {
    dashboard: {
        back: {
            __init() {
                $('.user_dashboard_list').removeClass('hide');

                $('.user_dashboard_settings').addClass('hide');
                $('.user_dashboard_fav_searches').addClass('hide');
                $('.user_dashboard_fav_properties').addClass('hide');
                $('.user_dashboard_fav_alerts').addClass('hide');
                $('.user_dashboard_profile').addClass('hide');
                $('.user_dashboard_back_arrow').addClass('hide');
            }
        },
        profile: {
            __init: function() {
                $('.user_dashboard_profile, .user_dashboard_back_arrow').removeClass('hide');

                $('.user_dashboard_settings').addClass('hide');
                $('.user_dashboard_fav_searches').addClass('hide');
                $('.user_dashboard_fav_properties').addClass('hide');
                $('.user_dashboard_list').addClass('hide');
            }
        },
        settings: {
            __init: function() {
                $('.user_dashboard_settings, .user_dashboard_back_arrow').removeClass('hide');

                $('.user_dashboard_fav_searches').addClass('hide');
                $('.user_dashboard_fav_properties').addClass('hide');
                $('.user_dashboard_profile').addClass('hide');
                $('.user_dashboard_list').addClass('hide');
            }
        },
        favourite: {
            __init: function(type) {

                if (type === 'search') {

                    $('.user_dashboard_fav_searches, .user_dashboard_back_arrow').removeClass('hide');

                    $('.user_dashboard_settings').addClass('hide');
                    $('.user_dashboard_profile').addClass('hide');
                    $('.user_dashboard_fav_properties').addClass('hide');
                    $('.user_dashboard_list').addClass('hide');

                    user.property.favourite.property.searches();

                } else if (type === 'property') {

                    $('.user_dashboard_fav_properties, .user_dashboard_back_arrow').removeClass('hide');

                    $('.user_dashboard_settings').addClass('hide');
                    $('.user_dashboard_fav_searches').addClass('hide');
                    $('.user_dashboard_profile').addClass('hide');
                    $('.user_dashboard_list').addClass('hide');

                    user.property.favourite.property.properties();

                } else if (type === 'alerts') {

                    $('.user_dashboard_fav_alerts, .user_dashboard_back_arrow').removeClass('hide');

                    $('.user_dashboard_settings').addClass('hide');
                    $('.user_dashboard_fav_searches').addClass('hide');
                    $('.user_dashboard_profile').addClass('hide');
                    $('.user_dashboard_list').addClass('hide');

                    user.property.alert.property.properties();

                }

            }
        }
    },
    drawer: {
        __init: function(language) {
            $('.ui.modal.mobile_property_search').modal('hide');
            $("#sidedrawer_mobile").sidebar("setting", {
                onVisible: function () {
                    $('body').addClass('opened-sidebar');
                },
                onHidden: function () {
                    $('body').removeClass('opened-sidebar');
                }
            }).sidebar("toggle");
        }
    },
    landing: {
        __init: function() {
            page = 'landing';
            $('.secondary.menu .item').tab();
            
            pages.landing.state();

            other.blog.listings();

            pages.landing.dropdown.__init();

            pages.landing.general_enquiry.__init();

            other.update_language();

            //CLICK SEARCH PROPERTY CRITERIA
            $( ".rent_buy_btn" ).click(function(e) {
                $('.rent_buy_btn').removeClass('active');
                $(this).addClass('active');
            });

            $( "#rent_buy_criteria" ).click(function(e) {
                $('#rent_buy_criteria .active').removeClass('active');
                $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');

            });

            var autolanding = document.getElementById('property_suburb_landing');

            new Awesomplete(autolanding, {
                list: searchJson.property.location,
                maxItems: 30,
                multiple: true,
                autoFirst: true,
                filter: function(text, input) {
                    return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                },
                item: function(text, input) {
                    return Awesomplete.ITEM(text, input.match(/[^,]*$/)[0]);
                },
                replace: function (text) {

                    var before_hidden = $('#property_suburb_landing_hidden').val();
                    var before_values = before_hidden.match(/^.+,\s*|/)[0];
                    var before = this.input.value.match(/^.+,\s*|/)[0];

                    selectedLoc = _.filter(searchJson.property.location, function(loc) {
                        return loc.value === text.value;
                    });

                    if(!_.includes(selectedLoc,'0.label')){
                        search.property.regular.store_variables.suburb.label = before + _.get(selectedLoc,'0.label') + ', ';
                        search.property.regular.store_variables.suburb.value = before_values + _.get(selectedLoc,'0.value') + ', ';
                        $('#property_suburb_landing_hidden').val(before_values + _.get(selectedLoc,'0.value') + ', ');
                    }

                    this.input.value = search.property.regular.store_variables.suburb.label;

                }
            });
        },
        state: function() {
            var state = helpers.user.state.is_logged_in();
            if (state[0]) {
                // Landing Login State
                /**
                * 1.   Header shows 'username'
                * 2.   Lead modals are populated with user details
                */
                // 1.
                $('.state_li').show();
                $('.state_lo').hide();
                // 2.
                helpers.populate.modals(state[1]);
                helpers.populate.header(state[1]);
                $('.item').tab();
            } else {
                $('.state_lo').show();
                $('.state_li').hide();
                helpers.depopulate.modals();
                helpers.depopulate.header();
            }
        },
        property: {
            call: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Api call
                    */
                    var user = JSON.parse(helpers.store.read('user_o'));

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_call_phone');
                    }, 500);

                    var agent_name  = _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : $(data_trigger).parent().attr("data-agent_contact_name");
                        agent_name  = !_.isEmpty(agent_name) ? agent_name : 'Agent name not available';
                    var agent_phone = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'agent.contact.phone') ? _.get(data_trigger, 'agent.contact.phone') : ( $(data_trigger).parent().attr("data-agent_contact_phone") ? $(data_trigger).parent().attr("data-agent_contact_phone") : 'Phone not available') );

                    var agency_name = _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : $(data_trigger).parent().attr("data-agency_contact_name");
                        agency_name = !_.isEmpty(agency_name) ? agency_name : 'Agency name not available';

                    var property_ref_no = _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : $(data_trigger).parent().attr("data-ref_no");
                        property_ref_no = !_.isEmpty(property_ref_no) ? property_ref_no : 'Reference number not available';

                    var property_id = _.has(data_trigger, '_id') ? _.get(data_trigger, '_id') : $(data_trigger).parent().attr("data-_id");
                        property_id = !_.isEmpty(property_id) ? property_id : 'ID not available';


                    var data = {
                        agent: {
                            name: agent_name,
                            phone: agent_phone
                        },
                        agency: {
                            name: agency_name
                        },
                        property: {
                            reference: property_ref_no,
                            _id: property_id
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        source: 'mobile'
                    }

                    if(!data.agent.phone.includes("+") && !_.startsWith(data.agent.phone,'0')){
                        $('#modal_property_call_agent_contact_phone').html('+' + data.agent.phone);
                    }
                    else if(_.startsWith(data.agent.phone,'0')){
                        $('#modal_property_call_agent_contact_phone').html(data.agent.phone.replace('0', '+971'));
                    } else {
                        $('#modal_property_call_agent_contact_phone').html(data.agent.phone);
                    }

                    //2
                    $('#modal_property_call_agent_name').html(data.agent.name);
                    $('#modal_property_call_agency_name').html(data.agency.name);
                    $('#modal_property_call_ref_no').html(data.property.reference);
                    $('#modal_property_call_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_call_data').attr('data-_id', data.property._id);
                    $('#modal_property_call_data').attr('data-subsource', data.subsource);

                    // window.location.href = 'tel:'+$('#modal_property_call_agent_contact_phone').html();

                    //3
                    $('.ui.modal.property_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    //4
                    var payload = {
                        action: 'call',
                        source: 'consumer',
                        subsource: 'mobile',
                        message: _.has(lang_static, 'modals.property.call.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.call.payload_msg'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.',
                        property: {
                            _id: data.property._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //storing the payload for call buton api call
                    lead.property.store.save = payload;

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','call_submit', data_trigger);

                    // var url = baseApiUrl + 'lead/property';
                    //
                    // axios.post(url, payload).then(function (response) {
                    //     //gtm push on submit
                    //     var property_data = helpers.store.temp_data.read();
                    //     other.datalayer('porperty','call_submit', property_data);
                    //
                    // }).catch(function (error) {});


                }
            },
            email: {
                __init: function(data_trigger) {
                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_email_phone');
                    }, 500);

                    var agent_name  = _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : $(data_trigger).parent().attr("data-agent_contact_name");
                        agent_name  = !_.isEmpty(agent_name) ? agent_name : 'Agent name not available';

                    var agent_picture  = _.has(data_trigger, 'agent.contact.picture') ? _.get(data_trigger, 'agent.contact.picture') : $(data_trigger).parent().attr("data-agent_contact_picture");
                        agent_picture  = !_.isEmpty(agent_picture) ? agent_picture : '';

                    var agency_name = _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : $(data_trigger).parent().attr("data-agency_contact_name");
                        agency_name = !_.isEmpty(agency_name) ? agency_name : 'Agency name not available';

                    var property_ref_no = _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : $(data_trigger).parent().attr("data-ref_no");
                        property_ref_no = !_.isEmpty(property_ref_no) ? property_ref_no : 'Reference number not available';

                    var property_id = _.has(data_trigger, '_id') ? _.get(data_trigger, '_id') : $(data_trigger).parent().attr("data-_id");
                        property_id = !_.isEmpty(property_id) ? property_id : 'ID not available';

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: agent_name,
                            picture: agent_picture
                        },
                        agency: {
                            name: agency_name
                        },
                        property: {
                            reference: property_ref_no,
                            _id: property_id
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_email_agent_img');

                    //2
                    $('#modal_property_email_agent_name').html(data.agent.name);
                    $('#modal_property_email_agency_name').html(data.agency.name);
                    $('#modal_property_email_textarea').html(_.has(lang_static, 'modals.property.email.message') ? helpers.trans_value(_.get(lang_static, 'modals.property.email.message'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on zoom property. Please contact me. Thank you.')
                    $('#modal_property_email_ref_no').html(data.property.reference);
                    $('#modal_property_email_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_email_data').attr('data-_id', data.property._id);
                    $('#modal_property_email_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.property_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','email_click', data_trigger);
                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_callback_phone');
                    }, 500);

                    //1
                    var data = {
                        subsource: 'mobile',
                        agent: {
                            name: _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : 'Agent name not available',
                            picture: _.has(data_trigger, 'agent.contact.picture') ? _.get(data_trigger, 'agent.contact.picture') : ''
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : 'Agency name not available'
                        },
                        property: {
                            reference: _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : 'reference number not available',
                            _id: _.get(data_trigger, '_id')
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_callback_agent_img');

                    //2
                    $('#modal_property_callback_agent_name').html(data.agent.name);
                    $('#modal_property_callback_agency_name').html(data.agency.name);
                    $('#modal_property_callback_textarea').html(_.has(lang_static, 'modals.property.callback.message') ? helpers.trans_value(_.get(lang_static, 'modals.property.callback.message'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.')
                    $('#modal_property_callback_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_callback_data').attr('data-_id', data.property._id);
                    $('#modal_property_callback_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.property_callback').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','callback_click', data_trigger);

                }
            },
            whatsapp: {
                __init: function(data_trigger) {

                    var wapp_phone_number = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'agent.contact.phone') ? _.get(data_trigger, 'agent.contact.phone') :'Phone not available');

                    if (wapp_phone_number == 'Phone not available') {
                        alert('Phone number not available');
                    } else {

                        var agency_email  = _.get(data_trigger, 'agent.agency.contact.email');
                        //NOTE TEMPORARY CHANGE TO TRACK WHATSAPP (REMOVE AFTER TRIAL COMPLETED)
                        wapp_phone_number = '+971529887539'

                        var user = JSON.parse(helpers.store.read('user_o'));

                        //1
                        var data = {
                            action: 'whatsapp',
                            source: 'consumer',
                            subsource: 'mobile',
                            message: _.has(lang_static, 'modals.property.call.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.call.payload_msg'), {reference: _.get(data_trigger, 'ref_no')}) : 'Hi, I found your property with ref: ' + _.get(data_trigger, 'ref_no') + ' on Zoom Property. Please contact me. Thank you.',
                            property: {
                                reference: _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : 'reference number not available',
                                _id: _.get(data_trigger, '_id')
                            },
                            user: {
                                contact: {
                                    name: user ? user.name : 'anonymous',
                                    email: user ? user.email : 'anonymous@domain.com',
                                    phone: user && _.has(user, 'user.phone') ? user.phone : ''
                                }
                            }
                        }

                        //whatsapp lead create api
                        lead.property.whatsapp.__init(data);
                        //gtm push on submit
                        helpers.store.temp_data.write = data_trigger;
                        other.datalayer('property','whatsapp_click', data_trigger);

                        window.open(
                            'whatsapp://send?text='+encodeURIComponent('Hi, I saw your advert on Zoom Property, ref '+data.property.reference+', please get back in touch')+'&phone=' + wapp_phone_number,
                            '_blank'
                        );
                    }
                }
            }
        },
        dropdown: {
            __init: function() {

                $('#property_rent_buy').dropdown({
                    values: pages.landing.dropdown.data.rent_buy,
                    showOnFocus: false,
                    placeholder: _.has(lang_static, 'global.rent_buy') ? _.get(lang_static, 'global.rent_buy') : 'Rent or Buy',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.rent_buy = [];
                            search.property.regular.store_variables.rent_buy.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                        } else {
                            pages.landing.dropdown.callback.rent_buy(value);
                        }
                    }
                });

                var auto = document.getElementById('property_suburb');

                new Awesomplete(auto, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    multiple: true,
                    autoFirst: true,
                    filter: function(text, input) {
                        return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                    },
                    item: function(text, input) {
                        return Awesomplete.ITEM(text, input.match(/[^,]*$/)[0]);
                    },
                    replace: function (text) {

                        var before_hidden = $('#property_suburb_hidden').val();
                        var before_values = before_hidden.match(/^.+,\s*|/)[0];
                        var before = this.input.value.match(/^.+,\s*|/)[0];

                        selectedLoc = _.filter(searchJson.property.location, function(loc) {
                            return loc.value === text.value;
                        });

                        if(!_.includes(selectedLoc,'0.label')){
                            search.property.regular.store_variables.suburb.label = before + _.get(selectedLoc,'0.label') + ', ';
                            search.property.regular.store_variables.suburb.value = before_values + _.get(selectedLoc,'0.value') + ', ';
                            $('#property_suburb_hidden').val(before_values + _.get(selectedLoc,'0.value') + ', ');
                        }

                        this.input.value = search.property.regular.store_variables.suburb.label;

                    }
                });

                $('#property_price_min').dropdown({
                    values: pages.landing.dropdown.data.price_sale_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.price.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_price_max', rent_type, 'price');
                        }
                    }
                });

                $('#property_price_max').dropdown({
                    values: pages.landing.dropdown.data.price_sale_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.regular.store_variables.price.max != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.price.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_price_min', rent_type, 'price');
                        }

                    }
                });

                $('#property_type').dropdown({
                    values: pages.landing.dropdown.data.property_type.residential.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.type') ? _.get(lang_static, 'form.placeholders.type') : 'Property Type',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.type = [];
                            search.property.regular.store_variables.type.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.type = [];
                        }
                    }
                });

                $('#property_bedroom_min').dropdown({
                    values: pages.landing.dropdown.data.bedroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bed') ? _.get(lang_static, 'form.placeholders.min.bed') : 'Min Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bedroom.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bedroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bedroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_bedroom_max', pages.landing.dropdown.data.bedroom_max, 'bedroom');
                        }
                    }
                });

                $('#property_bedroom_max').dropdown({
                    values: pages.landing.dropdown.data.bedroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bed') ? _.get(lang_static, 'form.placeholders.max.bed') : 'Max Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.regular.store_variables.bedroom.max != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.bedroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bedroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_bedroom_min', pages.landing.dropdown.data.bedroom_min, 'bedroom');
                        }
                    }
                });

                $('#property_bathroom_min').dropdown({
                    values: pages.landing.dropdown.data.bathroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bath') ? _.get(lang_static, 'form.placeholders.min.bath') : 'Min Bath',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.regular.store_variables.bathroom.min != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.bathroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bathroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_bathroom_max', pages.landing.dropdown.data.bathroom_max, 'bathroom');
                        }
                    }
                });

                $('#property_bathroom_max').dropdown({
                    values: pages.landing.dropdown.data.bathroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bath') ? _.get(lang_static, 'form.placeholders.max.bath') : 'Max Bath',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.regular.store_variables.bathroom.max != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.bathroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bathroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_bathroom_min', pages.landing.dropdown.data.bathroom_min, 'bathroom');
                        }
                    }
                });

                $('#property_area_min').dropdown({
                    values: pages.landing.dropdown.data.area_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.area') ? _.get(lang_static, 'form.placeholders.min.area') : 'Min Area',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.regular.store_variables.area.min != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.area.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.area.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_area_max', pages.landing.dropdown.data.area_max, 'area');
                        }
                    }
                });

                $('#property_area_max').dropdown({
                    values: pages.landing.dropdown.data.area_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.area') ? _.get(lang_static, 'form.placeholders.max.area') : 'Max Area',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.regular.store_variables.area.max != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.area.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.area.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_area_min', pages.landing.dropdown.data.area_min, 'area');
                        }
                    }
                });

                $('#property_furnishing').dropdown({
                    values: pages.landing.dropdown.data.furnishing,
                    placeholder: _.has(lang_static, 'form.placeholders.furnishing') ? _.get(lang_static, 'form.placeholders.furnishing') : 'Furnishing',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            // search.property.regular.store_variables.furnishing = [];
                            // search.property.regular.store_variables.furnishing.push(text);
                            search.property.regular.store_variables.query_string.furnishing =   parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            // search.property.regular.store_variables.furnishing = [];
                            search.property.regular.store_variables.query_string.furnishing =   undefined;
                        }
                    }
                });

                $('#completion_status').dropdown({
                    values: pages.landing.dropdown.data.completion_status,
                    placeholder: _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.property.regular.store_variables.query_string.status = [];
                            search.property.regular.store_variables.query_string.status.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.query_string.status = [];
                        }
                    }
                });
            },
            clear_area_field: function(){
                var property_suburb = document.getElementById('property_suburb');
                var property_suburb_hidden = document.getElementById('property_suburb_hidden');
                $(property_suburb).val('');
                $(property_suburb_hidden).val('');
            },
            callback: {
                from: function(value, id, path, type) {

                    $(id).dropdown('clear');

                    var clone = path,
                    array = clone.slice(0);

                    array.splice(0, helpers.indexof(array, "value", parseInt(value)));

                    $(id).empty();

                    if(value != 0){
                        array.unshift({name: $(id).dropdown('get text'), value: ''});
                    }

                    $.each(array, function(i, item) {
                        $(id).append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    if (!(($(id).dropdown('get text') == 'Max Bed') || ($(id).dropdown('get text') == 'Max Bath') || ($(id).dropdown('get text') == 'Max Area') || ($(id).dropdown('get text') == 'Max Price'))) {

                        if (_.includes($(id).dropdown('get text'), 'Sq. ft')) {
                            $(id).dropdown('set selected', $(id).dropdown('get text').match(/\d+/)[0]);
                        } else if (_.includes($(id).dropdown('get text'), 'Beds') || _.includes($(id).dropdown('get text'), 'Bed')) {
                            if ($(id).dropdown('get text') === 'Studio') {
                                $(id).dropdown('set selected', 0);
                            } else {
                                $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                            }
                        } else if (_.includes($(id).dropdown('get text'), 'Baths')) {
                            $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                        } else if (_.includes($(id).dropdown('get text'), 'AED')) {
                            var price_with_comma = $(id).dropdown('get text').substr($(id).dropdown('get text').indexOf(" ") + 1);
                            var remove_comma = parseFloat(price_with_comma.replace(/,/g, ''))
                            $(id).dropdown('set selected', remove_comma);
                        }

                    } else {
                        $(id).dropdown('clear');
                    }

                },
                to: function(value, id, path, type) {

                    // var clone = path,
                    //     array = clone.slice(0);
                    //
                    // array.splice(helpers.indexof(array, "value", parseInt(value)) + 1, array.length);
                    //
                    // $(id).empty();
                    //
                    // $.each(array, function(i, item) {
                    //     $(id).append($('<option>', {
                    //         value: item.value,
                    //         text: item.name
                    //     }))
                    // })
                    //
                    // if (!(($(id).dropdown('get text') == 'Min Bed') || ($(id).dropdown('get text') == 'Min Bath') || ($(id).dropdown('get text') == 'Min Area') || ($(id).dropdown('get text') == 'Min Price'))) {
                    //
                    //     if (_.includes($(id).dropdown('get text'), 'Sq. ft')) {
                    //         $(id).dropdown('set selected', $(id).dropdown('get text').match(/\d+/)[0]);
                    //     } else if (_.includes($(id).dropdown('get text'), 'Beds') || _.includes($(id).dropdown('get text'), 'Bed')) {
                    //         if ($(id).dropdown('get text') === 'Studio') {
                    //             $(id).dropdown('set selected', 0);
                    //         } else {
                    //             $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                    //         }
                    //     } else if (_.includes($(id).dropdown('get text'), 'Baths')) {
                    //         $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                    //     } else if (_.includes($(id).dropdown('get text'), 'AED')) {
                    //         var price_with_comma = $(id).dropdown('get text').substr($(id).dropdown('get text').indexOf(" ") + 1);
                    //         var remove_comma = parseFloat(price_with_comma.replace(/,/g, ''))
                    //         $(id).dropdown('set selected', remove_comma);
                    //     }
                    //
                    // } else {
                    //     $(id).dropdown('clear');
                    // }
                },
                rent_buy: function(value) {

                    if (value === 'Commercial-Rent' || value === 'Commercial-Sale') {
                        $("#property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max").dropdown().addClass("disabled");

                        $('#property_type').empty();

                        if (value === 'Commercial-Rent') {
                            $.each(pages.landing.dropdown.data.property_type.commercial.rent, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        } else {
                            $.each(pages.landing.dropdown.data.property_type.commercial.sale, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        }

                        $('#property_type, #property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max').dropdown('clear');

                    } else {
                        $("#property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max").dropdown().removeClass("disabled");

                        $('#property_type').empty();

                        if (value === 'Rent') {
                            $.each(pages.landing.dropdown.data.property_type.residential.rent, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        } else {
                            $.each(pages.landing.dropdown.data.property_type.residential.sale, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        }

                        $('#property_type, #property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max').dropdown('clear');

                    }

                    if (value === 'Sale' || value === 'Commercial-Sale') {

                        $('#property_price_min, #property_price_max').empty();

                        $.each(pages.landing.dropdown.data.price.sale, function(i, item) {
                            $('#property_price_min, #property_price_max').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $('#property_price_min, #property_price_max').dropdown('clear');

                    } else if (value === 'Rent' || value === 'Commercial-Rent' || value === 'Short-term-Rent') {

                        $('#property_price_min, #property_price_max').empty();

                        $.each(pages.landing.dropdown.data.price.rent, function(i, item) {
                            $('#property_price_min, #property_price_max').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $('#property_price_min, #property_price_max').dropdown('clear');

                    }
                }
            },
            data: helpers.unshift(searchJson.property)
        },
        newsletter: {
            __init: function(trigger) {
                user.newsletter.__init(trigger);
            }
        },
        search: {
            __init: function(rent_buy = '') {
                console.log(rent_buy);
                
                rent_buy_landing = $('.rent_buy_btn.active').attr('id');

                console.log(rent_buy_landing);

                $('#rent_buy_criteria .active').removeClass('active');
                $('#rent_buy_criteria .' + rent_buy).addClass('active');

                $('#property_suburb').val($('#property_suburb_landing').val());
                $('#property_suburb_hidden').val($('#property_suburb_landing_hidden').val());

                $('.ui.modal.mobile_property_search').modal('setting', 'transition', 'vertical flip').modal('show');
                $('#modal_mobile_property_search').parent().css('padding', 0);
            }
        },
        general_enquiry: {
            __init: function() {

                var location_field = document.getElementById('ge_location'),
                    user = JSON.parse(helpers.store.read('user_o'));

                new Awesomplete(location_field, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    autoFirst: true
                });

                $('div.ge.dropdown').dropdown();

                helpers.phonenumber.init('ge_phone');

                var data = {
                    name: _.has(user, 'name') ? user.name : '',
                    email: _.has(user, 'email') ? user.email : '',
                }

                $('#ge_name').val(data.name);
                $('#ge_email').val(data.email);

                validate.rules.lead.general_enquiry.__init();
            },
            open: function() {

                $('#modal_general_enquiry').modal('setting', 'transition', 'fade').modal('show');
                // $('#modal_mobile_property_search').parent().css('padding', 0);
            }
        },
        quick_access:   {
            __init      :   function(el){

                search.property.regular.store_variables.rent_buy = [ $(el).data('rent_buy') ];
                search.property.regular.store_variables.type = [ $(el).data('type') ];
                var price_min = $(el).data('min'),
                    price_max = $(el).data('max');
                if( price_min !== '' ) {
                    search.property.regular.store_variables.price.min = price_min;
                }
                if( price_max !== '' ) {
                    search.property.regular.store_variables.price.max = price_max;
                }

                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

                var qrsObj_prev     =   {};
                if(_.has(qrsObj_url_prev,'agency_id')){
                    qrsObj_prev.agency_id   =   qrsObj_url_prev.agency_id;
                }
                if(_.has(qrsObj_url_prev,'sort')){
                    qrsObj_prev.sort        =   qrsObj_url_prev.sort;
                }

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );

                window.location.href =  url.toString();

            }
        }
    },
    signin: {
        modal: {
            __init: function() {
                $('.ui.modal.mobile_property_search').modal('hide');
                $('.ui.modal.mobile_signin_register').modal('setting', 'transition', 'vertical flip').modal('show');
                $('#modal_mobile_signin_register').parent().css('padding', 0);
                $('.tabular.menu .item').tab();
                $('.item_forgot_password').tab();
                $('.item_back_to_signin').tab();
            }
        }
    },
    property: {
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();

                //pagination init
                helpers.pagination.__init(true);

                //sorting init
                helpers.sort.__init();

                // Lazy Load
                helpers.lazy_load();

                pages.landing.dropdown.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);

                setTimeout(function() {
                    $('#currency_dropdown').dropdown({
                        onChange: function(value, text, $selectedItem) {
                            other.change_currency_mobile(text, value );
                        }
                    });
                }, 1000);

                other.update_language();

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                $('.ui.sticky').sticky({
                    context: '#pages_property_listings'
                });

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
    
                });

                var autolistings = document.getElementById('property_suburb_listings');

                new Awesomplete(autolistings, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    multiple: true,
                    autoFirst: true,
                    filter: function(text, input) {
                        return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                    },
                    item: function(text, input) {
                        return Awesomplete.ITEM(text, input.match(/[^,]*$/)[0]);
                    },
                    replace: function (text) {

                        var before_hidden = $('#property_suburb_listings_hidden').val();
                        var before_values = before_hidden.match(/^.+,\s*|/)[0];
                        var before = this.input.value.match(/^.+,\s*|/)[0];

                        selectedLoc = _.filter(searchJson.property.location, function(loc) {
                            return loc.value === text.value;
                        });

                        if(!_.includes(selectedLoc,'0.label')){
                            search.property.regular.store_variables.suburb.label = before + _.get(selectedLoc,'0.label') + ', ';
                            search.property.regular.store_variables.suburb.value = before_values + _.get(selectedLoc,'0.value') + ', ';
                            $('#property_suburb_listings_hidden').val(before_values + _.get(selectedLoc,'0.value') + ', ');
                        }

                        this.input.value = search.property.regular.store_variables.suburb.label;

                    }
                });

                Awesomplete.$.bind(autolistings, { "awesomplete-selectcomplete": selectProperty });

                function selectProperty (el){
                    console.log(el);
                    console.log("Property Label");
                    console.log(el.text['label']);
                    console.log("Property Value");
                    console.log(el.text['value']);
                    search.property.regular.find(el.text);
                }
            },
            search: {
                __init: function(rent_buy='Sale') {

                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + rent_buy).addClass('active');

                    $('#property_suburb').val($('#property_suburb_listings').val());
                    $('#property_suburb_hidden').val($('#property_suburb_listings_hidden').val());

                    $('.ui.modal.mobile_property_search').modal('setting', 'transition', 'fade').modal('show');
                    $('#modal_mobile_property_search').parent().css('padding', 0);
                }
            },
        },
        details: {
            slider: {
                __init: function() {

                    $('.slider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        lazyLoad: 'ondemand',
                        dots: false,
                        infinite: true,
                        appendDots: '.slider-nav',
                        appendArrows: '.slider-arrows',
                        nextArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--left"><i class="chevron right icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--right"><i class="chevron right icon"></i></a>',
                        prevArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--right"><i class="chevron left icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--left"><i class="chevron left icon"></i></a>'
                    });
                }
            },
            map: {
                __init: function(lat, lng) {
                    var center = {};

                    center.lat = Number(lat);
                    center.lng = Number(lng);

                    // No location overlay
                    if (center.lat === 0 || center.lng === 0) {
                        $('.no_loc').show();
                    } else {
                        g_map.__init(center, [], 'property_details_map');
                        $('.ui.checkbox').checkbox();
                    }
                    return;
                }
            },
            info: {
                __init: function() {

                    var suburb = $('#pages_property_details_data').attr('data-_area');

                    if(suburb === 'jumeirah-lake-towers(-j-l-t)'){
                        suburb = 'jumeirah-lake-towers';
                    }

                    var url = envUrl + 'en/blog/guide/' + suburb;

                    axios.get(url).then(function(response) {
                        var data;

                        if (response.data.length < 5) {
                            $('.local_info_show').addClass('hide');
                        } else {
                            $('.local_info_show').removeClass('hide');
                            data = response.data;
                            $('#property_details_local_info').html(data);
                        }

                    }).catch(function(error) {});

                }
            },
            mortgage: {
                __init: function(trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('mortgage_user_phone');
                    }, 500);

                    $('.ui.modal.property_mortgage').modal('setting', 'transition', 'vertical flip').modal('show');

                    var years           = Number($('#emi_years').val().split(',').join('')),
                        down_payment    = Number($('#emi_down_payment').val().split(',').join('')),
                        interest        = Number($('#emi_interest').val().split(',').join('')),
                        principal       = $('#purchase_price').html().replace(/,/g, '');
                        finance_amount  = principal - down_payment;

                    //Computing EMI
                    var i               = interest / 100 / 12,
                        n               = years * 12,
                        monthly         = (principal - down_payment) * (1 - i) * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1),
                        total_amount    = monthly * years * 12;

                    $('#project_loan_amount').html($('#purchase_price').html());
                    $('#project_emi_monthly').text(Math.ceil(monthly).toLocaleString('en'));
                    $('#project_emi_down_payment').text('AED ' + Math.ceil(down_payment).toLocaleString('en'));
                    $('#project_emi_total_amount').text('AED ' + Math.ceil(total_amount).toLocaleString('en'));
                    $( '#project_emi_total_interest' ).text( 'AED ' + Math.ceil( total_amount - principal ).toLocaleString('en') );
                    $('#project_emi_term').text(years);
                    $('#project_emi_finance_amount').text('AED ' + finance_amount.toLocaleString('en'));

                    $('#mortgage_lead').attr('data-model', $(trigger).attr('data-model'));
                }
            },
            dewa: {
                tabs: {
                    energy: {
                        __init: function(){
                            var monthly = parseInt($('#property_monthly_value').html()),
                            elec_cost   =   parseInt($('#monthly_cost_energy_electricity').val()),
                            gas_cost    =   parseInt($('#monthly_cost_energy_gas').val());

                            $('#monthly_cost_energy_electricity, #monthly_cost_energy_gas').change(function(trigger) {

                                var energy = elec_cost + gas_cost,
                                minus = monthly - energy;

                                var new_monthly = minus + parseInt($('#monthly_cost_energy_electricity').val()) + parseInt($('#monthly_cost_energy_gas').val());

                                $('#property_monthly_value').html(Math.round(new_monthly));
                            });

                        }
                    },
                    water: {
                        __init: function(){
                            var monthly = parseInt($('#property_monthly_value').html()),
                            water_cost   =   parseInt($('#monthly_cost_water').val());

                            $('#monthly_cost_water').change(function() {

                                var minus = monthly - water_cost;

                                var new_monthly = minus + parseInt($('#monthly_cost_water').val());

                                $('#property_monthly_value').html(Math.round(new_monthly));

                            });

                        }
                    }
                },
                sale : {
                    __init: function(){

                        var years = Number($('#emi_years').val().split(',').join('')),
                        down_payment = Number($('#emi_deposit').val().split(',').join('')),
                        interest = Number($('#emi_interest').val().split(',').join('')),
                        principal = $('#purchase_price').html().replace(/,/g, ''),
                        energy_electricity = !_.isEmpty($('#monthly_cost_energy_electricity').val()) ? parseInt($('#monthly_cost_energy_electricity').val()) : 0,
                        energy_gas = !_.isEmpty($('#monthly_cost_energy_gas').val()) ? parseInt($('#monthly_cost_energy_gas').val()) : 0,
                        energy = energy_electricity + energy_gas,
                        water = parseInt($('#monthly_cost_water').val());


                        //Computing EMI
                        var i = interest / 100 / 12,
                        n = years * 12,
                        monthly = (principal - down_payment) * (1 - i) * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1) + energy + water,
                        total_amount = monthly * years * 12;

                        $('#project_loan_amount').html($('#purchase_price').html());
                        $('#project_emi_monthly').text(Math.ceil(monthly).toLocaleString('en'));
                        $('#project_emi_down_payment').text('AED ' + Math.ceil(down_payment).toLocaleString('en'));
                        $('#project_emi_total_amount').text('AED ' + Math.ceil(total_amount).toLocaleString('en'));
                        $('#project_emi_total_interest' ).text( 'AED ' + Math.ceil( total_amount - principal ).toLocaleString('en') );
                        $('#project_emi_term').text(years);

                        $('#property_monthly_value').html(Math.round(monthly).toLocaleString('en'));

                    }
                },
                rent: {
                    __init: function(){
                        //TODO add cheque calculator
                    }
                }
            },
            __init: function() {
                page = 'landing';
                pages.landing.state();

                pages.landing.dropdown.__init();

                $('#sidedrawer_mobile').sidebar('hide');

                setTimeout(function() {
                    $('#currency_dropdown').dropdown({
                        onChange: function(value, text, $selectedItem) {
                            other.change_currency_mobile(text, value );
                        }
                    });
                }, 1000);

                other.update_language();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);

                $('.secondary.menu .item').tab();
                helpers.lazy_load();
                //Property local info
                // pages.property.details.info.__init();
                setTimeout(function(){
                    pages.property.details.slider.__init();
                }, 1200);

                setTimeout(function(){

                    let url =   new URI( window.location.href );
                    pages.property.details.dewa.sale.__init();

                    $('#emi_deposit, #emi_years, #emi_interest').change(function() {
                        pages.property.details.dewa.sale.__init();
                    });

                }, 2000);

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
    
                });

                //Property similar properties
                search.property.similar($('#similar_properties').attr('data-_id'), 'similar_properties');
            }
        }
    },
    projects: {
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();

                pages.projects.dropdown.__init();

                pages.landing.dropdown.__init();

                //pagination init
                helpers.pagination.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);

                // Initiate Lazy Load
                helpers.lazy_load();

                other.update_language();

                $('#sticky_header').sticky();

                $('.progress').progress();

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
                });

            },
            search: {
                __init: function() {

                }
            },
        },
        details: {
            __init: function() {
                page = 'landing';
                pages.landing.state();

                //pages.projects.dropdown.__init();
                pages.landing.dropdown.__init();
                pages.projects.details.slider.__init();

                //Tabs init
                $('#main-tab .item').tab();
                $('#multimedia .item').tab();

                //Accordin init for floor plans
                // $('.ui.accordion').accordion();
                // Initiate Lazy Load
                helpers.lazy_load();

                pages.landing.dropdown.__init();

                other.update_language();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);

                setTimeout(function() {
                    helpers.phonenumber.init('project_lead_phone');
                }, 500);

                // Map init
                pages.projects.details.map.__init($('#projects_details_map').attr('data-lat'), $('#projects_details_map').attr('data-lng'));
                pages.projects.details.map.__init($('#projects_details_enlarged_map').attr('data-lat'), $('#projects_details_enlarged_map').attr('data-lng'), 'projects_details_enlarged_map');

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
    
                });

            },
            slider: {
                __init: function() {

                    $('#galleryslider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        lazyLoad: 'ondemand',
                        dots: false,
                        infinite: true,
                        appendArrows: '.slider-arrows',
                        nextArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--left"><i class="chevron right icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--right"><i class="chevron right icon"></i></a>',
                        prevArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--right"><i class="chevron left icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--left"><i class="chevron left icon"></i></a>'
                    });

                    $('#paymentslider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                        infinite: true,
                        autoplay: true,
                        appendDots: '.slider-nav'
                    });

                }
            },
            map: {
                __init: function(lat, lng, container='projects_details_map') {

                    var center = {};

                    center.lat = Number(lat);
                    center.lng = Number(lng);

                    // No location overlay
                    if (center.lat === 0 || center.lng === 0) {
                        $('.no_loc').show();
                    } else {
                        g_map.__init(center, [], container);
                        $('.ui.checkbox').checkbox();
                    }
                    return;
                }
            },
            call: {
                __init: function(data_trigger) {

                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') : 'Phone not available',
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        subsource: 'mobile'
                    }
                    $('#modal_developer_call_developer_contact_phone').html(data.developer.phone);

                    $('#modal_developer_call_project_name').html(data.project.name);
                    $('#modal_developer_call_contact_name').html(data.developer.name);
                    $('#modal_developer_call_data').attr('data-_id', data.project._id);
                    $('#modal_developer_call_data').attr('data-subsource', data.subsource);

                    $('.ui.modal.developer_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','call_click', data_trigger);
                    other.datalayer('project','call_submit', data_trigger);

                    lead.project.call.__init();
                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_developer_callback_phone');
                    }, 500);

                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Agent name not available'
                        },
                        agency: {
                            name: _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Agent name not available'
                        },
                        project: {
                            _id: _.get(data_trigger, '_id')
                        }
                    }


                    $('#modal_developer_callback_agent_name').html(data.agent.name);
                    $('#modal_developer_callback_agency_name').html(data.agency.name);
                    $('#modal_developer_callback_data').attr('data-_id', data.project._id);

                    $('.ui.modal.developer_callback').modal({
                        onHide: function(){
                            helpers.phonenumber.init('project_lead_phone');
                        },
                    }).modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','callback_click', data_trigger);

                }
            },
            email: {
                __init: function(data_trigger) {
                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_developer_email_phone');
                    }, 500);

                    //1
                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') :'Phone not available')
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        subsource: 'desktop'
                    }

                    // helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_email_agent_img');

                    //2
                    $('#modal_developer_email_project_name').html(data.project.name);
                    $('#modal_developer_email_contact_name').html(data.developer.name);
                    $('#modal_developer_email_ref_no').html(data.project.reference);
                    $('#modal_developer_email_data').attr('data-ref_no', data.project.reference);
                    $('#modal_developer_email_data').attr('data-_id', data.project._id);
                    $('#modal_developer_email_data').attr('data-subsource', data.subsource);

                    //3
                    $('.ui.modal.developer_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','email_click', data_trigger);
                }
            },
            whatsapp: {
                __init: function(data_trigger) {


                    var name = _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Agent name not available';

                    var wapp_phone_number  = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') :'Phone not available');
                    //NOTE TEMPORARY CHANGE TO TRACK WHATSAPP (REMOVE AFTER TRIAL COMPLETED)
                    wapp_phone_number = '+971529887539'

                    var user = JSON.parse(helpers.store.read('user_o'));

                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') : 'Phone not available',
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        subsource: 'mobile'
                    }

                    var payload = {
                        action: 'whatsapp',
                        source: 'consumer',
                        subsource: 'mobile',
                        message: _.has(lang_static, 'modals.project.whatsapp.payload_msg') ? _.get(lang_static, 'modals.project.whatsapp.payload_msg', {name: data.project.name}) : 'Hi, I saw your advert on Zoom Property, project: '+data.project.name+', please get back in touch',
                        project: {
                            _id: data.project._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //whatsapp lead create api
                    lead.project.whatsapp.__init(payload);

                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','whatsapp_submit', data_trigger);
                    window.open(
                        'whatsapp://send?text='+encodeURIComponent('Hi, I saw your advert on Zoom Property, '+_.get(data_trigger, 'name')+', please get back in touch')+'&phone=' + wapp_phone_number,
                        '_blank'
                    );
                }
            }
        },
        dropdown: {
            __init: function() {

                setTimeout(function() {
                    $('#projects_project').dropdown({
                        values: _.orderBy(pages.projects.dropdown.data.developers,'name','asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.search_developer') ? _.get(lang_static, 'form.placeholders.search_developer') : 'Search for Developer',
                        maxSelections: 1,
                        action: function(text, value, element) {
                            $('#search').attr('data-stub', value);
                            $(this).dropdown('set selected', value).dropdown('hide');
                        }
                    });
                }, 1000);

            },
            data: searchJson.project
        }
    },
    budget: {
        landing: {
            __init: function() {
                page = 'landing';
                pages.landing.state();

                pages.budget.dropdown.__init();

                // pages.landing.dropdown.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);
            }
        },
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();

                pages.budget.dropdown.__init();

                // search.property.budget.deconstruct.url();
                search.property.budget.listings.__init();

                pages.landing.dropdown.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.budget.prepopulate();
                }, 2000);
            },
            sort: {
                __init: function(_class, trigger) {
                    $('.p_c').hide();
                    $('.' + _class).show();
                    //TODO Area in message uppercase
                    helpers.toast.__init( _.get(lang_static, 'toast.showing_results') + _class.replace(/-/g, " "));
                    $('.budget_sort_btn').removeClass('active');
                    $(trigger).addClass('active');
                    $('.ui.modal.refine_search').modal('hide');
                }
            },
            refine: {
                __init: function(trigger) {
                    //TODO feed data to inputs
                    $('.ui.modal.refine_search').modal('show');
                    $('#modal_refine_search').parent().css('padding', 0);

                }
            }
        },
        dropdown: {
            __init: function() {

                $('#budget_city').dropdown({
                    values: pages.landing.dropdown.data.cities,
                    placeholder: _.has(lang_static, 'global.emirate') ? _.get(lang_static, 'global.emirate') : 'Emirate',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.budget.store_variables.city = [];
                            search.property.budget.store_variables.city.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                    }
                });

                $('#property_rent_buy').dropdown({
                    values: pages.landing.dropdown.data.rent_buy,
                    placeholder: _.has(lang_static, 'global.rent_buy') ? _.get(lang_static, 'global.rent_buy') : 'Rent or Buy',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.rent_buy = [];
                            search.property.budget.store_variables.rent_buy.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                    }
                });

                $('#property_price_min').dropdown({
                    values: pages.landing.dropdown.data.price.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.budget.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price.sale
                        } else {
                            var rent_type = pages.landing.dropdown.data.price.rent
                        }
                        pages.landing.dropdown.callback.from(value, '#property_price_max', rent_type, 'price');
                    }
                });

                $('#property_price_max').dropdown({
                    values: pages.landing.dropdown.data.price.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                        (search.property.budget.store_variables.price.max != parseInt(value)) &&
                        !_.isEmpty(value)
                        ) {
                            search.property.budget.store_variables.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price.sale
                        } else {
                            var rent_type = pages.landing.dropdown.data.price.rent
                        }
                        pages.landing.dropdown.callback.to(value, '#property_price_min', rent_type, 'price');
                    }
                });

            }
        }
    },
    services: {
        __init: function() {
            page = 'landing';
            pages.landing.state();

            // $('.ui.sticky').sticky({context: '#listings_wrapper'});
            pages.services.dropdown.__init();

            pages.landing.dropdown.__init();

            other.update_language();

            // Prepopulate search
            setTimeout(function() {
                search.property.regular.prepopulate();
            }, 1500);
        },
        dropdown: {
            __init: function() {
                setTimeout(function() {
                    $('#services_service').dropdown({
                        values: pages.services.dropdown.data.list,
                        placeholder: _.has(lang_static, 'form.placeholders.search_service') ? _.get(lang_static, 'form.placeholders.search_service') :'Search for a service',
                        maxSelections: 6,
                        action: function(text, value, element) {
                            $("#search").attr('data-_id', value);
                            $(this).dropdown('set selected', value).dropdown('hide');
                        }
                    });
                }, 1000);
            },
            data: searchJson.service
        },
        listings: {
            contact: {
                __init: function(trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_services_contact_user_phone');
                    }, 500);

                    //1
                    var data = {
                        contact: {
                            name: $(trigger).parent().attr("data-service_contact_name") ? $(trigger).parent().attr("data-service_contact_name") : 'Service name not available',
                            logo: $(trigger).parent().attr("data-service_logo") ? $(trigger).parent().attr("data-service_logo") : 'Logo not available',
                        },
                        writeup: {
                            description: $(trigger).parent().attr("data-service_description") ? $(trigger).parent().attr("data-service_description") : 'Description not available'
                        },
                        _id: $(trigger).parent().attr("data-service_id")
                    }

                    //2
                    $('#modal_services_contact_name').html(data.contact.name);
                    $('#modal_services_contact_logo').attr('src', data.contact.logo);
                    $('#modal_services_contact_description').html(data.writeup.description);
                    $('#modal_services_contact_data').attr('data-service_id', data._id);
                    $('#modal_services_contact_data').attr('data-subsource', 'mobile');

                    //3
                    $('.ui.modal.services_contact').modal('setting', 'transition', 'vertical flip').modal('show');

                    helpers.store.temp_data.write = data;

                    other.datalayer('service', 'contact_click', data);

                    other.update_language();

                }
            }
        }
    },
    agent: {
        landing: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                pages.agent.dropdown.__init();

                pages.landing.dropdown.__init();

                $('.popup-video-youtube').magnificPopup({
                    type: 'iframe'
                });

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
    
                });

                other.update_language();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);
            }
        },
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.agent.dropdown.__init();

                pages.landing.dropdown.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);

                // Lazy Load
                helpers.lazy_load();

                other.update_language();

                //pagination init
                helpers.pagination.__init(true);

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
    
                });
            },
            call: {
                __init: function(data_trigger) {
                  /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */
                    var user = JSON.parse(helpers.store.read('user_o'));
                    //1
                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : 'Phone not available'
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        _id: _.get(data_trigger, '_id')
                    }

                    if(!data.agent.phone.includes("+") && !_.startsWith(data.agent.phone,'0')){
                        $('#modal_agent_call_agent_contact_phone').html('+' + data.agent.phone);
                    }
                    else if(_.startsWith(data.agent.phone,'0')){
                        $('#modal_agent_call_agent_contact_phone').html(data.agent.phone.replace('0', '+971'));
                    } else {
                        $('#modal_agent_call_agent_contact_phone').html(data.agent.phone);
                    }

                    $('#modal_agent_call_agent_name').html(data.agent.name);
                    $('#modal_agent_call_agency_name').html(data.agency.name);

                    $('.ui.modal.agent_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    //4
                    var payload = {
                        action: 'call',
                        source: 'consumer',
                        subsource: 'mobile',
                        message: _.has(lang_static, 'modals.agent.call.payload_msg') ? _.get(lang_static, 'modals.agent.call.payload_msg') : 'Hi, I found your profile on Zoom Property. Please contact me. Thank you.',
                        agent: {
                            _id: data._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //storing the payload for call buton api call
                    lead.agent.store.save = payload;

                    payload.agent_name = $(data_trigger).parent().attr("data-agent_contact_name");
                    payload.agency_name = $(data_trigger).parent().attr("data-agency_contact_name");

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','call_submit', data_trigger);

                    lead.agent.call.__init();

                }
            },
            email: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_agent_email_phone');
                    }, 500);

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */

                    //1
                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            picture: _.has(data_trigger, 'contact.picture') ? _.get(data_trigger, 'contact.picture') : '',
                            _id: _.get(data_trigger, '_id')
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_agent_email_agent_img');

                    //2
                    $('#modal_agent_email_agent_name').html(data.agent.name);
                    $('#modal_agent_email_agency_name').html(data.agency.name);
                    $('#modal_agent_email_data').attr('data-_id', data.agent._id);

                    //3
                    $('.ui.modal.agent_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','email_click', data_trigger);

                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_agent_callback_phone');
                    }, 500);

                    //1
                    var data = {
                        subsource: 'mobile',
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        }
                    }

                    //2
                    $('#modal_agent_callback_agent_name').html(data.agent.name);
                    $('#modal_agent_callback_agency_name').html(data.agency.name);
                    // $('#modal_agent_callback_textarea').html('Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.')
                    $('#modal_agent_callback_data').attr('data-_id', data.agent._id);
                    $('#modal_agent_callback_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.agent_callback').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','callback_click', data_trigger);
                },
            },
            whatsapp: {
                __init: function(data_trigger) {

                    var wapp_phone_number = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'contact.phone') ? _.get(data_trigger, 'contact.phone') :'Phone not available');

                    if (wapp_phone_number == 'Phone not available') {
                        alert('Phone number not available');
                    } else {

                        //NOTE TEMPORARY CHANGE TO TRACK WHATSAPP (REMOVE AFTER TRIAL COMPLETED)
                        wapp_phone_number = '+971529887539'

                        var user = JSON.parse(helpers.store.read('user_o'));

                        //1
                        var data = {
                            agent: {
                                name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                                phone: _.has(data_trigger, 'contact.phone') ? _.get(data_trigger, 'contact.phone') : 'Phone not available'
                            },
                            agency: {
                                name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                            },
                            user: {
                                name: user ? user.name : 'anonymous',
                                email: user ? user.email : 'anonymous@domain.com',
                                phone: user && _.has(user, 'user.phone') ? user.phone : ''
                            },
                            _id: _.get(data_trigger, '_id')
                        }

                        var payload = {
                            action: 'whatsapp',
                            source: 'consumer',
                            subsource: 'mobile',
                            message: _.has(lang_static, 'modals.agent.whatsapp.payload_msg') ? _.get(lang_static, 'modals.agent.whatsapp.payload_msg') : 'Hi, I found your profile on Zoom Property. Please contact me. Thank you.',
                            agent: {
                                _id: data._id
                            },
                            user: {
                                contact: {
                                    name: data.user.name,
                                    email: data.user.email,
                                    phone: data.user.phone
                                }
                            }
                        }

                        if (payload.user.contact.phone.length === 0) {
                            delete payload.user.contact.phone
                        }

                        //whatsapp lead create api
                        lead.agent.whatsapp.__init(payload);
                        //gtm push on click submit
                        helpers.store.temp_data.write = data_trigger;
                        other.datalayer('agent','whatsapp_submit', data_trigger);

                        window.open(
                            'whatsapp://send?text='+encodeURIComponent('Hi, I saw your advert on Zoom Property, , please get back in touch')+'&phone=' + wapp_phone_number,
                            '_blank'
                        );
                    }
                }
            }
        },
        details: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                /**
                * 1.   Load Properties for agent
                * 2.   Populate popup with Agent Data
                */
                // 1.
                var sink = 'agent_properties';
                var query = new search.property.regular.variables();
                query.agent._id = $('#agent_properties').attr('data-_id');
                query.size = 3;
                search.property.listings(query, sink);

                pages.landing.dropdown.__init();

                other.update_language();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 1500);

                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.agent.dropdown.__init();

                $( "#rent_buy_criteria" ).click(function(e) {
                    $('#rent_buy_criteria .active').removeClass('active');
                    $('#rent_buy_criteria .' + $(e.target).prop('class')).addClass('active');
    
                });
                $('.rating').rating('disable');
            }
        },
        dropdown: {
            __init: function() {

                setTimeout(function() {

                    $('#agent_brokerage').dropdown({
                        values: _.orderBy(pages.agent.dropdown.data.list,'name','asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.brokerage') ? _.get(lang_static, 'form.placeholders.brokerage') : 'Brokerage',
                        maxSelections: 1,
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                                search.agent.store_variables.agency.push(value);
                                search.agent.store_variables.agency = [_.uniq(search.agent.store_variables.agency).pop()];
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if(!value){
                                $(this).dropdown('clear');
                                search.agent.store_variables.agency = undefined;
                            }
                        }
                    });

                    $('#agent_areas').dropdown({
                        values: _.orderBy(pages.agent.dropdown.data.areas,'name','asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.areas_buildings') ? _.get(lang_static, 'form.placeholders.areas_buildings') : 'Areas and Buildings',
                        maxSelections: 1,
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                                areas = value.split(',');
                                locations = areas[areas.length - 1];
                                search.agent.store_variables.area.push(locations);
                                search.agent.store_variables.area = [_.uniq(search.agent.store_variables.area).pop()];
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if(!value){
                                $(this).dropdown('clear');
                                search.agent.store_variables.area = undefined;
                            }
                        }
                    });

                }, 1000);

            },
            data: searchJson.brokerage
        }
    },
    reset_password: {
        change_password: function(trigger){

            $(trigger).addClass('loading');

            var password = $('#reset_password_new_password > input').val(),
            confirmPassword = $("#reset_password_confirm_password > input").val();

            $('#change_password_error').html('');

            var url_token = window.location.href,
            parts = url_token.split('/'),
            token = parts.pop();

            var url = baseApiUrl + "user/auth/reset";

            var payload = {
                email: $('#reset_password_email > input').val(),
                password: password,
                password_confirmation: confirmPassword,
                token: token
            }

            axios.post(url, payload).then(function(response){

                $(trigger).removeClass('loading');

                helpers.toast.__init( _.get(lang_static, 'toast.success_password_changed') );

                $('#reset_password_new_password > input').val('');
                $('#reset_password_confirm_password > input').val('');
                $("#reset_password_email > input").val('');

            }).catch(function (error) {

                $(trigger).removeClass('loading');

                if(error.response.data.errors){
                    // error.response.data.errors.email
                    var errs    =   error.response.data.errors;
                    var errors  =   '';
                    for(var i in errs){
                        errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                    }
                    $('#change_password_error').html('<div class="ui error message">'+errors+'</div>');
                }else{
                    $('#change_password_error').html(error.response.data.message);
                }
            });

        },
        __init: function() {

        }
    },
    components: {
        modal: {
            signin: {
                toggle: {
                    __init: function(id, trigger) {
                    }
                }
            }
        }
    }
}
