var guide_me = {
    __init: function() {
        guide_me.methods.height = $(window).height();
        guide_me.methods.dropdown.__init();
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
                guide_me.map.user.coordinates.lat = Number(position.coords.latitude);
                guide_me.map.user.coordinates.lng = Number(position.coords.longitude);
                guide_me.map.__init();
            }, function(){
                alert( _.get(lang_static, 'guideme.enable_location') );
                guide_me.map.__init();
            });
        } else {
            alert( _.get(lang_static, 'guideme.enable_location') );
        }
        guide_me.g_map_autocomplete.__init();
    },
    g_map_autocomplete:{
        __init: function(){
            guide_me.g_map_autocomplete.dropdown();
        },
        dropdown: function(){

            new Awesomplete(document.getElementById('user-location'), {
                list: guide_me.locationsJSON,
                maxItems: 30,
                multiple: false,
                autoFirst: false,
                replace: function(suggestion) {
                    var place_name = suggestion.label;
                    var place_cord = suggestion.value.split(',');
                    this.input.value = place_name;
                    guide_me.map.coordinates.center.lat   = place_cord[0];
                    guide_me.map.coordinates.center.lng   = place_cord[1];
                    guide_me.map.coordinates.location     = place_name;
                    guide_me.map.view = 0;
                    guide_me.map.oms.unspiderfy();
                    guide_me.invoke.listings.__init( guide_me.map.meters, guide_me.methods.refine.data, guide_me.methods.refine.payload );
                    guide_me.methods.show_map();
                }
            });

        },
        // map: function(map){
        //     var options = {
        //         componentRestrictions: {country: "ae"}
        //     };
        //     var autocomplete = new google.maps.places.Autocomplete(document.getElementById('user-location'), options);
        //     google.maps.event.addListener(map,'bounds_changed', function() {
        //         autocomplete.bindTo(map, 'bounds');
        //     });
        //     autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        //     autocomplete.addListener('place_changed', function() {
        //         var place = autocomplete.getPlace();
        //         guide_me.map.coordinates.center.lat   = place.geometry.location.lat();
        //         guide_me.map.coordinates.center.lng   = place.geometry.location.lng();
        //         guide_me.map.coordinates.location     = place.name;
        //         guide_me.map.view = 0;
        //         guide_me.map.oms.unspiderfy();
        //         guide_me.invoke.listings.__init( guide_me.map.meters, guide_me.methods.refine.data, guide_me.methods.refine.payload );
        //         guide_me.methods.show_map();
        //     });
        // }
    },
    g_map:{
        map: function(zoom_level){
            guide_me.map.map = new google.maps.Map(document.getElementById('guideMeMap'), {
                center: guide_me.map.user.coordinates,
                zoom: zoom_level ? zoom_level : 14,
                disableDefaultUI: true,
                scrollwheel: false,
                styles: [
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#46bcec"
                            },
                            {
                                "lightness": 75
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 75
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#666666"
                            },
                            {
                                "lightness": 85
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    }
                ],
            });
            guide_me.map.oms = new OverlappingMarkerSpiderfier(guide_me.map.map, {
                markersWontMove: true,
                markersWontHide: true,
                basicFormatEvents: true,
                keepSpiderfied: true
            });
            google.maps.event.addListener(guide_me.map.map,'click', function() {
                guide_me.methods.close_card_btn();
            });
        },
        marker: function( position = guide_me.map.user.coordinates, icon, z_index = 0 ){
            var title = '';
            if( icon == null ){
                var icon = new google.maps.MarkerImage(
                    envUrl + 'assets/img/guide_me/markers/bluedot_retina.png',
                    null,
                    null,
                    new google.maps.Point( 8, 8 ),
                    new google.maps.Size( 17, 17 )
                );
                title = 'I might be here';
            }
            var current_marker = new google.maps.Marker({
                position: position,
                map: guide_me.map.map,
                icon: icon,
                zIndex: z_index,
                title: title,
            });
            return current_marker;
        }
    },
    data: {
        trends: {}
    },
    map: {
        __init: function () {
            guide_me.g_map.map();
            guide_me.g_map.marker();
            guide_me.map.setLocation();
            setTimeout(function(){
                guide_me.invoke.listings.__init();
            }, 1000);
        },
        markers_all		:	[],
        markers_list	:	[],
        markers_property	:	[],
        data:{
            rent:{
                total: 0
            },
            sale: {
                total: 0
            },
            active: 'both'
        },
        meters: 500,
        coordinates: {
            location: _.get(lang_static, 'guideme.default_location'),
            center: {
                lat: Number(25.20814),
                lng: Number(55.27209)
            }
        },
        user: {
            location: _.get(lang_static, 'guideme.default_location'),
            coordinates: {
                lat: Number(25.20814),
                lng: Number(55.27209)
            }
        },
        view: 1,
        property: {
            location: null,
        },
        map: {},
        oms: {},
        landmarks:   {
            unique_landmark_type    :   function( locationTypes, queryTypes ){
                for (var i = 0; i < locationTypes.length; i++) {
                    for (var j = 0; j < queryTypes.length; j++) {
                        if ( queryTypes[j] === locationTypes[i] ){
                            return queryTypes[j];
                        }
                    }
                }
            },
            locate_landmarks    :   function(landmarkTypes = guide_me.map.markers_list, radius = 500 ){
                for (let k = 0; k < landmarkTypes.length; k++ ){
                    let eachLandmarkType = landmarkTypes[k];
                    if( guide_me.map.view === 1 ) {
                        area_points = guide_me.map.user.coordinates;
                    } else {
                        area_points = guide_me.map.coordinates.center;
                    }
                    var places = new google.maps.places.PlacesService( guide_me.map.map );
                    places.nearbySearch({
                        location: area_points,
                        radius: radius,
                        types: [eachLandmarkType]
                    }, function (results, status, pagination) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            guide_me.map.landmarks.create_landmark_markers(results);
                            if (pagination.hasNextPage) {
                                pagination.nextPage();
                            }
                        }
                    });
                }
            },
            create_landmark_markers     :   function ( places, map = guide_me.map.map ) {
                var iconBase = envUrl + 'assets/img/guide_me/markers/';
                for (var i = 0; i < places.length; i++) {
                    var place 		= 	places[i],
                        marker 		= 	new google.maps.Marker({
                            id 		:		place.id,
                            name	:		place.name,
                            map		: 		map,
                            types	:		place.types,
                            filter 	:		g_map.landmarks.unique_landmark_type( place.types , guide_me.map.markers_list ),
                            icon: {
                                url: iconBase + guide_me.map.landmarks.unique_landmark_type( place.types, guide_me.map.markers_list ) +'.png',
                                scaledSize: new google.maps.Size(32,32),
                                origin: new google.maps.Point(0,0),
                                anchor: new google.maps.Point(0,0)
                            },
                            position: 		place.geometry.location,
                            visible :       true,
                        });
                    guide_me.map.markers_all.push( marker );
                }
            },
            remove_all_landmark_markers     :   function( markers ){
                for ( var i = 0; i < markers.length; i++ ) {
                    markers[i].setMap( null );
                }
            },
            selected_markers       :   function( type, value ){
                guide_me.map.markers_list.push(type);
                if(value){
                    $('#guide_me_amenities_marker_'+type).show();
                }
                else{
                    $('#guide_me_amenities_marker_'+type).hide();
                }
            },
            clear_all_markers: function(){
                $('.guide_me_amenities_checkbox').each(function () {
                    $(this).find('input').prop('checked', false);
                    $(this).find('i.check').hide();
                });
                for (var k = 0; k < guide_me.map.markers_all.length; k++) {
                    guide_me.map.markers_all[k].setVisible(false);
                }
                guide_me.map.markers_list = [];
            }
        },
        invoke: function(){
            guide_me.map.landmarks.remove_all_landmark_markers(guide_me.map.markers_property);
            guide_me.map.markers_property = [];
            if(guide_me.map.data.total !== 0){

                var property_data = [];

                property_data = guide_me.map.data.data;

                if(!_.isEmpty(property_data)){

                    for(var d = 0; d < property_data.length; d ++){

                        var position =  new google.maps.LatLng(property_data[d].coordinates.lat, property_data[d].coordinates.lng);

                        var icon = {
                            url: envUrl + 'assets/img/guide_me/markers/'+_.toLower(property_data[d].rent_buy)+'.png',
                            scaledSize: new google.maps.Size(30, 40)
                        };

                        var marker = guide_me.g_map.marker(position, icon, d);

                        guide_me.map.oms.addMarker( marker );

                        guide_me.map.markers_property[d] = marker;

                        guide_me.map.markers_property[d].property = {
                            property: property_data[d],
                            index: d
                        };

                        google.maps.event.addListener(marker, 'spider_click', function(event){

                            guide_me.methods.menu.close();
                            guide_me.methods.top_bar.close();
                            $('#property_info').show('fast');
                            $('#full_action_menu').hide();
                            $('#guide_me_property_list').slick('slickGoTo',[this.property.index]);

                        });

                    }

                } else {
                    helpers.toast.__init( _.get(lang_static, 'guideme.no_results_area_increased') );
                    guide_me.invoke.reload(1500);
                }

            }
        },
        setLocation: function(){
            var geocoder = new google.maps.Geocoder,
                location = {
                    lat: guide_me.map.user.coordinates.lat,
                    lng: guide_me.map.user.coordinates.lng
                }
            geocoder.geocode({'location': location}, function(results, status) {
                if (status === 'OK') {
                    rsLoop: for( var rs = 0; rs < results.length; rs++){
                        addressLoop: for (var ac = 0; ac < results[rs].address_components.length; ac++) {
                            var component = results[rs].address_components[ac];
                            if(component.types.includes('sublocality') || component.types.includes('neighborhood')) {
                                guide_me.map.user.location = component.long_name;
                                break rsLoop;
                            }
                            else if(component.types.includes('locality')){
                                guide_me.map.user.location = component.long_name;
                            }
                        }
                    }
                    if (results[0]) {
                        $('#user-location').val( _.get(lang_static, 'guideme.you_are_in') + guide_me.map.user.location);
                    } else {
                        window.alert( _.get(lang_static, 'guideme.no_results') );
                    }

                } else {
                    window.alert('Geo coder failed due to: ' + status);
                }
            });
        }
    },
    invoke: {
        listings: {
            __init: function(meters = 500, data, extra_data ){

                if( guide_me.map.view === 1 ) {
                    area_name = guide_me.map.user.location;
                    area_points = guide_me.map.user.coordinates;
                } else {
                    area_name = guide_me.map.coordinates.location;
                    area_points = guide_me.map.coordinates.center;
                }

                if(data){

                    if( ( data.rent_buy[0] === 'Sale' ) && ( data.rent_buy.length === 1 ) ){
                        $('#Sale').addClass('active');
                        $('#Rent').removeClass('active');
                    }else if( ( data.rent_buy[0] === 'Rent' ) && ( data.rent_buy.length === 1 ) ){
                        $('#Rent').addClass('active');
                        $('#Sale').removeClass('active');
                    }else{
                        $('#Sale').removeClass('active');
                        $('#Rent').removeClass('active');
                    }

                    var payload = {
                        rent_buy: data.rent_buy,
                        coordinates: area_points,
                        meters: meters,
                        size: 50,
                        page: 1,
                        area: area_name
                    };

                    if(!_.isEmpty(data.type)){
                        payload.type = data.type;
                    }

                    if(!_.isEmpty(extra_data.bedroom)){
                        payload.bedroom = {};
                        if(extra_data.bedroom.min){
                            payload.bedroom.min = extra_data.bedroom.min;
                        }
                        if(extra_data.bedroom.max){
                            payload.bedroom.max = extra_data.bedroom.max;
                        }
                    }

                    if(!_.isEmpty(extra_data.price)){
                        payload.price = {};
                        if(extra_data.price.min){
                            payload.price.min = extra_data.price.min;
                        }
                        if(extra_data.price.max){
                            payload.price.max = extra_data.price.max;
                        }
                    }
                } else {

                    var payload = {
                        rent_buy: ["Sale","Rent"],
                        coordinates: area_points,
                        meters: meters,
                        size: 50,
                        page: 1,
                        area: area_name
                    };

                }

                var url = baseApiUrl + 'analytic/property/guideme/listings?query=' + JSON.stringify(helpers.clean.junk(payload));
                axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {

                    if( response.data.total > 0 )  {

                        guide_me.map.data = response.data;

                        guide_me.map.property.location = guide_me.map.data.data[0].area;

                        guide_me.map.map.panTo( area_points );

                        guide_me.map.invoke();
                        guide_me.methods.view_all();

                    } else {

                        var data = guide_me.methods.refine.data;

                        var tag = '';

                        tag += ( data.type.length !== 0 ) ? data.type : 'property with applied filters';

                        tag += ( data.rent_buy.length !== 0 ) ? ' for ' + data.rent_buy : '';

                        var msg = 'No ' + tag + ' is found in ' + area_name + ' area. Please clear the filters or search in a different location and try again.';

                        helpers.toast.__init( msg )

                    }

                    guide_me.methods.filters_badge.__init();

                }).catch( function( error ) {
                    console.log(error);
                });
            }
        },
        reload: function(meters){
            if(guide_me.map.data.active == 'both'){
                guide_me.invoke.listings.__init(meters);
            }
            else{
                var rent_buy = [_.upperFirst(guide_me.map.data.active)];
                guide_me.invoke.listings.__init(meters);
            }
        }
    },
    methods: {
        height: null,
        dropdown: {
            __init: function(){

                var options = pages.landing.dropdown.data.rent_buy;
                options.length = 2;

                $('#guide_me_price_min').dropdown({
                    values: pages.landing.dropdown.data.price_sale_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                            (guide_me.methods.refine.data.price.min != parseInt(value)) &&
                            !_.isEmpty(value)
                        ) {
                            guide_me.methods.refine.data.price.min = parseInt(value);
                            guide_me.methods.refine.payload.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }

                        if(!value){
                            $(this).dropdown('clear');
                            guide_me.methods.refine.data.price.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#guide_me_price_max', rent_type, 'price');
                        }
                    }
                });

                $('#guide_me_price_max').dropdown({
                    values: pages.landing.dropdown.data.price_sale_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                            (guide_me.methods.refine.data.price.max != parseInt(value)) &&
                            !_.isEmpty(value)
                        ) {
                            guide_me.methods.refine.data.price.max = parseInt(value);
                            guide_me.methods.refine.payload.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }

                        if(!value){
                            $(this).dropdown('clear');
                            guide_me.methods.refine.data.price.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#guide_me_price_min', rent_type, 'price');
                        }

                    }
                });

                $('#guide_me_bedroom_min').dropdown({
                    values: pages.landing.dropdown.data.bedroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bed') ? _.get(lang_static, 'form.placeholders.min.bed') : 'Min Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (guide_me.methods.refine.data.bedroom.min != parseInt(value)) && !_.isEmpty(value)) {
                            guide_me.methods.refine.data.bedroom.min = parseInt(value);
                            guide_me.methods.refine.payload.bedroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            guide_me.methods.refine.data.bedroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#guide_me_bedroom_max', pages.landing.dropdown.data.bedroom_max, 'bedroom');
                        }
                    }
                });

                $('#guide_me_bedroom_max').dropdown({
                    values: pages.landing.dropdown.data.bedroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bed') ? _.get(lang_static, 'form.placeholders.max.bed') : 'Max Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                            (guide_me.methods.refine.data.bedroom.max != parseInt(value)) &&
                            !_.isEmpty(value)
                        ) {
                            guide_me.methods.refine.data.bedroom.max = parseInt(value);
                            guide_me.methods.refine.payload.bedroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');

                        if(!value){
                            $(this).dropdown('clear');
                            guide_me.methods.refine.data.bedroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#guide_me_bedroom_min', pages.landing.dropdown.data.bedroom_min, 'bedroom');
                        }
                    }
                });

            }
        },
        clear_area_field: function(){
            var property_suburb = document.getElementById('user-location');
            $(property_suburb).val('');
            guide_me.methods.menu.close();
        },
        close_card_btn: function(){
            guide_me.methods.stop_marker();
            $('#full_action_menu').show();
            $('#property_info').hide();
        },
        refine: {
            payload: {
                bedroom: {},
                price: {}
            },
            data: {
                bedroom		        :		{
                    min             :       undefined,
                    max             :       undefined,
                },
                price				:		{
                    min			    :		undefined,
                    max			    :		undefined,
                },
                type       :       [],
                rent_buy	        :	[ 'Sale', 'Rent' ]
            }
        },
        refine_results: function(){
            guide_me.map.oms.unspiderfy();
            guide_me.methods.filters.close();
            guide_me.invoke.listings.__init( guide_me.map.meters, guide_me.methods.refine.data, guide_me.methods.refine.payload );
        },
        rent_buy_btn: function(rent_buy, trigger){
            $('.gide_me_rent_buy_btn>div>button').removeClass('active');
            $(trigger).addClass('active');
            if( rent_buy === 'Sale' || rent_buy === 'sale' ){
                $('#stats_tab_b,#stats_tab_content_b').removeClass('active');
                $('#stats_tab_a,#stats_tab_content_a').addClass('active');
            } else {
                $('#stats_tab_a,#stats_tab_content_a').removeClass('active');
                $('#stats_tab_b,#stats_tab_content_b').addClass('active');
            }
            guide_me.map.oms.unspiderfy();
            guide_me.methods.menu.close();
            guide_me.methods.refine.data.rent_buy = [];
            guide_me.methods.refine.data.rent_buy.push(rent_buy);
            guide_me.invoke.listings.__init(guide_me.map.meters, guide_me.methods.refine.data, guide_me.methods.refine.payload);
        },
        view_all: function(){
            property_data = guide_me.map.data.data;
            var property = [];
            for(var p = 0; p < property_data.length; p ++){
                var data = {
                    property:  property_data[p]
                };
                property.push(data);
            }
            var template = $("#guide_me_property_card_template").html();

            if ($('#guide_me_property_list').hasClass('slick-initialized')) {
                $('#guide_me_property_list').slick('unslick');
            }

            helpers.bind(property, search.property.bindings, template ,'guide_me_property_list');
            setTimeout(function(){
                guide_me.methods.slick._init();
            }, 1000);

        },
        area_stats: function(){

            $('.tabular.menu .item').tab();

            var url = baseApiUrl + 'analytic/property/guideme/stats?query=' + JSON.stringify({area: [ guide_me.map.coordinates.location ]});
            axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {
                if(!_.isEmpty(response.data)){
                    guide_me.data.trends = response.data;
                    $('.area_status_content').show();
                    $("#guide_me_area_stats_content").html("");
                    guide_me.methods.area_stats_top_menu();
                    guide_me.methods.area_stats_trends();
                    $('#area_stats_notice').empty().html('<p>Showing you area stats for '+ guide_me.map.coordinates.location +'</p>');
                } else {
                    var url = baseApiUrl + 'analytic/property/guideme/stats?query=' + JSON.stringify({area: [ guide_me.map.property.location ]});
                    axios.get(url, { 'headers': { 'Content-Type': 'application/json' }} ).then( function( response ) {
                        if(!_.isEmpty(response.data)){
                            guide_me.data.trends = response.data;
                            $('.area_status_content').show();
                            $("#guide_me_area_stats_content").html("");
                            guide_me.methods.area_stats_top_menu();
                            guide_me.methods.area_stats_trends();
                            $('#area_stats_notice').empty().html('<p>' + _.get(lang_static, 'guideme.showing_stats_for') + ' '+ guide_me.map.property.location +'</p>');
                        } else {
                            $('.area_status_content').show();
                            $("#guide_me_area_stats_content").html("<div class='guide_me_area_no_stats_trends'>" + guide_me.map.user.location + " " + _.get(lang_static, 'guideme.no_stats') + "</div>");
                        }
                    }).catch( function( error ) {
                        console.log(error);
                    });
                }
            }).catch( function( error ) {
                console.log(error);
            });

        },
        area_stats_top_menu: function(){
            var top_stats_bar_html = '';
            $.each( guide_me.data.trends.type, function( index, value ){
                var name = value.name,
                    name_translated = _.has(lang_static, 'guideme.'+_.toLower( name.replace(/[^A-Z0-9]/ig, '_'))) ? _.get(lang_static, 'guideme.'+_.toLower( name.replace(/[^A-Z0-9]/ig, '_') )) : name;
                top_stats_bar_html += '<figure class="item" onclick="guide_me.methods.area_stats_trends(\''+name+'\')">\n' +
                    '                            <div><img src="'+envUrl+'assets/img/guide_me/'+_.toLower(name.replace(/[^A-Z0-9]/ig, '-'))+'.png" alt=""></div>\n' +
                    '                            <figcaption>'+name_translated+'</figcaption>\n' +
                    '                        </figure>';
            } );

            $('#top_stats_bar').html(top_stats_bar_html);
        },
        area_stats_trends: function(trend_type){

            if(!trend_type){
                var trend_type = 'apartment';
            }

            var stats_content = '';
            var data_set_a = [];
            var data_set_b = [];
            var data_a = null;
            var data_b = null;

            $.each( guide_me.data.trends.type, function( index, value ){
                var name = _.toLower( value.name ),
                    average = _.get(lang_static, 'guideme.average'),
                    price = _.get(lang_static, 'guideme.price'),
                    sqft = _.get(lang_static, 'guideme.sqft');
                if( name === trend_type ){
                    if( guide_me.data.trends.type[index].rent_buy[1] ) {
                        var rent_text = _.get(lang_static, 'guideme.'+_.toLower( guide_me.data.trends.type[index].rent_buy[0].name )),
                            buy_text = _.get(lang_static, 'guideme.'+_.toLower( guide_me.data.trends.type[index].rent_buy[1].name ));
                        stats_content = '<div class="ui grid">\n' +
                            '<div class="eight wide column"><div>'+average+' '+rent_text+' '+price+'</div><div>AED '+guide_me.data.trends.type[index].rent_buy[0].avg_price+'</div></div>\n' +
                            '<div class="eight wide column"><div>'+average+' '+buy_text+' '+price+'</div><div>AED '+guide_me.data.trends.type[index].rent_buy[1].avg_price+'</div>\n' +
                            '</div></div>';
                        type_a = guide_me.data.trends.type[index].rent_buy[0].name;
                        type_b = guide_me.data.trends.type[index].rent_buy[1].name;
                        $('#stats_tab_a').html( rent_text );
                        $('#stats_tab_b').html( buy_text ).show();
                        data_a = guide_me.data.trends.type[index].rent_buy[0].trends.price.price_per_month;
                        data_b = guide_me.data.trends.type[index].rent_buy[1].trends.price.price_per_month;
                    } else {
                        var rent_text = _.get(lang_static, 'guideme.'+_.toLower( guide_me.data.trends.type[index].rent_buy[0].name )),
                            area_text = _.get(lang_static, 'guideme.area');
                        stats_content = '<div class="ui grid">\n' +
                            '<div class="eight wide column"><div>'+average+' '+rent_text+' '+price+'</div><div>AED '+guide_me.data.trends.type[index].rent_buy[0].avg_price+'</div></div>\n' +
                            '<div class="eight wide column"><div>'+average+' '+area_text+'</div><div>'+guide_me.data.trends.type[index].rent_buy[0].avg_builtup_area+' '+sqft+'</div></div>\n' +
                            '</div></div>';
                        type_a = guide_me.data.trends.type[index].rent_buy[0].name;
                        $('#stats_tab_a').html( rent_text );
                        $('#stats_tab_b').html('').hide();
                        data_a = guide_me.data.trends.type[index].rent_buy[0].trends.price.price_per_month;
                    }
                }

            });

            $('.guide_me_area_stats_trends').show();

            $("#guide_me_area_stats_content").html( stats_content );

            var chart_data = {
                labels_a: [],
                labels_b: [],
                data_set_a: [],
                data_set_b: []
            };

            if( data_b ){
                chart_data.labels_b = _.map(data_b, 'date');
                chart_data.data_set_b = _.map(data_b, 'price');
            }
            if( data_a ) {
                chart_data.labels_a = _.map(data_a, 'date');
                chart_data.data_set_a = _.map(data_a, 'price');
            }

            var options = {
                responsive: true,
                title: {
                    display: false
                },
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: true
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: _.get(lang_static, 'guideme.graph_label_a')
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: _.get(lang_static, 'guideme.graph_label_b')
                        }
                    }]
                }
            };

            var configA = {
                type: 'line',
                data: {
                    labels: chart_data.labels_a,
                    datasets: [{
                        label: type_a,
                        fill: false,
                        backgroundColor: '#5d0e8b',
                        borderColor: '#5d0e8b',
                        data: chart_data.data_set_a
                    }]
                },
                options: options
            };
            var ctxa = document.getElementById("guide_me_area_stats_a_trends").getContext("2d");
            window.myLineA = new Chart.Line(ctxa, configA);


            if( data_b ){

                var configB = {
                    type: 'line',
                    data: {
                        labels: chart_data.labels_b,
                        datasets: [{
                            label: type_b,
                            fill: false,
                            backgroundColor: '#5d0e8b',
                            borderColor: '#5d0e8b',
                            data: chart_data.data_set_b
                        }]
                    },
                    options: options
                };
                var ctxb = document.getElementById("guide_me_area_stats_b_trends").getContext("2d");
                window.myLineB = new Chart.Line(ctxb, configB);

            }

        },
        zoom: {
            in: function(){
                guide_me.map.map.setZoom(guide_me.map.map.getZoom() + 1);
                if( guide_me.map.map.getZoom() <= 14 )  {
                    guide_me.map.meters = guide_me.map.meters-3000;
                    guide_me.methods.refine_results();
                }
            },
            out: function(){
                guide_me.map.map.setZoom(guide_me.map.map.getZoom() - 1);
                if( guide_me.map.map.getZoom() < 14 )  {
                    guide_me.map.meters = guide_me.map.meters+3000;
                    guide_me.methods.refine_results();
                }
            }
        },
        menu: {
            trigger: function(){
                var icon = $('#action_menu_trigger').children('i');
                if( guide_me.methods.height <= 414 ) {
                    $('#action_sub_menu').toggleClass('open-it');
                } else {
                    $('#action_sub_menu').slideToggle('slow');
                }
                icon.toggleClass('close');
            },
            close: function(){
                var icon = $('#action_menu_trigger').children('i');
                icon.removeClass('close');
                $('#action_sub_menu').hide();
            },
            action: function(trigger, type){
                $(trigger).addClass('active');
                guide_me.methods.menu.close();
                if(type === 'map'){
                    $('.on_map_only').show();
                    $('#top_header').slideDown();
                    guide_me.methods.filters_badge.__init();
                    guide_me.map.landmarks.locate_landmarks();
                    $('.top_navigation_content').css('display','none');
                    $('#full_action_menu').css('bottom','5rem');
                } else if(type === 'area_status'){
                    $('#top_header').slideUp();
                    $('.on_map_only,.amenities_content').hide();
                    $('.area_status_content').show();
                    $('.top_navigation_content').css('display','block');
                    $('#full_action_menu').css('bottom','1rem');
                    guide_me.methods.area_stats("apartment");
                } else if(type === 'amenities'){
                    $('#top_header').slideUp();
                    $('.on_map_only,.area_status_content').hide();
                    $('.amenities_content').show();
                    $('.top_navigation_content').css('display','block');
                    $('#full_action_menu').css('bottom','1rem');
                }
            }
        },
        show_map: function(){
            guide_me.methods.menu.close();
            $('#top_header').slideDown();
            $('.on_map_only').show();
            $('.top_navigation_content').css('display','none');
            $('#full_action_menu').css('bottom','5rem');
        },
        filters: {
            open: function(){
                guide_me.g_map_autocomplete.__init();
                guide_me.methods.menu.close();
                $('#refine_form').slideToggle();
                guide_me.g_map_autocomplete.__init();
            },
            close: function(){
                $('#refine_form').hide();
            },
            clear_all: function(trigger){
                guide_me.methods.refine = {
                    payload: {
                        bedroom: {},
                        price: {}
                    },
                    data: {
                        bedroom		        :		{
                            min             :       undefined,
                            max             :       undefined,
                        },
                        price				:		{
                            min			    :		undefined,
                            max			    :		undefined,
                        },
                        type       :       [],
                        rent_buy	        :	['Sale','Rent']
                    }
                };
                $('#guide_me_bedroom_max,#guide_me_bedroom_min,#guide_me_price_max,#guide_me_price_min').dropdown('clear');
                $('#top_bar').find('.item').removeClass('active');
                $('#Rent,#Sale').removeClass('active');
                $('#show_filters_applied span').css('display','none');
                guide_me.map.landmarks.clear_all_markers();
                guide_me.methods.refine_results();
            }
        },
        top_bar: {
            trigger: function(){
                var icon = $('#top_bar_trigger').children('i');
                $('#top_bar').slideToggle('slow');
                guide_me.methods.menu.close();
                if( icon.hasClass('up') ){
                    icon.removeClass('up').addClass('down');
                }else{
                    icon.removeClass('down').addClass('up');
                }
            },
            close: function(){
                var icon = $('#top_bar_trigger').children('i');
                $('#top_bar').slideUp('slow');
                icon.removeClass('up').addClass('down');
            }
        },
        current_location: function(){
            guide_me.map.coordinates.center.lat = guide_me.map.user.coordinates.lat;
            guide_me.map.coordinates.center.lng = guide_me.map.user.coordinates.lng;
            $('#user-location').val( _.get(lang_static, 'guideme.you_are_in') + guide_me.map.user.location);
            guide_me.map.view = 1;
            guide_me.methods.menu.close();
            guide_me.map.oms.unspiderfy();
            guide_me.methods.show_map();
            guide_me.invoke.listings.__init( guide_me.map.meters, guide_me.methods.refine.data, guide_me.methods.refine.payload );
            guide_me.map.coordinates.location = guide_me.map.user.location;
        },
        prop_types_select: function(type, trigger){
            $(trigger).addClass('active').siblings().removeClass('active');
            guide_me.methods.menu.close();
            guide_me.methods.refine.data.type = [type];
            guide_me.methods.refine_results();
        },
        show_marker: function (index) {
            guide_me.methods.stop_marker();
            guide_me.map.markers_property[index].setAnimation(google.maps.Animation.BOUNCE);
            guide_me.map.map.panTo(guide_me.map.markers_property[index].getPosition());
        },
        stop_marker: function(){
            for (var i = 0; i < guide_me.map.markers_property.length; i++) {
                guide_me.map.markers_property[i].setAnimation(null);
            }
        },
        slick: {
            _init: function(){
                var slickElem = $('#guide_me_property_list'),
                    options = {
                        arrows: true,
                        dots: false,
                        lazyLoad: 'ondemand',
                        prevArrow: '<button type="button" class="slick-prev"><i class="angle left icon"></i></button>',
                        nextArrow: '<button type="button" class="slick-next"><i class="angle right icon"></i></button>'
                    };
                slickElem.slick(options);
                slickElem.on('afterChange', function(slick, currentSlide){
                    guide_me.methods.show_marker(currentSlide.currentSlide);
                });
            }
        },
        filters_badge: {
            __init: function()  {
                var markersShowing = 0;
                for(var i=0; i< guide_me.map.markers_list.length; i++){
                    markersShowing++;
                }
                if( ( guide_me.methods.refine.data.bedroom.min !== undefined ) || ( guide_me.methods.refine.data.bedroom.max !== undefined ) || ( guide_me.methods.refine.data.price.min !== undefined ) || ( guide_me.methods.refine.data.bedroom.max !== undefined ) || ( guide_me.methods.refine.data.type.length !== 0 ) || ( guide_me.methods.refine.data.rent_buy.length === 1 ) || ( markersShowing !== 0 ) )  {
                    $('#show_filters_applied span').css('display','block');
                } else {
                    $('#show_filters_applied span').hide();
                }
            },
            show: function(){
                $('#show_filters_applied span').css('display','block');
            },
            hide: function(){
                $('#show_filters_applied span').hide();
            }
        }
    },
    locationsJSON: [
        {
            "label": "abu dhabi",
            "value": "24.3865729,54.2784264"
        },
        {
            "label": "al reem island, abu dhabi",
            "value": "24.492671,54.3909826"
        },
        {
            "label": "al ghadeer, abu dhabi",
            "value": "24.842554,55.0222349"
        },
        {
            "label": "al raha beach, abu dhabi",
            "value": "24.842554,55.0222349"
        },
        {
            "label": "al reef, abu dhabi",
            "value": "24.4661049,54.6387816"
        },
        {
            "label": "al raha gardens, abu dhabi",
            "value": "24.4340013,54.5555518"
        },
        {
            "label": "al reef villas, abu dhabi",
            "value": "24.458519,54.6718073"
        },
        {
            "label": "saadiyat island, abu dhabi",
            "value": "24.5403728,54.4004481"
        },
        {
            "label": "yas island, abu dhabi",
            "value": "24.4858485,54.5679587"
        },
        {
            "label": "corniche road, abu dhabi",
            "value": "24.4698718,54.3333196"
        },
        {
            "label": "khalifa city, abu dhabi",
            "value": "24.4200482,54.5380698"
        },
        {
            "label": "al khalidiya, abu dhabi",
            "value": "24.4691786,54.3407981"
        },
        {
            "label": "bani yas, abu dhabi",
            "value": "24.3063509,54.6170922"
        },
        {
            "label": "corniche area, abu dhabi",
            "value": "24.4698718,54.3333196"
        },
        {
            "label": "al raha golf gardens, abu dhabi",
            "value": "24.4193631,54.527476"
        },
        {
            "label": "hydra village, abu dhabi",
            "value": "24.5476676,54.6968759"
        },
        {
            "label": "salam street, abu dhabi",
            "value": "24.4539851,54.4019773"
        },
        {
            "label": "al marina, abu dhabi",
            "value": "24.4777354,54.3131661"
        },
        {
            "label": "al karama, abu dhabi",
            "value": "24.4620427,54.365319"
        },
        {
            "label": "mohamed bin zayed city, abu dhabi",
            "value": "24.3403937,54.5275295"
        },
        {
            "label": "al maqtaa, abu dhabi",
            "value": "24.4054263,54.4947347"
        },
        {
            "label": "al bateen, abu dhabi",
            "value": "24.4513155,54.3281447"
        },
        {
            "label": "eastern road, abu dhabi",
            "value": "24.4546536,54.3913953"
        },
        {
            "label": "al mina, abu dhabi",
            "value": "24.5244655,54.3609552"
        },
        {
            "label": "sas al nakhl, abu dhabi",
            "value": "24.4430234,54.473163"
        },
        {
            "label": "masdar city, abu dhabi",
            "value": "24.4264004,54.6017502"
        },
        {
            "label": "tourist club area, abu dhabi",
            "value": "24.496567,54.3608636"
        },
        {
            "label": "airport road, abu dhabi",
            "value": "24.4657488,54.3737002"
        },
        {
            "label": "khalifa park, abu dhabi",
            "value": "24.4255478,54.4672139"
        },
        {
            "label": "al mushrif, abu dhabi",
            "value": "24.4411435,54.3725603"
        },
        {
            "label": "mussafah, abu dhabi",
            "value": "24.3488532,54.4518355"
        },
        {
            "label": "officers club area, abu dhabi",
            "value": "24.3966735,54.4649788"
        },
        {
            "label": "al maryah island, abu dhabi",
            "value": "24.5021881,54.3819165"
        },
        {
            "label": "abu dhabi gate city",
            "value": "24.3935048,54.476156"
        },
        {
            "label": "al nahyan camp, abu dhabi",
            "value": "24.4637708,54.3693771"
        },
        {
            "label": "nurai island, abu dhabi",
            "value": "24.6162423,54.473199"
        },
        {
            "label": "marina village, abu dhabi",
            "value": "24.4783704,54.311003"
        },
        {
            "label": "shakhbout city, abu dhabi",
            "value": "24.3593799,54.5942408"
        },
        {
            "label": "al muneera, abu dhabi",
            "value": "24.4511345,54.6028902"
        },
        {
            "label": "al samha, abu dhabi",
            "value": "24.6658527,54.7160366"
        },
        {
            "label": "al shamkha, abu dhabi",
            "value": "24.3782719,54.6794225"
        },
        {
            "label": "electra street, abu dhabi",
            "value": "24.4934104,54.3708137"
        },
        {
            "label": "danet abu dhabi",
            "value": "24.4298658,54.433832"
        },
        {
            "label": "nareel island, abu dhabi",
            "value": "24.4523464,54.3215936"
        },
        {
            "label": "al najda street, abu dhabi",
            "value": "24.4900746,54.3694898"
        },
        {
            "label": "khalidia, abu dhabi",
            "value": "24.4691867,54.3481045"
        },
        {
            "label": "al khaleej al arabi street, abu dhabi",
            "value": "24.4040823,54.4819644"
        },
        {
            "label": "al gurm west, abu dhabi",
            "value": "24.4204437,54.4018935"
        },
        {
            "label": "khalifa city c, abu dhabi",
            "value": "24.3804404,54.5879315"
        },
        {
            "label": "al markaziyah, abu dhabi",
            "value": "24.4833501,54.3466723"
        },
        {
            "label": "al bahia, abu dhabi",
            "value": "24.5568273,54.5778634"
        },
        {
            "label": "al dhafrah, abu dhabi",
            "value": "24.187618,54.4396579"
        },
        {
            "label": "al shahama, abu dhabi",
            "value": "24.505878,54.6340006"
        },
        {
            "label": "al wahda, abu dhabi",
            "value": "24.4703363,54.3705651"
        },
        {
            "label": "marina mall, abu dhabi",
            "value": "24.4760586,54.3193255"
        },
        {
            "label": "madinat zayed, abu dhabi",
            "value": "24.481617,54.3603168"
        },
        {
            "label": "zayed sports city, abu dhabi",
            "value": "24.4169388,54.4494492"
        },
        {
            "label": "sharjah",
            "value": "25.3174292,55.370521"
        },
        {
            "label": "aljada, sharjah",
            "value": "25.3231393,55.4725768"
        },
        {
            "label": "sharjah waterfront city",
            "value": "25.5030006,55.5193047"
        },
        {
            "label": "al tai, sharjah",
            "value": "25.2472945,55.5669592"
        },
        {
            "label": "muwaileh, sharjah",
            "value": "25.2989579,55.4451662"
        },
        {
            "label": "al khan, sharjah",
            "value": "25.3223803,55.3495823"
        },
        {
            "label": "al suyoh, sharjah",
            "value": "25.2373172,55.6022357"
        },
        {
            "label": "sharjah university city, sharjah",
            "value": "25.2986629,55.4662477"
        },
        {
            "label": "tilal city, sharjah",
            "value": "25.257542,55.5755773"
        },
        {
            "label": "hamriyah free zone, sharjah",
            "value": "25.4555128,55.501427"
        },
        {
            "label": "al majaz, sharjah",
            "value": "25.3292074,55.3646969"
        },
        {
            "label": "corniche al buhaira, sharjah",
            "value": "25.3344243,55.3865734"
        },
        {
            "label": "maryam island, sharjah",
            "value": "25.3241876,55.3577405"
        },
        {
            "label": "al hamriya, sharjah",
            "value": "25.4808488,55.4906552"
        },
        {
            "label": "al qasimia, sharjah",
            "value": "25.344456,55.3841553"
        },
        {
            "label": "maleha, sharjah",
            "value": "25.1218101,55.8407586"
        },
        {
            "label": "sharjah industrial area, sharjah",
            "value": "25.309668,55.3924805"
        },
        {
            "label": "al jada, sharjah",
            "value": "25.3231393,55.4725768"
        },
        {
            "label": "al nahda sharjah, sharjah",
            "value": "25.3038718,55.3655575"
        },
        {
            "label": "al shuwaihean, sharjah",
            "value": "25.360872,55.3851861"
        },
        {
            "label": "al suyoh 1, sharjah",
            "value": "25.2384847,55.6038666"
        },
        {
            "label": "al butina, sharjah",
            "value": "25.3752881,55.3990259"
        },
        {
            "label": "al gharb, sharjah",
            "value": "25.3555149,55.3855243"
        },
        {
            "label": "al mujarrah, sharjah",
            "value": "25.3663639,55.3876055"
        },
        {
            "label": "al sajaa, sharjah",
            "value": "25.3462553,55.418743"
        },
        {
            "label": "muwaileh commercial, sharjah",
            "value": '25.2973658,55.439476'
        },
        {
            "label": "rolla area, sharjah",
            "value": "25.356618,55.3880529"
        },
        {
            "label": "sharjah airport freezone (saif), sharjah",
            "value": "25.3386651,55.4764734"
        },
        {
            "label": "al garayen, sharjah",
            "value": "25.2981951,55.5038832"
        },
        {
            "label": "al qasbaa, sharjah",
            "value": "25.3223305,55.3753377"
        },
        {
            "label": "al wahda, sharjah",
            "value": "25.3253654,55.3901722"
        },
        {
            "label": "kalba, sharjah",
            "value": "25.051536,56.3063356"
        },
        {
            "label": "abu shagara, sharjah",
            "value": "25.3362683,55.3876912"
        },
        {
            "label": "al ghuair, sharjah",
            "value": '25.3579392,55.3864896'
        },
        {
            "label": "al nahda, sharjah",
            "value": "25.3038718,55.3655575"
        },
        {
            "label": "buhaira, sharjah",
            "value": "25.3262358,55.3873412"
        },
        {
            "label": "ras al khaimah",
            "value": "25.7263236,55.8281909",
        },
        {
            "label": "al hamra village, ras al khaimah",
            "value": "25.692062,55.7755494"
        },
        {
            "label": "al marjan island, ras al khaimah",
            "value": "25.6718993,55.7319452"
        },
        {
            "label": "mina al arab, ras al khaimah",
            "value": "25.720232,55.8293438"
        },
        {
            "label": "bab al bahr, ras al khaimah",
            "value": "25.6634006,55.7463965"
        },
        {
            "label": "yasmin village, ras al khaimah",
            "value": "25.7418069,55.9824671"
        },
        {
            "label": "al hamra village, ras al khaimah",
            "value": "25.692062,55.7755494"
        },
        {
            "label": "al ghail, ras al khaimah",
            "value": "25.4107498,55.9879951"
        },
        {
            "label": "al nakheel, ras al khaimah",
            "value": "25.7895056,55.9721371"
        },
        {
            "label": "al hudaibah south, ras al khaimah",
            "value": "25.7277622,55.8930464"
        },
        {
            "label": "khuzam, ras al khaimah",
            "value": "25.7589133,55.9322925"
        },
        {
            "label": "ajman",
            "value": "25.405216,55.513641"
        },
        {
            "label": "ajman uptown",
            "value": "25.4168122,55.5883325"
        },
        {
            "label": "ajman industrial area",
            "value": "25.3120623,55.3922724"
        },
        {
            "label": "al bustan, ajman",
            "value": "25.4113276,55.4395015"
        },
        {
            "label": "al rawda 1, ajman",
            "value": "25.3911401,55.5128216"
        },
        {
            "label": "emirates city, ajman",
            "value": "25.3962672,55.5690085"
        },
        {
            "label": "al rashidiya, ajman",
            "value": "25.3965679,55.4432988"
        },
        {
            "label": "ajman marina",
            "value": "25.4223365,55.4435581"
        },
        {
            "label": "al mwaihat, ajman",
            "value": "25.3761888,55.4952746"
        },
        {
            "label": "al nuaimia 2, ajman",
            "value": "25.383987,55.4467106"
        },
        {
            "label": "al rumaila, ajman",
            "value": "25.4004237,55.427136"
        },
        {
            "label": "fujairah",
            "value": "25.1436311,56.2918739"
        },
        {
            "label": "eagle hills fujairah beach",
            "value": "25.1359275,56.3539263"
        },
        {
            "label": "umm al quwain",
            "value": "25.5362381,55.512047"
        },
        {
            "label": "umm al quwain marina",
            "value": "25.526820,55.595241"
        }
    ]

}
