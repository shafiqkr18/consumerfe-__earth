var pages = {
    landing: {
        // Used to stop the rent buy trigger from launching on page load
        first: 2,
        property: {
            call: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Api call
                    */

                    var user = JSON.parse(helpers.store.read('user_o'));

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_call_phone');
                    }, 500);

                    var agent_name  = _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : $(data_trigger).parent().attr("data-agent_contact_name");
                        agent_name  = !_.isEmpty(agent_name) ? agent_name : 'Agent name not available';
                    var agent_phone = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'agent.contact.phone') ? _.get(data_trigger, 'agent.contact.phone') : ( $(data_trigger).parent().attr("data-agent_contact_phone") ? $(data_trigger).parent().attr("data-agent_contact_phone") : 'Phone not available') );

                    var agency_name = _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : $(data_trigger).parent().attr("data-agency_contact_name");
                        agency_name = !_.isEmpty(agency_name) ? agency_name : 'Agency name not available';

                    var property_ref_no = _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : $(data_trigger).parent().attr("data-ref_no");
                        property_ref_no = !_.isEmpty(property_ref_no) ? property_ref_no : 'Reference number not available';

                    var property_id = _.has(data_trigger, '_id') ? _.get(data_trigger, '_id') : $(data_trigger).parent().attr("data-_id");
                        property_id = !_.isEmpty(property_id) ? property_id : 'ID not available';

                    //1
                    var data = {
                        agent: {
                            name: agent_name,
                            phone: agent_phone
                        },
                        agency: {
                            name: agency_name
                        },
                        property: {
                            reference: property_ref_no,
                            _id: property_id
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        source: 'desktop'
                    }

                    if(!data.agent.phone.includes("+") && !_.startsWith(data.agent.phone,'0')){
                        $('#modal_property_call_agent_contact_phone').html('+' + data.agent.phone);
                    }
                    else if(_.startsWith(data.agent.phone,'0')){
                        $('#modal_property_call_agent_contact_phone').html(data.agent.phone.replace('0', '+971'));
                    }else {
                        $('#modal_property_call_agent_contact_phone').html(data.agent.phone);
                    }

                    //2
                    $('#modal_property_call_agent_name').html(data.agent.name);
                    $('#modal_property_call_agency_name').html(data.agency.name);
                    $('#modal_property_call_ref_no').html(data.property.reference);
                    $('#modal_property_call_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_call_data').attr('data-_id', data.property._id);
                    $('#modal_property_call_data').attr('data-subsource', data.subsource);

                    //3
                    $('.ui.modal.property_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    pages.signup_guide_slider.__init( 'call-slider' );

                    //4
                    var payload = {
                        action: 'call',
                        source: 'consumer',
                        subsource: 'desktop',
                        message: _.has(lang_static, 'modals.property.call.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.call.payload_msg'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.',
                        property: {
                            _id: data.property._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //storing the payload for call buton api call
                    lead.property.store.save = payload;

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','call_click', data_trigger);
                    other.datalayer('property','call_submit', data_trigger);

                    lead.property.call.__init();

                }
            },
            email: {
                __init: function(data_trigger) {
                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_email_phone');
                    }, 500);

                    var agent_name  = _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : $(data_trigger).parent().attr("data-agent_contact_name");
                        agent_name  = !_.isEmpty(agent_name) ? agent_name : 'Agent name not available';

                    var agent_picture  = _.has(data_trigger, 'agent.contact.picture') ? _.get(data_trigger, 'agent.contact.picture') : $(data_trigger).parent().attr("data-agent_contact_picture");
                        agent_picture  = !_.isEmpty(agent_picture) ? agent_picture : '';

                    var agency_name = _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : $(data_trigger).parent().attr("data-agency_contact_name");
                        agency_name = !_.isEmpty(agency_name) ? agency_name : 'Agency name not available';

                    var property_ref_no = _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : $(data_trigger).parent().attr("data-ref_no");
                        property_ref_no = !_.isEmpty(property_ref_no) ? property_ref_no : 'Reference number not available';

                    var property_id = _.has(data_trigger, '_id') ? _.get(data_trigger, '_id') : $(data_trigger).parent().attr("data-_id");
                        property_id = !_.isEmpty(property_id) ? property_id : 'ID not available';

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: agent_name,
                            picture: agent_picture
                        },
                        agency: {
                            name: agency_name
                        },
                        property: {
                            reference: property_ref_no,
                            _id: property_id
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_email_agent_img');


                    //2
                    $('#modal_property_email_agent_name').html(data.agent.name);
                    $('#modal_property_email_agency_name').html(data.agency.name);
                    $('#modal_property_email_textarea').html(_.has(lang_static, 'modals.property.email.message') ? helpers.trans_value(_.get(lang_static, 'modals.property.email.message'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.')
                    $('#modal_property_email_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_email_data').attr('data-_id', data.property._id);
                    $('#modal_property_email_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.property_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    pages.signup_guide_slider.__init( 'email-slider' );

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;

                    other.datalayer('property','email_click', data_trigger);

                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_property_callback_phone');
                    }, 500);

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : 'Agent name not available',
                            picture: _.has(data_trigger, 'agent.contact.picture') ? _.get(data_trigger, 'agent.contact.picture') : ''
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : 'Agency name not available'
                        },
                        property: {
                            reference: _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : 'reference number not available',
                            _id: _.get(data_trigger, '_id')
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_callback_agent_img');

                    //2
                    $('#modal_property_callback_agent_name').html(data.agent.name);
                    $('#modal_property_callback_agency_name').html(data.agency.name);
                    $('#modal_property_callback_data').attr('data-ref_no', data.property.reference);
                    $('#modal_property_callback_data').attr('data-_id', data.property._id);
                    $('#modal_property_callback_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.property_callback').modal('setting', 'transition', 'vertical flip').modal('show');

                    pages.signup_guide_slider.__init( 'callback-slider' );

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('property','callback_click', data_trigger);

                }
            },
            whatsapp: {
                __init: function(data_trigger) {

                    var wapp_phone_number = _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'agent.contact.phone') ? _.get(data_trigger, 'agent.contact.phone') :'Phone not available');

                    if (wapp_phone_number == 'Phone not available') {
                        alert('Phone number not available');
                    } else {

                        var agency_email  = _.get(data_trigger, 'agent.agency.contact.email');
                        //NOTE TEMPORARY CHANGE TO TRACK WHATSAPP (REMOVE AFTER TRIAL COMPLETED)
                        wapp_phone_number = '+971529887539'

                        var user = JSON.parse(helpers.store.read('user_o'));

                        //1
                        var data = {
                            agent: {
                                name: _.has(data_trigger, 'agent.contact.name') ? _.get(data_trigger, 'agent.contact.name') : 'Agent name not available',
                                phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : 'Phone not available'
                            },
                            agency: {
                                name:  _.has(data_trigger, 'agent.agency.contact.name') ? _.get(data_trigger, 'agent.agency.contact.name') : 'Agency name not available'
                            },
                            property: {
                                reference: _.has(data_trigger, 'ref_no') ? _.get(data_trigger, 'ref_no') : 'reference number not available',
                                _id: _.get(data_trigger, '_id')
                            },
                            user: {
                                name: user ? user.name : 'anonymous',
                                email: user ? user.email : 'anonymous@domain.com',
                                phone: user && _.has(user, 'user.phone') ? user.phone : ''
                            }
                        }

                        var payload = {
                            action: 'whatsapp',
                            source: 'consumer',
                            subsource: 'desktop',
                            message: _.has(lang_static, 'modals.property.whatsapp.payload_msg') ? helpers.trans_value(_.get(lang_static, 'modals.property.whatsapp.payload_msg'), {reference: data.property.reference}) : 'Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.',
                            property: {
                                _id: data.property.id
                            },
                            user: {
                                contact: {
                                    name: data.user.name,
                                    email: data.user.email,
                                    phone: data.user.phone
                                }
                            }
                        }

                        if (payload.user.contact.phone.length === 0) {
                            delete payload.user.contact.phone
                        }

                        //whatsapp lead create api
                        lead.property.whatsapp.__init(payload);

                        //gtm push on submit
                        helpers.store.temp_data.write = data_trigger;
                        other.datalayer('property','whatsapp_submit', data_trigger);

                        window.open(
                            'whatsapp://send?text='+encodeURIComponent(payload.message)+'&phone=' + wapp_phone_number,
                            '_blank'
                        );
                    }
                }
            }
        },
        __init: function() {
            page = 'landing';

            /**
            * 2.   Initial Blog posts are loaded
            * 3.   Change Featured Properties based on rent/buy dropdown
            * 4.   Change Featured Properties based on area dropdown
            * 5.   Fire a search
            * 6.   Newsletter
            * 7.   Load images of featured properties
            * 8.   Ideal Property Popup
            */
            // 2.
            other.blog.listings();

            // 3.
            pages.landing.dropdown.__init();

            // 7.
            helpers.lazy_load();

            // 4.   State
            pages.landing.state();

            // 8.   State
            pages.landing.ideal_property.__init();

            $('#rentbuy_area item').tab({history:false});
            $('#popular-buy item').tab({history:false});
            $('#popular-rent item').tab({history:false});

            $('.popup-video-youtube').magnificPopup({
                type: 'iframe'
            });

        },
        state: function() {
            var state = helpers.user.state.is_logged_in();
            if (state[0]) {
                // Landing Login State
                /**
                * 1.   Header shows 'username'
                * 2.   Lead modals are populated with user details
                */
                // 1.
                $('.state_li').show();
                $('.state_lo').hide();
                // 2.
                helpers.populate.modals(state[1]);
                helpers.populate.header(state[1]);
                $('.item').tab();
            } else {
                $('.state_lo').show();
                $('.state_li').hide();
                helpers.depopulate.modals();
                helpers.depopulate.header();
            }
        },
        subscribe_for_newsletter: function(el) {
            user.newsletter.__init(el);
        },
        change_featured_properties: function() {

            /**
            * 1.   Onchange    =   rent/buy
            * 2.   Onchange    =   area
            */

            // Changing the title

            featured_stub = "Featured :residential_commercial: Properties for :rent_buy: :area:";
            featured_stub = featured_stub.replace( ':area:', '');

            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'residential_commercial', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'residential_commercial', []))) {

                residential_commercial  =   _.get(search.property.regular.store_variables, 'residential_commercial.0').replace('-', ' ');
                featured_stub = featured_stub.replace( ':residential_commercial:', residential_commercial );
            }

            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'city', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'city', []))) {
                featured_stub = featured_stub.replace( ':area:', ' in ' + _.get(search.property.regular.store_variables, 'city.0') );
            }

            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'area', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'area', []))) {
                featured_stub = featured_stub.replace( ':area:', ' in ' + _.get(search.property.regular.store_variables, 'area.0') );
            }
            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'building', undefined)) &&
                !_.isEmpty(_.get(search.property.regular.store_variables, 'building', []))) {
                featured_stub = featured_stub.replace( ':area:', ' in ' + _.get(search.property.regular.store_variables, 'building.0') );
            }
            // Rent Buy
            if (!_.isUndefined(_.get(search.property.regular.store_variables, 'rent_buy', undefined)) &&
            !_.isEmpty(_.get(search.property.regular.store_variables, 'rent_buy', []))) {
                if (_.includes(_.get(search.property.regular.store_variables, 'rent_buy.0'), 'Commercial')) {
                    var parts = _.get(search.property.regular.store_variables, 'rent_buy.0').split(/-(.+)/);
                    rent_buy  = _.get(parts, '1', 'Sale').replace(/-/g, ' ');
                    residential_commercial  =   _.get(parts, '0', 'Commercial');
                    featured_stub = featured_stub.replace( ':rent_buy:', rent_buy );
                    featured_stub = featured_stub.replace( ':residential_commercial:', residential_commercial );
                } else if (!_.isEmpty(_.get(search.property.regular.store_variables, 'rent_buy.0'))) {
                    rent_buy  = _.get(search.property.regular.store_variables, 'rent_buy.0').replace(/-/g, ' ');
                    residential_commercial  =   'Residential';
                    featured_stub = featured_stub.replace( ':rent_buy:', rent_buy);
                    featured_stub = featured_stub.replace( ':residential_commercial:', residential_commercial);
                }
            }
            var url = search.property.regular.construct.url(search.property.regular.store_variables);
            last_segment = url.split( '/' ).pop();
            featured_url = ( !_.startsWith( rent_buy, 'Short' ) ) ? url.replace( last_segment, 'featured-'+last_segment ) : url;

            featured_stub  =  _.has(lang_static, 'featured.'+_.toLower(residential_commercial)+'.'+_.toLower(rent_buy)+'.title') ? _.get(lang_static, 'featured.'+_.toLower(residential_commercial)+'.'+_.toLower(rent_buy)+'.title') : featured_stub;
            featured_stub = '<a href=" ' + featured_url + '" style="color:black;">' + featured_stub + '</a>';
            $('.f_properties').html(featured_stub);

            var query = {};
            query.area = _.get(search.property.regular.store_variables, 'area', []);
            query.city = _.get(search.property.regular.store_variables, 'city', []);
            query.building = _.get(search.property.regular.store_variables, 'building', []);
            query.rent_buy = _.get(search.property.regular.store_variables, 'rent_buy', 'Sale');
            query.size = 5;
            query.featured = {};
            query.featured.status = true;
            search.property.listings(query, 'featured_properties');
        },
        search: {
            toggle: function(el) {
                // Fields drawer
                $('.advanced_search').toggleClass('search_active');
                // Text editing
                if ($('.advanced_search').hasClass('search_active')) {
                    $(el).find('a').html('- Advanced Search');
                } else {
                    $(el).find('a').html('+ Advanced Search');

                }
            }
        },
        dropdown: {
            __init: function() {

                $('#property_rent_buy').dropdown({
                    values: pages.landing.dropdown.data.rent_buy,
                    placeholder: _.has(lang_static, 'global.rent_buy') ? _.get(lang_static, 'global.rent_buy') : 'Rent or Buy',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.rent_buy = [];
                            search.property.regular.store_variables.rent_buy.push(value);
                        }

                        if ($('#landing_search').length) {
                            pages.landing.first--;

                            if (!_.isEmpty(_.get(search.property.regular.store_variables, 'rent_buy', [])) && pages.landing.first < 2) {
                                pages.landing.change_featured_properties();
                            }
                        }

                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                        if(!value){
                            $(this).dropdown('clear');
                        }
                    }
                });

                var auto = document.getElementById('property_suburb');
                var auto_hidden = document.getElementById('property_suburb_hidden');
                var auto_hidden_b = document.getElementById('property_suburb_hidden_b');

                new Awesomplete(auto, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    multiple: true,
                    autoFirst: true,
                    filter: function(text, input) {
                        return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                    },
                    item: function(text, input) {
                        return Awesomplete.ITEM(text, input.match(/[^,]*$/)[0]);
                    },
                    replace: function (text) {

                        var before = this.input.value.match(/^.+,\s*|/)[0];
                        var before_hidden = '';

                        selectedLoc = _.filter(searchJson.property.location, function(loc) {
                            return loc.value === text.value;
                        });

                        if(!_.includes(selectedLoc,'0.label')){
                            search.property.regular.store_variables.suburb.label = before + _.get(selectedLoc,'0.label') + ', ';
                        }

                        items = search.property.regular.store_variables.suburb.label.split(', ');
                        items.length = (items.length)-1;
                        $.each(items, function( index, value ){
                            selectedLoc = _.filter(searchJson.property.location, function(loc) {
                                return loc.label === value;
                            });
                            before_hidden += _.get(selectedLoc,'0.value') + ', ';
                        });

                        search.property.regular.store_variables.suburb.value = before_hidden;

                        auto_hidden.value = search.property.regular.store_variables.suburb.value;
                        auto_hidden_b.value = search.property.regular.store_variables.suburb.label;
                        this.input.value = search.property.regular.store_variables.suburb.label;

                    }
                });

                $('#property_type').dropdown({
                    values: pages.landing.dropdown.data.property_type.residential.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.type') ? _.get(lang_static, 'form.placeholders.type') : 'Property Type',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.type = [];
                            search.property.regular.store_variables.type.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.type = [];
                        }
                    }
                });

                $('#property_bedroom_min').dropdown({
                    values: pages.landing.dropdown.data.bedroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bed') ? _.get(lang_static, 'form.placeholders.min.bed') : 'Min Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bedroom.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bedroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bedroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_bedroom_max', pages.landing.dropdown.data.bedroom_max, 'bedroom');
                        }
                    }
                });

                $('#property_bedroom_max').dropdown({
                    values: pages.landing.dropdown.data.bedroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bed') ? _.get(lang_static, 'form.placeholders.max.bed') : 'Max Bed',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) &&
                            (search.property.regular.store_variables.bedroom.max != parseInt(value)) &&
                            !_.isEmpty(value)
                        ) {
                            search.property.regular.store_variables.bedroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bedroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_bedroom_min', pages.landing.dropdown.data.bedroom_min, 'bedroom');
                        }
                    }
                });

                $('#property_bathroom_min').dropdown({
                    values: pages.landing.dropdown.data.bathroom_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.bath') ? _.get(lang_static, 'form.placeholders.min.bath') : 'Min Bath',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bathroom.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bathroom.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bathroom.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_bathroom_max', pages.landing.dropdown.data.bathroom_max, 'bathroom');
                        }
                    }
                });

                $('#property_bathroom_max').dropdown({
                    values: pages.landing.dropdown.data.bathroom_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.bath') ? _.get(lang_static, 'form.placeholders.max.bath') : 'Max Bath',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.bathroom.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.bathroom.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.bathroom.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_bathroom_min', pages.landing.dropdown.data.bathroom_min, 'bathroom');
                        }
                    }
                });

                $('#property_area_min').dropdown({
                    values: pages.landing.dropdown.data.area_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.area') ? _.get(lang_static, 'form.placeholders.min.area') : 'Min Area',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.dimension.builtup_area.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.dimension.builtup_area.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.dimension.builtup_area.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_area_max', pages.landing.dropdown.data.area_max, 'area');
                        }
                    }
                });

                $('#property_area_max').dropdown({
                    values: pages.landing.dropdown.data.area_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.area') ? _.get(lang_static, 'form.placeholders.max.area') : 'Max Area',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.dimension.builtup_area.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.dimension.builtup_area.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.dimension.builtup_area.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_area_min', pages.landing.dropdown.data.area_min, 'area');
                        }
                    }
                });

                $('#property_price_min').dropdown({
                    values: pages.landing.dropdown.data.price_sale_min,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.price.min = undefined;
                        } else {
                            pages.landing.dropdown.callback.from(value, '#property_price_max', rent_type, 'price');
                        }

                    }
                });

                $('#property_price_max').dropdown({
                    values: pages.landing.dropdown.data.price_sale_max,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.regular.store_variables.price.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.regular.store_variables.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price_sale_min
                        } else {
                            var rent_type = pages.landing.dropdown.data.price_rent_min
                        }
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.price.max = undefined;
                        } else {
                            pages.landing.dropdown.callback.to(value, '#property_price_min', rent_type, 'price');
                        }
                    }
                });

                $('#property_furnishing').dropdown({
                    values: pages.landing.dropdown.data.furnishing,
                    placeholder: _.has(lang_static, 'form.placeholders.furnishing') ? _.get(lang_static, 'form.placeholders.furnishing') : 'Furnishing',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.property.regular.store_variables.query_string.furnishing =   parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.query_string.furnishing =   undefined;
                        }
                    }
                });

                $('#completion_status').dropdown({
                    values: pages.landing.dropdown.data.completion_status,
                    placeholder: _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.property.regular.store_variables.query_string.status = [];
                            search.property.regular.store_variables.query_string.status.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.property.regular.store_variables.query_string.status = [];
                        }
                    }
                });

            },
            callback: {
                from: function(value, id, path, type) {

                    $(id).dropdown('clear');

                    var clone = path,
                    array = clone.slice(0);

                    array.splice(0, helpers.indexof(array, "value", parseInt(value)));

                    $(id).empty();

                    if(value != 0){
                        array.unshift({name: $(id).dropdown('get text'), value: ''});
                    }

                    $.each(array, function(i, item) {
                        $(id).append($('<option>', {
                            value: item.value,
                            text: item.name
                        }))
                    })

                    if (!(($(id).dropdown('get text') == 'Max Bed') || ($(id).dropdown('get text') == 'Max Bath') || ($(id).dropdown('get text') == 'Max Area') || ($(id).dropdown('get text') == 'Max Price'))) {

                        if (_.includes($(id).dropdown('get text'), 'Sq. ft')) {
                            $(id).dropdown('set selected', $(id).dropdown('get text').match(/\d+/)[0]);
                        } else if (_.includes($(id).dropdown('get text'), 'Beds') || _.includes($(id).dropdown('get text'), 'Bed')) {
                            if ($(id).dropdown('get text') === 'Studio') {
                                $(id).dropdown('set selected', 0);
                            } else {
                                $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                            }
                        } else if (_.includes($(id).dropdown('get text'), 'Baths')) {
                            $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                        } else if (_.includes($(id).dropdown('get text'), 'AED')) {
                            var price_with_comma = $(id).dropdown('get text').substr($(id).dropdown('get text').indexOf(" ") + 1);
                            var remove_comma = parseFloat(price_with_comma.replace(/,/g, ''))
                            $(id).dropdown('set selected', remove_comma);
                        }

                    } else {
                        $(id).dropdown('clear');
                    }

                },
                to: function(value, id, path, type) {

                    //
                    // var clone = path,
                    //     array = clone.slice(0);
                    //
                    // array.splice(helpers.indexof(array, "value", parseInt(value)) + 1, array.length);
                    //
                    // $(id).empty();
                    //
                    // $.each(array, function(i, item) {
                    //     $(id).append($('<option>', {
                    //         value: item.value,
                    //         text: item.name
                    //     }))
                    // })
                    //
                    // if (!(($(id).dropdown('get text') == 'Min Bed') || ($(id).dropdown('get text') == 'Min Bath') || ($(id).dropdown('get text') == 'Min Area') || ($(id).dropdown('get text') == 'Min Price'))) {
                    //
                    //     if (_.includes($(id).dropdown('get text'), 'Sq. ft')) {
                    //         $(id).dropdown('set selected', $(id).dropdown('get text').match(/\d+/)[0]);
                    //     } else if (_.includes($(id).dropdown('get text'), 'Beds') || _.includes($(id).dropdown('get text'), 'Bed')) {
                    //         if ($(id).dropdown('get text') === 'Studio') {
                    //             $(id).dropdown('set selected', 0);
                    //         } else {
                    //             $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                    //         }
                    //     } else if (_.includes($(id).dropdown('get text'), 'Baths')) {
                    //         $(id).dropdown('set selected', $(id).dropdown('get text')[0]);
                    //     } else if (_.includes($(id).dropdown('get text'), 'AED')) {
                    //         var price_with_comma = $(id).dropdown('get text').substr($(id).dropdown('get text').indexOf(" ") + 1);
                    //         var remove_comma = parseFloat(price_with_comma.replace(/,/g, ''))
                    //         $(id).dropdown('set selected', remove_comma);
                    //     }
                    //
                    // } else {
                    //     $(id).dropdown('clear');
                    // }
                },
                rent_buy: function(value) {

                    if (value === 'Commercial-Rent' || value === 'Commercial-Sale') {
                        $("#property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing").dropdown().addClass("disabled");

                        $('#property_type').empty();

                        if (value === 'Commercial-Rent') {
                            $.each(pages.landing.dropdown.data.property_type.commercial.rent, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        } else {
                            $.each(pages.landing.dropdown.data.property_type.commercial.sale, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        }

                        $('#property_type, #property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing').dropdown('clear');

                    } else {
                        $("#property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing").dropdown().removeClass("disabled");

                        $('#property_type').empty();

                        if (value === 'Rent') {
                            $.each(pages.landing.dropdown.data.property_type.residential.rent, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        } else {
                            $.each(pages.landing.dropdown.data.property_type.residential.sale, function(i, item) {
                                $('#property_type').append($('<option>', {
                                    value: item.value,
                                    text: item.name
                                }))
                            })
                        }

                        $('#property_type, #property_bedroom_min, #property_bedroom_max, #property_bathroom_min, #property_bathroom_max, #property_furnishing').dropdown('clear');

                    }

                    if (value === 'Sale' || value === 'Commercial-Sale') {

                        $('#property_price_min, #property_price_max').empty();

                        $.each(pages.landing.dropdown.data.price_sale_min, function(i, item) {
                            $('#property_price_min').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $.each(pages.landing.dropdown.data.price_sale_max, function(i, item) {
                            $('#property_price_max').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $('#property_price_min, #property_price_max').dropdown('clear');

                    } else if (value === 'Rent' || value === 'Commercial-Rent' || value === 'Short-term-Rent') {

                        $('#property_price_min, #property_price_max').empty();

                        $.each(pages.landing.dropdown.data.price_rent_min, function(i, item) {
                            $('#property_price_min').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $.each(pages.landing.dropdown.data.price_rent_max, function(i, item) {
                            $('#property_price_max').append($('<option>', {
                                value: item.value,
                                text: item.name
                            }))
                        })

                        $('#property_price_min, #property_price_max').dropdown('clear');

                    }
                }
            },
            data: helpers.unshift(searchJson.property)
        },
        ideal_property: {
            __init: function() {

                helpers.phonenumber.init('ideal_property_phone');

                var auto = document.getElementById('ideal_property_location');

                new Awesomplete(auto, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    autoFirst: true
                });

                var mouseX = 0;
                var mouseY = 0;

                document.addEventListener("mousemove", function(e) {
                    mouseX = e.clientX;
                    mouseY = e.clientY;
                });

                //$('.ui.modal.ideal_property').modal('setting', 'transition', 'vertical flip').modal('show');

                $(document).mouseleave(function () {
                    if ( (mouseY < 100) && (localStorage.idealProperty === undefined) ) {
                        localStorage.idealProperty = 'yes';
                        $('.ui.modal.ideal_property').modal('setting', 'transition', 'vertical flip').modal('show');
                    }
                });

            }
        },
        general_enquiry: {
            __init: function(){

                var location_field = document.getElementById('ge_location'),
                    user = JSON.parse(helpers.store.read('user_o'));

                new Awesomplete(location_field, {
                    list: searchJson.property.location,
                    maxItems: 30,
                    autoFirst: true
                });

                $('div.ge.dropdown').dropdown();

                helpers.phonenumber.init('ge_phone');

                var data = {
                    name: _.has(user, 'name') ? user.name : '',
                    email: _.has(user, 'email') ? user.email : '',
                }

                $('#ge_name').val(data.name);
                $('#ge_email').val(data.email);

                validate.rules.lead.general_enquiry.__init();
            }
          },
        quick_access:   {
            __init      :   function(el){

                search.property.regular.store_variables.rent_buy = [ $(el).data('rent_buy') ];
                search.property.regular.store_variables.type = [ $(el).data('type') ];
                var price_min = $(el).data('min'),
                    price_max = $(el).data('max');
                if( price_min !== '' ) {
                    search.property.regular.store_variables.price.min = price_min;
                }
                if( price_max !== '' ) {
                    search.property.regular.store_variables.price.max = price_max;
                }

                //Writing the search to localstorage
                helpers.store.write('property_search', helpers.store.stringify(search.property.regular.store_variables));

                //  1.   Construct URL
                var urlPath     =   search.property.regular.construct.url(search.property.regular.store_variables);

                //  1.1.  Get new query string object
                var qrsObj      =   search.property.regular.store_variables.query_string;
                for (var propName in qrsObj) {
                    if (qrsObj[propName] === null || qrsObj[propName] === undefined) {
                        delete qrsObj[propName];
                    }
                }

                //  2.  Retain search with previous query string
                var url_prev        =   new URI( window.location.href );
                var qrsObj_url_prev =   (url_prev.query()).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];

                var qrsObj_prev     =   {};
                if(_.has(qrsObj_url_prev,'agency_id')){
                    qrsObj_prev.agency_id   =   qrsObj_url_prev.agency_id;
                }
                if(_.has(qrsObj_url_prev,'sort')){
                    qrsObj_prev.sort        =   qrsObj_url_prev.sort;
                }

                //  3.  Construct URL with new and previous query strings
                var url         =   new URI( urlPath );
                url         =   url.setSearch( qrsObj );
                url         =   url.setSearch( qrsObj_prev );

                window.location.href =  url.toString();

            }
        }
    },
    property: {
        listings: {
            map: {
                __init: function() {

                    google_maps.listings.__init('property_listings_map');

                    $('.ui.sticky.map, #property_list_view, #property_grid_view').fadeIn();
                    $('.property_listings_ad, #property_map_view').hide();

                    $('#property_card_grid').attr('id', 'property_card_list');
                    $("#property_card_list").attr('class', 'eleven wide column');
                    $('#property_card_list > .ui.two.column.grid').attr('class', 'ui one column grid');
                    $('#property_card_list').siblings('.five.wide.column').attr('class', 'five wide column');

                    //Sticky map in listings page
                    setTimeout(function() {
                        $('.ui.sticky.map').sticky({
                            context: '#pages_property_listings'
                        });
                    }, 1000);

                    //Bind mouseenter/hover event for google maps trigger on card
                    google_maps.events.bind.mouseenter('.ui.segment');

                    helpers.store.write('card_view', helpers.store.stringify('map'));
                }
            },
            grid: {
                __init: function() {
                    $('.ui.sticky.map, #property_grid_view').hide();
                    $('#property_card_list').attr('id', 'property_card_grid');
                    $('#property_card_grid, .property_listings_ad, #property_list_view, #property_map_view').fadeIn();
                    $("#property_card_grid").attr('class', 'eleven wide column');
                    $('#property_card_grid > .ui.one.column.grid').attr('class', 'ui two column grid');
                    $('#property_card_grid').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter/hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.ui.segment');

                    helpers.store.write('card_view', helpers.store.stringify('grid'));

                    // if($('.p_c:nth-child(5) > div > div > div > div.jss263 > span').html() === 'featured'){
                    //     $(".p_c:nth-child(5)").hide();
                    // }


                }
            },
            list: {
                __init: function() {
                    $('.ui.sticky.map, #property_list_view').hide();
                    $('#property_card_grid').attr('id', 'property_card_list');
                    $('#property_card_list, .property_listings_ad, #property_grid_view, #property_map_view').fadeIn();
                    $("#property_card_list").attr('class', 'eleven wide column');
                    $('#property_card_list > .ui.two.column.grid').attr('class', 'ui one column grid');
                    $('#property_card_list').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter/hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.ui.segment');

                    helpers.store.write('card_view', helpers.store.stringify('list'));
                }
            },
            __init: function() {
                page = 'landing';
                pages.landing.state();

                var view = JSON.parse(localStorage.getItem("card_view"));

                if(view === 'map'){
                    pages.property.listings.map.__init();
                } else if(view === 'grid'){
                    pages.property.listings.grid.__init();
                }

                pages.landing.dropdown.__init();

                //pagination init
                helpers.pagination.__init();

                //sorting init
                helpers.sort.__init();

                // Lazy Load
                helpers.lazy_load();

                // $('.property_listings_ad').sticky();
                $('.gnral-stcky').sticky({
                    bottomOffset : 14,
                    context: '#property_card_list'
                }
                );

                $('.ui.grid aside').sticky();

                // Prepopulate search
                setTimeout(function() {
                    search.property.regular.prepopulate();
                }, 100);

                if(document.getElementById("property_request_phone")){
                    setTimeout(function() {
                        helpers.phonenumber.init('property_request_phone');
                    }, 500);
                }

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                other.update_currency();


            }
        },
        details: {
            slider: {
                __init: function() {

                    $('.slider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerPadding: 0,
                        dots: false,
                        infinite: true,
                        appendDots: '.slider-nav',
                        appendArrows: '.slider-arrows',
                        nextArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--left"><i class="chevron right icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--right"><i class="chevron right icon"></i></a>',
                        prevArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--right"><i class="chevron left icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--left"><i class="chevron left icon"></i></a>'
                    });

                    setTimeout(function() {
                        helpers.phonenumber.init('property_details_email_phone');
                    }, 500);

                }
            },
            map: {
                __init: function(lat, lng) {
                    var center = {};

                    center.lat = Number(lat);
                    center.lng = Number(lng);

                    // No location overlay
                    if (center.lat === 0 || center.lng === 0) {
                        $('.no_loc').show();
                    } else {
                        g_map.map_container = 'property_details_map';
                        g_map.center = center;
                        g_map.map.invoke();
                        g_map.marker();
                        $('.ui.checkbox').checkbox();
                    }
                    return;
                }
            },
            info: {
                __init: function() {

                    var suburb = $('#pages_property_details_data').attr('data-_area');

                    if(suburb === 'jumeirah-lake-towers(-j-l-t)'){
                        suburb = 'jumeirah-lake-towers';
                    }

                    var url = envUrl + 'en/blog/guide/' + suburb;

                    axios.get(url).then(function(response) {
                        var data;

                        if (response.data.length < 5) {
                            $('.local_info_show').addClass('hide');
                        } else {
                            $('.local_info_show').removeClass('hide');
                            data = response.data;
                            $('#property_details_local_info').html(data);
                        }

                    }).catch(function(error) {});

                }
            },
            mortgage: {
                __init: function(trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('mortgage_user_phone');
                    }, 500);

                    $('.ui.modal.property_mortgage').modal('setting', 'transition', 'fade up').modal('show');

                    var years = Number($('#emi_years').val().split(',').join('')),
                    down_payment = Number($('#emi_down_payment').val().split(',').join('')),
                    interest = Number($('#emi_interest').val().split(',').join('')),
                    principal = $('#purchase_price').html().replace(/,/g, ''),
                    finance_amount = principal - down_payment;

                    //Computing EMI
                    var i = interest / 100 / 12,
                    n = years * 12,
                    monthly = (principal - down_payment) * (1 - i) * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1),
                    total_amount = monthly * years * 12;

                    $('#project_loan_amount').html($('#purchase_price').html());
                    $('#project_emi_monthly').text(Math.ceil(monthly).toLocaleString('en'));
                    $('#project_emi_down_payment').text('AED ' + Math.ceil(down_payment).toLocaleString('en'));
                    $('#project_emi_total_amount').text('AED ' + Math.ceil(total_amount).toLocaleString('en'));
                    $('#project_emi_total_interest' ).text( 'AED ' + Math.ceil( total_amount - principal ).toLocaleString('en') );
                    $('#project_emi_term').text(years);

                    //finance amount
                    $('#project_emi_finance_amount').text('AED ' + finance_amount.toLocaleString('en'));

                    $('#mortgage_lead').attr('data-model', $(trigger).attr('data-model'));
                }
            },
            dewa: {
                tabs: {
                    energy: {
                        __init: function(){
                            var monthly = parseInt($('#property_monthly_value').html()),
                            elec_cost   =   parseInt($('#monthly_cost_energy_electricity').val()),
                            gas_cost    =   parseInt($('#monthly_cost_energy_gas').val());

                            $('#monthly_cost_energy_electricity, #monthly_cost_energy_gas').change(function(trigger) {

                                var energy = elec_cost + gas_cost,
                                minus = monthly - energy;

                                var new_monthly = minus + parseInt($('#monthly_cost_energy_electricity').val()) + parseInt($('#monthly_cost_energy_gas').val());

                                $('#property_monthly_value').html(Math.round(new_monthly));
                            });

                        }
                    },
                    water: {
                        __init: function(){
                            var monthly = parseInt($('#property_monthly_value').html()),
                            water_cost   =   parseInt($('#monthly_cost_water').val());

                            $('#monthly_cost_water').change(function() {

                                var minus = monthly - water_cost;

                                var new_monthly = minus + parseInt($('#monthly_cost_water').val());

                                $('#property_monthly_value').html(Math.round(new_monthly));

                            });

                        }
                    }
                },
                sale : {
                    __init: function(){

                        var years = Number($('#emi_years').val().split(',').join('')),
                        down_payment = Number($('#emi_deposit').val().split(',').join('')),
                        interest = Number($('#emi_interest').val().split(',').join('')),
                        principal = $('#purchase_price').html().replace(/,/g, ''),
                        energy_electricity = !_.isEmpty($('#monthly_cost_energy_electricity').val()) ? parseInt($('#monthly_cost_energy_electricity').val()) : 0,
                        energy_gas = !_.isEmpty($('#monthly_cost_energy_gas').val()) ? parseInt($('#monthly_cost_energy_gas').val()) : 0,
                        energy = energy_electricity + energy_gas,
                        water = parseInt($('#monthly_cost_water').val());

                        //Computing EMI
                        var i = interest / 100 / 12,
                        n = years * 12,
                        monthly = (principal - down_payment) * (1 - i) * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1) + energy + water,
                        total_amount = monthly * years * 12;

                        $('#project_loan_amount').html($('#purchase_price').html());
                        $('#project_emi_monthly').text(Math.ceil(monthly).toLocaleString('en'));
                        $('#project_emi_down_payment').text('AED ' + Math.ceil(down_payment).toLocaleString('en'));
                        $('#project_emi_total_amount').text('AED ' + Math.ceil(total_amount).toLocaleString('en'));
                        $('#project_emi_total_interest' ).text( 'AED ' + Math.ceil( total_amount - principal ).toLocaleString('en') );
                        $('#project_emi_term').text(years);

                        $('#property_monthly_value').html(Math.round(monthly).toLocaleString('en'));

                    }
                },
                rent: {
                    __init: function(){
                        //TODO add cheque calculator
                    }
                }
            },
            get_home_finance: function(e){

                $('html, body').animate({
                    scrollTop: $("#property_details_email_data").offset().top //scroll to this div so the mortgage calc looks in center of screen
                }, 1000);

                setTimeout(function(){
                    $('.wiggle').addClass('wiggle-animate');
                }, 1100);

            },
            __init: function() {

                page = 'landing';
                pages.landing.state();

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                pages.landing.dropdown.__init();
                $('.secondary.menu .item').tab();
                helpers.lazy_load();

                //Property local info
                // pages.property.details.info.__init();

                setTimeout(function(){
                    pages.property.details.slider.__init();
                    other.update_currency();
                }, 1200);

                pages.property.details.map.__init($('#property_details_map').attr('data-lat'), $('#property_details_map').attr('data-lng'));

                //dewa calculations init for sale and rent
                setTimeout(function(){

                    let url =   new URI( window.location.href );

                    if( url.segment(1) == 'sale' ){
                        pages.property.details.dewa.sale.__init();

                        $('#emi_deposit, #emi_years, #emi_interest').change(function() {
                            pages.property.details.dewa.sale.__init();
                        });

                    } else {
                        pages.property.details.dewa.rent.__init();
                    }

                }, 2000);

                //Property similar properties
                search.property.similar($('#similar_properties').attr('data-_id'), 'similar_properties');

            }
        }
    },
    budget: {
        landing: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                pages.budget.dropdown.__init();

            }
        },
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                pages.budget.dropdown.__init();

                search.property.budget.listings.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.property.budget.prepopulate();
                }, 1500);
            },
            sort: {
                __init: function(_class) {
                    $('.p_c').hide();
                    $('.' + _class).show();
                }
            }
        },
        dropdown: {
            __init: function() {

                $('#budget_city').dropdown({
                    values: pages.landing.dropdown.data.cities,
                    placeholder: _.has(lang_static, 'global.emirate') ? _.get(lang_static, 'global.emirate') : 'Emirate',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.city = [];
                            search.property.budget.store_variables.city.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                    }
                });

                $('#property_rent_buy').dropdown({
                    values: pages.landing.dropdown.data.rent_buy,
                    placeholder: _.has(lang_static, 'global.rent_buy') ? _.get(lang_static, 'global.rent_buy') : 'Rent or Buy',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.rent_buy = [];
                            search.property.budget.store_variables.rent_buy.push(value);
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        pages.landing.dropdown.callback.rent_buy(value);
                    }
                });

                $('#property_price_min').dropdown({
                    values: pages.landing.dropdown.data.price.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.min.price') ? _.get(lang_static, 'form.placeholders.min.price') : 'Min Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.budget.store_variables.price.min != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.price.min = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price.sale
                        } else {
                            var rent_type = pages.landing.dropdown.data.price.rent
                        }
                        pages.landing.dropdown.callback.from(value, '#property_price_max', rent_type, 'price');
                    }
                });

                $('#property_price_max').dropdown({
                    values: pages.landing.dropdown.data.price.sale,
                    placeholder: _.has(lang_static, 'form.placeholders.max.price') ? _.get(lang_static, 'form.placeholders.max.price') : 'Max Price',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && (search.property.budget.store_variables.price.max != parseInt(value)) && !_.isEmpty(value)) {
                            search.property.budget.store_variables.price.max = parseInt(value);
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if ($('#property_rent_buy').dropdown('get text') === 'Buy' || $('#property_rent_buy').dropdown('get text') === 'Commercial Buy') {
                            var rent_type = pages.landing.dropdown.data.price.sale
                        } else {
                            var rent_type = pages.landing.dropdown.data.price.rent
                        }
                        pages.landing.dropdown.callback.to(value, '#property_price_min', rent_type, 'price');
                    }
                });
            }
        }
    },
    services: {
        __init: function() {
            page = 'landing';
            pages.landing.state();
            // $('.ui.sticky').sticky({context: '#listings_wrapper'});
            pages.services.dropdown.__init();
        },
        dropdown: {
            __init: function() {
                setTimeout(function() {
                    $('#services_service').dropdown({
                        values: pages.services.dropdown.data.list,
                        placeholder: _.has(lang_static, 'form.placeholders.search_service') ? _.get(lang_static, 'form.placeholders.search_service') : 'Search for a service',
                        maxSelections: 6,
                        action: function(text, value, element) {
                            $("#search").attr('data-_id', value);
                            $(this).dropdown('set selected', value).dropdown('hide');
                        }
                    });
                }, 1000);
            },
            data: searchJson.service
        },
        listings: {
            contact: {
                __init: function(trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_services_contact_user_phone');
                    }, 500);

                    //1
                    var data = {
                        contact: {
                            name: $(trigger).parent().attr("data-service_contact_name") ? $(trigger).parent().attr("data-service_contact_name") : 'Service name not available',
                            logo: $(trigger).parent().attr("data-service_logo") ? $(trigger).parent().attr("data-service_logo") : 'Logo not available',
                        },
                        writeup: {
                            description: $(trigger).parent().attr("data-service_description") ? $(trigger).parent().attr("data-service_description") : 'Description not available'
                        },
                        _id: $(trigger).parent().attr("data-service_id")
                    }

                    //2
                    $('#modal_services_contact_name').html(data.contact.name);
                    $('#modal_services_contact_logo').attr('src', data.contact.logo);
                    $('#modal_services_contact_description').html(data.writeup.description);
                    $('#modal_services_contact_data').attr('data-service_id', data._id);
                    $('#modal_services_contact_data').attr('data-subsource', 'desktop');

                    //3
                    $('.ui.modal.services_contact').modal('setting', 'transition', 'vertical flip').modal('show');

                    helpers.store.temp_data.write = data;

                    other.datalayer('service', 'contact_click', data);

                }
            }
        }
    },
    projects: {
        listings: {
            map: {
                __init: function() {

                    google_maps.listings.__init('projects_listings_map');

                    $('.ui.sticky.map, #projects_list_view, #projects_grid_view').fadeIn();
                    $('.projects_listings_ad, #projects_map_view, .enquiry_project').hide();

                    $('#projects_card_grid').attr('id', 'projects_card_list');
                    $("#projects_card_list").attr('class', 'ten wide column');

                    $('#projects_card_list > .ui.link.two.cards').attr('class', 'ui link one cards');
                    // $('#projects_card_list').siblings('.five.wide.column').attr('class', 'six wide column');

                    //Sticky map in listings page
                    setTimeout(function() {
                        $('.ui.sticky.map').sticky({
                            context: '#pages_projects_listings'
                        });
                    }, 1000);

                    //Bind mouseenter/hover event for google maps trigger on card
                    google_maps.events.bind.mouseenter('.card');

                }
            },
            grid: {
                __init: function() {
                    $('.ui.sticky.map, #projects_grid_view').hide();
                    $('#projects_card_list').attr('id', 'projects_card_grid');
                    $('#projects_card_grid, .projects_listings_ad, #projects_list_view, #projects_map_view, .enquiry_project').fadeIn();

                    // $("#projects_card_grid").attr('class', 'eleven wide column');
                    $('#projects_card_grid > .ui.link.one.cards').attr('class', 'ui link two cards');
                    // $('#projects_card_grid').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter/hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.card');

                }
            },
            list: {
                __init: function() {
                    $('.ui.sticky.map, #projects_list_view').hide();
                    $('#projects_card_grid').attr('id', 'projects_card_list');
                    $('#projects_card_list, .projects_listings_ad, #projects_grid_view, #projects_map_view, .enquiry_project').fadeIn();

                    // $("#projects_card_list").attr('class', 'eleven wide column');
                    $('#projects_card_list > .ui.link.two.cards').attr('class', 'ui link one cards');
                    // $('#projects_card_list').siblings('.six.wide.column').attr('class', 'five wide column');

                    //Unbind mouseenter//hover event for google maps trigger on card
                    google_maps.events.unbind.mouseenter('.card');
                }
            },
            __init: function(country='uae') {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.projects.dropdown.__init(country);

                //pagination init
                helpers.pagination.__init();

                //sorting init
                helpers.sort.__init();

                // Initiate Lazy Load
                helpers.lazy_load();

                // Prepopulate search
                setTimeout(function() {
                    search.projects.prepopulate(country);
                }, 100);
                //
                helpers.phonenumber.init('modal_developer_enquiry_phone');

                setTimeout(function() {
                    $('ui.sticky.enquiry_project').sticky({
                        context: '#pages_projects_listings'
                    });
                }, 1000);

                $('.progress').progress();

                validate.rules.lead.project.enquiry.__init();
            }
        },
        details: {
            __init: function(country='uae') {
                page = 'landing';
                pages.landing.state();
                // pages.projects.dropdown.__init(country);
                pages.projects.details.slider.__init();
                pages.projects.details.map.__init($('#projects_details_map').attr('data-lat'), $('#projects_details_map').attr('data-lng'));
                //Tabs init
                $('#main-tab .item').tab();
                $('#multimedia .item').tab();

                //Accordin init for floor plans
                // $('.ui.accordion').accordion();

                // Initiate Lazy Load
                helpers.lazy_load();
                setTimeout(function() {
                    helpers.phonenumber.init('project_lead_phone', false);
                }, 500);

                $('.popup-video-html5').magnificPopup({
                    type: 'iframe'
                });

                pages.property.listings.list.__init();

            },
            view_video_360: function(id){
                // $('html, body').animate({
                //     scrollTop: $("#" + id).offset().top
                // }, 1000);

                if(id === 'view_video'){
                    var url = $('#video_source').attr('src');
                    if(_.includes(url, '?autoplay')){
                        var url_upd = $('#video_source').attr('src').substring(0, $('#video_source').attr('src').indexOf('?'))
                        $('#video_source').attr('src', url_upd + '?autoplay=true');
                    }else{
                        $("#video_src")[0].play();
                    }
                }
            },
            slider: {
                __init: function() {

                    $('.slider .slides').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        lazyLoad: 'ondemand',
                        dots: false,
                        infinite: true,
                        appendDots: '.slider-nav',
                        appendArrows: '.slider-arrows',
                        nextArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--left"><i class="chevron right icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--right"><i class="chevron right icon"></i></a>',
                        prevArrow: (selectedLanguage == 'ar') ? '<a href="#" dir="rtl" class="slider-arrow slider-arrow--right"><i class="chevron left icon"></i></a>' : '<a href="#" dir="ltr" class="slider-arrow slider-arrow--left"><i class="chevron left icon"></i></a>'
                    });


                    // $('.slides').slick({
                    //     slidesToShow: 1,
                    //     slidesToScroll: 1,
                    //     dots: true,
                    //     arrows: false,
                    //     infinite: true,
                    //     autoplay: false
                    // });


                    // $('.slider').glide({
                    //     autoplay: false,
                    //     arrowsWrapperClass: 'slider-arrows',
                    //     arrowRightText: '',
                    //     arrowLeftText: ''
                    // });
                    // $('.slider-arrow--right').append('<i class="chevron right icon"></i>');
                    // $('.slider-arrow--left').append('<i class="chevron left icon"></i>');
                }
            },
            map: {
                __init: function(lat, lng) {
                    var center = {};

                    center.lat = Number(lat);
                    center.lng = Number(lng);

                    // No location overlay
                    if (center.lat === 0 || center.lng === 0) {
                        $('.no_loc').show();
                    } else {
                        g_map.__init(center, [], 'projects_details_map');
                        $('.ui.checkbox').checkbox();
                    }
                    return;
                }
            },
            call: {
                __init: function(data_trigger) {

                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') : 'Phone not available',
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        subsource: 'desktop'
                    }

                    $('#modal_developer_call_developer_contact_phone').html(data.developer.phone);

                    $('#modal_developer_call_project_name').html(data.project.name);
                    $('#modal_developer_call_contact_name').html(data.developer.name);
                    $('#modal_developer_call_data').attr('data-_id', data.project._id);
                    $('#modal_developer_call_data').attr('data-subsource', data.subsource);

                    $('.ui.modal.developer_call').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','call_click', data_trigger);
                    other.datalayer('project','call_submit', data_trigger);

                    lead.project.call.__init();
                }
            },
            email: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    * 4.   Validate rules init
                    */

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_developer_email_phone');
                    }, 500);

                    //1
                    var data = {
                        developer: {
                            name:  _.has(data_trigger, 'developer.contact.name') ? _.get(data_trigger, 'developer.contact.name') : 'Developer not available',
                            phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : (_.has(data_trigger, 'developer.contact.phone') ? _.get(data_trigger, 'developer.contact.phone') :'Phone not available')
                        },
                        project: {
                            name: _.has(data_trigger, 'name') ? _.get(data_trigger, 'name') : 'Name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        subsource: 'desktop',
                        custom_form: 'setting.custom_form'
                    }
                    // helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_property_email_agent_img');

                    //2
                    $('#modal_developer_email_project_name').html(data.project.name);
                    $('#modal_developer_email_contact_name').html(data.developer.name);
                    $('#modal_developer_email_data').attr('data-custom_form', data.custom_form);
                    $('#modal_developer_email_data').attr('data-_id', data.project._id);
                    $('#modal_developer_email_data').attr('data-subsource', data.subsource);

                    //3
                    $('.ui.modal.developer_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('project','email_click', data_trigger);
                }
            }
        },
        dropdown: {
            __init: function(country) {
                //TODO enable country wise routing
                let uae = 'united arab emirates (uae)';
                country =   country == 'uae' ? uae : country;
                if(country=='international'){
                    projects_project_values     =   _.omit(pages.projects.dropdown.data.developers, uae);
                    completion_status_values    =   _.omit(pages.projects.dropdown.data.completion_status, uae);
                    project_locations_list      =   _.omit(pages.projects.dropdown.data.locations, uae);

                    projects_project_values     =   _.uniqBy(_.flatten(_.map(projects_project_values, 'developers')),'value');
                    completion_status_values    =   _.uniqBy(_.flatten(_.map(completion_status_values, 'completion_status')),'value');
                    project_locations_list      =   _.uniqBy(_.flatten(_.map(project_locations_list, 'locations')),'value');
                }
                else{
                    projects_project_values     =   _.orderBy(pages.projects.dropdown.data.developers[country].developers,'name','asc');
                    completion_status_values    =   pages.projects.dropdown.data.completion_status[country].completion_status;
                    project_locations_list      =   pages.projects.dropdown.data.locations[country].locations;
                }

                $('#projects_project').dropdown({
                    values: projects_project_values,
                    placeholder: _.has(lang_static, 'form.placeholders.all_developer') ? _.get(lang_static, 'form.placeholders.all_developer') : 'All Developer',
                    maxSelections: 1,
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.projects.store_variables.developer = value;
                        }
                    },
                    action: function(text, value, element) {
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.projects.store_variables.developer = undefined;
                        }
                    }
                });

                $('#completion_status').dropdown({
                    values: completion_status_values,
                    placeholder: _.has(lang_static, 'form.placeholders.completion_status') ? _.get(lang_static, 'form.placeholders.completion_status') : 'Completion Status',
                    onChange: function(value, text, element) {
                        if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                            search.projects.store_variables.query_string.status = value;
                        }
                    },
                    action: function(text, value, element) {
                        // Storing value in query holder
                        $(this).dropdown('set selected', value).dropdown('hide');
                        if(!value){
                            $(this).dropdown('clear');
                            search.projects.store_variables.query_string.status = undefined;
                        }
                    }
                });

                var auto = document.getElementById('project_location');

                new Awesomplete(auto, {
                    // list: searchJson.project.locations,
                    list: project_locations_list,
                    maxItems: 30,
                    autoFirst: true,
                    replace: function (text) {
                        if (!_.isUndefined(text) && !_.isEmpty(text)) {
                            search.projects.store_variables.location = text.label;
                        }
                        this.input.value = search.projects.store_variables.location;
                    }
                });

            },
            data: searchJson.project
        }
    },
    agent: {
        landing: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.agent.dropdown.__init();
            }
        },
        listings: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                // $('.ui.sticky').sticky({context: '#listings_wrapper'});
                pages.agent.dropdown.__init();

                // Lazy Load
                helpers.lazy_load();

                //pagination init
                helpers.pagination.__init();

                // Prepopulate search
                setTimeout(function() {
                    search.agent.prepopulate();
                }, 1500);

                $('.popup-video-youtube').magnificPopup({
                    type: 'iframe'
                });
            },
            call: {
                __init: function(data_trigger) {

                  /**
                  * 1.   Create Object with Data from trigger
                  * 2.   Bind Data to ID's
                  * 3.   Show Modal
                  */
                  var user = JSON.parse(helpers.store.read('user_o'));
                    //1
                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            phone: _.has(data_trigger, 'number') ? _.get(data_trigger, 'number') : 'Phone not available'
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        },
                        user: {
                            name: user ? user.name : 'anonymous',
                            email: user ? user.email : 'anonymous@domain.com',
                            phone: user && _.has(user, 'user.phone') ? user.phone : ''
                        },
                        _id: _.get(data_trigger, '_id')
                    }

                    if(!data.agent.phone.includes("+") && !_.startsWith(data.agent.phone,'0')){
                        $('#modal_agent_call_agent_contact_phone').html('+' + data.agent.phone);
                    }
                    else if(_.startsWith(data.agent.phone,'0')){
                        $('#modal_agent_call_agent_contact_phone').html(data.agent.phone.replace('0', '+971'));
                    } else {
                        $('#modal_agent_call_agent_contact_phone').html(data.agent.phone);
                    }

                    //2
                    $('#modal_agent_call_agent_name').html(data.agent.name);
                    $('#modal_agent_call_agency_name').html(data.agency.name);

                    //3
                    $('.ui.modal.agent_call').modal('setting', 'transition', 'vertical flip').modal('show');
                    //4
                    var payload = {
                        action: 'call',
                        source: 'consumer',
                        subsource: 'desktop',
                        message: _.has(lang_static, 'modals.agent.call.payload_msg') ? _.get(lang_static, 'modals.agent.call.payload_msg') : 'Hi, I found your profile on Zoom Property. Please contact me. Thank you.',
                        agent: {
                            _id: data._id
                        },
                        user: {
                            contact: {
                                name: data.user.name,
                                email: data.user.email,
                                phone: data.user.phone
                            }
                        }
                    }

                    if (payload.user.contact.phone.length === 0) {
                        delete payload.user.contact.phone
                    }

                    //storing the payload for call buton api call
                    lead.agent.store.save = payload;


                    payload.agent_name = data.agent.name;
                    payload.agency_name = data.agency.name;

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','call_submit', data_trigger);

                    lead.agent.call.__init();

                }
            },
            email: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_agent_email_phone');
                    }, 500);

                    pages.signup_guide_slider.__init( 'email-slider' );

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Bind Data to ID's
                    * 3.   Show Modal
                    */

                    //1
                    var data = {
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            picture: _.has(data_trigger, 'contact.picture') ? _.get(data_trigger, 'contact.picture') : '',
                            _id: _.get(data_trigger, '_id')
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        }
                    }

                    helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_agent_email_agent_img');

                    //2
                    // $('#modal_agent_email_agent_img').attr("src", data.agent.picture);
                    $('#modal_agent_email_agent_name').html(data.agent.name);
                    $('#modal_agent_email_agency_name').html(data.agency.name);
                    $('#modal_agent_email_data').attr('data-_id', data.agent._id);

                    //3
                    $('.ui.modal.agent_email').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','email_click', data_trigger);

                }
            },
            callback: {
                __init: function(data_trigger) {

                    setTimeout(function() {
                        helpers.phonenumber.init('modal_agent_callback_phone');
                    }, 500);

                    //1
                    var data = {
                        subsource: 'desktop',
                        agent: {
                            name: _.has(data_trigger, 'contact.name') ? _.get(data_trigger, 'contact.name') : 'Agent name not available',
                            _id: _.get(data_trigger, '_id')
                        },
                        agency: {
                            name:  _.has(data_trigger, 'agency.contact.name') ? _.get(data_trigger, 'agency.contact.name') : 'Agency name not available'
                        }
                    }

                    // helpers.modal_image_load(data.agent.picture, cdnUrl + 'assets/img/default/no-agent-img.png', '#modal_agent_callback_agent_img');

                    //2
                    // $('#modal_agent_callback_agent_name').html(data.agent.name);
                    // $('#modal_agent_callback_agency_name').html(data.agency.name);
                    // $('#modal_agent_callback_textarea').html('Hi, I found your property with ref: ' + data.property.reference + ' on Zoom Property. Please contact me. Thank you.')
                    $('#modal_agent_callback_data').attr('data-_id', data.agent._id);
                    $('#modal_agent_callback_data').attr('data-subsource', data.subsource);
                    //3
                    $('.ui.modal.agent_callback').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','callback_click', data_trigger);

                }
            },
            whatsapp: {
                __init: function(trigger) {

                    if ($(trigger).parent().attr("data-agent_contact_phone").length === 0) {
                        alert('Phone number not available');
                    } else {

                        //whatsapp lead create api
                        lead.agent.whatsapp.__init(payload);

                        window.open(
                            'https://wa.me/' + $(trigger).parent().attr("data-agent_contact_phone"),
                            '_blank'
                        );

                    }
                }
            }
        },
        details: {
            __init: function() {
                page = 'landing';
                pages.landing.state();
                /**
                * 1.   Load Properties for agent
                * 2.   Populate popup with Agent Data
                */
                // 1.
                var sink = 'agent_properties';
                var query = new search.property.regular.variables();
                query.agent._id = $('#' + sink).attr('data-_id');
                query.size = 3;
                search.property.listings(query, sink);

                pages.agent.dropdown.__init();

                $('.popup-video-youtube').magnificPopup({
                    type: 'iframe'
                });

                $('.rating').rating('disable');
            },
            review: {
                __init: function(data_trigger) {

                    /**
                    * 1.   Create Object with Data from trigger
                    * 2.   Show Modal
                    */

                    //1
                    var data = {
                        agent: {
                            _id: _.get(data_trigger, '_id')
                        },
                    }

                    $('#modal_agent_review_rate').rating();

                    //2
                    $('.ui.modal.agent_review').modal('setting', 'transition', 'vertical flip').modal('show');

                    //gtm push on click submit
                    helpers.store.temp_data.write = data_trigger;
                    other.datalayer('agent','review_click', data_trigger);

                }
            },
        },
        dropdown: {
            __init: function() {

                setTimeout(function() {

                    $('#agent_brokerage').dropdown({
                        values: _.orderBy(pages.agent.dropdown.data.list,'name', 'asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.brokerage') ? _.get(lang_static, 'form.placeholders.brokerage') : 'Brokerage',
                        maxSelections: 1,
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                                search.agent.store_variables.agency.push(value);
                                search.agent.store_variables.agency = [_.uniq(search.agent.store_variables.agency).pop()];
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if(!value){
                                $(this).dropdown('clear');
                                search.agent.store_variables.agency = undefined;
                            }
                        }
                    });

                    $('#agent_areas').dropdown({
                        values: _.orderBy(pages.agent.dropdown.data.areas,'name','asc'),
                        placeholder: _.has(lang_static, 'form.placeholders.areas') ? _.get(lang_static, 'form.placeholders.areas') : 'Areas',
                        maxSelections: 1,
                        onChange: function(value, text, element) {
                            if (!_.isUndefined(value) && !_.isEmpty(value) ) {
                                areas = value.split(',');
                                locations = areas[areas.length - 1];
                                search.agent.store_variables.area.push(locations);
                                search.agent.store_variables.area = [_.uniq(search.agent.store_variables.area).pop()];
                            }
                        },
                        action: function(text, value, element) {
                            $(this).dropdown('set selected', value).dropdown('hide');
                            if(!value){
                                $(this).dropdown('clear');
                                search.agent.store_variables.area = undefined;
                            }
                        }
                    });

                }, 1000);

            },
            data: searchJson.brokerage
        }
    },
    components: {
        modal: {
            signin: {
                toggle: {
                    __init: function(id) {

                        helpers.clear.errors();

                        let array = [$('#modal_signin'), $('#modal_register'), $('#modal_forgot_password')];

                        for (var k = 0; k < array.length; k++) {
                            each = array[k];
                            if (id === $(each).attr('id')) {
                                $(each).show();
                            } else {
                                $(each).hide();
                            }
                        }

                        if (id === 'modal_register') {

                            var input = document.getElementById('register_input_confirm_password');

                            input.addEventListener("keyup", function(event) {
                                event.preventDefault();
                                if (event.keyCode === 13) {
                                    document.getElementById('register_submit_btn').click();
                                }
                            });

                        } else if (id === 'modal_forgot_password') {

                            var input = document.getElementById('forgot_password_input_confirm_password');

                            input.addEventListener("keyup", function(event) {
                                event.preventDefault();
                                if (event.keyCode === 13) {
                                    document.getElementById('forgot_password_submit_btn').click();
                                }
                            });

                        }

                    }
                }
            }
        }
    },
    signin: {
        modal: {
            __init: function() {

                helpers.clear.errors();

                $('.ui.modal.signin_register').modal('setting', 'transition', 'fade').modal('show');

                var input = document.getElementById("signin_input_password");

                input.addEventListener("keyup", function(event) {
                    event.preventDefault();
                    if (event.keyCode === 13) {
                        document.getElementById("signin_submit_btn").click();
                    }
                });

            }
        }
    },
    reset_password: {
        change_password: function(trigger){

            $(trigger).addClass('loading');

            var password = $('#reset_password_new_password > input').val(),
            confirmPassword = $("#reset_password_confirm_password > input").val();

            $('#change_password_error').html('');

            var url_token = window.location.href,
            parts = url_token.split('/'),
            token = parts.pop();

            var url = baseApiUrl + "user/auth/reset";

            var payload = {
                email: $('#reset_password_email > input').val(),
                password: password,
                password_confirmation: confirmPassword,
                token: token
            }

            axios.post(url, payload).then(function(response){

                $(trigger).removeClass('loading');

                helpers.toast.__init( _.get(lang_static, 'toast.success_password_changed') );

                $('#reset_password_new_password > input').val('');
                $('#reset_password_confirm_password > input').val('');
                $("#reset_password_email > input").val('');

            }).catch(function (error) {

                $(trigger).removeClass('loading');

                if(error.response.data.errors){
                    // error.response.data.errors.email
                    var errs    =   error.response.data.errors;
                    var errors  =   '';
                    for(var i in errs){
                        errors += '<div>'+ i + ' - ' + errs[i]+'</div>';
                    }
                    $('#change_password_error').html('<div class="ui error message">'+errors+'</div>');
                }else{
                    $('#change_password_error').html(error.response.data.message);
                }

                // helpers.toast.__init('Something went wrong, Please try later');

                // $('#reset_password_new_password > input').val('');
                // $('#reset_password_confirm_password > input').val('');
                // $("#reset_password_email > input").val('');

                // $('#change_password_error').html('Something went wrong, Please try later');
            });

        },
        __init: function() {

        }
    },
    signup_guide_slider: {
        __init: function( trigger ) {
            $('.'+trigger).slick({
                arrows: false,
                dots: true,
                autoplay: true,
                autoplaySpeed: 2000
            });
        }
    }
}

$('.counter').each(function() {
    $(this).prop('counter', 0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function(now) {
            $(this).text(Math.ceil(now));
        }
    });
});
