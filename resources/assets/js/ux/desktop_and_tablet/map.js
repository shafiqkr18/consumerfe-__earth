var google_maps = {
    map_id : {},
    map : {},
    center              :   {
		lat				   : 	25.2048,
		lng				   : 	55.2708
	},
    icons : {
        property : envUrl + 'assets/img/map/property-marker.png'
    },
    listings: {
        __init: function(id, coordinates, data){

            map_id = id;

            if(data){
                var property = {
                    title: data.title,
                    type: data.type,
                    rent_buy: data.rent_buy,
                    bed: data.bedroom,
                    bath: data.bathroom,
                    sqft: data.sqft,
                    price: data.price,
                    ref: data.ref,
                    building: data.building,
                    area: data.area,
                    city: data.city,
                    href: data.href
                }
            } else {
                var property = {
                    title: 'Property',
                    rent_buy: 'Property',
                }
            }

            var contentString = '<a href="'+ property.href +'">'+
                '<div id="property_listings_map_hover_card">'+
                '<div>' + property.title + '</div>'+
                '<div>' + property.type + ' for ' + property.rent_buy + '</div>'+
                '<div>' + property.bed + ' bed, ' + property.bath + ' bath, ' + property.sqft + ' sqft' + '</div>'+
                '<div><span>' + 'AED ' + '</span><span>' + property.price + '</span>' + ' | REF: ' + property.ref + '</div>'+
                '<div>' + property.building + ' ' + property.area + ', ' + property.city + '</div>'+
                '</div>'+
                '</a>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            });

            var lat = coordinates ? coordinates.lat : 25.2048,
                lng = coordinates ? coordinates.lng : 55.2708;

            var mapOptions = {
                zoom: coordinates ? 15 : 12,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = google_maps.map = new google.maps.Map(document.getElementById(id), mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: coordinates ? google_maps.icons.property : '',
                map: google_maps.map
            });

            marker.addListener('click', function() {
             infowindow.open(map, marker);
           });

        }
    },
    events: {
        bind : {
            mouseenter : function(class_name){

                $(class_name).mouseenter(function(trigger){
                    $('.no_loc').hide();

                    var coordinates = {
                        lat: Number($(this).attr('data-coordinates_lat')),
                        lng: Number($(this).attr('data-coordinates_lng'))
                    }

                    var data = {
                        title: $(this).attr('data-title'),
                        type: $(this).attr('data-type'),
                        rent_buy: $(this).attr('data-rent_buy'),
                        bedroom: $(this).attr('data-bedroom'),
                        bathroom: $(this).attr('data-bathroom'),
                        sqft: $(this).attr('data-sqft'),
                        price: $(this).attr('data-price'),
                        ref: $(this).attr('data-ref'),
                        building: $(this).attr('data-building'),
                        area: $(this).attr('data-area'),
                        city: $(this).attr('data-city'),
                        href: $(this).attr('data-href')
                    }

                    var agency  =   $(this).attr('data-agency');

                    // No location overlay
                    if(coordinates.lat === 0 || coordinates.lng === 0){
                        $('.no_loc').show();
                        $('.no_loc.text').html(agency + ' has not provided a location for this property');
                    } else {
                        google_maps.listings.__init(map_id, coordinates, data);
                    }

                });

            }
        },
        unbind : {
            mouseenter : function(class_name){
                $(class_name).unbind('mouseenter mouseleave');
            }
        }
    }
}
