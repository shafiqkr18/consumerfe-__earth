<?php return array (
  'keywords' => 
  array (
    0 => 
    array (
      'name' => 'New',
      'value' => 'new',
    ),
    1 => 
    array (
      'name' => 'Daily',
      'value' => 'daily',
    ),
    2 => 
    array (
      'name' => 'Cheap',
      'value' => 'cheap',
    ),
    3 => 
    array (
      'name' => 'Luxury',
      'value' => 'luxury',
    ),
    4 => 
    array (
      'name' => 'Family',
      'value' => 'family',
    ),
    5 => 
    array (
      'name' => 'Featured',
      'value' => 'featured',
    ),
    6 => 
    array (
      'name' => 'Furnished',
      'value' => 'furnished',
    ),
    7 => 
    array (
      'name' => 'Independent',
      'value' => 'independent',
    ),
    8 => 
    array (
      'name' => 'Private Pool',
      'value' => 'private-pool',
    ),
  ),
);