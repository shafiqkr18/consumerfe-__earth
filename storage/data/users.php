<?php
    return [
            'data'  =>  [
                [
                    'contact'   =>  [
                        'name'          =>  'Purple',
                        'email'         =>  'purple@planetearth.com',
                        'password'      =>  'bm9pc2UyNHg3',
                        'nationality'   =>  'Emirati',
                        'phone'         =>  '971299846272'
                    ],
                    'settings'  =>  [
                        'verified'      =>  false,
                        'newsletter'    =>  false,
                        'status'        =>  true,
                        'role_id'       =>  2,
                        'reengage'      =>  false
                    ],
                    '_id'       =>  'zu-purple-3256',
                    'timestamp' =>  [
                        'created'       =>  '',
                        'updated'       =>  ''
                    ]
                ],
                [
                    'contact'   =>  [
                        'name'          =>  'Yellow',
                        'email'         =>  'yellow@planetearth.com',
                        'password'      =>  'bm9pc2UyNHg3',
                        'nationality'   =>  'Indian',
                        'phone'         =>  '971643408245'
                    ],
                    'settings'  =>  [
                        'verified'      =>  false,
                        'newsletter'    =>  false,
                        'status'        =>  true,
                        'role_id'       =>  2,
                        'reengage'      =>  false
                    ],
                    '_id'       =>  'zu-yellow-5743',
                    'timestamp' =>  [
                        'created'       =>  '',
                        'updated'       =>  ''
                    ]
                ]
            ]
        ];
