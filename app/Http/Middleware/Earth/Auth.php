<?php

namespace App\Http\Middleware\Earth;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty( $request->session()->get('contact.email',false) || true)){
            return response()->json( [ 'message' => 'Only authenticated users can go past this point'], 403 );
        }
        return $next($request);

    }
}
