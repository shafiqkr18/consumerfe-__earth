<?php

namespace App\Http\Middleware\Earth;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
        $http_origin    =   $request->server( 'HTTP_ORIGIN' );
        $app_url        =   ( substr( env( 'APP_URL' ), -1 ) == '/' ) ? substr( env( 'APP_URL' ), 0, -1 ) : env( 'APP_URL' );

        if( $http_origin == "https://localhost" || $http_origin ==  $app_url || $http_origin == "https://blog.zoomproperty.com" ){

            $response = $next($request);
            $IlluminateResponse = 'Illuminate\Http\Response';
            $SymfonyResopnse = 'Symfony\Component\HttpFoundation\Response';

            $headers = [
                'Access-Control-Allow-Origin' => $http_origin,
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ];

            if($response instanceof $IlluminateResponse) {
                foreach ($headers as $key => $value) {
                    $response->header($key, $value);
                }
                return $response;
            }

            if($response instanceof $SymfonyResopnse) {
                foreach ($headers as $key => $value) {
                    $response->headers->set($key, $value);
                }
                return $response;
            }

            return $response;

        }
        else{

            return $next( $request );

        }
    }
}
