<?php

namespace App\Http\Middleware\Earth;

use Closure;

class TokenParameters
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        if(!is_null($request->getQueryString())){
            $queries = explode("&", $request->getQueryString());
            foreach( $queries as $query ){
                list( $key, $value )    =   explode( "=", $query );
                if( $key === 'token' ){
                    \Session::put( 'project-token', $value );
                }
                else if( $key === 'key' ){
                    \Session::put( 'project-key', $value );
                }
            }
        }
        return $next($request);
    }
}
