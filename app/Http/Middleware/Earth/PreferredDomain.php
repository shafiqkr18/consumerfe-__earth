<?php

namespace App\Http\Middleware\Earth;

use Closure;
use Illuminate\Support\Facades\Redirect;

class PreferredDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
        $host           =   $request->header('host');
        $ebs            =   env( 'EBS_SERVER' );
        $ebs_ip         =   env( 'EBS_SERVER_IP' );

        if($host == $ebs || $host == $ebs_ip){
            $request->headers->set('host', str_replace(["https:", "/"],["",""],env( 'APP_URL' )));
            return Redirect::to($request->fullUrl(), 301);
        }

        return $next($request);
    }
}
