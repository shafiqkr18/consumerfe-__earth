<?php

namespace App\Http\Middleware\Earth;

use Closure;

class Request
{

    use \App\Traits\JsonValidator;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $leaf       =   strtolower(class_basename($request->route()->controller));
        $payload    =   !empty($request->query('query')) ? json_decode( $request->query('query'), 1 ) : [];

        $this->set_leaf($leaf);
        $this->set_payload($payload);

        $validate   =   $this->validate_request();

        if( is_bool( $validate ) ){
            return $next($request);
        }
        else{
            return response()->json( [ 'errors' => $validate ], 400 );
        }

    }

}
