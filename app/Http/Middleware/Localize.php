<?php

namespace App\Http\Middleware;

use Closure;

class Localize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( is_null( \Session::get( 'zp-locale' ) ) ){
            $user_lang  =   detect_user_languages();
        }
        $lang           =   array_has( config( 'languages' ), $request->route()->parameter('lng','en'), false ) ? $request->route()->parameter('lng','en') : 'en';
        // TODO Uncomment line below to enable redirection to user preferred language site
        // $lang           =   !empty($user_lang) ? $user_lang : $lang;

        if($request->route()->parameter('lng','en') !== $lang){
            $links  =   get_links();
            $path   =   array_get($links,$lang,'en');
            if($request->fullUrl() != $path)
                return redirect($path);
        }

        if(ends_with($request->fullUrl(), '/en')){
            return redirect($request->root());
        }

        #client IP
        if( is_null( \Session::get( 'zp-client-ip' ) ) ){
            \Session::put( 'zp-client-ip', detect_client_ip() );
        }

        \App::setLocale( $lang );
        \Session::put( 'zp-locale', \App::getLocale() );
        return $next($request);
    }
}
