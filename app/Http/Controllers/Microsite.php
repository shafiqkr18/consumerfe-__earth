<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Microsite extends Controller
{
    const leaf = 'microsite';

    public function __construct(Request $request = null){
        $this->payload      =   !empty( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $this->config       =   config('models.'.$this::leaf);
        $this->api_gateway  =   env('APP_ENV') == 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    public function index(){
        $this->api_endpoint     =   array_get($this->config,'api.endpoint.r','');
        if(!array_has($this->payload,'active')){
            array_set($this->payload,'active',1);
        }
        if(in_array(env('APP_ENV'),[ 'production', 'staging', 'local' ])){
            array_set($this->payload,'domain',array(env('APP_ENV')));
        }
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    public function show($id){
        $this->api_endpoint     =   str_replace('#',$id,array_get($this->config,'api.endpoint.s',''));
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    public function get_details(){
        $this->api_endpoint     =   array_get($this->config,'api.endpoint.d','');
        if(in_array(env('APP_ENV'),[ 'production', 'staging', 'local' ])){
            array_set($this->payload,'domain',array(env('APP_ENV')));
        }
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }
}
