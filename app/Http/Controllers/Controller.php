<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use \App\Traits\ApiCaller;
    protected $payload, $config, $request, $api_gateway;

    public function __construct(Request $request = null){
        $this->payload          =   !empty( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : '';
        $this->config           =   config('models.'.$this::leaf);
        $this->api_gateway      =   env('APP_ENV') == 'production' || $this::leaf == 'project' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    /**
     *  1.  Validate Request (middleware)
     *          1.  Payload mapping
     *          2.  Payload validation
     *  2.  Sanitize Request
     *  3.  Api Request
     *  4.  Validate API Response
     *          1.  Response mapping
     *          2.  Response validation
     *  5.  Response trimming
     *  6.  Response Validation (middleware)
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        if(!array_has($this->payload,'settings.status')){
            array_set($this->payload,'settings.status',1);
        }

        if(env('APP_ENV') == 'production'){
            array_set($this->payload,'settings.approved',true);
        }
        // if($this::leaf == 'agent')
        //   dd($this->payload);
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.array_get($this->config,'api.endpoint.r',''));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return  $this   ->set_method('POST')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->leaf);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.str_replace_array('#',[$id],array_get($this->config,'api.endpoint.s','')));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function similar($id){
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.str_replace_array('#',[$id],array_get($this->config,'api.endpoint.si','')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }


    public function set_payload(array $payload){
        $this->payload  =   $payload;
        return $this;
    }

    public function set_request(Request $request){
        $this->request  =   $request;
        return $this;
    }
}
