<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

use Socialite;

class Socialites extends BaseController
{

    public function redirect( $provider )
    {
        return Socialite::driver( $provider )->redirect();

    }

    public function callback( $provider )
    {

        try {

            $socialUserInfo =   Socialite::driver( $provider )->stateless()->user();

            $socialProfileData  =  [
                'provider'      =>  $provider,
                'provider_id'   =>  $socialUserInfo->id,
                'name'          =>  $socialUserInfo->name,
                'email'         =>  $socialUserInfo->email,
                'avatar'        =>  $socialUserInfo->avatar
            ];

            return view( 'data.social-callback', [ 'result' => $socialProfileData ] );

            // return $socialProfileData;

        } catch ( Exception $e ) {

            throw new Exception( "failed to authenticate with $provider" );

        }

    }

}
