<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Favourite extends User
{
    use \App\Traits\ApiCaller;
    use \App\Traits\Utility;

    /**
    * Display a listing of the favourites.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(...$vars)
    {
        $user_id       =   array_get($vars,0);
        $model         =   array_get($vars,1);
        if( !empty( $user_id ) ){
            /**
            *  List all user favourites
            */
            if( !empty( $model ) ){
                $this->api_endpoint     =   'user/'.$user_id.'/favourite/'.$model;
            }
            /**
            *  List all favourites of a model
            */
            else{
                $this->api_endpoint     =   'user/'.$user_id.'/favourite';
            }

            return Parent::index();
        }
        else{
            return response()->json( 'Please login', 400 );
        }
    }

    /**
    * Store a newly created favourite in storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request, ...$vars)
    {

        $user       =   array_get($vars,'0',false);
        $model      =   array_get($vars,'1',false);
        $model_id   =   array_get($vars,'2',false);
        if($model){
            // Favourite a model
            if($model_id){
                $this->api_endpoint     =   str_replace_array('#',[$user,$model,$model_id],array_get($this->config,'api.endpoint.favourite.model'));
            }
            // Favourite a search
            else{
                $this->api_endpoint     =   str_replace_array('#',[$user,$model],array_get($this->config,'api.endpoint.favourite.search'));
                $this->payload          =   $request->all();

                /**
                *  Addding search url
                */
                $this->payload['url']     =   $this->property_search_url_encode($this->payload);

            }
            // dd($this->api_endpoint);
            return Parent::store($request);
        }
    }


    /**
    * Remove the specified favourite from storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function destroy(Request $request, ...$vars)
    {

        $user       =   array_get($vars,'0',false);
        $model      =   array_get($vars,'1',false);
        $model_id   =   array_get($vars,'2',false);

        $this->api_endpoint     =   str_replace_array('#',[$user,$model,$model_id],array_get($this->config,'api.endpoint.favourite.delete'));

        return Parent::destroy($request);
    }

}
