<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class Utility extends BaseController
{

    protected   $aws_services,$client,$env_url,$remote_url,$config_bucket,$files, $reduce;

    public function __construct( ){
        $this->aws_services     =   new AwsServices;
        //verify only for local
        $this->client           =   new \GuzzleHttp\Client(['verify' => false]);
        $this->env_url          =   env('APP_URL');
        $this->remote_url       =   env('APP_ENV') === 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
        $this->config_bucket    =   env('APP_ENV') === 'production' ? 'zp-' .env('COUNTRY') . '-configs' : 'zp-earth';
        $this->files            =   [
                                        'brokerage-areas','brokerage-brokers','locations','types','property-completion-status',
                                        'project-global-developers','project-global-completion-status','project-global-locations',
                                        'new-internal-links','bedroom-internal-links','keywords'
                                    ];
        $this->reduce           =   false;
    }

    public function download_configs(...$files){
        /**
        * 1. Get Objects from S3
        * 2. Download fetched object from S3
        */
        try{
            #   1
            $files      =   count($files) == count($files, COUNT_RECURSIVE) ? $files : array_collapse($files);
            $files      =   in_array('all', $files) ? $this->files : $files;
            if( !empty( $files ) ){
                foreach($files as  $file) {
                    $response   =  $this->aws_services->get_s3_object( $this->config_bucket, $file, 'aws:kms' );
                    if( $response->getStatusCode() == 200 ){
                        #   2
                        \Storage::disk( 'search' )->put( $file . '.php', $response->getData(1) );
                    }
                    else{
                        return response()->json( [ 'message' => $response->getData(1)[ 'error' ] ], $response->getStatusCode() );
                    }
                }
                return response()->json( [ 'message' => 'configs downloaded' ], $response->getStatusCode() );
            }
        }
        catch( \Exception $e ){
            return response()->json( [ 'message' => $e->getMessage() ], 500 );
        }
    }


    public function build_search_config(){
        /**
        * 1. Get search file content
        * 2. Replace current data with new one like: property types, locations, services, projects
        */

        #1
        $current_search_data  =   include( storage_path( 'search/search.php' ) );

        #2
        #Getting property types
        $types_data           =   include( storage_path( 'search/types.php' ) );
        $types_data           =   $types_data[ 'residential_commercial' ];
        foreach($types_data as $res_comm_key => $res_comm_data){
            $res_comm_key   = strtolower($res_comm_key);
            foreach($res_comm_data['rent_buy'] as $rent_buy_key => $rent_buy_data){

                $rent_buy_key     = strtolower(str_replace(" ", "_", $rent_buy_key));
                $current_search_data['property']['types'][$res_comm_key][$rent_buy_key]   =   $rent_buy_data['types'];
            }
        }
        #Deleting local types file
        \Storage::disk('search')->delete('types.php');
        #Getting locations
        $locations_data  =   include( storage_path( 'search/locations.php' ) );
        foreach($locations_data as $location => $data){
            $lang_key    =   str_after($location, 'location');
            $countries   =   $locations_data[$location]['countries'];
            foreach($countries as $country_key => $country_values){
                $cities_data = array_get($country_values,'cities');
                $current_search_data['property']['cities'.$lang_key]   =   array_keys($cities_data);
                $locations = [];
                foreach($cities_data as $city_key => $city_value){
                    $locations[]   =   $city_key;
                    if(!empty($city_value['areas'])){
                        foreach($city_value['areas'] as $suburb_key => $suburb_value){
                            $locations[]            =   $suburb_key.' - '.$city_key;
                            if(!empty($suburb_value['buildings'])){
                                foreach($suburb_value['buildings'] as $building_value){
                                    $locations[]            =   $building_value.' - '.$suburb_key.' - '.$city_key;
                                }
                            }
                        }
                    }
                }
            }
            $current_search_data['property']['locations'.$lang_key]   =   array_unique($locations);
        }
        #Deleting local locations file
        \Storage::disk('search')->delete('locations.php');

        #Getting property completion statuses
        $property_completion_status  =   include( storage_path( 'search/property-completion-status.php' ) );
        $property_completion_status  =   $property_completion_status[ 'property']['completion_status' ];
        $current_search_data['property']['completion_status'] = $property_completion_status;
        #Deleting local property-completion-status file
        \Storage::disk('search')->delete('property-completion-status.php');

        #Getting brokerages
        $brokerages_data  =   include( storage_path( 'search/brokerage-brokers.php' ) );
        $brokerages_data  =   $brokerages_data[ 'brokerage' ];
        $list = [];
        $brokers = [];
        foreach($brokerages_data as $brokerages_key => $brokerages_value){
            $list[$brokerages_value['id']] = $brokerages_value['name'];
            foreach($brokerages_value['brokers'] as $broker_data){
                $brokers[$brokerages_value['id']][$broker_data['id']] = $broker_data['name'];
            }
        }
        $current_search_data['brokerage']['list'] = $list;
        $current_search_data['broker'] = $brokers;
        #Deleting local brokerage-brokers file
        \Storage::disk('search')->delete('brokerage-brokers.php');

        #Getting brokerages areas
        $brokerages_areas  =   include( storage_path( 'search/brokerage-areas.php' ) );
        $brokerages_areas  =   $brokerages_areas[ 'total_areas' ];
        $final_areas  = [];
        foreach($brokerages_areas as $areas){
            $final_areas[] = $areas;
        }
        $current_search_data['brokerage']['areas'] = array_values(array_unique($final_areas));

        #Deleting local brokerage-areas file
        \Storage::disk('search')->delete('brokerage-areas.php');

        #Getting project developers
        $project_developers  =   include( storage_path( 'search/project-global-developers.php' ) );
        $project_developers  =   $project_developers[ 'project'];
        $current_search_data['project']['developers']   =   $project_developers;
        #Deleting local project-global-developers file
        \Storage::disk('search')->delete('project-global-developers.php');

        #Getting project completion statuses
        $project_completion_status  =   include( storage_path( 'search/project-global-completion-status.php' ) );
        $project_completion_status  =   $project_completion_status[ 'project'];
        $current_search_data['project']['completion_status'] = $project_completion_status;
        #Deleting local project-global-completion-status file
        \Storage::disk('search')->delete('project-global-completion-status.php');

        #Getting project locations
        $all_project_locations  =   include( storage_path( 'search/project-global-locations.php' ) );
        foreach($all_project_locations as $project_location => $data){
            $project_countries  =   $all_project_locations[$project_location]['countries'];
            foreach($project_countries as $country_key => $country_values){
                $country    =   strtolower($country_key);
                $cities_data    =   array_get($country_values,'cities');
                $project_locations[$country]['locations'][]    =   $country_key;
                foreach($cities_data as $city_key => $city_value){
                    $project_locations[$country]['locations'][]    =   $city_key.' - '.$country_key;
                    if(!empty($city_value['areas'])){
                        foreach($city_value['areas'] as $suburb_key => $suburb_value){
                            $project_locations[$country]['locations'][]    =   $suburb_value.' - '.$city_key.' - '.$country_key;
                        }
                    }
                }
            }
            $current_search_data['project']['locations']   =   $project_locations;
        }
        #Deleting local project-global-locations file
        \Storage::disk('search')->delete('project-global-locations.php');

        #Writing search file
        \Storage::disk( 'search' )->put( 'search.php', '<?php return '.var_export( $current_search_data, 1 ).";" );

        return response()->json( [ 'message' => 'Search config updated' ], 200 );
    }


    public function populate_search_config(){

        /**
        * Function to be triggered from lambda to keep search files updated
        * 1.- Download config files(location, property_type, internal_links, etc.)
        * 2.- Build search file with data to be use in JsonData functions
        * 3.- Call JsonData functions to populate search Web file
        */

        #1.-
        $download =    $this->client->getAsync( $this->env_url . 'utility/download/all')->then(
            function( $response ){
                if($response->getStatusCode() == 200){
                    $download_rsp    =   json_decode( $response->getBody()->getContents() );

                    #2.-
                    $build_search =    $this->client->getAsync( $this->env_url . 'utility/search-file')->then(
                        function( $response ){
                            if($response->getStatusCode() == 200){
                                #3.-
                                $json_promises = [
                                    'property'      => $this->client->getAsync( $this->env_url .'api/'.env('API_VERSION').'/data/property/search'),
                                    'brokerage'     => $this->client->getAsync( $this->env_url .'api/'.env('API_VERSION').'/data/brokerage/search'),
                                    'service'       => $this->client->getAsync( $this->env_url .'api/'.env('API_VERSION').'/data/service/search'),
                                    'project'       => $this->client->getAsync( $this->env_url .'api/'.env('API_VERSION').'/data/project/search')
                                ];
                                // Wait for the requests to complete, even if some of them fail
                                $data_results = \GuzzleHttp\Promise\settle($json_promises)->wait();

                                $langs      =   config('languages');
                                $lang_keys  =   array_keys ($langs);
                                foreach($lang_keys as $key => $lang){
                                    $lang_keys[$key] = '_'.$lang;
                                }
                                foreach($langs as $lang => $desc ){
                                    $current_search_data  =   include( resource_path( 'lang/'.$lang.'/search.php' ) );
                                    foreach($data_results as $key => $promise_data){
                                        $json_search_data   =   json_decode( $promise_data['value']->getBody(), 1);
                                        $json_search_data   =   $json_search_data['json']['search'];

                                        foreach($json_search_data as $key_listings => $listings){
                                            if($lang !== 'en' && ends_with($key_listings, '_'.$lang)){
                                                    $key_listings = str_replace('_'.$lang, '',$key_listings);
                                                    $current_search_data[$key][$key_listings]  = $listings;
                                            }
                                            else{
                                                if(!str_contains($key_listings, $lang_keys)){
                                                    $current_search_data[$key][$key_listings]  = $listings;
                                                }
                                            }
                                        }
                                    }

                                    \Storage::disk( 'lang' )->put( $lang.'/search.php', '<?php return '.var_export( $current_search_data, 1 ).";" );
                                }
                            }
                        }, function ($exception) {
                            return $exception->getMessage();
                        }
                    );
                    $build_response  =   $build_search->wait();
                }
                return $download_rsp;
            }, function ($exception) {
                return $exception->getMessage();
            }
        );
        $final_response  =   $download->wait();
        return response()->json( $final_response, 200 );
    }

    /*
    ************************************
    *  L A N G    DYNAMIC DATA
    ************************************
    */
    public function populate_lang(){
        /**
        * 1. Download languages file
        * 2. Write data and translations files
        * 3. Delete local languages file
        */
        #1
        $this->download_configs(['languages']);
        #2
        $languages  =   include( storage_path( 'search/languages.php' ) );
        foreach($languages as $lang => $data){
            $data_dotted = array_dot($data);
            $lang_content = [];
            $plain_content = [];
            foreach($data_dotted as $key => $value){
                $key = snake_case_custom($key);
                array_set($lang_content, $key, $value);
            }
            \Storage::disk( 'lang' )->put( $lang.'/data.php', '<?php return '.var_export( $lang_content, 1 ).";" );

        }
        #3
        \Storage::disk('search')->delete('languages.php');
        return response()->json(['message' => 'Lang for dynamic data downloaded'], 200);
    }

    /*
    ************************************
    *  S I T E M A P
    ************************************
    */
    public function get_main_sitemap_feed($lng = 'en'){
        # 1. Return sitemap main file
        return response( file_get_contents(  public_path(    'sitemap/'.$lng.'/sitemap.xml'   )   ), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    public function get_sitemap_feed( $lng = 'en', $feed = ''){
        # 1. Return sitemap $feed file
        $bucket_url = 'https://zp-uae-sitemap.s3.ap-south-1.amazonaws.com/'.$lng.'/';
        return response( file_get_contents($bucket_url . $feed), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    public function get_main_sitemap_feed_reduce($lng = 'en'){
        # 1. Return sitemap main file
        return response( file_get_contents(  public_path(    'sitemap/'.$lng.'/reduce/sitemap.xml'   )   ), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    public function get_sitemap_feed_reduce( $lng = 'en', $feed = ''){
        # 1. Return sitemap $feed file
        $bucket_url = 'https://zp-uae-sitemap.s3.ap-south-1.amazonaws.com/'.$lng.'/reduce/';
        return response( file_get_contents($bucket_url . $feed), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    public function sitemap_feeds(){
        #Download internal-links file
        $this->download_configs(['internal-links']);
        #Empty S3 bucket
        $bucket_name   =  'zp-' . env( 'COUNTRY' ) .'-sitemap';
        $this->aws_services->delete_all_s3_object_from_bucket($bucket_name);
        #Generating sitemap per languages
        $location_data  =   include( storage_path( 'search/internal-links.php' ) );
        $location_data  =   $location_data[ 'residential_commercial' ];
        $locales        =   config('languages');
        foreach($locales as $locale => $desc){
            if(!\File::exists(public_path().'/sitemap/'.$locale)) {
                \File::makeDirectory(public_path().'/sitemap/'.$locale, $mode = 0777, true, true);
            }
            $file_maps      =   [];
            $files          =   [];
            foreach($location_data as $subtype_key => $rent_buy){
                $residential_commercial     =   $subtype_key;
                foreach( $rent_buy['rent_buy'] as $rent_buy_key => $locations){
                    $rent_buy_key       = preg_replace( array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $rent_buy_key);
                    $rent_buy_segment   =   ( $rent_buy_key == 'sale' ) ? 'buy' : $rent_buy_key;
                    $base_url           =   $this->env_url . $locale . '/' . $rent_buy_segment . '/' . $residential_commercial . '/';
                    $file_names         =   $this->sitemap_xml_file($base_url, $rent_buy_key, $residential_commercial, $locations,$locale);
                    $files[]            =   $file_names['no_type'];
                    if(isset($file_names['type'])){
                        $file_map           =   $residential_commercial . '-property-type-for-' . $rent_buy_key;
                        $file_maps[]        =   $file_map;
                        $this->sitemap_xml_map( $file_names, $file_map,$locale );
                    }
                    #Uploading sitemap files to S3 and deleting local files
                    $this->set_sitemap_feeds_in_s3($locale,$bucket_name);
                }
            }
            $files          =   array_collapse($files);
            $general_url    =   $this->env_url . $locale . '/';

            $this->sitemap_general_file( $general_url,$locale );
            #Uploading general file to S3 and deleting local file
            $this->set_sitemap_feeds_in_s3($locale,$bucket_name);
            $this->sitemap_general_map( $files, $file_maps, 'sitemap', $locale );
        }
        \Storage::disk('search')->delete('internal-links.php');
        return response()->json( [ 'message' => 'Sitemap updated' ], 200 );
    }

    private function sitemap_general_map( $files, $file_maps, $file_general_map, $locale ){
        $xml_string =   '<?xml version="1.0" encoding="utf-8"?>';
        $xml_string .=  '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $path_section = $this->reduce ? '/sitemap/reduce/' : '/sitemap/';
        foreach( $files as $file_name ){

            $name       =   $this->env_url . $locale . $path_section . $file_name;
            $xml_string .=  '<sitemap>';
            $xml_string .=  '<loc>' . $name . '</loc>';
            $xml_string .=  '</sitemap>';
        }
        foreach( $file_maps as $file_map_name ){
            $map_name    =   $this->env_url . $locale . $path_section . $file_map_name;
            $xml_string .=  '<sitemap>';
            $xml_string .=  '<loc>' . $map_name . '</loc>';
            $xml_string .=  '</sitemap>';
        }
        $xml_string .=  '<sitemap>';
        $xml_string .=  '<loc>' . $this->env_url . $locale . $path_section .'general-map</loc>';
        $xml_string .=  '</sitemap>';
        $xml_string .= '</sitemapindex>';
        $dom    =   new \DOMDocument;
        $dom->preserveWhiteSpace = true;
        $dom->formatOutput = true;
        $dom->loadXML( $xml_string );
        $path = ($this->reduce) ? 'sitemap/'.$locale.'/reduce/' : 'sitemap/'.$locale.'/';
        $dom->save($path.'sitemap.xml');
        return response()->json( [ 'message' => 'Xml feed saved successfully.' ], 200 );
    }

    private function sitemap_general_file( $general_url, $locale ){
        $xml_string =   '<?xml version="1.0" encoding="utf-8"?>';
        $xml_string .=  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . $general_url . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . 'https://blog.zoomproperty.com/' . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . 'https://agents.zoomproperty.com/' . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . $general_url. 'budget-search' . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . $general_url. 'agents-search' . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . $general_url. 'projects/uae' . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .=  '<url>';
        $xml_string .=  '<loc>' . $general_url. 'services' . '</loc>';
        $xml_string .=  '</url>';
        $xml_string .= '</urlset>';
        $dom    =   new \DOMDocument;
        $dom->preserveWhiteSpace = true;
        $dom->formatOutput = true;
        $dom->loadXML( $xml_string );
        $path = ($this->reduce) ? 'sitemap/'.$locale.'/reduce/' : 'sitemap/'.$locale.'/';
        $dom->save($path.'general-map.xml');
        return response()->json( [ 'message' => 'Xml feed saved successfully.' ], 200 );

    }

    private function sitemap_xml_map( $file_names, $file, $locale ){
        $xml_string =   '<?xml version="1.0" encoding="utf-8"?>';
        $xml_string .=  '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $path_section = $this->reduce ? '/sitemap/reduce/' : '/sitemap/';
        foreach( $file_names['type'] as $file_name ){
            $name       =   $this->env_url . $locale . $path_section . $file_name;
            $xml_string .=  '<sitemap>';
            $xml_string .=  '<loc>' . $name . '</loc>';
            $xml_string .=  '</sitemap>';
        }
        $xml_string .= '</sitemapindex>';
        $dom    =   new \DOMDocument;
        $dom->preserveWhiteSpace = true;
        $dom->formatOutput = true;
        $dom->loadXML( $xml_string );
        $path = ($this->reduce) ? 'sitemap/'.$locale.'/reduce/' : 'sitemap/'.$locale.'/';
        $dom->save($path .$file. '.xml' );
        return response()->json( [ 'message' => 'Xml feed saved successfully.' ], 200 );
    }

    private function sitemap_xml_file( $base_url, $rent_buy, $residential_commercial, $locations, $locale ){

        $response   =   [];
        $urls       =   $this->sitemap_xml_data($base_url, $rent_buy, $residential_commercial, $locations);
        foreach($locations['types'] as $type_key => $type_value){

            $file_name  =   $residential_commercial . '-' . $type_key . '-for-' . $rent_buy;
            $urls_type  = array_where($urls, function ($value, $key) use($type_key) {
                return str_contains($value, $type_key);
            });
            $sized_urls =   array_chunk( $urls_type, 1000 );

            foreach( $sized_urls as $key => $thousand_urls ){

                $xml_string =   '<?xml version="1.0" encoding="utf-8"?>';
                $xml_string .=  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

                if( $key == 0 ){

                    $replaced = str_replace_last( $residential_commercial . '/', '', $base_url );
                    $xml_string .=  '<url>';
                    $xml_string .=  '<loc>' . $replaced . $file_name . '</loc>';
                    $xml_string .=  '</url>';

                }

                foreach( $thousand_urls as $url ){

                    $url    =   str_contains( $url , '&' ) ? str_replace( '&', 'and', $url ) : $url;
                    $xml_string .=  '<url>';
                    $xml_string .=  '<loc>' . $url . '</loc>';
                    $xml_string .=  '</url>';

                }

                $xml_string .= '</urlset>';
                // dd($xml_string);
                $dom    =   new \DOMDocument;
                $dom->preserveWhiteSpace = true;
                $dom->formatOutput = true;
                $dom->loadXML( $xml_string );

                $number_page    =   $key + 1;
                $path = ($this->reduce) ? 'sitemap/'.$locale.'/reduce/' : 'sitemap/'.$locale.'/';
                $dom->save($path. $file_name . '-' . $number_page . '.xml');
                $response['type'][]     =   $file_name . '-' . $number_page;

            }
        }

        $file          =   $residential_commercial . '-properties-for-' . $rent_buy;
        $urls_no_type  = array_where($urls, function ($value, $key) {
            return str_contains($value, 'properties-for');
        });
        $sized_urls =   array_chunk( $urls_no_type, 1000 );

        foreach( $sized_urls as $key => $thousand_urls ){

            $xml_string =   '<?xml version="1.0" encoding="utf-8"?>';
            $xml_string .=  '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

            if( $key == 0 ){

                $replaced = str_replace_last( $residential_commercial . '/', '', $base_url );
                $xml_string .=  '<url>';
                $xml_string .=  '<loc>' . $replaced . $file . '</loc>';
                $xml_string .=  '</url>';

            }

            foreach( $thousand_urls as $url ){

                $url    =   str_contains( $url , '&' ) ? str_replace( '&', 'and', $url ) : $url;

                $xml_string .=  '<url>';
                $xml_string .=  '<loc>' . $url . '</loc>';
                $xml_string .=  '</url>';

            }

            $xml_string .= '</urlset>';

            $dom    =   new \DOMDocument;
            $dom->preserveWhiteSpace = true;
            $dom->formatOutput = true;
            $dom->loadXML( $xml_string );

            $number_page    =   $key + 1;
            $path = ($this->reduce) ? 'sitemap/'.$locale.'/reduce/' : 'sitemap/'.$locale.'/';
            $dom->save($path .$file . '-' . $number_page . '.xml');
            $response['no_type'][]     =   $file . '-' . $number_page;

        }
        return $response;

    }

    private function sitemap_xml_data( $base_url, $rent_buy, $residential_commercial, $locations ){

        $urls       =   [];
        foreach( $locations[ 'cities' ] as $city_name => $cities){
            $city       =   str_replace( " ", "-", $city_name );
            $urls[]     =   strtolower($base_url . 'properties-for-' . $rent_buy . '-in-' . $city . '-city');
            foreach( $cities[ 'areas' ] as $area_name => $areas ){

                if($this->reduce && $areas['count'] < 100)
                    continue;
                $area       =   str_replace( " ", "-", $area_name );
                $area       =   str_contains( $area , '(' ) ? substr( $area, 0, strrpos( $area, '-' ) ) : $area;
                $urls[]     =   strtolower($base_url . $city .'/' . 'properties-for-' . $rent_buy . '-in-' . $area . '-area');
                foreach( $areas[ 'buildings' ] as $building_key => $buildings){
                    if($this->reduce && $buildings['count'] < 100)
                        continue;
                    $building   =   str_replace( " ", "-", $building_key );
                    $urls[]     =   strtolower($base_url . $city .'/' . $area . '/'. 'properties-for-' . $rent_buy . '-in-' . $building . '-building');
                    foreach($buildings['types'] as $property_type_key => $property_type_values){
                        $property_type_key  =   preg_replace(array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $property_type_key);
                        $urls[]     =   strtolower($base_url . $city .'/' . $area . '/'. $property_type_key . '-for-' . $rent_buy . '-in-' . $building . '-building');
                    }
                }
                foreach($areas['types'] as $property_type_key => $property_type_values){
                    $property_type_key  =   preg_replace(array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $property_type_key);
                    $urls[]     =   strtolower($base_url . $city .'/' . $property_type_key. '-for-' . $rent_buy . '-in-' . $area . '-area');
                }
            }
            foreach($cities['types'] as $property_type_key => $property_type_values){
                $property_type_key  =   preg_replace(array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $property_type_key);
                $urls[]     =   strtolower($base_url . $property_type_key. '-for-' . $rent_buy . '-in-' . $city . '-city');
            }
        }
        if( $residential_commercial == 'residential' ){
            foreach($locations['types'] as $property_type_key => $property_type_values){
                $property_type_key  =   preg_replace(array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $property_type_key);
                $urls[]     =   $base_url . 'studio-bedroom-' . $property_type_key . '-for-' . $rent_buy;
                for( $i = 1; $i <= 10; $i ++ ){
                    $urls[]     =   $base_url . $i . '-bedroom-' . $property_type_key . '-for-' . $rent_buy;
                }
            }
            $urls[]     =   $base_url . 'studio-bedroom-for-' . $rent_buy;
            for( $i = 1; $i <= 10; $i ++ ){
                $urls[]     =   $base_url . $i . '-bedroom-for-' . $rent_buy;
            }
        }

        $urls[]     =   strtolower($base_url . 'properties-for-' . $rent_buy);

        foreach($locations['types'] as $property_type_key => $property_type_values){
            $property_type_key  =   preg_replace(array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $property_type_key);
            $urls[]     =   strtolower($base_url . $property_type_key. '-for-' . $rent_buy );
        }

        return $urls;
    }

    private function set_sitemap_feeds_in_s3($locale,$bucket_name){
        //REDUCE
        if($this->reduce){
            $files = glob( public_path().'/sitemap/'.$locale.'/reduce/*.{xml}', GLOB_BRACE );
            if( !empty( $files ) ){
                foreach($files as $file) {
                    # 1. Put Objects in S3
                    try{
                        $slice      = str_replace('.xml', '', str_after($file, $locale.'/reduce/'));
                        $this->aws_services->put_s3_object($bucket_name, $locale.'/reduce/'.$slice, $file);
                        \Storage::disk('search')->delete($file.'xml');
                    }
                    catch( \Exception $e ){
                        return response()->json( [ 'message' => $e->getMessage() ], 500 );
                    }
                }
            }

            \File::delete($files);
        }

        $files = glob( public_path().'/sitemap/'.$locale.'/*.{xml}', GLOB_BRACE );
        if( !empty( $files ) ){
            foreach($files as $file) {
                # 1. Put Objects in S3
                try{
                    $slice      = str_replace('.xml', '', str_after($file, $locale.'/'));
                    $this->aws_services->put_s3_object($bucket_name, $locale.'/'.$slice, $file);
                    \Storage::disk('search')->delete($file.'xml');
                }
                catch( \Exception $e ){
                    return response()->json( [ 'message' => $e->getMessage() ], 500 );
                }
            }
        }
        \File::delete($files);


    }

    //QUESTION Why here and not in Lead Controller?
    public function book_viewing_lead( Request $request ){

        $payload  =   $request->all();

        $email   =  new Email;
        $email->book_viewing_email( $payload );

    }
    //QUESTION Why here and not in Lead Controller?
    public function ideal_property_lead( Request $request ){

        $payload  =   $request->all();

        if( in_array(null, $payload) === false )
        {
            if( filter_var( $payload['email'], FILTER_VALIDATE_EMAIL) )
            {
                $email   =  new Email;
                $email->ideal_property_email( $payload );
                return response()->json( array( 'success' => 'Your request is sent. Someone will get in touch with you asap.' ),200 );
            }
            else
            {
                return response()->json( array( 'error' => 'Please enter a valid email address.' ),200 );
            }
        }
        else{
            return response()->json( array( 'error' => 'Please fill all fields.' ),200 );
        }

    }

    public function api_call($api_route, $method, $payload, $api ){

        try{
            $client         =   new \GuzzleHttp\Client();
            if( $api == 'earth' ){
                $payload        =   ( $method == 'GET' ) ? [ 'query' => [ 'query' => json_encode($payload) ] ] : [ 'json' => $payload ];
                // if($api !== 'earth') dd( $method, $api_route, $payload );
                // if($api !== 'earth') {
                //     echo "$method <br>";
                //     echo "$api_route <br>";
                //     echo json_encode( $payload, JSON_PRETTY_PRINT ); die();
                // }
                return $client->request( $method, $api_route, $payload );
            }
        }
        catch( \GuzzleHttp\Exception\RequestException $e ){
            if( $e->hasResponse() ) {
                return response()->json( json_decode( $e->getResponse()->getBody()->getContents(), 1 ), $e->getResponse()->getStatusCode() );
            }
            else{
                return response()->json( array( 'message' => 'No response' ), 400 );
            }
        }
    }

    public function urls(){
        $this->download_configs(['internal-links']);
        $location_data  =   include( storage_path( 'search/internal-links.php' ) );
        $location_data  =   $location_data[ 'residential_commercial' ];

        $urls           =   [];
        $languages      =   config( 'languages' );

        foreach($languages as $lang => $data){
            foreach($location_data as $subtype_key => $rent_buy){
                $residential_commercial     =   $subtype_key;
                foreach( $rent_buy['rent_buy'] as $rent_buy_key => $locations){
                    $rent_buy_key       =   preg_replace( array( "/\s+/", "/[\/]+/" ), array( "-", "-" ), $rent_buy_key);
                    $rent_buy_segment   =   ( $rent_buy_key == 'sale' ) ? 'buy' : $rent_buy_key;
                    $base_url           =   $lang . '/' . $rent_buy_segment . '/' . $residential_commercial . '/';
                    $urls[]             =   $this->sitemap_xml_data($base_url, $rent_buy_key, $residential_commercial, $locations);
                }
            }
            $urls   =   array_collapse($urls);

            \Storage::disk( 'lang' )->put( $lang.'/urls.php', '<?php return '.var_export( $urls, 1 ).";" );
        }
        \Storage::disk('search')->delete('internal-links.php');
        return response()->json( [ 'message' => 'URLs generated.' ], 200 );

    }

}
