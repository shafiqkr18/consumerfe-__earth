<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Analytic extends BaseController
{
    const leaf = 'analytic';
    use \App\Traits\ApiCaller;
    use \App\Traits\JsonValidator;

    protected $api_endpoint,$config,$api_gateway;

    public function __construct( Request $request ){
        $this->payload      =   !empty( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : '';
        $this->config       =   config('models.'.$this::leaf);
        $this->api_gateway  =   env('APP_ENV') == 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function guide_me_listings(...$vars){

        $model = array_get($vars,0);
        $this->api_endpoint =   str_replace('#', $model,array_get($this->config,'api.endpoint.g.listings') );
        #Caching
        $folder = 'cache/property/guide_me';

        $rent_buy_segment = $this->payload['rent_buy'];
        $meters = $this->payload['meters'];
        $area = $this->payload['area'];

        $type = ( array_has( $this->payload, 'type' ) ) ? $this->payload['type'][0] : 'properties';

        $rent_buy = implode('-', $rent_buy_segment);

        array_forget( $this->payload, 'area' );

        if( array_has( $this->payload, 'price' ) || array_has( $this->payload, 'bedroom' ) ) {

            $response = $this->set_method('GET')
                ->build_payload($this->payload)
                ->call_api($this->api_gateway.$this->api_endpoint);

            return $response;

        } else {

            if(\Storage::exists($folder.'/en/'.$type.'-for-'.strtolower($rent_buy).'-in-'.strtolower(str_replace(' ','-',$area)).'-'.$meters.'.php')){
                $properties = include( storage_path( $folder.'/en/'.$type.'-for-'.strtolower($rent_buy).'-in-'.strtolower(str_replace(' ','-',$area)).'-'.$meters.'.php' ) );
                return response()->json($properties);

            } else {

                $response = $this->set_method('GET')
                    ->build_payload($this->payload)
                    ->call_api($this->api_gateway.$this->api_endpoint);

                if( $response->getStatusCode() === 200 ) {

                    $properties = json_decode((string)$response->getBody(),1);

                    $results    =   is_array($properties) ? $properties : '';
                    $data       =   var_export_min($results, true);

                    \Storage::put($folder.'/en/'.$type.'-for-'.strtolower($rent_buy).'-in-'.strtolower(str_replace(' ','-',$area)).'-'.$meters.'.php','<?php return '.$data.'; ?>');
                    return $response;

                } else {

                    return $response;

                }

            }

        }
        #End Caching

    }

    /**
    * Display a analytics and stats for guide me.
    *
    * @return \Illuminate\Http\Response
    */
    public function guide_me_stats(...$vars){
        $model = array_get($vars,0);
        $this->api_endpoint =   str_replace('#', $model,array_get($this->config,'api.endpoint.g.stats') );
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

}
