<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Property extends Controller
{
    use \App\Traits\Utility;
    const leaf = 'property';

    public function index(){

        $randomizer     =   array_random([1,2,3,4,5]);
        $path           =   'cache/'.$this::leaf;

        // Request is not a web response
        if(is_null($this->request)){
            $payload    = $this->payload;
            $properties = Parent::index();
            if($properties->getStatusCode() == 200){
                $properties = json_decode((string)$properties->getBody(),1);
                $res_comm   =   strtolower(array_get($payload,'residential_commercial.0', 'Residential'));
                $rent_buy_segment = array_get($payload,'rent_buy.0', 'Sale') == 'Sale' ? 'buy' : 'rent';
                $rent_buy   =   array_get($payload,'rent_buy.0', 'Sale') == 'Sale' ? 'sale' : 'rent';
                $folder     =   $path.(!is_null($this->request) && !empty($this->request->segments()) && starts_with(array_last($this->request->segments()),'featured-') ? '/featured' : '');
                $randomizer_txt =   ($folder == 'featured') ? '' : '-'.$randomizer;
                if(\Storage::exists($folder.'/en/'.$rent_buy_segment. '/'.$res_comm.'-properties-for-'.$rent_buy.$randomizer_txt.'.php')){
                    $properties             =   include( storage_path( 'cache/'.$this::leaf.'/en/'.$rent_buy_segment. '/'.$res_comm.'-properties-for-'.$rent_buy.$randomizer_txt.'.php' ) );
                    $properties['total']    =   0;
                    return response()->json($properties);
                }
            }
            else {
                return $properties;
            }
            return response()->json($properties);
        }
        else{

            $request    =   $this->request;
            /**
            * 1.   Url doesn't have query params
            *      1.1.    File exists in storage
            *              1.1.1.  [Yes] Use storage data
            *
            * 2.   Make API Call
            * 3.   Store results in Cache
            */

            // 1.
            if(empty($request->query())){
                // 1.1.
                $folder = $path.(starts_with(array_last($this->request->segments() ),'featured-') ? '/featured' : '');
                $randomizer_txt =   (str_contains($folder,'featured')) ? '' : '-'.$randomizer;
                if(\Storage::exists($folder.'/'.$request->path().$randomizer_txt.'.php')){
                    $properties     =   include( storage_path( $folder.'/'.$request->path().$randomizer_txt.'.php' ) );
                    $properties     =   json_decode( json_encode($properties), 1 );

                    if( !empty( $properties['data'] ) ){
                        if(count($properties['data']) < 20 && ($randomizer !== 1 && !empty($randomizer_txt)) && \Storage::exists($folder.'/'.$request->path().'-1.php') ){
                            $properties     =   include( storage_path( $folder.'/'.$request->path().'-1.php' ) );
                            $properties     =   json_decode( json_encode($properties), 1 );
                        }
                        shuffle( $properties['data'] );
                    }
                    return response()->json($properties);
                }
            }
            // 2.
            $this->set_payload($this->create_payload($request));

            $properties     =   Parent::index();
            $properties     =   json_decode((string)$properties->getBody(),1);

            // 3.
            $data           =   var_export_min($properties, true);

            // Store in cache only if its a property search
            if(array_get($properties,'data',false)){
                $folder = $path.(!empty($this->request->segments()) && starts_with(array_last($this->request->segments()),'featured-') ? '/featured' : '');
                $randomizer_txt =   str_contains($folder,'featured') ? '' : '-'.$randomizer;
                \Storage::put($folder.'/'.$request->path().$randomizer_txt.'.php','<?php return '.$data.'; ?>');
            }
            else{
                //No results showing
                $files    = [];
                $segments = $request->segments();
                $res_comm = isset($segments[2]) ? $segments[2] : null;

                do {
                    $new_segments = $segments;
                    $new_path = implode('/', array_splice($new_segments, 0, -1) );

                    $files    = glob( storage_path().'/'.$path.'/'.$new_path.'/*.{php}', GLOB_BRACE );
                    array_pop($segments);
                } while (empty($files) && count($segments) >= 2);
                if(!empty($files)){
                    if(!is_null($res_comm)){
                        foreach($files as $file){
                            if(str_contains($file, $res_comm)){
                                $properties  =   include( $file );
                                break;
                            }
                        }
                        $properties =   isset($properties) ? $properties : include( $files[0] );
                    }
                    else
                        $properties =   include( $files[0] );

                    $properties     =   json_decode( json_encode($properties), 1 );
                    if( !empty( $properties['data'] ) )
                        shuffle( $properties['data'] );

                    $properties['total']   =   0;
                    return response()->json($properties);

                }
            }
            return response()->json($properties);
        }

    }

    public function featured(){

        $request    =   $this->request;
        $path       =   'cache/'.$this::leaf.'/featured';

        if(empty($request->query())){
            // 1.1.
            if(\Storage::exists($path.'/'.$request->path().'.php')){
                $featured     =   include( storage_path( $path.'/'.$request->path().'.php' ) );
                $featured     =   json_decode( json_encode($featured), 1 );
                if( !empty( $featured['data'] ) )
                    shuffle( $featured['data'] );
                return response()->json($featured);
            }
        }
        else{
            if((array_has($request->query(), 'page') && count($request->query()) > 1) || (!array_has($request->query(), 'page' ))){
                $response['total']  =   0;
                $response['data']   =   [];
                return response()->json($response);
            }
        }
        $this->set_payload($this->create_payload($request));
        array_set($this->payload,'settings.status',1);
        array_set($this->payload,'featured.status',true);
        array_set($this->payload,'size',25);

        $properties     =   Parent::index();
        $properties     =   json_decode((string)$properties->getBody(),1);
        $data           =   var_export_min($properties, true);
        // Store in cache only if its a property search
        if(array_get($properties,'data',false)){
            \Storage::put($path.'/'.$request->path().'.php','<?php return '.$data.'; ?>');
            return response()->json($properties);
        }
        else{
            $response['total']  =   0;
            $response['data']   =   [];
            return response()->json($response);
        }
    }

    public function create_payload($request){

        // 1.   Extract Buckets
        list($query,$url)   =   $this->extract_buckets( trim(str_replace('https://'.$request->getHost(),'',str_replace('http://'.$request->getHost(),'',$request->fullUrl() )),'/') );
        // 2.   Extract Query String
        list($query,$url)   =   $this->extract_querystring($query,$url);
        // 3.   Extract Other fragments
        $query              =   $this->extract_fragments($query,'search/'.$url);
        // 4.   Forcing price > 2010 for undisclosed
        if(!array_has($query, 'price.min')){
            array_set($query, 'price.min', 2010);
        }
        return $query;
    }

    public function extract_buckets($url){
        $url_parts          =   explode('/',$url);
        // Ignoring Language
        array_shift( $url_parts );

        if( count( $url_parts ) == 1 ){
            // Only Rent/Buy expressed
            $fragments_url      =   $url_parts[0];

            $buckets_url        =   preg_replace('/\?.*/', '', $url_parts);
        }
        else{
            // 1.   Fragments
            $fragments_url      =   array_last( $url_parts );

            // 2.   Buckets
            $buckets_url        =   array_splice( $url_parts, 0, -1 );
        }

        // Identify buckets
        $buckets_template   =   [ 'rent_buy', 'residential_commercial', 'city', 'area', 'building' ];
        // Assign buckets
        for( $i=0; $i<count($buckets_url); $i++ ){

            $query_field    =   $buckets_template[$i];
            $query_value    =   $buckets_url[$i];

            if( $query_field == 'rent_buy' ){
                if( $query_value === 'buy' )
                $query_value    =   'sale';
                array_set( $query, 'rent_buy', [title_case(str_replace( '-',' ', $query_value))] );

                if( str_contains( $fragments_url, 'residential' ) ){
                    array_set( $query, 'residential_commercial', ['Residential'] );
                }
                else if( str_contains( $fragments_url, 'commercial' ) ){
                    array_set( $query, 'residential_commercial', ['Commercial'] );
                }
            }

            if( $query_field == 'residential_commercial' ){
                array_set( $query, 'residential_commercial', [title_case($query_value)] );
            }

            if( $query_field == 'city' ){
                // array_set( $query, 'city', [title_case($query_value)] );
                $cityArr = explode( ' Or ', str_replace("-", " ", title_case( $query_value ) ) );
                array_set( $query, 'city', $cityArr );
            }

            if( $query_field == 'area' ){
                $suburbs    =   explode( "Or", title_case(str_replace("-", " ", $query_value)) );
                $inSuburb   =   [];
                foreach( $suburbs as $eachSuburb ){
                    if( array_get( config( 'areas_building_mapping.area' ), $eachSuburb, false ) ) {
                      $eachSuburb = array_get( config( 'areas_building_mapping.area' ), $eachSuburb);
                    }
                    else{
                        $eachSuburb     =   ucwords($eachSuburb);
                    }
                    $inSuburb[] =   trim( $eachSuburb );
                }
                array_set( $query, 'area', $inSuburb );
            }

            if( $query_field == 'building' ){
                $suburbs    =   explode( "Or", title_case(str_replace("-", " ", $query_value)) );
                $inSuburb   =   [];
                foreach( $suburbs as $eachSuburb ){
                    if( array_get( config( 'areas_building_mapping.building' ), $eachSuburb, false ) ) {
                      $eachSuburb = array_get( config( 'areas_building_mapping.building' ), $eachSuburb);
                    }
                    else{
                        $eachSuburb     =   ucwords($eachSuburb);
                    }
                    $inSuburb[] =   trim( $eachSuburb );
                }
                array_set( $query, 'building', $inSuburb );
            }

        }
        if( starts_with( $fragments_url, 'featured-' ) ){
            array_set( $query, 'featured.status', true );
        }
        return [$query, $fragments_url];
    }

    public function extract_querystring($query,$url){
        // Check if query string exists
        $remove_from_url    =   [];
        if(str_contains($url,'?')){
            $search     =   explode( "&", explode( "?", $url )[1] );
            foreach( $search as $eachSearch ){
                list( $key, $value )    =   explode( "=", $eachSearch );
                if( $key === 'page' ){
                    $query[ 'page' ]    =   (int)$value;
                }
                else if( $key === 'sort' ){
                    if(str_contains($value,'-')){
                        list( $field, $order )  =   explode( "-", $value );
                        $query[ 'sort' ][ $field ][ 'order' ]   =   $order;
                    }
                }
                else if( $key === 'consumer_email' ){
                    $query[ 'consumer_email' ]     =   urldecode($value);
                }
                else if( $key === 'agency_id' ){
                    // array_set($p)
                    $query['agent']['agency']['_id']     =   urldecode($value);
                }
                else if( $key === 'agency_email' ){
                    $query['agent']['agency']['contact']['email'][]     =   urldecode($value);
                }
                else if( $key === 'agent_id' ){
                    $query[ 'agent' ][ '_id' ]     =   urldecode($value);
                }
                else if( $key === 'agent_email' ){
                    $query[ 'agent' ][ 'contact' ][ 'email' ][]     =   urldecode($value);
                }
                else if( $key === 'furnishing' ){
                    $query[ 'furnished' ]     =   (int)$value;
                }
                else if( $key === 'status' ){
                    $v  =   str_replace('-', '_', $value);
                    $query[ 'completion_status' ]     =   $v;
                }
                else if( $key === 'keyword' ){
                    $query[ 'keyword' ]     =   urldecode($value);
                }
                else if( $key === 'location' ){
                    $v = base64_decode(urldecode(urldecode($value)));
                    $query[ 'location' ]     =   array_map('trim', explode(',', $v));
                }
                $remove_from_url[]  =   $key."=".$value;
            }
        }
        $url    =   str_replace('?'.implode('&',$remove_from_url),'',$url);
        return [$query,$url];
    }

    public function extract_fragments($query,$url){
        
        preg_match( "/price-greater-than-(.*?)-aed/", $url, $match );
        if( count( $match ) > 0 ){
            $query[ 'price' ][ 'min' ]  =   (int)$match[1];
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

        preg_match( "/price-less-than-(.*?)-aed/", $url, $match );
        if( count( $match ) > 0 ){
            $query[ 'price' ][ 'max' ]  =   (int)$match[1];
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

        preg_match( "/builtup-area-greater-than-(.*?)-sqft/", $url, $match );
        if( count( $match ) > 0 ){
            $query[ 'dimension' ][ 'builtup_area' ][ 'min' ]  =   (int)$match[1];
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }


        preg_match( "/builtup-area-less-than-(.*?)-sqft/", $url, $match );
        if( count( $match ) > 0 ){
            $query[ 'dimension' ][ 'builtup_area' ][ 'max' ]  =   (int)$match[1];
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }
        
        /**
        * Bedrooms
        */
        if( strpos( $url, "bedroom" ) ){
            $url        =   str_replace( "studio", "0", $url );
            preg_match( "/with-\d(.*)-bedroom/", $url, $match );
            if( count( $match ) > 0 ){
                $bath   =   str_replace( "with-", "", $match[0] );
            }
            else{
                preg_match( "/\d(.*)-bedroom/", $url, $match );
                $bath   =   str_replace( "search/", "", $match[0] );
            }
            $bath   =   str_replace( "-bedroom", "", $bath );
            if( strpos( $bath, "-to-" ) ){
                $bath   =   explode( "-to-", $bath );
                $query[ 'bedroom' ][ 'min' ]   =   (int)$bath[0];
                $query[ 'bedroom' ][ 'max' ]   =   (int)$bath[1];
            }
            else{
                $query[ 'bedroom' ][ 'min' ]    =   (int)$bath;
                $query[ 'bedroom' ][ 'max' ]    =   (int)$bath;
            }
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

        /**
        * Bathroom
        */
        if( strpos( $url, "bathroom" ) ){
            preg_match( "/with-\d(.*)-bathroom/", $url, $match );
            if( count( $match ) > 0 ){
                $bath   =   str_replace( "with-", "", $match[0] );
            }
            else{
                preg_match( "/search\/\d(.*)-bathroom/", $url, $match );
                $bath   =   str_replace( "search/", "", $match[0] );
            }
            $bath   =   str_replace( "-bathroom", "", $bath );
            if( strpos( $bath, "-to-" ) ){
                $bath   =   explode( "-to-", $bath );
                $query[ 'bathroom' ][ 'min' ]   =   (int)$bath[0];
                $query[ 'bathroom' ][ 'max' ]   =   (int)$bath[1];
            }
            else{
                $query[ 'bathroom' ][ 'min' ]    =   (int)$bath;
                $query[ 'bathroom' ][ 'max' ]    =   (int)$bath;
            }
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

        /**
        * Suburbs
        */
        if( strpos( $url, "-area" ) ){
            
            preg_match( "/-in-(.*)-area/", $url, $match );
            if( count( $match ) > 0 ){
                $suburbs     =   str_replace( "-in-", "", $match[0] );
            }
            else{
                preg_match( "/search\/(.*)-area/", $url, $match );
                $suburbs     =   str_replace( "search/", "", $match[0] );
            }
            $suburbs        =   str_replace_last( "-area", "", $suburbs );

            if( strpos( $suburbs, "-or-" ) ){
                $suburbs     =   explode( "-or-", $suburbs );
                foreach( $suburbs as $k => $v ){
                    $area = urldecode( trim(str_replace( "-", " ", $v )) );
                    if(!in_array($area, array_get($query,'area',[]))){
                        if( array_get( config( 'areas_building_mapping.area' ), $area, false ) ) {
                          $area = array_get( config( 'areas_building_mapping.area' ), $area);
                        }
                        else{
                            $area     =   ucwords($area);
                        }
                        $query['area'][]   =   $area;
                    }
                }

            }
            else{
                $area = urldecode( trim(str_replace( "-", " ", $suburbs )) );
                if(!in_array($area, array_get($query,'area',[]))){
                    if( array_get( config( 'areas_building_mapping.area' ), $area, false ) ) {
                      $area = array_get( config( 'areas_building_mapping.area' ), $area);
                    }
                    else{
                        $area     =   ucwords($area);
                    }
                    $query['area'][]   =   $area;
                }
            }
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

        /**
        * Buildings
        */
        if( strpos( $url, "-building" ) ){

            if( preg_match('/-in-(.*)-building/', $url, $match) == 1){

                if( count( $match ) > 0 ){
                    $buildings     =   str_replace( "-in-", "", $match[0] );
                }
                else{
                    preg_match( "/search\/(.*)-building/", $url, $match );
                    $buildings     =   str_replace( "search/", "", $match[0] );
                }

                $buildings        =   str_replace_last( "-building", "", $buildings );

                if( strpos( $buildings, "-or-" ) ){
                    $buildings     =   explode( "-or-", $buildings );
                    foreach( $buildings as $k => $v ){
                        $building = urldecode( trim(str_replace( "-", " ", $v ) ) );
                        if(!in_array($building,array_get($query,'building',[]))) {
                            if( array_get( config( 'areas_building_mapping.building' ), $building, false ) ) {
                              $building = array_get( config( 'areas_building_mapping.building' ), $building);
                            }
                            else{
                                $building = ucwords($building);
                            }
                            $query['building'][]   =   $building;
                        }
                    }
                }
                else{
                    $building = urldecode( trim(str_replace( "-", " ", $buildings )) );
                    if(!in_array($building,array_get($query,'building',[]))) {
                        if( array_get( config( 'areas_building_mapping.building' ), $building, false ) ) {
                          $building = array_get( config( 'areas_building_mapping.building' ), $building);
                        }
                        else{
                            $building = ucwords($building);
                        }
                        $query['building'][]   =   $building;
                    }
                }
                // Trimmed Url
                $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
            }

        }

        /**
        * City
        */
        if( strpos( $url, "-city" ) ){

            preg_match( "/-in-(.*)-city/", $url, $match );

            if( count( $match ) > 0 ){
                $cities     =   str_replace_first( "-in-", "", $match[0] );
            }
            else{
                preg_match( "/search\/(.*)-city/", $url, $match );
                $cities     =   str_replace( "search/", "", $match[0] );
            }
            $cities         =   str_replace_last( "-city", "", $cities );

            if( strpos( $cities, "-or-" ) ){
                $cities     =   explode( "-or-", $cities );
                foreach( $cities as $k => $v ){
                    $possible_ct                =   title_case( trim(str_replace( "-", " ", $v )) );
                    if(!in_array($possible_ct,array_get($query,'city',[]))){
                        $query['city'][]   =   ucwords($possible_ct);
                    }
                }
            }
            else{
                $possible_ct                =   title_case( trim(str_replace( "-", " ", $cities )) );
                if(!in_array($possible_ct,array_get($query,'city',[]))){
                    $query['city'][]   =   ucwords($possible_ct);
                }
            }
            // Trimmed Url
            $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

        /**
        * Rent or Sale
        */
        if( strpos( $url, "rent" ) || strpos( $url, "sale" ) ){
            if( strpos( $url, "for-" ) ){
                $start  =   "for-";
            }
            else{
                $start  =   "search\/";
            }
            
            if( strpos( $url, '-in-' ) ){
                $nearest_end['-in']         =   strpos( $url, '-in-' );
            }
            
            if( strpos( $url, "-with" ) ){
                $nearest_end['-with']       =   strpos( $url, "-with" );
            }
            
            if( isset( $nearest_end ) ){
                asort( $nearest_end, 1 );
                $end    =   array_keys( $nearest_end )[0];
                preg_match( "/".$start."(.*)".$end."/", $url, $match );
            }
            else{
                preg_match( "/".$start."(.*)/", $url, $match );
            }
            // dd( $match );
            $rent_buy       =   $match[1];
            // dd( $rent_buy );
            if( strpos( $rent_buy, "-or-" ) ){
                $rent_buy   =   explode( "-or-", $rent_buy );
                foreach( $rent_buy as $rentBuy ){
                    $rentbuy = title_case( $rentBuy );
                    if(in_array($rentbuy,array_get($query, 'rent_buy', []))){
                        $query[ 'rent_buy' ][] =   $rentbuy;
                    }
                }
            }
            else{
                $rentbuy = title_case( $rent_buy );
                if(in_array($rentbuy,array_get($query, 'rent_buy', []))){
                    $query[ 'rent_buy' ][] =   $rentbuy;
                }
            }
            $query[ 'rent_buy' ]            =   array_unique($query[ 'rent_buy' ]);
            // Trimmed Url
            $url                            =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_ireplace( $match[0], "", $url ) ) ), "-");
        }

        /**
         * Property Types
         */
        if( strpos($url, 'properties') == false ){
            preg_match( "/search\/(.*)/", $url, $match );
            if( count( $match ) > 0 ){

                $all_property_types =   array_flip(array_map('kebab_case_custom', $this->property_types()));
                $match[0]       =   str_replace( [ "search/", "-for" ], [ "", "" ], $match[0] );
                $matched_type   =   '';
                
                $property_types_patterns    =   array_map(function($value) { return '/'.kebab_case_custom($value).'/'; }, $all_property_types);
                $property_types_patterns    =   array_values($property_types_patterns);
                
                for( $i = 0; $i < count($property_types_patterns); $i++ ){
                    if( preg_match($property_types_patterns[$i], $match[0], $matc) == 1 ){
                        $matched_type   =   $matc[0];
                        break;
                    }
                }

                if(!empty($matched_type)){
                    $query[ 'type' ][]  =   title_case(str_replace('-', ' ', $matched_type));
                    
                    // Trimmed Url
                    if( !empty( $match ) )
                        $url                            =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_ireplace( kebab_case($matched_type), "", $url ) ) ), "-");
                }
            }
        }

        /**
         * Keywords
         */
        preg_match( "/search\/(.*)/", $url, $match );
        if( count( $match ) > 0 ){
            $all_keywords       =   $this->keywords();
            $match[0]           =   str_replace( [ "search/" ], [ "" ], $match[0] );
            $matched_keyword    =   '';

            $keyword_patterns    =   array_map(function($value) { return '/'.kebab_case_custom($value).'/'; }, array_values($all_keywords));
            $keyword_patterns    =   array_values($keyword_patterns);
            for( $i = 0; $i < count($keyword_patterns); $i++ ){
                if( preg_match($keyword_patterns[$i], $match[0], $matc) == 1 ){
                    $matched_keyword   =   $matc[0];
                }
            }

            if(!empty($matched_keyword)){
                $query[ 'keyword' ]   =   $matched_keyword;
                $query[ 'keyword_in_path' ]   =   1;

                // Trimmed Url
                if( !empty( $match ) )
                    $url                            =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_ireplace( kebab_case($matched_keyword), "", $url ) ) ), "-");
            }
        }

        if(array_has($query, 'featured')){
            if(array_has($query, 'keyword') && array_get($query, 'keyword') == 'featured'){
                array_forget( $query, 'keyword' );
                array_forget( $query, 'keyword_in_path' );
            }
        }
        return $query;
    }

    public function create_property_search_url(){
        $url        =   $this->property_search_url_encode( $this->payload );
        $segments   =   explode('/',parse_url($url,PHP_URL_PATH));
        array_splice($segments, 1, 1);
        $return =   implode($segments,'/');
        return $return;
    }

    public function internal_links($query){
        $links_data       =   include( storage_path( 'search/new-internal-links.php' ) );
        $links_data       =   $links_data[ 'residential_commercial' ];
        $beds_data        =   include( storage_path( 'search/bedroom-internal-links.php' ) );
        $beds_data        =   $beds_data[ 'residential_commercial' ];
        $locale           =   \App::getLocale();

        $env_url          =   env( 'APP_URL' );

        $internal_links             = [];
        $internal_links[ 'nav' ]    = [];
        $internal_links[ 'header' ] = [];
        $internal_links[ 'nearby' ] = [];

        if( !empty( $query ) && array_has( $query, 'rent_buy' ) && array_has( $query, 'residential_commercial' ) ){
            $bedroom                        =   array_get( $query, 'bedroom.min' );
            $bedroom_slug                   =   ( $bedroom == 0 ) ? 'studio' : $bedroom;
            $residential_commercial         =   strtolower( array_get( $query, 'residential_commercial.0' ) );
            $residential_commercial_lang    =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom($residential_commercial), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom($residential_commercial)) : ucfirst($residential_commercial);
            $rent_sale                      =   strtolower( array_get( $query, 'rent_buy.0' ) );
            $rent_sale_for_lang             =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale)) : snake_case_custom($rent_sale);
            $rent_buy                       =   ( $rent_sale == 'sale' ) ? 'buy' : $rent_sale;
            $base_url                       =   $env_url . $locale . '/' . $rent_buy . '/' . $residential_commercial;

            $uae_lang               =   __('page.global.uae');
            $city                   =   array_get( $query, 'city.0', null );
            $city_lang              =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
            $suburb                 =   is_null(array_get( $query, 'area.0', null )) ? array_get( $query, 'suburb.0', null )  : array_get( $query, 'area.0', null );
            $suburb_lang            =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($suburb), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($suburb)) : $suburb;
            $building               =   array_get( $query, 'building.0', null );
            $building_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
            $property_type          =   (strtolower(array_get( $query, 'type.0', null ) ) == 'properties' ) ? null : array_get( $query, 'type.0', null);
            $property_type_lang     =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom($property_type), \App::getLocale())) ? __( 'data.type.'.snake_case_custom($property_type)) : (is_null($property_type) ? $property_type : ucfirst( str_plural($property_type)));
            $type_lang              =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom($property_type), \App::getLocale())) ? __( 'data.type.'.snake_case_custom($property_type)) : (is_null($property_type) ? $property_type : strtolower($property_type));

            $alternate_rent_sale            =   ($rent_sale == 'sale') ? 'rent' : 'sale';
            $alternate_rent_buy             =   ($alternate_rent_sale == 'sale') ? 'buy' : 'rent';
            $properties_or_property_type    =   !empty($property_type) ? strtolower($property_type) : 'properties';
            $alternate_base_url             =   $env_url . $locale . '/' . $alternate_rent_buy . '/' . $residential_commercial;
            $alternate_rent_buy_lang        =   (\Lang::has('data.rent_buy.'.snake_case_custom($alternate_rent_buy), \App::getLocale())) ? __( 'data.rent_buy.'.snake_case_custom($alternate_rent_buy)) : snake_case_custom($alternate_rent_buy);
            $alternate_rent_sale_lang       =   (\Lang::has('data.rent_buy.'.snake_case_custom($alternate_rent_sale), \App::getLocale())) ? __( 'data.rent_buy.'.snake_case_custom($alternate_rent_sale)) : snake_case_custom($alternate_rent_sale);
            $alternate_rent_sale_for_lang   =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($alternate_rent_sale), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($alternate_rent_sale)) : snake_case_custom($alternate_rent_sale);

            if(!empty($property_type)){
                $internal_links[ 'bedroom' ][ 'title' ]     =   title_case($type_lang . ' ' . __( 'page.global.types' ));
            }

            // Building Search
            if( !is_null( $building ) && !is_null( $suburb ) && !is_null( $city ) ){
                //  nav links
                foreach($links_data as $res_comm_key => $res_comm_data){
                    foreach($res_comm_data['rent_sale'] as $rent_sale_key => $rent_sale_data){
                        $key                        =   0;
                        $rent_buy_key               =   $rent_sale_key == 'sale' ? 'buy' : $rent_sale_key;
                        $current_rent_sale_for_lang =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale_key), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale_key)) : snake_case_custom($rent_sale_key);
                        $current_res_comm_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties')) : ucfirst($res_comm_key);
                        $current_base_url           =   $env_url . $locale . '/' . $rent_buy_key . '/' . $res_comm_key;
                        foreach($rent_sale_data['types'] as $types_key => $types_data){
                            foreach($types_data['cities'] as $cities_key => $cities_data){
                                foreach($cities_data['areas'] as $areas_key => $areas_data){
                                    foreach($areas_data['buildings'] as $buildings_key => $buildings_data){
                                        if( $buildings_key == $building &&  $areas_key == $suburb && $cities_key == $city ){
                                            $building_lang                  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($areas_key), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($buildings_key)) : $buildings_key;
                                            $current_property_type_lang     =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom(str_plural($types_key)), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(str_plural($types_key))) : str_plural($types_key);
                                            $content_lang                   =   __('data.seo_titles.property_type.nav_content', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang)]);
                                            $title_lang                     =   __('data.seo_titles.property_type.nav_title', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang), 'loc' => $building_lang]);
                                            $internal_links[ 'nav' ][ 'head' ]    =   $building_lang;
                                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]    =   $content_lang;
                                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]   =   $title_lang;
                                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]    =   kebab_case_custom( $current_base_url . '/' . $cities_key . '/' . $areas_key . '/' . $types_key . '-for-' . $rent_sale_key . '-in-' . $buildings_key . '-building' );
                                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'count' ]   =   $buildings_data[ 'count' ];
                                            $key++;
                                        }
                                    }
                                }
                            }
                        }
                        if( $key !== 0 ){
                            //  nav default link
                            $content_lang           =   __('data.seo_titles.default.content', ['residential_commercial' => $current_res_comm_lang]);
                            $title_lang             =   __('data.seo_titles.default.title', ['for_rent_buy' => strtolower($current_rent_sale_for_lang), 'residential_commercial' => $current_res_comm_lang, 'loc' => $building_lang]);
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]    =   $content_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]   =   $title_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]    =   kebab_case_custom( $current_base_url . '/' . $city . '/' . $suburb . '/properties-for-' . $rent_sale_key . '-in-' . $building . '-building' );

                            //  alternate link
                            if( $residential_commercial === $res_comm_key ){
                                if( $alternate_rent_sale === $rent_sale_key ){
                                    if(!isset($bedroom)){
                                        $alternate_link     =   kebab_case_custom( $alternate_base_url . '/' . $city . '/' . $suburb . '/' . $properties_or_property_type . '-for-' . $alternate_rent_sale . '-in-' . $building . '-building' );
                                        if(empty($property_type)){
                                            $alternate_link_title       =   __('data.seo_titles.no_property_type.alternate_link_title', ['for_rent_buy' => $alternate_rent_sale_for_lang, 'loc' => $building_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.no_property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'loc' => $building_lang]);
                                        }
                                        else{
                                            $alternate_link_title       =   __('data.seo_titles.property_type.alternate_link_title', ['for_rent_buy' => strtolower($alternate_rent_sale_for_lang), 'type' => title_case($property_type_lang), 'loc' => $building_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'type' => $type_lang, 'loc' => $building_lang]);
                                        }
                                        $anchor                     =   '<a href="' . $alternate_link . '" title="' . $alternate_link_title . '">' . $alternate_link_content . '</a>';
                                        $alternate_content          =   __('data.seo_titles.property_type.alternate_content', ['content' => $anchor]);
                                        $internal_links[ 'alternate' ][ 'name' ]    =   $alternate_content;
                                    }
                                }
                            }
                        }
                    }
                }
                //  no header links
                //  nearby links
                if( array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' ) ){
                    $types      =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types', [] );
                    $internal_links[ 'nearby' ][ 'title' ]   =   __('data.seo_titles.near', ['loc' => $building_lang]);
                    if(empty($property_type)){
                        if(!isset($bedroom)){
                            //  nearby links
                            $buildings  =   collect($types)->pluck('cities.'.$city.'.areas.'.$suburb.'.buildings')->collapse()->keys()->all();
                            if(!empty($buildings)){
                                foreach($buildings as $key => $nearby_building){
                                    if( $building !== $nearby_building ){
                                        $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($nearby_building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($nearby_building)) : $nearby_building;
                                        $content_lang   =   __('data.seo_titles.no_property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                        $title_lang     =   __('data.seo_titles.no_property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . 'properties-for-' . $rent_sale . '-in-' . $nearby_building . '-building' );
                                    }
                                }
                            }
                        }
                        else{
                            //  nearby links
                            $types          =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' );
                            $buildings_arr  =   collect($types)->pluck('beds.'.$bedroom.'.cities.'.$city.'.areas.'.$suburb.'.buildings')->all();
                            $buildings      =   [];
                            foreach($buildings_arr as $each_building)
                                if( is_array( $each_building ) )
                                    $buildings       =   array_merge( $buildings, $each_building );
                            $buildings     =   array_keys($buildings);
                            if(!empty($buildings)){
                                foreach($buildings as $key => $nearby_building){
                                    if( $building !== $nearby_building ){
                                        $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($nearby_building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($nearby_building)) : $nearby_building;
                                        $content_lang   =   __('data.seo_titles.no_property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                        $title_lang     =   __('data.seo_titles.no_property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $bedroom_slug . '-bedroom-for-' . $rent_sale . '-in-' . $nearby_building . '-building' );
                                    }
                                }
                            }
                        }
                    }
                    else{
                        if(!isset($bedroom)){
                            if(array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas.' . $suburb . '.buildings' )){
                                //  nearby links
                                $buildings  =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas.' . $suburb . '.buildings', [] );
                                $key        =   0;
                                if(!empty($buildings)){
                                    foreach($buildings as $nearby_building => $data){
                                        if( $building !== $nearby_building ){
                                            $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($nearby_building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($nearby_building)) : $nearby_building;
                                            $content_lang   =   __('data.seo_titles.property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'type' => $property_type_lang, 'loc' => $building_lang]);
                                            $title_lang     =   __('data.seo_titles.property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'type' => strtolower($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $property_type . '-for-' . $rent_sale . '-in-' . $nearby_building . '-building' );
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                            $key++;
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas.' . $suburb . '.buildings' )){
                                //  nearby links
                                $buildings  =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas.' . $suburb . '.buildings', [] );
                                $key        =   0;
                                if(!empty($buildings)){
                                    foreach($buildings as $nearby_building => $data){
                                        if( $building !== $nearby_building ){
                                            $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($nearby_building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($nearby_building)) : $nearby_building;
                                            $content_lang   =   __('data.seo_titles.property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'type' => $property_type_lang, 'loc' => $building_lang]);
                                            $title_lang     =   __('data.seo_titles.property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'type' => strtolower($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $bedroom_slug . '-bedroom-' . $property_type . '-for-' . $rent_sale . '-in-' . $nearby_building . '-building' );
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                            $key++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //  bedroom links
                if(!empty($property_type)){
                    if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds')){
                        $building_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
                        $bedrooms_building      =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds' );
                        $bedrooms_building_arr  =   collect($bedrooms_building)->filter(function ($value, $key) use ($city, $suburb, $building) {
                            return array_has($value,'cities.'.$city.'.areas.'.$suburb.'.buildings.'.$building);
                        })->all();
                        $bedrooms_building_arr  =   array_keys($bedrooms_building_arr);
                        sort($bedrooms_building_arr);
                        if(isset($bedroom)){
                            if (($bed_key = array_search($bedroom, $bedrooms_building_arr)) !== false) {
                                unset($bedrooms_building_arr[$bed_key]);
                            }
                        }
                        // Remove -1 i.e., no bedrooms
                        if (($no_bed_key = array_search(-1, $bedrooms_building_arr)) !== false) {
                            unset($bedrooms_building_arr[$no_bed_key]);
                        }
                        // Remove commercial property links without bedrooms
                        if (($zero_bed_key = array_search(0, $bedrooms_building_arr)) !== false && (strtolower($residential_commercial) == 'commercial' || strtolower($property_type) !== 'apartment')) {
                            unset($bedrooms_building_arr[$zero_bed_key]);
                        }
                        $bedrooms_building      =   array_slice($bedrooms_building_arr, 0, 5);
                        $key = 0;
                        foreach($bedrooms_building as $beds_building){
                            $content_lang   =   __('data.seo_titles.bedrooms.'.$beds_building).__('data.seo_titles.bedroom_content', ['type' => ($beds_building == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                            $title_lang     =   __('data.seo_titles.bedrooms.'.$beds_building).__('data.seo_titles.bedroom_content', ['type' => ($beds_building == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                            $beds_building  =   $beds_building == 0 ? 'studio' : $beds_building;

                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'name' ]   =   preg_replace('/\s+/', ' ', trim($content_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'title' ]  =   preg_replace('/\s+/', ' ', trim($title_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $beds_building . '-bedroom-'  . $property_type . '-for-' . $rent_sale . '-in-' . $building . '-building' );
                            $key++;
                        }
                    }
                }
            }
            // Area Search
            else if( is_null( $building ) && !is_null( $suburb ) && !is_null( $city ) ){
                //  nav links
                foreach($links_data as $res_comm_key => $res_comm_data){
                    foreach($res_comm_data['rent_sale'] as $rent_sale_key => $rent_sale_data){
                        $key                        =   0;
                        $rent_buy_key               =   $rent_sale_key == 'sale' ? 'buy' : $rent_sale_key;
                        $current_rent_sale_for_lang =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale_key), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale_key)) : snake_case_custom($rent_sale_key);
                        $current_res_comm_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties')) : ucfirst($res_comm_key);
                        $current_base_url           =   $env_url . $locale . '/' . $rent_buy_key . '/' . $res_comm_key;
                        foreach($rent_sale_data['types'] as $types_key => $types_data){
                            foreach($types_data['cities'] as $cities_key => $cities_data){
                                foreach($cities_data['areas'] as $areas_key => $areas_data){
                                    if( $areas_key == $suburb && $cities_key == $city ){
                                        $area_lang                  =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($areas_key), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($areas_key)) : $areas_key;
                                        $current_property_type_lang =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom(str_plural($types_key)), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(str_plural($types_key))) : str_plural($types_key);
                                        $content_lang               =   __('data.seo_titles.property_type.nav_content', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang)]);
                                        $title_lang                 =   __('data.seo_titles.property_type.nav_title', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'nav' ][ 'head' ]   =   $area_lang;
                                        $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]   =   kebab_case_custom( $current_base_url . '/' . $cities_key . '/' . $types_key . '-for-' . $rent_sale_key . '-in-' . $areas_key. '-area' );
                                        $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'count' ]  =   $areas_data['count'];
                                        $key++;
                                    }
                                }
                            }
                        }
                        if( $key !== 0 ){
                            //  nav default link
                            $content_lang           =   __('data.seo_titles.default.content', ['residential_commercial' => $current_res_comm_lang]);
                            $title_lang             =   __('data.seo_titles.default.title', ['for_rent_buy' => strtolower($current_rent_sale_for_lang), 'residential_commercial' => $current_res_comm_lang, 'loc' => $area_lang]);
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]    =   $content_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]   =   $title_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]    =   kebab_case_custom( $current_base_url . '/' . $city . '/properties-for-' . $rent_sale_key . '-in-' . $suburb . '-area' );

                            //  alternate link
                            if( $residential_commercial === $res_comm_key ){
                                if( $alternate_rent_sale === $rent_sale_key ){
                                    if(!isset($bedroom)){
                                        $alternate_link     =   kebab_case_custom( $alternate_base_url . '/' . $city . '/' . $properties_or_property_type . '-for-' . $alternate_rent_sale . '-in-' . $suburb . '-area' );
                                        if(empty($property_type)){
                                            $alternate_link_title       =   __('data.seo_titles.no_property_type.alternate_link_title', ['for_rent_buy' => $alternate_rent_sale_for_lang, 'loc' => $suburb_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.no_property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'loc' => $suburb_lang]);
                                        }
                                        else{
                                            $alternate_link_title       =   __('data.seo_titles.property_type.alternate_link_title', ['for_rent_buy' => strtolower($alternate_rent_sale_for_lang), 'type' => title_case($property_type_lang), 'loc' => $suburb_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'type' => $type_lang, 'loc' => $suburb_lang]);
                                        }
                                        $anchor                     =   '<a href="' . $alternate_link . '" title="' . $alternate_link_title . '">' . $alternate_link_content . '</a>';
                                        $alternate_content          =   __('data.seo_titles.property_type.alternate_content', ['content' => $anchor]);
                                        $internal_links[ 'alternate' ][ 'name' ]    =   $alternate_content;
                                    }
                                }
                            }
                        }
                    }
                }
                //  header & nearby links
                if( array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' ) ){
                    $rent_buy   =   $rent_sale == 'sale' ? 'buy' : $rent_sale;
                    $types      =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types', [] );
                    $internal_links[ 'nearby' ][ 'title' ]   =   __('data.seo_titles.near', ['loc' => $suburb_lang]);

                    if(empty($property_type)){
                        if(!isset($bedroom)){
                            //  header links
                            $buildings  =   collect($types)->pluck('cities.'.$city.'.areas.'.$suburb.'.buildings')->collapse()->keys()->all();
                            if(!empty($buildings)){
                                foreach($buildings as $key => $building){
                                    $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
                                    $content_lang   =   __('data.seo_titles.no_property_type.header_content', ['loc' => $building_lang]);
                                    $title_lang     =   __('data.seo_titles.no_property_type.header_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                    $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                    $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                    $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . 'properties-for-' . $rent_sale . '-in-' . $building . '-building' );
                                }
                            }
                            //  nearby links
                            $areas  =   collect($types)->pluck('cities.'.$city.'.areas')->collapse()->keys()->all();
                            if(!empty($areas)){
                                foreach($areas as $key => $nearby_area){
                                    if( $nearby_area !== $suburb ){
                                        $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($nearby_area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($nearby_area)) : $nearby_area;
                                        $content_lang   =   __('data.seo_titles.no_property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $title_lang     =   __('data.seo_titles.no_property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . 'properties-for-' . $rent_sale . '-in-' . $nearby_area . '-area' );
                                    }
                                }
                            }
                        }
                        else{
                            //  header links
                            $types          =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' );
                            $buildings_arr  =   collect($types)->pluck('beds.'.$bedroom.'.cities.'.$city.'.areas.'.$suburb.'.buildings')->all();
                            $buildings      =   [];
                            foreach($buildings_arr as $each_building)
                                if( is_array( $each_building ) )
                                    $buildings       =   array_merge( $buildings, $each_building );
                            $buildings     =   array_keys($buildings);
                            if(!empty($buildings)){
                                foreach($buildings as $key => $building){
                                    $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
                                    $content_lang   =   __('data.seo_titles.no_property_type.header_content', ['loc' => $building_lang]);
                                    $title_lang     =   __('data.seo_titles.no_property_type.header_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                    $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                    $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                    $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $bedroom_slug . '-bedroom-for-' . $rent_sale . '-in-' . $building . '-building' );
                                }
                            }
                            //  nearby links
                            $areas_arr  =   collect($types)->pluck('beds.'.$bedroom.'.cities.'.$city.'.areas')->all();
                            $areas      =   [];
                            foreach($areas_arr as $each_area)
                                if( is_array( $each_area ) )
                                    $areas       =   array_merge( $areas, $each_area );
                            $areas     =   array_keys($areas);
                            if(!empty($areas)){
                                foreach($areas as $key => $nearby_area){
                                    if( $nearby_area !== $suburb ){
                                        $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($nearby_area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($nearby_area)) : $nearby_area;
                                        $content_lang   =   __('data.seo_titles.no_property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $title_lang     =   __('data.seo_titles.no_property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $bedroom_slug . '-bedroom-for-' . $rent_sale . '-in-' . $nearby_area . '-area' );
                                    }
                                }
                            }
                        }
                    }
                    else{
                        if(!isset($bedroom)){
                            if(array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas.' . $suburb . '.buildings' )){
                                //  header links
                                $buildings  =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas.' . $suburb . '.buildings', [] );
                                $key        =   0;
                                if(!empty($buildings)){
                                    foreach($buildings as $building => $data){
                                        $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
                                        $content_lang   =   __('data.seo_titles.property_type.header_content', ['loc' => $building_lang]);
                                        $title_lang     =   __('data.seo_titles.property_type.header_title', ['type' => $property_type_lang, 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                        $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $property_type . '-for-' . $rent_sale . '-in-' . $building . '-building' );
                                        $internal_links[ 'header' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                        $key++;
                                    }
                                }
                                //  nearby links
                                $areas  =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas', [] );
                                $key    =   0;
                                if(!empty($areas)){
                                    foreach($areas as $nearby_area => $data){
                                        $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($nearby_area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($nearby_area)) : $nearby_area;
                                        $content_lang   =   __('data.seo_titles.property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'type' => $property_type_lang, 'loc' => $area_lang]);
                                        $title_lang     =   __('data.seo_titles.property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'type' => strtolower($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $property_type . '-for-' . $rent_sale . '-in-' . $nearby_area . '-area' );
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                        $key++;
                                    }
                                }
                            }
                        }
                        else{
                            if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas.' . $suburb . '.buildings' )){
                                //  header links
                                $buildings  =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas.' . $suburb . '.buildings', [] );
                                $key        =   0;
                                if(!empty($buildings)){
                                    foreach($buildings as $building => $data){
                                        $building_lang  =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
                                        $content_lang   =   __('data.seo_titles.property_type.header_content', ['loc' => $building_lang]);
                                        $title_lang     =   __('data.seo_titles.property_type.header_title', ['type' => $property_type_lang, 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $building_lang]);
                                        $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $suburb . '/' . $bedroom_slug . '-bedroom-' . $property_type . '-for-' . $rent_sale . '-in-' . $building . '-building' );
                                        $internal_links[ 'header' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                        $key++;
                                    }
                                }
                                //  nearby links
                                $areas  =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas', [] );
                                $key    =   0;
                                if(!empty($areas)){
                                    foreach($areas as $nearby_area => $data){
                                        $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($nearby_area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($nearby_area)) : $nearby_area;
                                        $content_lang   =   __('data.seo_titles.property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'type' => $property_type_lang, 'loc' => $area_lang]);
                                        $title_lang     =   __('data.seo_titles.property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'type' => strtolower($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $bedroom_slug . '-bedroom-' . $property_type . '-for-' . $rent_sale . '-in-' . $nearby_area . '-area' );
                                        $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                        $key++;
                                    }
                                }
                            }
                        }
                    }
                }
                //  bedroom links
                if(!empty($property_type)){
                    if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds')){
                        $area_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($suburb), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($suburb)) : $suburb;
                        $bedrooms_area      =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds' );
                        $bedrooms_area_arr  =   collect($bedrooms_area)->filter(function ($value, $key) use ($city, $suburb) {
                            return array_has($value,'cities.'.$city.'.areas.'.$suburb);
                        })->all();
                        $bedrooms_area_arr  =   array_keys($bedrooms_area_arr);
                        sort($bedrooms_area_arr);
                        if(isset($bedroom)){
                            if (($bed_key = array_search($bedroom, $bedrooms_area_arr)) !== false) {
                                unset($bedrooms_area_arr[$bed_key]);
                            }
                        }
                        // Remove -1 i.e., no bedrooms
                        if (($no_bed_key = array_search(-1, $bedrooms_area_arr)) !== false) {
                            unset($bedrooms_area_arr[$no_bed_key]);
                        }
                        // Remove commercial property links without bedrooms
                        if (($zero_bed_key = array_search(0, $bedrooms_area_arr)) !== false && (strtolower($residential_commercial) == 'commercial' || strtolower($property_type) !== 'apartment')) {
                            unset($bedrooms_area_arr[$zero_bed_key]);
                        }
                        $bedrooms_area      =   array_slice($bedrooms_area_arr, 0, 5);
                        $key = 0;
                        foreach($bedrooms_area as $beds_area){
                            $content_lang   =   __('data.seo_titles.bedrooms.'.$beds_area).__('data.seo_titles.bedroom_content', ['type' => ($beds_area == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                            $title_lang     =   __('data.seo_titles.bedrooms.'.$beds_area).__('data.seo_titles.bedroom_content', ['type' => ($beds_area == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                            $beds_area      =   $beds_area == 0 ? 'studio' : $beds_area;

                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'name' ]   =   preg_replace('/\s+/', ' ', trim($content_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'title' ]  =   preg_replace('/\s+/', ' ', trim($title_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $beds_area . '-bedroom-'  . $property_type . '-for-' . $rent_sale . '-in-' . $suburb . '-area' );
                            $key++;
                        }
                    }
                }
            }
            // City Search Only
            else if( is_null( $building ) && is_null( $suburb ) && !is_null( $city ) ){
                //  nav links
                foreach($links_data as $res_comm_key => $res_comm_data){
                    foreach($res_comm_data['rent_sale'] as $rent_sale_key => $rent_sale_data){
                        $key                        =   0;
                        $rent_buy_key               =   $rent_sale_key == 'sale' ? 'buy' : $rent_sale_key;
                        $current_rent_sale_for_lang =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale_key), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale_key)) : snake_case_custom($rent_sale_key);
                        $current_res_comm_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties')) : ucfirst($res_comm_key);
                        $current_base_url           =   $env_url . $locale . '/' . $rent_buy_key . '/' . $res_comm_key;
                        foreach($rent_sale_data['types'] as $types_key => $types_data){
                            foreach($types_data['cities'] as $cities_key => $cities_data){
                                if( $cities_key == $city ){
                                    $city_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($cities_key), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($cities_key)) : $cities_key;
                                    $current_property_type_lang =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom(str_plural($types_key)), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(str_plural($types_key))) : str_plural($types_key);
                                    $content_lang       =   __('data.seo_titles.property_type.nav_content', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang)]);
                                    $title_lang         =   __('data.seo_titles.property_type.nav_title', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang), 'loc' => $city_lang]);
                                    $internal_links[ 'nav' ][ 'head' ]   =   $city_lang;
                                    $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]   =   $content_lang;
                                    $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]  =   $title_lang;
                                    $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]   =   kebab_case_custom( $current_base_url . '/' . $types_key . '-for-' . $rent_sale_key . '-in-' . $cities_key. '-city' );
                                    $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'count' ]  =   $cities_data['count'];
                                    $key++;
                                }
                            }
                        }
                        if( $key !== 0 ){
                            //  nav default link
                            $content_lang           =   __('data.seo_titles.default.content', ['residential_commercial' => $current_res_comm_lang]);
                            $title_lang             =   __('data.seo_titles.default.title', ['for_rent_buy' => strtolower($current_rent_sale_for_lang), 'residential_commercial' => $current_res_comm_lang, 'loc' => $city_lang]);
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]    =   $content_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]   =   $title_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]    =   kebab_case_custom( $current_base_url . '/properties-for-' . $rent_sale_key . '-in-' . $city . '-city' );

                            //  alternate link
                            if( $residential_commercial === $res_comm_key ){
                                if( $alternate_rent_sale === $rent_sale_key ){
                                    if(!isset($bedroom)){
                                        $alternate_link     =   kebab_case_custom( $alternate_base_url . '/' . $properties_or_property_type . '-for-' . $alternate_rent_sale . '-in-' . $city . '-city' );
                                        if(empty($property_type)){
                                            $alternate_link_title       =   __('data.seo_titles.no_property_type.alternate_link_title', ['for_rent_buy' => $alternate_rent_sale_for_lang, 'loc' => $city_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.no_property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'loc' => $city_lang]);
                                        }
                                        else{
                                            $alternate_link_title       =   __('data.seo_titles.property_type.alternate_link_title', ['for_rent_buy' => strtolower($alternate_rent_sale_for_lang), 'type' => title_case($property_type_lang), 'loc' => $city_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'type' => $type_lang, 'loc' => $city_lang]);
                                        }
                                        $anchor                     =   '<a href="' . $alternate_link . '" title="' . $alternate_link_title . '">' . $alternate_link_content . '</a>';
                                        $alternate_content          =   __('data.seo_titles.property_type.alternate_content', ['content' => $anchor]);
                                        $internal_links[ 'alternate' ][ 'name' ]    =   $alternate_content;
                                    }
                                }
                            }
                        }
                    }
                }
                //  header & nearby links
                if( array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' ) ){
                    $internal_links[ 'nearby' ][ 'title' ]   =   __('data.seo_titles.near', ['loc' => $city_lang]);
                    $types  =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types', [] );
                    if(!empty($types)){
                        if(empty($property_type)){
                            if(!isset($bedroom)){
                                //  header links
                                $areas      =   collect($types)->pluck('cities.'.$city.'.areas')->collapse()->keys()->all();
                                if(!empty($areas)){
                                    foreach($areas as $key => $area){
                                        $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
                                        $content_lang   =   __('data.seo_titles.no_property_type.header_content', ['loc' => $area_lang]);
                                        $title_lang     =   __('data.seo_titles.no_property_type.header_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . 'properties-for-' . $rent_sale . '-in-' . $area . '-area' );
                                    }
                                }
                                //  nearby links
                                $cities =   collect($types)->pluck('cities')->collapse()->keys()->all();
                                if(!empty($cities)){
                                    foreach($cities as $key => $nearby_city){
                                        if( $nearby_city !== $city ){
                                            $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($nearby_city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($nearby_city)) : $nearby_city;
                                            $content_lang   =   __('data.seo_titles.no_property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                            $title_lang     =   __('data.seo_titles.no_property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . 'properties-for-' . $rent_sale . '-in-' . $nearby_city . '-city' );
                                        }
                                    }
                                }
                            }
                            else{
                                //  header links
                                $types      =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' );
                                $areas_arr  =   collect($types)->pluck('beds.'.$bedroom.'.cities.'.$city.'.areas')->all();
                                $areas      =   [];
                                foreach($areas_arr as $each_area)
                                    if( is_array( $each_area ) )
                                        $areas       =   array_merge( $areas, $each_area );
                                $areas     =   array_keys($areas);

                                if(!empty($areas)){
                                    foreach($areas as $key => $area){
                                        $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
                                        $content_lang   =   __('data.seo_titles.no_property_type.header_content', ['loc' => $area_lang]);
                                        $title_lang     =   __('data.seo_titles.no_property_type.header_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                        $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                        $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                        $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $bedroom_slug . '-bedroom-for-' . $rent_sale . '-in-' . $area . '-area' );
                                    }
                                }
                                //  nearby links
                                $cities_arr     =   collect($types)->pluck('beds.'.$bedroom.'.cities')->all();
                                $cities         =   [];
                                foreach($cities_arr as $each_city)
                                    if( is_array( $each_city ) )
                                        $cities       =   array_merge( $cities, $each_city );
                                $cities     =   array_keys($cities);
                                if(!empty($cities)){
                                    foreach($cities as $key => $nearby_city){
                                        if( $nearby_city !== $city ){
                                            $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($nearby_city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($nearby_city)) : $nearby_city;
                                            $content_lang   =   __('data.seo_titles.no_property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                            $title_lang     =   __('data.seo_titles.no_property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                            $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $bedroom_slug . '-bedroom-for-' . $rent_sale . '-in-' . $nearby_city . '-city' );
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            if(!isset($bedroom)){
                                if(array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities')){
                                    //  header links
                                    if(array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas' )){
                                        $areas  =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities.' . $city . '.areas', [] );
                                        $key    =   0;
                                        if(!empty($areas)){
                                            foreach($areas as $area => $data){
                                                $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
                                                $content_lang   =   __('data.seo_titles.property_type.header_content', ['loc' => $area_lang]);
                                                $title_lang     =   __('data.seo_titles.property_type.header_title', ['type' => $property_type_lang, 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                                $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                                $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                                $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $property_type . '-for-' . $rent_sale . '-in-' . $area . '-area' );
                                                $internal_links[ 'header' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                                $key++;
                                            }
                                        }
                                    }
                                    //  nearby links
                                    $cities =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities', [] );
                                    $key    =   0;
                                    if(!empty($cities)){
                                        foreach($cities as $nearby_city => $data){
                                            if( $nearby_city !== $city ){
                                                $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($nearby_city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($nearby_city)) : $nearby_city;
                                                $content_lang   =   __('data.seo_titles.property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'type' => $property_type_lang, 'loc' => $city_lang]);
                                                $title_lang     =   __('data.seo_titles.property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'type' => strtolower($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $property_type . '-for-' . $rent_sale . '-in-' . $nearby_city . '-city' );
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                                $key++;
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities')){
                                    //  header links
                                    if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas' )){
                                        $areas  =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities.' . $city . '.areas', [] );
                                        $key    =   0;
                                        if(!empty($areas)){
                                            foreach($areas as $area => $data){
                                                $area_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
                                                $content_lang   =   __('data.seo_titles.property_type.header_content', ['loc' => $area_lang]);
                                                $title_lang     =   __('data.seo_titles.property_type.header_title', ['type' => $property_type_lang, 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $area_lang]);
                                                $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                                $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                                $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $city . '/' . $bedroom_slug . '-bedroom-' . $property_type . '-for-' . $rent_sale . '-in-' . $area . '-area' );
                                                $internal_links[ 'header' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                                $key++;
                                            }
                                        }
                                    }
                                    //  nearby links
                                    $cities =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities', [] );
                                    $key    =   0;
                                    if(!empty($cities)){
                                        foreach($cities as $nearby_city => $data){
                                            if( $nearby_city !== $city ){
                                                $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($nearby_city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($nearby_city)) : $nearby_city;
                                                $content_lang   =   __('data.seo_titles.property_type.nearby_content', ['for_rent_buy' => strtolower($rent_sale_for_lang), 'type' => $property_type_lang, 'loc' => $city_lang]);
                                                $title_lang     =   __('data.seo_titles.property_type.nearby_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'type' => strtolower($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'name' ]   =   $content_lang;
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'title' ]  =   $title_lang;
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $bedroom_slug . '-bedroom-' . $property_type . '-for-' . $rent_sale . '-in-' . $nearby_city . '-city' );
                                                $internal_links[ 'nearby' ][ 'content' ][ $key ][ 'count' ]  =   $data[ 'count' ];
                                                $key++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //  bedroom links
                if(!empty($property_type)){
                    if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds')){
                        $city_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
                        $bedrooms_city      =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds' );
                        $bedrooms_city_arr  =   collect($bedrooms_city)->filter(function ($value, $key) use ($city) {
                            return array_has($value,'cities.'.$city);
                        })->all();
                        $bedrooms_city_arr  =   array_keys($bedrooms_city_arr);
                        sort($bedrooms_city_arr);
                        if(isset($bedroom)){
                            if (($bed_key = array_search($bedroom, $bedrooms_city_arr)) !== false) {
                                unset($bedrooms_city_arr[$bed_key]);
                            }
                        }
                        // Remove -1 i.e., no bedrooms
                        if (($no_bed_key = array_search(-1, $bedrooms_city_arr)) !== false) {
                            unset($bedrooms_city_arr[$no_bed_key]);
                        }
                        // Remove commercial property links without bedrooms
                        if (($zero_bed_key = array_search(0, $bedrooms_city_arr)) !== false && (strtolower($residential_commercial) == 'commercial' || strtolower($property_type) !== 'apartment')) {
                            unset($bedrooms_city_arr[$zero_bed_key]);
                        }
                        $bedrooms_city      =   array_slice($bedrooms_city_arr, 0, 5);
                        $key = 0;
                        foreach($bedrooms_city as $beds_city){
                            $content_lang   =   __('data.seo_titles.bedrooms.'.$beds_city).__('data.seo_titles.bedroom_content', ['type' => ($beds_city == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                            $title_lang     =   __('data.seo_titles.bedrooms.'.$beds_city).__('data.seo_titles.bedroom_content', ['type' => ($beds_city == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                            $beds_city      =   $beds_city == 0 ? 'studio' : $beds_city;

                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'name' ]   =   preg_replace('/\s+/', ' ', trim($content_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'title' ]  =   preg_replace('/\s+/', ' ', trim($title_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $beds_city . '-bedroom-'  . $property_type . '-for-' . $rent_sale . '-in-' . $city . '-city' );
                            $key++;
                        }
                    }
                }
            }
            // Search without Location
            else if( is_null( $building ) && is_null( $suburb ) && is_null( $city ) ){
                //  nav links
                foreach($links_data as $res_comm_key => $res_comm_data){
                    foreach($res_comm_data['rent_sale'] as $rent_sale_key => $rent_sale_data){
                        $key                        =   0;
                        $rent_buy_key               =   $rent_sale_key == 'sale' ? 'buy' : $rent_sale_key;
                        $current_base_url           =   $env_url . $locale . '/' . $rent_buy_key . '/' . $res_comm_key;
                        $current_rent_sale_for_lang =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale_key), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale_key)) : snake_case_custom($rent_sale_key);
                        $current_res_comm_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties'), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom($res_comm_key.'_properties')) : ucfirst($res_comm_key);
                        foreach($rent_sale_data['types'] as $types_key => $types_data){
                            $current_property_type_lang =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom(str_plural($types_key)), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(str_plural($types_key))) : str_plural($types_key);
                            $content_lang               =   __('data.seo_titles.property_type.nav_content', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang)]);
                            $title_lang                 =   __('data.seo_titles.property_type.nav_title', ['type' => title_case($current_property_type_lang), 'for_rent_buy' => strtolower($current_rent_sale_for_lang), 'loc' => $uae_lang]);
                            $internal_links[ 'nav' ][ 'head' ]   =   strtoupper($uae_lang);
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]   =   $content_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]  =   $title_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]   =   kebab_case_custom( $current_base_url . '/' . $types_key . '-for-' . $rent_sale_key );
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'count' ]  =   $types_data['count'];
                            $key++;
                        }
                        if( $key !== 0 ){
                            //  nav default link
                            $content_lang           =   __('data.seo_titles.default.content', ['residential_commercial' => $current_res_comm_lang]);
                            $title_lang             =   __('data.seo_titles.default.title', ['for_rent_buy' => strtolower($current_rent_sale_for_lang), 'residential_commercial' => $current_res_comm_lang, 'loc' => $uae_lang]);
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'name' ]    =   $content_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'title' ]   =   $title_lang;
                            $internal_links[ 'nav' ][ $rent_sale_key ][ $res_comm_key ][ $key ][ 'link' ]    =   kebab_case_custom( $current_base_url . '/properties-for-' . $rent_sale_key );

                            //  alternate link
                            if( $residential_commercial === $res_comm_key ){
                                if( $alternate_rent_sale === $rent_sale_key ){
                                    if(!isset($bedroom)){
                                        $alternate_link     =   kebab_case_custom( $alternate_base_url . '/' . $properties_or_property_type . '-for-' . $alternate_rent_sale );
                                        if(empty($property_type)){
                                            $alternate_link_title       =   __('data.seo_titles.no_property_type.alternate_link_title', ['for_rent_buy' => $alternate_rent_sale_for_lang, 'loc' => $uae_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.no_property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'loc' => $uae_lang]);
                                        }
                                        else{
                                            $alternate_link_title       =   __('data.seo_titles.property_type.alternate_link_title', ['for_rent_buy' => strtolower($alternate_rent_sale_for_lang), 'type' => title_case($property_type_lang), 'loc' => $uae_lang]);
                                            $alternate_link_content     =   __('data.seo_titles.property_type.alternate_link_content', ['rent_buy' => strtolower($alternate_rent_buy_lang), 'type' => $type_lang, 'loc' => $uae_lang]);
                                        }
                                        $anchor                     =   '<a href="' . $alternate_link . '" title="' . $alternate_link_title . '">' . $alternate_link_content . '</a>';
                                        $alternate_content          =   __('data.seo_titles.property_type.alternate_content', ['content' => $anchor]);
                                        $internal_links[ 'alternate' ][ 'name' ]    =   $alternate_content;
                                    }
                                }
                            }
                        }
                    }
                }
                //  header links
                if( array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' ) ){
                    if(empty($property_type)){
                        if(!isset($bedroom)){
                            $types                  =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' );
                            $cities                 =   collect($types)->pluck('cities')->collapse()->keys()->all();
                            foreach($cities as $key => $city){
                                $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
                                $content_lang   =   __('data.seo_titles.no_property_type.header_content', ['loc' => $city_lang]);
                                $title_lang     =   __('data.seo_titles.no_property_type.header_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . 'properties-for-' . $rent_sale . '-in-' . $city . '-city' );
                            }
                        }
                        else{
                            $types                  =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types' );
                            $cities_arr             =   collect($types)->pluck('beds.'.$bedroom.'.cities')->all();
                            $cities                 =   [];
                            foreach($cities_arr as $each_city)
                                if( is_array( $each_city ) )
                                    $cities       =   array_merge( $cities, $each_city );
                            $cities     =   array_keys($cities);

                            foreach($cities as $key => $city){
                                $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
                                $content_lang   =   __('data.seo_titles.no_property_type.header_content', ['loc' => $city_lang]);
                                $title_lang     =   __('data.seo_titles.no_property_type.header_title', ['residential_commercial' => ucfirst( $residential_commercial_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $bedroom_slug . '-bedroom-for-' . $rent_sale . '-in-' . $city . '-city' );
                            }
                        }
                    }
                    else{
                        if(!isset($bedroom)){
                            if(array_has( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities' )){
                                $cities =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.cities' );
                                $key    =   0;
                                foreach($cities as $city => $data){
                                    $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
                                    $content_lang   =   __('data.seo_titles.property_type.header_content', ['loc' => $city_lang]);
                                    $title_lang     =   __('data.seo_titles.property_type.header_title', ['type' => $property_type_lang, 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                    $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                    $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                    $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $property_type . '-for-' . $rent_sale . '-in-' . $city . '-city' );
                                    $internal_links[ 'header' ][ $key ][ 'count' ]  =   $data['count'];
                                    $key++;
                                }
                            }
                        }
                        else{
                            if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities')){
                                $cities =   array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds.' . $bedroom . '.cities' );
                                $key    =   0;
                                foreach($cities as $city => $data){
                                    $city_lang      =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;
                                    $content_lang   =   __('data.seo_titles.property_type.header_content', ['loc' => $city_lang]);
                                    $title_lang     =   __('data.seo_titles.property_type.header_title', ['type' => $property_type_lang, 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $city_lang]);
                                    $internal_links[ 'header' ][ $key ][ 'name' ]   =   $content_lang;
                                    $internal_links[ 'header' ][ $key ][ 'title' ]  =   $title_lang;
                                    $internal_links[ 'header' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $bedroom_slug . '-bedroom-' . $property_type . '-for-' . $rent_sale . '-in-' . $city . '-city' );
                                    $internal_links[ 'header' ][ $key ][ 'count' ]  =   $data['count'];
                                    $key++;
                                }
                            }
                        }
                    }
                }
                //  no nearby links
                //  bedroom links
                if(!empty($property_type)){
                    if(array_has( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds')){
                        $bedrooms_uae   =   array_keys( array_get( $beds_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types.' . strtolower($property_type) . '.beds' ) );
                        sort($bedrooms_uae);
                        if(isset($bedroom)){
                            if (($bed_key = array_search($bedroom, $bedrooms_uae)) !== false) {
                                unset($bedrooms_uae[$bed_key]);
                            }
                        }
                        // Remove -1 i.e., no bedrooms
                        if (($no_bed_key = array_search(-1, $bedrooms_uae)) !== false) {
                            unset($bedrooms_uae[$no_bed_key]);
                        }
                        // Remove commercial property links without bedrooms
                        if (($zero_bed_key = array_search(0, $bedrooms_uae)) !== false && (strtolower($residential_commercial) == 'commercial' || strtolower($property_type) !== 'apartment')) {
                            unset($bedrooms_uae[$zero_bed_key]);
                        }
                        $bedrooms_uae   =   array_slice($bedrooms_uae, 0, 5);
                        $key = 0;
                        foreach($bedrooms_uae as $beds_uae){
                            $content_lang   =   __('data.seo_titles.bedrooms.'.$beds_uae).__('data.seo_titles.bedroom_content', ['bedroom' => $beds_uae, 'type' => ($beds_uae == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $uae_lang]);
                            $title_lang     =   __('data.seo_titles.bedrooms.'.$beds_uae).__('data.seo_titles.bedroom_content', ['bedroom' => $beds_uae, 'type' => ($beds_uae == 0 ? '' : strtolower($property_type_lang)), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $uae_lang]);
                            $beds_uae       =   $beds_uae == 0 ? 'studio' : $beds_uae;

                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'name' ]   =   preg_replace('/\s+/', ' ', trim($content_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'title' ]  =   preg_replace('/\s+/', ' ', trim($title_lang));
                            $internal_links[ 'bedroom' ][ 'content' ][ $key ][ 'link' ]   =   kebab_case_custom( $base_url . '/' . $beds_uae . '-bedroom-'  . $property_type . '-for-' . $rent_sale );
                            $key++;
                        }
                    }
                }
            }
        }
        return $internal_links;
    }

}
