<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Alert extends User
{
    use \App\Traits\ApiCaller;

    /**
     * Display a listing of the resource.
     *
     * @param  string  $user_id, $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(...$vars)
    {
        $user       =   array_get($vars,'0',false);
        $model      =   array_get($vars,'1',false);
        $model_id   =   array_get($vars,'2',false);

        if($model){
            // List model and search alerts
            $this->api_endpoint     =   str_replace_array('#',[$user,$model],array_get($this->config,'api.endpoint.alert.list'));
        }
        else{
            //List all alerts
            $this->api_endpoint     =   str_replace_array('#',[$user],array_get($this->config,'api.endpoint.alert.all'));
        }
        return Parent::index($this->payload);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  string  $user_id, $model, $model_id
     * @return \Illuminate\Http\JsonResponse
     */
     public function store(Request $request, ...$vars)
     {
         $user       =   array_get($vars,'0',false);
         $model      =   array_get($vars,'1',false);
         $model_id   =   array_get($vars,'2',false);

         if($model){
             // Alert a model
             if($model_id){
                 $this->api_endpoint     =   str_replace_array('#',[$user,$model,$model_id],array_get($this->config,'api.endpoint.alert.model'));
             }
             // Alert a search
             else{
                 $this->api_endpoint     =   str_replace_array('#',[$user,$model],array_get($this->config,'api.endpoint.alert.search'));
                 $this->payload          =   $request->all();
             }
             return Parent::store($request);
         }
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $user_id, $model, $model_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, ...$vars)
    {
        $user       =   array_get($vars,'0',false);
        $model      =   array_get($vars,'1',false);
        $model_id   =   array_get($vars,'2',false);

        $this->api_endpoint     =   str_replace_array('#',[$user,$model,$model_id],array_get($this->config,'api.endpoint.alert.delete'));
        $this->payload          =   json_decode($this->request->get('query'), true);
        if(isset($this->payload['frequency']) && is_array($this->payload['frequency'])){
            $this->payload['frequency'] = $this->payload['frequency'][0];
        }
        return Parent::destroy($request);
    }
}
