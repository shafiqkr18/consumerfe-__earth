<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

class AwsServices extends BaseController
{

  /*
  |--------------------------------------------------------------------------
  | AWS S3 functions
  |--------------------------------------------------------------------------
  */
  /**
  * Get data from an S3 bucket
  * @param  [type] $bucket
  * @param  [type] $key
  * @return [type] S3 object
  */
  public function get_s3_object( $bucket, $key, $encryption = 'none' ){
    try {
      $files 	=	$this->connect_to_s3()->getObject([
        'Bucket' 				        =>		$bucket,
        'Key' 				          => 		$key,
        'ServerSideEncryption' 	=>		$encryption
      ]);
      return response()->json( [ $files->get('Body')->getContents() ], 200 );
    }
    catch( \Aws\S3\Exception\S3Exception $e) {
      return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
    }
  }

  /**
  * Get data from an S3 bucket
  * @param  [type] $bucket [description]
  * @param  [type] $key    [description]
  * @return [type]         [description]
  */
  public function get_all_s3_objects( $bucket, $prefix, $encryption = 'none' ){

    try {
      $files 	=	$this->connect_to_s3()->listObjects([
        'Bucket' 				           =>		$bucket,
        'Prefix'                   =>   $prefix,
        'ServerSideEncryption' 	   =>		$encryption
      ]);
      return $files->getPath( 'Contents' );
    }
    catch( \Aws\S3\Exception\S3Exception $e) {
      return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
    }
  }

  /**
  * Put Object in S3 bucket
  * @param  [type] $bucket        [description]
  * @param  [type] $key           [description]
  * @return [type] $file          [description]
  * @return [type] $encryption    [description]
  */
  public function put_s3_object( $bucket, $key, $file ){

    try {
      $this->connect_to_s3()->putObject([
        'Bucket' 					  =>		$bucket,
        'Key' 					    =>		$key,
        'SourceFile' 				=>		$file,
        'CacheControl'      =>    'max-age=7884000,public',
        'Expires'           =>    'Sun, 01 Jan 2034 00:00:00 GMT',
        'StorageClass'      =>    'REDUCED_REDUNDANCY',
        'ACL'               =>    'public-read'
      ]);

    } catch( \Aws\S3\Exception\S3Exception $e ) {
      return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
    }

  }
  /**
  * Delete all objects from bucket
  * @param  [type] $bucket        [description]
  */
  public function delete_all_s3_object_from_bucket( $bucket ){

    try {
      $client = $this->connect_to_s3();
      $batch = \Aws\S3\BatchDelete::fromListObjects($client, ['Bucket' => $bucket]);
      $batch->delete();
    } catch( \Aws\S3\Exception\S3Exception $e ) {
      return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
    }

  }

  /*
  |--------------------------------------------------------------------------
  | AWS SES functions
  |--------------------------------------------------------------------------
  */

  public function send_email( $to_addresses, $subject_data, $body_view, $body_data, $cc_addresses = array(), $bcc_addresses = array(), $reply_to_addresses = array(), $return_path = NULL, $source_arn = NULL, $return_path_arn = NULL ){

    $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.zoomtemplate'));

    // dd(config('mail.zoomtemplate'));
    // dd($to_addresses, $subject_data, $body_view, $body_data);
    try {

      $this->connect_to_ses()->sendEmail(array(
        'Source' => 'Zoom Property <hello@zoomproperty.com>',
        'Destination' => array(
          /**
          * ToAddresses is required
          * ToAddresses, CcAddresses, BccAddresses => array('address@domain.com') ** Array fields
          */
          'ToAddresses' => $to_addresses,
          'CcAddresses' => $cc_addresses,
          'BccAddresses' => $bcc_addresses
        ),
        'Message' => array(
          'Subject' => array(
            /**
            * Data is required
            * String
            */
            'Data' => $subject_data,
            'Charset' => 'UTF-8',
          ),
          'Body' => array(
            'Html' => array(
              /**
              * Data is required
              * $body_view --- String view path
              * $body_data --- Array view data
              */
              'Data' => $markdown->render($body_view, $body_data ),
              'Charset' => 'UTF-8',
            ),
          ),
        ),
        /**
        * Not required
        * ReplyToAddresses => array('address@domain.com') ** Array field
        * ReturnPath, SourceArn, ReturnPathArn ** String fields
        */
        'ReplyToAddresses' => $reply_to_addresses,
        // 'ReturnPath' => $return_path,
        // 'SourceArn' => $source_arn,
        // 'ReturnPathArn' => $return_path_arn,
      ));

      return response()->json( array( 'message' => 'email sent'), 200);

    }
    catch (\Aws\Ses\Exception\SesException $error) {
      //TODO send error email
    }
  }


  /*
  |--------------------------------------------------------------------------
  | AWS S3 connect
  |--------------------------------------------------------------------------
  */
  private function connect_to_s3(){
    try{
      return new \Aws\S3\S3Client([
        'region'        =>  config( 'aws.credentials.region' ),
        'version'       =>  'latest',
        'credentials'   =>  [
          'key'       =>  config( 'aws.credentials.key' ),
          'secret'    =>  config( 'aws.credentials.secret' )
        ]
      ]);
    }
    catch ( Aws\S3\Exception\S3Exception $e ) {
      return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
    }
  }
  /*
  |--------------------------------------------------------------------------
  | AWS SES connect
  |--------------------------------------------------------------------------
  */
  private function connect_to_ses(){
    try{
      return new \Aws\Ses\SesClient([
        'credentials' =>  [
          'key'     =>  config( 'aws.credentials.key' ),
          'secret'  =>  config( 'aws.credentials.secret' ),
        ],
        'region'  =>  config( 'aws.credentials.ses-region' ),
        'version' =>  'latest'
      ]);
    }
    catch ( \Exception $e ) {
      return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
    }
  }

}
