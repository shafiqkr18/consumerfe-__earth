<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Cache extends BaseController
{
  public function __construct(){

  }

  public function build($cache, $type = 'cache'){

    // Clean Cache
    $cache_directory    =   ($type == 'cache') ? 'cache/'.$cache : 'cache/'.$cache.'/'.$type;
    \Storage::deleteDirectory($cache_directory);
    \Storage::makeDirectory($cache_directory);


    if($cache == 'property'){
      /**
      * 1.   Get URLs to cache
      * 2.   Foreach url,
      *      2.1    Cache Properties
      *          2.1.1    Decode
      *          2.1.2    Property::Index()
      *          2.1.3    \Storage::put();
      *      2.2    Cache Featured Properties
      *          2.2.1
      */

      $urls   =   $this->urls_to_cache($cache,$type);

      foreach($urls as $url){

        if(!is_null($type) && $type == 'guide_me')
        {

          $area = key($url);
          $coordinates = explode(',',$url[$area]);

          $payload = [];
          $payload['size'] = 75;
          $payload['page'] = 1;
          $payload['area'] = $area;
          $payload['settings']['status'] = 1;
          $payload['meters'] = 500;
          $payload['coordinates']['lat'] = $coordinates[0];
          $payload['coordinates']['lng'] = $coordinates[1];
          $payload['rent_buy'] = ['Sale', 'Rent'];

          if(env('APP_ENV') === 'production')
          $payload['settings']['approved'] = 1;

          $request = new \Illuminate\Http\Request();
          $request->replace($payload);
          $properties_ctr =  new \App\Http\Controllers\Analytic($request);
          $properties =  $properties_ctr->guide_me_listings($cache);

          $results    =   is_array($properties) ? $properties : '';
          $data       =   var_export_min($results, true);
          \Storage::put($cache_directory.'/en/properties-for-rent-sale-in-'.strtolower(str_replace(' ','-',$area)).'-'.$payload['meters'].'.php','<?php return '.$data.'; ?>');

        }
        else {
          $payload    =   $this->decode_url_to_cache($url,$cache);
          $payload['settings']['status'] = 1;
          $payload['settings']['approved'] = true;
          foreach([1,2,3,4,5] as $randomizer){
            // 2.1
            //
            // 2.2
            if($type == 'featured'){
              //
              $payload['featured']['status'] = true;
              $payload['size'] = 10;
              $request = new \Illuminate\Http\Request();
              $request->replace($payload);
              $properties =   app('\App\Http\Controllers\Property')->set_payload($payload)->featured();
              $results    =   is_array($properties) ? $properties : $properties->getData(1);
              $data       =   var_export_min($results, true);
              // 2.3
              \Storage::put($cache_directory.'/'.$url.'.php','<?php return '.$data.'; ?>');
            }
            else{
              $properties =   app('\App\Http\Controllers\Property')->set_payload($payload)->index();
              $results    =   is_array($properties) ? $properties : $properties->getData(1);
              // 2.3
              $data       =   var_export_min($results, true);
              \Storage::put($cache_directory.'/'.$url.'-'.$randomizer.'.php','<?php return '.$data.'; ?>');
            }

          }
        }

      }

      return response()->json([ 'message' => 'Property '.$type.' cache built.' ], 200);
    }
    elseif($cache == 'project'){
      $payload    =   [];
      $developers =   collect(__( 'search.project.developers' ));
      $developers =   $developers->pluck('developers')->collapse()->pluck('value')->all();

      foreach([1,2,3] as $randomizer){
        $payload['settings']['status'] = 1;
        $payload['settings']['approved'] = true;
        $payload['developer']['_id'] = array_rand(array_flip($developers),9);
        $projects   =   app('\App\Http\Controllers\Project')->set_payload($payload)->index();
        $results    =   is_array($projects) ? $projects : json_decode((string)$projects->getBody(),1);
        $data       =   var_export_min($results, true);
        \Storage::put($cache_directory.'/new-projects-'.$randomizer.'.php','<?php return '.$data.'; ?>');
      }
      return response()->json([ 'message' => 'Project cache built.' ], 200);
    }
    elseif($cache == 'agent'){
      $payload    =   [];
      $payloads   =   [];
      $urls       =   $this->urls_to_cache($cache);

      foreach($urls as $url){
        $payloads[] = $this->decode_url_to_cache($url,$cache);
      }

      foreach([1,2,3,4,5] as $randomizer){
        $payload    =   array_random($payloads);
        $agents     =   app('\App\Http\Controllers\Agent')->set_payload($payload)->index();
        $results    =   is_array($agents) ? $agents : json_decode((string)$agents->getBody(),1);
        $data       =   var_export_min($results, true);
        if(array_get($results,'total',0) > 0){
          \Storage::put($cache_directory.'/agent-finder-'.$randomizer.'.php','<?php return '.$data.'; ?>');
        }
      }
      return response()->json([ 'message' => 'Agent cache built.' ], 200);
    }
    // dd($urls);
  }

  public function decode_url_to_cache($url,$cache){

    if($cache == 'property'){
      $parsed_url     =   [];

      $url_parts  =   explode('/',trim($url,'/'));
      // Ignoring Language
      array_shift( $url_parts );

      if( count( $url_parts ) == 1 ){
        // Only Rent/Buy expressed
        $fragments_url      =   "";

        $buckets_url        =   $url_parts;
      }
      else{
        // 1.   Fragments
        $fragments_url      =   array_last( $url_parts );

        // 2.   Buckets
        $buckets_url        =   array_splice( $url_parts, 0, -1 ); //dd( $buckets_url );
      }

      // Identify buckets
      $buckets_template   =   [ 'rent_buy', 'residential_commercial', 'city', 'area', 'building' ];
      // Assign buckets
      for( $i=0; $i<count($buckets_url); $i++ ){

        $query_field    =   $buckets_template[$i];
        $query_value    =   $buckets_url[$i];

        if( $query_field == 'rent_buy' ){
          if( $query_value === 'buy' )
          $query_value    =   'sale';
          array_set( $parsed_url, 'rent_buy', [title_case($query_value)] );

          if( str_contains( $fragments_url, 'residential' ) ){
            array_set( $parsed_url, 'residential_commercial', ['Residential'] );
          }
          else if( str_contains( $fragments_url, 'commercial' ) ){
            array_set( $parsed_url, 'residential_commercial', ['Commercial'] );
          }
        }

        if( $query_field == 'residential_commercial' ){
          array_set( $parsed_url, 'residential_commercial', [title_case($query_value)] );
        }

        if( $query_field == 'city' ){
          // array_set( $query, 'city', [title_case($query_value)] );
          $cityArr = explode( ' Or ', str_replace("-", " ", title_case( $query_value ) ) );
          array_set( $parsed_url, 'city', [$cityArr] );
        }

        if( $query_field == 'area' ){
          $suburbs    =   explode( "Or", title_case(str_replace("-", " ", $query_value)) );
          $inSuburb   =   [];
          foreach( $suburbs as $eachSuburb ){
            $inSuburb[] =   trim( $eachSuburb );
          }
          array_set( $parsed_url, 'area', $inSuburb );
        }

        if( $query_field == 'building' ){
          $suburbs    =   explode( "Or", title_case(str_replace("-", " ", $query_value)) );
          $inSuburb   =   [];
          foreach( $suburbs as $eachSuburb ){
            $inSuburb[] =   trim( $eachSuburb );
          }
          array_set( $parsed_url, 'building', $inSuburb );
        }

      }


      $query  =   'search/'.$fragments_url;

      /**
      * Isolating Consumer Email, Page and Sort
      */
      $remove_from_query  =   [];

      $remove_from_query  =   implode( "&", $remove_from_query );

      $query  =   str_replace( "?".$remove_from_query, "", $query );

      $query          =   preg_replace( "/search(.*)\//", "search/", $query );

      $url            =   str_replace( env('APP_URL'), "", $query );

      // 1-to-4-bedroom
      // -apartment
      // -for-rent-or-sale
      // -in-Dubai-Marina-or-JBR-area
      // -in-Marina-Gate-1-building
      // -in-dubai-city
      // -less-than-500000-aed
      // -greater-than-100000-aed
      // -with-1-to-5-bathroom
      // apartment-with-1-to-5-bathroom
      // 1-to-4-bedroom-apartment-for-rent-or-sale-in-Dubai-Marina-or-JBR-area-in-dubai-city-less-than-500000-aed-greater-than-100000-aed-with-1-to-5-bathroom

      preg_match( "/price-greater-than-(.*?)-aed/", $url, $match );
      if( count( $match ) > 0 ){
        $parsed_url[ 'price' ][ 'min' ]  =   (int)$match[1];
        // Trimmed Url
        $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
      }


      preg_match( "/price-less-than-(.*?)-aed/", $url, $match );
      if( count( $match ) > 0 ){
        $parsed_url[ 'price' ][ 'max' ]  =   (int)$match[1];
        // Trimmed Url
        $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
      }




      /**
      * Bedrooms
      */
      if( strpos( $url, "bedroom" ) ){
        $url        =   str_replace( "studio", "0", $url );
        preg_match( "/with-\d(.*)-bedroom/", $url, $match );
        if( count( $match ) > 0 ){
          $bath   =   str_replace( "with-", "", $match[0] );
        }
        else{
          preg_match( "/search\/\d(.*)-bedroom/", $url, $match );
          $bath   =   str_replace( "search/", "", $match[0] );
        }
        $bath   =   str_replace( "-bedroom", "", $bath );
        if( strpos( $bath, "-to-" ) ){
          $bath   =   explode( "-to-", $bath );
          $parsed_url[ 'bedroom' ][ 'min' ]   =   (int)$bath[0];
          $parsed_url[ 'bedroom' ][ 'max' ]   =   (int)$bath[1];
        }
        else{
          $parsed_url[ 'bedroom' ][ 'min' ]    =   (int)$bath;
          $parsed_url[ 'bedroom' ][ 'max' ]    =   (int)$bath;
        }
        // Trimmed Url
        $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        if( !strpos( $url, "search/" ) )
        $url    =  "search/".$url;
      }




      /**
      * Bathroom
      */
      if( strpos( $url, "bathroom" ) ){
        preg_match( "/with-\d(.*)-bathroom/", $url, $match );
        if( count( $match ) > 0 ){
          $bath   =   str_replace( "with-", "", $match[0] );
        }
        else{
          preg_match( "/search\/\d(.*)-bathroom/", $url, $match );
          $bath   =   str_replace( "search/", "", $match[0] );
        }
        $bath   =   str_replace( "-bathroom", "", $bath );
        if( strpos( $bath, "-to-" ) ){
          $bath   =   explode( "-to-", $bath );
          $parsed_url[ 'bathroom' ][ 'min' ]   =   (int)$bath[0];
          $parsed_url[ 'bathroom' ][ 'max' ]   =   (int)$bath[1];
        }
        else{
          $parsed_url[ 'bathroom' ][ 'min' ]    =   (int)$bath;
          $parsed_url[ 'bathroom' ][ 'max' ]    =   (int)$bath;
        }
        // Trimmed Url
        $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
      }




      /**
      * Suburbs
      */
      if( strpos( $url, "-area" ) ){
        preg_match( "/in-(.*)-area/", $url, $match );
        if( count( $match ) > 0 ){
          $suburbs     =   str_replace( "in-", "", $match[0] );
        }
        else{
          preg_match( "/search\/(.*)-area/", $url, $match );
          $suburbs     =   str_replace( "search/", "", $match[0] );
        }
        $suburbs        =   str_replace( "-area", "", $suburbs );

        if( strpos( $suburbs, "-or-" ) ){
          $parsed_url[ 'area' ]     =   explode( "-or-", $suburbs );
          foreach( $parsed_url['area'] as $k => $v ){
            $area = urldecode( trim(str_replace( "-", " ", $v )) );
            if(!in_array($area, array_get($parsed_url,'area',[]))){
              $parsed_url['area'][]   =   $area;
            }
          }
        }
        else{
          $area = urldecode( trim(str_replace( "-", " ", $suburbs )) );
          if(!in_array($area, array_get($parsed_url,'area',[]))){
            $parsed_url['area'][]   =   $area;
          }
        }
        // Trimmed Url
        $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
      }




      /**
      * Buildings
      */
      if( strpos( $url, "-building" ) ){

        if( preg_match('/in-(.*?)-building/', $url, $match) == 1){

          if( count( $match ) > 0 ){
            $buildings     =   str_replace( "in-", "", $match[0] );
          }
          else{
            preg_match( "/search\/(.*)-building/", $url, $match );
            $buildings     =   str_replace( "search/", "", $match[0] );
          }

          $buildings        =   str_replace( "-building", "", $buildings );

          if( strpos( $buildings, "-or-" ) ){
            $parsed_url[ 'building' ]     =   explode( "-or-", $buildings );
            foreach( $parsed_url['building'] as $k => $v ){
              $building = urldecode( trim(str_replace( "-", " ", $v )) );
              if(!in_array($building,array_get($parsed_url,'building',[]))) {
                $parsed_url['building'][]   =   $building;
              }
            }
          }
          else{
            $building = urldecode( trim(str_replace( "-", " ", $buildings )) );
            if(!in_array($building,array_get($parsed_url,'building',[]))) {
              $parsed_url['building'][]   =   $building;
            }
          }
          // Trimmed Url
          $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
        }

      }

      /**
      * City
      */
      if( strpos( $url, "-city" ) ){

        preg_match( "/in-(.*)-city/", $url, $match );

        if( count( $match ) > 0 ){
          $cities     =   str_replace_first( "in-", "", $match[0] );
        }
        else{
          preg_match( "/search\/(.*)-city/", $url, $match );
          $cities     =   str_replace( "search/", "", $match[0] );
        }
        $cities         =   str_replace( "-city", "", $cities );

        if( strpos( $cities, "-or-" ) ){
          $cities     =   explode( "-or-", $cities );
          foreach( $cities as $k => $v ){
            $possible_ct                =   title_case( trim(str_replace( "-", " ", $v )) );
            if(!in_array($possible_ct,array_get($parsed_url,'city',[]))){
              $parsed_url['city'][]   =   $possible_ct;
            }
          }
        }
        else{
          $possible_ct                =   title_case( trim(str_replace( "-", " ", $cities )) );
          if(!in_array($possible_ct,array_get($parsed_url,'city',[]))){
            $parsed_url['city'][]   =   $possible_ct;
          }
        }
        // Trimmed Url
        $url                        =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_replace( $match[0], "", $url ) ) ), "-");
      }




      /**
      * Rent or Sale
      */
      if( strpos( $url, "rent" ) || strpos( $url, "sale" ) ){
        if( strpos( $url, "for-" ) ){
          $start  =   "for-";
        }
        else{
          $start  =   "search\/";
        }
        if( strpos( $url, '-in' ) )
        $nearest_end['-in']         =   strpos( $url, '-in' );
        if( strpos( $url, "-with" ) )
        $nearest_end['-with']       =   strpos( $url, "-with" );

        if( isset( $nearest_end ) ){
          asort( $nearest_end, 1 );
          $end    =   array_keys( $nearest_end )[0];
          preg_match( "/".$start."(.*)".$end."/", $url, $match );
        }
        else{
          preg_match( "/".$start."(.*)/", $url, $match );
        }
        // dd( $match );
        $rent_buy       =   $match[1];
        // dd( $rent_buy );
        if( strpos( $rent_buy, "-or-" ) ){
          $rent_buy   =   explode( "-or-", $rent_buy );
          foreach( $rent_buy as $rentBuy ){
            $rentbuy = title_case( $rentBuy );
            if(!in_array($rentbuy,array_get($parsed_url, 'rent_buy', []))){
              $parsed_url[ 'rent_buy' ][] =   $rentbuy;
            }
          }
        }
        else{
          $rentbuy = title_case( $rent_buy );
          if(!in_array($rentbuy,array_get($parsed_url, 'rent_buy', []))){
            $parsed_url[ 'rent_buy' ][] =   $rentbuy;
          }
        }
        // Trimmed Url
        $url                            =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_ireplace( $match[0], "", $url ) ) ), "-");
      }




      // dd( $url, $parsed_url );
      /**
      * Property Type
      * @var [type]
      */
      if( strlen( $url ) > 7 ){
        preg_match( "/search\/(.*)/", $url, $match );
        if( count( $match ) > 0 ){
          $match[0]   =   str_replace( "search/", "", $match[0] );
          if( strpos( $match[0], "-or-" ) ){
            $ptypes     =   explode( "-or-", $match[0] );
            foreach( $ptypes as $ptype ){
              if($ptype!='properties'){
                if(str_contains($ptype,'residential')){
                  if(!in_array('Residential',array_get($parsed_url,'residential_commercial',[]))){
                    $parsed_url['residential_commercial'][]     =   'Residential';
                  }
                }
                else if(str_contains($ptype,'commercial')){
                  if(!in_array('Commercial',array_get($parsed_url,'residential_commercial',[]))){
                    $parsed_url['residential_commercial'][]     =   'Commercial';
                  }
                }
                else{
                  $parsed_url[ 'type' ][]    =   title_case( str_replace("-", " ", $ptype ) );
                }
              }
            }
          }
          else{
            if($match[0]!='properties'){
              if(str_contains($match[0],'residential')){
                if(!in_array('Residential',array_get($parsed_url,'residential_commercial',[]))){
                  $parsed_url['residential_commercial'][]     =   'Residential';
                }
              }
              else if(str_contains($match[0],'commercial')){
                if(!in_array('Commercial',array_get($parsed_url,'residential_commercial',[]))){
                  $parsed_url['residential_commercial'][]     =   'Commercial';
                }
              }
              else{
                $parsed_url[ 'type' ][]    =   title_case( str_replace("-", " ", $match[0] ) );
              }
            }
          }
          // Trimmed Url
          if( !empty( $match ) )
          $url                            =   trim( str_replace( "/-", "/", preg_replace( "/-+/", "-", str_ireplace( kebab_case($match[0]."-for"), "", $url ) ) ), "-");
        }
      }

      return $parsed_url;
    }
    if($cache == 'agent'){
      $segments   =   explode('/',$url);

      if(count($segments) > 3){
        $agency     =   array_get($segments,'3');
        $area       =   array_get($segments,'4');
        $payload    =   [];

        if($agency != 'all-agency'){
          array_set($payload,'agency._id',$agency);
        }

        if($area != 'all-suburbs'){
          $area   =   str_before($area, '-area');
          array_set($payload,'contact.areas',[$area]);
        }
      }
      return $payload;
    }
  }

  private function urls_to_cache($cache='property', $type = 'cache'){
    if($cache==='property'){
      if($type === 'guide_me'){
        return [
          ['Dubai' => '25.2081751,55.2720224'],
          ['Trade Center' => '25.2081751,55.2720224'],
          ['Dubai Marina' => '25.0805422,55.1403425'],
          ['International City' => '25.1648925,55.4084034'],
          ['Downtown Dubai' => '25.1949849,55.2784140'],
          ['Jumeirah Lakes Towers' => '25.0692834,55.1417222'],
        ];
      }
      else{
        return [
          // Generic Rent
          'en/rent/properties-for-rent',
          // Generic Sale
          'en/buy/properties-for-sale',
          // Generic Residential Rent
          'en/rent/residential/properties-for-rent',
          // Generic Residential Sale
          'en/buy/residential/properties-for-sale',
          // Stats driven URLs
          'en/rent/residential/apartment-for-rent-in-dubai-city',
          'en/buy/residential/villa-for-sale-in-dubai-city',
          'en/buy/residential/apartment-for-sale-in-dubai-city'
        ];
      }
    }
    if($cache=='agent'){
      return [
        'en/agents-search/uae/zb-forest-real-estate-12082/all-suburbs',
        'en/agents-search/uae/zb-tabani-real-estate-283/all-suburbs',
        'en/agents-search/uae/zb-real-choice-real-estate-brokers-llc-1068/all-suburbs',
        'en/agents-search/uae/zb-castles-plaza-real-estate-203/all-suburbs',
        'en/agents-search/uae/zb-hamptons-international-358/all-suburbs',
        'en/agents-search/uae/zb-obg-real-estate-broker-17196/all-suburbs',
        'en/agents-search/uae/all-agency/all-suburbs'
      ];
    }
  }

}
