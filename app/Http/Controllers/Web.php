<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class Web extends Controller{

    use \App\Traits\Utility;

    public function __construct( Request $request = null ){
        $this->request      =   $request;
        $this->page_name    =   collect(config('data.seo.pages'));
        // $this->setLocale();
    }

    public function privacy_policy(){
        $page_name  =   $this->page_name->where( 'id', 'privacy_policy' )->pluck('value')->get(0);
        $seo        =   $this->seo_content( $page_name );

        return view( 'pages.privacy_policy', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function real_choice(){
        $page_name  =   $this->page_name->where( 'id', 'real_choice' )->pluck('value')->get(0);
        $seo        =   $this->seo_content( $page_name );

        return view( 'pages.real_choice', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function scholarship(){
        $popular_links =   $this->popular_internal_links();
        $page_name  =   $this->page_name->where( 'id', 'scholarship' )->pluck('value')->get(0);
        $seo        =   $this->seo_content( $page_name );
        return view( 'pages.scholarship', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt',''),
        'popular_links'     =>  $popular_links
        ]);
    }

    public function budget_search(){
        $page_name  =   $this->page_name->where( 'id', 'budget_search_landing' )->pluck('value')->get(0);
        $seo        =   $this->seo_content( $page_name );

        return view( 'pages.budget.landing', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function guide_me(){
        return view( 'pages.guide_me' );
    }

    public function budget_search_results(){
        // ['results'=>app('\App\Http\Controllers\Property')->set_request($this->request)->index()->getData(1)];
        return view( 'pages.budget.listings',[
        'query'         =>  $this->request->all()
        ]);
    }

    public function budget_search_results_data(){
        array_set($payload,'budget',1);
        array_set($payload,'rent_buy',[str_replace('-',' ',$this->request->get('rent_buy','Sale') )]);
        array_set($payload,'residential_commercial',[$this->request->get('residential_commercial','Residential')]);
        array_set($payload,'price.min',(int)$this->request->get('price_min',10000));
        array_set($payload,'price.max',(int)$this->request->get('price_max',50000000));
        array_set($payload,'city',[title_case($this->request->get('emirate','Dubai'))]);
        #TODO implement pagination
        array_set($payload,'page',1);
        array_set($payload,'size',10);

        $results        =   app('\App\Http\Controllers\Property')->set_payload($payload)->index()->getData(true);

        // SEO Content
        $page_name      =   $this->page_name->where( 'id', 'budget_search_listings' )->pluck('value')->get(0);
        $seo            =   $this->seo_basic_content( $page_name, $payload );

        //Cache results
        if(isset($results['data'])){
        $refine         =   [];
        $properties     =   $results;
        }else{
        // Making UI Components
        if(!empty($results)){
            foreach($results as $area){

            $area           =   array_get($area,'area');
            $refine_array   =   [];
            $refine_array['area']       =   array_get($area,'name');
            $refine_array['total']      =   array_get($area,'total');
            foreach(array_get($area,'property',[]) as $property){
                // Beds
                $refine_array['bedroom'][]      =   array_get($property,'bedroom');
                // Baths
                $refine_array['bathroom'][]     =   array_get($property,'bathroom');
                // Properties
                $properties[]                   =   $property;
            }
            $refine_array['bedroom']        =   array_sort(array_unique($refine_array['bedroom']));
            $refine_array['bathroom']       =   array_sort(array_unique($refine_array['bathroom']));
            $refine[]   =   $refine_array;
            }

            // Sort refinements by properties
            $refine     =   collect($refine)->sortByDesc('total');
        }
        else{
            $refine         =   [];
            $properties     =   [];
        }
        }

        // Return View
        return view( 'pages.budget.listings.listings', [
        'query'         =>  $payload,
        'refinements'   =>  $refine,
        'properties'    =>  $properties,
        'seo_image_alt' =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function t_and_c(){
        $page_name  =   $this->page_name->where( 'id', 'terms_and_conditions' )->pluck('value')->get(0);
        $seo        =   $this->seo_content( $page_name );

        return view('pages.terms_and_conditions', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function landing_page(){
        $popular_links    =   $this->popular_internal_links();
        $page_name        =   $this->page_name->where( 'id', 'home' )->pluck('value')->get(0);
        $seo              =   $this->seo_content( $page_name );
        $takeover         =   app('\App\Http\Controllers\Takeover')->index();
        $takeover         =   json_decode($takeover->getBody(),1);
        // $projects         =   app('\App\Http\Controllers\Project')->set_payload(['featured' => ['status' => true],'size' => 3])->index();
        $projects         =   app('\App\Http\Controllers\Project')->set_payload(['featured' => ['status' => true]])->index();
        $projects         =   json_decode($projects->getBody(),1);
        $projects         =   array_slice(array_get($projects, 'data'), 12, 3);
        $publicityvideos  =   app('\App\Http\Controllers\PublicityVideo')->index();
        $publicityvideos  =   json_decode($publicityvideos->getBody(),1);
        return view('pages.landing', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt',''),
        'locale'            =>  \App::getLocale(),
        'popular_links'     =>  $popular_links,
        'projects'          =>  $projects,
        'publicityvideos'    =>  $publicityvideos,
        'takeover'          => (array_has($takeover, 'data') && is_array(array_get($takeover, 'data'))) ? array_get($takeover, 'data.0') : $takeover
        ]);
    }

    public function property_search_results(){
        $query              =   app('\App\Http\Controllers\Property')->create_payload($this->request);
        $internal_links     =   app('\App\Http\Controllers\Property')->internal_links($query);
        $results            =   app('\App\Http\Controllers\Property')->set_request($this->request)->index();
        $featured           =   app('\App\Http\Controllers\Property')->set_request($this->request)->featured();
        $results            =   is_array($results) ? $results : $results->getData(1);
        $featured           =   is_array($featured) ? $featured : $featured->getData(1);
        if(array_has($query,'city')){
        array_set($project_payload,'city',array_get($query,'city'));
        }
        if(array_has($query,'area')){
        array_set($project_payload,'area',array_get($query,'area'));
        array_set($agents_payload,'contact.areas',array_get($query,'area'));
        }
        if(array_has($query,'building')){
        array_set($project_payload,'name',array_get($query,'building'));
        }
        array_set($project_payload,'featured.status',true);
        array_set($project_payload,'size',1);
        array_set($agents_payload,'featured.status',true);
        array_set($agents_payload,'size',1);
        $agents             =   app('\App\Http\Controllers\Agent')->set_payload($agents_payload)->index();
        $agent              =   json_decode((string)$agents->getBody(),1);
        $projects           =   app('\App\Http\Controllers\Project')->set_payload($project_payload)->index();
        $project            =   json_decode((string)$projects->getBody(),1);
        if(array_has($query,'price.min') && array_get($query,'price.min') == '2010'){
        array_forget( $query, 'price.min' );
        }

        //FAVOURITE INTEGRATION
        if( \Session::has('user')){
        $favourites = $this->all_user_fav_properties();
        if(count(array_get($favourites,'data',[])) > 0){
            foreach(array_get($favourites,'data') as $favourite){
            // $key_result = array_search(array_get($favourite, 'property._id'), array_column(array_get($results,'data'), '_id'));
            $key_result = !empty(array_get($results,'data',[])) ? array_search(array_get($favourite, 'property._id'), array_column(array_get($results,'data'), '_id')) : false;
            if($key_result !== false){
                array_set($results,'data.'.$key_result.'.favourite',true);
            }
            // $key_featured = array_search(array_get($favourite, 'property._id'), array_column(array_get($featured,'data'), '_id'));
            $key_featured = !empty(array_get($featured,'data', [])) ? array_search(array_get($favourite, 'property._id'), array_column(array_get($featured,'data'), '_id')) : false;
            if($key_featured !== false){
                array_set($featured,'data.'.$key_featured.'.favourite',true);
            }
            }
        }
        }

        /**
        *  S E O
        */
        $page_name          =   $this->page_name->where( 'id', 'property_listings' )->pluck('value')->get(0);
        $allowed_parameters =   collect(config('data.seo.parameters.'.$page_name));
        $allowed_parameters =   $allowed_parameters->pluck('value')->toArray();
        if(array_has($query,'keyword') && array_has($query,'keyword_in_path')){
        array_push($allowed_parameters,'keyword');
        array_push($allowed_parameters,'keyword_in_path');
        }
        $query              =   array_filter(array_only($query,array_keys(array_undot(array_flip($allowed_parameters)))));
        $request            =   $this->property_search_request($query);

        // Remove bedroom range from query when equal
        if(array_has($request,'bedroom.min') && array_has($request,'bedroom.max')){
        if(array_get($request,'bedroom.min',0) === array_get($request,'bedroom.max',0)){
            array_forget( $request, 'bedroom.max' );
        }
        }
        if(array_has($request, 'featured.status')){
        array_forget( $request, 'featured.status' );
        array_set( $request,'keyword', 'featured');
        array_set( $request,'keyword_in_path',1);
        }

        // SEO Content
        $parameters         =   array_keys($request);
        $seo                =   $this->seo_content( $page_name, $parameters, $request, 'property' );

        // Redirection
        if(count($request) == 1 && array_has($request,'rent_buy')){
        $new_request['rent_buy'][]                =   title_case( array_get($request,'rent_buy', 'rent') );
        $new_request['residential_commercial'][]  =   'Residential';

        return redirect( $this->property_search_url_encode( $new_request ) );
        }

        $page[ 'current' ]  =   1;
        $page[ 'total' ]    =   intdiv(array_get($results,'total',0),20);

        // Page Tracking
        $allowed_urls       =   __( 'urls' );
        $allowed_urls       =   array_flip($allowed_urls);
        $url_from_request   =   $this->property_search_url_encode( $query );
        $current_url        =   $this->request->path();

        $block_filters      =   [ 'bathroom.min', 'bathroom.max', 'dimension.builtup_area.min', 'dimension.builtup_area.max' ];

        // Range Filters
        $dotted_request     =   array_dot($request);
        $range_request      =   $range_count        =   [];
        foreach( $dotted_request as $k => $v ){
        $suffix = substr(strrchr($k, "."), 1);
        if( $suffix == 'min' || $suffix == 'max' ){
            if( preg_match('/(.*?).'.$suffix.'/', $k, $match) == 1 ){
            $range_request[$match[1]][$suffix]   =  $v;
            }
        }
        }
        foreach($range_request as $key => $range){
        $range_count[]  =   count($range);
        }

        $is_trackable       =   isset($allowed_urls[$current_url]) ? true : false;
        $is_trackable       =   url($current_url) == $url_from_request ? true : $is_trackable;
        $is_trackable       =   count(array_intersect(array_keys($dotted_request), array_values($block_filters))) > 0 ? false : $is_trackable;
        $is_trackable       =   count($range_count) > 0 && max($range_count) > 1 ? false : $is_trackable;

        Session::put('property_search_results',$results);

        if(!empty($this->request->query())){
        $trackable_query_string =   array_except($this->request->query(),'page');
        $is_trackable           =   !empty($trackable_query_string) ? false : $is_trackable;
        $page[ 'current' ]      =   (int)array_get($this->request->query(),'page',1);
        }

        $page_prefix        =   '';
        if(array_get($page,'total',0) != 0 && array_get($page,'current') > 1){
        $page_prefix          =   'Page '.array_get($page,'current').' of '.array_get($page,'total').' | ';
        }

        return view('pages.property.listings', [
        'query'             =>  $query,
        'request'           =>  $this->request,
        'internal_links'    =>  $internal_links,
        'agent'             =>  $agent,
        'project'             =>  $project,
        'is_trackable'      =>  $is_trackable,
        'locale'            =>  \App::getLocale(),
        'results'           =>  $results,
        'featured'          =>  $featured,
        'seo_title'         =>  $page_prefix.array_get($seo,'seo_title',''),
        'seo_description'   =>  $page_prefix.array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt',''),
        'seo_article'       =>  array_get($seo,'seo_article',[]),
        'seo_links'         =>  array_get($seo,'seo_links',[])
        ]);
    }

    public function property_details(...$var){

        if(Session::has('property_search_results')){
        $properties = Session::get('property_search_results');
        } else {
        $properties = array( 'total' => 0 );
        }

        // Get Details
        $details        =   app('\App\Http\Controllers\Property')->show(end($var));
        $details        =   json_decode((string)$details->getBody(),1);

        array_set($stats_payload,'area',array_get($details,'area'));
        array_set($stats_payload,'rent_buy',array_get($details,'rent_buy'));
        array_set($stats_payload,'type',array_get($details,'type'));
        $stats       =   app('\App\Http\Controllers\Analytic')->set_payload($stats_payload)->guide_me_stats('property');
        $stats       =   json_decode((string)$stats->getBody(),1);
        
        $price_per_month = array_get( $stats, 'type.0.rent_buy.0.trends.price.price_per_month');
        $month = date('F');
        $oldMonth = 0;
        $newMonth = 0;
        $percent = 0;
        // UNCOMMENT FOR PRDUCTION
        // foreach ($price_per_month as $price) {
        //     if (array_get($price, 'date') == $month) {
        //         if ($oldMonth == 0) {
        //             $oldMonth = array_get($price, 'price');
        //         } else {
        //             $newMonth = array_get($price, 'price');
        //         }
        //     }
        // }
        // REMOVE IN PRODUCTION
        $oldMonth = array_get($price_per_month, '0.price');
        $newMonth = array_get($price_per_month, '2.price');
        // END REMOVE

        if ($oldMonth > 0 && $newMonth > 0) {
            $percent = (($newMonth - $oldMonth)/$oldMonth)*100;
            $percent = sprintf("%.2f%%", $percent);
        }
        array_set($stats, 'type.0.rent_buy.0.percent', $percent);
        
        // SEO Content
        $page_name      =   $this->page_name->where( 'id', 'property_details' )->pluck('value')->get(0);

        $parameters     =   collect(config('data.seo.parameters.'.$page_name));
        $parameters     =   $parameters->pluck('value')->toArray();

        if(array_get($details,'bathroom') == 0 || array_get($details,'bathroom') == -1){
        array_forget($parameters, array_get($details,'bathroom'));
        }

        if(array_get($details,'bedroom') == -1){
        array_forget($parameters, array_get($details,'bedroom'));
        }

        $seo            =   $this->seo_content( $page_name, $parameters, $details );

        if( !empty(array_get($details,'_id')) ){
        if( \Session::has('user')){
            array_set($details,'favourite',false);
            $favourites = $this->all_user_fav_properties();
            if(count(array_get($favourites,'data',[])) > 0){
            $key = array_search(end($var), array_column(array_column(array_get($favourites,'data'), 'property'), '_id'));
            if($key !== false){
                array_set($details,'favourite',true);
            }
            }
        }
        
        return view('pages.property.details', [
            'data'              =>  $details,
            'properties'        =>  $properties,
            'stats'             =>  $stats,
            'seo_title'         =>  array_get($seo,'seo_title',''),
            'seo_description'   =>  array_get($seo,'seo_description',''),
            'seo_h1'            =>  array_get($seo,'seo_h1',''),
            'seo_h2'            =>  array_get($seo,'seo_h2',''),
            'seo_h3'            =>  array_get($seo,'seo_h3',''),
            'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
        }
        else{
        $request['rent_buy'][]                =   title_case( str_replace( 'buy', 'Sale', $this->request->segment( '2' ) ) );
        $request['residential_commercial'][]  =   title_case( $this->request->segment( '3' ) );

        return redirect( $this->property_search_url_encode( $request ) );
        }
    }

    public function services(){
        $service_types  =   app('\App\Http\Controllers\Service')->types();
        $service_types  =   json_decode((string)$service_types->getBody(),1);
        $data           =   array_get($service_types,'data',[]);
        $total          =   array_get($service_types,'total',0);

        $page_name      =   $this->page_name->where( 'id', 'services_landing' )->pluck('value')->get(0);
        $seo            =   $this->seo_content( $page_name );

        return view('pages.services.landing', [
        'data'              =>  $data,
        'total'             =>  $total,
        'locale'            =>  \App::getLocale(),
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function services_listings(...$var){
        $service        =   app('\App\Http\Controllers\Service')->set_payload(['type'=>['_id'=>[end($var)]]])->index();
        $service        =   json_decode((string)$service->getBody(),1);
        $service_type   =   collect(array_get($service, 'data'))->pluck('type.name')->get(0);

        //SEO
        $page_name      =   $this->page_name->where( 'id', 'services_listings' )->pluck('value')->get(0);
        $total          =   array_get($service,'total',0);
        $query_data     =   [
        'total'         =>  $total,
        'service.name'  =>  $service_type
        ];

        $query_data     =   array_filter($query_data, 'strlen');
        $parameters     =   array_keys($query_data);

        $seo            =   $this->seo_content( $page_name, $parameters, $query_data );

        return view('pages.services.listings', [
        'service'           =>  $service_type,
        'data'              =>  $service,
        'locale'            =>  \App::getLocale(),
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function agent_finder(){
        $page_name  =   $this->page_name->where( 'id', 'agent_landing' )->pluck('value')->get(0);
        $seo        =   $this->seo_content( $page_name );

        return view('pages.agent.landing', [
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function agent_finder_listings(){
        $page_name  =   $this->page_name->where( 'id', 'agent_listings' )->pluck('value')->get(0);

        $segments   =   $this->request->segments();
        $query      =   $this->request->query();
        $parameters =   $query_data =   [];


        if(count($segments) > 3){
        $agency     =   array_get($segments,'3');
        $area       =   array_get($segments,'4');
        $payload    =   [];

        if($agency != 'all-agency'){
            array_set($payload,'agency._id',$agency);
        }

        if($area != 'all-suburbs'){
            $area   =   str_before($area, '-area');
            array_set($payload,'contact.areas',[$area]);
        }
        }

        if(!empty($query)){
        foreach($query as $key => $value){
            if($key == 'keyword'){
            array_set($payload,$key,$value);
            }
        }
        }

        $page   =   array_get($query,'page', 1);
        array_set($payload, 'size', 20);
        array_set($payload, 'page', $page);

        $agents_payload = array_except($payload,['keyword','page']);
        array_set($agents_payload,'featured.status',true);
        array_set($agents_payload,'size',1);
        $agents             =   app('\App\Http\Controllers\Agent')->set_payload($agents_payload)->index();
        $f_agent            =   json_decode((string)$agents->getBody(),1);

        $data = app('\App\Http\Controllers\Agent')->set_payload($payload)->index();
        $data = json_decode((string)$data->getBody(),1);

        $projects         =   app('\App\Http\Controllers\Project')->set_payload(['featured' => ['status' => true],'size' => 3])->index();
        $projects         =   json_decode($projects->getBody(),1);

        if(array_get($data,'total', 0) == 0){
        $agents =   [];
        foreach([1,2,3,4,5] as $random){
            if(\Storage::exists('cache/agent/agent-finder-'.$random.'.php')){
            $agents     =   include( storage_path( 'cache/agent/agent-finder-'.$random.'.php' ) );
            }
        }
        $size           =   array_get($agents,'total', 0) < 10 ? array_get($agents,'total', 0) : 10;
        $data['data']   =   array_random(array_get($agents,'data',[]), $size);
        $data['total']  =   0;
        }

        //SEO
        $total          =   array_get($data,'total',0);
        $agency_name    =   !empty($agency) && $agency != 'all-agency' ? array_get($data,'data.0.agency.contact.name','') : '';
        $area           =   !empty($area) && $area != 'all-suburbs' ? str_replace('-',' ',$area) : '';

        $query_data     =   [
        'total'         =>  $total,
        'agency_name'   =>  $agency_name,
        'area'          =>  $area
        ];

        $query_data     =   array_filter($query_data, 'strlen');
        $parameters     =   array_keys($query_data);

        $seo            =   $this->seo_content( $page_name, $parameters, $query_data );

        return view('pages.agent.listings', [
        'data'              =>  $data,
        'featured_agent'    =>  $f_agent,
        'projects'          =>  $projects,
        'segments'          =>  $segments,
        'locale'            =>  \App::getLocale(),
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function agent_finder_details(...$var){
        $data           =   app('\App\Http\Controllers\Agent')->show(end($var));
        $data           =   json_decode((string)$data->getBodY(),1);

        array_set($reviews_payload,'agent_id',end($var));
        $reviews        =   app('\App\Http\Controllers\AgentRatings')->set_payload($reviews_payload)->index();
        $reviews        =   json_decode($reviews->getBody(),1);
        
        // SEO Content
        $page_name      =   $this->page_name->where( 'id', 'agent_details' )->pluck('value')->get(0);

        $parameters     =   collect(config('data.seo.parameters.'.$page_name));
        $parameters     =   $parameters->pluck('value')->toArray();
        $content        =   array_filter($data);
        if(array_has($content,'contact.about') && empty(array_get($content,'contact.about'))){
        array_forget($content, 'contact.about');
        $parameters = array_diff($parameters, array('contact.about'));
        }
        $seo            =   $this->seo_content( $page_name, $parameters, $content );

        return view('pages.agent.details', [
        'data'              =>  $data,
        'reviews'           =>  $reviews,
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function all_developers(Request $request,...$var){
        $payload        =   [];
        $country        =   array_get( $var, 1, '' );
        $developer      =   array_get( $var, 2, '' );

        $developer_name =   collect(__( 'search.project.developers' ))->pluck('developers')->collapse()->where( 'value', $developer )->pluck('name')->get(0);

        if(isset($request) && !empty($request->query())){
        $queries    =   $request->query();
        foreach($queries as $key => $query){
            if($key === 'sort'){
            $sort = explode('-', $query);
            if($sort[0] === 'price'){
                $payload['sort']['pricing.starting']['order'] = $sort[1];
            }
            }
            else{
            if($key == 'status' || $key == 'country' || $key == 'city' || $key == 'area'){
                $payload[$key][] = $query;
            }
            else{
                $payload[$key] = $query;
            }
            }
        }
        }

        if( !empty($developer) ){
        $payload['developer']['_id']    =  $developer;
        }

        //TODO make abbreviations dynamic country wise
        if( !empty($country) && in_array($country,[ 'uae', 'international' ]) ){
        if($country == 'uae'){
            $payload['country']    =  ['United Arab Emirates'];
        }
        else{
            $payload['exclude']['country']    =  ['United Arab Emirates'];
        }
        }

        $data           =   app('\App\Http\Controllers\Project')->set_payload($payload)->index();

        //SEO
        $page_name      =   $this->page_name->where( 'id', 'project_listings' )->pluck('value')->get(0);
        $parameters     =   $query_data     =   [];

        if($data->getStatusCode() == 200){
        $data                           =   json_decode((string)$data->getBody(),1);
        $query_data['developer_name']   =   $developer_name;
        $query_data['total']            =   array_get($data,'total',0);
        }

        //SEO
        $query_data     =   array_filter($query_data, 'strlen');
        $parameters     =   array_keys($query_data);
        $seo            =   $this->seo_content( $page_name, $parameters, $query_data );

        if(array_get($data,'total', 0) == 0){
        $projects           =   [];
        $randomizer         =   array_random([1,2,3]);
        if(\Storage::exists('cache/project/new-projects-'.$randomizer.'.php')){
            $projects           =   include( storage_path( 'cache/project/new-projects-'.$randomizer.'.php' ) );
        }
        $size               =   array_get($projects,'total', 0) < 10 ? array_get($projects,'total', 0) : 10;
        $data['data']       =   array_random(array_get($projects,'data',[]), $size);
        $data['total']      =   0;
        }

        return view('pages.projects.listings', [
        'data'              =>  $data,
        'request'           =>  $this->request,
        'country'           =>  !empty($country) ? title_case($country) : '',
        'developer'         =>  !empty($developer) ? $developer_name : '',
        'locale'            =>  \App::getLocale(),
        'query'             =>  end($var),
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function all_developers_details(...$var){
        $data       =   app('\App\Http\Controllers\Project')->show(end($var));
        $data       =   json_decode((string)$data->getBody(),1);

        $agencies   =   collect( config( 'agency.email_ids' ) );
        $agency     =   $agencies->where( 'dev', array_get( $data, 'developer._id' ) )->pluck('id')->get(0);

        $country    =   array_get( $data, 'country', '' );
        $country    =   $country == 'United Arab Emirates (UAE)' ? 'UAE' : 'International';

        //properties by developers
        $p_p        =   ['page' => 1, 'size' => 20];
        array_set($p_p, 'agent.agency._id', array_get($data,'developer._id'));
        array_set($p_p, 'settings.status', 1);
        array_set($p_p, 'settings.approved', true);
        array_set($p_p, 'size',3);
        $properties =   app('\App\Http\Controllers\Property')->set_payload($p_p)->index();
        $properties =   $properties->getData(1);

        // SEO Content
        $page_name      =   $this->page_name->where( 'id', 'project_details' )->pluck('value')->get(0);
        // $parameters     =   collect(config('data.seo.parameters.'.$page_name));
        // $parameters     =   $parameters->pluck('value')->toArray();
        $parameters     =   [end($var)];
        $seo            =   $this->seo_content( $page_name, $parameters, $data );

        return view('pages.projects.details', [
        'data'              =>  $data,
        'agency'            =>  $agency,
        'country'           =>  $country,
        'properties'         =>  $properties,
        'locale'            =>  \App::getLocale(),
        'seo_title'         =>  array_get($seo,'seo_title',''),
        'seo_description'   =>  array_get($seo,'seo_description',''),
        'seo_h1'            =>  array_get($seo,'seo_h1',''),
        'seo_h2'            =>  array_get($seo,'seo_h2',''),
        'seo_h3'            =>  array_get($seo,'seo_h3',''),
        'seo_image_alt'     =>  array_get($seo,'seo_image_alt','')
        ]);
    }

    public function all_favourites_user(...$vars){
        if(str_contains(array_get($vars, 0), ['en', 'ar'])){
        array_forget($vars, 0);
        $vars = array_values($vars);
        }
        $model     =   array_get($vars,1, false);
        $subtypes  =   json_decode($this->request->get('query'), true)['subtype'];
        $subtype   = array_search('search', $subtypes);
        if($subtype !== false && $model == 'property'){
        $data = app('\App\Http\Controllers\Favourite')->index(...$vars);
        $data = json_decode((string)$data->getBody(),1);
        return view('components.cards.favourite_search', ['favResults'=>$data]);
        }
        elseif(!$subtype && $model == 'property'){
        $data = app('\App\Http\Controllers\Favourite')->index(...$vars);
        $data = json_decode((string)$data->getBody(),1);
        return view('components.cards.favourite_property', ['favResults'=>$data]);
        }
    }

    public function all_alerts_user(...$vars){
        if(str_contains(array_get($vars, 0), ['en', 'ar'])){
        array_forget($vars, 0);
        $vars = array_values($vars);
        }
        $data = app('\App\Http\Controllers\Alert')->index(...$vars);
        $data = json_decode((string)$data->getBody(),1);
        return view('components.cards.user_alerts', ['alerts'=>$data]);
    }

    private function all_user_fav_properties(){
        $payload['query'] = json_encode(['subtype'=>['model']]);
        $request = new \Illuminate\Http\Request();
        $request->replace($payload);
        $fav_ctr = new \App\Http\Controllers\Favourite($request);
        $favourite = $fav_ctr->index(\Session::get('user._id'),'property');
        return json_decode((string)$favourite->getBody(),1);
    }

    private function seo_basic_content($page, $query = []){

            $seo_title          =   null;
            $seo_description    =   null;
            $seo_h1             =   null;
            $seo_h2             =   null;
            $seo_h3             =   null;
            $seo_image_alt      =   null;
            $is_residential     =   false;
            $is_studio          =   false;
            $is_bedroom         =   false;

            if($page == 'property_listings'){

                $all_property_types =   $this->property_types();
                $is_residential     =   array_get( $query, 'residential_commercial', '' ) == 'Residential' ? true : false;

                if( !empty( $query ) ){

                    $keyword    =   array_has( $query, 'keyword' ) ? title_case(str_replace('-',' ',array_get( $query, 'keyword' ))) : '';
                    $keyword    =   (\App::getLocale() !== 'en' && \Lang::has('data.keyword.'.snake_case_custom($keyword), \App::getLocale())) ? __( 'data.keyword.'.snake_case_custom($keyword)) : $keyword;

                    if( !array_has( $query, 'rent_buy' ) ){
                    return [ $seo_title,$seo_description,$seo_h1,$seo_h2,$seo_h3,$seo_image_alt ];
                    }
                    else{

                    $rb     =   array_get( $query, 'rent_buy', 'Rent or Buy' );
                    $rb     =   str_replace('-',' ',$rb);
                    $rs     =   (\App::getLocale() !== 'en') ? (\Lang::has('data.rent_buy.for_'.snake_case_custom($rb), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rb)) : 'for_'.ucfirst($rb) : 'for '.ucfirst($rb);
                    $rb     =   $rb == "Sale" ? "Buy" : $rb;

                    $rb     =   (\App::getLocale() !== 'en' && \Lang::has('data.rent_buy.'.snake_case_custom($rb), \App::getLocale())) ? __( 'data.rent_buy.'.snake_case_custom($rb)) : ucfirst($rb);

                    // Residential / Commercial
                    $rc     =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom(array_get( $query, 'residential_commercial', 'Residential and Commercial' )), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom(array_get( $query, 'residential_commercial', 'Residential and Commercial' ))) : array_get( $query, 'residential_commercial', 'Residential and Commercial' );

                    if( array_has( $query, 'type' ) && isset($all_property_types[array_get( $query, 'type', '' )]) ){
                        $rc     =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom(array_get( $query, 'residential_commercial', '' )), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom(array_get( $query, 'residential_commercial', '' ))) : array_get( $query, 'residential_commercial', '' );
                    }
                    else if( array_has( $query, 'type' ) ){
                        $rc     =   '';
                    }

                    // Property Type
                    $type       =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom(array_get( $query, 'type')), \App::getLocale())) ? __( 'data.type.'.snake_case_custom(array_get( $query, 'type'))) : array_get( $query, 'type', __( 'page.global.properties') );
                    $pt         =   str_replace( '-', ' ', $type );

                    // Building
                    if( array_get( $query, 'building', false ) ){
                        $b          =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom(array_get( $query, 'building', '')), \App::getLocale())) ? __( 'data.building.'.snake_case_custom(array_get( $query, 'building', ''))) : array_get( $query, 'building', '');
                        $a          =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom(array_get( $query, 'area', '' )), \App::getLocale())) ? __( 'data.area.'.snake_case_custom(array_get( $query, 'area', '' ))) : array_get( $query, 'area', '' );
                        if( is_array( $b ) ) {
                        $b = join( $b, ' & ' );
                        }
                        /*if( is_array( $a ) ) {
                        $a = join( $a, ' & ' );
                    }*/
                    $ar_str = $b;
                    }
                    else{
                    $ac         =   array_has($query, 'area') ? 'area' : 'city';
                    $c          =   array_get( $query, 'area', array_get( $query, 'city', __( 'page.global.uae') ) );
                    $ac         =   (\App::getLocale() !== 'en' && \Lang::has('data.'.$ac.'.'.snake_case_custom($c), \App::getLocale())) ? __( 'data.'.$ac.'.'.snake_case_custom($c)) : $c;
                    $ar_str     =   $ac;
                    }

                    if( is_array( $ar_str ) ) {
                    $ar         =   join( $ar_str, ' & ' );
                    } else {
                    $ar         =   $ar_str;
                    }

                    $ct         =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom(array_get( $query, 'city', '' )), \App::getLocale())) ? __( 'data.city.'.snake_case_custom(array_get( $query, 'city', '' ))) : array_get( $query, 'city', '' );

                    if( is_array( $ct ) ) {
                    $ct = join( $ct, ' & ' );
                    }

                    // Bedroom
                    $bd         =   '';
                    $bd_count   =   array_get( $query, 'bedroom.min' );

                    if( array_has( $query, 'bedroom.min' ) || array_has( $query, 'bedroom.max' ) ){

                    $studio     =   (\App::getLocale() !== 'en' && \Lang::has('page.global.'.snake_case_custom('studio'), \App::getLocale())) ? __( 'page.global.'.snake_case_custom('studio')) : 'Studio';

                    if(array_has( $query, 'bedroom.min' )){
                        $bd_min     =   array_get( $query, 'bedroom.min', '' ) == 0 ? $studio : array_get( $query, 'bedroom.min', '' );
                        $is_studio  =   array_get( $query, 'bedroom.min', '' ) == 0 ? true : false;
                    }
                    if(array_has( $query, 'bedroom.max' )){
                        $bd_max     =   array_get( $query, 'bedroom.max', '' ) == 0 ? $studio : array_get( $query, 'bedroom.max', '' );
                        $is_studio  =   array_get( $query, 'bedroom.min', '' ) == 0 ? true : false;
                    }

                    if( isset($bd_min) && isset($bd_max) ){
                        $bd     =   ( $bd_min == $bd_max ) ? $bd_min : $bd_min.' '. __( 'page.global.to'). ' '.$bd_max;
                    }
                    elseif( isset($bd_min) && !isset($bd_max) ){
                        $bd     =   $bd_min;
                        $is_bedroom = true;
                    }
                    elseif( !isset($bd_min) && isset($bd_max) ){
                        $bd     =   $bd_max;
                        $is_bedroom = true;
                    }
                    if($is_bedroom && \App::getLocale() == 'ar'){
                        $bd         =   \Lang::has('data.seo_titles.bedrooms', \App::getLocale()) ? __( 'data.seo_titles.bedrooms.'.$bd) : $bd;
                    }
                    else{
                        $bd         =   $bd.( $is_studio ? '' : ' '.trans_choice( __( 'page.global.bedroom'), 1));
                    }
                    }

                }

                // SEO Title
                $seo_title_stub     =   \Lang::has('data.seo_titles.seo_title_stub', \App::getLocale()) ? __( 'data.seo_titles.seo_title_stub') : ':rent_buy: :bedroom: :residential_commercial: :property_type: : :bedroom: :residential_commercial: :property_type: :rent_sale: :in: :area:';
                $seo_title_stub     =   $is_residential ? str_replace(':residential_commercial:','',$seo_title_stub) : $seo_title_stub;
                $seo_title_stub     =   $is_studio ? str_replace(':property_type:','',$seo_title_stub) : $seo_title_stub;
                $seo_title          =   str_replace( ":keyword:", $keyword, str_replace( ':bedroom:', $bd, str_replace( ":in:", __( 'page.global.in'), str_replace( ":residential_commercial:", $rc, str_replace( ":rent_sale:", $rs, str_replace( ":rent_buy:", $rb, str_replace( ":property_type:", $pt, str_replace( ":area:", $ar, $seo_title_stub ) ) ) ) ) ) ) );

                // SEO Description
                $description_stub   =   \Lang::has('data.seo_titles.description_stub', \App::getLocale()) ? __( 'data.seo_titles.description_stub') :  " : Zoom Property is the best & largest real estate website in :area:. Providing a huge range of :residential_commercial: :property_type: :rent_sale: in :area:. If you are searching for a :property_type: in :area:, you will find everything on our property portal.";
                $seo_description    =   array_get( explode( ":", $seo_title ), '0', '' ) . $description_stub;
                $seo_description    =   str_replace( ":rent_sale:", $rs, str_replace( ":residential_commercial:", $rc, str_replace( ":rent_buy:", $rb, str_replace( ":property_type:", $pt, str_replace( ":area:", $ar, $seo_description ) ) ) ) );

                // Image Alt
                $seo_image_alt_stub =   \Lang::has('data.seo_titles.seo_image_alt_stub', \App::getLocale()) ? __( 'data.seo_titles.seo_image_alt_stub') : ":keyword: :residential_commercial: :property_type: :rent_sale: in :area:, :rent_buy: :residential_commercial: :property_type: in :area:";
                $seo_image_alt      =   str_replace( ":keyword:", $keyword, str_replace( ":residential_commercial:", $rc, str_replace( ":rent_sale:", $rs, str_replace( ":rent_buy:", $rb, str_replace( ":property_type:", $pt, str_replace( ":area:", $ar, $seo_image_alt_stub ) ) ) ) ) );

                // H1
                $seo_h1_stub        =   \Lang::has('data.seo_titles.seo_h1_stub', \App::getLocale()) ? __( 'data.seo_titles.seo_h1_stub') : ":keyword: :bedroom: :residential_commercial: :property_type: :rent_sale: in :area:";
                $seo_h1_stub        =   $is_residential ? str_replace(':residential_commercial:','',$seo_h1_stub) : $seo_h1_stub;
                $seo_h1_stub        =   $is_studio ? str_replace(':property_type:','',$seo_h1_stub) : $seo_h1_stub;
                $seo_h1             =   str_replace( ":keyword:", $keyword, str_replace( ":bedroom:", $bd, str_replace( ":residential_commercial:", $rc, str_replace( ":rent_sale:", $rs, str_replace( ":property_type:", $pt, str_replace( ":area:", $ar, $seo_h1_stub ) ) ) ) ) );

                // H2
                $seo_h2_stub        =   \Lang::has('data.seo_titles.seo_h2_stub', \App::getLocale()) ? __( 'data.seo_titles.seo_h2_stub') : ":rent_buy: :bedroom: :residential_commercial: :property_type: in :area:";
                $seo_h2             =   str_replace( ":bedroom:", $bd, str_replace( ":residential_commercial:", $rc, str_replace( ":rent_buy:", $rb, str_replace( ":property_type:", $pt, str_replace( ":area:", $ar, $seo_h2_stub ) ) ) ) );

                // H3
                $seo_h3_stub        =   \Lang::has('data.seo_titles.seo_h3_stub', \App::getLocale()) ? __( 'data.seo_titles.seo_h3_stub') : "Find :property_type: in :area:";
                $seo_h3             =   str_replace( ":property_type:", $pt, str_replace( ":area:", $ar, $seo_h3_stub ) );

            }
        }
        elseif($page == 'property_details'){
            $data       =   $query;
            $seo_title  =   str_replace_array( '?', [
            array_get( $data, 'writeup.title', '' ),
            array_get( $data, 'area', '' ),
            array_get( $data, 'city', '' ),
            array_get( $data, 'ref_no', '' ),
            ], '?, ?, ? - Ref # ?' );

            if(str_contains( array_get( $data, 'rent_buy', false ), 'Commercial' ) ){
            $seo_description    =   str_replace_array( '?', [
                array_get( $data, 'writeup.title', '' ),
                array_get( $data, 'rent_buy', '' ),
                array_get( $data, 'building', array_get( $data, 'area', '' ) ),
                str_contains( array_get( $data, 'rent_buy', '' ), 'Rent' ) ? 'Rent' : 'Buy',
                array_get( $data, 'type', '' ),
                array_get( $data, 'price', '' ),
                array_get( $data, 'writeup.title', '' ),
                array_get( $data, 'dimension.builtup_area', '' ),
                array_get( $data, 'type', '' ),
                array_get( $data, 'rent_buy', '' ),
                array_get( $data, 'area', '' ),
                array_get( $data, 'name', array_get( $data, 'area', '' ) )
            ], '? for ? in ? - ? ? in AED ?, ? having ? sqft. ? is available for ? in ?, ?.' );
            }
            elseif( array_get( $data, 'bedroom', 0 ) == 0){
            $seo_description    =   str_replace_array( '?', [
                array_get( $data, 'writeup.title', '' ),
                array_get( $data, 'rent_buy', '' ),
                array_get( $data, 'building', array_get( $data, 'area', '' ) ),
                str_contains( array_get( $data, 'rent_buy', '' ), 'Rent' ) ? 'Rent' : 'Buy',
                array_get( $data, 'type', '' ),
                array_get( $data, 'price', '' ),
                array_get( $data, 'writeup.title', '' ),
                array_get( $data, 'bathroom', '' ),
                array_get( $data, 'dimension.builtup_area', '' ),
                array_get( $data, 'type', '' ),
                array_get( $data, 'rent_buy', '' ),
                array_get( $data, 'area', '' ),
                array_get( $data, 'name', array_get( $data, 'area', '' ) )
            ], '? for ? in ? - ? ? in AED ?, ? with ? bathrooms and having ? sqft. ? is available for ? in ?, ?.' );
            }
            else{
            $seo_description    =   str_replace_array( '?', [
                array_get( $data, 'writeup.title', '' ),
                array_get( $data, 'rent_buy', '' ),
                array_get( $data, 'building', array_get( $data, 'area', '' ) ),
                str_contains( array_get( $data, 'rent_buy', '' ), 'Rent' ) ? 'Rent' : 'Buy',
                array_get( $data, 'type', '' ),
                array_get( $data, 'price', '' ),
                array_get( $data, 'writeup.title', '' ),
                array_get( $data, 'bedroom', '' ),
                array_get( $data, 'bathroom', '' ),
                array_get( $data, 'dimension.builtup_area', '' ),
                array_get( $data, 'type', '' ),
                array_get( $data, 'rent_buy', '' ),
                array_get( $data, 'area', '' ),
                array_get( $data, 'building', array_get( $data, 'area', '' ) )
            ], '? for ? in ? - ? ? in AED ?, ? with ? bedrooms, ? bathrooms and having ? sqft. ? is available for ? in ?, ?.' );
            }

            $seo_h1     =   array_get( $data, 'writeup.title', '' );
        }
        elseif($page == 'budget_search'){
            $seo_title  =   str_replace( ":price_max:", array_get( $query, 'price_max', 0 ), str_replace( ":rent_buy:", array_get( $query, 'rent_buy', 'Rent' ), __( 'page.budget.seo_title' ) ) );
        }
        elseif($page == 'project_listings'){
            $seo_title  =   'New Projects Search | '.__( 'page.global.seo_title' );
            if(array_get($query,'total',0) != 0){
            $seo_h1     =   array_get($query,'total',0). ' ' .
            trans_choice( __( 'page.project.listings.new_projects' ), array_get($query,'total',0));
            }
            else{
            $seo_h1     =   __( 'page.results.no_results_found' );
            }
        }
        elseif($page == 'project_details'){
            $data               =   $query;
            $seo_title          =   array_has(__( 'seo.project'), array_get($data,'_id',''))  ? __( 'seo.project.'.array_get($data,'_id','').'.title') : array_get($data,'name',__( 'page.global.seo_title' ));
            $seo_description    =   array_has(__( 'seo.project'), array_get($data,'_id',''))  ? __( 'seo.project.'.array_get($data,'_id','').'.description') : array_get($data,'name',__( 'page.global.seo_description' ));
            $seo_h1             =   (\App::getLocale() !== 'en' && array_get($data,'writeup.title_'.\App::getLocale(),'') != '') ? array_get($data,'writeup.title_'.\App::getLocale()) : array_get($data,'writeup.title','');
        }
        elseif($page == 'agent_landing'){
            $seo_h1     =   __( 'page.sections.banner.agent.title' );
        }
        elseif($page == 'agent_listings'){
            $data       =   $query;
            //TODO uncomment data when needed
            if(array_get($query,'total',0) != 0){
            $seo_h1     =   '<span class="counter listings_total" data-listings_total="'.array_get($data,'total','').'">'. (is_numeric(array_get($data,'total','Many'))?array_get($data,'total','Many'):0) .'</span> '
            //.array_get($data,'agency_name','').' '
            . ' ' .trans_choice( __( 'page.agent.details.agent_found' ), array_get($data,'total',0) )
            //.(!empty(array_get($data,'area','')) ? ' in '.array_get($data,'area','') : '' )
            ;
            }
            else{
            $seo_h1     =   __( 'page.results.no_results_found' );
            }
        }
        elseif($page == 'services_landing'){
            $seo_h1     =   __( 'page.sections.banner.service.title' );
        }
        elseif($page == 'services_listings'){
            $data       =   $query;
            $seo_h1     =   array_get($data,'total','Many').' '.trans_choice( __( 'page.services.listings.results_found' ), array_get($data,'total',3) );
        }
        elseif($page == 'real_choice'){
            $seo_title  =   __('real_choice.title');
        }
        elseif($page == 'terms_and_conditions'){
            $seo_h1     =   __( 'terms_conditions.h1' );
        }
        elseif($page == 'privacy_policy'){
            $seo_h1     =   __( 'terms_conditions.h1' );
        }

        $seo_title              =   preg_replace('/\s+/', ' ', trim($seo_title));
        $seo_description        =   preg_replace('/\s+/', ' ', trim($seo_description));
        $seo_h1                 =   preg_replace('/\s+/', ' ', trim($seo_h1));
        $seo_h2                 =   preg_replace('/\s+/', ' ', trim($seo_h2));
        $seo_h3                 =   preg_replace('/\s+/', ' ', trim($seo_h3));
        $seo_image_alt          =   preg_replace('/\s+/', ' ', trim($seo_image_alt));

        //Default Content
        if(empty($seo_title)){
            $seo_title          =   __( 'page.global.seo_title' );
        }
        if(empty($seo_description)){
            $seo_description    =   __( 'page.global.seo_description' );
        }
        if(empty($seo_image_alt)){
            $seo_image_alt      =   __( 'page.global.seo_image_alt' );
        }

        return [
            'seo_title'         =>  $seo_title,
            'seo_description'   =>  $seo_description,
            'seo_h1'            =>  $seo_h1,
            'seo_h2'            =>  $seo_h2,
            'seo_h3'            =>  $seo_h3,
            'seo_image_alt'     =>  $seo_image_alt
        ];
    }

    private function seo_dynamic_content($page, $parameters = [], $data = [], $model = ''){

        $seo_title          =   null;
        $seo_description    =   null;
        $seo_h1             =   null;
        $seo_h2             =   null;
        $seo_h3             =   null;
        $seo_image_alt      =   null;
        $seo_article        =   null;
        $seo_links          =   null;

        $payload['page_name']       =   $page;
        $payload['seo_parameters']  =   $parameters;
        if(!empty($model)){
            $payload['model']       =   $model;
            $payload[$model]        =   array_undot($data);
        }
        $template                   =   $this->get_seo_details($payload);

        if(!empty($template)){
            $locale                 =   \App::getLocale();

            //Get SEO article and it's metadata if exists and merge with generic SEO metadata template
            $seo_article            =   array_get($template,'article', []);
            $seo_article_metadata   =   array_filter(array_get($seo_article,'metadata',[]));
            if(!empty($seo_article)){
            array_forget($seo_article,'metadata');
            }

            $seo_links  =   array_get($template,'links', []);

            $template   =   array_merge($template,$seo_article_metadata);

            /**
            * SEO title
            */
            if($locale !== 'en' && array_has($template,'seo_title_'.$locale) && array_get($template,'seo_title_'.$locale) != ''){
            $seo_title  =   array_get($template,'seo_title_'.$locale);
            }
            elseif($locale == 'en' && array_has($template,'seo_title') && !empty(array_get($template,'seo_title'))){
            $seo_title  =   array_get($template,'seo_title');
            }

            /**
            * SEO description
            */
            if($locale !== 'en' && array_has($template,'seo_description_'.$locale) && array_get($template,'seo_description_'.$locale) != ''){
            $seo_description  =   array_get($template,'seo_description_'.$locale);
            }
            elseif($locale == 'en' && array_has($template,'seo_description') && !empty(array_get($template,'seo_description'))){
            $seo_description  =   array_get($template,'seo_description');
            }

            /**
            * SEO h1
            */
            if($locale !== 'en' && array_has($template,'seo_h1_'.$locale) && array_get($template,'seo_h1_'.$locale) != ''){
            $seo_h1  =   array_get($template,'seo_h1_'.$locale);
            }
            elseif($locale == 'en' && array_has($template,'seo_h1') && !empty(array_get($template,'seo_h1'))){
            $seo_h1  =   array_get($template,'seo_h1');
            }

            /**
            * SEO h2
            */
            if($locale !== 'en' && array_has($template,'seo_h2_'.$locale) && array_get($template,'seo_h2_'.$locale) != ''){
            $seo_h2  =   array_get($template,'seo_h2_'.$locale);
            }
            elseif($locale == 'en' && array_has($template,'seo_h2') && !empty(array_get($template,'seo_h2'))){
            $seo_h2  =   array_get($template,'seo_h2');
            }

            /**
            * SEO h3
            */
            if($locale !== 'en' && array_has($template,'seo_h3_'.$locale) && array_get($template,'seo_h3_'.$locale) != ''){
            $seo_h3  =   array_get($template,'seo_h3_'.$locale);
            }
            elseif($locale == 'en' && array_has($template,'seo_h3') && !empty(array_get($template,'seo_h3'))){
            $seo_h3  =   array_get($template,'seo_h3');
            }

            /**
            * SEO image alt
            */
            if($locale !== 'en' && array_has($template,'seo_image_alt_'.$locale) && array_get($template,'seo_image_alt_'.$locale) != ''){
            $seo_image_alt  =   array_get($template,'seo_image_alt_'.$locale);
            }
            elseif($locale == 'en' && array_has($template,'seo_image_alt') && !empty(array_get($template,'seo_image_alt'))){
            $seo_image_alt  =   array_get($template,'seo_image_alt');
            }
        }

        $return = [
            'seo_title'         =>  $seo_title,
            'seo_description'   =>  $seo_description,
            'seo_h1'            =>  $seo_h1,
            'seo_h2'            =>  $seo_h2,
            'seo_h3'            =>  $seo_h3,
            'seo_image_alt'     =>  $seo_image_alt,
            'seo_article'       =>  $seo_article,
            'seo_links'         =>  $seo_links
        ];

        return $return;
    }

    private function seo_content($page, $seo_parameters = [], $data = [], $model = ''){
        /**
         *  1.  Get dynamic content
        *  2.  Get basic content
        *  3.  Merge basic content with dynamic content and get template
        *  4.  Get data to be substituted
        *  5.  Translate SEO template to content (en)
        *  6.  Remove duplicates concatenated by & or and (eg. plots & plots for sale)
        *  7.  Translate English content to other languages
        *  8.  Sanitize data by removing consecutive duplicate words and angular brackets
        */

        $locale                     =   \App::getLocale();

        #   1
        $seo_dynamic_content        =   array_filter($this->seo_dynamic_content($page, $seo_parameters, $data, $model));
        $seo_article                =   array_get($seo_dynamic_content, 'seo_article', []);
        array_forget($seo_dynamic_content, 'seo_article');

        $seo_links                  =   array_get($seo_dynamic_content, 'seo_links', []);
        array_forget($seo_dynamic_content, 'seo_links');

        #   2
        $seo_basic_content          =   array_filter($this->seo_basic_content($page,$data));

        #   3
        $contents                   =   array_merge($seo_basic_content,$seo_dynamic_content);
        $translation_content        =   $contents;

        #   4
        $data                       =   array_dot($data);

        if(array_has($data,'type')){
            $property_type          =   array_get($data,'type');

            $property_types_plural  =   $this->property_types(true);
            array_set($data,'type_plural', array_get($property_types_plural,$property_type,$property_type));

            $property_types_alias   =   $this->property_type_alias();
            array_set($data,'type_alias', array_get($property_types_alias,$property_type,$property_type));

            $property_types_alias_plural    =   $this->property_type_alias(true);
            array_set($data,'type_alias_plural', array_get($property_types_alias_plural,$property_type,$property_type));
        }

        if(array_has($data,'keyword')){
            array_set($data, 'keyword', title_case(str_replace('-',' ',array_get($data, 'keyword'))));
        }

        #   5
        foreach($contents as $key => $template){
            if (preg_match_all("/<<(.*?)>>/", $template, $m)){
            if(!empty($m[1])){
                foreach($m[1] as $i => $varname){

                /* Buy */
                if($varname == 'rent_sale'){
                    $varname = 'rent_buy';
                }
                elseif($varname == 'rent_buy'){
                    $myValue    =   array_get($data,sprintf('%s', $varname) ) == 'Sale' ? 'Buy' : array_get($data,sprintf('%s', $varname));
                    $varname    =   'rent_sale';
                    array_set($data, 'rent_sale', $myValue);
                }
                $rent_sale  =   snake_case_custom(array_get($data,'rent_buy',''));
                $rent_buy   =   $rent_sale == 'Sale' ? 'Buy' : $rent_sale ;

                if($varname == 'for_rent_sale'){
                    array_set($data, 'for_rent_sale', 'for_'.$rent_sale);
                }
                elseif($varname == 'for_rent_buy'){
                    array_set($data, 'for_rent_buy', 'for_'.$rent_buy);
                }
                if($varname == 'commercial'){
                    $myValue    =   array_get($data,sprintf('%s', 'residential_commercial') ) == 'Commercial' ? array_get($data,sprintf('%s', 'residential_commercial')) : '';
                    array_set($data, 'commercial', $myValue);
                }
                if(array_has($data,sprintf('%s', $varname))){
                    $value = $data[sprintf('%s', $varname)];
                    /* Studio */
                    if(in_array($varname,['bedroom.min','bedroom.max','bedroom']) && $value == 0 ){
                    $value = 'Studio';
                    }
                    $contents[$key] = str_replace($m[0][$i], $value, $contents[$key]);
                    /* rent_buy value for Arabic */
                    // if(in_array($value,['Rent','Buy','Sale', 'Short Term Rent'])){
                    //     $value = 'for '.$value;
                    // }
                    if(is_int($value)){
                    $translation_content[$key] = str_replace($m[0][$i], $value, $translation_content[$key]);
                    }
                    else{
                    $translation_content[$key] = str_replace($m[0][$i], '<<'.snake_case_custom($value).'>>', $translation_content[$key]);
                    }
                    $translation_content[$key] = str_replace('<<>>', '', $translation_content[$key]);

                    // Replace Studio bedroom to Studio
                    if($value == 'Studio'){
                    preg_match('/Studio \b(bed\w+)\b/i', $contents[$key], $match );
                    $contents = preg_replace(array('/Studio \b(bhk)\b/i'), 'Studio', $contents);
                    $contents[$key] = !empty($match[1]) ? str_replace($match[1],'',$contents[$key]) : $contents[$key];
                    $translation_content[$key] = !empty($match[1]) ? str_replace($match[1],'',$translation_content[$key]) : $translation_content[$key];
                    }
                }
                }
            }
            }
        }

        foreach($contents as $key => $template){
            $contents[$key] = preg_replace('/\s+/', ' ',$contents[$key]);
            $translation_content[$key] = preg_replace('/\s+/', ' ',$translation_content[$key]);
            $contents[$key] = preg_replace(array('/Studio \b(properties)\b/i'), 'Studio', $contents[$key]);
        }

        #   6
        if(!empty($model) && $model == 'property'){
            foreach($contents as $key => $content){
            $finds   =   ["&","and"];
            foreach($finds as $find){
                if(preg_match("!(\S+)\s*$find\s*(\S+)!i", $content, $match)){
                if(!empty($match[1]) && !empty($match[2])){
                    if($match[1]==$match[2]){
                    $contents[$key] = str_replace($match[1].' '.$find.' '.$match[2], $match[1], $content);
                    }
                }
                }
                if(preg_match("!(\S+\s*\S+)\s*$find\s*(\S+\s*\S+)!i", $content, $match)){
                if(!empty($match[1]) && !empty($match[2])){
                    if($match[1]==$match[2]){
                    $contents[$key] = str_replace($match[1].' '.$find.' '.$match[2], $match[1], $content);
                    }
                }
                }
                if(preg_match("!(\S+\s*\S+\s*\S+)\s*$find\s*(\S+\s*\S+\s*\S+)!i", $content, $match)){
                if(!empty($match[1]) && !empty($match[2])){
                    if($match[1]==$match[2]){
                    $contents[$key] = str_replace($match[1].' '.$find.' '.$match[2], $match[1], $content);
                    }
                }
                }
            }
            }
        }

        #   7
        if($locale !== 'en'){
            $translations = array_dot(__('data'));
            $translations = array_combine(array_map(function($key){ return substr($key, strrpos($key, '.') + 1); }, array_keys($translations)),array_values($translations));
            foreach($translation_content as $key => $template){
            if (preg_match_all("/<<(.*?)>>/", $template, $m)){
                if(!empty($m[1])){
                foreach($m[1] as $i => $varname){
                    if(array_has($translations,sprintf('%s', $varname))){
                    $translation_content[$key] = str_replace($m[0][$i], $translations[sprintf('%s', $varname)], $translation_content[$key]);
                    }
                }
                }
            }
            }
            $contents   =   $translation_content;
        }

        #   8
        $seo_title          =   !str_contains(array_get($contents,'seo_title',''),['<<','>>']) ? array_get($contents,'seo_title','') : array_get($seo_basic_content,'seo_title','');
        $seo_title          =   preg_replace('/\s+/', ' ',$seo_title);
        if(strlen($seo_title) > 65){
            $seo_title          =   trim(array_get( explode( ":", $seo_title ), '1', $seo_title ));
        }

        $seo_description    =   !str_contains(array_get($contents,'seo_description',''),['<<','>>']) ? array_get($contents,'seo_description','') : array_get($seo_basic_content,'seo_description','');
        $seo_description    =   preg_replace('/\s+/', ' ',$seo_description);
        if(strlen($seo_description) > 160){
            $seo_description    =   trim(array_get( explode( ":", $seo_description ), '1', $seo_description ));
        }

        $seo_h1             =   !str_contains(array_get($contents,'seo_h1',''),['<<','>>']) ? array_get($contents,'seo_h1','') : array_get($seo_basic_content,'seo_h1','');
        $seo_h1             =   preg_replace('/\s+/', ' ',$seo_h1);

        $seo_h2             =   !str_contains(array_get($contents,'seo_h2',''),['<<','>>']) ? array_get($contents,'seo_h2','') : array_get($seo_basic_content,'seo_h2','');
        $seo_h2             =   preg_replace('/\s+/', ' ',$seo_h2);

        $seo_h3             =   !str_contains(array_get($contents,'seo_h3',''),['<<','>>']) ? array_get($contents,'seo_h3','') : array_get($seo_basic_content,'seo_h3','');
        $seo_h3             =   preg_replace('/\s+/', ' ',$seo_h3);

        $seo_image_alt      =   !str_contains(array_get($contents,'seo_image_alt',''),['<<','>>']) ? array_get($contents,'seo_image_alt','') : array_get($seo_basic_content,'seo_image_alt','');
        $seo_image_alt      =   preg_replace('/\s+/', ' ',$seo_image_alt);

        $contents = [
            'seo_title'         =>  $seo_title,
            'seo_description'   =>  $seo_description,
            'seo_h1'            =>  $seo_h1,
            'seo_h2'            =>  $seo_h2,
            'seo_h3'            =>  $seo_h3,
            'seo_image_alt'     =>  $seo_image_alt,
            'seo_article'       =>  $seo_article,
            'seo_links'         =>  $seo_links
        ];

        return $contents;
    }

    private function property_search_request($query){
        $dotted_request     =   array_dot($query);
        $request            =   [];

        foreach( $dotted_request as $k => $v ){
            if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 ){
            $request[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  str_replace('-',' ',$v);
            array_forget( $dotted_request, $k );
            }
            else{
            $request[ $k ]   =  $v;
            }
        }

        foreach($request as $key => $val){
            if(is_array($val)){
            $val = array_unique($val);
            array_set( $request, $key, implode( $val, ' & ' ));
            }
        }

        return $request;
    }

    public function get_seo_details($payload){
        array_set($payload,'active',1);
        $seo_response   =   app('\App\Http\Controllers\Seo')->set_payload($payload)->get_details();
        $details        =   $seo_response->getStatusCode() == 200 ? json_decode((string)$seo_response->getBody(),1) : [];
        return $details;
    }

    public function microsites(...$var){
        $slug   =   ( array_has($var,1) && !empty(array_get($var,1)) ) ? array_get($var,1) : '';

        $payload['developer']['slug']   =   $slug;
        $payload['active']              =   1;
        $request    =    new \Illuminate\Http\Request();
        $request->replace($payload);
        $microsite  =    new \App\Http\Controllers\Microsite($request);
        $microsite  =    $microsite->get_details();

        if($microsite->getStatusCode() == 200 ){
            $microsite  =   json_decode((string)$microsite->getBody(),1);

            $projects   =   array_get($microsite, 'projects', []);
            $project    =   ( array_has($var,2) && !empty(array_get($var,2)) ) ? array_get($var,2) : array_random($projects);

            $data       =   app('\App\Http\Controllers\Project')->show($project);
            if($data->getStatusCode() == 200 ){
            $data       =   json_decode((string)$data->getBody(),1);
            return view('pages.microsites', [
                'data'      =>  $data,
                'microsite' =>  $microsite,
                'locale'    =>  \App::getLocale()
            ]);
            }
        }
        else{
            return redirect('/');
        }
    }

    public function popular_internal_links(){
        $links_data     =   include( storage_path( 'search/new-internal-links.php' ) );
        $links_data     =   $links_data[ 'residential_commercial' ];
        $locale         =   \App::getLocale();
        $env_url        =   env( 'APP_URL' );
        $base_url       =   $env_url . $locale;
        $uae_lang       =   __('page.global.uae');

        $internal_links =   [];
        $max_listings   =   20;
        $rc             =   [ 'residential', 'commercial' ];
        $links          =   config( 'data.seo.internal_links.home' );
        $ptypes         =   collect($links)->pluck('property_type')->unique()->toArray();
        $ptypes         =   array_diff($ptypes, array('properties'));
        $j              =   1;

        foreach($links as $key => $value){

            $count                  =   0;
            $rent_sale              =   array_get($value,'rent_sale','');
            $rent_sale_for_lang     =   (\Lang::has('data.rent_buy.for_'.snake_case_custom($rent_sale), \App::getLocale())) ? __( 'data.rent_buy.for_'.snake_case_custom($rent_sale)) : $rent_sale;
            $property_type          =   strtolower(array_get($value,'property_type'));
            $label                  =   snake_case(array_get($value,'label',''));
            if($property_type == 'properties'){
            $property_type_lang     =   (\App::getLocale() !== 'en' && \Lang::has('page.global.'.snake_case_custom($property_type), \App::getLocale())) ? __( 'page.global.'.snake_case_custom($property_type)) : str_plural($property_type);
            }
            else{
            $property_type_lang     =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom($property_type), \App::getLocale())) ? __( 'data.type.'.snake_case_custom($property_type)) : str_plural($property_type);
            }

            // $label_lang             =   __('data.seo_titles.property_type.nav_content', ['type' => title_case($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang)]);
            $label_lang             =   __('data.seo_titles.property_type.nav_menu', ['type' => title_case($property_type_lang)]);
            $title_lang             =   __('data.seo_titles.property_type.nav_title', ['type' => title_case($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'loc' => $uae_lang]);
            $rent_buy               =   ( $rent_sale == 'sale' ) ? 'buy' : $rent_sale;

            foreach($rc as $residential_commercial){
            $i                               =   0;
            $residential_commercial_lang     =   (\App::getLocale() !== 'en' && \Lang::has('data.residential_commercial.'.snake_case_custom($residential_commercial), \App::getLocale())) ? __( 'data.residential_commercial.'.snake_case_custom($residential_commercial)) : $residential_commercial;
            if($property_type == 'properties' ){
                $contents    =   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.types', [] );
                $count      +=   array_get( $links_data, $residential_commercial . '.rent_sale.' . $rent_sale . '.count', 0 );
                $contents    =   array_except($contents,$ptypes);
            }
            else{
                $contents[ $property_type ]    =   array_get( $links_data, $residential_commercial.'.rent_sale.'.$rent_sale.'.types.'.$property_type );
                $count      +=   array_get( $links_data, $residential_commercial.'.rent_sale.'.$rent_sale.'.types.'.$property_type.'.count' );
            }

            $current_base_url       =   kebab_case_custom($base_url . '/' . $rent_buy . '/' . $residential_commercial);

            $menu_label = $residential_commercial == 'commercial' ? $residential_commercial_lang : $label_lang;

            $internal_links[ 'menu' ][ $menu_label ][ $rent_buy ][ 'label' ]  =   $label_lang;
            $internal_links[ 'menu' ][ $menu_label ][ $rent_buy ][ 'title' ]  =   $title_lang;
            $internal_links[ 'menu' ][ $menu_label ][ $rent_buy ][ 'link' ]   =   $label;
            $internal_links[ 'menu' ][ $menu_label ][ $rent_buy ][ 'count' ]  =   $count;

            $cities = [];

            foreach($contents as $type => $content){

                $i          =   0;
                $cities     =   array_get($content,'cities',[]);
                $property_type_lang     =   (\App::getLocale() !== 'en' && \Lang::has('data.type.'.snake_case_custom($type), \App::getLocale())) ? __( 'data.type.'.snake_case_custom($type)) : str_plural($type);

                if(!empty($cities)){

                $internal_links[ 'quick_access' ][ $rent_buy ][0]['city'] = __( 'page.landing.other');
                $internal_links[ 'quick_access' ][ $rent_buy ][0]['property_type'] = __( 'page.global.emirate');

                $areas = [];
                $internal_links[ 'content' ][ $label_lang ][ $rent_buy ][ $residential_commercial ] = [];
                foreach($cities as $city => $city_areas){
                    $cities_count       =   array_get($city_areas, 'count');

                    if($cities_count > $max_listings){
                    $city_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.city.'.snake_case_custom($city), \App::getLocale())) ? __( 'data.city.'.snake_case_custom($city)) : $city;

                    $title_city_lang    =   __('data.seo_titles.property_type.'.$residential_commercial.'.nav_title', ['type' => title_case($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'residential_commercial' => title_case($residential_commercial_lang), 'loc' => $city_lang]);

                    $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'label' ]          =   $city_lang;
                    $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'title' ]          =   $title_city_lang;
                    $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'link' ]           =   kebab_case_custom( $current_base_url . '/' . $type . '-for-' . $rent_sale . '-in-' . $city. '-city' );
                    $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'count' ]          =   $cities_count;

                    $i++;

                    $areas  =   array_get($city_areas,'areas',[]);

                    if(!empty($areas)){
                        #new internal links top section
                        if(($residential_commercial == 'residential') && ($property_type == 'apartment' || $property_type == 'villa') ){
                        if((strtolower($city) == 'dubai' || strtolower($city) == 'abu dhabi') || (strtolower($city) == 'sharjah' && $property_type == 'apartment')){
                            $internal_links[ 'quick_access' ][ $rent_buy ][$j]['city'] = $city_lang;
                            $internal_links[ 'quick_access' ][ $rent_buy ][$j]['property_type'] = $label_lang;
                        }
                        else if($property_type == 'apartment'){
                            $internal_links[ 'quick_access' ][ $rent_buy ][0]['areas'][$j]['label'] = $city_lang;
                            $internal_links[ 'quick_access' ][ $rent_buy ][0]['areas'][$j]['link'] = kebab_case_custom( $current_base_url . '/' . $type . '-for-' . $rent_sale . '-in-' . $city. '-city' );
                        }
                        }
                        $buildings = [];
                        $k  =   0;

                        foreach($areas as $area => $area_buildings){
                        $areas_count        =   array_get($area_buildings, 'count');
                        if($areas_count > $max_listings){
                            $area_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.area.'.snake_case_custom($area), \App::getLocale())) ? __( 'data.area.'.snake_case_custom($area)) : $area;
                            $title_area_lang    =   __('data.seo_titles.property_type.'.$residential_commercial.'.nav_title', ['type' => title_case($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'residential_commercial' => title_case($residential_commercial_lang), 'loc' => $area_lang]);
                            $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'label' ]          =   $area_lang;
                            $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'title' ]          =   $title_area_lang;
                            $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'link' ]           =   kebab_case_custom( $current_base_url . '/' . $city . '/' . $type . '-for-' . $rent_sale . '-in-' . $area . '-area' );
                            $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'count' ]          =   $areas_count;

                            if(array_has($internal_links,'quick_access.'.$rent_buy.'.'.$j) && $k < 10 && (strtolower($city) == 'dubai' || strtolower($city) == 'abu dhabi') || (strtolower($city) == 'sharjah' && $property_type == 'apartment') ){
                            $internal_links[ 'quick_access' ][ $rent_buy ][$j]['areas'][$k]['label'] = $area_lang;
                            $internal_links[ 'quick_access' ][ $rent_buy ][$j]['areas'][$k]['link'] = kebab_case_custom( $current_base_url . '/' . $city . '/' . $type . '-for-' . $rent_sale . '-in-' . $area . '-area' );
                            $k++;
                            }
                            $buildings  =   array_get($area_buildings,'buildings',[]);
                            $i++;

                            if(!empty($buildings)){
                            foreach($buildings as $building => $building_loc){
                                $buildings_count    =   array_get($building_loc, 'count');
                                if($buildings_count > $max_listings){
                                $building_lang          =   (\App::getLocale() !== 'en' && \Lang::has('data.building.'.snake_case_custom($building), \App::getLocale())) ? __( 'data.building.'.snake_case_custom($building)) : $building;
                                $title_building_lang    =   __('data.seo_titles.property_type.'.$residential_commercial.'.nav_title', ['type' => title_case($property_type_lang), 'for_rent_buy' => strtolower($rent_sale_for_lang), 'residential_commercial' => title_case($residential_commercial_lang), 'loc' => $building_lang]);
                                $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'label' ]          =   $building_lang;
                                $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'title' ]          =   $title_building_lang;
                                $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'link' ]           =   kebab_case_custom( $current_base_url . '/' . $city . '/' . $area . '/' . $type . '-for-' . $rent_sale . '-in-' . $building . '-building' );
                                $internal_links[ 'content' ][ $menu_label ][ $rent_buy ][ $residential_commercial ][ $i ][ 'count' ]          =   $buildings_count;
                                $i++;
                                }
                            }
                            }
                        }
                        }

                    }
                    $j++;
                    }
                }
                }
            }
            }
        }
        if(array_has($internal_links,'menu.commercial')){
            $comm_menu    = array_get($internal_links,'menu.commercial');
            $comm_content = array_get($internal_links,'content.commercial');
            array_forget($internal_links, 'menu.commercial');
            array_forget($internal_links, 'content.commercial');
            array_set($internal_links,'menu.Commercial',$comm_menu);
            array_set($internal_links,'content.Commercial',$comm_content);
        }
        return $internal_links;
    }

    public function reset_password($token){
        return view( 'pages.reset_password',['token'=>$token] );
    }

    public function setLocale($locale = null){
        if( \Session::has( 'locale-set' ) ){
            // Session has locale set
            if( is_null( $locale ) )
            \App::setLocale( \Session::get( 'locale-set' ) );
            else
            \App::setLocale( $locale );
        }
        else{

            // Session doesn't have locale set
            if( is_null( $locale ) ){
            \App::setLocale( 'en' );
            }
            else{
            if( in_array( $locale, array_keys(config('languages')) ) ){
                \App::setLocale( $locale );
            }
            else{
                \App::setLocale( 'en' );
            }
            }
        }
        \Session::put( 'locale-set', \App::getLocale() );
    }

}
