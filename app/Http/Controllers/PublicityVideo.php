<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class PublicityVideo extends BaseController
{
    const leaf = 'publicity-video';

    use \App\Traits\ApiCaller;

    use \App\Traits\JsonValidator;

    protected $payload, $config,$api_gateway;


    public function __construct(Request $request = null){
        $this->payload  =   !empty( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $this->config   =   config('models.'.$this::leaf);
        $this->api_gateway  =   env('APP_ENV') == 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    public function index(){
        $this->api_endpoint     =   array_get($this->config,'api.endpoint.r','');
        if(!array_has($this->payload,'active')){
            array_set($this->payload,'active',1);
        }
        if(!array_has($this->payload,'pv_domain')){
            array_set($this->payload,'pv_domain',array(env('APP_ENV')));
        }

        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

}
