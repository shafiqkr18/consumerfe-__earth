<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Lead extends User
{
    use \App\Traits\ApiCaller;

    public function __construct( Request $request ){
        $this->leaf         =   'lead';
        $this->request      =   $request;
        $this->payload      =   $request->all();
        $this->api_gateway  =   env('APP_ENV') == 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    /**
    * Display a listing of the leads.
    *
    * @param  string  $user_id, $model
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(...$vars){
        $user_id    = array_get( $vars, 0 );
        $model      = array_get( $vars, 1 );

        /**
        *  List all leads of a model
        */
        if( !empty( $model ) ){
            $this->api_endpoint     =   '/user/'.$user_id.'/'.$this->leaf.'/'.$model;
        }
        /**
        *  List all user leads
        */
        else{
            $this->api_endpoint     =   '/user/'.$user_id.'/'.$this->leaf;
        }

        return Parent::index();
    }

    /**
    * Store a newly created lead in storage.
    *
    * @param  string  $model
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request, ...$vars){

        $model          =   array_get($vars,0);

        $spam           =   false;

        $request_action     =   array_get( $this->payload, 'action' );
        $request_model_id   =   array_get( $this->payload, $model.'._id' );
        $request_email      =   array_get( $this->payload, 'user.contact.email' );
        $request_phone      =   (string)array_get( $this->payload, 'user.contact.phone' );

        $track_params       =   [ 'email' => $request_email, 'phone' => $request_phone ];

        $session_tracking       =   \Session::get('lead_tracking');
        $max_leads_per_session  =   env('MAX_USER_LEADS_PER_SESSION');

        /**
        *  It's spam
        *  1   When lead is created twice for the same entity with same action, email and phone
        *  2   When params email or phone in user session exceeds permitted number as in env('MAX_USER_LEADS_PER_SESSION')
        */
        foreach($track_params as $key => $value){
            if(!empty($session_tracking)){
                foreach($session_tracking as $params){
                    foreach($params as $track){
                        if((($request_phone == array_get($track,'contact.phone') || $request_email == array_get($track,'contact.email')) &&
                        $request_action == array_get($track,'action') &&
                        $request_model_id == array_get($track,$model.'._id'))
                        || (count($params)+1) > (int)$max_leads_per_session ){
                            $spam     =   true;
                        }
                    }
                }
            }
        }

        // Applying Blacklist & Tracking Spam
        if( in_array( array_get( $this->payload, 'user.contact.email', null ), config( 'lead_blacklist' ) ) || $spam ){
            return response( '{message: "lead has been successfully created!"}', 200 );
        }
        // Return if is test
        if( strtolower( $this->payload['user']['contact']['name'] ) === strtolower('#test') ){
            return response(' Test lead created ');
        }
        $this->api_endpoint     =    $this->leaf.'/'.$model;

        // Detect client location
        $position           =   \Location::get(detect_client_ip());
        $country_code       =   $position->countryCode;
        $map_country_codes  =   config('data.country_codes');
        $country_source     =   array_has($map_country_codes,$country_code) ? array_get($map_country_codes,$country_code) : $country_code;
        array_set($this->payload,'country_source', $country_source);

        if($model=='property' && array_has( $this->payload, 'preferences' )){
            return  $this->preferences_lead($request, $vars);
        }
        else{
            // Append model details to the payload
            $this->append_details($model);
            return $this->store_lead($request, $vars, $model);
        }
    }

    public function mortgage_calc_lead($model){

        // Validate request
        $validate   =   $this   ->set_leaf('lead')
        ->set_method_name('mortgage')
        ->set_payload($this->payload)
        ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        return response('Staging mortgage calculator lead created');

        $this->append_details($model);
        $email_controller    =   new Email();
        $email_controller->mortgage_calc_email($this->payload, $model);

        return response()->json( [ 'message' => title_case( $model ).' mortgage calculator lead created' ], 200 );

    }

    public function property_listings_lead(){

        $email_controller    =   new Email();
        $email_controller->property_listings_email($this->payload);

        return response()->json( [ 'message' => 'Property listings lead created' ], 200 );

    }

    private function append_details($model){
        if( array_has( $this->payload, $model.'._id' ) ){
            $id         =   array_get( $this->payload, $model.'._id' );
            switch( $model ){
                case 'property':
                $controller =   new Property();
                $id         =   starts_with( $id, 'zp-' ) ? $id : 'invalid';
                break;
                case 'agent':
                $controller =   new Agent();
                $id         =   starts_with( $id, 'za-' ) ? $id : 'invalid';
                break;
                case 'developer':
                $controller =   new Developer();
                $id         =   starts_with( $id, 'zd-' ) ? $id : 'invalid';
                break;
                case 'project':
                $controller =   new Project();
                $id         =   starts_with( $id, 'zj-' ) ? $id : 'invalid';
                break;
                case 'service':
                $controller =   new Service();
                $id         =   starts_with( $id, 'zs-' ) ? $id : 'invalid';
                break;
                default:
                return response()->json( [ 'message' => 'Invalid Model' ], 400 );
            }

            if( !empty( $controller ) ){
                if( !empty( $id ) && $id != 'invalid' ){
                    if( !empty( $controller ) ){
                        $data       =   $controller->show($id);
                        $details    =   json_decode((string)$data->getBody(),1);
                    }
                    else{
                        $details    =   '';
                    }
                }
                else{
                    return response()->json( [ 'message' => 'Invalid '.$model.' id' ], 400 );
                }
            }

            if( !empty( $details ) ){
                array_set( $this->payload, $model, $details );
            }

        }
        else{
            return response()->json( [ 'message' => 'Please select '.$model ], 400 );
        }
    }

    private function store_lead($request, $vars, $model){

        if( ( array_has($this->payload,'property.agent.agency.settings.lead.qualification') && array_get($this->payload,'property.agent.agency.settings.lead.qualification')
        || array_has($this->payload,'project.developer.settings.lead.qualification') && array_get($this->payload,'project.developer.settings.lead.qualification') )
        && array_get($this->payload,'action') !== 'call' && array_get($this->payload,'action') !== 'whatsapp' ){
            array_set($this->payload,'settings.approved',false);
        }
        else{
            array_set($this->payload,'settings.approved',true);
        }

        $email_data     =   $this->payload;
        $response       =   Parent::store($request, $vars);

        if( $response->getStatusCode() === 200 && $email_data['action'] !== 'call' && $email_data['action'] !== 'whatsapp' && $model !== 'preferences'){

            $payload        =   array_get($this->payload,'json');

            $user_email     =   array_get( $payload, 'user.contact.email' );
            $user_phone     =   (string)array_get( $payload, 'user.contact.phone' );
            $lead_tracking  =   [];

            $track_params   =   [ 'email' => $user_email, 'phone' => $user_phone ];
            $i              =   [ 'email' => 0, 'phone' => 0 ];
            $max_in_session =   (int)env('MAX_LEADS_IN_SESSION');

            $lead_tracking_session      =   \Session::has( 'lead_tracking' ) ? \Session::get( 'lead_tracking' ) : [];
            $lead_tracking_session      =   (!empty($lead_tracking_session) && count($lead_tracking_session) > $max_in_session) ? array_slice($lead_tracking_session, -$max_in_session, $max_in_session, true) : $lead_tracking_session;

            /**
            *  Store user sessions in lead_tracking session array with identified by phone and email parameters of the lead
            *  Append new lead details to the session for tracking
            *  Trim session array and get last n elements if size exceeds permitted limit as in env('MAX_LEADS_IN_SESSION')
            */
            foreach($track_params as $key => $value){
                if(isset($lead_tracking_session[$value])){
                    $i[$key]    =  count($lead_tracking_session[$value]);
                }
                $lead_tracking[ 'lead_tracking' ][ $value ][ $i[$key] ][ $model ][ '_id' ]      =   array_get( $payload, $model.'._id' );
                $lead_tracking[ 'lead_tracking' ][ $value ][ $i[$key] ][ 'contact' ][ 'email' ] =   array_get( $payload, 'user.contact.email' );
                $lead_tracking[ 'lead_tracking' ][ $value ][ $i[$key] ][ 'contact' ][ 'phone' ] =   array_get( $payload, 'user.contact.phone' );
                $lead_tracking[ 'lead_tracking' ][ $value ][ $i[$key] ][ 'action' ]             =   array_get( $payload, 'action' );
                $lead_tracking_request  =   $lead_tracking[ 'lead_tracking' ];
                if(isset($lead_tracking_session[$value])){
                    $new_lead_tracking['lead_tracking'][$value] =   array_merge($lead_tracking_session[$value],$lead_tracking_request[$value]);
                }
                else{
                    $new_lead_tracking['lead_tracking'] =   array_merge($lead_tracking_session,$lead_tracking_request);
                }
                $i[$key]++;
            }

            \Session::put( $new_lead_tracking );

            $email   =  new Email;
            $email->lead_email($email_data, $model);

            #NOTIFY REALAUDIENCES
            $this->notify_realaudiences();
            return $response;
        }
        else{
            return $response;
        }
    }

    private function preferences_lead($request, $vars){
        //Get agency details
        $agency_payload = [];
        array_set($agency_payload, 'settings.status', 1);
        array_set($agency_payload, 'settings.approved', true);
        array_set($agency_payload, 'settings.payment_tier', true);
        $agencies_data      =   app('\App\Http\Controllers\Agency')->set_payload($agency_payload)->index();
        $agencies_data      =   json_decode((string)$agencies_data->getBody(),1);
        if(array_get($agencies_data,'total', 0) > 0){
            $email_data     =   $this->payload;
            foreach(array_get($agencies_data,'data') as $agency){
                $this->payload['property']['agent']['agency'] =  $agency;
                $this->payload['preferences']['agent']['agency']['contact']['name'] =  array_get($agency, 'contact.name');
                $this->payload['preferences']['agent']['agency']['contact']['email'] =  array_get($agency, 'contact.email');
                $this->payload['preferences']['agent']['agency']['_id']  =  array_get($agency, '_id');
                $response = $this->store_lead($request, $vars, 'preferences');
                if($response->getStatusCode() !== 200)
                return $response;
            }
            array_set($email_data, 'agencies', array_get($agencies_data,'data'));

            $email   =  new Email;
            $email->lead_email($email_data, 'preferences');
        }
        return response()->json( [ 'message' => 'Preferences lead created'], 200 );

        //TODO Scenario when there are not agencies with payment_tier sending then an email to sales
    }


    public function scholarship_lead( Request $request ){

        $payload  = $request->all();
        $file     = array_get($payload, 'file');
        $payload  = json_decode(array_get($payload,'data'),true);
        $email   =  new Email;
        $email->scholarship_email( $payload,$file );
        return response()->json( array( 'success' => 'Your request is sent. Someone will get in touch with you asap.' ),200 );

    }

    private function notify_realaudiences(){
        #TOKEN = ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SnBjM01pT2lKYWIyOXRjSEp2Y0dWeWRIa2lMQ0pwWVhRaU9qRTFOamt5TkRVeE5UQXNJbVY0Y0NJNk1UWXdNRGM0TVRFMU1Dd2lZWFZrSWpvaWQzZDNMbnB2YjIxd2NtOXdaWEowZVM1amIyMGlMQ0p6ZFdJaU9pSmtaWFpBZW05dmJYQnliM0JsY25SNUxtTnZiU0lzSW5OdmRYSmpaU0k2SW5KbFlXeGhkV1JwWlc1alpYTWlmUS5DRENjMGxIWW1TMFNIU1ZaSU91eGZ3ZHBhRlVqb3dScFVPR1Jqb2ZjS29j
        #KEY = rwSfuz2AI1kxNOPiTg03PhT0hf7SuHHc
        if(!is_null(\Session::get('project-token')) && !is_null(\Session::get('project-key'))){
            try{
                #Realaudiences route
                $base_route = 'http://conversions.realaudiences.com/notify/'.\Session::get('project-token').'/' .\Session::get('project-key');
                $client     =   new \GuzzleHttp\Client();
                return $client->get($base_route);
            }
            catch( \GuzzleHttp\Exception\RequestException $e ){
                if( $e->hasResponse() ) {
                    return response()->json( json_decode( $e->getResponse()->getBody()->getContents(), 1 ), $e->getResponse()->getStatusCode() );
                }
                else{
                    return response()->json( array( 'message' => 'No response' ), 400 );
                }
            }
        }

    }

}
