<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use \DrewM\MailChimp\MailChimp;
use Session;

class User extends BaseController
{
    const leaf = 'user';
    use \App\Traits\ApiCaller;
    use \App\Traits\JsonValidator;

    protected $api_endpoint,$config,$api_gateway;

    public function __construct( Request $request ){
        $this->request      =   $request;
        $this->config       =   config('models.'.$this::leaf);
        $this->api_gateway  =   env('APP_ENV') == 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(...$vars){

        $this->payload  =   json_decode($this->request->get('query'), true);
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request, ...$vars){
        $request    =   is_null($this->payload) ? $this->request->all() : $this->payload;
        return  $this   ->set_method('POST')
                        ->build_payload( $request )
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    public function register(){

        $this->payload  =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('register')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        array_set($this->payload, 'token', bin2hex(openssl_random_pseudo_bytes(10)));
        $payload_email = $this->payload;

        // Assuming all values present
        if(array_get($this->payload,'password',1) === array_get($this->payload,'password_confirmation',2)){
            $this->api_endpoint     =   array_get($this->config,'api.endpoint.re');
            $data   =   $this->store($this->request, $this->payload);
            // Check for error
            if($data->getStatusCode()==200){
                return json_decode((string)$data->getBody(),1);
            }
            else{
                // $email      =   new Email;
                // $email->register($payload_email);
                return $data;
            }
        }
        else{
            return response()->json(['message' => 'Passwords do not match'], 404);
        }

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id){

        $this->payload          =   $this->request->all();

        $this->api_endpoint     =   str_replace('#', $id,array_get($this->config,'api.endpoint.sh') );
        return  $this   ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    public function social_auth(){

        $this->payload  =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('social')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        $this->api_endpoint     =   array_get($this->config,'api.endpoint.so');
        return $this    ->set_method('POST')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    /**
    * Update the specified resource in storage.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function update(Request $request, ...$vars){

        $this->payload          =   $request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('update')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        $id                     =   array_get($vars,0);
        $this->api_endpoint     =   str_replace('#', $id,array_get($this->config,'api.endpoint.up') );
        return  $this   ->set_method('PUT')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, ...$vars){

        $request    =   is_null($this->payload) ? $this->request->all() : $this->payload;

        return $this    ->set_method('DELETE')
                        ->build_payload($request)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    /**
    * Display the specified resource.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function login(){

        $this->payload  =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('login')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        $this->api_endpoint     =   array_get($this->config,'api.endpoint.lo');
        $data   =   $this   ->set_method('POST')
                            ->build_payload( $this->payload )
                            ->call_api($this->api_gateway.$this->api_endpoint);

        if($data->getStatusCode()==200){
            \Session::put( ['user' => json_decode($data->getBody(),1)] );
            return json_decode((string)$data->getBody(),1);
        }
        else{
            return response()->json(['message'=>'These credentials do not match our records'],404);
        }
    }

    /**
    * Forgot password
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function forgot_password(...$vars){

        $this->payload  =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('forgot_password')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        array_set($this->payload, 'token', bin2hex(openssl_random_pseudo_bytes(10)));
        $payload_email = $this->payload;

        $this->api_endpoint     =   array_get($this->config,'api.endpoint.fo');
        $return                 =   $this   ->set_method('POST')
                                            ->build_payload( $this->payload )
                                            ->call_api($this->api_gateway.$this->api_endpoint);

        if($return->getStatusCode()==200){
            $email      =   new Email;
            $email->forgot_password($payload_email);
            return response()->json( [ 'message' => 'If this is the email address that you have registered with us, you will get a password reset link in your email shortly.' ], 200 );
        }
        else{
            return $return;
        }
    }

    /**
    * Reset password
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function reset_password(...$vars){

        $this->payload  =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('reset_password')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        if( array_get($this->payload,'password',1) === array_get($this->payload,'password_confirmation',2) ){

            $this->api_endpoint     =   array_get($this->config,'api.endpoint.rs');

            return $this    ->set_method('POST')
                            ->build_payload( $this->payload )
                            ->call_api($this->api_gateway.$this->api_endpoint);
        }
        else{
            return response()->json(['message' => 'Passwords do not match'], 400);
        }
    }

    /**
    * Verify email
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function verify_email(...$vars){

        $this->payload  =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('verify_email')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        $this->api_endpoint     =   array_get($this->config,'api.endpoint.vf');

        return $this    ->set_method('POST')
                        ->build_payload( $this->payload )
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    /**
    * Change password
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function change_password(...$vars){

        $this->payload          =   $this->request->all();

        // Validate request
        $validate   =   $this   ->set_leaf($this::leaf)
                                ->set_method_name('change_password')
                                ->set_payload($this->payload)
                                ->validate_request();

        // Validation fail
        if( !is_bool( $validate) ){
            return response()->json( [ 'errors' => $validate ], 400 );
        }

        if( array_get($this->payload,'password',1) === array_get($this->payload,'password_confirmation',2) ){

            $id                     =   array_get($vars,0);
            $this->api_endpoint     =   str_replace('#', $id,array_get($this->config,'api.endpoint.ch') );
            return  $this   ->set_method('PUT')
                            ->build_payload( $this->payload )
                            ->call_api($this->api_gateway.$this->api_endpoint);
        }
        else{
            return response()->json(['message' => 'Passwords do not match'], 400);
        }
    }

    /**
    * Newletter subscribe
    */
    public function newsletter_subscribe(...$var){
        $email  =   end($var);
        try{
            $MailChimp          =   new MailChimp( env( 'MAILCHIMP_API_KEY' ) );
            $list_id            =   '675fab831e';
            $create             =   $MailChimp->post( "lists/$list_id/members", [
                'email_address' => $email,
                'status'        => 'subscribed'
            ]);
            $subscriber_hash    =   $MailChimp->subscriberHash( $email );
            $result             =   $MailChimp->patch( "lists/$list_id/members/$subscriber_hash" );
            // TODO: Update user setting if logged in
            return $result;
        }
        catch( \Exception $e ){
            return response()->json( [ 'message' => $e->getMessage() ], 500 );
        }
    }

    /**
    * Newsletter unsubscribe
    */
    public function newsletter_unsubscribe(...$var){
        $email  =   end($var);
        try{
            $MailChimp          =   new MailChimp( env( 'MAILCHIMP_API_KEY' ) );
            $list_id            =   '675fab831e';
            $subscriber_hash    =   $MailChimp->subscriberHash( $email );
            $MailChimp->delete("lists/$list_id/members/$subscriber_hash");
            // TODO: Update user setting if logged in
            return response()->json( [ 'message' => 'You have successfully unsubscribed from our newsletters.' ], 200 );
        }
        catch( \Exception $e ){
            return response()->json( [ 'message' => $e->getMessage() ], 500 );
        }
    }

    public function logout(){
        //Clear session
        \Session::forget('user');
        return response()->json( [ 'message' => 'You have successfully logout.' ], 200 );
    }

}
