<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

class Email extends BaseController
{
    use \App\Traits\Utility;

    public function lead_email($payload, $model){
        switch( $model ){
            case 'property':
                $this->property_lead_email($payload);
            break;
            case 'agent':
                $this->agent_lead_email($payload);
            break;
            case 'developer':
                // $this->developer_lead_email($payload);
            break;
            case 'project':
                $this->project_lead_email($payload);
            break;
            case 'service':
                $this->service_lead_email($payload);
            break;
            case 'preferences':
                $this->preferences_lead_email($payload);
            break;
            default:
            break;
        }
    }

    public function service_lead_email($lead_data){

        $mail[ 'user' ]     =   array_get( $lead_data, 'user.contact', '' );
        $mail[ 'lead' ]     =   array_except($lead_data, ['user', 'service']);
        $mail[ 'service' ]  =   array_get( $lead_data, 'service.contact', '' );

        //Consumer Email
        $this->consumer_email($mail, 'service');
        //Sales Email
        $this->sales_email($mail, 'service');
    }

    public function agent_lead_email($lead_data){

        $mail[ 'user' ]     =   array_get( $lead_data, 'user.contact', '' );
        $mail[ 'lead' ]     =   array_except($lead_data, ['user', 'agent']);
        $mail[ 'agent' ]    =   array_get( $lead_data, 'agent.contact', '' );

        //Consumer Email
        $this->consumer_email($mail, 'agent');
        $this->agent_email($mail, 'agent');
    }

    public function project_lead_email($lead_data){

        $mail[ 'title' ]    =   'Hi';

        $mail[ 'user' ]     =   array_get( $lead_data, 'user.contact', '' );
        $mail[ 'lead' ]     =   array_except($lead_data, ['user', 'project']);
        $mail[ 'action' ]   =   ( !empty( $mail[ 'lead' ][ 'action' ] ) ) ? $mail[ 'lead' ][ 'action' ] : 'email';

        //Lead Details
        $mail[ 'lead' ][ 'name' ]        =  ( !empty( $mail[ 'user' ][ 'name' ] ) )  ? $mail[ 'user' ][ 'name' ] : 'Curious Consumer';
        $mail[ 'lead' ][ 'email' ]       =  ( !empty( $mail[ 'user' ][ 'email' ] ) ) ? $mail[ 'user' ][ 'email' ] : '';
        $mail[ 'lead' ][ 'phone' ]       =  ( !empty( $mail[ 'user' ][ 'phone' ] ) ) ? $mail[ 'user' ][ 'phone' ] : '';
        $mail[ 'lead' ][ 'message' ]     =  ( !empty( $mail[ 'lead' ][ 'message' ] ) ) ? $mail[ 'lead' ][ 'message' ] : 'I am interested in this property.';

        //Project Details
        $mail[ 'project' ][ 'title' ]               =  array_get( $lead_data, 'project.name', '' );
        $mail[ 'project' ][ '_id' ]                 =  array_get( $lead_data, 'project._id', '' );
        $mail[ 'project' ][ 'property_ref_no' ]     =  array_get( $lead_data, 'project._id', '' );
        $mail[ 'project' ][ 'city' ]                =  array_get( $lead_data, 'project.city', '' );
        $mail[ 'project' ][ 'suburb' ]              =  array_get( $lead_data, 'project.area', '' );
        $mail[ 'project' ][ 'units' ]               =  array_get( $lead_data, 'project.unit.count', '' );
        $mail[ 'project' ][ 'starting_price' ]      =  array_get( $lead_data, 'project.pricing.starting', '' );
        $mail[ 'project' ][ 'currency' ]            =  array_get( $lead_data, 'project.pricing.currency', '' );
        $mail[ 'project' ][ 'description' ]         =  array_get( $lead_data, 'project.writeup.description', '' );

        $mail[ 'project' ][ 'images' ]              =  empty(array_get( $lead_data, 'project.image.portal.mobile',[])) ? array_get( $lead_data, 'project.image.original',[]) : array_get( $lead_data, 'project.image.portal.mobile',[]);

        //Consumer Email
        $this->consumer_email($mail, 'project');

        if(array_has($mail,'lead.settings.approved') && array_get($mail,'lead.settings.approved')){
            $mail['developer']['lead']    = $lead_data[ 'project' ]['developer']['settings']['lead'];
            $mail['developer']['name']    = $lead_data[ 'project' ]['developer']['contact']['name'];
            //Send Developer Email
            $this->developer_email($mail, 'project');
        }
        else{
            $this->sales_email($mail, 'project');
        }

        if(array_get( $lead_data, 'custom_form', false )){

            $mail[ 'custom_data' ][ 'when' ]                =  array_get( $lead_data, 'custom_data.when', '' );
            $mail[ 'custom_data' ][ 'rooms' ]               =  array_get( $lead_data, 'custom_data.rooms', '' );
            $mail[ 'custom_data' ][ 'times' ]               =  array_get( $lead_data, 'custom_data.times', '' );
            $mail[ 'custom_data' ][ 'investor_job' ]        =  array_get( $lead_data, 'custom_data.investor_job', '' );
            $mail[ 'custom_data' ][ 'where' ]               =  array_get( $lead_data, 'custom_data.where', '' );
            $mail[ 'custom_data' ][ 'when_now' ]               =  array_get( $lead_data, 'custom_data.when_now', '' );
            $this->custom_enquiry_email($mail);
        }
    }

    public function property_lead_email( $lead_data ){

        $mail[ 'subject' ]          =   'You have a lead!';
        $mail[ 'title' ]            =   'Hi';
        $mail[ 'agent' ]            =   array_get($lead_data,'property.agent.contact','');
        $mail[ 'agency' ]           =   array_get($lead_data,'property.agent.agency','');
        $mail[ 'user' ]             =   array_get($lead_data,'user.contact','');

        $mail[ 'lead' ]             =   array_except($lead_data, ['user', 'property']);
        $mail[ 'action' ]           =   ( !empty(array_get($mail,'lead.action','')) ) ? array_get($mail,'lead.action','email') : 'email';
        $mail[ 'button_link' ]      =   env( 'APP_URL' );

        //Lead Details
        $mail[ 'lead' ][ 'name' ]        =  ( !empty(array_get($mail,'user.name','')) ) ? array_get($mail,'user.name','Curious client') : 'Curious client';
        $mail[ 'lead' ][ 'email' ]       =  ( !empty(array_get($mail,'user.name','')) ) ? array_get($mail,'user.email','') : '';
        $mail[ 'lead' ][ 'phone' ]       =  ( !empty(array_get($mail,'user.phone','')) ) ? array_get($mail,'user.phone','') : '';
        $mail[ 'lead' ][ 'message' ]     =  ( !empty(array_get($mail,'lead.message','')) ) ? array_get($mail,'lead.message','I am interested in this property') : 'I am interested in this property';

        //Property Details
        $mail[ 'property' ][ 'title' ]              =   ( !empty(array_get($lead_data,'property.writeup.title','')) ) ? array_get($lead_data,'property.writeup.title','A beautiful property') : 'A beautiful property';
        $mail[ 'property' ][ 'property_ref_no' ]    =   ( !empty(array_get($lead_data,'property.ref_no','')) ) ? array_get($lead_data,'property.ref_no','') : '';
        $mail[ 'property' ][ 'city' ]               =   ( !empty(array_get($lead_data,'property.city','')) ) ? array_get($lead_data,'property.city','') : '';
        $mail[ 'property' ][ 'suburb' ]             =   ( !empty(array_get($lead_data,'property.area','')) ) ? array_get($lead_data,'property.area','') : '';
        $mail[ 'property' ][ 'property_type' ]      =   ( !empty(array_get($lead_data,'property.type','')) ) ? array_get($lead_data,'property.type','') : '';
        $mail[ 'property' ][ 'bedroom' ]            =   ( !empty(array_get($lead_data,'property.bedroom','')) ) ? array_get($lead_data,'property.bedroom','') : '';
        $mail[ 'property' ][ 'bathroom' ]           =   ( !empty(array_get($lead_data,'property.bathroom','')) ) ? array_get($lead_data,'property.bathroom','') : '';
        $mail[ 'property' ][ 'builtup_area' ]       =   ( !empty(array_get($lead_data,'property.dimension.builtup_area','')) ) ? array_get($lead_data,'property.dimension.builtup_area','') : '';
        $mail[ 'property' ][ 'description' ]        =   ( !empty(array_get($lead_data,'property.writeup.description','')) ) ? array_get($lead_data,'property.writeup.description','') : '';
        $mail[ 'property' ][ 'description' ]        =   str_replace( "||", "<br>", strip_tags( preg_replace( "/\|\|+/", "||", trim( $mail[ 'property' ][ 'description' ], "||" ) ) ) );

        $mail[ 'property_name' ]                    =   ( !empty(array_get($mail,'property.writeup.title','')) ) ? array_get($mail,'property.writeup.title','') : '';

        $mail[ 'id' ]                               =   ( !empty(array_get($lead_data,'property._id','')) ) ? array_get($lead_data,'property._id','') : '';
        $mail[ 'rent_or_buy' ]                      = 	( !empty( array_get($lead_data,'property.rent_buy','') ) AND array_get($lead_data,'property.rent_buy','') == 'Rent' )? 'rent' : 'buy';
        $mail[ 'residential_or_commercial' ]        = 	( !empty( array_get($lead_data,'property.residential_commercial','') ) ) ? strtolower(array_get($lead_data,'property.residential_commercial','')) : 'residential';

        $mail[ 'property_link' ]                    =   $this->property_link( array_get( $lead_data, 'property.rent_buy', 'sale' ),  array_get( $lead_data, 'property.residential_commercial', 'residential' ), array_get( $lead_data, 'property.writeup.title', '' ), array_get( $lead_data, 'property._id', '' ) );

        if( !empty( $lead_data['property'][ 'images' ]['original'] ) ){
            $mail[ 'property' ][ 'images' ]     =   array_get($lead_data,'property.images.original');
        }

        //Consumer Email
        $this->consumer_email($mail, 'property');

        if(array_has($mail,'lead.settings.approved') && array_get($mail,'lead.settings.approved')){
            //Send Agency Email
            $this->agency_email($mail, 'property');
            if(isset($mail[ 'agency' ]['settings']['lead']) && $mail[ 'agency' ]['settings']['lead']['agent']['receive_emails'] == true){
                //Send Agent Email
                $this->agent_email($mail, 'property');
            }
        }
        else{
            $this->sales_email($mail, 'property');
        }

    }
    public function preferences_lead_email( $lead_data ){

        $mail[ 'subject' ]          =   'You have a lead!';
        $mail[ 'title' ]            =   'Hi';

        $mail[ 'user' ]             =   array_get($lead_data,'user.contact','');

        $mail[ 'lead' ]             =   array_except($lead_data, ['user', 'property', 'preferences']);
        $mail[ 'action' ]           =   ( !empty(array_get($mail,'lead.action','')) ) ? array_get($mail,'lead.action','email') : 'email';
        $mail[ 'button_link' ]      =   env( 'APP_URL' );

        //Lead Details
        $mail[ 'lead' ][ 'name' ]        =  ( !empty(array_get($mail,'user.name','')) ) ? array_get($mail,'user.name','Curious client') : 'Curious client';
        $mail[ 'lead' ][ 'email' ]       =  ( !empty(array_get($mail,'user.name','')) ) ? array_get($mail,'user.email','') : '';
        $mail[ 'lead' ][ 'phone' ]       =  ( !empty(array_get($mail,'user.phone','')) ) ? array_get($mail,'user.phone','') : '';
        $mail[ 'lead' ][ 'message' ]     =  ( !empty(array_get($mail,'lead.message','')) ) ? array_get($mail,'lead.message','I am interested in a property') : 'I am interested in a property';


        $mail[ 'property' ]                         =   array_get( $lead_data, 'property' );

        //Preferences
        $mail[ 'preferences' ][ 'suburb']           =   !empty($lead_data[ 'preferences' ][ 'area' ]) ? implode( ',', $lead_data[ 'preferences' ][ 'area' ] ) : '';
        $mail[ 'preferences' ][ 'city' ]            =   !empty($lead_data[ 'preferences' ][ 'city' ]) ? $lead_data[ 'preferences' ][ 'city' ] : '';
        $mail[ 'preferences' ][ 'rent_buy' ]        =   !empty($lead_data[ 'preferences' ][ 'rent_buy' ]) ? str_replace( '-', ' ', $lead_data[ 'preferences' ][ 'rent_buy' ] ) : '';
        $mail[ 'preferences' ][ 'property_type' ]   =   !empty($lead_data[ 'preferences' ][ 'type' ]) ? $lead_data[ 'preferences' ][ 'type' ] : '';

        foreach($lead_data[ 'agencies' ] as $agency){

            $mail[ 'agency' ]           =   $agency;

            if(array_has($mail,'agency.settings.lead.qualification') && !array_get($mail,'agency.settings.lead.qualification')){
                //Send Agency Email
                $this->agency_email($mail, 'property');
            }
            else{
                $this->sales_email($mail, 'property');
            }
        }

        //Consumer Email
        $this->consumer_email($mail, 'property');

    }

    public function consumer_email($mail,$model = 'property'){

        if( array_get($mail, 'user.email', '') !== 'anonymous@domain.com' ){
            $mail[ 'receiver_name' ]    =   explode( " ", ( !empty( $mail[ 'user' ][ 'name' ] ) ) ? $mail[ 'user' ][ 'name' ]   : 'There' )[0];
            $mail[ 'receiver_email' ]   =   array_get( $mail, 'user.email', [] );
            $mail[ 'send_to' ]          =   'consumer';

            $mail[ 'agent' ][ 'name' ]  =   array_get( $mail, 'agent.name', '' );
            $mail[ 'agent' ][ 'email' ] =   array_get( $mail, 'agent.email', '' );
            $mail[ 'agent' ][ 'phone' ] =   array_get( $mail, 'agent.phone', '' );

            $cc   =   [];
            $bcc  =   config( 'internal_leads_forwarding' );

            if($model == 'property'){
                $mail[ 'content' ]          =   'You have contacted agent '.$mail[ 'agent' ][ 'name' ].' through '.$mail[ 'action' ].' for this property.';
                $mail[ 'subject' ]          =   'You have contacted agent '.$mail[ 'agent' ][ 'name' ].' through '.$mail[ 'action' ];

                $this->send_email( $mail, 'emailers.lead', $cc, $bcc );
            }
            else if($model == 'project'){
                $mail[ 'content' ]          =   'The sales team will be in touch with you shortly';
                $mail[ 'button_label' ]     =   'View';
                $mail[ 'button_link' ]      =   $this->project_link( array_get( $mail, 'project._id', '' ) );
                $mail[ 'subject' ]          =   'Your interest in ' . array_get( $mail, 'project.title', 'An Awesome project' );

                $this->send_email( $mail, 'emailers.project', $cc, $bcc );
            }
            else if($model == 'agent'){
                $mail[ 'content' ]          =   'You have contacted agent ' . $mail[ 'agent' ][ 'name' ]. ' who will be in touch with you shortly!';
                $mail[ 'subject' ]          =   'Your interest in ' . array_get( $mail, 'project.title', 'An Awesome project' );

                $this->send_email( $mail, 'emailers.agent_lead', $cc, $bcc );
            }
            else if($model == 'service'){
                $mail[ 'subject' ]          =   'Moving Service Enquiry';
                $this->send_email( $mail, 'emailers.ancillary_lead', $cc, $bcc );
            }
        }
    }

    public function agent_email($mail, $model = 'property'){

        $mail[ 'receiver_email' ]   =   array_get( $mail, 'agent.email');
        $mail[ 'receiver_name' ]    =   explode( " ", ( !empty( $mail[ 'agent' ][ 'name' ] ) ) ? $mail[ 'agent' ][ 'name' ] : 'Agent' )[0];
        $mail[ 'send_to' ]          =   'agent';
        $cc   =   [];
        $bcc  =   config( 'internal_leads_forwarding' );

        if($model == 'property'){
            $mail[ 'content' ]          =   'You have received a lead through '.$mail[ 'action' ].'. Congratulations!';
            $mail[ 'subject' ]          =   'You have received a lead through '.$mail[ 'action' ];
            $mail[ 'button_label' ]     =   'Reply';
            $mail[ 'button_link' ]      =   env( 'APP_URL' );

            $this->send_email( $mail, 'emailers.lead', $cc, $bcc );
        }
        else if($model == 'agent'){
            $mail[ 'content' ]          =   'You have received a lead through agent finder. Congratulations!';
            $mail[ 'subject' ]          =   'You have received a lead through agent finder.';

            $this->send_email( $mail, 'emailers.agent_lead', $cc, $bcc );
        }
    }

    public function agency_email($mail, $model = 'property'){
        if(isset($mail[ 'agency' ]['settings']['lead'])){
            $mail[ 'receiver_email' ]   =   ($mail[ 'agency' ]['settings']['lead']['admin']['receive_emails'] == true) ? $mail[ 'agency' ]['settings']['lead']['admin']['email'] : $mail[ 'agency' ]['settings']['lead']['admin']['cc'];
            $cc                         =   $mail[ 'agency' ]['settings']['lead']['admin']['cc'];
        }
        else{
            $mail[ 'receiver_email' ]   =   'sales@zoomproperty.com';
            $cc                         =   [];
        }
        $bcc                        =   config( 'internal_leads_forwarding' );
        if($model == 'property'){
            $mail[ 'agency' ]           =   $mail[ 'agency' ]['contact'];
            $agent                      =   ( !empty( $mail[ 'agent' ][ 'name' ] ) ) ? $mail[ 'agent' ][ 'name' ] : '';
            $mail[ 'receiver_name' ]    =   ( !empty( $mail[ 'agency' ][ 'name' ] ) ) ? $mail[ 'agency' ][ 'name' ] : 'Agency';
            $mail[ 'content' ]          =   'Your agent '.$agent.' has received a Lead';
            $mail[ 'subject' ]          =   $mail[ 'content' ];
            $mail[ 'button_label' ]     =   'View Agent';
            $mail[ 'button_link' ]      =   env( 'APP_URL' );
            $mail[ 'send_to' ]          =   'agency';
        }
        $this->send_email( $mail, 'emailers.lead', $cc, $bcc );
    }

    public function developer_email($mail, $model = 'project'){
        $mail[ 'receiver_name' ]    =   ( !empty( $mail['developer']['contact']['name'] ) ) ? $mail['developer']['contact']['name'] : 'Developer';
        $mail[ 'receiver_email' ]   =   ($mail['developer']['lead']['admin']['receive_emails'] == true) ? $mail['developer']['lead']['admin']['email'] : $mail[ 'developer' ]['lead']['admin']['cc'];
        $cc                         =   $mail[ 'developer' ]['lead']['admin']['cc'];
        $bcc                        =   config( 'internal_leads_forwarding' );

        if($model == 'project'){
            $mail[ 'content' ]          =   'You have received a lead through '.$mail[ 'action' ].'. Congratulations!';
            $mail[ 'subject' ]          =   'You have received a lead through '.$mail[ 'action' ];
            $mail[ 'button_label' ]     =   'Reply';
            $mail[ 'button_link' ]      =   env( 'APP_URL' );
            $mail[ 'send_to' ]          =   'agency';
            $this->send_email( $mail, 'emailers.project', $cc, $bcc );
        }
    }

    public function sales_email($mail, $model ){

        $mail[ 'receiver_name' ]    =   'Sales';
        $mail[ 'receiver_email' ]   =   'sales@zoomproperty.com';
        $mail[ 'send_to' ]          =   'sales';
        $cc                         =   [];
        $bcc                        =   config( 'internal_leads_forwarding' );
        if($model == 'service'){
            $mail[ 'subject' ]          =   'Moving Service Enquiry';
            $this->send_email( $mail, 'emailers.ancillary_lead', $cc, $bcc );
        }
        if($model == 'property'){
            $mail[ 'subject' ]          =   'You have a new property lead to qualify';
            $mail[ 'content' ]          =   'This lead is not yet approved. Please check this property lead and take necessary action.';
            $this->send_email( $mail, 'emailers.lead', $cc, $bcc );
        }
        if($model == 'project'){
            $mail[ 'subject' ]          =   'You have a new project lead to qualify';
            $mail[ 'content' ]          =   'This lead is not yet approved. Please check this project lead and take necessary action.';
            $this->send_email( $mail, 'emailers.project', $cc, $bcc );
        }
    }

    public function mortgage_calc_email($payload, $type){

        //User
        $mail[ 'user' ][ 'contact' ][ 'first_name' ]    =  array_get( $payload, 'user.contact.first_name', 'Curious' );
        $mail[ 'user' ][ 'contact' ][ 'last_name' ]     =  array_get( $payload, 'user.contact.last_name', 'Consumer' );
        $mail[ 'user' ][ 'contact' ][ 'email' ]         =  array_get( $payload, 'user.contact.email', '' );
        $mail[ 'user' ][ 'contact' ][ 'mobile_number' ] =  array_get( $payload, 'user.contact.mobile_number', '' );
        $mail[ 'user' ][ 'contact' ][ 'work_number' ]   =  array_get( $payload, 'user.contact.work_number', array_get( $payload, 'user.contact.mobile_number', '' ) );

        //EMI
        $mail[ 'emi' ][ 'loan_amount' ]         =   array_get( $payload, 'emi.loan_amount', '' );
        $mail[ 'emi' ][ 'monthly' ]             =   array_get( $payload, 'emi.monthly', '' );
        $mail[ 'emi' ][ 'down_payment' ]        =   array_get( $payload, 'emi.down_payment', '' );
        $mail[ 'emi' ][ 'total_amount' ]        =   array_get( $payload, 'emi.total_amount', '' );
        $mail[ 'emi' ][ 'total_interest' ]      =   array_get( $payload, 'emi.total_interest', '' );

        $mail[ 'emi' ][ 'interest_rate' ]       =   !empty(array_get( $payload, 'emi.interest_rate', '' )) ? array_get( $payload, 'emi.interest_rate' ) : 4.2;
        $mail[ 'emi' ][ 'emi_term' ]            =   !empty(array_get( $payload, 'emi.term', '' )) ? array_get( $payload, 'emi.term' ) : 20;

        $mail[ 'enquiry' ][ 'type' ]            =   $type;
        $model                                  =   array_get( $payload, $type );

        //Property
        if( $mail[ 'enquiry' ][ 'type' ] == 'property' ){

            $mail[ 'property' ][ 'title' ]              =   array_get( $model, 'writeup.title', '' );
            $mail[ 'property' ][ 'link' ]               =   $this->property_link( array_get( $model, 'rent_buy', 'sale' ),  array_get( $model, 'residential_commercial', 'residential' ), array_get( $model, 'writeup.title', '' ), array_get( $model, '_id', '' ) );

            $mail[ 'property' ][ 'ref_no' ]             =   array_get( $model, 'ref_no', '' );
            $mail[ 'property' ][ 'area' ]               =   array_get( $model, 'area', '' );
            $mail[ 'property' ][ 'city' ]               =   array_get( $model, 'city', '' );

            $mail[ 'property' ][ 'type' ]               =   array_get( $model, 'type', '' );
            $mail[ 'property' ][ 'bedroom' ]            =   array_get( $model, 'bedroom', '' );
            $mail[ 'property' ][ 'bathroom' ]           =   array_get( $model, 'bathroom', '' );
            $mail[ 'property' ][ 'builtup_area' ]       =   array_get( $model, 'dimension.builtup_area', '' );

            $mail[ 'property' ][ 'agent' ][ 'name' ]    =   array_get( $model, 'agent.contact.name', '' );
            $mail[ 'property' ][ 'agent' ][ 'email' ]   =   array_get( $model, 'agent.contact.email', '' );
            $mail[ 'property' ][ 'agent' ][ 'phone' ]   =   array_get( $model, 'agent.contact.phone', '' );

            $mail[ 'property' ][ 'agency' ][ 'name' ]   =   array_get( $model, 'agent.agency.contact.name', '' );
            $mail[ 'property' ][ 'agency' ][ 'email' ]  =   array_get( $model, 'agent.agency.contact.email', '' );
            $mail[ 'property' ][ 'agency' ][ 'phone' ]  =   array_get( $model, 'agent.agency.contact.phone', '' );

        }
        //Project
        elseif( $mail[ 'enquiry' ][ 'type' ] == 'project' ){

            $mail[ 'project' ][ 'title' ]                   =   array_get( $model, 'name', '' );
            $mail[ 'project' ][ 'link' ]                    =   $this->project_link( array_get( $model, '_id', '' ) );

            $mail[ 'project' ][ 'developer' ][ 'name' ]     =   array_get( $model, 'developer.contact.name', '' );

        }

        $mail[ 'receiver_email' ]   =   'sales@zoomproperty.com';
        $mail[ 'subject' ]          =   title_case( $type ).' Mortgage Calculator Enquiry';

        $cc     =   [ 'HabibDewan@emiratesislamic.ae', 'AshwaniB@emiratesislamic.ae', 'gary@zoomproperty.com' ];
        $bcc    =   config( 'internal_leads_forwarding' );

        return $this->send_email( $mail, 'emailers.mortgage_enquiry', $cc, $bcc );
    }

    public function register($payload){

        $mail[ 'sender_name' ]      =   'Zoom Property';
        $mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
        $mail[ 'receiver_name' ]    =   explode( " ", array_get( $payload, 'name', 'Beautiful Person' ) )[0];
        $mail[ 'receiver_email' ]   =   array_get( $payload, 'email', 'sarah@wewantzoom.com' );

        $mail[ 'token' ]            =   array_get( $payload, 'token', '' );
        $mail[ 'content' ]          =   'We, at Zoom, welcome you to the world of real estate!';
        $mail[ 'subject' ]          =   'Welcome Email from Zoom!';
        $mail[ 'title' ]            =   'Hi';

        if( !empty( $mail[ 'token' ] ) )
        $this->send_email( $mail, 'emailers.register' );

    }

    public function forgot_password($payload){

        $mail[ 'sender_name' ]      =   'Zoom Property';
        $mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
        $mail[ 'receiver_email' ]   =   ( !empty( $payload[ 'email' ] ) )  ? $payload[ 'email' ]  : 'sarah@wewantzoom.com';

        $mail[ 'token' ]            =   array_get($payload, 'token', '' );
        $mail[ 'content' ]          =   'There was a request to reset your password. Please click below to reset your password.';
        $mail[ 'subject' ]          =   'Forgot Password Email from Zoom!';

        if( !empty( $mail[ 'token' ] ) )
        $this->send_email( $mail, 'emailers.forgot-password' );

    }

    public function book_viewing_email( $payload ){

  		$mail[ 'sender_name' ]      =   'Zoom Property';
  		$mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
  		$mail[ 'receiver_email' ]   =   'salman@zoomproperty.com';

  		$mail[ 'subject' ]          =   'Book a Viewing for Real Choice!';

          if( $payload[ 'interest' ] == 'a' ) {
              $interest = '1 Bedroom';
          } elseif( $payload[ 'interest' ] == 'b' ) {
              $interest = '2 Bedrooms';
          } elseif( $payload[ 'interest' ] == 'c' ) {
              $interest = '3 Bedrooms';
          } else {
              $interest = '';
          }
          $mail[ 'name' ]             = $payload[ 'name' ];
          $mail[ 'email' ]            = $payload[ 'email' ];
          $mail[ 'phone' ]            = $payload[ 'phone' ];
          $mail[ 'interest' ]         = $interest;
          $mail[ 'newsletter' ]       = ( $payload[ 'newsletter' ] == 'true' ) ? 'Yes' : 'No';

          $cc                         = [];
          $bcc                        = config( 'internal_leads_forwarding' );

          $this->send_email( $mail, 'emailers.real_choice', $cc, $bcc );

    }

    public function ideal_property_email( $payload ){

        $mail[ 'sender_name' ]      =   'Zoom Property';
        $mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
        $mail[ 'receiver_email' ]   =   'sales@zoomproperty.com';

        $mail[ 'subject' ]          =   'Ideal Property Lead';

        $mail[ 'name' ]             = $payload[ 'name' ];
        $mail[ 'email' ]            = $payload[ 'email' ];
        $mail[ 'phone' ]            = $payload[ 'phone' ];
        $mail[ 'budget' ]           = $payload[ 'budget' ];
        $mail[ 'type' ]             = $payload[ 'type' ];
        $mail[ 'location' ]         = $payload[ 'location' ];

        $cc                         = [];
        $bcc                        = config( 'internal_leads_forwarding' );

        $this->send_email( $mail, 'emailers.ideal_property', $cc, $bcc );

    }

    public function property_listings_email( $payload ){

        $mail[ 'sender_name' ]      =   'Zoom Property';
        $mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
        $mail[ 'receiver_email' ]   =   'sales@zoomproperty.com';

        $mail[ 'subject' ]          =   'Request for more details';

        $mail[ 'name' ]             =   array_get( $payload, 'user.contact.name', '' );
        $mail[ 'email' ]            =   array_get( $payload, 'user.contact.email', '' );
        $mail[ 'phone' ]            =   array_get( $payload, 'user.contact.phone', '' );
        $mail[ 'message' ]          =   array_get( $payload, 'message', '' );
        $mail[ 'url' ]              =   array_get( $payload, 'url', '' );
        $mail[ 'title' ]            =   array_get( $payload, 'title', '' );

        $cc                         =   [];
        $bcc                        =   config( 'internal_leads_forwarding' );

        $this->send_email( $mail, 'emailers.property_listings', $cc, $bcc );

    }

    public function scholarship_email( $payload, $file ){

        $mail[ 'sender_name' ]      =   'Zoom Property';
        $mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
        $mail[ 'receiver_email' ]   =   ['lisandra@wewantzoom.com','shahid@wewantzoom.com','gary@wewantzoom.com'];

        $mail[ 'subject' ]          =   'Scholarship Application';

        $mail['applicant'][ 'name' ]      = $payload[ 'name' ];
        $mail['applicant'][ 'email' ]     = $payload[ 'email' ];
        $mail['applicant'][ 'college' ]   = $payload[ 'college' ];
        $mail['applicant'][ 'degree' ]    = $payload[ 'degree' ];
        $mail['applicant'][ 'gpa' ]       = $payload[ 'gpa' ];
        $mail['applicant'][ 'aditional' ] = $payload[ 'aditional' ];
        $mail['applicant'][ 'checklist' ] = $payload[ 'checklist' ];
        $mail['applicant'][ 'advice' ]    = $payload[ 'advice' ];
        $mail['applicant'][ 'trends' ]    = $payload[ 'trends' ];

        $cc                         = ['shahid@wewantzoom.com'];
        $bcc                        = [];
        try{
            //FILE
            if(!\File::exists(storage_path().'/temp-use-files')) {
                \File::makeDirectory(storage_path().'/temp-use-files', $mode = 0777, true, true);
            }
            $bucket    =   'zp-scholarship';
            $file_name =   kebab_case_custom($file->getClientOriginalName());
            $extention =   substr( $file_name, strrpos( $file_name, '.' ) + 1);
            $path      =   storage_path().'/temp-use-files/'.$file_name;
            $file = file_get_contents($file);
            file_put_contents($path, $file);
            $aws_services  = new AwsServices;
            $aws_services->put_s3_object( $bucket, $file_name, $path );
            unlink( $path );
            $mail[ 'file' ]             = 'https://zp-scholarship.s3.ap-south-1.amazonaws.com/'.$file_name;
        }
        catch( \Exception $e ){
            return response()->json( [ 'message' => $e->getMessage(), 'line' => $e->getLine() ], 500 );
        }
        $this->send_email( $mail, 'emailers.scholarship', $cc, $bcc );

    }

    public function custom_enquiry_email( $mail ){

        $mail[ 'sender_name' ]      =   'Zoom Property';
        $mail[ 'sender_email' ]     =   'hello@zoomproperty.com';
        $mail[ 'receiver_email' ]   =   'sales@zoomproperty.com';

        $mail[ 'subject' ]          =   'Custom Enquiry';

        $cc                         =   [];
        $bcc                        =   [];

        $this->send_email( $mail, 'emailers.custom_enquiry', $cc, $bcc );

    }

    private function send_email($mail, $view, $cc = [], $bcc = ['gary@zoomproperty.com'], $reply_to_addresses = [ 'Zoom Property <hello@zoomproperty.com>' ]){
        try{
            $receiver = ['lisandra@wewantzoom.com'];
            $aws_services  = new AwsServices;
            $aws_services->send_email(
                $receiver,
                $mail['subject'],
                $view,
                ['mail' => $mail],
                [],
                [],
                $reply_to_addresses
            );
            return response()->json( [ 'message' => 'Email has been sent.' ], 200 );
        }
        catch( \Exception $e ){
            return response()->json( [ 'message' => $e->getMessage(), 'line' => $e->getLine() ], 500 );
        }
    }

}
