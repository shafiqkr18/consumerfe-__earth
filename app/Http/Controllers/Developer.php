<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Developer extends Controller
{
    const leaf = 'agency';

    public function index(){
        if(!array_has($this->payload,'size')){
            array_set($this->payload,'size',100);
        }
        $agencies = json_decode(Parent::index()->getBody()->getContents(),true);
        $developers = [];
        foreach($agencies['data'] as $agency){
            if(array_get($agency, 'settings.role_id') !== 4){
                $developers[] = $agency;
            }
        }
        $response['total']  = count($developers);
        $response['data']   = $developers;
        return response()->json($response,200);
    }
}
