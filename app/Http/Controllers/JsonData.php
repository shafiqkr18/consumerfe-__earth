<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

class JsonData extends BaseController
{
    public function property(){
        //TODO define langs file globaly
        $langs         =   config('languages');
        $search_data   =   include( storage_path( 'search/search.php' ) );
        $search_data   =   $search_data[ 'property' ];

        foreach($langs as $lang => $desc){
            $data_lang = include(resource_path('lang/'.$lang.'/data.php'));
            $page_lang = include(resource_path('lang/'.$lang.'/page.php'));

            $lang_key = $lang == 'en' ? '' : '_'.$lang;

            //Furnishing
            foreach($search_data['furnishing'] as $key => $val){
                $val = array_has($data_lang, 'furnishing.'.snake_case_custom($val)) ? array_get($data_lang,'furnishing.'.snake_case_custom($val)) : $val;
                ${"furnishing" . $lang_key}[]     =   array( 'name' => $val, 'value' => $key );
            }
            //Alert Frequency
            foreach($search_data['alert']['frequency'] as $key => $val){
                $val = array_has($data_lang, 'frequency.'.snake_case_custom($val)) ? array_get($data_lang,'frequency.'.snake_case_custom($val)) : $val;
                $alert["frequency" . $lang_key][]     =   array( 'name' => $val, 'value' => $key );
            }
            //Frequency
            foreach($search_data['rental_period'] as $key => $val){
                $key = array_has($data_lang, 'frequency.'.snake_case_custom($key)) ? array_get($data_lang,'frequency.'.snake_case_custom($key)) : $key;
                ${"frequency" . $lang_key}[]     =   array( 'name' => $key, 'value' => $val );
            }
            //Bedroom
            foreach( $search_data['bedroom'] as $key => $val ){
                if( $val !== '' ){
                    $key = trans_choice( array_get($page_lang,'global.bed'), $val, ['bed' => $val] );
                    ${"bedroom" . $lang_key}[]   =   array( 'name' => $key, 'value' => $val );
                }
            }
            //Bathroom
            foreach( $search_data['bathroom'] as $key => $val ){
                if( $val !== '' ){
                    $key = trans_choice( array_get($page_lang,'global.bath'), $val, ['bath' => $val] );
                    ${"bathroom" . $lang_key}[]   =   array( 'name' => $key, 'value' => $val );
                }
            }

            foreach( $search_data['prices']['rent'] as $key => $val ){
                if( $val !== '' ){
                    $curr = array_get($page_lang,'global.aed');
                    ${"price" . $lang_key}['rent'][]   =   array( 'name' => $curr.' '.number_format( (int)$val ), 'value' => $val );
                }
            }

            foreach( $search_data['prices']['sale'] as $key => $val ){
                if( $val !== '' ){
                    $curr = array_get($page_lang,'global.aed');
                    ${"price" . $lang_key}['sale'][]   =   array( 'name' => $curr.' '.number_format( (int)$val ), 'value' => $val );
                }
            }

            //Build up area NOTE All languages
            foreach( $search_data['area'] as $key => $val ){
              if( $val !== ''){
                $curr = array_get($page_lang,'global.sqft_long');
                ${"areas" . $lang_key}[]   =   array( 'name' => number_format( (int)$val ).' '.$curr, 'value' => $val );
              }
            }
            //Residential Commercial
            foreach( $search_data['residential_commercial'] as $key => $val )
            if( $val !== '' ){
                $key = array_has($data_lang, 'residential_commercial.'.snake_case_custom($key)) ? array_get($data_lang,'residential_commercial.'.snake_case_custom($key)) : $key;
                ${"residential_commercial" . $lang_key}[]   =   array( 'name' => $key, 'value' => $val );
            }

            //Rent/Buy
            foreach($search_data['rent_buy'] as $key => $val){
                $key = array_has($data_lang, 'rent_buy.'.snake_case_custom($key)) ? array_get($data_lang,'rent_buy.'.snake_case_custom($key)) : $key;
                ${"rent_buy_data" . $lang_key}[]     =   array( 'name' => $key, 'value' => $val );
            }
            ${"rent_buy_data" . $lang_key}[0]['selected'] = true;

            //City
            foreach($search_data['cities'] as $key => $val){
                $key = array_has($data_lang, 'city.'.snake_case_custom($val)) ? array_get($data_lang,'city.'.snake_case_custom($val)) : $val;
                ${"cities" . $lang_key}[]     =   array( 'name' => $key, 'value' => $val );
            }
            //Property Type
            $property_type  = [];
            foreach($search_data['types'] as $residential => $rent_buy){
                foreach($rent_buy as $key => $types){
                    foreach($types as $type){
                        $type_key = array_has($data_lang, 'type.'.snake_case_custom($type)) ? array_get($data_lang,'type.'.snake_case_custom($type)) : $type;
                        ${"property_type" . $lang_key}[$residential][$key][]    = array( 'name' => $type_key, 'value' => $type );
                    }
                }
            }
            if(!isset(${"property_type" . $lang_key}['residential']['short_term_rent'])){
                ${"property_type" . $lang_key}['residential']['short_term_rent'] = ${"property_type" . $lang}['residential']['rent'];
            }
            //Locations
            $locations  = [];
            foreach($search_data['locations'] as $key => $location){
                $locs = explode('-',$location);
                $location = [];
                if(count($locs) == 1){
                    $city = trim($locs[0]);

                    $location[ 'label' ][]        =   array_has($data_lang, 'city.'.snake_case_custom($city)) ? array_get($data_lang,'city.'.snake_case_custom($city)) : $city;
                    $location[ 'value' ][]        =   $city;
                }
                if(count($locs) == 2){
                    $area = trim($locs[0]);
                    $city = trim($locs[1]);

                    $location[ 'label' ][]        =   array_has($data_lang, 'area.'.snake_case_custom($area)) ? array_get($data_lang,'area.'.snake_case_custom($area)) : $area;
                    $location[ 'value' ][]        =   $area;
                    $location[ 'label' ][]        =   array_has($data_lang, 'city.'.snake_case_custom($city)) ? array_get($data_lang,'city.'.snake_case_custom($city)) : $city;
                    $location[ 'value' ][]        =   $city;
                }
                if(count($locs) == 3){
                    $building   =   trim($locs[0]);
                    $area       =   trim($locs[1]);
                    $city       =   trim($locs[2]);

                    $location[ 'label' ][]    =   array_has($data_lang, 'building.'.snake_case_custom($building)) ? array_get($data_lang,'building.'.snake_case_custom($building)) : $building;
                    $location[ 'value' ][]    =   $building;
                    $location[ 'label' ][]    =   array_has($data_lang, 'area.'.snake_case_custom($area)) ? array_get($data_lang,'area.'.snake_case_custom($area)) : $area;
                    $location[ 'value' ][]    =   $area;
                    $location[ 'label' ][]    =   array_has($data_lang, 'city.'.snake_case_custom($city)) ? array_get($data_lang,'city.'.snake_case_custom($city)) : $city;
                    $location[ 'value' ][]    =   $city;
                }

                $label = implode(array_get($location, 'label',[] ),' - ');
                $value = implode(array_get($location, 'value',[] ),'-');

                ${"locations" . $lang_key}[]    =   array( 'label' => $label, 'name' => $label, 'value' => $value );
            }
            //Completion Status
            foreach($search_data['completion_status'] as $key => $comp){
                $name = array_has($data_lang, 'completion_status.'.snake_case_custom($comp['value'])) ? array_get($data_lang,'completion_status.'.snake_case_custom($comp['value'])) : $comp['name'];
                ${"completion_status" . $lang_key}[]     =   array( 'name' => $name, 'value' => $comp['value'] );
            }
            $property_search_fields[ 'location'. $lang_key ]                =   ${"locations" . $lang_key};
            $property_search_fields[ "property_type" . $lang_key ]          =   ${"property_type" . $lang_key};
            $property_search_fields[ "cities" . $lang_key ]                 =   ${"cities" . $lang_key};
            $property_search_fields[ "rent_buy" . $lang_key ]               =   ${"rent_buy_data" . $lang_key};
            $property_search_fields[ "residential_commercial" . $lang_key ] =   ${"residential_commercial" . $lang_key};
            $property_search_fields[ "area" . $lang_key ]                   =   ${"areas" . $lang_key};
            $property_search_fields[ 'price' . $lang_key ]                  =   ${"price" . $lang_key};
            $property_search_fields[ 'bathroom' . $lang_key ]               =   ${"bathroom" . $lang_key};
            $property_search_fields[ 'bedroom' . $lang_key ]                =   ${"bedroom" . $lang_key};
            $property_search_fields[ "frequency" . $lang_key ]              =   ${"frequency" . $lang_key};
            $property_search_fields[ "furnishing" . $lang_key ]             =   ${"furnishing" . $lang_key};
            $property_search_fields[ "completion_status" . $lang_key ]      =   ${"completion_status" . $lang_key};
            $property_search_fields[ 'alert' ]                              =   $alert;
        }

        return response()->json( [ 'json' => [ 'search' => $property_search_fields ] ], 200 );
    }

    public function brokerages(){
        $langs              =   config('languages');

        $search_data        =   include( storage_path( 'search/search.php' ) );
        $brokerage_data     =   $search_data[ 'brokerage' ];
        $broker_data        =   $search_data[ 'broker' ];

        foreach($langs as $lang => $desc){
            $data_lang  =   include(resource_path('lang/'.$lang.'/data.php'));
            $lang_key   =   $lang == 'en' ? '' : '_'.$lang;

            // areas
            foreach($brokerage_data['areas'] as $key => $val){
                $value = array_has($data_lang, 'area.'.snake_case_custom($val)) ? array_get($data_lang,'area.'.snake_case_custom($val)) : $val;
                ${"areas" . $lang_key}[]     =   array( 'name' => $value, 'value' => $val );
            }
            $brokerage_search_fields[ 'areas' . $lang_key ]      =   ${"areas" . $lang_key};

            //brokerages
            foreach($brokerage_data['list'] as $key => $val){
                $value = array_has($data_lang, 'list.'.snake_case_custom($val)) ? array_get($data_lang,'list.'.snake_case_custom($val)) : $val;
                ${"list" . $lang_key}[]     =   array( 'name' => $value, 'value' => $key );
            }
            $brokerage_search_fields[ 'list' . $lang_key ]      =   ${"list" . $lang_key};

            //brokers
            foreach($broker_data as $key_brokerage => $broker_val){
                foreach($broker_val as $key => $val){
                    $value = array_has($data_lang, 'brokers.'.snake_case_custom($val)) ? array_get($data_lang,'brokers.'.snake_case_custom($val)) : $val;
                    ${"brokers" . $lang_key}[$key_brokerage][]     =   array( 'name' => $value, 'value' => $key );
                }
            }
            $brokerage_search_fields[ 'brokers' . $lang_key ]      =   ${"brokers" . $lang_key};
        }

        return response()->json( [ 'json' => [ 'search' => $brokerage_search_fields ] ], 200 );
    }

    public function services(){

        $langs         =    config('languages');
        $service_types =    json_decode(app('\App\Http\Controllers\Service')->types()->getBody()->getContents(),true);
        //services
        foreach($langs as $lang => $desc){
            $data_lang = include(resource_path('lang/'.$lang.'/data.php'));

            $lang_key   = $lang == 'en' ? '' : '_'.$lang;

            foreach($service_types['data'] as $key => $val){
                $value = array_has($data_lang, 'ancillary.'.snake_case_custom($val['name'])) ? array_get($data_lang,'ancillary.'.snake_case_custom($val['name'])) : $val['name'];
                ${"services" . $lang_key}[]     =   array( 'name' => $value, 'value' => $val['_id'] );
            }
            $service_search_fields['list'.$lang_key]   =   ${"services" . $lang_key};
        }

        return response()->json( [ 'json' => [ 'search' => $service_search_fields ] ], 200 );
    }

    public function projects(){
        $langs         =   config('languages');
        $search_data   =   include( storage_path( 'search/search.php' ) );
        $search_data   =   $search_data[ 'project' ];

        foreach($langs as $lang => $desc){
            //TODO include language translations when available
            $lang_key   = $lang == 'en' ? '' : '_'.$lang;

            $locations  = [];
            foreach($search_data['locations'] as $country => $location){
                $location = array_get($location,'locations');
                foreach($location as $loc){
                    $value = str_replace(" - ", "-", $loc);
                    ${"locations" . $lang_key}[$country]['locations'][]    = array( 'label' => $loc, 'name' => $loc, 'value' => $value );
                }
            }

            $project_search_fields['developers'.$lang_key]        =   array_get($search_data,'developers');
            $project_search_fields['completion_status'.$lang_key] =   array_get($search_data,'completion_status');
            $project_search_fields['locations'.$lang_key]         =   ${"locations" . $lang_key};
        }

        return response()->json( [ 'json' => [ 'search' => $project_search_fields ] ], 200 );
    }
}
