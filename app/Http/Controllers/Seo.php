<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Seo extends BaseController
{
    const leaf = 'seo';

    use \App\Traits\ApiCaller;
    use \App\Traits\JsonValidator;
    protected $payload, $config,$api_gateway;

    public function __construct(Request $request = null){
        $this->payload  =   !empty( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $this->config   =   config('models.'.$this::leaf);
        $this->api_gateway  =   env('APP_ENV') == 'production' ? env('API_GATEWAY_URL_PRODUCTION') : env('API_GATEWAY_URL_STAGING');
    }

    public function get_details(){
        $this->api_endpoint     =   array_get($this->config,'api.endpoint.d','');
        if(in_array(env('APP_ENV'),[ 'production', 'staging', 'local' ])){
            array_set($this->payload,'seo_domain',array(env('APP_ENV')));
        }
        return $this    ->set_method('GET')
                        ->build_payload($this->payload)
                        ->call_api($this->api_gateway.$this->api_endpoint);
    }

    public function set_payload(array $payload){
        $this->payload  =   $payload;
        return $this;
    }
}
