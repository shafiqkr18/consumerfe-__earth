<?php

if (! function_exists('cloudfront_asset')) {
    /**
     * @param  array  $array
     * @return array
     */
    function cloudfront_asset(string $asset_url = '')
    {
        return env('CLOUDFRONT_URL').$asset_url;
    }
}

if (! function_exists('cdn_asset_version')) {
    /**
     * @param  array  $array
     * @return array
     */
    function cdn_asset_version(string $asset_url = '')
    {
        return preg_replace('/([^:])(\/{2,})/', '$1/', cdn_asset(mix($asset_url)));
    }
}

if (! function_exists('snake_case_custom')) {
    /**
     * @param  array  $array
     * @return array
     */
    function snake_case_custom($text = '')
    {
      if(is_null($text))
        return null;
      else if(is_string($text))
        return strtolower(preg_replace(['/\(|\)/', '/\s+/','/_{2,}/'],['', '_','_'],$text));
      else
        return response()->json(['message' => 'Value expected must be String'],404);
    }
}

if(! function_exists('array_undot'))
{
    function array_undot($array_dot){
        $array = array();
        foreach ($array_dot as $key => $value){
            array_set($array, $key, $value);
        }
        return $array;
    }
}

if (! function_exists('kebab_case_custom')) {
    /**
    * @param  array  $array
    * @return array
    */
    function kebab_case_custom($text = '', $withoutBracket = '')
    {
        if(is_null($text)){
            return null;
        }
        else if(is_string($text)){
            $patterns       = ['/\(|\)/','/\s+/','/_{2,}/'];
            $replaceWith    = ['','-','-'];
            if($withoutBracket){
                array_unshift($patterns,'/\(([^()]*+|(?R))*\)/');
                array_unshift($replaceWith,'');
            }
            return trim(strtolower(preg_replace($patterns,$replaceWith,$text)), '-');
        }
        else{
            return response()->json(['message' => 'Value expected must be String'],404);
        }
    }
}

if (! function_exists('array_dot_only')) {
    /**
     * @param  array  $array
     * @return array
     */
    function array_dot_only($array, $keys) {
        $newArray = [];
        foreach((array) $keys as $key) {
            array_set($newArray, $key, array_get($array, $key));
        }
        return $newArray;
    }
}

if (! function_exists('get_links')) {
    /**
     * @param  array  $array
     * @return array
     */
    function get_links() {
        $languages  =   array_keys(config('languages'));
        $lang_with_slashes = array_map(function($str) {
          return '/'.$str.'/';
        }, $languages);
        $langs = array_combine($languages,$lang_with_slashes);
        $links = [];

        if(!empty($langs)){
            foreach($langs as $key => $value){
                if(str_contains(\Request::fullUrl(),$lang_with_slashes)){
                    $links[$key]    =   str_replace('/'.\Request::segment(1).'/',$value,\Request::fullUrl());
                }
                else{
                    if(!empty(\Request::segment(1))){
                        $value          =   rtrim($value, '/');
                        $links[$key]    =   str_replace('/'.\Request::segment(1),$value,\Request::fullUrl());
                    }
                    else{
                        $links[$key]    =   \Request::root().'/'.$key;
                    }
                }
            }
        }

        foreach($links as $link_key => $link_val){
            if(ends_with($link_val, '/en')){
                $links[$link_key]    =   str_replace('/en','',$link_val);
            }
        }

        return $links;
    }
}

if (! function_exists('detect_user_languages')) {
    /**
     * @param  array  $array
     * @return array
     */
    function detect_user_languages() {
        $user_langs     =   array();
        $return_lang    =   'en';

        if (\Request::server('HTTP_ACCEPT_LANGUAGE')) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', \Request::server('HTTP_ACCEPT_LANGUAGE'), $lang_parse);

            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $user_langs = array_combine($lang_parse[1], $lang_parse[4]);

                // set default to 1 for any without q factor
                foreach ($user_langs as $lang => $val) {
                    if ($val === '') $user_langs[$lang] = 1;
                }

                // sort list based on value
                arsort($user_langs, SORT_NUMERIC);
            }
            // replace zh with ch in the array for chinese
            $json = str_replace('"zh":', '"ch":', json_encode($user_langs));
            $user_langs = json_decode($json,1);
        }

        // look through sorted list and use first one that matches our languages
        foreach ($user_langs as $user_lang => $val) {
            $our_langs  =   array_keys(config('languages'));
            $flag = 0;
            foreach($our_langs as $our_lang){
                if (strpos($user_lang, $our_lang) === 0) {
                    $return_lang = $our_lang;
                    $flag++;
                    break;
                }
            }
            if($flag>0) break;
        }

        return $return_lang;
    }
}

if (! function_exists('detect_client_ip')){

    function detect_client_ip() {
        $ipaddress = '';

        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
}

if (! function_exists('nearest_string')){

    function nearest_string(array $strArr,string $str){
        $minStr =   "";
        $minDis =   PHP_INT_MAX;
        foreach($strArr as $curStr) {
            $dis = levenshtein($str, $curStr);
            if ($dis < $minDis) {
                $minDis = $dis;
                $minStr = $curStr;
            }
        }
        return $minStr;
    }
}

if (! function_exists('closest_string')){

    function closest_string(array $strArr,string $str){
        // no shortest distance found, yet
        $shortest = -1;

        // loop through words to find the closest
        foreach ($strArr as $word) {

            // calculate the distance between the input word,
            // and the current word
            $lev = levenshtein($str, $word);

            // check for an exact match
            if ($lev == 0) {

                // closest word is this one (exact match)
                $closest = $word;
                $shortest = 0;

                // break out of the loop; we've found an exact match
                break;
            }

            // if this distance is less than the next found shortest
            // distance, OR if a next shortest word has not yet been found
            if ($lev <= $shortest || $shortest < 0) {
                // set the closest match, and shortest distance
                $closest  = $word;
                $shortest = $lev;
            }
        }

        return $closest;
    }
}

if (! function_exists('remove_url_params')){

    function remove_url_params(string $url,array $params_to_removes){
        $parts = parse_url($url);
        //grab the query part
        parse_str(array_get($parts,'query'), $query);
        //remove parameters from query
        foreach($params_to_removes as $param){
            unset($query[$param]);
        }
        $dest_query =   http_build_query($query);
        $url_path   =   strtok($url,'?');
        $url        =   $url_path.(!empty($query) ? '?'.$dest_query : '');

        return $url;
    }
}
