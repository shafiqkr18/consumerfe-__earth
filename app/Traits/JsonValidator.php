<?php

namespace App\Traits;
use Illuminate\Http\Response;

trait JsonValidator
{

    protected   $leaf, $payload, $method;

    public function set_leaf($leaf){
        $this->leaf     =   $leaf;
        return $this;
    }

    public function set_method_name($method){
        $this->method     =   $method;
        return $this;
    }

    public function set_payload(Array $payload){
        $this->payload  =   $payload;
        return $this;
    }

    public function validate_request( $schema_path='' ){

        /**
         *  1.   Pick schema
         *  4.   Initiate validation
         *  5.   Return results
         */

        $schema_path        =   ( !empty($schema_path) ) ? $schema_path : storage_path().'/schemas/'.env('API_VERSION').'/request.json';

        $leaf               =   $this->leaf;
        $method             =   ( !empty($this->method) ) ? $this->method : '';
        $payload            =   (object)json_decode(json_encode($this->payload),false);

        // dd($leaf, $method);

        #   1
        $schemas            =   json_decode( file_get_contents( $schema_path ) );
        $schema             =   ( !empty($method) ) ? $schemas->schema->$leaf->$method : $schemas->definitions->$leaf;

        // if( !empty($method) ) dd($payload, $schema);

        $schemaStorage      =   new \JsonSchema\SchemaStorage();
        $schemaStorage->addSchema( $schema_path, $schema );

        #   2
        $validator          =   new \JsonSchema\Validator( new \JsonSchema\Constraints\Factory($schemaStorage));
        $validator->validate( $payload, $schema );

        // dd( $leaf,$payload,$schema,$validator );

        $errors             =   [];

        #   3
        if ($validator->isValid()) {
            return true;
        }
        else {
            //JSON does not validate. Violations
            foreach ($validator->getErrors() as $error) {
                $errors[$error['property']] = sprintf("%s",$error['message']);
            }
            return $errors;
        }

    }

    public function validate_response( $response, $leaf_cruds ){
        // dd($response, $leaf_cruds);

        list($leaf,$method)     =   explode(".",$leaf_cruds);

        #   3
        $schema_path        =   storage_path().'/schemas/'.env('API_VERSION').'/response.json';
        $schemas            =   json_decode( file_get_contents( $schema_path ) );
        $schema             =   $schemas->schema->$leaf->$method;

        // dd($schemas,$schema);

        $schemaStorage      =   new \JsonSchema\SchemaStorage();
        $schemaStorage->addSchema( $schema_path, $schema );

        #   4
        $validator          =   new \JsonSchema\Validator( new \JsonSchema\Constraints\Factory($schemaStorage));
        $validator->validate( $response, $schema, \JsonSchema\Constraints\Constraint::CHECK_MODE_APPLY_DEFAULTS );

        // dd( $leaf,$response,$schema,$validator );

        $errors             =   [];

        if ($validator->isValid()) {
            return true;
        }
        else {
            echo "JSON does not validate. Violations:\n";
            foreach ($validator->getErrors() as $error) {
                echo "<br>".sprintf("[%s] %s\n", $error['property'], $error['message']);
            }
        }

    }

}
