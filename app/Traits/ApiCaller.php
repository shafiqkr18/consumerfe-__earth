<?php

namespace App\Traits;

trait ApiCaller
{
    protected $payload, $client, $method;

    public function set_method($m){
        $this->method   =   $m;
        return $this;
    }

    public function build_payload($d){

        if( strtolower( $this->method ) === 'get' ){
            $this->payload  =   ['query'=>['query' => json_encode($d)]];
        }
        else if( strtolower( $this->method ) === 'post' ){
            $this->payload  =   ['json'=> $d];
        }
        else if( strtolower( $this->method ) === 'put' ){
            $this->payload  =   [['Content-Type' => 'application/x-www-form-urlencoded'],'form_params'=> $d];
        }
        else if( strtolower( $this->method ) === 'delete' ){
            $this->payload  =   ['json'=>$d];
        }
        $this->client   =   new \GuzzleHttp\Client( ['headers' => ['x-api-key' => 'SyhdKLOjjZ66fXXLbD8z77eVAAC9RaKRiY9H2Vwe']] );

        return $this;
    }

    public function call_api($p){
        // echo json_encode( $this->payload, JSON_PRETTY_PRINT ); die();
      // dd($this->method,$p,$this->payload);
        try{
            return $this->client->request($this->method,$p,$this->payload);
        }
        catch( \GuzzleHttp\Exception\RequestException $e ){
            if( $e->hasResponse() ) {
                if( in_array( $e->getResponse()->getStatusCode(), array( 301, 403, 404, 405 ) ) OR floor( $e->getResponse()->getStatusCode() / 100 ) === 5.0 ){
                    return  response()->json( array( 'error' => 'Service Currently Unavailable' ), 503 );
                }
                else{
                    return response()->json( json_decode( $e->getResponse()->getBody()->getContents(), 1 ), $e->getResponse()->getStatusCode() );
                }
            }
            else{
                return response()->json( array( 'message' => 'No response' ), 400 );
            }
        }

    }
}
