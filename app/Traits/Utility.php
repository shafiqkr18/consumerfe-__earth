<?php

namespace App\Traits;

trait Utility
{
    public function search_json( $payload, $json_data ){

        $payload    =   array_dot( $payload );

        // Group arrays together
        foreach( $payload as $k => $v ){
            // Identify and group min, max range
            $suffix     =   substr(strrchr($k, "."), 1);

            if( $suffix == 'min' || $suffix == 'max' ){
                if( preg_match('/(.*?).'.$suffix.'/', $k, $match) == 1 ){
                    $payload[$match[1]][$suffix]   =  $v;
                }
                array_forget( $payload, $k );
            }
            else{
                if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 ){
                    $payload[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                    array_forget( $payload, $k );
                }
                else{
                    $payload[ $k ]   =  $v;
                }
            }
        }

        $size       =   array_get( $payload, 'size', 50 );
        array_forget( $payload, 'size' );

        //Filter results
        $results    =   collect( $json_data );
        foreach( $payload as $keys => $value ){
            $results     =   $results->filter(function( $resource ) use( $keys, $value, $payload ) {
                $resource    =   array_dot( $resource );
                foreach( $resource as $k => $v ){
                    if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 ){
                        $resource[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                        array_forget( $resource, $k );
                    }
                    else{
                        $resource[ $k ]   =  $v;
                    }
                }

                if( is_array( $value ) ){
                    foreach( $value as $k => $v ){
                        if( $k === 'min' || $k === 'max' ){
                            if( $k === 'min' ){
                                return $payload[ $keys ][ $k ] <= $resource[ $keys ];
                            }
                            else{
                                return $payload[ $keys ][ $k ] >= $resource[ $keys ];
                            }
                        }
                        else{
                            if( !empty($resource[ $keys ]) && is_array($resource[ $keys ])){
                                foreach( $value as $i => $j ){
                                    return in_array( $j, $resource[ $keys ] );
                                }
                            }
                            else{
                                return in_array( $resource[ $keys ], $value );
                            }
                        }
                    }
                }
                elseif(!empty($resource[ $keys ])){
                    return $payload[ $keys ] == $resource[ $keys ];
                }

            });
        }

        return [ count( $results ), $results->slice( 0, $size )->values()->toArray() ];

    }

    public function property_link(...$var){

        $rent_buy       =   strtolower(array_get( $var, 0, 'sale' ));
        $resi_comm      =   strtolower(array_get( $var, 1, 'residential' ));
        $title			=	strtolower( preg_replace('/-+/', '-', preg_replace( '/[^a-zA-Z0-9\-]/', '-', trim( array_get( $var, 2, '' ) ) ) ) );
        $id             =   array_get( $var, 3, '' );

        $lng            =   \App::getLocale();

        return env( 'ENV_URL' ).$lng.'/'.$rent_buy.'/'.$resi_comm.'/'.$title.'/'.$id;
    }

    public function project_link(...$var){

        $id             =   array_get( $var, 0, '' );
        $lng            =   \App::getLocale();

        return env( 'ENV_URL' ).$lng.'/projects/details/'.$id;
    }

    public function property_search_url_encode($search_data){
        // dd($search_data);
        array_forget( $search_data, 'consumer' );
        // Constructing query
        $q = [];

        // Setting Bedrooms
        $bedroom_min = array_get( $search_data, 'bedroom.min' );
        $bedroom_max = array_get( $search_data, 'bedroom.max' );

        $bedroom_min = ($bedroom_min == 0) ? 'studio' : $bedroom_min;
        $bedroom_max = ($bedroom_max == 0) ? 'studio' : $bedroom_max;

        $bedroom_str = [];
        if(array_has($search_data, 'bedroom.min')) {
            $bedroom_str[]    =   $bedroom_min;
        }
        if(array_has($search_data, 'bedroom.max')) {
            if(empty($bathroom_str))
                $bathroom_str[]  =  0;
            if( $bedroom_max !== $bedroom_min ){
                $bedroom_str[]  =   "to";
            }
            $bedroom_str[]  =   $bedroom_max;
        }
        $bedroom_str    =   array_unique($bedroom_str);
        if(!empty($bedroom_str)){
            $bedroom_str[]  =   "bedroom";
            $q['bedroom']   =   implode("-", $bedroom_str);
        }
        // Property Type
        if(array_has( $search_data, 'type')) {
            $q['type']   =   str_replace(' ', '-',implode("-or-", array_get($search_data, 'type')));
        }
        // Rent or Sale
        if(array_has($search_data, 'rent_buy.0')) {
            $q['rent_buy']   =   str_replace(' ','-',array_get( $search_data, 'rent_buy.0'));
        }
        // Residential Commercial
        if(array_has($search_data, 'residential_commercial')) {
            $q['residential_commercial']   =   array_get( $search_data, 'residential_commercial');
        }
        // Building
        if(array_has($search_data, 'building')) {
            $q['building']   =   str_replace(' ', '-',implode("-or-", array_get( $search_data, 'building'))).'-building';
        }
        // Area
        if(array_has($search_data, 'area')) {
            $q['area']   =   str_replace(' ', '-',implode("-or-", array_get( $search_data, 'area'))).'-area';
        }
        // City
        if(array_has($search_data, 'city')) {
            $q['city']   =    preg_replace( '/ *\([^)]*\) */', '',str_replace(' ', '-',implode("-or-", array_get( $search_data, 'city')))).'-city';
        }
        // Price min
        if ( array_has( $search_data, 'price.min' ) ) {
            $q['price_min']   =   "price-greater-than-" . array_get( $search_data, 'price.min' ) . "-aed";
        }
        // Price max
        if ( array_has( $search_data, 'price.max' ) ) {
            $q['price_max']   =   "price-less-than-" . array_get( $search_data, 'price.max' ) . "-aed";
        }
        // Setting Bathroom
        $bathroom_str = [];
        if(array_has($search_data, 'bathroom.min')) {
          $bathroom_str[]    =   array_get( $search_data, 'bathroom.min' );
        }
        if(array_has($search_data, 'bathroom.max')) {
          if(empty($bathroom_str))
            $bathroom_str[]  =   0;
          $bathroom_str[]  =   "to";
          $bathroom_str[]  =   array_get( $search_data, 'bathroom.max' );
        }
        if(!empty($bathroom_str)){
            $bathroom_str[]  =   "bathroom";
            $q['bathroom']   =   implode("-", $bathroom_str);
        }
        if( array_has( $search_data, 'featured' ) ){
            $q['featured']['status']   =   true;
        }

        $keyword    =   array_has($search_data,'keyword','') ? kebab_case(array_get($search_data,'keyword').'-') : '';

        // Generating URL
        $url            =   [];
        $buckets        =   [];
        $seo_buckets    =   [ 'rent_buy', 'city', 'area', 'building' ];
        $seo_buckets    =   array_intersect($seo_buckets, array_keys($q));
        $length         =   count($seo_buckets)-1 == 0 ? count($seo_buckets) : count($seo_buckets)-1;
        for ( $i = 0; $i < $length; $i++ ){
            $v = $seo_buckets[$i];
            if($v == 'rent_buy' && !empty($q[$v])){
                $q[$v]  =   strtolower($q[$v]);
                $q[$v]  =   strtolower(array_get($q,'residential_commercial.0','Residential'))."-" . $q[$v];

                $p      =	explode('-', $q[$v], 2);
                $url[]  =   ( $p[1] == 'sale' ) ?  'buy' : $p[1];
                $url[]  =   $p[0];
                $q[$v] 	=	$p[1];
            }
            else{
                if($v == 'city' && !empty($q[$v])){
                    $url[] = str_replace( '-city', '', $q[$v] );
                }
                if($v == 'area' && !empty($q[$v])){
                    $url[] = str_replace( '-area', '', $q[$v] );
                }
                if($v == 'building' && !empty($q[$v])){
                    $url[] = str_replace( '-building', '', $q[$v] );
                }
                array_forget( $q, $v );
            }

        }
        $url = strtolower(implode( "/", $url ));

        // Generating Query String
        $query_string       =   '';
        $query_params_map   =   config('data.property.query_params');
        if(array_has($search_data,'keyword') && array_has($search_data,'keyword_in_path')){
            array_forget($query_params_map,'keyword');
        }
        if(array_get($search_data,'page',1) != 1){
            array_set($query_params_map,'page','page');
        }
        $query_params_data   =   array_dot_only($search_data,array_values($query_params_map));
        $query_params_data   =   array_filter(array_dot($query_params_data));

        if(!empty($query_params_data)){
            $query_params_map   =   array_flip($query_params_map);
            $query_params       =   [];
            foreach($query_params_data as $key => $value){
                $query_params[$query_params_map[$key]] =  $value;
            }
            $query_string   =   http_build_query($query_params);
        }

        // Prefixes
        $prefixes[0] = [];
        $prefixes[1] = [];
        $prefixes[2] = [];
        $prefixes[3] = [];
        $prefixes[4] = [];
        $prefixes[5] = [];
        $prefixes[6] = [];
        $prefixes[7] = [];
        $prefixes[8] = [];
        $prefixes[9] = [];
        $prefixes[10] = [];
        $prefixes[0]['field'] = "bedroom";
        $prefixes[0]['prefix'] = "";
        $prefixes[1]['field'] = "type";
        $prefixes[1]['prefix'] = "-";
        $prefixes[2]['field'] = "rent_buy";
        $prefixes[2]['prefix'] = "-for-";
        $prefixes[3]['field'] = "area";
        $prefixes[3]['prefix'] = "-in-";
        $prefixes[4]['field'] = "building";
        $prefixes[4]['prefix'] = "-in-";
        $prefixes[5]['field'] = "city";
        $prefixes[5]['prefix'] = "-in-";
        $prefixes[6]['field'] = "price_min";
        $prefixes[6]['prefix'] = "-";
        $prefixes[7]['field'] = "price_max";
        $prefixes[7]['prefix'] = "-";
        $prefixes[8]['field'] = "area_min";
        $prefixes[8]['prefix'] = "-";
        $prefixes[9]['field'] = "area_max";
        $prefixes[9]['prefix'] = "-";
        $prefixes[10]['field'] = "bathroom";
        $prefixes[10]['prefix'] = "-with-";

        $url_string = [];
        $first_one  = true;

        for($i = 0; $i < count( $prefixes ); $i++) {

            $field = $prefixes[$i]['field'];
            $prefix = $prefixes[$i]['prefix'];

            if ( array_has( $q, $field ) ) {
                if (!$first_one) {
                    $url_string[]   =   $prefix . $q[$field];
                }
                else {
                    $url_string[]   =   $q[$field];
                    $first_one      =   false;
                }
            }
        }

        $url_string =   preg_replace( "/-+/", "-", str_replace( " ", "-", implode( "-", $url_string ) ) );

        $slug       =   ( !empty( $search_data['featured']['status'] ) ? 'featured-' : '' ) . 'properties-for-';
        $slug       =   ( !empty( $search_data[ 'type' ] ) || isset( $search_data[ 'bedroom' ][ 'min' ] ) || isset( $search_data[ 'bedroom' ][ 'max' ] ) ) ? '' : $slug;
        $url       .=   "/" . $keyword . $slug . $url_string;
        $lng        =   \App::getLocale();
        $urlPath    =   strtolower(env( 'APP_URL' ) . $lng . '/' . $url);
        $urlPath    =   !empty($query_string) ? $urlPath.'?'.$query_string : $urlPath;

        return $urlPath;
    }

    public function property_types($plural=false){
        $search_data    =   include( storage_path( 'search/search.php' ) );
        $search_data    =   $search_data[ 'property' ][ 'types' ];
        $property_types =   array_values(array_unique(array_flatten($search_data)));

        return  array_combine($property_types, ($plural ? array_map('str_plural', $property_types) : $property_types));
    }

    public function property_type_alias($plural=false){
        $property_type_alias    =   config('data.seo.property_type.alias');
        $property_type_aliases  =   [];
        $all_property_types     =   $this->property_types();
        foreach($all_property_types as $property_type){
            if($plural){
                $property_type_aliases[$property_type]   =    str_plural(array_get($property_type_alias,$property_type,$property_type));
            }
            else {
                $property_type_aliases[$property_type]   =    array_get($property_type_alias,$property_type,$property_type);
            }
        }
        return $property_type_aliases;
    }

    public function keywords(){
        $keywords   =   include( storage_path( 'search/keywords.php' ) );
        $keywords   =   collect($keywords[ 'keywords' ]);
        $keywords   =   $keywords->pluck('value')->toArray();
        return $keywords;
    }
}
