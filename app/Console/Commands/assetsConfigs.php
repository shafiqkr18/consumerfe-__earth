<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Utility;

class assetsConfigs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:configs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Zoomproperty configs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle(Utility $utility)
     {
         $utility->populate_search_config();
     }
}
