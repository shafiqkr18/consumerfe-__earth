<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class assetsUploadMin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:upload-min';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload minify assets to S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = config('aws.credentials.key');
        $secret = config('aws.credentials.secret');
        $local  = public_path().'/';
        $remote = env('APP_ENV') == 'production' ? 's3://earth-consumerfe-assets/' : 's3://staging-earth-consumer-assets/';

        exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
           '.$local.'assets/css/ '.$remote.'assets/css/ \
           --cache-control "public, max-age=31536000" \
           --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
           --acl public-read \
           --content-encoding "gzip" \
           --recursive \
           --exclude "*" \
           --include "*.min.css"
           ');
        exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
           '.$local.'assets/dependencies/ '.$remote.'assets/dependencies/ \
           --cache-control "public, max-age=31536000" \
           --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
           --acl public-read \
           --content-encoding "gzip" \
           --recursive \
           --exclude "*" \
           --include "*.min.css"
           ');
        exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
          '.$local.'js/ '.$remote.'js/ \
          --cache-control "public, max-age=31536000" \
          --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
          --acl public-read \
          --content-encoding "gzip" \
          --recursive \
          --exclude "*" \
          --include "*.min.js"
         ');
        exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
          '.$local.'semantic/dist/ '.$remote.'semantic/dist/ \
          --cache-control "public, max-age=31536000" \
          --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
          --acl public-read \
          --content-encoding "gzip" \
          --recursive \
          --exclude "*" \
          --include "*.min.css"
         ');
        exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
          '.$local.'font-awesome/ '.$remote.'font-awesome/ \
          --cache-control "public, max-age=31536000" \
          --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
          --acl public-read \
          --recursive \
         ');
         exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
           '.$local.'assets/microsites/css/ '.$remote.'assets/microsites/css/ \
           --cache-control "public, max-age=31536000" \
           --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
           --acl public-read \
           --content-encoding "gzip" \
           --recursive \
           --exclude "*" \
           --include "*.min.css"
          ');
         exec('AWS_ACCESS_KEY_ID='.$key.' AWS_SECRET_ACCESS_KEY='.$secret.' aws s3 cp \
           '.$local.'assets/microsites/js/ '.$remote.'assets/microsites/js/ \
           --cache-control "public, max-age=31536000" \
           --expires "Thu, 15 Apr 2020 20:00:00 GMT" \
           --acl public-read \
           --content-encoding "gzip" \
           --recursive \
           --exclude "*" \
           --include "*.min.js"
          ');
    }
}
