<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class assetsDeleteImg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:delete-img';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete images from public assets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \File::cleanDirectory(public_path('assets/img/ad_banner'));
        \File::cleanDirectory(public_path('assets/img/agency/logo'));
        \File::cleanDirectory(public_path('assets/img/ancillary/logo'));
        \File::cleanDirectory(public_path('assets/img/ancillary/servicer/logo'));
        \File::cleanDirectory(public_path('assets/img/banner'));
        \File::cleanDirectory(public_path('assets/img/default'));
        \File::cleanDirectory(public_path('assets/img/no_listing'));
    }
}
