<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Utility;

class assetsSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Zoomproperty sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle(Utility $utility)
     {
         $utility->sitemap_feeds();
     }
}
