<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class assetsCompression extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:compress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compress public files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $local  = public_path().'/';

        exec('find '.$local.'js \( -iname "*.min.js" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        exec('find '.$local.'assets/css \( -iname "*.min.css" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        exec('find '.$local.'assets/microsites/js \( -iname "*.min.js" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        exec('find '.$local.'assets/microsites/css \( -iname "*.min.css" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        #NOTE uncomment only if semeantic or dependencies files change
        // exec('find '.$local.'assets/dependencies \( -iname "intlTelInput.min.css" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        // exec('find '.$local.'assets/dependencies \( -iname "slider.min.css" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        // exec('find '.$local.'semantic/dist \( -iname "semantic.min.css" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
        // exec('find '.$local.'semantic/dist \( -iname "semantic.rtl.min.css" \) -exec gzip -9 -n {} \; -exec mv {}.gz {} \;');
    }
}
