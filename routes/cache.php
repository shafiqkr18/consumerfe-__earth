<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Auth:: CORS -> Middleware
 *                  =>  Auth,
 *                  =>  Throttle
 */
Route::middleware(['api'])->group(function(){
    Route::prefix('earth')->group( function(){
        Route::get('build/{cache}','Cache@build')->name('build_cache');
        Route::get('build/{cache}/{type}','Cache@build')->name('build_cache_type');
    });

});
