<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
* Auth:: CORS -> Middleware
*                  =>  Auth,
*                  =>  Throttle
*/
Route::middleware(['api','cors','request'])->group(function(){
    Route::prefix('earth')->group( function(){
        //Property
        Route::prefix('property')->group( function(){
            // Search, Budget, Featured
            Route::get( '/', 'Property@index' );
            // Construct Property Search URL
            Route::get( '/url', 'Property@create_property_search_url' );
            // Details
            Route::get( '/{id}', 'Property@show' );
            // Similar
            Route::get( '/{id}/similar', 'Property@similar' );
        });
        //Agency
        Route::prefix('agency')->group( function(){
            // Search
            Route::get( '/', 'Agency@index' );
            // Details
            Route::get( '/{id}', 'Agency@show' );
            // Similar
            Route::get( '/{id}/similar', 'Agency@similar' );
        });
        //Agent
        Route::prefix('agent')->group( function(){
            // Search
            Route::get( '/', 'Agent@index' );
            // Details
            Route::get( '/{id}', 'Agent@show' );
            // Similar
            Route::get( '/{id}/similar', 'Agent@similar' );
        });
        //Service
        Route::prefix('service')->group( function(){
            // Search
            Route::get( '/types', 'Service@types' );
            // Search
            Route::get( '/', 'Service@index' );
            // Details
            Route::get( '/{id}', 'Service@show' );
            // Similar
            Route::get( '/{id}/similar', 'Service@similar' );
        });
        //Developer
        Route::prefix('developer')->group( function(){
            // Search
            Route::get( '/', 'Developer@index' );
            // Details
            Route::get( '/{id}', 'Developer@show' );
            // Similar
            Route::get( '/{id}/similar', 'Developer@similar' );
        });
        //Project
        Route::prefix('project')->group( function(){
            // Search
            Route::get( '/', 'Project@index' );
            // Details
            Route::get( '/{id}', 'Project@show' );
            // Similar
            Route::get( '/{id}/similar', 'Project@similar' );
        });

        Route::prefix('analytic')->group( function(){
            Route::prefix('{model}')->group( function(){
                //GUIDE ME
                Route::prefix('guideme')->group( function(){
                    // Listings
                    Route::get( '/listings', 'Analytic@guide_me_listings' );
                    // Stats
                    Route::get( '/stats', 'Analytic@guide_me_stats' );
                });
            });
        });
        // User
        Route::prefix('user')->group( function(){
            // auth
            Route::prefix('auth')->group( function(){
                // register
                Route::post('register', 'User@register');
                // login
                Route::post('login', 'User@login');
                // Social
                Route::post('social', 'User@social_auth');
                // verify
                Route::post('verify', 'User@verify_email');
                // forgot password
                Route::post('forgot', 'User@forgot_password');
                // reset password
                Route::post('reset', 'User@reset_password');
                // logout
                Route::get('logout', 'User@logout');
            });
            // User specific authed actions
            Route::middleware([ 'is_logged_in' ])->group(function (){
                Route::prefix('{id}')->group( function($id){
                    // Details
                    Route::get( '/', 'User@show' );
                    // Update
                    Route::put( '/', 'User@update' );
                    // auth
                    Route::prefix('auth')->group( function(){
                        // update password
                        Route::put('change', 'User@change_password');
                    });
                    // Favourites
                    Route::prefix('favourite')->group( function($id){
                        // All Favourites
                        Route::get( '/', 'Favourite@index' );
                        // Favourite Specific actions
                        Route::prefix('{model}')->group( function($model){
                            // All of model
                            Route::get( '/', 'Favourite@index' );
                            // Favourite a search
                            Route::post( '/', 'Favourite@store' );
                            //Actions on models
                            Route::prefix('{model_id}')->group( function($model_id){
                                // Favourite a particular model
                                Route::post( '/', 'Favourite@store' );
                                // Details of particular favourite
                                Route::get( '/', 'Favourite@show' );
                                // Delete search
                                Route::delete( '/', 'Favourite@destroy' );
                            });
                        });
                    });
                    // Alerts
                    Route::prefix('alert')->group( function($id){
                        // All Alerts
                        Route::get( '/', 'Alert@index' );
                        // Alert Specific actions
                        Route::prefix('{model}')->group( function($model){
                            // All of model
                            Route::get( '/', 'Alert@index' );
                            // Alert a search
                            Route::post( '/', 'Alert@store' );
                            //Actions on models
                            Route::prefix('{model_id}')->group( function($model_id){
                                // Alert a particular model
                                Route::post( '/', 'Alert@store' );
                                // Update an alert
                                Route::put( '/', 'Alert@update' );
                                // Details of particular alert
                                Route::get( '/', 'Alert@show' );
                                // Delete alert
                                Route::delete( '/', 'Alert@destroy' );
                            });
                        });
                    });
                    // Lead
                    Route::prefix('lead')->group( function($id){
                        // All Leads
                        Route::get( '/', 'Lead@index' );
                        Route::prefix('{model}')->group( function($model){
                            // All of model
                            Route::get( '/', 'Lead@index' );
                        });
                    });
                });
            });
            // Subscribe to newsletter
            Route::group([ 'prefix' => 'newsletter' ], function(){
                Route::post( 'subscribe/{email}', 'User@newsletter_subscribe' );
                Route::post( 'unsubscribe/{email}', 'User@newsletter_unsubscribe' );
            });
        });
        //Project
        Route::prefix('lead')->group( function(){
            Route::prefix('{model}')->group( function($model){
                // Lead
                Route::post( '/', 'Lead@store' );
                Route::post( 'mortgage', 'Lead@mortgage_calc_lead' );
                Route::post( 'listings', 'Lead@property_listings_lead' );
            });
        });
        //Data
        Route::prefix('data')->group( function(){
            // Property search
            Route::get( 'property/search', 'JsonData@property' );
            Route::get( 'brokerage/search', 'JsonData@brokerages' );
            Route::get( 'service/search', 'JsonData@services' );
            Route::get( 'project/search', 'JsonData@projects' );
        });
        Route::prefix('enquiry')->group( function(){
            Route::post( 'book_viewing', 'Utility@book_viewing_lead' );
            Route::post( 'ideal_property', 'Utility@ideal_property_lead' );
            // Route::post( 'scholarship', 'Lead@scholarship_lead' );
        });
        Route::prefix('seo')->group( function(){
            Route::get( 'contents', 'Seo@get_details' );
        });
        Route::prefix('takeover')->group( function(){
            Route::get( '/', 'Takeover@index' );
        });
        Route::prefix('publicityvideo')->group( function(){
            Route::get( '/', 'PublicityVideo@index' );
        });
        //Microsite
        Route::prefix('microsite')->group( function(){
            // Search
            Route::get( '/', 'Microsite@index' );
            // Details
            Route::get( '/{id}', 'Microsite@show' );
            // Get microsite by slug
            Route::get( 'details', 'Microsite@get_details' );
        });
    });
});
