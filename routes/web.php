<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix( 'utility' )->group(function(){
  //Populate Search Web config file
  Route::get( 'populate-search', 'Utility@populate_search_config')->name( 'populate_search' );
  //Download config files from s3
  Route::get( 'download/{files}', 'Utility@download_configs')->name( 'download' );
  //Building search file
  Route::get( 'search-file', 'Utility@build_search_config')->name( 'build_search' );
  //Building sitemap xml
  Route::get( 'sitemap-xml', 'Utility@sitemap_feeds')->name( 'sitemap_xml' );
  Route::get( 'urls', 'Utility@urls')->name( 'sitemap_xml' );
  //Lang files for dynamic data
  Route::get( 'languages', 'Utility@populate_lang')->name( 'populate_lang' );
});

Route::group( [ 'middleware' => ['localize'] ], function() {

  // SSL Validation
  Route::get( '.well-known/pki-validation/DB424B8262B58D42061F77F21DA94A64.txt',  function(){
    return '69AD803B54D85180175D7B8A56BCD3FEF22FF7379A6EA752288D04D7AE2EFBF8 comodoca.com 5ac9bb00787a9';
  } );
  // Twilio
  // Route::post( 'messaging/token', '\MessagingPKG\Controllers\Token@generate' )->name( 'token.generate' );

  // Social Auth Redirect
  Route::get( 'login/{provider}', 'Socialites@redirect' )->where( [ 'provider' => 'facebook|google' ] );
  // Social Auth Callback
  Route::get( 'login/{provider}/callback', 'Socialites@callback' )->where( [ 'provider' => 'facebook|google' ] );

  // Localized Routes
  Route::prefix( '{lng?}' )->group(function(){
    // Sitemap
    Route::prefix( 'sitemap' )->group(function(){
      Route::get( '/', 'Utility@get_main_sitemap_feed')->name( 'get_main_sitemap' );
      Route::get( 'reduce', 'Utility@get_main_sitemap_feed_reduce')->name( 'get_main_sitemap_reduce' );
      Route::get( '{feed}', 'Utility@get_sitemap_feed')->name( 'get_sitemap' );
      Route::get( 'reduce/{feed}', 'Utility@get_sitemap_feed_reduce')->name( 'get_sitemap_reduce' );
    });
    // Blog
    Route::prefix( 'blog' )->group(function(){
      // Recent Posts
      Route::get( 'recent', function(){
        return html_entity_decode( htmlentities(file_get_contents( 'http://blog.zoomproperty.com/rss-recentposts' )) );
      } );
      // Area Guide
      Route::get( 'guide/{area}', function(...$vars){
        $area       =   end($vars);
        $blog       =   preg_replace( "/\s+/", "-", strtolower( preg_replace( "/[^A-Za-z0-9\s\-]/", "", $area ) ) );
        $feeds      =   htmlentities(file_get_contents( 'https://blog.zoomproperty.com/'.$blog.'/feed/?withoutcomments=1' ));
        $feeds      =   str_replace( "content:encoded", "contentEncoded", $feeds );
        $xml        =   simplexml_load_string( html_entity_decode( $feeds ), 'SimpleXMLElement', LIBXML_NOCDATA );
        echo str_replace( '<div style="position:absolute;top:0;left:-9999px;">Did you find apk for android? You can find new <a href="https://dlandroid24.com/">Free Android Games</a> and apps.</div>', "", str_replace( '<div style="position:absolute;top:0;left:-9999px;">Want create site? Find <a href="http://dlwordpress.com/">Free WordPress Themes</a> and plugins.</div>', "", str_replace( "<p>Data</p>", "", str_replace( "<h1>Data</h1>", "", $xml->channel->item->contentEncoded ) ) ) );

        } );
      });

      // T&C
      Route::get( 'terms-and-conditions', 'Web@t_and_c')->name( 'terms-and-conditions' );
      // PP
      Route::get( 'privacy-policy', 'Web@privacy_policy')->name( 'privacy-policy' );
      // Real Choice Landing Page
      Route::get( 'real-choice', 'Web@real_choice')->name( 'real-choice' );
      // Guide Me
      Route::get( 'guide-me', 'Web@guide_me')->name( 'guide-me' );
      // Scholarship Landing Page
      Route::get( 'scholarship', 'Web@scholarship')->name( 'scholarship' );
      // Reset password
      Route::get( 'reset-password/{id?}', 'Web@reset_password')->name( 'reset-password' );
      // Budget Search
      Route::prefix( 'budget-search' )->group(function(){
        // Landing
        Route::get( '/', 'Web@budget_search')->name( 'budget-search' );
        // Results
        Route::get( 'results/{catchall?}', 'Web@budget_search_results' )->name( 'budget-search-results' );
        // Results
        Route::get( 'data/{catchall?}', 'Web@budget_search_results_data' )->name( 'budget-search-results-data' );
      });
      // Agents Search
      Route::prefix( 'agents-search' )->group(function(){
        // Landing
        Route::get( '/', 'Web@agent_finder')->name( 'agent-finder' );
        // Results
        Route::get( '{city}/{agency}/{suburb}', 'Web@agent_finder_listings')->name( 'agent-finder-listings' );
        // Details
        Route::get( '{name}/{id}', 'Web@agent_finder_details' )->name( 'agent-finder-details' );
      });
      // Landing Page
      Route::get( '/', 'Web@landing_page' )->name( 'landing-page' );
      // Projects
      Route::group( [ 'prefix' => 'projects', 'middleware' => ['token'] ], function() {
        // Details
        Route::get( 'details/{id}', 'Web@all_developers_details' )->name( 'new-project-details' );
        // Results
        // Route::get( 'uae/{dev?}', 'Web@all_developers')->name( 'new-projects-list' );
        Route::prefix('{country?}')->group( function($country){
          Route::get( '/', 'Web@all_developers')->name( 'new-projects-list' )->where( [
            'country' => 'uae|international'
            ] );
            Route::get( '{dev?}', 'Web@all_developers');
          });
        });
        // User
        Route::prefix( 'users' )->group(function(){

          Route::middleware([ 'is_logged_in' ])->group(function (){

            Route::prefix('{id}')->group( function($id){
              // Favourites
              Route::prefix('favourite')->group( function($id){
                // All Favourites
                Route::get( '/', 'Web@all_favourites_user' );
                // Favourite Specific actions
                Route::prefix('{model}')->group( function($model){
                  // All of model
                  Route::get( '/', 'Web@all_favourites_user' )->name( 'all-favourites-user' );
                });
              });
              Route::prefix('alert')->group( function($id){
                // All Favourites
                Route::get( '/', 'Web@all_alerts_user' );
                // Favourite Specific actions
                Route::prefix('{model}')->group( function($model){
                  // All of model
                  Route::get( '/', 'Web@all_alerts_user' )->name( 'all-alerts-user' );
                });
              });
            });
          });
        });

        // Services
        Route::prefix( 'services' )->group(function(){
          // Landing
          Route::get( '/', 'Web@services')->name( 'services' );
          // Results
          Route::get( '{type}/uae', 'Web@services_listings')->name( 'services_listings' );
        });

        if (in_array(Request::segment(2), ['rent','buy','short-term-rent','sale','terms-and-conditions','privacy-policy','budget-search','agents-search','services','projects','users',null])){
          // Property
          Route::prefix( '{rent_buy}' )->group(function(){
            // Details
            Route::get( '{residential_commercial?}/{name?}/{id}', 'Web@property_details')->name( 'property-details-page' )->where( [
              'id' => 'zp-.*'
              ] );
              // All Other Search Results
              Route::get( '{residential_commercial?}/{city?}/{suburb?}/{catchall?}', 'Web@property_search_results')
              ->name( 'search-results-page' );
            });
          }

          Route::prefix( '{developer}' )->group(function(){
            Route::get( '/', 'Web@microsites');
            Route::prefix( '{project}' )->group(function(){
              Route::get( '/', 'Web@microsites');
            });
          });

        });

      });

      // Social Auth
      // 404 Catchall
      Route::any( '{catchall}', function ( $page = null ) {
        return redirect('/');
      } )->where('catchall', '(.*)');
